import { servicesVersion } from "typescript";
import {strings} from "../lang/Strings";

export type select = {
  key: number;
  value: any;
  label: string;
};

export type pet = {
  age: number;
  image: string;
  name: string;
  location: string;
  weight: number;
};

export const successOptions = {
  position: "top-right",
  style: {
    backgroundColor: "white",
    color: "green",
    fontFamily: "Menlo, monospace",
    fontSize: "16px",
    textAlign: "center",
  },
  closeStyle: {
    color: "green",
    fontSize: "16px",
  },
};

export const errorOptions = {
  position: "top-right",
  zIndex: 102,
  style: {
    backgroundColor: "white",
    color: "red",
    fontFamily: "Menlo, monospace",
    fontSize: "16px",
    textAlign: "center",
    zIndex: 102,
  },
  closeStyle: {
    color: "red",
    fontSize: "16px",
  },
};

export const serviceFeeObject = {
  capacity: 1,
  pet_size_id: 1,
  service_charge: "800",
  earning_amount: "640",
};

export const cities: select[] = [
  { key: 1, label: strings.ShinjukuCity, value: "Shinjuku City" },
  { key: 2, label: strings.AnotherCity, value: "Another City" },
];

export const pets = [
  { key: 1, label: strings.Dog, value: 1 },
  { key: 2, label: strings.Cat, value: 2 },
  { key: 3, label: strings.Birds, value: 3 },
  { key: 4, label: strings.Reptiles, value: 4 },
  { key: 5, label: strings.Smallanimals, value: 5 },
];


type service = {
  name: string;
  description: string;
  id: number;
};

export const serviceData: select[] = [
  { key: 1, value: 1, label: strings.Boarding },
  { key: 2, value: 2, label: strings.HouseSitting },
  { key: 3, value: 3, label: strings.DropInVisits },
  { key: 4, value: 4, label: strings.PetDayCare },
  { key: 5, value: 5, label: strings.DogWalking },
  { key: 6, value: 6, label: strings.Petgrooming },
  { key: 7, value: 7, label: strings.HouseCall },
];

export const petType: select[] = [
  { key: 1, value: "Dog Boarding", label: strings.DogBoarding },
  { key: 2, value: "House Sitting", label: strings.houseSitting },
  { key: 3, value: "Drop-in-visits", label: strings.DropInVisits },
  { key: 4, value: "Pet Day care", label: strings.PetDayCare },
  { key: 5, value: "Pet walking", label: strings.PetWalking },
];

export const prices: select[] = [
  { key: 1, value: 1, label: strings.AllPrices },
  { key: 2, value: 2, label: "10000-20000" },
  { key: 3, value: 3, label: "20000-30000" },
  { key: 4, value: 4, label: `50000 - ${strings.more}` },
];

export const ServiceTime: select[] = [
  { key: 1, value: "One Time", label: strings.OneTime },
  { key: 2, value: "Repeat Weekly", label: strings.RepeatWeekly },
];

export const petSize: select[] = [
  { key: 1, value: 1, label: `0 - 5 ${strings.kg}` },
  { key: 2, value: 2, label: `5 - 10 ${strings.kg}` },
  { key: 3, value: 3, label: `10 - 25 ${strings.kg}` },
  { key: 4, value: 4, label: `25 - 40 ${strings.kg}` },
  { key: 5, value: 5, label: `40+ ${strings.kg}` },
];
export const petSizeSearchOption: select[] = [
  { key: 0, value: null, label: strings.Select },
  { key: 1, value: 1, label: `0 - 5 ${strings.kg}` },
  { key: 2, value: 2, label: `5 - 10 ${strings.kg}` },
  { key: 3, value: 3, label: `10 - 25 ${strings.kg}` },
  { key: 4, value: 4, label: `25 - 40 ${strings.kg}` },
  { key: 5, value: 5, label: `40+ ${strings.kg}` },
];
export const reviewSorting: select[] = [
  { key: 1, value: 1, label: strings.highToLow },
  { key: 2, value: 2, label: strings.lowToHigh },
  { key: 3, value: 3, label: strings. NewtoOld },
  { key: 4, value: 4, label: strings.OldtoNew },

];
export const starRating: select[] = [
  { key: 0, value: 0, label: strings.All },
  { key: 1, value: 1, label: strings.One },
  { key: 2, value: 2, label: strings.Two },
  { key: 3, value: 3, label: strings.Three},
  { key: 4, value: 4, label: strings.Four },
  { key: 5, value: 5, label: strings.Five },
  
];

export const sitterSorting: select[] = [
  { key: 1, value: 1, label: strings.HighestRating },
  { key: 2, value: 2, label: strings.LowestRating },
  { key: 3, value: 3, label: strings.HighestReview },
  { key: 4, value: 4, label: strings.LowestReview },
  { key: 5, value: 5, label: strings.ClosestSitter },
];

export const experienceOption: select[] = [
  { key: 1, value: 1, label: strings._0_1_year },
  { key: 3, value: 3, label: strings._1_3_year },
  { key: 5, value: 5, label: strings._3_5_year },
  { key: 7, value: 7, label: strings._5_7_year },
  { key: 8, value: 8, label: strings.addCustom },
];

export const favouriteSorting: select[] = [
  { key: 0, value: 'asc', label: strings.NewtoOld },
  { key: 1, value: 'desc', label: strings.OldtoNew }
];

export const durations: select[] = [
  { key: 1, value: 30, label: `30 ${strings.mins}` },
  { key: 2, value: 60, label: `60 ${strings.mins}` },
  { key: 3, value: 90, label: `90 ${strings.mins}` },
  { key: 4, value: 120, label: `120 ${strings.mins}` },
];
export const durationsSearchOption: select[] = [
  { key: 0, value: null, label: strings.Select },
  { key: 1, value: 30, label: `30 ${strings.mins}` },
  { key: 2, value: 60, label: `60 ${strings.mins}` },
  { key: 3, value: 90, label: `90 ${strings.mins}` },
  { key: 4, value: 120, label: `120 ${strings.mins}` },
];
export const petCount: select[] = [
  { key: 0, value: null, label: strings.Select },
  { key: 1, value: 1, label: "1" },
  { key: 2, value: 2, label: "2" },
  { key: 3, value: 3, label: "3" },
];

export const sort: select[] = [
  {
    key: 1,
    value: "Distance closest to furthest",
    label: strings.DistanceClosestToFurthest,
  },
  { key: 2, value: "Ranking", label: strings.Ranking },
  { key: 3, value: "Number of reviews", label: strings.NumberOfReviews },
];

export const reviewFilter: select[] = [
  { key: 1, value: 'asc', label: strings.NewtoOld },
  { key: 2, value: 'desc', label: strings.OldtoNew },
];

export const pet: select[] = [
  { key: 1, label: strings.Dog, value: "1" },
  { key: 2, label: strings.Cat, value: "2" },
  { key: 3, label: strings.Birds, value: "3" },
  { key: 4, label: strings.Reptiles, value: "4" },
  { key: 5, label: strings.Smallanimals, value: "5" },
];

export const gender = [
  { label: strings.Male, value: 0 },
  { label: strings.Female, value: 1 },
];

// dummy data
export const newsAndEvents = [
  {
    key: 1,
    posted: "07/10/2021, 3PM",
    post: "Petcitation website will be on maintenance from 3PM to 6PM on 21July 2021(Japan Time).We apologise for the inconvinience caused.",
  },
  {
    key: 2,
    posted: "07/10/2021, 3PM",
    post: "Petcitation website will be on maintenance from 3PM to 6PM on 21July 2021(Japan Time).We apologise for the inconvinience caused.",
  },
  {
    key: 3,
    posted: "07/10/2021, 3PM",
    post: "Petcitation website will be on maintenance from 3PM to 6PM on 21July 2021(Japan Time).We apologise for the inconvinience caused.",
  },
  {
    key: 4,
    posted: "07/10/2021, 3PM",
    post: "Petcitation website will be on maintenance from 3PM to 6PM on 21July 2021(Japan Time).We apologise for the inconvinience caused.",
  },
  {
    key: 5,
    posted: "07/10/2021, 3PM",
    post: "Petcitation website will be on maintenance from 3PM to 6PM on 21July 2021(Japan Time).We apologise for the inconvinience caused.",
  },
  {
    key: 6,
    posted: "07/10/2021, 3PM",
    post: "Petcitation website will be on maintenance from 3PM to 6PM on 21July 2021(Japan Time).We apologise for the inconvinience caused.",
  },
];

export const faq = [
  {
    id: "1",
    question: "1. How do I assure pet sitter is a genuine person?",
    answer:
        "We advise our students to start at least 15 months prior to the intake period of an educational institute. This allows suffcient time to build an ideal profile and process the application.",
  },
  {
    id: "2",
    question: "2. How do I assure pet sitter is a genuine person?",
    answer:
        "We advise our students to start at least 15 months prior to the intake period of an educational institute. This allows suffcient time to build an ideal profile and process the application.",
  },
  {
    id: "3",
    question: "3. How do I assure pet sitter is a genuine person?",
    answer:
        "We advise our students to start at least 15 months prior to the intake period of an educational institute. This allows suffcient time to build an ideal profile and process the application.",
  },
  {
    id: "4",
    question: "4. How do I assure pet sitter is a genuine person?",
    answer:
        "We advise our students to start at least 15 months prior to the intake period of an educational institute. This allows suffcient time to build an ideal profile and process the application.",
  },
];

export const myPets: pet[] = [
  {
    age: 7,
    image: "/",
    name: "Milo",
    location: "Siberian husky",
    weight: 16,
  },
  {
    age: 7,
    image: "/",
    name: "Milo",
    location: "Siberian husky",
    weight: 16,
  },
];

export const reviewStateData = {
  pet_booking_id: 189,
  rating: 4,
  cleanliness: 4,
  accuracy: 4,
  communication: 4,
  location: 4,
  checkIn: 4,
  value: 5,
};

export const reviewObjectData = {
  userImage: "/",
  userName: "Mark Evans",
  date: "June 10, 2021",
  time: "7:30pm",
  review:
      "“Abbey is the best dog mom I’ve ever known! Responsible, attentive, playful, and loving. I met her when she was volunteering to help shelter animals - her empathy for animal souls is limitless.”",
  rating: 4,
  userImages: ["/", "/", "/", "/"],
  userCount: 10,
  sitterResponse: true,
  sitterReply: "Thank you for your kind words Mark!",
  sitterImage: "/",
  sitterName: "Rebecca W.",
};

export const sitterServices = [
  {
    service: "Boarding",
    description: "in the sitter's home",
    price: "¥40",
    period: "per night",
  },
  {
    service: "House Sitting",
    description: "in the home",
    price: "¥40",
    period: "per night",
  },
  {
    service: "Drop-In Visits",
    description: "visit in your home",
    price: "¥40",
    period: "per night",
  },
  {
    service: "Doggy Day Care",
    description: "in the sitter's home",
    price: "¥40",
    period: "per night",
  },
  {
    service: "Pet Walking",
    description: "in your neighborhood",
    price: "¥40",
    period: "per night",
  },
];

export const sitterStates = {
  bookingForMe: 100,
  bookingByMe: 50,
  pBookingForMe: 20,
  pBookingByMe: 15,
  pTotalBooking: 25,
  totalEarning: 34000,
  pTotalEarning: -25,
  totalReferral: 10,
  pTotalReferral: 10,
};

export const requestsArray = [
  {
    name: "Finch’s",
    service: "Dog Walk",
    address: "Vancouver, WA, 98686",
    from: "29 Jun 2021",
    to: "30 Jun 2021",
    total: "¥350",
  },
  {
    name: "Finch’s",
    service: "Dog Walk",
    address: "Vancouver, WA, 98686",
    from: "29 Jun 2021",
    to: "30 Jun 2021",
    total: "¥360",
  },
  {
    name: "Finch’s",
    service: "Dog Walk",
    address: "Vancouver, WA, 98686",
    from: "29 Jun 2021",
    to: "30 Jun 2021",
    total: "¥370",
  },
  {
    name: "Finch’s",
    service: "Dog Walk",
    address: "Vancouver, WA, 98686",
    from: "29 Jun 2021",
    to: "30 Jun 2021",
    total: "¥380",
  },
];

export const meetingRequests = [
  {
    date: 0,
    service: "Day Care",
    clientName: "David T.",
    address: " Vancouver, WA, 98686",
  },
  {
    date: 0,
    service: "Day Care",
    clientName: "David T.",
    address: " Vancouver, WA, 98687",
  },
  {
    date: 0,
    service: "Day Care",
    clientName: "David T.",
    address: " Vancouver, WA, 98688",
  },
  {
    date: 0,
    service: "Day Care",
    clientName: "David T.",
    address: " Vancouver, WA, 98689 ",
  },
];

export const reservationRequests = [
  {
    id: 1,
    from: "29 Jun 2021, 3PM",
    to: "30 Jun 2021, 3PM",
    paymentStatus: "PAID",
    total: "¥350",
    name: "Molly’s - Day Care",
    clientName: "David T.",
    address: "Vancouver, WA, 98686",
    image: "/",
  },
  {
    id: 1,
    from: "29 Jun 2021, 3PM",
    to: "30 Jun 2021, 3PM",
    paymentStatus: "PAID",
    total: "¥350",
    name: "Molly’s - Day Care",
    clientName: "David T.",
    address: "Vancouver, WA, 98686",
    image: "/",
  },
  {
    id: 1,
    from: "29 Jun 2021, 3PM",
    to: "30 Jun 2021, 3PM",
    paymentStatus: "PAID",
    total: "¥350",
    name: "Molly’s - Day Care",
    clientName: "David T.",
    address: "Vancouver, WA, 98686",
    image: "/",
  },
  {
    id: 1,
    from: "29 Jun 2021, 3PM",
    to: "30 Jun 2021, 3PM",
    paymentStatus: "PAID",
    total: "¥350",
    name: "Molly’s - Day Care",
    clientName: "David T.",
    address: "Vancouver, WA, 98686",
    image: "/",
  },
  {
    id: 1,
    from: "29 Jun 2021, 3PM",
    to: "30 Jun 2021, 3PM",
    paymentStatus: "PAID",
    total: "¥350",
    name: "Molly’s - Day Care",
    clientName: "David T.",
    address: "Vancouver, WA, 98686",
    image: "/",
  },
];

//Important

export const myServices = [
  {
    serviceName: "Boarding",
    description: "Your client’s pets come to your home and stay overnight.",
    active: true,
    instantBooking: false,
    specifications: [
      "Lives in a House",
      "Get your first booking sooner",
      "Make more money on the weekends (Thu-Sun)",
    ],
    route: "/user/my-services/boarding",
  },
  {
    serviceName: "Grooming",
    description:
        "Your client can come or you can go for grooming services of their pets",
    active: true,
    instantBooking: true,
    specifications: [],
    route: "/user/my-services/grooming",
  },
  {
    serviceName: "House Sitting",
    description:
        "You go to your client’s home and stay overnight, taking care of their dogs and home.",
    active: false,
    instantBooking: true,
    specifications: [],
    route: "/user/my-services/boarding",
  },
  {
    serviceName: "Drop-In Visits",
    description:
        "Your clients ask you to do 60-minute home visits to feed and play with their pets",
    active: true,
    instantBooking: true,
    specifications: [],
    route: "/user/my-services/boarding",
  },
  {
    serviceName: "Pet Day Care",
    description:
        "Your client’s pets stay at your home during the day, Drop offs are around 7-9am, and pick ups are around 4-6pm.",
    active: true,
    instantBooking: true,
    specifications: [],
    route: "/user/my-services/boarding",
  },
  {
    serviceName: "Pet Walking",
    description:
        "Your clients request 30-minute dog walk in their neighborhood",
    active: true,
    instantBooking: true,
    specifications: [],
    route: "/user/my-services/boarding",
  },
  {
    serviceName: "House Call",
    description:
        "Your client’s ask you to bring veterinary care into client’s home.",
    active: true,
    instantBooking: true,
    specifications: [],
    route: "/user/my-services/boarding",
  },
];
export const spots = [
  {
    images: [
      "/images/img9.png",
      "/images/img9.png",
      "/images/img9.png",
      "/images/img9.png",
      "/images/img9.png",
    ],
    name: "Pets Home Stay",
    location: "Shinjuku City, Tokyo",
    availableFor: ["Dog", "Cat"],
    rating: "4.69",
    review: 10,
    repeatedClient: 43,
  },
  {
    images: [
      "/images/img9.png",
      "/images/img9.png",
      "/images/img9.png",
      "/images/img9.png",
      "/images/img9.png",
    ],
    name: "Pets Home Stay",
    location: "Shinjuku City, Tokyo",
    availableFor: ["Dog", "Cat"],
    rating: "4.69",
    review: 10,
    repeatedClient: 43,
  },
];

export const reviews = [
  {
    name: "Doggytales Home Stay",
    place: "Pet friendly Hotel",
    address: "Shinjuku City, Tokyo",
    review:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    rating: 4.5,
    image: "/images/review-img1.png",
  },
  {
    name: "Doggytales Home Stay",
    place: "Pet friendly Hotel",
    address: "Shinjuku City, Tokyo",
    review:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    rating: 4,
    image: "/images/review-img1.png",
  },
  {
    name: "Doggytales Home Stay",
    place: "Pet friendly Hotel",
    address: "Shinjuku City, Tokyo",
    review:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    rating: 3.5,
    image: "/images/review-img1.png",
  },
  {
    name: "Doggytales Home Stay",
    place: "Pet friendly Hotel",
    address: "Shinjuku City, Tokyo",
    review:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    rating: 5,
    image: "/images/review-img1.png",
  },
];

