import { select } from "./AppData";
import {strings} from "../lang/Strings";
import {getCurrencySign} from "../../api/Constants";

export const D_WALK_COUNT: select[] = [
  {
    value: 1,
    label: `1 ${strings.Dog}`,
    key: 1,
  },
  {
    value: 2,
    label: `2 ${strings.Dogs}`,
    key: 2,
  },
  {
    value: 3,
    label: `3 ${strings.Dogs}`,
    key: 3,
  },
  {
    value: 4,
    label: `4 ${strings.Dogs}`,
    key: 4,
  },
  {
    value: 5,
    label: `5 ${strings.Dogs}`,
    key: 5,
  },
  {
    value: 6,
    label: `6 ${strings.Dogs}`,
    key: 6,
  },
];

export const D_FLEXIBILITY: select[] = [
  {
    value: 1,
    label: strings.Flexible,
    key: 1,
  },
  {
    value: 2,
    label: strings.FullTime,
    key: 2,
  },
  {
    value: 3,
    label: strings.PartTime,
    key: 3,
  },
];

export const D_FREQUENT_BREAKS: select[] = [
  {
    value: 1,
    label: `0 - 2 ${strings.Hrs}`,
    key: 1,
  },
  {
    value: 2,
    label: `2 - 4 ${strings.Hrs}`,
    key: 2,
  },
];

export const D_BOOLEAN: select[] = [
  {
    value: true,
    label: strings.Yes,
    key: 1,
  },
  {
    value: false,
    label: strings.No,
    key: 2,
  },
];

export const D_TRANSPORTATION: select[] = [
  {
    value: 1,
    label: strings.Car,
    key: 1,
  },
  {
    value: 2,
    label: strings.Bus,
    key: 2,
  },
  {
    value: 3,
    label: strings.Train,
    key: 3,
  },
  {
    value: 4,
    label: strings.Walk,
    key: 4,
  },
];

export const D_DISTANCE: select[] = [
  { value: 7, label: `300 ${strings.m}`, key: 7 },
  { value: 8, label: `500 ${strings.m}`, key: 8 },
  { value: 9, label: `800 ${strings.m}`, key: 9 },
  { value: 1, label: `1 ${strings.km}`, key: 1 },
  { value: 2, label: `3 ${strings.km}`, key: 2 },
  { value: 3, label: `5 ${strings.km}`, key: 3 },
  { value: 4, label: `10 ${strings.km}`, key: 4 },
  { value: 5, label: `15 ${strings.km}`, key: 5 },
  { value: 6, label: `20 ${strings.km}`, key: 6 },
];

export const D_BUDGET: select[] = [
  { value: 0, label: strings.none, key: 0 },
    { value: 1, label: `~ ${getCurrencySign()} 999`, key: 1 },
  { value: 2, label: `${getCurrencySign()} 1,000 ~ ${getCurrencySign()} 1,999`, key: 2 },
  { value: 3, label: `${getCurrencySign()} 2,000 ~ ${getCurrencySign()} 2,999`, key: 3 },
  { value: 4, label: `${getCurrencySign()} 3,000 ~ ${getCurrencySign()} 3,999`, key: 4 },
  { value: 5, label: `${getCurrencySign()} 4,000 ~ ${getCurrencySign()} 4,999`, key: 5 },
  { value: 6, label: `${getCurrencySign()} 5,000 ~ ${getCurrencySign()} 5,999`, key: 6 },
  { value: 7, label: `${getCurrencySign()} 6,000 ~ ${getCurrencySign()} 6,999`, key: 7 },
];

export const D_SERVICE_AND_AMENITIES = [
  { name: strings.DogRun, id: 1 },
  { name: strings.HasPetMenu, id: 2 },
  { name: strings.CageFree, id: 3 },
  { name: strings.HasPickUpService, id: 4 },
  { name: strings.HasPetsGoodShop, id: 5 },
  { name: strings.PhotographService, id: 6 },
  { name: strings.ProvideWater, id: 7 },
  { name: strings.others, id: 8 },
];

export const D_TRANSPORTATION_PREFERENCE = [
  {transport_mode_id:1,price_start_from:'0',status: 0},
  {transport_mode_id:2,price_start_from:'0',status: 0},
  {transport_mode_id:3,price_start_from:'0',status: 0},
  {transport_mode_id:4,price_start_from:'0',status: 0},
]
