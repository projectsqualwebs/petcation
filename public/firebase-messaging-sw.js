// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/9.9.2/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.9.2/firebase-messaging-compat.js');

// Initialize the Firebase app in the service worker by passing the generated config
var firebaseConfig = {
    apiKey: "AIzaSyCKAyRaeFk7asxtPjYTZc5qP873bve_9rc",
    authDomain: "petcation-35703.firebaseapp.com",
    databaseURL: "https://petcation-35703-default-rtdb.firebaseio.com",
    projectId: "petcation-35703",
    storageBucket: "petcation-35703.appspot.com",
    messagingSenderId: "577097513066",
    appId: "1:577097513066:web:926ce1367eefb2bab97213",
    measurementId: "G-7D6YNGBY4P"
};

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
    console.log('Received background message ', payload);

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body,
    };

    self.registration.showNotification(notificationTitle,
        notificationOptions);
});