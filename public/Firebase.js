import { initializeApp } from 'firebase/app';
import { getMessaging, getToken, onMessage } from "firebase/messaging";

const firebaseConfig = {
    apiKey: "AIzaSyCKAyRaeFk7asxtPjYTZc5qP873bve_9rc",
    authDomain: "petcation-35703.firebaseapp.com",
    databaseURL: "https://petcation-35703-default-rtdb.firebaseio.com",
    projectId: "petcation-35703",
    storageBucket: "petcation-35703.appspot.com",
    messagingSenderId: "577097513066",
    appId: "1:577097513066:web:926ce1367eefb2bab97213",
    measurementId: "G-7D6YNGBY4P"
};

const firebaseApp = initializeApp(firebaseConfig);

export const fetchToken = async () => {
    const messaging = await getMessaging(firebaseApp);
    return getToken(messaging, {vapidKey: 'BMtJYFzt5mstBmksC7jetVI9fukoDcYfcYOZNr7ehDPUN4d4nZBUReh3nkscwSpsbUQNluyH0f7f88MUKPfytCk'}).then((currentToken) => {
        if (currentToken) {
            console.log('current token', currentToken);
            document.cookie = `fcm_token=${currentToken}; path=/`;
            // Track the token -> client mapping, by sending to backend server
            // show on the UI that permission is secured
        } else {
            console.log('No registration token available. Request permission to generate one.');
            // shows on the UI that permission is required
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        // catch error while creating client token
    });
};

export const onMessageListener = () =>
    new Promise((resolve) => {
        const messaging = getMessaging(firebaseApp);
        onMessage(messaging, (payload) => {
            resolve(payload);
        });
    });