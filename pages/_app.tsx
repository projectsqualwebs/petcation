import Layout from "../components/layout/Layout";
import "bootstrap/dist/css/bootstrap.css";
import "../public/css/style.css";
import "../public/css/media.css";
import "../styles/globals.css";
import { Provider } from "react-redux";
import store from "../redux/store";
import  { NextPage } from "next";
import  { AppProps } from "next/app";
import React, { ReactElement, ReactNode } from "react";
import SnackbarProvider from "react-simple-snackbar";
import AppContext from "../utils/AppContext";
import { useState, useEffect } from "react";
import {useRouter} from "next/router";
import {fetchToken, onMessageListener} from '../public/Firebase';
import {ToastContainer, toast} from "react-toastify";
import Head from "next/head";


type NextPageWithLayout = NextPage & {
    getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
    Component: NextPageWithLayout;
};

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
    const getLayout = Component.getLayout ?? ((page) => page);
    const [contextData, setContextData] = useState<any>();
    const [gmapLoaded, setGmapLoaded] = useState<boolean>(false);
    const router = useRouter();


    const handleClickPushNotification = (response:any) => {
        const notificationTitle = response.notification.title.toLowerCase();
        const data = response.data?.data  ? JSON.parse(response.data?.data) : {};
        if(notificationTitle.includes("message")) {
            router.push(`/chat?threadId=${data?.chat_thread_id}`)
        } else if (notificationTitle.includes("booking")) {
            router.push(`/user/reservation?index=1`)
        } else if (notificationTitle.includes("dispute")) {
            router.push(`/user/reservation?index=1&activeTab=5`)
        } else {
            return null
        }
    };

    const handlePushNotifications = (event) =>{
        console.log("event for the service worker", event);
        getCustomMessage(event?.data)
    };

    const getCustomMessage = (payload:any) => {
        console.log("received payload====>", payload);
        if (payload.notification?.title) {
            const notificationTitle = payload.notification.title.toLowerCase();
            if(notificationTitle.includes("message") && router.pathname.includes("chat")) {
                return null
            } else {
                toast(<div onClick={() => handleClickPushNotification(payload)}>
                        <h5>{payload?.notification?.title}</h5>
                        <h6>{payload?.notification?.body}</h6>
                    </div>,
                    {
                        closeOnClick: false
                    }
                );
            }
        }
    };

    /*useEffect(()=>{
        fetchToken().then(r => {
            onMessageListener().then((payload:any) => {
               if(payload) {
                   getCustomMessage(payload);
               }
            }).catch(err => console.log('failed: ', err));
        }).catch(err => console.log('failed: ', err));
    },[]);*/

    useEffect(() => {
        fetchToken();
        // Event listener that listens for the push notification event in the background
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker.addEventListener("message", handlePushNotifications);
        }

        return () => {
            // Remove the event listener when the component unmounts
            if ("serviceWorker" in navigator) {
                navigator.serviceWorker.removeEventListener("message", handlePushNotifications);
            }
        };
    },[]);


    return (
     <>
         <Head>
             <script src='/tawk.js'/>
             <meta name="theme-color" content="#FFFFFF"/>
             <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
             <meta name="HandheldFriendly" content="true" />
         </Head>
         <AppContext.Provider
                value={{
                    state: contextData,
                    setState: setContextData,
                }}
            >
                <Provider store={store}>
                    <SnackbarProvider>
                        <Layout>{getLayout(<Component {...pageProps} />)}</Layout>
                    </SnackbarProvider>
                </Provider>
            </AppContext.Provider>
         <ToastContainer />
        </>
    );
}

export default MyApp;
