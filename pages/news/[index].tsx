import React, {useEffect, useState} from 'react';
import API from '../../api/Api';
import {useRouter} from "next/router";
import {useSnackbar} from 'react-simple-snackbar';
import Link from "next/link";
import {successOptions} from "../../public/appData/AppData";
import {strings} from "../../public/lang/Strings";
import {DropdownButton, Dropdown} from "react-bootstrap";

const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;
import Carousel from 'react-bootstrap/Carousel';
import RelatedStoriesObject from "../../components/news/RelatedStoriesObject";
import moment from "moment";


const api = new API();

interface IMAGES {
    id: number;
    news_room_blog_id: number;
    path: string;
}

interface SINGLE_NEWS_ROOM {
    banner_image: string;
    category: {
        id: number;
        name: string;
    }
    category_id: number;
    created_at: number;
    description: string;
    facebook: string;
    id: number;
    images: IMAGES[];
    other: string;
    title: string;
    twitter: string;
    updated_at: string;
    youtube: string;
}

const SingleNews = () => {
    const [newsData, setNewsData] = useState<SINGLE_NEWS_ROOM>({
        category: {
            id: null,
            name: ''
        },
        category_id: null,
        created_at: null,
        banner_image: null,
        description: '',
        facebook: '',
        id: null,
        images: [],
        other: '',
        title: '',
        twitter: '',
        updated_at: '',
        youtube: '',
    });
    const [myDescription, setMyDescription] = useState<string[]>([])
    const [copied, setCopied] = useState(false);
    const [openSnackbar] = useSnackbar();
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const router = useRouter();
    const [lang, setLang] = useState('en')
    const [carouselIndex, setIndex] = useState(0);
    const [relatedStories, setRelatedStories] = useState([]);


    useEffect(() => {
        let myLang = localStorage.getItem('language');
        if (myLang) {
            setLang(myLang)
        }
    }, []);

    useEffect(() => {
        const id = router.query.index;
        const getSingleNewsRoom = (id) => {
            api.getSingleNewsRoom(id)
                .then((response) => {
                    seLanguageContent(response.data.response, lang).then(r => {
                    });
                })
                .catch((error) => console.log(error));
        };
        getSingleNewsRoom(id);
        getRelatedStories()
    }, [router.query]);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };
    const getRelatedStories = () => {
        api.getLandingPageNewsRoom()
            .then((res) => {
                setRelatedStories(res.data.response?.data)
            })
            .catch((error) => console.log(error))
    };
    const seLanguageContent = async (myData, myLang) => {
        if (myLang == 'en' && myData) {
            let engData = {
                id: myData?.id,
                title: myData?.title_en,
                created_at: myData?.created_at,
                description: myData?.description_en,
                banner_image: myData?.banner_image,
                category: {
                    id: myData?.category?.id,
                    name: myData?.category?.name ?? myData?.category?.name_en,
                },
                category_id: myData?.category_id,
                facebook: myData?.facebook,
                images: myData?.images,
                other: myData?.other,
                twitter: myData?.twitter,
                updated_at: myData?.updated_at,
                youtube: myData?.youtube,
            };
            await setNewsData(engData)

        }
        if (myLang == 'jp' && myData) {
            let jpData = {
                id: myData?.id,
                title: myData?.title_jp,
                created_at: myData?.created_at,
                banner_image: myData?.banner_image,
                description: myData?.description_jp,
                category: {
                    id: myData?.category?.id,
                    name: myData?.category?.name ?? myData?.category?.name_jp,
                },
                category_id: myData?.category_id,
                facebook: myData?.facebook,
                images: myData?.images,
                other: myData?.other,
                twitter: myData?.twitter,
                updated_at: myData?.updated_at,
                youtube: myData?.youtube,
            }
            await setNewsData(jpData);
        }
        if (myLang == 'zh' && myData) {
            let zhData = {
                id: myData?.id,
                title: myData?.title_zh,
                banner_image: myData?.banner_image,
                created_at: myData?.created_at,
                description: myData?.description_zh,
                category: {
                    id: myData?.category?.id,
                    name: myData?.category?.name ?? myData?.category?.name_zh,
                },
                category_id: myData?.category_id,
                facebook: myData?.facebook,
                images: myData?.images,
                other: myData?.other,
                twitter: myData?.twitter,
                updated_at: myData?.updated_at,
                youtube: myData?.youtube,
            }
            await setNewsData(zhData);
        }
    }

    const copyUrl = (e) => {
        e.preventDefault();
        const el = document.createElement("input");
        el.value = getLocation();
        document.body.appendChild(el);
        el.select();
        document.execCommand("copy");
        document.body.removeChild(el);
        openSuccess(strings.CopiedToClipboard)
    };

    const getLocation = () => {
        if (typeof window !== "undefined") {
            return window.location.href
        }
    };

    console.log(newsData)

    return (
        newsData && <div className="main-wrapper mt-0">
            <div className="booking-sitter py-2">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                            <div className="single-news single-service">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            {" "}
                                            <Link href="/">
                                                {strings.Home}
                                            </Link>
                                        </li>
                                        {/*<li className="breadcrumb-item">*/}
                                        {/*    {" "}*/}
                                        {/*    <a href="/static/NewsEvents">*/}
                                        {/*        News*/}
                                        {/*    </a>*/}
                                        {/*</li>*/}
                                        {newsData && <li className="breadcrumb-item">
                                            {" "}
                                            <Link href={"/static/NewsRoom"}>
                                                {strings.News}
                                            </Link>
                                        </li>}

                                        {newsData && <li className="breadcrumb-item active" aria-current="page">
                                            {" "}
                                            {newsData?.title}

                                        </li>}

                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="padding news-single">
                <div className="container">
                    <div className="col-12 filter-design bg-white p-4 rounded-15">
                        <div className="row align-items-center">
                            <div className="col-12 col-md-6">
                                <div className="blog-img single-img m-0">
                                    {newsData?.images?.length ?
                                        <img src={ newsData?.images ? (IMAGE_BASE_URL + (newsData?.images[0]?.path)) : "/images/dog1.jpg"} className="img-fluid"
                                             alt=""/> : null}
                                </div>
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="col-12">
                                    <div className="blog-day mb-2">
                                        <h6 className="blog-date font-weight-medium text-muted small">Updated: {moment(newsData?.created_at).format("DD-MM-YYYY")}</h6>
                                    </div>
                                    <div className="blog-content mb-5">
                                        <h2 className="font-semibold">
                                            {newsData?.title}{" "}
                                        </h2>
                                    </div>
                                    <div className="follow-us blog-follow ml-0">
                                        <div className="follow-details ">
                                            <ul className="list-group list-group-horizontal">
                                                <li>
                                                    <a href={newsData?.other}>
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="far"
                                                            data-icon="basketball-ball"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 496 512"
                                                            className="svg-inline--fa fa-basketball-ball fa-w-16 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M248 8C111 8 0 118.9 0 256c0 137.9 111.6 248 248 248 136.2 0 248-110 248-248C496 119 385.2 8 248 8zm-13.9 447.3c-38.9-2.7-77.1-16.7-109.4-42L248 290l43 43c-29.2 35.1-48.9 77.4-56.9 122.3zm91.5-87.7l45.7 45.7c-26.1 20.5-56.1 33.6-87.2 39.3 7.3-31.1 21.6-59.9 41.5-85zm34-33.9c25-20 53.9-34.2 85.1-41.5-5.8 31.9-19.2 61.7-39.4 87.3l-45.7-45.8zm87.7-91.6c-45 8.1-87.2 27.8-122.4 57l-43-43 123.3-123.4c24.8 31.4 39.4 69.2 42.1 109.4zM139 181c-25.8 20.6-55.8 35-88.1 42.1 5.5-33 19-63.9 39.8-90.4L139 181zm-14.3-82.3C151.1 77.9 182 64.4 215 58.9c-7.1 32.3-21.5 62.3-42.1 88.1l-48.2-48.3zm140.2-41.9c39.1 3.3 75.8 17.8 106.4 41.9L248 222.1l-40.4-40.4c29.7-35.8 49.6-78.9 57.3-124.9zM48.8 273c46-7.8 89.1-27.6 124.8-57.3l40.4 40.4L90.7 379.4C66.6 348.7 52.1 312 48.8 273z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href={newsData?.facebook}>
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fab"
                                                            data-icon="facebook"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512"
                                                            className="svg-inline--fa fa-facebook fa-w-16 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href={newsData?.youtube}>
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fab"
                                                            data-icon="youtube"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 576 512"
                                                            className="svg-inline--fa fa-youtube fa-w-18 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href={newsData?.twitter}>
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fab"
                                                            data-icon="twitter"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512"
                                                            className="svg-inline--fa fa-twitter fa-w-16 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li>
                                                    <div className="col-auto user-profile-icon pl-2">
                                                        <div className="d-flex profile-share-icon">
                                                            <div className="profile-share-drop">
                                                                <DropdownButton
                                                                    className='bg-transparent'
                                                                    align="end"
                                                                    title={<svg
                                                                        aria-hidden="true"
                                                                        focusable="false"
                                                                        data-prefix="fas"
                                                                        data-icon="share"
                                                                        role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 512 512"
                                                                        className="svg-inline--fa fa-share fa-w-16 fa-2x"
                                                                    >
                                                                        <path
                                                                            fill="currentColor"
                                                                            d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                                                                            className="">
                                                                        </path>
                                                                    </svg>}
                                                                    id="dropdown-menu-align-end"
                                                                >
                                                                    <Dropdown.Item
                                                                        href={`mailto:?subject=${newsData && newsData?.title + ' spot'}&body=${encodeURIComponent(getLocation()) || ''}`}>
                                                                        <a>
                                                                            <img
                                                                                src="/images/social-img4.png"/> {strings.viaEmail}
                                                                        </a>
                                                                    </Dropdown.Item>
                                                                    <Dropdown.Item
                                                                        href={`https://api.whatsapp.com/send?text=${newsData && newsData?.title + ' spot:\n' + (getLocation())}`}
                                                                        data-action="share/whatsapp/share">
                                                                        <a>
                                                                            <img
                                                                                src="/images/social-img3.png"/> {strings.viaWhatsapp}
                                                                        </a>
                                                                    </Dropdown.Item>
                                                                    {/*<Dropdown.Item href={`fb-messenger://share/?link=${getLocation()}&app_id=1497423670682445`}>*/}
                                                                    {/*    <a>*/}
                                                                    {/*        {" "}*/}
                                                                    {/*        <img src="/images/social-img1.png" /> {strings.viaMessenger}*/}
                                                                    {/*    </a>*/}
                                                                    {/*</Dropdown.Item>*/}
                                                                    <Dropdown.Item onClick={copyUrl}>
                                                                        <a>
                                                                            {" "}
                                                                            <img src="/images/social-img1.png"/>
                                                                            {!copied ? strings.CopyLink : strings.Copied}
                                                                        </a>
                                                                    </Dropdown.Item>
                                                                </DropdownButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12 col-md-10 my-5 mx-auto">
                                <div className="single-blog-content">
                                    <h6 dangerouslySetInnerHTML={{__html: newsData?.description}}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    {newsData?.images?.length ? <div className="col-12 mx-0 mb-md-5 pb-md-4 col-md-10 mx-auto">
                        <div>
                            <div className="col-12 bd-example mx-0 p-0 w-100">
                                {newsData?.images?.length ?
                                    <div className="row justify-content-center align-items-center">
                                        {newsData?.images?.map((item, index) =>
                                            <div className="col-md-6 mt-4">
                                            <img
                                                className="d-block w-100"
                                                height={250}
                                                width={500}
                                                src={IMAGE_BASE_URL + item?.path ?? "/images/dog1.jpg"}
                                                alt={"slide " + (index + 1)}
                                            />
                                        </div>)}
                                    </div> : null}
                            </div>
                        </div>
                    </div> : null}
                    {/*<div className="row">*/}
                    {/*    <div className="col-12 col-md-9 col-lg-9 col-xl-9 mx-auto">*/}
                    {/*        <div className="single-blog-content mt-3">*/}
                    {/*            {myDescription.slice(3,myDescription.length).map((item, index)=><div dangerouslySetInnerHTML={{ __html: item+"</p>"}} />)}*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    <hr/>
                    <div className="related-stories my-5">
                        <h3 className="font-semibold mb-3">{strings.RelatedStories}</h3>
                        <div className="row">
                            {relatedStories && relatedStories.slice(0, 2).map((data, index) => <RelatedStoriesObject
                                data={data}
                                key={index}
                            />)}
                            <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                                <div className="events-details newsletter-content">
                                    <div className="newsletter-details">
                                        <h6>{strings.SignUpForOurNewsletter}</h6>
                                        <p className="mb-3 font-14">
                                            {strings.EnterYourEmailToReceiveTheLatestNewsAndUpdatesFromPetcationNews}
                                        </p>
                                        <button className="btn btn-primary border-btn px-3">
                                            {strings.Subscribe}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*------------------categories section--------------------*/}
        </div>
    )
}

export default SingleNews;
