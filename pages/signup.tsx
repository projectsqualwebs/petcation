import React from "react";
import { strings } from "../public/lang/Strings";
import "react-day-picker/lib/style.css";
import DayPickerInput from "react-day-picker/DayPickerInput";
import parseDate from "react-day-picker/moment";
import formatDate from "react-day-picker/moment";
import Link from "next/link";
import API from "../api/Api";
import Cookies from "universal-cookie";
import { withSnackbar, useSnackbar } from "react-simple-snackbar";
import Router from "next/router";
import YearMonthForm from "../components/common/YearMonthForm";
import moment from "moment";
import FeatherIcon from "feather-icons-react";
import GoogleLoginComponent from "../components/authentication/GoogleLogin";
import ReactTwitterLogin from "../components/authentication/TwiiterLogin";
import LoginFacebook from "../components/authentication/FacebookLogin";
import { EventEmitter } from "../public/EventEmitter";
import { errorOptions } from "../public/appData/AppData";
import { numberInput } from "../utils/Helper";
import Loader from "../components/common/Loader";
import ReCAPTCHA from "react-google-recaptcha";
import axios from "axios";
const secretKey = process.env.NEXT_PUBLIC_RECAPTCHA_SECRET_KEY;
const siteKey = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;

interface IState {
  dateOfBirth: Date;
  firstName: string;
  lastName: string;
  email: string;
  mobile_number: number;
  password: string;
  confirmPassword: string;
  condition: boolean;
  errors: any;
  month: Date;
  showConfirm: boolean;
  showPass: boolean;
  rememberMe: boolean;
  loader: boolean;
  is_captcha_verified: boolean;
}

const api = new API();
const cookies = new Cookies();

class Signup1 extends React.Component<any, IState> {
  private passwordInput: React.RefObject<HTMLInputElement>;
  private confirmInput: React.RefObject<HTMLInputElement>;
  private recaptchaRef: React.RefObject<any>;
  constructor(props) {
    super(props);
    this.state = {
      dateOfBirth: null,
      firstName: "",
      lastName: "",
      email: "",
      mobile_number: null,
      password: "",
      confirmPassword: "",
      condition: false,
      errors: {},
      month: new Date(new Date().getFullYear() - 13, 1),
      rememberMe: false,
      showConfirm: false,
      showPass: false,
      loader: false,
      is_captcha_verified: false,
    };
    this.passwordInput = React.createRef();
    this.recaptchaRef = React.createRef();
    this.confirmInput = React.createRef();
    this.handleDateChange = this.handleDateChange.bind(this);
    this.onTextChange = this.onTextChange.bind(this);
    this.checkboxOnChange = this.checkboxOnChange.bind(this);
    this.checkboxRememberOnChange = this.checkboxRememberOnChange.bind(this);
  }

  componentDidMount(): void {}

  handleDateChange(day) {
    this.setState({ errors: {}, dateOfBirth: day });
  }

  onTextChange = (event) => {
    // let errors = this.state.errors;
    // errors.confirmPassword = '';
    this.setState({ errors: {} });
    this.setState({
      [event.target.name]: event.target.value,
    } as { [K in keyof IState]: IState[K] });
  };

  checkboxOnChange = (event) => {
    this.setState({ errors: {} });
    this.setState({
      condition: event.target.checked,
    });
  };

  checkboxRememberOnChange = (event) => {
    this.setState({
      rememberMe: !this.state.rememberMe,
    });
  };

  registerUser = async (e: React.FormEvent<HTMLInputElement>) => {
    this.setState({ loader: true });
    if (!this.state.firstName) {
      let errors = this.state.errors;
      errors.firstName = strings.EnterFirstName;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    if (!this.state.lastName) {
      let errors = this.state.errors;
      errors.lastName = strings.EnterLastName;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    if (!this.state.email) {
      let errors = this.state.errors;
      errors.email = strings.EnterYourEmail;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    if (!this.state.mobile_number) {
      let errors = this.state.errors;
      errors.mobile_number = strings.EnterMobileNumber;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    if (!this.state.dateOfBirth) {
      let errors = this.state.errors;
      errors.dateOfBirth = strings.SelectDateOfBirth;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    if (!this.state.password) {
      let errors = this.state.errors;
      errors.password = strings.EnterPassword;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    if (!this.state.confirmPassword) {
      let errors = this.state.errors;
      errors.confirmPassword = strings.EnterConfirmPassword;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    if (this.state.confirmPassword !== this.state.password) {
      let errors = this.state.errors;
      errors.confirmPassword = strings.ConfirmPasswordDoesNotMatch;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }

    if (!this.state.condition) {
      let errors = this.state.errors;
      errors.condition = "*" + strings.AgreeToTermsAndCondition;
      this.setState({ errors: errors, loader: false });
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }

    if (!this.state.is_captcha_verified) {
      this.setState({
        errors: { captcha: "Captcha not verified" },
        loader: false,
      });
      return;
    }
    e.preventDefault();
    let that = this;
    let arr = [];
    this.setState({
      errors: {},
    });

    let data = JSON.stringify({
      firstname: this.state.firstName,
      lastname: this.state.lastName,
      email: this.state.email,
      mobile_number: this.state.mobile_number,
      dob: this.state.dateOfBirth
        ? moment(this.state.dateOfBirth).format("DD/MM/yyyy")
        : null,
      password: this.state.password,
      is_agree: this.state.condition,
      type: Router.query.google_id
        ? 2
        : Router.query.facebook_id
        ? 1
        : Router.query.twitter_id
        ? 3
        : null,
      google_id: Router.query.google_id ? Router.query.google_id : null,
      facebook_id: Router.query.facebook_id ? Router.query.facebook_id : null,
      twitter_id: Router.query.twitter_id ? Router.query.twitter_id : null,
      referral_token: Router.query.referral ? Router.query.referral : null,
      token: await cookies.get("fcm_token"),
      platform: "web",
    });

    api
      .registerUser(data)
      .then(async (json) => {
        if (json.data.response?.email_verified_at == null) {
          await Router.push(
            {
              pathname: "/verify",
              query: {
                userData: JSON.stringify(json.data.response),
                type: 1,
                signup: Router.query.signup,
              },
            },
            "/verify"
          );
          this.setState({ loader: false });
        } else {
          document.cookie = `id=${json.data.response.id}; path=/`;
          document.cookie = `token=${json.data.response.token}; path=/`;
          document.cookie = `firstname=${json.data.response.firstname}; path=/`;
          document.cookie = `lastname=${json.data.response.lastname}; path=/`;
          document.cookie = `email=${json.data.response.email}; path=/`;
          document.cookie = `profile_picture=${json.data.response.profile_picture}; path=/`;
          document.cookie = `phone_number=${json.data.response.phone_number}; path=/`;
          document.cookie = `get_recent_view=${arr}; path=/`;
          EventEmitter.dispatch("updateUserDetail");
          if (this.state.rememberMe) {
            document.cookie = `rem_email=${this.state.email}; path=/`;
            document.cookie = `rem_pass=${this.state.password}; path=/`;
          }
          Router.query.signup == "sitter"
            ? await Router.push("/user/my-profile")
            : await Router.push("/search-sitter");
          this.setState({ loader: false });
        }
      })
      .catch((error) => {
        let errors = this.state.errors;
        if (error.response.status == 422) {
          if (error.response.data?.errors?.email) {
            errors.email = error.response.data?.errors?.email;
          }
          if (error.response.data?.errors?.mobile_number) {
            errors.mobile_number = error.response.data?.errors?.mobile_number;
          }
          if (error.response.data?.errors?.firstname) {
            errors.firstName = error.response.data?.errors?.firstname;
          }
          if (error.response.data?.errors?.lastname) {
            errors.lastName = error.response.data?.errors?.lastname;
          }
          if (error.response.data?.errors?.dob) {
            errors.dateOfBirth = error.response.data?.errors?.dob;
          }
          if (error.response.data?.errors?.password) {
            errors.password = error.response.data?.errors?.password;
          }
          if (error.response.data?.errors?.is_agree) {
            errors.condition = error.response.data?.errors?.is_agree;
          }
          if (error.response.data?.errors?.type) {
            errors.other = error.response.data?.errors?.type;
          }
          if (error.response.data?.errors?.google_id) {
            errors.other = error.response.data?.errors?.google_id;
          }
          if (error.response.data?.errors?.facebook_id) {
            errors.other = error.response.data?.errors?.facebook_id;
          }
          if (error.response.data?.errors?.twitter_id) {
            errors.other = error.response.data?.errors?.twitter_id;
          }
          if (error.response.data?.errors?.referral_token) {
            errors.other = error.response.data?.errors?.referral_token;
          } else {
            errors.other = error.response.data.message;
          }
          that.setState({
            errors: errors,
            loader: false,
          });
        }

        this.setState({ loader: false });
        this.recaptchaRef?.current?.reset();
        this.setState({ is_captcha_verified: false });
      });
  };

  handleYearMonthChange = (month) => {
    this.setState({
      month: month,
    });
  };

  toggleSecurePassword = () => {
    this.setState({ showPass: !this.state.showPass });
  };

  toggleConfirmPassword = () => {
    this.setState({ showConfirm: !this.state.showConfirm });
  };

  onSuccess = (res) => {
    alert(`${strings.LoggedInSuccessfullyWelcome + " " + res.profileObj.name}`);
  };

  onFailure = (res) => {
    console.log("Login failed: res:", res);
  };

  onReCAPTCHAChange = (token) => {

    api.verifyRecaptcha(token)
        .then((response) => {
          if (response.data.success) {
            this.setState({ is_captcha_verified: true });
          } else {
            this.setState({ is_captcha_verified: false });
          }
        })
        .catch((error) => {
          this.setState({ is_captcha_verified: false });
          this.recaptchaRef?.current?.reset();
        });
  };

  render() {
    return (
      <div className="vector">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-md-8 col-lg-6 col-xl-6">
              <div className="bg-white main-content">
                <div className="login-details">
                  <div className="logo-img mb-2">
                    <img src="images/logo.png" className="img-fluid" />
                  </div>
                  <p className="font-14">
                    {strings.Createyourfreeaccounttoday}
                  </p>
                </div>
                <div className="login-form">
                  <div className="my-4">
                    <div className="row">
                      <LoginFacebook />
                      <GoogleLoginComponent />
                      <div className="col-12">
                        <ReactTwitterLogin />
                      </div>
                    </div>
                  </div>
                  <div className="loginOr mb-4">
                    <hr className="hrOr" />
                    <span className="spanOr">{strings.or}</span>
                  </div>
                  <form>
                    <div className="form-row">
                      <div className="form-group col-sm-6">
                        <div className="form-label-view">
                          <label>
                            {strings.Firstname}{" "}
                            <span className="text-danger">*</span>
                          </label>
                        </div>
                        <input
                          className={
                            "form-control " +
                            (this.state.errors?.firstName ? "invalid" : "")
                          }
                          id="fname"
                          type="text"
                          name="firstName"
                          placeholder={strings.EnterFirstName}
                          onChange={this.onTextChange}
                        />
                        {this.state.errors?.firstName && (
                          <label className={"error-text"}>
                            {this.state.errors?.firstName}
                          </label>
                        )}
                      </div>
                      <div className="form-group col-sm-6">
                        <div className="form-label-view">
                          <label>
                            {strings.Lastname}{" "}
                            <span className="text-danger">*</span>
                          </label>
                        </div>
                        <input
                          className={
                            "form-control " +
                            (this.state.errors?.lastName ? "invalid" : "")
                          }
                          id="lname"
                          type="text"
                          name="lastName"
                          placeholder={strings.EnterLastName}
                          onChange={this.onTextChange}
                        />
                        {this.state.errors?.lastName && (
                          <label className={"error-text"}>
                            {this.state.errors?.lastName}
                          </label>
                        )}
                      </div>
                      <div className="form-group col-sm-12">
                        <div className="form-label-view">
                          <label>
                            {strings.Emailaddress}{" "}
                            <span className="text-danger">*</span>
                          </label>
                        </div>
                        <input
                          className={
                            "form-control " +
                            (this.state.errors?.email ? "invalid" : "")
                          }
                          id="email"
                          type="email"
                          name="email"
                          placeholder={strings.EnterEmailAddress}
                          onChange={this.onTextChange}
                        />
                        {this.state.errors?.email && (
                          <label className={"error-text"}>
                            {this.state.errors?.email}
                          </label>
                        )}
                      </div>
                      <div className="form-group col-sm-6">
                        <div className="form-label-view">
                          <label>
                            {strings.MobileNumber}{" "}
                            <span className="text-danger">*</span>
                          </label>
                        </div>
                        <input
                          className={
                            "form-control " +
                            (this.state.errors?.mobile_number ? "invalid" : "")
                          }
                          id="mobile_number"
                          type="text"
                          maxLength={10}
                          name="mobile_number"
                          placeholder={strings.EnterMobileNumber}
                          onChange={this.onTextChange}
                          onKeyPress={numberInput}
                        />
                        {this.state.errors?.mobile_number && (
                          <label className={"error-text"}>
                            {this.state.errors?.mobile_number}
                          </label>
                        )}
                      </div>
                      <div className="form-group col-sm-6">
                        <div className="form-label-view">
                          <label>
                            {strings.DateofBirth}{" "}
                            <span className="text-danger">*</span>
                          </label>
                        </div>
                        <div
                          className={
                            "form-control valid-control " +
                            (this.state.errors?.dateOfBirth ? "invalid" : "")
                          }>
                          <DayPickerInput
                            formatDate={formatDate.formatDate}
                            parseDate={parseDate.parseDate}
                            dayPickerProps={{
                              modifiers: {
                                disabled: [
                                  {
                                    after: new Date(),
                                  },
                                ],
                              },
                              month: this.state.month,
                              captionElement: ({ date, localeUtils }) => (
                                <YearMonthForm
                                  isDob={true}
                                  before={true}
                                  date={date ? date : null}
                                  localeUtils={localeUtils}
                                  onChange={this.handleYearMonthChange.bind(
                                    this
                                  )}
                                />
                              ),
                            }}
                            inputProps={{
                              style: {
                                border: 0,
                                background: "transparent",
                              },
                              readOnly: true,
                            }}
                            placeholder="DD/MM/YYYY"
                            format="DD/MM/yyyy"
                            value={this.state.dateOfBirth}
                            onDayChange={this.handleDateChange}
                          />
                        </div>
                        {this.state.errors?.dateOfBirth && (
                          <label className={"error-text"}>
                            {this.state.errors?.dateOfBirth}
                          </label>
                        )}
                      </div>
                      <div className="form-group col-sm-6">
                        <label>{strings.Password}</label>
                        <input
                          ref={this.passwordInput}
                          className={
                            "form-control " +
                            (this.state.errors?.password ? "invalid" : "")
                          }
                          id="password"
                          placeholder={strings.EnterPassword}
                          type={this.state.showPass ? "text" : "password"}
                          name="password"
                          onChange={this.onTextChange}
                        />
                        <FeatherIcon
                          className="svg-inline--fa fa-eye fa-w-18 fa-2x eye-icon"
                          icon={this.state.showPass ? "eye-off" : "eye"}
                          onClick={this.toggleSecurePassword}
                        />
                        {this.state.errors?.password && (
                          <label className={"error-text"}>
                            {this.state.errors?.password}
                          </label>
                        )}
                      </div>
                      <div className="form-group col-sm-6">
                        <label>{strings.ConfirmPassword}</label>
                        <input
                          ref={this.confirmInput}
                          className={
                            "form-control " +
                            (this.state.errors?.confirmPassword
                              ? "invalid"
                              : "")
                          }
                          id="confirm-password"
                          placeholder={strings.EnterConfirmPassword}
                          type={this.state.showConfirm ? "text" : "password"}
                          name="confirmPassword"
                          onChange={this.onTextChange}
                        />
                        <FeatherIcon
                          className="svg-inline--fa fa-eye fa-w-18 fa-2x eye-icon"
                          icon={this.state.showConfirm ? "eye-off" : "eye"}
                          onClick={this.toggleConfirmPassword}
                        />
                        {this.state.errors?.confirmPassword && (
                          <label className={"error-text"}>
                            {this.state.errors?.confirmPassword}
                          </label>
                        )}
                      </div>
                      <div className="col-12 col-md-6 col-lg-6 col-xl-6 col-sm-6 mb-3 px-0">
                        <div className="custom-check">
                          <label className="check ">
                            <input
                              onChange={this.checkboxRememberOnChange}
                              type="checkbox"
                              name="is_name1"
                            />
                            <span className="checkmark" />
                            {strings.Rememberme}
                          </label>
                        </div>
                      </div>

                      <div className="custom-check">
                        <label className="check ">
                          <input
                            onChange={this.checkboxOnChange}
                            type="checkbox"
                            checked={this.state.condition}
                          />
                          <span className="checkmark" />
                          {strings.IAgreeTo + ` `}
                          <label className="create-account">
                            <Link href={"/static/Terms"}>
                              <a target="_blank" className="font-medium">
                                {strings.TermsofService}
                              </a>
                            </Link>
                          </label>{" "}
                          {" " + strings.and + " "}{" "}
                          <label className="create-account">
                            <Link href={"/static/PrivacyPolicy"}>
                              <a target="_blank" className="font-medium">
                                {strings.privacyPolicy}
                              </a>
                            </Link>
                          </label>
                          {strings.registerTerm}
                        </label>
                        {this.state.errors?.condition ? (
                          <label className={"error-text"}>
                            {this.state.errors?.condition}
                          </label>
                        ) : null}
                      </div>
                    </div>
                    <div className="d-flex justify-content-center mt-3">
                      <ReCAPTCHA
                        ref={this.recaptchaRef}
                        sitekey={siteKey}
                        onChange={this.onReCAPTCHAChange}
                      />
                    </div>

                    {this.state.loader ? (
                      <Loader />
                    ) : (
                      <div className="text-center my-3">
                        {this.state.errors?.captcha ? (
                          <label className={"error-text"}>
                            *{this.state.errors.captcha}
                          </label>
                        ) : null}
                        {this.state.errors?.other ? (
                          <label className={"error-text"}>
                            {this.state.errors?.other}
                          </label>
                        ) : null}
                        <a
                          onClick={(val: any) => this.registerUser(val)}
                          className="btn btn-primary w-100">
                          {strings.Joinnow}
                        </a>
                      </div>
                    )}

                    <div className="text-center create-account">
                      <p className="font-12 mb-0 font-medium">
                        {strings.Alreadyhaveanaccount}
                        <Link href={"./signin"}>
                          <a className="font-medium">{strings.Login}</a>
                        </Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const Signup = () => {
  const [openError, closeError] = useSnackbar(errorOptions);
  return <Signup1 openError={(text) => openError(text)} />;
};

// export default withAuth(withSnackbar(Signup, FailureOptions));
export default Signup;
