import React from "react";
import Select from "react-select";
import "react-day-picker/lib/style.css";
import DayPickerInput from "react-day-picker/DayPickerInput";
import {
    durationsSearchOption,
    pet,
    petCount,
    petSize, petSizeSearchOption,
    prices,
    select,
    serviceData,
    ServiceTime,
    sitterSorting,
    sort,
} from "../public/appData/AppData";
import SitterObject from "../components/SearchSitter/SitterObject";
import {strings} from "../public/lang/Strings";
import API from "../api/Api";
import {AxiosResponse} from "axios";
import Res from "../models/response.interface";
import Loader from "../components/common/Loader";
import {I_SEARCH_SITTER} from "../models/searchSitter.interface";
import AppContext from "../utils/AppContext";
import moment from 'moment';
import "rc-slider/assets/index.css";
import MyMapComponent from "../components/common/MySearchMapComponent";
import Cookies from "universal-cookie";
import Link from 'next/link';
import LocationSearchInput2 from "../components/common/LocationSearchInput2";
import {Marker} from "react-google-maps";
import OutsideClickHandler from "../utils/OutsideClickHandler";

interface LAT_LNG {
    lat: string;
    lng: string;
}

interface IState {
    checkInDate: Date;
    checkOutDate: Date;
    pet: select;
    duration : select;
    service: select;
    serviceTime: select;
    petSize: select;
    petCount: select;
    price: select;
    medical_service_id: select;
    grooming_service_id: select;
    medical_services: select[];
    grooming_services: select[];
    sitters: I_SEARCH_SITTER[];
    minPrice: number;
    maxPrice: number;
    loading: boolean;
    allPrice: boolean;
    lat: string;
    lng: string;
    formatted_address: string;
    locality: string;
    postal_code: string;
    city: string;
    defaultValue: string;
    transportation: 1 | 0;
    closest_sitter: select;
    drag_map: 1 | 0;
    has_house: 1 | 0;
    has_fenced_yard: 1 | 0;
    dogs_allowed_on_furniture: 1 | 0;
    dogs_allowed_on_bed: 1 | 0;
    non_smoking_home: 1 | 0;
    does_not_own_a_dog: 1 | 0;
    does_not_own_a_cat: 1 | 0;
    does_not_own_caged_pets: 1 | 0;
    accepts_only_one_client_at_a_time: 1 | 0;
    has_no_children: 1 | 0;
    no_children_0_5_years_old: 1 | 0;
    no_children_6_12_years_old: 1 | 0;
    sort_by_rating: 1 | 0;
    sort_by_review: 1 | 0;
    latlng: any[],
    user_id: number;
    serviceData: any[];
    saveSearch: boolean;
    ne_lat: string;
    sw_lat: string;
    ne_lng: string;
    sw_lng: string;
    defaultCenter?: any;
    currentHoveringSitter: I_SEARCH_SITTER;
    mapZoom: number;
    showFilter: boolean;
    wishlist: any;
    myLatLng: any;
    show_map: boolean;
    showMobileFilter: boolean;
}

const api = new API();
const Slider = require("rc-slider");
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const cookies = new Cookies();


export default class SearchSitter extends React.Component<{}, IState> {
    static contextType = AppContext;
    latlng: any;

    constructor(props) {
        super(props);
        this.state = {
            sitters: [],
            checkInDate: null,
            checkOutDate: null,
            duration : durationsSearchOption[0],
            pet: pet[0],
            service: serviceData[0],
            serviceTime: ServiceTime[0],
            petSize: petSize[0],
            petCount: petCount[0],
            price: prices[0],
            medical_service_id: {key: null, value: null, label: strings.All},
            grooming_service_id: {key: null, value: null, label: strings.All},
            medical_services: [{key: null, value: null, label: strings.All}],
            grooming_services: [{key: null, value: null, label: strings.All}],
            minPrice: 1,
            maxPrice: 5000,
            allPrice: true,
            loading: true,
            lat: "0",
            lng: "0",
            formatted_address: '',
            locality: '',
            postal_code: null,
            city: '',
            defaultValue: "",
            transportation: 0,
            closest_sitter: sitterSorting[0],
            drag_map: 0,
            has_house: 0,
            has_fenced_yard: 0,
            dogs_allowed_on_furniture: 0,
            dogs_allowed_on_bed: 0,
            non_smoking_home: 0,
            does_not_own_a_dog: 0,
            does_not_own_a_cat: 0,
            does_not_own_caged_pets: 0,
            accepts_only_one_client_at_a_time: 0,
            has_no_children: 0,
            no_children_0_5_years_old: 0,
            no_children_6_12_years_old: 0,
            sort_by_rating: 0,
            sort_by_review: 0,
            latlng: [],
            user_id: 0,
            ne_lat: null,
            sw_lat: null,
            ne_lng: null,
            sw_lng: null,
            serviceData: null,
            saveSearch: false,
            currentHoveringSitter: null,
            defaultCenter: null,
            mapZoom: 10,
            showFilter: false,
            wishlist: [],
            myLatLng: {},
            show_map: false,
            showMobileFilter: false
        };
        this.handlePetChange = this.handlePetChange.bind(this);
        this.handleServiceTimeChange = this.handleServiceTimeChange.bind(this);
        this.handlePetCountChange = this.handlePetCountChange.bind(this);
        this.handlePetSizeChange = this.handlePetSizeChange.bind(this);
        this.handleMedicalServiceChange = this.handleMedicalServiceChange.bind(this);
        this.handleGroomingServiceChange = this.handleGroomingServiceChange.bind(this);
        this.handleSortingChange = this.handleSortingChange.bind(this);
        this.handleServiceChange = this.handleServiceChange.bind(this);
        this.handleCheckoutClick = this.handleCheckoutClick.bind(this);
        this.handleCheckInDayClick = this.handleCheckInDayClick.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.resetFilter = this.resetFilter.bind(this);
        this.handleDurationChange = this.handleDurationChange.bind(this);
        // this.getFilteredAvailableSitter = this.getFilteredAvailableSitter.bind(this);
    }

    async componentDidMount() {
        this.getAllWishlist();
        let searchData = JSON.parse(localStorage.getItem('search-data'));
        if (localStorage.getItem('search-data')) {
            this.setState({
                    pet: searchData.pet,
                    duration :searchData?.duration ? searchData.duration : null,
                    service: searchData.service,
                    serviceTime: searchData.serviceTime,
                    petSize: searchData.petSize,
                    petCount: searchData.petCount,
                    price: searchData.price,
                    checkInDate: searchData.checkInDate ? new Date(searchData.checkInDate) : null,
                    checkOutDate: searchData.checkOutDate ? new Date(searchData.checkOutDate) : null,
                    minPrice: searchData.minPrice,
                    maxPrice: searchData.maxPrice,
                    lat: searchData.lat,
                    lng: searchData.lng,
                    defaultCenter: {
                        lat: parseFloat(searchData.lat),
                        lng: parseFloat(searchData.lng),
                    },
                    formatted_address: searchData.formatted_address,
                    locality: searchData.locality,
                    postal_code: searchData.postal_code,
                    city: searchData.city,
                    transportation: searchData?.transportation,
                    closest_sitter: searchData?.closest_sitter ?? 0,
                    has_house: searchData?.has_house ?? 0,
                    has_fenced_yard: searchData?.has_fenced_yard ?? 0,
                    dogs_allowed_on_furniture: searchData?.dogs_allowed_on_furniture ?? 0,
                    dogs_allowed_on_bed: searchData?.dogs_allowed_on_bed ?? 0,
                    non_smoking_home: searchData?.non_smoking_home ?? 0,
                    does_not_own_a_dog: searchData?.does_not_own_a_dog ?? 0,
                    does_not_own_a_cat: searchData?.does_not_own_a_cat ?? 0,
                    does_not_own_caged_pets: searchData?.does_not_own_caged_pets ?? 0,
                    accepts_only_one_client_at_a_time: searchData?.accepts_only_one_client_at_a_time ?? 0,
                    has_no_children: searchData?.has_no_children ?? 0,
                    sort_by_rating: searchData.sort_by_rating,
                    sort_by_review: searchData.sort_by_review,
                    no_children_0_5_years_old: searchData?.no_children_0_5_years_old ?? 0,
                    no_children_6_12_years_old: searchData?.no_children_6_12_years_old ?? 0,
                    defaultValue: searchData?.defaultValue
                },
                () => {
                    let staticData = JSON.parse(JSON.stringify(serviceData));
                    if (searchData.pet.value != '1') {
                        staticData.splice(4, 1)
                    }
                    if (searchData.pet.value === "3" || searchData.pet.value === "4") {
                        staticData.splice(4, 1);
                    }
                    this.setState({
                        serviceData: staticData,
                    });
                    if (searchData?.service?.value == 7) {
                        this.getHouseCallServices(Number(this.state.pet.value))
                    }
                    if (searchData?.service?.value == 6) {
                        this.getGroomingServices(Number(this.state.pet.value))
                    }
                    console.log('local storage called');
                    this.getFilteredAvailableSitter();

                })
        } else {
            this.setState({
                pet: pet[0],
                duration: durationsSearchOption[0],
                service: serviceData[0],
                serviceTime: ServiceTime[0],
                petSize: petSize[0],
                petCount: petCount[0],
                price: prices[0],
                checkInDate: null,
                checkOutDate: null,
                minPrice: 0,
                maxPrice: 5000,
                lat: "0",
                lng: "0",
                ne_lat: null,
                sw_lat: null,
                ne_lng: null,
                sw_lng: null,
                formatted_address: '',
                locality: '',
                postal_code: null,
                city: '',
                transportation: 0,
                closest_sitter: sitterSorting[0],
                has_house: 0,
                has_fenced_yard: 0,
                dogs_allowed_on_furniture: 0,
                dogs_allowed_on_bed: 0,
                non_smoking_home: 0,
                does_not_own_a_dog: 0,
                does_not_own_a_cat: 0,
                does_not_own_caged_pets: 0,
                accepts_only_one_client_at_a_time: 0,
                has_no_children: 0,
                sort_by_rating: 0,
                sort_by_review: 0,
                no_children_0_5_years_old: 0,
                no_children_6_12_years_old: 0,
                defaultValue: ""
            })
        }

        let staticData = JSON.parse(JSON.stringify(serviceData));
        if (this.state.pet.value !== "1") {
            staticData.splice(4, 1);
        }
        if (this.state.pet.value === "3" || this.state.pet.value === "4") {
            staticData.splice(4, 1);
        }
        this.setState({
            serviceData: staticData
        });
        let id = cookies.get("id");
        if (id) {
            this.setState({user_id: id})
        }
        if (this.context.state && Object.keys(this.context.state).length !== 0) {
            const that = this.context.state;
            if (!that.petSize && !that.pet && !that.service && !that.checkInDate && !that.checkOutDate) {
                this.setState(
                    {
                        checkInDate: null,
                        checkOutDate: null,
                        sitters: [],
                        lat: String(this.context.state.lat),
                        lng: String(this.context.state.lng),
                        defaultCenter: {
                            lat: parseFloat(this.context.state.lat),
                            lng: parseFloat(this.context.state.lng),
                        },
                        formatted_address: String(this.context.state.formatted_address),
                        locality: String(this.context.state.locality),
                        postal_code: String(this.context.state.postal_code),
                        city: String(this.context.state.city),
                        defaultValue: this.context.state.defaultValue
                            ? this.context.state.defaultValue
                            : "",
                        transportation: this.context?.state?.transportation ?? 0,
                        closest_sitter: this.context.state?.closest_sitter ? this.context.state?.closest_sitter : sitterSorting[0],
                        has_house: this.context.state.has_house ? this.context.state.has_house : 0,
                        has_fenced_yard: this.context.state.has_fenced_yard ? this.context.state.has_fenced_yard : 0,
                        dogs_allowed_on_furniture: this.context.state.dogs_allowed_on_furniture ? this.context.state.dogs_allowed_on_furniture : 0,
                        dogs_allowed_on_bed: this.context.state.dogs_allowed_on_bed ? this.context.state.dogs_allowed_on_bed : 0,
                        non_smoking_home: this.context.state.non_smoking_home ? this.context.state.non_smoking_home : 0,
                        does_not_own_a_dog: this.context.state.does_not_own_a_dog ? this.context.state.does_not_own_a_dog : 0,
                        does_not_own_a_cat: this.context.state.does_not_own_a_cat ? this.context.state.does_not_own_a_cat : 0,
                        does_not_own_caged_pets: this.context.state.does_not_own_caged_pets ? this.context.state.does_not_own_caged_pets : 0,
                        accepts_only_one_client_at_a_time: this.context.state.accepts_only_one_client_at_a_time ? this.context.state.accepts_only_one_client_at_a_time : 0,
                        has_no_children: this.context.state.has_no_children ? this.context.state.has_no_children : 0,
                        sort_by_rating: this.context.state.sort_by_rating ? this.context.state.sort_by_rating : 0,
                        sort_by_review: this.context.state.sort_by_review ? this.context.state.sort_by_review : 0,
                        no_children_0_5_years_old: this.context.state.no_children_0_5_years_old ? this.context.state.no_children_0_5_years_old : 0,
                        no_children_6_12_years_old: this.context.state.no_children_6_12_years_old ? this.context.state.no_children_6_12_years_old : 0,
                        minPrice: this.context.state.minPrice ? this.context.state.minPrice : 0,
                        maxPrice: this.context.state.maxPrice ? this.context.state.maxPrice : 5000,
                    },
                    () => {
                        this.context.setState({});
                        console.log('context called');
                        if (this.state?.service?.value == 7) {
                            this.getHouseCallServices(Number(this.state.pet.value))
                        }
                        if (this.state?.service?.value == 6) {
                            this.getGroomingServices(Number(this.state.pet.value))
                        }
                        this.getFilteredAvailableSitter();
                    }
                );
            } else {
                this.setState(
                    {
                        lat: String(this.context.state.lat),
                        lng: String(this.context.state.lng),
                        defaultCenter: {
                            lat: parseFloat(this.context.state.lat),
                            lng: parseFloat(this.context.state.lng),
                        },
                        formatted_address: String(this.context.state.formatted_address),
                        locality: String(this.context.state.locality),
                        postal_code: String(this.context.state.postal_code),
                        city: String(this.context.state.city),
                        pet: pet.find((element) => element.value == this.context.state.pet),
                        service: serviceData.find(
                            (element) => element.value == this.context.state.service
                        ),
                        defaultValue: this.context.state.defaultValue
                            ? this.context.state.defaultValue
                            : "",
                        petSize: this.context.state.petSize
                            ? this.context.state.petSize
                            : petSize[0],
                        medical_service_id: this.context?.state?.medical_service_id ? this.context?.state?.medical_service_id : {key: null, value: null, label: strings.All},
                        grooming_service_id: this.context?.state?.grooming_service_id ? this.context?.state?.grooming_service_id : {key: null, value: null, label: strings.All},
                        checkInDate: this.context.state.checkInDate ?? null,
                        checkOutDate: this.context.state.checkOutDate ?? null,
                        transportation: this.context?.state?.transportation ?? 0,
                        closest_sitter: this.context.state?.closest_sitter ? this.context.state?.closest_sitter : sitterSorting[0],
                        has_house: this.context.state.has_house ? this.context.state.has_house : 0,
                        has_fenced_yard: this.context.state.has_fenced_yard ? this.context.state.has_fenced_yard : 0,
                        dogs_allowed_on_furniture: this.context.state.dogs_allowed_on_furniture ? this.context.state.dogs_allowed_on_furniture : 0,
                        dogs_allowed_on_bed: this.context.state.dogs_allowed_on_bed ? this.context.state.dogs_allowed_on_bed : 0,
                        non_smoking_home: this.context.state.non_smoking_home ? this.context.state.non_smoking_home : 0,
                        does_not_own_a_dog: this.context.state.does_not_own_a_dog ? this.context.state.does_not_own_a_dog : 0,
                        does_not_own_a_cat: this.context.state.does_not_own_a_cat ? this.context.state.does_not_own_a_cat : 0,
                        does_not_own_caged_pets: this.context.state.does_not_own_caged_pets ? this.context.state.does_not_own_caged_pets : 0,
                        accepts_only_one_client_at_a_time: this.context.state.accepts_only_one_client_at_a_time ? this.context.state.accepts_only_one_client_at_a_time : 0,
                        has_no_children: this.context.state.has_no_children ? this.context.state.has_no_children : 0,
                        sort_by_rating: this.context.state.sort_by_rating ? this.context.state.sort_by_rating : 0,
                        sort_by_review: this.context.state.sort_by_review ? this.context.state.sort_by_review : 0,
                        no_children_0_5_years_old: this.context.state.no_children_0_5_years_old ? this.context.state.no_children_0_5_years_old : 0,
                        no_children_6_12_years_old: this.context.state.no_children_6_12_years_old ? this.context.state.no_children_6_12_years_old : 0,
                        minPrice: this.context.state.minPrice ? this.context.state.minPrice : 0,
                        maxPrice: this.context.state.maxPrice ? this.context.state.maxPrice : 5000,
                    },
                    () => {
                        this.context.setState({});
                        console.log('called form home')
                        if (this.state?.service?.value == 7) {
                            this.getHouseCallServices(Number(this.state.pet.value))
                        }
                        if (this.state?.service?.value == 6) {
                            this.getGroomingServices(Number(this.state.pet.value))
                        }
                        this.getFilteredAvailableSitter();
                    }
                );
            }

        } else {
            if (!localStorage.getItem('search-data')) {
                console.log('direct called')
                this.getFilteredAvailableSitter();
            }

        }

        setTimeout(() => {
            this.setState({show_map: true})
        }, 1000)
    };

    componentWillUpdate(prevState, nextState) {
        if (nextState.saveSearch === true) {
            localStorage.setItem('search-data', JSON.stringify(nextState));
        }
    };


    handleCheckInDayClick(day) {
        this.setState({
                checkInDate: day,
                checkOutDate: day,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handleCheckoutClick(day) {
        this.setState({checkOutDate: day},
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handleServiceChange(service: select) {
        const check = service.value != 1 && service.value != 2;
        if (service.value == 7) {
            this.getHouseCallServices(Number(this.state.pet.value))
        }
        if (service.value == 6) {
            this.getGroomingServices(Number(this.state.pet.value))
        }
        this.setState({
                service: service,
                checkOutDate: check ? this.state.checkInDate : this.state.checkOutDate,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handleSortingChange(closest_sitter: select) {
        this.setState({closest_sitter: closest_sitter},
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
        console.log({closest_sitter})
    }

    handleServiceTimeChange(petType: select) {
        this.setState({
                serviceTime: petType,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handlePetSizeChange(petSize: select) {
        this.setState({
                petSize: petSize,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handleMedicalServiceChange(service: select) {
        this.setState({
                medical_service_id: service,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handleDurationChange(duration: select) {
        this.setState({
                duration: duration,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }


    handleGroomingServiceChange(service: select) {
        this.setState({
                grooming_service_id: service,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handlePetCountChange(petCount: select) {

        this.setState({petCount: petCount},
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    handlePetChange(pet: select) {

        let staticData = JSON.parse(JSON.stringify(serviceData));
        if (pet.value !== "1") {
            staticData.splice(4, 1);
        }
        if (pet.value === "3" || pet.value === "4") {
            staticData.splice(4, 1);
        }
        this.setState({
                pet: pet,
                service: serviceData[0],
                serviceData: staticData,
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    getHouseCallServices = (pet_type) => {
        if (this.state.medical_services.length < 2) {
            api
                .getHouseCallService({
                    pet_type: pet_type,
                })
                .then(async (res) => {
                    let arr = [];
                    await res.data.response.map((val) => {
                        return arr.push({ key: val?.id, value: val?.id, label: val?.name })
                    });
                    await this.setState({
                        medical_services: [{key: null, value: null, label: strings.All}, ...arr],
                        medical_service_id: {key: null, value: null, label: strings.All}
                    });
                })
                .catch((error) => console.log(error));
        }
    };

    getGroomingServices = (pet_type) => {
        if (this.state.grooming_services.length < 2) {
            api
                .getGroomingService({
                    pet_type: pet_type,
                })
                .then(async (res) => {
                    let arr = [];
                    await res.data.response.map((val) => {
                        return arr.push({ key: val?.id, value: val?.id, label: val?.name })
                    })
                    await this.setState({
                        grooming_services: [{key: null, value: null, label: strings.All}, ...arr],
                        grooming_service_id: {key: null, value: null, label: strings.All}
                    });
                })
                .catch((error) => console.log(error));
        }
    };

    onTextChange<T extends keyof IState>(
        event: React.ChangeEvent<HTMLInputElement>
    ) {
        this.setState({[event.target.name]: event.target.value} as {
                [P in T]: IState[P];
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            });
    }

    customStyles = {
        option: (provided, state) => ({
            ...provided,
            fontSize: 14,
        }),

        control: (provided) => ({
            ...provided,
            width: 140,
        }),
        singleValue: (provided, state) => {
            return {...provided, fontSize: 12, color: "#383838", fontWeight: "500"};
        },
    };

    sortDropdownStyles = {
        option: (provided, state) => ({
            ...provided,
            fontSize: 14,
        }),

        control: (provided) => ({
            ...provided,
            width: 180,
        }),
        singleValue: (provided, state) => {
            return {...provided, fontSize: 12, color: "#383838", fontWeight: "500"};
        },
    };

    async getFilteredAvailableSitter() {
        if (this.state.showMobileFilter && window.innerWidth < 768) {
            return false
        }
        this.setState({
            loading: true,
            showMobileFilter: false
        });
        if (this.state.lat == "0" || this.state.lng == "0") {
            await navigator.permissions.query({ name: 'geolocation' })
                .then(async (val) => {if(val.state === 'denied' || val.state === 'prompt') 
                {
                    console.log('Please enable your location for better results')
                    this.setState({...this.state,  lat: null, lng: null }, () =>{
                        let data = {
                            pet_type: this.state.pet.value,
                            duration: this.state.duration?.value ? this.state.duration.value : null,
                            service_id: this.state.service.value,
                            availability_start: this.state.checkInDate,
                            availability_end: this.state.checkOutDate,
                            number_of_pets: this.state.petCount.value,
                            pet_size: this.state.petSize.value,
                            medical_service_id: this.state.medical_service_id.value,
                            grooming_service_id: this.state.grooming_service_id.value,
                            for_all_prices: 0,
                            transportation: this.state.transportation,
                            sort: this.state.closest_sitter.value,
                            price: {
                                max: this.state.maxPrice,
                                min: this.state.minPrice,
                            },
                            address: this.state.ne_lat ? null : {
                                latitude: this.state.lat,
                                longitude: this.state.lng,
                                city: this.state?.city ? this.state?.city : "",
                                formatted_address: this.state.formatted_address,
                            },
                            locality: {
                                dog_other_pets: null,
                                non_smoking_household: this.state?.non_smoking_home,
                                no_children_present: this.state?.has_no_children,
                                fenced_yard: this.state?.has_fenced_yard,
                                dogs_on_furniture: this.state?.dogs_allowed_on_furniture,
                                dogs_on_bed: this.state?.dogs_allowed_on_bed,
                                does_not_own_a_dog: this.state?.does_not_own_a_dog,
                                does_not_own_a_cat: this.state?.does_not_own_a_cat,
                                does_not_own_caged_pets: this.state?.does_not_own_caged_pets,
                                accept_only_on_client_at_time: this.state?.accepts_only_one_client_at_a_time,
                                no_children_5_year_old: this.state?.no_children_0_5_years_old,
                                no_children_12_year_old: this.state?.no_children_6_12_years_old,
                                live_in_house: this.state?.has_house,
                            },
                            ne_lat: this.state.ne_lat,
                            sw_lat: this.state.sw_lat,
                            ne_lng: this.state.ne_lng,
                            sw_lng: this.state.sw_lng
                        };
                        let that = this;
                        api
                            .getFilteredAvialableSitter(data)
                            .then(async (response: AxiosResponse<Res<I_SEARCH_SITTER[]>>) => {
                                let data = response.data.response.filter(val => val.id != this.state.user_id)
                                let address = await data.map((val) => {
                                    return {lat: Number(val.address?.map_latitude), lng: Number(val.address?.map_longitude)}
                                });
                                this.state.ne_lat ? this.setState({
                                        defaultCenter: null,
                                        sitters: data,
                                        loading: false,
                                        latlng: address,
                                        ne_lat: null,
                                        sw_lat: null,
                                        ne_lng: null,
                                        sw_lng: null,
                                        showMobileFilter: false
                                    }) :
                                    this.setState({
                                        defaultCenter: await address?.length ? address[0] : {lat: this.state.lat, lng: this.state.lng},
                                        mapZoom: 10,
                                        sitters: data,
                                        loading: false,
                                        latlng: address,
                                        ne_lat: null,
                                        sw_lat: null,
                                        ne_lng: null,
                                        sw_lng: null,
                                        showMobileFilter: false
                                    });

                            })
                            .catch((error) => {
                                console.log("error",error);
                            });
                    })
                } else {
                    await navigator.geolocation.getCurrentPosition(
                        (position) => {
                            let data = {
                                pet_type: this.state.pet.value,
                                duration: this.state.duration.value,
                                service_id: this.state.service.value,
                                availability_start: this.state.checkInDate,
                                availability_end: this.state.checkOutDate,
                                number_of_pets: this.state.petCount.value,
                                pet_size: this.state.petSize.value,
                                medical_service_id: this.state.medical_service_id.value,
                                grooming_service_id: this.state.grooming_service_id.value,
                                for_all_prices: 0,
                                transportation: this.state.transportation,
                                sort: this.state.closest_sitter.value,
                                price: {
                                    max: this.state.maxPrice,
                                    min: this.state.minPrice,
                                },
                                address: this.state.ne_lat ? null : {
                                    latitude: String(position.coords.latitude),
                                    longitude: String(position.coords.longitude),
                                    city: this.state?.city ? this.state?.city : "",
                                    formatted_address: this.state.formatted_address,
                                },
                                locality: {
                                    dog_other_pets: null,
                                    non_smoking_household: this.state?.non_smoking_home,
                                    no_children_present: this.state?.has_no_children,
                                    fenced_yard: this.state?.has_fenced_yard,
                                    dogs_on_furniture: this.state?.dogs_allowed_on_furniture,
                                    dogs_on_bed: this.state?.dogs_allowed_on_bed,
                                    does_not_own_a_dog: this.state?.does_not_own_a_dog,
                                    does_not_own_a_cat: this.state?.does_not_own_a_cat,
                                    does_not_own_caged_pets: this.state?.does_not_own_caged_pets,
                                    accept_only_on_client_at_time: this.state?.accepts_only_one_client_at_a_time,
                                    no_children_5_year_old: this.state?.no_children_0_5_years_old,
                                    no_children_12_year_old: this.state?.no_children_6_12_years_old,
                                    live_in_house: this.state?.has_house,
                                },
                                ne_lat: this.state.ne_lat,
                                sw_lat: this.state.sw_lat,
                                ne_lng: this.state.ne_lng,
                                sw_lng: this.state.sw_lng
                            };
                            let that = this;
                            api
                                .getFilteredAvialableSitter(data)
                                .then(async (response: AxiosResponse<Res<I_SEARCH_SITTER[]>>) => {
                                    let data = response.data.response.filter(val => val.id != this.state.user_id)
                                    console.log({data})
                                    let address = await data.map((val) => {
                                        return {lat: Number(val.address?.map_latitude), lng: Number(val.address?.map_longitude)}
                                    })
                                    await console.log('address[0]',address[0])
                                    this.state.ne_lat ? this.setState({
                                            defaultCenter: null,
                                            sitters: data,
                                            loading: false,
                                            latlng: address,
                                            ne_lat: null,
                                            sw_lat: null,
                                            ne_lng: null,
                                            sw_lng: null,
                                            showMobileFilter: false
                                        }) :
                                        this.setState({
                                            defaultCenter: await address?.length ? address[0] : {lat: position.coords.latitude, lng: position.coords.longitude},
                                            mapZoom: 10,
                                            sitters: data,
                                            loading: false,
                                            latlng: address,
                                            ne_lat: null,
                                            sw_lat: null,
                                            ne_lng: null,
                                            sw_lng: null,
                                            showMobileFilter: false
                                        });

                                })
                                .catch((error) => {
                                    console.log("error",error);
                                });
                            if (typeof window !== 'undefined') {
                                //localStorage.setItem('search-data', JSON.stringify(this.state));
                                //console.log(JSON.stringify(this.state));
                            }
                        },
                        function (error) {
                            return ("Error Code = " + error.code + " - " + error.message);
                        }
                    );
                }
                })
                .catch(() => console.log('error'))
        } else {
            let data = {
                pet_type: this.state.pet.value,
                duration: this.state.duration?.value,
                service_id: this.state.service.value,
                availability_start: this.state.checkInDate,
                availability_end: this.state.checkOutDate,
                number_of_pets: this.state.petCount.value,
                pet_size: this.state.petSize.value,
                medical_service_id: this.state.medical_service_id.value,
                grooming_service_id: this.state.grooming_service_id.value,
                for_all_prices: 0,
                transportation: this.state.transportation,
                sort: this.state.closest_sitter.value,
                price: {
                    max: this.state.maxPrice,
                    min: this.state.minPrice,
                },
                address: this.state.ne_lat ? null : {
                    latitude: this.state.lat,
                    longitude: this.state.lng,
                    city: this.state?.city ? this.state?.city : "",
                    formatted_address: this.state.formatted_address,
                },
                locality: {
                    dog_other_pets: null,
                    non_smoking_household: this.state?.non_smoking_home,
                    no_children_present: this.state?.has_no_children,
                    fenced_yard: this.state?.has_fenced_yard,
                    dogs_on_furniture: this.state?.dogs_allowed_on_furniture,
                    dogs_on_bed: this.state?.dogs_allowed_on_bed,
                    does_not_own_a_dog: this.state?.does_not_own_a_dog,
                    does_not_own_a_cat: this.state?.does_not_own_a_cat,
                    does_not_own_caged_pets: this.state?.does_not_own_caged_pets,
                    accept_only_on_client_at_time: this.state?.accepts_only_one_client_at_a_time,
                    no_children_5_year_old: this.state?.no_children_0_5_years_old,
                    no_children_12_year_old: this.state?.no_children_6_12_years_old,
                    live_in_house: this.state?.has_house,
                },
                ne_lat: this.state.ne_lat,
                sw_lat: this.state.sw_lat,
                ne_lng: this.state.ne_lng,
                sw_lng: this.state.sw_lng
            };
            let that = this;
            api
                .getFilteredAvialableSitter(data)
                .then(async (response: AxiosResponse<Res<I_SEARCH_SITTER[]>>) => {
                    let data = response.data.response.filter(val => val.id != this.state.user_id);
                    
                    let address = await data.map((val) => {
                        return {lat: Number(val.address.map_latitude), lng: Number(val.address.map_longitude)}
                    })
                    
                    this.state.ne_lat ? this.setState({
                            defaultCenter: null,
                            sitters: data,
                            loading: false,
                            latlng: address,
                            ne_lat: null,
                            sw_lat: null,
                            ne_lng: null,
                            sw_lng: null,
                            showMobileFilter: false
                        }) :
                        this.setState({
                            defaultCenter: await address[0],
                            mapZoom: 10,
                            sitters: data,
                            loading: false,
                            latlng: address,
                            ne_lat: null,
                            sw_lat: null,
                            ne_lng: null,
                            sw_lng: null,
                            showMobileFilter: false
                        });

                })
                .catch((error) => {
                    console.log(error);
                });
            if (typeof window !== 'undefined') {
                //localStorage.setItem('search-data', JSON.stringify(this.state));
                //console.log(JSON.stringify(this.state));
            }
        }

    }


    resetFilter() {
        this.setState({
                sitters: [],
                checkInDate: null,
                checkOutDate: null,
                minPrice: 0,
                maxPrice: 5000,
                allPrice: true,
                lat: "0",
                lng: "0",
                ne_lat: null,
                sw_lat: null,
                ne_lng: null,
                sw_lng: null,
                formatted_address: '',
                medical_service_id: {key: null, value: null, label: "All"},
                grooming_service_id: {key: null, value: null, label: "All"},
                locality: '',
                postal_code: '',
                city: '',
                defaultValue: "",
                transportation: 0,
                has_house: 0,
                has_fenced_yard: 0,
                dogs_allowed_on_furniture: 0,
                dogs_allowed_on_bed: 0,
                non_smoking_home: 0,
                does_not_own_a_dog: 0,
                does_not_own_a_cat: 0,
                does_not_own_caged_pets: 0,
                accepts_only_one_client_at_a_time: 0,
                has_no_children: 0,
                no_children_0_5_years_old: 0,
                no_children_6_12_years_old: 0,
                sort_by_rating: 0,
                sort_by_review: 0,
                latlng: [],
                pet: pet[0],
                duration: durationsSearchOption[0],
                service: serviceData[0],
                serviceTime: ServiceTime[0],
                petSize: petSize[0],
                petCount: petCount[0],
                price: prices[0],
                serviceData: serviceData

            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            })
    }

    //This method will get executed once the map is scrolled
    handlingMapOperation = (ne: any, sw: any) => {
        console.log({
            ne_lat: String(ne.lat()),
            ne_lng: String(ne.lng()),
            sw_lat: String(sw.lat()),
            sw_lng: String(sw.lng())
        })
        this.setState({
                ne_lat: ne.lat(),
                ne_lng: ne.lng(),
                sw_lat: sw.lat(),
                sw_lng: sw.lng()
            },
            () => {
                this.context.setState({});
                this.getFilteredAvailableSitter();
            })
    }

    /**
     * Wishlist api's
     */
    getAllWishlist = () => {
        let token = cookies.get('token');
        if (token) {
            api
                .getSitterLists()
                .then(async (res) => {
                    await this.setState({wishlist: res.data.response})
                })
                .catch(err => console.log(err.response.data.message))
        }
    };


    render() {
        return (
            <div className="main-wrapper search-page">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 mob-filter-tab">
                            <div className="row">
                                <div className="col">
                                    <div className="d-flex align-items-center justify-content-between col-12 form-group mb-0" onClick={() => this.setState({showMobileFilter: true})}>
                                        <label className="form-label show show1">
                                            Filter
                                        </label>
                                        <svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" className="css-tj5bde-Svg">
                                            <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div className="col drop">
                                    <div className="d-flex align-items-center justify-content-between col-12 form-group mb-0">
                                        <Select
                                            value={this.state.closest_sitter}
                                            isSearchable={false}
                                            onChange={this.handleSortingChange}
                                            options={sitterSorting}
                                            // menuIsOpen={true}
                                            styles={this.customStyles}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-8">
                            <div className={`filter-section ${this.state.showMobileFilter ? "show" : ''}`}>
                                <div className="col-12 filter-design">
                                    <div className="row ">
                                        <div className="col-12 f-head d-flex d-md-none align-items-center justify-content-between p-1">
                                            <h4>Filters</h4>
                                            <svg onClick={() => this.setState({showMobileFilter: false})} viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                        <div className="col-12">
                                            <div className="row">
                                                <div className="col-6 col-sm-auto col-md form-group mb-0">
                                                    <label>{strings.petType}</label>
                                                    <Select
                                                        onChange={this.handlePetChange}
                                                        value={this.state.pet}
                                                        isSearchable={false}
                                                        options={pet}
                                                        styles={this.customStyles}
                                                    />
                                                </div>

                                                <div className="col-6 col-sm-auto col-md form-group mb-0">
                                                    <label>{strings.serviceType}</label>
                                                    <Select
                                                        value={this.state.service}
                                                        isSearchable={false}
                                                        onChange={this.handleServiceChange}
                                                        options={this.state.serviceData}
                                                        styles={this.customStyles}
                                                    />
                                                </div>

                                                {this.state.service.value == 6 && this.state.grooming_services &&
                                                <div className="col-6 col-sm-auto col-md-auto form-group mb-0">
                                                    <label>{strings.GroomingService}</label>
                                                    <Select
                                                        onChange={this.handleGroomingServiceChange}
                                                        value={this.state.grooming_service_id}
                                                        isSearchable={false}
                                                        options={this.state.grooming_services}
                                                        isMulti={false}
                                                        styles={this.customStyles}
                                                    />
                                                </div>}
                                                {this.state.service.value == 7 && this.state.medical_services &&
                                                <div className="col-6 col-sm-auto col-md-auto form-group mb-0">
                                                    <label>{strings.MedicalService}</label>
                                                    <Select
                                                        onChange={this.handleMedicalServiceChange}
                                                        value={this.state.medical_service_id}
                                                        isSearchable={false}
                                                        options={this.state.medical_services}
                                                        isMulti={false}
                                                        styles={this.customStyles}
                                                    />
                                                </div>}

                                                {(this.state.service.value == 3 || this.state.service.value == 5 ) &&
                                                <div className="col-6 col-sm-auto col-md-auto form-group mb-0">
                                                    <label>{strings.Duration}</label>
                                                    <Select
                                                        onChange={this.handleDurationChange}
                                                        value={this.state.duration}
                                                        isSearchable={false}
                                                        options={durationsSearchOption}
                                                        isMulti={false}
                                                        styles={this.customStyles}
                                                    />
                                                </div>}

                                                <div className="col-6 col-sm-auto col-md form-group mb-0">
                                                    <label>{strings.StartDate}</label>
                                                    <div
                                                        className="input-group datepicker1 date"
                                                        data-date-format="mm-dd-yyyy"
                                                    >
                                                        <div className="form-control">
                                                            <DayPickerInput
                                                                dayPickerProps={{
                                                                    modifiers: {
                                                                        disabled: [
                                                                            {
                                                                                before: new Date(),
                                                                            },
                                                                        ],
                                                                    },
                                                                }}
                                                                inputProps={{
                                                                    style: {
                                                                        border: 0,
                                                                        background: "transparent",
                                                                    },
                                                                    readOnly: true,
                                                                }}
                                                                placeholder="DD/MM/YYYY"
                                                                format="DD/MM/YYYY"
                                                                value={this.state.checkInDate ? JSON.parse(moment(new Date(this.state.checkInDate)).format('"MM-DD-YYYY"')) : ''}
                                                                onDayChange={this.handleCheckInDayClick}
                                                            />
                                                        </div>
                                                        <span className="input-group-addon filter-point"></span>
                                                    </div>
                                                </div>
                                                {this.state.service.value == 1 ||
                                                this.state.service.value == 2 ? (
                                                    <div className="col-6 col-sm-auto col-md form-group mb-0">
                                                        <label>{strings.EndDate}</label>
                                                        <div
                                                            className="input-group datepicker1 date"
                                                            data-date-format="mm-dd-yyyy"
                                                        >
                                                            <div className="form-control ">
                                                                <DayPickerInput
                                                                    dayPickerProps={{
                                                                        modifiers: {
                                                                            disabled: [
                                                                                {
                                                                                    before: this.state.checkInDate,
                                                                                },
                                                                            ],
                                                                        },
                                                                    }}
                                                                    inputProps={{
                                                                        style: {border: 0, background: "transparent"},
                                                                        readOnly: true,
                                                                        disabled:
                                                                            this.state.service.value != 1 &&
                                                                            this.state.service.value != 2,
                                                                    }}
                                                                    placeholder="DD/MM/YYYY"
                                                                    format="DD/MM/YYYY"
                                                                    value={this.state.checkOutDate ? JSON.parse(moment(new Date(this.state.checkOutDate)).format('"MM-DD-YYYY"')) : ''}
                                                                    onDayChange={this.handleCheckoutClick}
                                                                />
                                                            </div>
                                                            <span className="input-group-addon filter-point"></span>
                                                        </div>
                                                    </div>
                                                ) : null}
                                                {this.state.service.value != 6 && this.state.service.value != 7 && this.state.pet.value != 5 && this.state.pet.value != 4 &&
                                                <div className="col-6 col-sm-auto col-md-auto form-group mb-0">
                                                    <label>{strings.PetSize}</label>
                                                    <Select
                                                        onChange={this.handlePetSizeChange}
                                                        value={this.state.petSize}
                                                        isSearchable={false}
                                                        options={petSizeSearchOption}
                                                        isMulti={false}
                                                        styles={this.customStyles}
                                                    />
                                                </div>}
                                                {this.state.service.value != 6 && this.state.service.value != 7 &&
                                                <div className="col-6 col-sm-auto col-md-auto form-group mb-0">
                                                    <label>{strings.Howmanypets}</label>
                                                    <Select
                                                        value={this.state.petCount}
                                                        onChange={this.handlePetCountChange}
                                                        isSearchable={false}
                                                        options={petCount}
                                                        styles={
                                                            (this.customStyles,
                                                                {
                                                                    control: (provided) => ({
                                                                        ...provided,
                                                                        width: 80,
                                                                    }),
                                                                })
                                                        }
                                                    />
                                                </div>
                                                }
                                                {/*more start*/}
                                                <div className="col-12 col-sm-auto col-md-auto form-group mb-0">
                                                    <label>{strings.More}</label>
                                                    <div className="dropdown class1 cursor-pointer"
                                                         onClick={() => this.setState({showFilter: true})}>
                                                        <a className="dropdown-toggle point">
                                                            {strings.More}
                                                        </a>
                                                        <OutsideClickHandler
                                                            onOutsideClick={() => {
                                                                if (this.state.showFilter === true) {
                                                                    this.setState({showFilter: false})
                                                                }
                                                            }}
                                                        >
                                                            <div
                                                                className={`dropdown-menu ${this.state?.showFilter ? 'show' : ''}`}>
                                                                <p className="mb-0 filter-title">{strings.HousingConditions}</p>
                                                                <div className="filter-dropdown-items pb-0">
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            has_house:
                                                                                                this.state.has_house == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.has_house == 1}
                                                                                type="checkbox"
                                                                                name="more3"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.HasHouseExcludesApartments}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            has_fenced_yard:
                                                                                                this.state.has_fenced_yard == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.has_fenced_yard == 1}
                                                                                type="checkbox"
                                                                                name="more4"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.HasFencedYard}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            dogs_allowed_on_furniture:
                                                                                                this.state.dogs_allowed_on_furniture == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.dogs_allowed_on_furniture == 1}
                                                                                type="checkbox"
                                                                                name="more5"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.DogsAllowedOnFurniture}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            dogs_allowed_on_bed:
                                                                                                this.state.dogs_allowed_on_bed == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.dogs_allowed_on_bed == 1}
                                                                                type="checkbox"
                                                                                name="more6"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.DogsAllowedOnBed}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            non_smoking_home:
                                                                                                this.state.non_smoking_home == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.non_smoking_home == 1}
                                                                                type="checkbox"
                                                                                name="more6"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.NonSmokingHome}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <p className="mb-0 filter-title mt-2">{strings.PetsInTheHome}</p>
                                                                <div className="filter-dropdown-items pb-0">
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            does_not_own_a_dog:
                                                                                                this.state.does_not_own_a_dog == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.does_not_own_a_dog == 1}
                                                                                type="checkbox"
                                                                                name="more7"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.DoesnotOwnADog}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            does_not_own_a_cat:
                                                                                                this.state.does_not_own_a_cat == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.does_not_own_a_cat == 1}
                                                                                type="checkbox"
                                                                                name="more8"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.DoesnotOwnACat}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            accepts_only_one_client_at_a_time:
                                                                                                this.state.accepts_only_one_client_at_a_time == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.accepts_only_one_client_at_a_time == 1}
                                                                                type="checkbox"
                                                                                name="more9"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.AcceptsOnlyOneClientAtATime}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            does_not_own_caged_pets:
                                                                                                this.state.does_not_own_caged_pets == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.does_not_own_caged_pets == 1}
                                                                                type="checkbox"
                                                                                name="more9"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.DoesNotOwnCagedPets}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                {/*<a href="#" className="filter-more">*/}
                                                                {/*  + More*/}
                                                                {/*</a>*/}
                                                                <p className="mb-0 filter-title">{strings.ChildrenInTheHome}</p>
                                                                <div className="filter-dropdown-items pb-0">
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            has_no_children:
                                                                                                this.state.has_no_children == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.has_no_children == 1}
                                                                                type="checkbox"
                                                                                name="more10"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.HasNoChildren}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            no_children_0_5_years_old:
                                                                                                this.state.no_children_0_5_years_old == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.no_children_0_5_years_old == 1}
                                                                                type="checkbox"
                                                                                name="more11"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.NoChildren05YearsOld}
                                                                        </label>
                                                                    </div>
                                                                    <div className="custom-check mb-1">
                                                                        <label className="check ">
                                                                            <input
                                                                                onChange={() =>
                                                                                    this.setState({
                                                                                            no_children_6_12_years_old:
                                                                                                this.state.no_children_6_12_years_old == 1 ? 0 : 1,
                                                                                        },
                                                                                        () => {
                                                                                            this.context.setState({});
                                                                                            this.getFilteredAvailableSitter();
                                                                                        })
                                                                                }
                                                                                checked={this.state.no_children_6_12_years_old == 1}
                                                                                type="checkbox"
                                                                                name="more12"/>
                                                                            <span className="checkmark"/>
                                                                            {strings.NoChildren612YearsOld}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </OutsideClickHandler>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <div className="row search-sitter">
                                                <div className="col-12 col-sm-auto col-md-3 form-group  mb-0 ">
                                                    <div>
                                                        <label>{strings.Price}</label>
                                                    </div>
                                                    <div>
                                                        <small className="font-10 float-left">{strings.B_Min_B}</small>
                                                        <small className="font-10 float-right">{strings.B_Max_B}</small>
                                                    </div>

                                                    <div className="mt-3 mb-1">
                                                        <Range
                                                            step={25}
                                                            value={[this.state.minPrice, this.state.maxPrice]}
                                                            min={0}
                                                            max={5000}
                                                            onChange={(e) =>
                                                                this.setState({
                                                                        minPrice: e[0],
                                                                        maxPrice: e[1],
                                                                    },
                                                                    () => {
                                                                        this.context.setState({});
                                                                        this.getFilteredAvailableSitter();
                                                                    })
                                                            }
                                                        />
                                                    </div>
                                                    <div>
                                                        <small className="font-10 float-left">
                                                            {this.state.minPrice}
                                                        </small>
                                                        <small className="font-10 float-right">
                                                            {this.state.maxPrice}
                                                        </small>
                                                    </div>
                                                </div>

                                                <div className="col-12 col-sm-auto col-md form-group mb-0 location-search-input">
                                                    <label className=''>{strings.Address}</label>
                                                    {this.state && <LocationSearchInput2
                                                        key={'search_sitter'}
                                                        className={''}
                                                        value={this.state.defaultValue}
                                                        setFilter={(address) => {
                                                            this.setState({
                                                                    formatted_address: address.value,
                                                                    locality: address.locality,
                                                                    postal_code: address.postalCode,
                                                                    city: address.city,
                                                                    defaultValue: address.value,
                                                                },
                                                                () => {
                                                                    this.context.setState({});
                                                                    this.getFilteredAvailableSitter();
                                                                })
                                                        }
                                                        }
                                                        setLatLng={(latLng) => {
                                                            this.setState({
                                                                ne_lat: null,
                                                                sw_lat: null,
                                                                ne_lng: null,
                                                                sw_lng: null,
                                                                lat: String(latLng.lat),
                                                                lng: String(latLng.lng)
                                                            })
                                                        }
                                                        }
                                                        setValue={(address) => {
                                                            this.setState({defaultValue: address})
                                                            if (address == '') {
                                                                this.setState({
                                                                    formatted_address: '',
                                                                    locality: '',
                                                                    postal_code: '',
                                                                    city: '',
                                                                    lat: '0',
                                                                    lng: '0',
                                                                }, () => {
                                                                    this.getFilteredAvailableSitter();
                                                                })
                                                            }
                                                        }}
                                                    />}
                                                </div>

                                                <div className="col-12 col-md-auto pl-md-0 save-div">
                                                    <button
                                                        onClick={() => {
                                                            this.setState({saveSearch: true})
                                                        }}
                                                        className="btn btn-save mb-2 mb-md-0"
                                                    >
                                                        {strings.SaveSearch}
                                                    </button>
                                                </div>

                                                <div className="col-12 col-sm-auto col-md-auto form-group mb-0 my-auto  ">
                                                    <div className="custom-check">
                                                        <label className="check ">
                                                            <input
                                                                onChange={() =>
                                                                    this.setState({
                                                                            transportation:
                                                                                this.state.transportation == 1 ? 0 : 1,
                                                                        },
                                                                        () => {
                                                                            this.context.setState({});
                                                                            this.getFilteredAvailableSitter();
                                                                        })
                                                                }
                                                                checked={this.state.transportation == 1}
                                                                type="checkbox"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            {strings.shouldHaveTransportationAvailable}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-auto pl-md-0 save-div d-block d-md-none">
                                                    <button
                                                        onClick={() => {
                                                            this.setState({showMobileFilter: false}, () => this.getFilteredAvailableSitter())
                                                        }}
                                                        className="btn btn-primary mb-2 mb-md-0"
                                                    >
                                                        {strings.Apply}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="container-fluid">
                                <div className="sort-details py-2">
                                    <div className="row ">
                                        <div className="col-12 mt-2 mb-3 px-0">
                                            <div className="d-flex align-items-center justify-content-between">
                                                <div className="d-md-flex d-none  high-rating align-center ml-0 filter-design"
                                                >
                                                    <div
                                                        className="d-flex align-items-center col-6 col-sm-auto col-md form-group mb-0">
                                                        <label
                                                            className="form-label show show1">{strings.SortBy}</label>
                                                        <Select
                                                            value={this.state.closest_sitter}
                                                            isSearchable={false}
                                                            onChange={this.handleSortingChange}
                                                            options={sitterSorting}
                                                            styles={this.customStyles}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-md-auto col-12 p-0 align-center mt-2 mt-md-0">
                                                    <div className="font-12 mb-0 w-100 align-center d-flex align-items-center justify-content-between d-lg-block">
                                                        {this.state.sitters.length + " " + strings.resultsAsPerPreferences}
                                                        <Link href="">
                                                            <a className="border border-danger text-danger font-weight-bold rounded-pill py-1 px-2 ml-2"
                                                               onClick={this.resetFilter}>{strings.Reset}</a>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 px-2">
                                            <div className="row">
                                                {this.state.loading ? (
                                                    <Loader/>
                                                ) : (this.state.sitters.length > 0 ?
                                                        this.state.sitters.map((value, index) => (
                                                            <SitterObject
                                                                wishlist={this.state.wishlist}
                                                                getWishlist={() => this.getAllWishlist()}
                                                                onMouseOut={() => this.setState({currentHoveringSitter: null})}
                                                                onMouseOver={() => this.setState({currentHoveringSitter: value})}
                                                                key={index}
                                                                props={value}
                                                                petType={this.state.pet}
                                                                serviceId={this.state.service.value}
                                                                transportAvailable={this.state.transportation}
                                                                label={this.state.service.label}
                                                                getSitter={() => this.getFilteredAvailableSitter()}
                                                                groomingService={this.state.grooming_services}
                                                                medicalServices={this.state.medical_services}
                                                            />
                                                        )) : (<>
                                                            <div className="text-center padding">
                                                                <p className="font-13 mb-0 font-italic">{strings.noSitterFound}</p>
                                                            </div>
                                                        </>)
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-4 map-full d-md-block d-none">
                            <div className="mapouter h-100">
                                <div className="gmap_canvas h-100">
                                    {this.state.show_map ? <MyMapComponent
                                        serviceId={this.state.service.value}
                                        data={this.state.sitters}
                                        draggable={true}
                                        latlng={this.state.latlng ? this.state.latlng : ''}
                                        handlingMapOperation={(ne: any, sw: any) => this.handlingMapOperation(ne, sw)} //ne.lat(), ne.lng() will work to get lat lng
                                        currentHoveringSitter={this.state.currentHoveringSitter}
                                        mapZoom={this.state.mapZoom}
                                        mapCenter={this.state.defaultCenter ? {
                                            lat: parseFloat(this.state.defaultCenter?.lat),
                                            lng: parseFloat(this.state.defaultCenter?.lng)
                                        } : null}
                                        defaultCenter={null}
                                    /> : <Loader />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
