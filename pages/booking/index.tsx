import { useRouter } from "next/router";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import "react-datepicker/dist/react-datepicker.css";

import {
  errorOptions,
  gender,
  petSize,
  petType,
  select,
  serviceData,
  successOptions,
} from "../../public/appData/AppData";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import { AxiosResponse } from "axios";
import Res from "../../models/response.interface";
import I_REQUEST_DATA, {
  I_CALCULATED_AMOUNT,
} from "../../models/requestSitter.interface";
import I_PET from "../../models/pet.interface";
import { deepClone } from "../../utils/Helper";
import Select from "react-select";
import { useSnackbar } from "react-simple-snackbar";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import { I_PET_ADDITIONAL_SERVICE } from "../../models/boardingService.interface";
import { addDays, addMonths, format, isSameDay } from "date-fns";
// import Autocomplete from "react-google-autocomplete";
import I_BREED from "../../models/breed.interface";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import withAuth from "../../components/hoc/withAuth";
import moment from "moment";
import UploadFileModal from "../../components/common/UploadfileModal";
import { dataURLtoFile } from "../../utils/Helper";
import "rc-time-picker/assets/index.css";
import { getCurrencySign, GOOGLE_PLACES_API } from "../../api/Constants";
import {
  D_TRANSPORTATION,
  D_TRANSPORTATION_PREFERENCE,
} from "../../public/appData/StaticData";
import TimePickerInput from "../../components/common/TimePickerInput";
import LocationSearchInput2 from "../../components/common/LocationSearchInput2";
import OverlayLoader from "../../components/common/OverlayLoader";
import Loader from "../../components/common/Loader";

const api = new API();

interface requestData {
  pet_type: number;
  pet_size_id: number;
  pickup_address: {
    address1: string;
    latitude: string;
    longitude: string;
    address2: string;
    city: string;
    zip_code: string;
  };
  message: string;
  want_to_receive_media: 0 | 1;
}

interface requestAmountData {
  pets: number[];
  additional_services: number[];
  need_sitter_to_pickup: 0 | 1;
}

export const Booking: React.FC<{}> = () => {
  const router = useRouter();
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);

  const [service, setService] = useState<select>(null);
  const [sitterName, setSitterName] = useState<string | string[]>();
  const [sitterId, setSitterId] = useState<string | string[]>();
  const [sitterPets, setSitterPets] = useState<number[]>([]);
  const [myPets, setMyPets] = useState<I_PET[]>([]);
  const [breeds, setBreeds] = useState<I_BREED[]>([]);
  const [selectedBreed, setSelectedBreed] = useState<I_BREED>();
  const [selectedImage, setSelectedImage] = useState<string>();
  const [selectedGender, setSelectedGender] = useState<any>();
  const [value, setValue] = useState<string>("");
  const [isSelectCheckIn, setIsSelectCheckIn] = useState(true);

  const [showTransport, setShowTransport] = useState<boolean>(false);
  const [transportPreference, setTransportPreference] = useState<any>(
    D_TRANSPORTATION_PREFERENCE
  );
  const [isTransportAvailable, setIsTransportAvailable] = useState(false);

  const [petDetails, setPetDetails] = useState<any>({
    pet_name: "",
    weight: "",
    age_year: "",
    age_month: "",
  });
  const [addPetView, setAddPetView] = useState<boolean>(false);
  const [additionalServices, setAdditionalServices] = useState<
    I_PET_ADDITIONAL_SERVICE[]
  >([]);
  const [cities, setCities] = useState<any>([]);
  const [calculatedAmount, setCalculatedAmount] =
    useState<I_CALCULATED_AMOUNT>(null);
  const [dates, setDates] = useState<any>({ start: undefined, end: undefined });
  const [disabledDates, setDisabledDates] = useState<Date[]>([]);
  const [errors, setErrors] = useState<any>({});
  const today = new Date();
  const [dropOffTime, setDropOffTime] = useState({
    from: ``,
    to: "",
  });
  const [pickupTime, setPickupTime] = useState({
    from: "",
    to: "",
  });

  const [requestAmountData, setRequestAmountData] = useState<requestAmountData>(
    {
      pets: [],
      additional_services: [],
      need_sitter_to_pickup: 0,
    }
  );
  const [requestData, setRequestData] = useState<requestData>({
    pet_type: undefined,
    pet_size_id: 1,
    pickup_address: {
      address1: "",
      latitude: "0",
      longitude: "0",
      address2: "",
      city: "",
      zip_code: "",
    },
    message: "",
    want_to_receive_media: 0,
  });
  const [openCropper, setOpenCropper] = useState<boolean>(false);
  const [cropper, setCropper] = useState<any>();
  const [uploadedFilePath, setUploadedFilePath] = useState<any>();
  const [loading, setLoading] = useState(false);
  const [servicePets, setServicePets] = useState([]);
  const [wantToReceiveMedia, setWantToReceiveMedia] = useState(false);

  useEffect(() => {
    if (router.query.sitterId && router.query.service) {
      setSitterId(router.query.sitterId);
      setService(serviceData.find((v) => v.value == router.query.service));
      if (router.query.petType) {
        let pet = petType.find(
          (val) => val.key == Number(router.query.petType)
        );
        setRequestData({ ...requestData, pet_type: pet.key });
      }
      setSitterName(router.query.name);
      getCities(1);
    }
  }, [router.query]);

  useEffect(() => {
    if (service) {
      setLoading(true);
      petAdditionalRequest();
      petServiceAvailability();
      getBreed();
    }
  }, [service, requestData.pet_type]);

  useEffect(() => {
    if (
      service &&
      requestAmountData.pets &&
      requestAmountData.pets.length > 0 &&
      requestData.pet_type != undefined
    ) {
      if (dates.from && dates.to) {
        petAmountCalculation();
      }
    }
  }, [
    requestAmountData?.pets,
    requestAmountData?.additional_services,
    requestAmountData?.need_sitter_to_pickup,
    dates,
  ]);

  const onChangeChecked = (e: React.ChangeEvent<HTMLInputElement>) => {
    let type = null;
    switch (e.target.id) {
      case "dog":
        type = 1;
        break;
      case "cat":
        type = 2;
        break;
      case "birds":
        type = 3;
        break;
      case "reptiles":
        type = 4;
        break;
      case "animals":
        type = 5;
        break;
    }
    setRequestData({ ...requestData, pet_type: type });
    setRequestAmountData({ ...requestAmountData, pets: [] });
  };

  const onSelectPet = (value: I_PET) => {
    let reqData: requestAmountData = deepClone(requestAmountData);
    if (requestAmountData.pets.includes(value.id)) {
      let index = reqData.pets.findIndex((val) => val == value.id);
      reqData.pets.splice(index, 1);
    } else {
      reqData.pets.push(value.id);
    }
    setRequestAmountData(reqData);
  };

  const addAdditonalService = (value: I_PET_ADDITIONAL_SERVICE) => {
    let reqData: requestAmountData = deepClone(requestAmountData);
    if (requestAmountData.additional_services.includes(value.id)) {
      let index = reqData.additional_services.findIndex(
        (val) => val == value.id
      );
      reqData.additional_services.splice(index, 1);
    } else {
      reqData.additional_services.push(value.id);
    }
    setRequestAmountData(reqData);
  };

  const handlePetSizeChange = (petSize: select) => {
    setPetDetails({ ...petDetails, weight: petSize });
  };

  const onAddressTextChange = (e) => {
    let reqData: requestData = deepClone(requestData);
    reqData.pickup_address = {
      ...reqData.pickup_address,
      [e.target.name]: e.target.value,
    };
    setRequestData(reqData);
  };

  const onMessageChange = (e) => {
    let reqData: requestData = deepClone(requestData);
    reqData.message = e.target.value;
    setRequestData(reqData);
  };

  const onMediaChange = (e) => {
    let reqData: requestData = deepClone(requestData);
    reqData.want_to_receive_media = reqData.want_to_receive_media == 1 ? 0 : 1;
    setRequestData(reqData);
  };



  function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
      dateArray.push(new Date(currentDate));
      currentDate = addDays(currentDate, 1);
    }
    return dateArray;
  }

  const getBreed = () => {
    let data = JSON.stringify({ pet_type: requestData.pet_type });
    api
      .getBreedWithType(data)
      .then((response: AxiosResponse<Res<I_BREED[]>>) => {
        setBreeds(response.data.response);
      })
      .catch((error) => console.log(error));
  };

  const getOptionValue = (option) => {
    return option.id;
  };

  const getOptionLabel = (option) => {
    return option.breed;
  };

  const onFileChange = (event) => {
    if (event.dataTransfer || event.target.files) {
      let files;
      if (event.dataTransfer) {
        files = event.dataTransfer.files;
      } else if (event.target) {
        files = event.target.files;
      }
      const reader = new FileReader();
      reader.onload = () => {
        setUploadedFilePath(reader.result);
      };

      if (files[0]) {
        reader.readAsDataURL(files[0]);
        setOpenCropper(true);
      }
    }
  };

  const onTextChange = (event) => {
    setPetDetails({ ...petDetails, [event.target.name]: event.target.value });
  };

  const addPet = () => {
    const data = JSON.stringify({
      pet_type: requestData.pet_type,
      pet_image: selectedImage,
      pet_name: petDetails.pet_name,
      weight: petDetails.weight.value,
      age_year: petDetails.age_year,
      age_month: petDetails.age_month,
      breed_id: selectedBreed ? selectedBreed.id : null,
      sex: selectedGender ? selectedGender.value : null,
    });

    api
      .addPet(data)
      .then((response) => {
        petAdditionalRequest();
        setAddPetView(false);
        setPetDetails({
          pet_name: "",
          weight: "",
          age_year: "",
          age_month: "",
        });
        setSelectedGender(null);
        setSelectedBreed(null);
        setSelectedImage(null);
      })
      .catch((error) => {
        setErrors(error.response.data.errors);
        console.log(error.response.data.errors);
      });
  };

  const petAdditionalRequest = () => {
    let data = JSON.stringify({
      pet_type: requestData.pet_type,
      sitter_id: sitterId,
      service_id: service.value,
      pets: requestAmountData.pets,
      additional_services: requestAmountData.additional_services,
      message: requestData.message,
      want_to_receive_media: requestData.want_to_receive_media,
    });
    api
      .petAdditionalServices(data)
      .then((response: AxiosResponse<Res<I_REQUEST_DATA>>) => {
        let data = response.data.response;
        setSitterPets(data.sitter_pets);
        if (data.sitter_pets.length > 0) {
          setRequestData({
            ...requestData,
            pet_type: requestData.pet_type
              ? requestData.pet_type
              : data.sitter_pets[0],
          });
        }
        let availableIndex = data?.my_pets.findIndex(
          (val) => val.is_available === true
        );
        if (availableIndex > -1) {
          onSelectPet(data?.my_pets[availableIndex]);
        }
        setMyPets(data.my_pets);
        setAdditionalServices(data.additional_services);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const checkboxRememberOnChange = () => {
    setRequestData({
      ...requestData,
      want_to_receive_media: requestData.want_to_receive_media === 0 ? 1 : 0,
    });
  };
  const petAmountCalculation = () => {
    let data = JSON.stringify({
      pets: requestAmountData?.pets,
      additional_services: requestAmountData?.additional_services,
      need_sitter_to_pickup: requestAmountData?.need_sitter_to_pickup,
      sitter_id: sitterId,
      service_id: service.value,
      duration: router.query?.duration ? Number(router.query?.duration) : null,
      grooming_service_id: router.query?.grooming
        ? Number(router.query?.grooming)
        : null,
      medical_service_id: router.query?.medical
        ? Number(router.query?.medical)
        : null,
      start_date:
        dates.from && dates.to
          ? format(new Date(dates.from), "yyyy-MM-dd")
          : null,
      end_date:
        dates.from || dates.to
          ? format(new Date(dates.to || dates.from), "yyyy-MM-dd")
          : null,
      ...requestData,
    });

    api
      .petAmountCalculation(data)
      .then((response) => {
        if (response.data.status == 200) {
          setCalculatedAmount(response.data.response);
        } else {
          openError(response.data.message);
        }
      })
      .catch((error) => {
        if (error.response.data && error.response.data.message) {
          openError(error.response.data.message);
        }
      });
  };

  const getCities = (id) => {
    api
      .getCities(id)
      .then((json) => {
        let data = json.data.response.map((value) => {
          return {
            label: value.name,
            value: value.id,
          };
        });
        setCities(data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  /**
   * @funtion will check the availability of services for that particular pet.
   * @success: will check availability and if not then will show error response to user.
   */
  const petServiceAvailability = () => {
    let data = JSON.stringify({
      sitter_id: sitterId,
      service_id: service.value,
      is_available: 0,
    });
    api
      .petServiceAvailability(data)
      .then((response) => {
        let dates = response?.data?.response?.services?.map(
          (value) => new Date(value.ScheduleDate)
        );
        setDisabledDates(dates);
        setLoading(false);
        if (router?.query?.service) {
          switch (router?.query?.service) {
            case "1":
              response.data?.response?.preferences?.boarding_preference
                ?.pickup_from_client_home == 1
                ? setIsTransportAvailable(true)
                : setIsTransportAvailable(false);
              break;
            case "2":
              response.data?.response?.preferences
                ?.house_sitting_service_preference?.pickup_from_client_home == 1
                ? setIsTransportAvailable(true)
                : setIsTransportAvailable(false);
              break;
            case "3":
              response.data?.response?.preferences
                ?.drop_in_visit_service_preference?.pickup_from_client_home == 1
                ? setIsTransportAvailable(true)
                : setIsTransportAvailable(false);
              let DropInData =
                response.data?.response?.preferences
                  ?.house_call_service_preference?.service_pets;
              setServicePets(DropInData);
              break;
            case "4":
              response.data?.response?.preferences?.day_care_service_preference
                ?.pickup_from_client_home == 1
                ? setIsTransportAvailable(true)
                : setIsTransportAvailable(false);
              break;
            case "5":
              response.data?.response?.preferences
                ?.pet_walking_service_preference?.pickup_from_client_home == 1
                ? setIsTransportAvailable(true)
                : setIsTransportAvailable(false);
              let data =
                response.data?.response?.preferences
                  ?.pet_walking_service_preference?.service_pets;
              setServicePets(data);

              break;
            case "6":
              response.data?.response?.preferences?.grooming_service_preference
                ?.pickup_from_client_home == 1
                ? setIsTransportAvailable(true)
                : setIsTransportAvailable(false);
              break;
            case "7":
              response.data?.response?.preferences
                ?.house_call_service_preference?.pickup_from_client_home == 1
                ? setIsTransportAvailable(true)
                : setIsTransportAvailable(false);
              let houseCallData =
                response.data?.response?.preferences
                  ?.house_call_service_preference?.service_pets;
              setServicePets(houseCallData);
              break;
          }
        }
      })
      .catch((error) => {
        if (
          error.response &&
          error.response.data &&
          error.response.data.message
        ) {
          openError(error.response.data.message);
        }
      });
  };

  /**
   * @funtion to send request after filling all necessary data
   * @success request sent & redirect to the chat where user can chat directly to sitter.
   * @failure it contains number of validations which can be fail and show error via snack bar to the user that they are not provided valid data.
   */
  const requestSitter = () => {
    console.log("reqdata", requestData);
    if (!requestAmountData.pets.length) {
      openError(strings.SelectPet);
      setLoading(false);
      return false;
    }
    if (!dates.from || !dates.to) {
      openError(strings.SelectDates);
      setLoading(false);
      return false;
    }

    if (dropOffTime.from == "" || dropOffTime.to == "") {
      openError(strings.SelectDropOfTime);
      setLoading(false);
      return false;
    }

    if (pickupTime.from == "" || pickupTime.to == "") {
      openError(strings.SelectPickupTime);
      setLoading(false);
      return false;
    }

    let currdate = new Date();
    let today = Date.parse(`${currdate}`);
    let a = Date.parse(
      `${new Date(
        `${moment(dates.from).format("MMM DD YYYY")} ${moment(
          dropOffTime.from,
          ["h:mm A"]
        ).format("HH:mm")}`
      )}`
    );
    let b = Date.parse(
      `${new Date(
        `${moment(dates.from).format("MMM DD YYYY")} ${moment(dropOffTime.to, [
          "h:mm A",
        ]).format("HH:mm")}`
      )}`
    );
    let c = Date.parse(
      `${new Date(
        `${moment(dates.to).format("MMM DD YYYY")} ${moment(pickupTime.from, [
          "h:mm A",
        ]).format("HH:mm")}`
      )}`
    );
    let d = Date.parse(
      `${new Date(
        `${moment(dates.to).format("MMM DD YYYY")} ${moment(pickupTime.to, [
          "h:mm A",
        ]).format("HH:mm")}`
      )}`
    );
    if (a < today) {
      openError(strings.InvalidDropOffTime1);
      setLoading(false);
      return false;
    }
    if (b < a) {
      openError(strings.InvalidDropOffTime2);
      setLoading(false);
      return false;
    }
    if (c < b) {
      openError(strings.InvalidPickUpTime1);
      setLoading(false);
      return false;
    }
    if (d < c) {
      openError(strings.InvalidPickUpTime2);
      setLoading(false);
      return false;
    }
    // return

    if (requestData.pet_type == undefined) {
      openError(strings.SelectPetType);
      setLoading(false);
      return false;
    }

    if (requestAmountData.pets.length == 0) {
      openError(strings.SelectPets);
      setLoading(false);
      return false;
    }

    if (requestAmountData.need_sitter_to_pickup == 1) {
      if (requestData.pickup_address.address1 == "") {
        openError(strings.EnterAddressLine1);
        setLoading(false);
        return false;
      } else if (requestData.pickup_address.zip_code == "") {
        openError(strings.EnterZipcode);
        setLoading(false);
        return false;
      }
    }
    setLoading(true);
    let data = JSON.stringify({
      ...requestData,
      pets: requestAmountData?.pets,
      additional_services: requestAmountData?.additional_services,
      need_sitter_to_pickup: requestAmountData?.need_sitter_to_pickup,
      sitter_id: sitterId,
      service_id: service.value,
      duration: router.query?.duration ? Number(router.query?.duration) : null,
      grooming_service_id: router.query?.grooming
        ? Number(router.query?.grooming)
        : null,
      medical_service_id: router.query?.medical
        ? Number(router.query?.medical)
        : null,
      drop_of: {
        date: dates
          ? dates.from
            ? format(new Date(dates.from), "yyyy-MM-dd")
            : ""
          : "",
        from: moment(dropOffTime.from, ["h:mm A"]).format("HH:mm"),
        to: moment(dropOffTime.to, ["h:mm A"]).format("HH:mm"),
      },
      pickup: {
        date: dates
          ? dates.to
            ? format(new Date(dates.to), "yyyy-MM-dd")
            : ""
          : "",
        from: moment(pickupTime.from, ["h:mm A"]).format("HH:mm"),
        to: moment(pickupTime.to, ["h:mm A"]).format("HH:mm"),
      },
    });

    api
      .sitterRequest(data)
      .then((json) => {
        openSuccess(strings.RequestSent);
        setErrors({});
        router
          .replace({ pathname: "/chat", query: { id: sitterId } })
          .then((r) => setLoading(false));
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        if (error.response.status == 422) {
          setErrors(error.response.data.errors);
          let err = error.response.data.errors;
          if (err["drop_of.from"]) {
            openError(strings.InvalidDropOffTime);
          } else if (err["drop_of.to"]) {
            openError(strings.InvalidDropOffTime);
          } else if (err["pickup.from"]) {
            openError(strings.InvalidPickUpTime);
          } else if (err["pickup.to"]) {
            openError(strings.InvalidPickUpTime);
          }
          setLoading(false);
        }
      });
  };

  const { from, to } = dates;
  const modifiers = { start: from, end: to };
 
  const handlePickupDateChange = (date) => {
    setDates({ ...dates, from: date, to: undefined });
  };
  
  const handleDropoffDateChange = (date) => {
    setDates({ ...dates, to: date });
  };

  return (
    <div className="main-wrapper bottom ">
      {loading ? <OverlayLoader /> : null}
      <div className="container">
        <div className="row  padding">
          <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
            <h4 className="font-semibold mb-3 ">
              {strings.Makebookingwith +
                " " +
                sitterName +
                " for " +
                (service ? service.label : "")}
            </h4>
            <div className="row">
              <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                <div className="bg-white main-content m-0">
                  <h5 className="font-medium mb-3">{strings.Bookingfor}</h5>
                  {/*---------for web------------------*/}
                  <div className="">
                    <div className="booking-for booking-check">
                      <div
                        onClick={() => {
                          sitterPets.includes(1)
                            ? null
                            : openError(
                                strings.PetIsNotFacilitatedByTheSelectedSitter
                              );
                        }}
                        className="custom-check"
                        style={{
                          opacity: sitterPets.includes(1) ? 1 : 0.5,
                        }}
                      >
                        <label className="check ">
                          <input
                            type="radio"
                            className="class1"
                            name="is_name2"
                            id="dog"
                            checked={requestData.pet_type == 1}
                            onChange={onChangeChecked}
                            defaultValue="dog"
                            disabled={!sitterPets.includes(1)}
                          />
                          <span className="checkmark" /> {strings.Dog}
                        </label>
                      </div>
                      <div
                        onClick={() => {
                          sitterPets.includes(2)
                            ? null
                            : openError(
                                strings.PetIsNotFacilitatedByTheSelectedSitter
                              );
                        }}
                        className="custom-check"
                        style={{ opacity: sitterPets.includes(2) ? 1 : 0.5 }}
                      >
                        <label className="check ">
                          <input
                            type="radio"
                            className="class1"
                            name="is_name2"
                            onChange={onChangeChecked}
                            checked={requestData.pet_type == 2}
                            defaultValue="cat"
                            id="cat"
                            disabled={!sitterPets.includes(2)}
                          />
                          <span className="checkmark" /> {strings.Cat}
                        </label>
                      </div>
                      <div
                        className="custom-check"
                        onClick={() => {
                          sitterPets.includes(3)
                            ? null
                            : openError(
                                strings.PetIsNotFacilitatedByTheSelectedSitter
                              );
                        }}
                        style={{
                          opacity: sitterPets.includes(3) ? 1 : 0.5,
                        }}
                      >
                        <label className="check ">
                          <input
                            type="radio"
                            className="class1"
                            name="is_name2"
                            onChange={onChangeChecked}
                            checked={requestData.pet_type == 3}
                            defaultValue="birds"
                            id="birds"
                            disabled={!sitterPets.includes(3)}
                          />
                          <span className="checkmark" /> {strings.Birds}
                        </label>
                      </div>
                      <div
                        className="custom-check"
                        onClick={() => {
                          sitterPets.includes(4)
                            ? null
                            : openError(
                                strings.PetIsNotFacilitatedByTheSelectedSitter
                              );
                        }}
                        style={{
                          opacity: sitterPets.includes(4) ? 1 : 0.5,
                        }}
                      >
                        <label className="check ">
                          <input
                            type="radio"
                            className="class1"
                            name="is_name2"
                            onChange={onChangeChecked}
                            checked={requestData.pet_type == 4}
                            defaultValue="reptiles"
                            id="reptiles"
                            disabled={!sitterPets.includes(4)}
                          />
                          <span className="checkmark" /> Reptiles
                        </label>
                      </div>
                      <div
                        className="custom-check"
                        onClick={() => {
                          sitterPets.includes(5)
                            ? null
                            : openError(
                                strings.PetIsNotFacilitatedByTheSelectedSitter
                              );
                        }}
                        style={{
                          opacity: sitterPets.includes(5) ? 1 : 0.5,
                        }}
                      >
                        <label className="check ">
                          <input
                            type="radio"
                            className="class1"
                            name="is_name2"
                            onChange={onChangeChecked}
                            checked={requestData.pet_type == 5}
                            defaultValue="animals"
                            id="animals"
                            disabled={!sitterPets.includes(5)}
                          />
                          <span className="checkmark" /> {strings.Smallanimals}
                        </label>
                      </div>
                      {/*--------for dog -------------*/}
                      <div className="dog box" style={{ display: "block" }}>
                        <ul className="chec-radio">
                          {myPets.map((value, index) => (
                            <li
                              key={index}
                              className="pet-select"
                              onClick={() =>
                                !(service.key == 6 || service.key == 7)
                                  ? value.is_available
                                    ? null
                                    : openError(
                                        strings.PetIsNotFacilitatedByTheSelectedSitter
                                      )
                                  : null
                              }
                            >
                              <label className="radio-inline">
                                <input
                                  type="checkbox"
                                  key={`pet${index}`}
                                  id={`pet${index}`}
                                  name="select1"
                                  checked={
                                    requestAmountData.pets
                                      ? requestAmountData.pets.includes(
                                          value.id
                                        )
                                      : false
                                  }
                                  onChange={() => onSelectPet(value)}
                                  className="pro-chx"
                                  defaultValue="yes"
                                  disabled={
                                    !(service.key == 6 || service.key == 7)
                                      ? !value.is_available
                                      : false
                                  }
                                />
                                <div className="select-radio text-center">
                                  <div className="pet-div">
                                    <div className="search-sitter-img">
                                      <img
                                        src={value.pet_image}
                                        className="img-fluid mb-2"
                                      />
                                    </div>
                                    <div className="pet-text">
                                      <h6 className="font-14 mb-1 font-medium">
                                        {`${
                                          value.pet_name +
                                          " " +
                                          value.age_year +
                                          strings.yrs
                                        },\n   ${value.weight.name}`}
                                      </h6>
                                      <p className="font-12 mb-0">
                                        {value.breed.breed}
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </label>
                            </li>
                          ))}
                        </ul>
                      </div>
                      {/*--------/for dog -------------*/}

                      {/*---------add new pets-------*/}
                      <div className="card-button mt-2">
                        <button
                          onClick={() => setAddPetView(true)}
                          className="btn btn-black"
                        >
                          {strings.Addnewpet}
                        </button>
                      </div>
                      <div
                        className="additional-services pickup-address"
                        id="welcomeDiv"
                        style={{ display: addPetView ? "block" : "none" }}
                      >
                        <div className="upload-doc">
                          {selectedImage ? (
                            <img className="file" src={selectedImage} />
                          ) : (
                            <div className="file">
                              + <br />
                              {strings.UploadYourPetsPicture}
                              <input
                                type="file"
                                name="file"
                                onChange={onFileChange}
                              />
                            </div>
                          )}
                        </div>
                        {errors.pet_image ? (
                          <span
                            style={{
                              color: "#ff0000",
                              fontSize: "12px",
                            }}
                          >
                            {strings.PleaseSelectPetImage}
                          </span>
                        ) : null}

                        <div className="upload-doc"></div>
                        <div className="row">
                          <div className="col-12 col-md-6 col-lg-6 col-xl-5">
                            <h6 className="font-14 mb-2">
                              {strings.Name + "*"}
                            </h6>
                            <div className="form-group">
                              <input
                                className={
                                  "form-control " +
                                  (errors.pet_name ? "invalid" : "")
                                }
                                id="pet-name"
                                value={petDetails.pet_name}
                                onChange={onTextChange}
                                name="pet_name"
                                type="text"
                              />
                            </div>
                          </div>
                          <div className="col-6 col-md-6 col-lg-6 col-xl-3">
                            <h6 className="font-14 mb-2">
                              {strings.Weight + "(" + strings.kgs + ")*"}
                            </h6>
                            <div className="form-group">
                              <Select
                                onChange={handlePetSizeChange}
                                value={petDetails.weight}
                                isSearchable={false}
                                options={petSize}
                                isMulti={false}
                              />
                            </div>
                          </div>
                          <div className="col-6 col-md-6 col-lg-6 col-xl-4">
                            <h6 className="font-14 mb-2">
                              {strings.Age + "*"}
                            </h6>
                            <div className="form-row">
                              <div className="form-group col-6 col-sm-6">
                                <input
                                  className={
                                    "form-control " +
                                    (errors.age_year ? "invalid" : "")
                                  }
                                  value={petDetails.age_year}
                                  onChange={onTextChange}
                                  id="year"
                                  maxLength={2}
                                  name="age_year"
                                  type="text"
                                  placeholder={strings.Yr_D}
                                />
                              </div>
                              <div className="form-group col-6 col-sm-6">
                                <input
                                  value={petDetails.age_month}
                                  onChange={onTextChange}
                                  className={
                                    "form-control " +
                                    (errors.age_month ? "invalid" : "")
                                  }
                                  maxLength={2}
                                  id="month"
                                  name="age_month"
                                  type="text"
                                  placeholder={strings.Mo_D}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6 col-lg-6 col-xl-8">
                            <h6 className="font-14 mb-2">
                              {strings.Bread + "*"}
                            </h6>
                            <div className="form-group">
                              <Select
                                isSearchable={true}
                                value={selectedBreed}
                                getOptionValue={getOptionValue}
                                getOptionLabel={getOptionLabel}
                                options={breeds}
                                styles={
                                  errors.breed_id
                                    ? {
                                        control: (provided) => ({
                                          ...provided,
                                          borderColor: "#ff0000",
                                        }),
                                      }
                                    : {}
                                }
                                noOptionsMessage={() =>
                                  requestData.pet_type
                                    ? strings.NotFound
                                    : strings.SelectPetFirst
                                }
                                placeholder={strings.TypeHereToSearchBreed}
                                onChange={setSelectedBreed}
                              />
                            </div>
                          </div>
                          <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                            <h6 className="font-14 mb-2">
                              {strings.Sex + "*"}
                            </h6>
                            <Select
                              value={selectedGender}
                              onChange={setSelectedGender}
                              options={gender}
                              isSearchable={false}
                              styles={
                                errors.sex
                                  ? {
                                      control: (provided) => ({
                                        ...provided,
                                        borderColor: "#ff0000",
                                      }),
                                    }
                                  : {}
                              }
                            />
                          </div>
                        </div>
                        <div>
                          <button
                            onClick={addPet}
                            className="btn btn-primary py-2 px-3"
                          >
                            {strings.Submit}
                          </button>
                          <a
                            onClick={() => {
                              setAddPetView(false);
                              setPetDetails({
                                pet_name: "",
                                weight: "",
                                age_year: "",
                                age_month: "",
                              });
                              setSelectedGender(null);
                              setSelectedBreed(null);
                              setSelectedImage(null);
                            }}
                            className="btn link"
                          >
                            {strings.Cancel}
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/*--------/for web------------------*/}

                  {requestData.pet_type ? (
                    <>
                      {additionalServices.length > 0 ? (
                        <>
                          <hr />
                          <div className="additional-services">
                            <h5 className="mb-3 font-medium">
                              {strings.additionalService}
                            </h5>
                            <div className="booking-for">
                              {additionalServices.map((value, index) => (
                                <div key={index} className="row mb-3">
                                  <div className="col-10 col-md-8 col-lg-8 col-xl-8 grooming-details">
                                    <p className="mb-0 groom">
                                      {value.name}&nbsp;
                                      <span className="font-semibold">
                                        {getCurrencySign() + value.price}
                                      </span>
                                    </p>
                                    <p className="mb-0 font-12">
                                      {value.description}
                                    </p>
                                  </div>
                                  <div className="col-2 col-md-4 col-lg-4 col-xl-4 alignment">
                                    <button
                                      onClick={() => addAdditonalService(value)}
                                      className="btn btn-black btn-green"
                                    >
                                      {requestAmountData.additional_services.includes(
                                        value.id
                                      ) ? (
                                        <>
                                          <svg
                                            aria-hidden="true"
                                            focusable="false"
                                            data-prefix="fal"
                                            data-icon="check-circle"
                                            role="img"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 512 512"
                                            className="svg-inline--fa fa-check-circle fa-w-16 fa-2x"
                                          >
                                            <path
                                              fill="currentColor"
                                              d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 464c-118.664 0-216-96.055-216-216 0-118.663 96.055-216 216-216 118.664 0 216 96.055 216 216 0 118.663-96.055 216-216 216zm141.63-274.961L217.15 376.071c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.97.068l68.976 69.533 163.441-162.13c4.705-4.667 12.303-4.637 16.97.068l8.451 8.52c4.668 4.705 4.637 12.303-.068 16.97z"
                                            />
                                          </svg>
                                          {strings.Added}
                                        </>
                                      ) : (
                                        strings.AddService
                                      )}
                                    </button>
                                  </div>
                                </div>
                              ))}
                            </div>
                          </div>
                        </>
                      ) : null}
                      <hr />
                      {requestAmountData.pets.length > 0 ? (
                        <>
                          <div className="additional-services booking-period">
                            <h5 className="mb-3 font-medium">
                              {strings.BookingPeriod}
                            </h5>
                            <div className="row booking-for">
                            {!(service.key == 5 || service.key == 3) && (
                                    <label className="text-muted">
                                      <span className="text-danger">*</span>
                                      Note: Select the time range for
                                      checki-in/out that suits you and should
                                      match with sitter's availability.
                                    </label>
                                  )}
                            <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                              <h6 className="font-14 mb-2">{strings.Check_in}<span className="text-danger">*</span></h6>
                              <DatePicker
                                selected={dates.from}
                                onChange={handlePickupDateChange}
                                minDate={new Date()}
                                disabledKeyboardNavigation
                                className="form-control form-control-lg mb-2"
                                placeholderText="Select check-in date "
                                filterDate={(date) => !disabledDates.some(disabledDate => date.toDateString() === disabledDate.toDateString())}
                              />
                              <div className="form-row">
                                    <div
                                      className="col-6 col-sm-6 picker-down"
                                      aria-disabled="true"
                                    >
                                      <DatePicker
                                        selected={
                                          dropOffTime.from &&
                                          dropOffTime.from !== "N/A" &&
                                          !dropOffTime.to?.includes("valid")
                                            ? moment(
                                                dropOffTime.from,
                                                "hh:mm A"
                                              )["_d"]
                                            : ""
                                        }
                                        onChange={(date) => {
                                          const time =
                                            moment(date).format("hh:mm A");
                                          if (
                                            service.key == 5 ||
                                            service.key == 3
                                          ) {
                                            setDropOffTime({
                                              from: time,
                                              to: time,
                                            });
                                            let date = moment(time, "hh:mm A")
                                              .add(120, "minutes")
                                              .format("hh:mm A");
                                            setPickupTime({
                                              from: date,
                                              to: date,
                                            });
                                          } else {
                                            setDropOffTime({
                                              ...dropOffTime,
                                              from: time,
                                            });
                                          }
                                        }}
                                        className="form-control "
                                        showTimeSelect
                                        placeholderText="select from time"
                                        showTimeSelectOnly
                                        timeCaption="Check-in from time"
                                        required
                                        timeIntervals={30}
                                        dateFormat="h:mm a"
                                      />
                                    </div>
                                    <div className="col-6 col-sm-6 picker-down picker-end">
                                      {!(
                                        service.key == 5 || service.key == 3
                                      ) ? (
                                        <DatePicker
                                          selected={
                                            dropOffTime.to &&
                                            dropOffTime.to !== "N/A" &&
                                            !dropOffTime.to?.includes("valid")
                                              ? moment(
                                                  dropOffTime.to,
                                                  "hh:mm A"
                                                )["_d"]
                                              : ""
                                          }
                                          onChange={(date) => {
                                            const time =
                                              moment(date).format("hh:mm A");
                                            setDropOffTime({
                                              ...dropOffTime,
                                              to: time,
                                            });
                                          }}
                                          minTime={
                                            dropOffTime.from &&
                                            dropOffTime.from !== "N/A" &&
                                            !dropOffTime.to?.includes("valid")
                                              ? moment(
                                                  dropOffTime.from,
                                                  "hh:mm A"
                                                )["_d"]
                                              : new Date().setHours(0, 0, 0)
                                          }
                                          maxTime={new Date().setHours(
                                            23,
                                            59,
                                            59
                                          )}
                                          className="form-control "
                                          showTimeSelect
                                          placeholderText="select to time"
                                          showTimeSelectOnly
                                          timeCaption="Check-in to time"
                                          required
                                          timeIntervals={30}
                                          dateFormat="h:mm a"
                                        />
                                      ) : null}
                                    </div>
                                  </div>
                            </div>
                            {!(service.key == 5 || service.key == 3) && (
                            <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                            <h6 className="font-14 mb-2">{strings.Check_out}<span className="text-danger">*</span></h6>
                              <DatePicker
                                selected={dates.to}
                                onChange={handleDropoffDateChange}
                                minDate={dates.from || new Date()}
                                disabled={!dates.from} 
                                disabledKeyboardNavigation
                                className="form-control form-control-lg mb-2"
                                placeholderText="Select check-out date"
                                filterDate={(date) => !disabledDates.some(disabledDate => date.toDateString() === disabledDate.toDateString())}
                              />
                              <div className="form-row">
                                      <div className="form-group col-6 col-sm-6 picker-down">
                                        <DatePicker
                                          selected={
                                            pickupTime.from &&
                                            pickupTime.from !== "N/A" &&
                                            !pickupTime.from?.includes("valid")
                                              ? moment(
                                                  pickupTime.from,
                                                  "hh:mm A"
                                                )["_d"]
                                              : ""
                                          }
                                          onChange={(date) => {
                                            const time =
                                              moment(date).format("hh:mm A");
                                            if (
                                              service.key == 5 ||
                                              service.key == 3
                                            ) {
                                              setDropOffTime({
                                                from: time,
                                                to: time,
                                              });
                                              let date = moment(time, "hh:mm A")
                                                .add(120, "minutes")
                                                .format("hh:mm A");
                                              setPickupTime({
                                                from: date,
                                                to: date,
                                              });
                                            } else {
                                              setPickupTime({
                                                ...pickupTime,
                                                from: time,
                                              });
                                            }
                                          }}
                                          className="form-control "
                                          showTimeSelect
                                          placeholderText="select from time"
                                          showTimeSelectOnly
                                          timeCaption="Check-out from time"
                                          required
                                          timeIntervals={30}
                                          dateFormat="h:mm a"
                                        />
                                      </div>
                                      <div className="form-group col-6 col-sm-6 picker-down picker-end">
                                        <DatePicker
                                          selected={
                                            pickupTime.to &&
                                            pickupTime.to !== "N/A" &&
                                            !pickupTime.to?.includes("valid")
                                              ? moment(
                                                  pickupTime.to,
                                                  "hh:mm A"
                                                )["_d"]
                                              : ""
                                          }
                                          onChange={(date) => {
                                            const time =
                                              moment(date).format("hh:mm A");
                                            setPickupTime({
                                              ...pickupTime,
                                              to: time,
                                            });
                                          }}
                                          minTime={
                                            pickupTime.from &&
                                            pickupTime.from !== "N/A" &&
                                            !pickupTime.from?.includes("valid")
                                              ? moment(
                                                  pickupTime.from,
                                                  "hh:mm A"
                                                )["_d"]
                                              : new Date().setHours(0, 0, 0)
                                          }
                                          maxTime={new Date().setHours(
                                            23,
                                            59,
                                            59
                                          )}
                                          className="form-control "
                                          showTimeSelect
                                          placeholderText="Check-out to time "
                                          showTimeSelectOnly
                                          timeCaption="Start time"
                                          required
                                          timeIntervals={30}
                                          dateFormat="h:mm a"
                                        />
                                      </div>
                                    </div>
                                    
                            </div>
                            )}
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                   <button
                                    className="btn btn-black mt-1 btn-green"
                                    onClick={() => {
                                      setDates({
                                        start: undefined,
                                        end: undefined,
                                      });
                                      setDropOffTime({
                                        from: "",
                                        to: "",
                                      });
                                      setPickupTime({
                                        from: "",
                                        to: "",
                                      });
                                    }}
                                  >
                                    {strings.Reset}
                                  </button>
                                  </div>
                            </div>
                            {/* booking logic */}
                          </div>
                          <hr />
                        </>
                      ) : null}
                    </>
                  ) : null}
                  {requestAmountData.need_sitter_to_pickup == 1 ? (
                    <div className="additional-services pickup-address">
                      <h5 className="mb-3 font-medium">
                        {strings.EnterPickupAddress}
                      </h5>
                      <div className="booking-for">
                        <div className="form-row">
                          <div className="form-group col-sm-12 location-search-input">
                            <label>{strings.AddressLine1}*</label>
                            <LocationSearchInput2
                              key={"booking"}
                              className={"form-control mb-0"}
                              value={value}
                              setFilter={(Address) => {
                                let newAddress = { ...requestData };
                                newAddress.pickup_address.address1 =
                                  Address.value;
                                newAddress.pickup_address.zip_code =
                                  Address.postalCode;
                                newAddress.pickup_address.city = Address.city;
                                setRequestData(newAddress);
                              }}
                              setLatLng={(latLng) => {
                                let newAddress = { ...requestData };
                                newAddress.pickup_address.latitude = String(
                                  latLng.lat
                                );
                                newAddress.pickup_address.longitude = String(
                                  latLng.lng
                                );
                                setRequestData(newAddress);
                              }}
                              setValue={(address) => {
                                setValue(address);
                                if (address == "") {
                                  let newAddress = { ...requestData };
                                  newAddress.pickup_address.address1 = "";
                                  newAddress.pickup_address.zip_code = "";
                                  newAddress.pickup_address.city = "";
                                  newAddress.pickup_address.latitude = `0`;
                                  newAddress.pickup_address.longitude = `0`;
                                  setRequestData(newAddress);
                                }
                              }}
                            />
                          </div>
                          <div className="form-group col-sm-6">
                            <label>{strings.AddressLine2}</label>
                            <input
                              className="form-control"
                              name="address2"
                              type="text"
                              onChange={onAddressTextChange}
                            />
                          </div>
                          <div className="form-group col-6">
                            <label>{strings.City}</label>
                            <div className="category-selection charge-select mt-0">
                              <input
                                className="form-control"
                                name="city"
                                value={
                                  requestData.pickup_address
                                    ? requestData.pickup_address.city
                                    : ""
                                }
                                type="text"
                                onChange={onAddressTextChange}
                              />
                            </div>
                          </div>
                          <div className="form-group col-sm-4 col-6">
                            <label htmlFor="exampleFormControlSelect1">
                              {strings.Zipcode}*
                            </label>
                            <div className="category-selection charge-select mt-0">
                              <input
                                className="form-control"
                                type="text"
                                placeholder={strings.Zipcode}
                                name="zip_code"
                                value={
                                  requestData.pickup_address
                                    ? requestData.pickup_address.zip_code
                                    : ""
                                }
                                onChange={onAddressTextChange}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr />
                    </div>
                  ) : null}
                  <div className="additional-services pickup-address">
                    <h5 className="mb-3 font-medium">{strings.Instructions}</h5>
                    <div className="booking-for">
                      <h6 className="font-14 font-normal">
                        {
                          strings.ShareSomeInformationAboutYourPetsThatNeedToBeTakenCareWhileSittings
                        }
                      </h6>
                      <div className="form-group">
                        <textarea
                          className="form-control"
                          id="message"
                          rows={6}
                          value={requestData.message}
                          onChange={onMessageChange}
                          defaultValue={""}
                        />
                      </div>
                    </div>
                  </div>

                  <hr className="d-none d-md-block" />

                  <div className="col-12 col-md-6 col-lg-6 col-xl-6 col-sm-6 mb-3 px-0">
                    <div className="custom-check">
                      <label className="check ">
                        <input
                          onChange={checkboxRememberOnChange}
                          type="checkbox"
                          name="is_name1"
                        />
                        <span className="checkmark" />I want to receive photos
                        or videos
                      </label>
                    </div>
                  </div>
                  <div className="d-none d-md-block">
                    <div className="row justify-content-end">
                      <div className="col-12 col-md-12 col-lg-12 col-xl-6 alignment">
                        <div className="proceed-btn m-0">
                          <a>
                            <button
                              onClick={() => {
                                requestSitter();
                              }}
                              className="btn btn-primary px-3"
                            >
                              {strings.Request}
                            </button>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-4 booking-info">
                <div className="bg-white main-content m-0">
                  <div className="col-12 px-0 mb-0">
                    <div className="row align-items-center">
                      <div className="col">
                        <h5 className="mb-0">{strings.bookingDetails}</h5>
                      </div>
                      <div className="col-auto d-block d-lg-none">
                        <svg
                          viewBox="0 0 24 24"
                          width="24"
                          height="24"
                          stroke="currentColor"
                          strokeWidth="2"
                          fill="none"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          className="icon-close-mob"
                        >
                          <line x1="18" y1="6" x2="6" y2="18"></line>
                          <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                      </div>
                    </div>
                  </div>
                  <hr />
                  <div className="boarding-details">
                    <h6 className="font-medium mb-3">
                      {(service ? service.label : "") + " " + strings.for}
                    </h6>
                    {calculatedAmount &&
                    requestAmountData.pets?.length &&
                    dates.to
                      ? calculatedAmount.pets?.map((value, index) => (
                          <div
                            key={index}
                            className="d-flex justify-content-between mb-3"
                          >
                            <div>
                              <p className="font-14 text-muted mb-0">
                                {value.name}
                              </p>
                            </div>
                            <div>
                              <p className="font-14 text-muted mb-0">
                                {getCurrencySign() + value.fee}
                              </p>
                            </div>
                          </div>
                        ))
                      : null}
                    {calculatedAmount &&
                    calculatedAmount.custom &&
                    calculatedAmount.custom.length &&
                    requestAmountData.pets.length &&
                    dates.to
                      ? calculatedAmount.custom.map((value, index) => (
                          <div
                            key={index}
                            className="d-flex justify-content-between mb-3"
                          >
                            <div>
                              <p className="font-14 text-muted mb-0">
                                {value.name}
                              </p>
                            </div>
                            <div>
                              <p className="font-14 text-muted mb-0">
                                {getCurrencySign() + value.fee}
                              </p>
                            </div>
                          </div>
                        ))
                      : null}
                    <hr />
                    <div className="d-flex justify-content-between">
                      <div>
                        <p className="font-14 font-semibold mb-0">
                          {strings.totalAmount}
                        </p>
                      </div>
                      <div>
                        <p className="font-14 font-semibold  mb-0">
                          {getCurrencySign() +
                            (calculatedAmount &&
                            requestAmountData.pets.length &&
                            dates.to
                              ? calculatedAmount.total
                              : "0")}
                        </p>
                      </div>
                    </div>
                  </div>
                  <p className="font-14 text-muted mb-0">
                    {
                      strings.FinalTransportationFeeWillBeCalculatedAtTheTimeOfCheckout
                    }
                    &nbsp;
                    {requestAmountData.pets.length && dates.to ? (
                      <a
                        onClick={() => setShowTransport(!showTransport)}
                        style={{ textDecorationLine: "underline" }}
                      >
                        {showTransport
                          ? strings.HideStartingPrice
                          : strings.ViewStartingPrice}
                      </a>
                    ) : (
                      ""
                    )}
                  </p>
                  {showTransport &&
                    calculatedAmount &&
                    calculatedAmount.transportation.map((val, index) =>
                      val.status == 0 ? null : (
                        <div
                          key={`transport_${index}`}
                          className="col-12 p-0 mb-2"
                        >
                          <div className="row align-items-center mt-3">
                            <div className="col-md-3 ps-0">
                              <div className="custom-check mb-0">
                                <label className="check ">
                                  {D_TRANSPORTATION[index].label}
                                </label>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <input
                                disabled
                                name="price_start_from"
                                type={"text"}
                                className="form-control"
                                placeholder={strings.amount}
                                value={val.price_start_from}
                              />
                            </div>
                          </div>
                        </div>
                      )
                    )}
                </div>
              </div>
            </div>
            {/*----------*/}
          </div>
        </div>
      </div>
      <div className="d-block d-md-none book-fixed">
        <div className="col-12">
          <div className="row align-items-center">
            <div className="col">
              <p className=" font-weight-medium">{strings.totalAmount}</p>
              <h6 className="mb-0">
                {getCurrencySign() +
                  (calculatedAmount && requestAmountData.pets.length && dates.to
                    ? calculatedAmount.total
                    : "0")}
                <span className="ml-2">
                  <svg
                    viewBox="0 0 24 24"
                    width="24"
                    height="24"
                    stroke="#0b6963"
                    strokeWidth="2"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <circle cx="12" cy="12" r="10"></circle>
                    <line x1="12" y1="16" x2="12" y2="12"></line>
                    <line x1="12" y1="8" x2="12.01" y2="8"></line>
                  </svg>
                </span>
              </h6>
            </div>
            <div className="col-12 col-md-6 col-lg-6 col-xl-6 col-sm-6 mb-3 px-0"></div>
            <div className="col-auto">
              <button
                onClick={() => {
                  requestSitter();
                }}
                className="btn btn-primary px-3"
              >
                {strings.Request}
              </button>
            </div>
          </div>
        </div>
      </div>
      <UploadFileModal
        onInitialized={(instance) => {
          setCropper(instance);
        }}
        path={uploadedFilePath}
        showModal={openCropper}
        zoomable={false}
        aspectRatio={16 / 9}
        setImage={(v) => {
          var file = dataURLtoFile(v, "image");
          const formData = new FormData();
          formData.append("image", file);
          setErrors({ ...errors, pet_image: null });
          formData.append("path", "pets");
          api
            .uploadFile(formData)
            .then((json) => {
              setSelectedImage(json.data.response);
            })
            .catch((error) => console.log(error));
        }}
        preview=".img-preview"
        guides={false}
        viewMode={1}
        dragMode="move"
        cropBoxMovable={true}
        hideModal={() => setOpenCropper(false)}
      />
    </div>
  );
};

export default withAuth(Booking);
