import Reviews from "../../components/sitterProfile/Reviews";
import API from "../../api/Api";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { strings } from "../../public/lang/Strings";
import ReviewObject from "../../components/sitterProfile/ReviewObject";
import Cookies from "universal-cookie";
import Link from "next/link";
import moment from "moment";
import RatingStars from "../../components/common/RatingStars";
import Select from "react-select";
import { reviewSorting, starRating } from "../../public/appData/AppData";

const MyReviews = () => {
  const api = new API();
  const router = useRouter();
  const [data, setData] = useState<any>();
  const cookie = new Cookies();
  const [sitterReview, setSitterReview] = useState<boolean>(true);

  const [filter, setFilter] = useState({
    sitter_id: router.query.id,
    sort: 0,
    filter: 0,
  });

  useEffect(() => {
    if (router.query.id) {
      getSitterDetails(filter);
    }
  }, [router.query.id, filter]);

  const getSitterDetails = (filter: any) => {
    api
      .getSitterReviews(filter)
      .then((res) => {
        setData(res.data.response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const OverallRating = (data) => {
    let total =
      Number(data.cleanliness) +
      Number(data.accuracy) +
      Number(data.communication) +
      Number(data.location) +
      Number(data.check_in) +
      Number(data.value);
    return (total / 6).toFixed(1);
  };
  return (
    <div className="container px-0 px-md-3">
      <div className="sort-details py-2">
        <div className="col-12 mt-2 mb-0 px-0">
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-md-flex d-none  high-rating align-center ml-0 filter-design">
              <div className="d-flex align-items-center col-6 col-sm-auto col-md form-group mb-0">
                <label
                  className="form-label show show1"
                  style={{ whiteSpace: "nowrap" }}
                >
                  {strings.SortBy}
                </label>
                <Select
                  defaultValue={reviewSorting[0]}
                  onChange={(e) => {
                    setFilter((prevFilter) => ({
                      ...prevFilter,
                      sort: e.value,
                    }));
                  }}
                  options={reviewSorting}
                />
              </div>
              <div className="d-flex align-items-center col-6 col-sm-auto col-md form-group mb-0">
                <label className="form-label show show1">
                  {strings.StarRating}
                </label>
                <Select
                  defaultValue={starRating[0]}
                  onChange={(e) => {
                    setFilter((prevFilter) => ({
                      ...prevFilter,
                      filter: e.value,
                    }));
                  }}
                  options={starRating}
                />
              </div>
            </div>
            <div className="col-md-auto col-12 p-0 align-center  mt-md-0">
              <div className="font-12 mb-0 w-100 align-center d-flex align-items-center justify-content-between d-lg-block"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="main-wrapper">
        {data ? (
          <>
            <div className="tabs-main main-background">
              <div className="row tabs-design">
                <div className="col-12 px-0 px-md-3">
                  <div className="pay-tabs">
                    <ul className="nav nav-tabs mb-0" id="myTab" role="tablist">
                      <li
                        className="nav-item cursor-pointer"
                        role="presentation"
                      >
                        <a
                          className={
                            sitterReview === true
                              ? "nav-link active"
                              : "nav-link"
                          }
                          style={{ position: "relative" }}
                          id="cards-tab"
                          data-toggle="tab"
                          onClick={() => {
                            setSitterReview(true);
                          }}
                          role="tab"
                        >
                          {strings.SitterReview}
                        </a>
                      </li>
                      <li
                        className="nav-item cursor-pointer"
                        role="presentation"
                      >
                        <a
                          className={
                            sitterReview === false
                              ? "nav-link active"
                              : "nav-link"
                          }
                          id="bank-tab"
                          data-toggle="tab"
                          onClick={() => {
                            setSitterReview(false);
                          }}
                          role="tab"
                        >
                          {strings.OwnerReview}
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 px-3 px-md-0">
              <div className="bg-white main-background">
                {/*------------review details--------------------*/}
                <div className="release-content pt-3">
                  {sitterReview == true ? (
                    <>
                      {data && data?.avg_rating && (
                        <div className="basic-info">
                          <div className="row align-items-center">
                            <div className="col-12 col-md-3 col-lg-3 col-xl-3">
                              <div className="overall-ratings">
                                <div>
                                  <h4 className="font-semibold">
                                    {OverallRating(data.avg_rating)}
                                  </h4>
                                  <p className="mb-0 font-14 font-semibold text-white">
                                    {strings.Overallrating}
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="col-12 col-md-9 col-lg-9 col-xl-9">
                              <div className="ratings-score">
                                <div className="row">
                                  <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <div className="score score1">
                                      <div className="score-details">
                                        <h6 className="mb-0">
                                          {strings.Cleanliness}
                                          <span>
                                            {parseInt(
                                              data.avg_rating.cleanliness
                                            ) + "/ 5"}
                                          </span>
                                        </h6>
                                      </div>
                                      <div className="score-bar">
                                        <div className="rSlider">
                                          <span
                                            className="slide"
                                            style={{
                                              width: `${
                                                parseInt(
                                                  data.avg_rating.cleanliness
                                                ) * 20
                                              }%`,
                                            }}
                                          ></span>
                                          <input
                                            id="range"
                                            type="range"
                                            min="0"
                                            max="5"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <div className="score score1">
                                      <div className="score-details">
                                        <h6 className="mb-0">
                                          {strings.Accuracy}
                                          <span>
                                            {parseInt(
                                              data.avg_rating.accuracy
                                            ) + " / 5"}
                                          </span>
                                        </h6>
                                      </div>
                                      <div className="score-bar">
                                        <div className="rSlider">
                                          <span
                                            className="slide"
                                            style={{
                                              width: `${
                                                parseInt(
                                                  data.avg_rating.accuracy
                                                ) * 20
                                              }%`,
                                            }}
                                          ></span>
                                          <input
                                            id="range"
                                            type="range"
                                            min="0"
                                            max="5"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <div className="score score1">
                                      <div className="score-details">
                                        <h6 className="mb-0">
                                          {strings.Communication}
                                          <span>
                                            {parseInt(
                                              data.avg_rating.communication
                                            ) + "/ 5"}
                                          </span>
                                        </h6>
                                      </div>
                                      <div className="score-bar">
                                        <div className="rSlider">
                                          <span
                                            className="slide"
                                            style={{
                                              width: `${
                                                parseInt(
                                                  data.avg_rating.communication
                                                ) * 20
                                              }%`,
                                            }}
                                          ></span>
                                          <input
                                            id="range"
                                            type="range"
                                            min="0"
                                            max="5"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <div className="score score1">
                                      <div className="score-details">
                                        <h6 className="mb-0">
                                          {strings.Location}{" "}
                                          <span>
                                            {parseInt(
                                              data.avg_rating.location
                                            ) + "/ 5"}
                                          </span>
                                        </h6>
                                      </div>
                                      <div className="score-bar">
                                        <div className="rSlider">
                                          <span
                                            className="slide"
                                            style={{
                                              width: `${
                                                parseInt(
                                                  data.avg_rating.location
                                                ) * 20
                                              }%`,
                                            }}
                                          ></span>
                                          <input
                                            id="range"
                                            type="range"
                                            min="0"
                                            max="5"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <div className="score score1 mb-md-0">
                                      <div className="score-details">
                                        <h6 className="mb-0">
                                          {strings.Check_in}{" "}
                                          <span>
                                            {parseInt(
                                              data.avg_rating.check_in
                                            ) + "/ 5"}
                                          </span>
                                        </h6>
                                      </div>
                                      <div className="score-bar">
                                        <div className="rSlider">
                                          <span
                                            className="slide"
                                            style={{
                                              width: `${
                                                parseInt(
                                                  data.avg_rating.check_in
                                                ) * 20
                                              }%`,
                                            }}
                                          ></span>
                                          <input
                                            id="range"
                                            type="range"
                                            min="0"
                                            max="5"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <div className="score score1 mb-0">
                                      <div className="score-details">
                                        <h6 className="mb-0">
                                          {strings.Value}{" "}
                                          <span>
                                            {parseInt(data.avg_rating.value) +
                                              " / 5"}
                                          </span>
                                        </h6>
                                      </div>
                                      <div className="score-bar">
                                        <div className="rSlider">
                                          <span
                                            className="slide"
                                            style={{
                                              width: `${
                                                parseInt(
                                                  data.avg_rating.value
                                                ) * 20
                                              }%`,
                                            }}
                                          ></span>
                                          <input
                                            id="range"
                                            type="range"
                                            min="0"
                                            max="5"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                      {data.avg_rating ? <hr /> : null}
                      <div className="sitter-review-section">
                        {data && data.reviews?.length ? (
                          data.reviews.map((val) => (
                            <ReviewObject
                              value={val}
                              sitterDetails={{
                                firstName: val?.sitter_info?.firstname,
                                lastName: val?.sitter_info?.lastname,
                                profile_picture:
                                  val?.sitter_info?.profile_picture,
                                id: val.sitter_info.id,
                              }}
                              isMe={
                                val.sitter_id == cookie.get("id") ? true : false
                              }
                              updateSitterProfile={() =>
                                getSitterDetails(router.query.id)
                              }
                            />
                          ))
                        ) : (
                          <h5>Reviews are not given yet!</h5>
                        )}

                        {/*<div className="col-12 px-0 d-flex justify-content-between align-items-center">*/}
                        {/*    <div className="read-more">*/}
                        {/*<p className="mb-0"><Link href={{pathname: '/sitter-profile/MyReviews', query: {id: reviews[0].sitter_id}}}>+ Read more reviews</Link></p>*/}
                        {/*</div>*/}
                        {/*</div>*/}
                        {/*<RateSitter showModal={showModal} hideModal={() => setShowModal(false)} />*/}
                      </div>
                    </>
                  ) : (
                    <>
                      <div className="col-12 mb-3">
                        <div className="sitter-review-section">
                          {data && data.owner_reviews?.length ? (
                            data.owner_reviews.map((value) => (
                              <div className="user-review-details">
                                <div className="row mb-2">
                                  <div className="col-12 col-md">
                                    <div className="d-flex">
                                      <div className="sitter-profile-img">
                                        <img
                                          src={
                                            value?.sitter_info?.profile_picture
                                          }
                                          className="img-fluid"
                                        />
                                      </div>
                                      <div className="sitter-review-details ml-2 my-auto">
                                        <h5 className="mb-0 font-medium">
                                          {value?.sitter_info?.firstname +
                                            " " +
                                            value?.sitter_info?.lastname}
                                        </h5>
                                        <p className="font-12 mb-0">
                                          {moment(value.created_at).format(
                                            "DD-MM-YYYY"
                                          ) +
                                            " | " +
                                            moment(value.created_at).format(
                                              "HH:MM a"
                                            )}
                                        </p>
                                        <div className="d-flex rating-star">
                                          <RatingStars rating={value.rating} />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <p className="font-14 ">{value.review}</p>
                                <hr />
                              </div>
                            ))
                          ) : (
                            <h5>{strings.ReviewsAreNotGivenYet}</h5>
                          )}
                        </div>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
          </>
        ) : null}
      </div>
    </div>
  );
};
export default MyReviews;
