import React from "react";
import API from "../../api/Api";
import ExtraInfoLocality from "../../components/sitterProfile/ExtraInfoLocality";
import GalleryView from "../../components/sitterProfile/GalleryView";
import MyPets from "../../components/sitterProfile/MyPets";
import Reviews from "../../components/sitterProfile/Reviews";
import SitterAvailablity from "../../components/sitterProfile/SitterAvailability";
import SitterProfileHeader from "../../components/sitterProfile/SitterProfileHeader";
import SitterServices from "../../components/sitterProfile/SitterServices";
import MyMapComponent from "../../components/user/myProfile/Map";
import { I_SINGLE_SITTER } from "../../models/sitter.interface";
import { strings } from "../../public/lang/Strings";
import { deepClone } from "../../utils/Helper";
import { useRouter, withRouter } from "next/router";
import { GOOGLE_PLACES_API } from "../../api/Constants";
import Geocode from "react-geocode";
import ReportSitterModal from "../../components/common/ReportSitterModal";

interface Iprops {
  data: I_SINGLE_SITTER;
  openSnackbar: any;
  router: any;
  myRef: any;
}

interface Istate {
  data: I_SINGLE_SITTER;
  serviceId: string;
  map_address: string;
  reported: boolean;
  showReportModal: boolean;
}

let api = new API();

class SitterProfile extends React.Component<Iprops, Istate> {
  private myRef;

  constructor(props: Iprops) {
    super(props);
    this.state = {
      data: null,
      serviceId: "",
      map_address: "",
      reported: false,
      showReportModal: false,
    };
    this.myRef = React.createRef();

    this.handleBookmarkSitter = this.handleBookmarkSitter.bind(this);
    // this.getSitterDetails = this.getSitterDetails.bind(this);
    this.coordinates_to_address = this.coordinates_to_address.bind(this);
  }

 
  
  componentDidMount() {
    Geocode.setApiKey(GOOGLE_PLACES_API);
    const { router } = this.props;
    const { id } = router.query;
  
    this.setState({
      data: null,
    });

    if (this.props.data) {
      this.setState({
        data: deepClone(this.props.data),
      });
    }
    let data = deepClone(this.props.data);
    if (data?.address?.map_latitude) {
      this.coordinates_to_address(
        data.address.map_latitude,
        data.address.map_longitude
      );
    }
  
    api.getReportedSitter(id)
      .then((res) => {
        console.log("reported res", res.data.response);
        this.setState({ reported: res.data.response });
        console.log("reported", this.state.reported);
      })
      .catch((error) => console.log(error.response));
   
  }

  extractCityAndPrefecture = (address) => {
    const addressParts = address.split(",");
  
    const prefecture = addressParts[addressParts.length - 2].trim();
  
    const city = addressParts[addressParts.length - 1].trim();
  
    return  city + "  " + prefecture;
  };


  handleBookmarkSitter(data: any) {
    api
      .markUnmarkSitter({ sitter_id: this.state.data.id })
      .then((res) => {
        if (res.data.status === 200) {
          if (this.state.data.is_favorite == 1) {
            alert(strings.unmarkSitter);
          } else {
            alert(strings.bookmarkSitter);
          }
          this.getSitterDetails(this.state.data.id);
        }
      })
      .catch((error) => {
        alert(strings.errorUpdatingStatus);
      });
  }

  coordinates_to_address(lat, lng) {
    Geocode.fromLatLng(lat, lng).then(
      (response) => {
        const address = response;
        if (address) {
          this.setState({
            map_address: address?.results[0]?.formatted_address,
          });
        } else {
          console.log(response);
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
  //
  getSitterDetails(id) {
    api
      .getSingleSitter(id)
      .then((res) => {
        this.setState({ data: res.data.response });
        let data = res.data.response;
        this.coordinates_to_address(
          data.address.map_latitude,
          data.address.map_longitude
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { data, reported, showReportModal } = this.state;
    const { query } = this.props.router;
    const { serviceId } = this.props.router.query;
    if (this.state.data) {
      return (
        <div>
          
          <div className="main-wrapper bottom mt-0">
            <div className="main-image d-none d-md-block">
              <img src="/images/banner2.png" className="img-fluid" alt="" />
            </div>
            <div className="container">
              <SitterProfileHeader
                location={data?.address?.address}
                hide_address={data?.address?.hide_address}
                name={data.firstname + " " + data.lastname}
                isVerified={data.is_verified}
                distance={data?.address?.distance}
                responseRate={data.preference.response_rate}
                responseWithin={data.preference.response_within}
                jobCompletion={data.preference.job_completion}
                isFavorite={data.is_favorite}
                rehireRate={data.preference.rehire_rate ?? 0}
                experience_in_month={data.experience_in_month}
                experience_in_year={data.experience_in_year}
                petTakenCare={data.preference.pets_care}
                happyCustomers={data.preference.happy_customers}
                online={data.preference.online}
                rating={data.overall_rate}
                review={data.total_review}
                id={data.id}
                profile_pic={this.state.data.profile_picture}
                markUnmark={() => this.getSitterDetails(data.id)}
                serviceId={query.serviceId}
                onAvailableClick={() => {
                  this.myRef.current.scrollIntoView({ behavior: "smooth" });
                }}
                isSitter={
                  this.state.data?.active_services?.length ? true : false
                }
              />
            </div>
            <div className="container">
              <div className="row">
                <div className="col-12 col-md-8 col-lg-8 col-xl-8 pr-md-0">
                  {this.state.data?.images?.length ? (
                    <GalleryView images={this.state.data.images} />
                  ) : null}
                  {this.state.data?.description ? (
                    <div className="bg-white main-background">
                      <h5 className="font-semibold mb-0">
                        {strings.Iam + ` ` + this.state.data.firstname}
                      </h5>
                      <hr />
                      <p className="mb-0 text-scrl">
                        {this.state.data.description}
                      </p>
                    </div>
                  ) : null}

                  {this.state.data?.questions?.length ? (
                    <div className="bg-white main-background about-pet">
                      <h5 className="font-semibold mb-0">
                        {` ` +
                          strings.What +
                          " " +
                          this.state.data.firstname +
                          ` ` +
                          strings.wouldLikeToKnowAboutYourPet}
                      </h5>
                      <hr />
                      <div className="booking-for">
                        {this.state.data.questions.map((value, index) => (
                          <li key={index}>{value.question} </li>
                        ))}
                      </div>
                    </div>
                  ) : null}
                  {this.state.data.active_services.length ? (
                    <SitterServices
                      name={this.state.data.firstname}
                      service={this.state.data.active_services}
                    />
                  ) : null}
                  <div className="row">
                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                      {this.state.data?.address ? (
                        <ExtraInfoLocality address={this.state.data.address} />
                      ) : null}
                    </div>
                    {this.state.data?.skills?.length ? (
                      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                        <div className="bg-white main-background about-place">
                          <h5 className="font-semibold mb-0">
                            {strings.AdditionalSkills}
                          </h5>
                          <hr />
                          <div className="skill-height scr-custom">
                            {this.state.data.skills.map((value, index) => (
                              <p
                                className="d-inline-flex align-items-baseline mb-2"
                                key={index}
                              >
                                {value.skill}
                              </p>
                            ))}
                          </div>
                        </div>
                      </div>
                    ) : null}
                  </div>

                  {this.state.data.pets.length ? (
                    <MyPets pets={this.state.data.pets} />
                  ) : null}
                  {data ? (
                    <Reviews
                      reviews={data?.reviews}
                      ownerReview={data?.owner_reviews}
                      average={data?.avg_rating}
                      sitterDetails={{
                        id: data.id,
                        profile_picture: data.profile_picture,
                        firstName: data.firstname,
                        lastName: data.lastname,
                      }}
                      overallRate={data.overall_rate}
                      updateSitterProfile={() =>
                        this.getSitterDetails(this.state.data.id)
                      }
                    />
                  ) : null}
                </div>
                <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                  {this.state.data.active_services.length ? (
                    <div ref={this.myRef}>
                      <SitterAvailablity
                        services={this.state.data.active_services}
                        state={this.state.data}
                        sitterName={data.firstname + " " + data.lastname}
                        sitterId={data.id}
                      />
                    </div>
                  ) : null}
                  {this.state.data.address &&
                  this.state.data.address.map_latitude ? (
                    <div className="bg-white main-background map-short">
                      <h5 className="font-semibold mb-3">{strings.Location}</h5>

                      {this.state.data.address &&
                        this.state.data.address.map_latitude && (
                          <div className="mapouter rounded-lg overflow-hidden">
                            <MyMapComponent
                              draggable={false}
                              latlng={{
                                lat: Number(
                                  this.state.data.address.map_latitude
                                ),
                                lng: Number(
                                  this.state.data.address.map_longitude
                                ),
                              }}
                              defaultCenter={{
                                lat: Number(
                                  this.state.data.address.map_latitude
                                ),
                                lng: Number(
                                  this.state.data.address.map_longitude
                                ),
                              }}
                            />
                          </div>
                        )}
                      {this.state?.data?.address &&
                      this.state?.data?.address?.hide_address == 0 ? (
                        <p className="font-weight-medium text-dark mb-2 mt-3">
                          {/*{this.state.data.address.house_number +*/}
                          {/*" " +*/}
                          {/*this.state.data.address.address}{" "}*/}
                          {this.state?.map_address}
                        </p>
                      ) : (
                        <>
                          <div className="d-flex align-items-center">
                            <p className="font-weight-medium text-dark mt-2">
                              <label>City :</label>{" "}
                
                              {(this.state.data.address  ? this.state?.data?.address?.city + " , "+ (this.state?.data?.address?.prefecture ? this.state?.data?.address?.prefecture : " ") : "" )}
                              {/* {this.state?.data?.address?.address.split(",")[0]} */}
                            </p>
                          </div>
                        </>
                      )}
                    </div>
                  ) : null}
                 
                  <div className="text-center">
                    { serviceId && (
                      <div className={`contact-now  alert ${!reported ? "alert-danger" : "alert alert-secondary"}`} role="alert">
                        {!this.state.reported ? (
                          <>
                  
                          <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="currentColor"
                              className="bi bi-x-circle cursor-pointer"
                              viewBox="0 0 16 16"
                            >
                              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                              <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                            </svg>
                            <span
                              className=" my-0 py-0 small w-100 cursor-pointer"
                              onClick={() => {
                                this.setState({ showReportModal: true });
                              }}
                            >
                              {" "}
                             <b>{strings.ReportThisProfile}</b> 
                            </span>
                           
                            
                          </>
                        ) : (
                          <>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="currentColor"
                              className="bi bi-x-circle"
                              viewBox="0 0 16 16"
                              style={{ cursor: "none" }}
                            >
                              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                              <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                            </svg>
                            <span
                              className="my-0 py-0 small disabled w-100"
                            >
                              {" "}
                              Reported
                            </span>
                          </>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
       
          <ReportSitterModal
            showModal={this.state.showReportModal}
            hideModal={() => this.setState({ showReportModal: false })}
            participantId={this.state.data.id}
            setReported={() =>
              this.setState((prevState) => ({
                reported: !prevState.reported,
              }))
            }
          />
        </div>
      );
    } else {
      return null;
    }
  }

  static async getInitialProps({ query: { id } }) {
    const api = new API();
    let data = null;
    await api
      .getSingleSitter(id)
      .then((res) => {
        data = res.data.response;
      })
      .catch((error) => {
        console.log(error);
      });
    return {
      data: data,
    };
  }
}

export default withRouter(SitterProfile);
