import React from "react";
import { strings } from "../../public/lang/Strings";
import Link from "next/link";
import API from "../../api/Api";
import { deleteCookie } from "../../utils/Helper";
import Router from "next/router";
import Cookies from "universal-cookie";
import FeatherIcon from "feather-icons-react";
import { useSnackbar } from "react-simple-snackbar";
import { EventEmitter } from "../../public/EventEmitter";
import LoginFacebook from "../../components/authentication/FacebookLogin";
import GoogleLoginComponent from "../../components/authentication/GoogleLogin";
import ReactTwitterLogin from "../../components/authentication/TwiiterLogin";
import { errorOptions } from "../../public/appData/AppData";
import Loader from "../../components/common/Loader";
import ReCAPTCHA from "react-google-recaptcha";
import axios from "axios";
const secretKey = process.env.NEXT_PUBLIC_RECAPTCHA_SECRET_KEY;
const siteKey = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;

interface IState {
  email: string;
  password: string;
  rememberMe: boolean;
  errors: any;
  showPass: boolean;
  loader: boolean;
  is_captcha_verified: boolean;
}

interface IProps {
  openError: (val) => void;
}

const cookies = new Cookies();
const api = new API();

class SignIn1 extends React.Component<IProps, IState> {
  private passwordInput: React.RefObject<HTMLInputElement>;
  private recaptchaRef: React.RefObject<any>;

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      rememberMe: false,
      errors: {},
      showPass: false,
      loader: false,
      is_captcha_verified: false,
    };
    this.recaptchaRef = React.createRef();
    this.passwordInput = React.createRef();
    this.onTextChange = this.onTextChange.bind(this);
    this.toggleSecurePassword = this.toggleSecurePassword.bind(this);
    this.checkboxOnChange = this.checkboxOnChange.bind(this);
    this.onReCAPTCHAChange = this.onReCAPTCHAChange.bind(this);
  }

  componentDidMount(): void {
    let email = cookies.get("rem_email");
    let pass = cookies.get("rem_pass");
    if (email && pass) {
      this.setState({ email: email, password: pass, rememberMe: true });
    }
  }

  onTextChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    } as { [K in keyof IState]: IState[K] });
  };

  toggleSecurePassword = () => {
    this.setState({ showPass: !this.state.showPass });
  };

  checkboxOnChange = (event) => {
    this.setState({
      rememberMe: !this.state.rememberMe,
    });
  };

  loginUser = async () => {
    let that = this;
    let arr = [];
    this.setState({
      errors: {},
      loader: true,
    });

    let data = JSON.stringify({
      email: this.state.email,
      password: this.state.password,
      token: cookies.get("fcm_token"),
      platform: "web",
    });

    if (!this.state.email) {
      this.setState((prevState) => ({
        errors: {
          ...prevState.errors,
          email: ["The email field is required"],
        },
        loader: false,
      }));
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }

    if (!this.state.password) {
      this.setState((prevState) => ({
        errors: {
          ...prevState.errors,
          password: ["The Password field is required"],
        },
        loader: false,
      }));
      this.recaptchaRef?.current?.reset();
      this.setState({ is_captcha_verified: false });
      return;
    }
    // if (!this.state.is_captcha_verified) {
    //   this.setState({
    //     errors: { captcha: "Captcha not verified" },
    //     loader: false,
    //   });
    //   return;
    // }
    api
      .loginUser(data)
      .then((json) => {
        if (json.data.status == 200) {
          if (json.data.response?.email_verified_at == null) {
            Router.push(
              {
                pathname: "/verify",
                query: {
                  userData: JSON.stringify(json.data.response),
                  type: 1,
                },
              },
              "/verify"
            ).then((r) => {
              this.setState({ loader: false });
            });
          }
          // else if (json.data.response?.phone_verified_at == null) {
          //   Router.push({
          //     pathname: "/verify",
          //     query: {userData: JSON.stringify(json.data.response), type:  2}
          //   }, '/verify').then(r =>{})
          // }
          else {
            if (this.state?.rememberMe == true) {
              document.cookie = `rem_email=${json.data.response.email}; path=/`;
              document.cookie = `rem_pass=${this.state.password}; path=/`;
            } else {
              deleteCookie("rem_email");
              deleteCookie("rem_pass");
            }
            document.cookie = `id=${json.data.response.id}; path=/`;
            document.cookie = `token=${json.data.response.token}; path=/`;
            document.cookie = `firstname=${json.data.response.firstname}; path=/`;
            document.cookie = `lastname=${json.data.response.lastname}; path=/`;
            document.cookie = `phone_number=${json.data.response.phone_number}; path=/`;
            document.cookie = `profile_picture=${json.data.response.profile_picture}; path=/`;
            document.cookie = `email=${json.data.response.email}; path=/`;
            document.cookie = `get_recent_view=${arr}; path=/`;
            Router.push("/user/dashboard").then((r) => {});
            EventEmitter.dispatch("updateUserDetail", json.data.response);
            this.setState({ loader: false });
          }
        }
      })
      .catch((error) => {
        this.setState({ loader: false });
        if (
          error.response?.data?.response?.email_verified_at == null &&
          error?.response?.data?.message == "Email not verified"
        ) {
          Router.push(
            {
              pathname: "/verify",
              query: {
                userData: JSON.stringify(error.response?.data?.response),
                type: 1,
              },
            },
            "/verify"
          ).then((r) => {});
        }
        if (error.response?.status == 422) {
          that.setState({
            errors: error.response.data.errors,
          });
          this.recaptchaRef?.current?.reset();
          this.setState({ is_captcha_verified: false });
        }

        if (error.response?.status == 400) {
          let errors = this.state.errors;
          errors.message = error.response?.data?.message;
          that.setState({
            errors: errors,
          });
          this.recaptchaRef?.current?.reset();
          this.setState({ is_captcha_verified: false });
        }
      });
  };



  onReCAPTCHAChange = (token) => {

    api.verifyRecaptcha(token)
        .then((response) => {
          if (response.data.success) {
            this.setState({ is_captcha_verified: true });
          } else {
            this.setState({ is_captcha_verified: false });
          }
        })
        .catch((error) => {
          this.setState({ is_captcha_verified: false });
          this.recaptchaRef?.current?.reset();
        });
  };

  render() {
    return (
      <div className="vector">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-md-8 col-lg-6 col-xl-6">
              <div className="bg-white main-content">
                <div className="login-details">
                  <div className="logo-img mb-2">
                    <img
                      src="images/logo.png"
                      className="img-fluid"
                      alt={"logo"}
                    />
                  </div>
                  <p className="font-14">{strings.Welcomeagain}</p>
                </div>
                <div className="login-form">
                  <div className="my-4">
                    <div className="row">
                      <LoginFacebook />
                      <GoogleLoginComponent />
                      {/*<div className="col-12 col-md-6 col-lg-6 col-xl-6">*/}
                      <div className="col-12">
                        <ReactTwitterLogin />
                      </div>
                    </div>
                  </div>
                  <div className="loginOr">
                    <hr className="hrOr" />
                    <span className="spanOr">{strings.or}</span>
                  </div>
                  <form>
                    <div className="row">
                      <div className="col-12 col-md-12 col-lg-12 col-xl-12 col-sm-12">
                        <div className="form-group">
                          <div className="form-label-view">
                            <label>{strings.Emailaddress}</label>
                          </div>

                          <input
                            className={
                              "form-control " +
                              (this.state.errors.email ? "invalid" : "")
                            }
                            id="email"
                            type="email"
                            name="email"
                            value={this.state?.email}
                            placeholder={strings.EnterYourRegisteredEmailID}
                            onChange={this.onTextChange}
                          />
                          {this.state.errors?.email && (
                            <label className={"error-text"}>
                              {this.state.errors?.email[0]}
                            </label>
                          )}
                        </div>
                      </div>
                      <div className="col-12 col-md-12 col-lg-12 col-xl-12 col-sm-12">
                        <div className="form-group login-password">
                          <div className="form-label-view">
                            <label>Password</label>
                          </div>
                          <input
                            ref={this.passwordInput}
                            className={
                              "form-control " +
                              (this.state.errors.password ? "invalid" : "")
                            }
                            id="password"
                            type={this.state.showPass ? "text" : "password"}
                            name="password"
                            value={this.state?.password}
                            placeholder={strings.EnterYourPassword}
                            onChange={this.onTextChange}
                          />
                          <FeatherIcon
                            className="svg-inline--fa fa-eye fa-w-18 fa-2x eye-icon"
                            icon={this.state.showPass ? "eye-off" : "eye"}
                            onClick={this.toggleSecurePassword}
                          />

                          {this.state.errors?.password && (
                            <label className={"error-text"}>
                              {this.state.errors?.password[0]}
                            </label>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-12 col-md-6 col-lg-6 col-xl-6 col-sm-6">
                        <div className="custom-check">
                          <label className="check ">
                            <input
                              onChange={this.checkboxOnChange}
                              type="checkbox"
                              name="is_name1"
                              checked={this.state?.rememberMe}
                            />
                            <span className="checkmark left-0" />
                            {strings.Rememberme}
                          </label>
                        </div>
                      </div>
                      <div className="col-12 col-md-6 col-lg-6 col-xl-6 col-sm-6">
                        <div className="forget-password">
                          <Link href="/forgot-password">
                            <a className="font-14 font-medium">
                              {strings.Forgotpassword}
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                    <div className="d-flex justify-content-center mt-2">
                      <ReCAPTCHA
                        className="mt-2"
                        ref={this.recaptchaRef}
                        sitekey={siteKey}
                        onChange={this.onReCAPTCHAChange}
                      />
                    </div>
                    {this.state.loader ? (
                      <Loader />
                    ) : (
                      <div className="text-center my-3">
                        {this.state.errors?.captcha ? (
                          <label className={"error-text"}>
                            *{this.state.errors.captcha}
                          </label>
                        ) : null}
                        {this.state.errors?.message ? (
                          <label className={"error-text"}>
                            *{this.state.errors?.message}
                          </label>
                        ) : null}
                        <a
                          onClick={this.loginUser.bind(this)}
                          className="btn btn-primary w-100">
                          {strings.Login}
                        </a>
                      </div>
                    )}
                  </form>
                </div>
                <div className="text-center create-account mt-3">
                  <p className="font-12 mb-0 font-medium">
                    {strings.Donthaveanaccount}
                    <Link href="/signup">
                      <a
                        style={{ textDecoration: "underline" }}
                        className="font-medium ">
                        {strings.Registernow}
                      </a>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const SignIn = () => {
  const [openError, closeError] = useSnackbar(errorOptions);
  return <SignIn1 openError={(text) => openError(text)} />;
};
export default SignIn;
