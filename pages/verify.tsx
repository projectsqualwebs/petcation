import React, {useEffect, useState} from "react";
import {strings} from "../public/lang/Strings";
import API from "../api/Api";
import {useSnackbar} from 'react-simple-snackbar';
import Router, {useRouter} from "next/router";
import {deleteCookie, numberInput} from "../utils/Helper";
import {EventEmitter} from "../public/EventEmitter";
import {errorOptions, successOptions} from "../public/appData/AppData";
import ActionLoader from "../components/common/ActionLoader";


interface IState {
    email: string;
}

const api = new API();

const VerifyEmailPhone: React.FC<{}> = (props) => {
    const [email, setEmail] = useState<string>();
    const [userDetail, setUserDetail] = useState<any>();
    const [verificationType, setVerificationType] = useState<any>();
    const [loader, setLoader] = useState<boolean>(false);


    const [resetObj, setResetObj] = useState({
        email: "",
        phone_number: '',
        emailOtp : "",
        mobileOtp : '',
        new_password : "",
        confirm_password : ""
    })
    const router = useRouter();
    const [openSnackbar, closeSnackbar] = useSnackbar(errorOptions);
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [errors, setErrors] = useState<any>({});

    useEffect(()=>{
        if(router.query.userData){
            let user = JSON.parse(String(router.query.userData))
            console.log(router.query.type)
            setUserDetail(user);
            setVerificationType(router.query.type)
        } else {
            router.push('/signin')
        }
    },[]);

    const onTextChange = (event) => {
        setEmail(event.target.value);
        setResetObj({...resetObj, [event.target.id]:event.target.value});
        setErrors({ ...errors, email : null });
    };

    const onResetChange = (event) => {
        setResetObj({...resetObj, [event.target.id]:event.target.value});
        setErrors({ ...errors, [event.target.id]: null });
    };

    const onEmailVerify = (e) => {
        setLoader(true);
        e.preventDefault();
        if (!resetObj.emailOtp) {
            setErrors({ ...errors, otp: strings.enterOtp });
            setLoader(false);
        }else {
            api.verifyEmail({user_id: userDetail.id, otp: resetObj.emailOtp}).then((res) => {
                if (res.data.status == 200) {
                    if (res.data.response?.phone_verified_at == null) {
                        // uncomment below line for mobile verification
                        // setVerificationType(2);

                        // remove below code when we go for validation
                        document.cookie = `id=${res.data.response.id}; path=/`;
                        document.cookie = `token=${res.data.response.token}; path=/`;
                        document.cookie = `firstname=${res.data.response.firstname}; path=/`;
                        document.cookie = `lastname=${res.data.response.lastname}; path=/`;
                        document.cookie = `phone_number=${res.data.response.phone_number}; path=/`;
                        document.cookie = `profile_picture=${res.data.response.profile_picture}; path=/`;
                        document.cookie = `email=${res.data.response.email}; path=/`;
                        document.cookie = `get_recent_view=${[]}; path=/`;
                        openSuccess('Successfully verified email');
                        setTimeout(() => {
                            if (router.query?.signup === 'sitter') {
                                router.push("/user/my-profile").then(r => {setLoader(false)})
                            } else {
                                router.push('/user/dashboard').then(r => {setLoader(false)});
                            }
                        }, 1000)

                    } else {
                        document.cookie = `id=${res.data.response.id}; path=/`;
                        document.cookie = `token=${res.data.response.token}; path=/`;
                        document.cookie = `firstname=${res.data.response.firstname}; path=/`;
                        document.cookie = `lastname=${res.data.response.lastname}; path=/`;
                        document.cookie = `phone_number=${res.data.response.phone_number}; path=/`;
                        document.cookie = `profile_picture=${res.data.response.profile_picture}; path=/`;
                        document.cookie = `email=${res.data.response.email}; path=/`;
                        document.cookie = `get_recent_view=${[]}; path=/`;
                        openSuccess('Successfully verified email');
                        setTimeout(() => {
                            if (router.query?.signup === 'sitter') {
                                router.push("/user/my-profile").then(r => {setLoader(false)})
                            } else {
                                router.push('/user/dashboard').then(r => {setLoader(false)});
                            }
                        }, 1000)
                    }
                } else {
                    openSnackbar(res.data.message);
                    setLoader(false);
                }
            }).catch((error) => {
                if (error.response && error.response.data) {
                    openSnackbar(error.response.data.message)
                    setLoader(false);
                }
               // console.log(error.data);
            })
        }
    }

    const onMobileVerify = (e) => {
        setLoader(true)
        e.preventDefault();
        if (!resetObj.mobileOtp) {
            setErrors({ ...errors, otp: strings.enterOtp });
        }else {
            api.verifyMobile({mobile_number: userDetail.phone_number, otp: resetObj.mobileOtp}).then((res) => {
                if (res.data.status == 200) {
                    document.cookie = `id=${res.data.response.id}; path=/`;
                    document.cookie = `token=${res.data.response.token}; path=/`;
                    document.cookie = `firstname=${res.data.response.firstname}; path=/`;
                    document.cookie = `lastname=${res.data.response.lastname}; path=/`;
                    document.cookie = `phone_number=${res.data.response.phone_number}; path=/`;
                    document.cookie = `profile_picture=${res.data.response.profile_picture}; path=/`;
                    document.cookie = `email=${res.data.response.email}; path=/`;
                    document.cookie = `get_recent_view=${[]}; path=/`;
                    openSuccess('Successfully verified mobile number');
                    setTimeout(()=>{
                        if (router.query?.signup === 'sitter') {
                            router.push("/user/my-profile").then(r => {setLoader(false)})
                        } else {
                            router.push('/user/dashboard').then(r => {setLoader(false)});
                        }
                    }, 1000)
                    EventEmitter.dispatch('updateUserDetail')
                } else {
                    openSnackbar(res.data.message);
                    setLoader(false)
                }
            }).catch((error) => {
                if (error.response && error.response.data) {
                    openSnackbar(error.response.data.message);
                    setLoader(false)
                }
                // console.log(error.data);
            })
        }
    }

    const resendEmailOTP = (e) => {
        e.preventDefault()
        let data = {
            user_id: userDetail?.id
        }
        api
            .resendEmailOTP(data)
            .then((res) => {
                openSuccess(res.data.message)})
            .catch((error) => {
                if (error.response && error.response.data) {
                    openSnackbar(error.response.data.message)
                }
        })
    }
    const resendMobileOTP = (e) => {
        e.preventDefault()
        let data = {
            mobile_number: userDetail?.phone_number ? userDetail?.phone_number : ''
        }
        api
            .resendMobileOTP(data)
            .then((res) => {
                openSuccess(res.data.message)})
            .catch((error) => {
                if (error.response && error.response.data) {
                    openSnackbar(error.response.data.message)
                }
            })
    }

    return (
        <div className="vector">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                        <div className="bg-white main-content text-center pt-4">
                            <div className="login-details">
                                <h5>{verificationType == 1 ? strings.verifyyourEmail:strings.verifyyourPhone}</h5>
                                <p className="font-14">Please enter the verification code sent to your {verificationType == 1 ? userDetail?.email:userDetail?.phone_number}</p>
                            </div>
                            <div className="login-form">
                                <form>
                                    <div>
                                        <div className="form-group">
                                            <label>{strings.otp}</label>
                                            {verificationType == 1 ? <input
                                                onChange={onResetChange}
                                                value={resetObj.emailOtp}
                                                className="form-control text-center"
                                                id="emailOtp"
                                                type="text"
                                                maxLength={5}
                                                onKeyPress={numberInput}
                                            /> : <input
                                                onChange={onResetChange}
                                                value={resetObj.mobileOtp}
                                                className="form-control text-center"
                                                id="mobileOtp"
                                                type="number"
                                                maxLength={11}
                                                onKeyPress={numberInput}
                                            />}
                                            {errors && errors.otp ? (
                                                <span
                                                    style={{
                                                        color: "#ff0000",
                                                        fontSize: "12px",
                                                    }}
                                                > {errors.otp}
                                             </span>
                                            ) : null}
                                        </div>
                                        <div className="text-center mt-3 mb-0">
                                            {loader ? <ActionLoader/> : <button onClick={verificationType == 1 ? onEmailVerify : onMobileVerify} className="btn btn-primary w-100 ">
                                                {verificationType == 1 ? strings.verifyEmail:strings.verifyPhone}
                                            </button>}
                                            <button onClick={verificationType == 1 ? resendEmailOTP : resendMobileOTP} className="btn btn-accept w-100 ">
                                                {strings.resendOtp}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default VerifyEmailPhone;
