import React, { useState, useEffect } from "react";
import { strings } from "../../public/lang/Strings";
import Loader from "../../components/common/Loader";
import API from "../../api/Api";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import moment from "moment";

const api = new API();
const Blog = () => {
  const router = useRouter();
  const [press, setPress] = useState(true);
  const [pressData, setPressData] = useState([]);
  const [eventData, setEventData] = useState([]);
  const [currentTab, setCurrentTab] = useState(0);
  const [categories, setCategories] = useState("");
  const [categoriesData, setCategoriesData] = useState([]);
  const [loading, setLoading] = useState(false);
  // const [data, setData] = useState<I_DATA>()
  const [lang, setLang] = useState("en");
  const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;

  useEffect(() => {
    let myLang = localStorage.getItem("language");
    if (myLang) {
      setLang(myLang);
    }
    setCurrentTab(0);
    SingleCate(0);
  }, []);

  const SingleCate = (value) => {
    setLoading(true);
    api
      .getSingleCategory(value)
      .then((response) => {
        setCategoriesData(response.data.response);
        console.log("categories data", response.data.response);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        console.log(error);
      });
  };

  const seLanguageContent = async (newsFeed, myLang) => {
    if (myLang == "en" && newsFeed?.length) {
      let engData = [];
      await newsFeed?.map(
        async (myData) =>
          await engData.push({
            banner_image : myData.banner_image,
            id: myData?.id,
            title: myData?.title_en,
            created_at: myData?.created_at,
            description: myData?.description_en,
            category: {
              name: myData?.category?.name ?? myData?.category?.name_en,
            },
          })
      );
      setPressData(engData);
    }
    if (myLang == "jp" && newsFeed?.length) {
      let jpData = [];
      await newsFeed?.map(
        async (myData) =>
          await jpData.push({
            id: myData?.id,
            title: myData?.title_jp,
            created_at: myData?.created_at,
            description: myData?.description_jp,
            category: {
              name: myData?.category?.name ?? myData?.category?.name_jp,
            },
          })
      );
      setPressData(jpData);
    }
    if (myLang == "zh" && newsFeed?.length) {
      let zhData = [];
      await newsFeed?.map(
        async (myData) =>
          await zhData.push({
            id: myData?.id,
            title: myData?.title_zh,
            created_at: myData?.created_at,
            description: myData?.description_zh,
            category: {
              name: myData?.category?.name ?? myData?.category?.name_zh,
            },
          })
      );
      setPressData(zhData);
    }
  };

  useEffect(() => {
    const getNews = () => {
      api
        .getLandingPageNewsRoom()
        .then((response) => {
          seLanguageContent(response.data.response?.data, lang).then((r) => {});
          console.log("get news", response.data.response);
        })
        .catch((error) => console.log(error));
    };
    api
      .getCategoryBlog()
      .then((response) => {
        seLanguageContent(response.data.response?.data, lang).then((r) => {});
        setCategories(response.data.response);
      })
      .catch((error) => console.log("failed", error));
    getNews();
  }, []);

  const tabChange = (id) => {
    SingleCate(id);
  };

  const LongText = ({ pathname, content, limit, id }) => {
    if (content.length <= limit) {
      return <div>{content}</div>;
    }
   
    const toShow = content.substring(3, limit) + "...";
    return (
      <div>
        {toShow}
        <Link href={pathname}>
          <a className="text-primary">{strings.ReadMore}</a>
        </Link>
      </div>
    );
  };

  return (
    <>
      <div>
        <div className="main-wrapper mt-0">
          <div className="padding">
            <div className="container">
            
              <div className="row row-direction">
                <div className="col-12 col-md-5 col-lg-4 col-xl-4 my-auto">
                  <div className="blog-day">
                    <p className="blog-date">
                      {moment(pressData[0]?.created_at)?.format("DD-MM-YYYY")}
                    </p>
                  </div>
                  <div className="blog-content">
                    <h4 className="font-semibold">{pressData[0]?.title.trim()}</h4>
                     {console.log("press data",pressData)}
                    <Link href={"/news/" + pressData[0]?.id}>
                      <a>{strings.ReadMore}...</a>
                    </Link>
                  </div>
                </div>
                <div className="col-12 col-md-7 col-lg-8 col-xl-8">
                  <div className="blog-img">
                    <img src={pressData[0]?.banner_image? IMAGE_BASE_URL + pressData[0]?.banner_image : "/images/images.png"} className="img-fluid" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="booking-sitter">
            <div className="padding">
              <div className="container">
                <div className="row">
                  <div className="col-12 col-md-8 col-lg-8 col-xl-9 blog-border">
                    <div className="latest-news">
                      <h4 className="font-semibold mb-4">
                        {strings.LatestNews}
                      </h4>
                      <div className="row">
                        {pressData?.map((news, index) => {
                          if (index == 0) {
                            return "";
                          } else {
                            return (
                              <div className="col-12 col-md-6 col-lg-6 col-xl-6 mb-2">
                                <div className="find-spot d-flex mb-3">
                                  <div className="spot-img blog-spot h-auto">
                                    <img
                                      src={news?.banner_image? IMAGE_BASE_URL + news?.banner_image : "/images/images.png"}
                                      className="img-fluid h-auto"
                                    />
                                  </div>
                                  <div className="ml-3 my-auto">
                                    <h6 className="mb-2 font-semibold">
                                      <Link href={"/news/" + news.id}>
                                        <a>{news?.title}</a>
                                      </Link>
                                    </h6>
                                    <div className="blog-day">
                                      <p className="blog-date mb-0">
                                        {moment(news.created_at)?.format(
                                          "DD-MM-YYYY"
                                        )}
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }
                        })}
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-md-4 col-lg-4 col-xl-3">
                    <div className="follow-us blog-follow">
                      <h4 className="font-semibold mb-4">{strings.FollowUs}</h4>
                      <p>{strings.GetTheLatestNewsAndTravelInspiration}</p>
                      <div className="follow-details ">
                        <ul className="list-group list-group-horizontal">
                          <li>
                            <a href="#">
                              <svg
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="far"
                                data-icon="basketball-ball"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 496 512"
                                className="svg-inline--fa fa-basketball-ball fa-w-16 fa-2x"
                              >
                                <path
                                  fill="currentColor"
                                  d="M248 8C111 8 0 118.9 0 256c0 137.9 111.6 248 248 248 136.2 0 248-110 248-248C496 119 385.2 8 248 8zm-13.9 447.3c-38.9-2.7-77.1-16.7-109.4-42L248 290l43 43c-29.2 35.1-48.9 77.4-56.9 122.3zm91.5-87.7l45.7 45.7c-26.1 20.5-56.1 33.6-87.2 39.3 7.3-31.1 21.6-59.9 41.5-85zm34-33.9c25-20 53.9-34.2 85.1-41.5-5.8 31.9-19.2 61.7-39.4 87.3l-45.7-45.8zm87.7-91.6c-45 8.1-87.2 27.8-122.4 57l-43-43 123.3-123.4c24.8 31.4 39.4 69.2 42.1 109.4zM139 181c-25.8 20.6-55.8 35-88.1 42.1 5.5-33 19-63.9 39.8-90.4L139 181zm-14.3-82.3C151.1 77.9 182 64.4 215 58.9c-7.1 32.3-21.5 62.3-42.1 88.1l-48.2-48.3zm140.2-41.9c39.1 3.3 75.8 17.8 106.4 41.9L248 222.1l-40.4-40.4c29.7-35.8 49.6-78.9 57.3-124.9zM48.8 273c46-7.8 89.1-27.6 124.8-57.3l40.4 40.4L90.7 379.4C66.6 348.7 52.1 312 48.8 273z"
                                  className=""
                                />
                              </svg>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <svg
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="fab"
                                data-icon="facebook"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512"
                                className="svg-inline--fa fa-facebook fa-w-16 fa-2x"
                              >
                                <path
                                  fill="currentColor"
                                  d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"
                                  className=""
                                />
                              </svg>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <svg
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="fab"
                                data-icon="youtube"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 576 512"
                                className="svg-inline--fa fa-youtube fa-w-18 fa-2x"
                              >
                                <path
                                  fill="currentColor"
                                  d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"
                                  className=""
                                />
                              </svg>
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <svg
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="fab"
                                data-icon="twitter"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512"
                                className="svg-inline--fa fa-twitter fa-w-16 fa-2x"
                              >
                                <path
                                  fill="currentColor"
                                  d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"
                                  className=""
                                />
                              </svg>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/*------------------categories section--------------------*/}
          <div className="padding">
            <div className="container">
              <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  <h4 className="font-semibold mb-4">{strings.Categories}</h4>
                  <div className="tabs-main main-background">
                    <div className="row tabs-design">
                      <div className="col-12 px-0 px-md-3">
                        <div className="pay-tabs">
                          <ul
                            className="nav nav-tabs mb-0"
                            id="myTab"
                            role="tablist"
                          >
                            <li className="nav-item" role="presentation">
                              <a
                                className={
                                  "nav-link cursor-pointer " +
                                  (currentTab === 0 ? "active" : "")
                                }
                                id="request-tab"
                                data-toggle="tab"
                                role="tab"
                                onClick={() => {
                                  setCurrentTab(0);
                                  tabChange(0);
                                }}
                              >
                                {strings.All}
                              </a>
                            </li>
                            {categories
                              ? categories.map((item, index) => {
                                  return (
                                    <li
                                      className="nav-item"
                                      role="presentation"
                                    >
                                      <a
                                        className={
                                          "nav-link cursor-pointer " +
                                          (currentTab === item.id
                                            ? "active"
                                            : "")
                                        }
                                        id="request-tab"
                                        data-toggle="tab"
                                        role="tab"
                                        onClick={() => {
                                          setCurrentTab(item.id);
                                          tabChange(item.id);
                                        }}
                                      >
                                        {item.name}
                                      </a>
                                    </li>
                                  );
                                })
                              : ""}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white main-background pending-request">
                    <div className="tab-content" id="myTabContent">
                      <div
                        className={
                          "tab-pane fade" +
                          (currentTab === 0 ? " show active" : "")
                        }
                        id="request"
                        role="tabpanel"
                      >
                        <div className="row">
                          {loading ? (
                            <div className="d-flex col-12 p-3 align-items-center justify-content-center">
                              <Loader />
                            </div>
                          ) : categoriesData.length > 0 ? (
                            categoriesData.map((press) => (
                              <div
                                className="col-12 col-md-3 col-lg-3 col-xl-3"
                                key={press.id}
                              >
                                
                                <div className="find-spot blog-margin">
                                <div className="spot-img mb-2">
                                <img
                                  src={press?.banner_image? IMAGE_BASE_URL + press?.banner_image : "/images/images.png"}
                                  className="img-fluid"
                                  alt=""
                                />
                              </div>

                                  <h6 className="mb-0 font-semibold">
                                    <Link href={"/news/" + press.id}>
                                      <a>{press.title_en}</a>
                                    </Link>
                                  </h6>
                                  <div className="blog-day">
                                    <p className="blog-date mb-0">
                                      {moment(press.created_at).format(
                                        "DD-MM-YYYY"
                                      )}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            ))
                          ) : (
                            <div className="d-flex col-12 p-3 align-items-center justify-content-center">
                              <p className="p-2 mb-0">No news found.</p>
                            </div>
                          )}
                        </div>
                      </div>
                      {categories
                        ? categories.map((item, index) => {
                            return (
                              <div
                                className={
                                  "tab-pane fade" +
                                  (currentTab === item.id ? " show active" : "")
                                }
                                id="request"
                                role="tabpanel"
                              >
                                <div className="row">
                                  {loading ? (
                                    <div className="d-flex col-12 p-3 align-items-center justify-content-center">
                                      <Loader />
                                    </div>
                                  ) : categoriesData.length > 0 ? (
                                    categoriesData.map((press) => (
                                      <div
                                        className="col-12 col-md-3 col-lg-3 col-xl-3"
                                        key={press.id}
                                      >
                                        <div className="find-spot blog-margin">
                                        <div className="spot-img mb-2">
                                <img
                                  src={press?.banner_image? IMAGE_BASE_URL + press?.banner_image : "/images/images.png"}
                                  className="img-fluid"
                                  alt=""
                                />
                                </div>
                                          <h6 className="mb-0 font-semibold">
                                            <Link href={"/news/" + press.id}>
                                              <a>{press.title_en}</a>
                                            </Link>
                                          </h6>
                                          <div className="blog-day">
                                            <p className="blog-date mb-0">
                                              {moment(press.created_at).format(
                                                "DD-MM-YYYY"
                                              )}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    ))
                                  ) : (
                                    <div className="d-flex col-12 p-3 align-items-center justify-content-center">
                                      <p className="p-2 mb-0">No news found.</p>
                                    </div>
                                  )}
                                </div>
                              </div>
                            );
                          })
                        : ""}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/*--------------news letter section----------------*/}
          <div className="booking-sitter">
            <div className="padding">
              <div className="container">
                <div className="row">
                  <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                    <div className="image">
                      <img src="/images/img1.png" className="img-fluid" />
                    </div>
                  </div>
                  <div className="col-12 col-md-6 col-lg-6 col-xl-6 my-auto">
                    <div className="main-title partner-title text-center mb-0">
                      <h2 className="font-semibold mb-3">
                        {strings.SignUpForOurNewsletter}
                      </h2>
                      <p className="mb-3">
                        {
                          strings.EnterYourEmailToReceiveTheLatestNewsAndUpdatesFromPetcationNews
                        }
                      </p>
                      <button className="btn btn-primary">
                        {strings.Subscribe}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Blog;
