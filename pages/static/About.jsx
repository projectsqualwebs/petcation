import React, {useEffect, useRef, useState} from "react";
import {strings} from "../../public/lang/Strings";
import {useRouter} from "next/router";
import API from "../../api/Api";
const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;

const About = ({data}) => {
    const [content, setContent] = useState()
    const [lang, setLang] = useState('en')

    useEffect(async () => {
        let myLang = await localStorage.getItem('language')
        if (myLang) {
            setLang(myLang)
        }
    }, [])

    useEffect(() => {
        seLanguageContent(data?.aboutUs, lang)
    }, [data?.aboutUs, lang])

    const seLanguageContent = (myData, myLang) => {
        let engData = {
            id: myData?.id,
            slug: myData?.slug,
            page_title: myData?.page_title_en,
            page_tagline: myData?.page_tagline_en,
            background_image: myData?.background_image,
            section_title: myData?.section_title_en,
            section_description: myData?.section_description_en,
            section2_title: myData?.section2_title_en,
            section2_tagline: myData?.section2_tagline_en,
            section3_title: myData?.section3_title_en,
            section3_description: myData?.section3_description_en,
            section3_button_text: myData?.section3_button_text_en,
            section3_button_link: myData?.section3_button_link,
            section3_image: myData?.section3_image,
            created_at: myData?.created_at,
            updated_at: myData?.updated_at
        }

        let jpData = {
            id: myData?.id,
            slug: myData?.slug,
            page_title: myData?.page_title_jp,
            page_tagline: myData?.page_tagline_jp,
            background_image: myData?.background_image,
            section_title: myData?.section_title_jp,
            section_description: myData?.section_description_jp,
            section2_title: myData?.section2_title_jp,
            section2_tagline: myData?.section2_tagline_jp,
            section3_title: myData?.section3_title_jp,
            section3_description: myData?.section3_description_jp,
            section3_button_text: myData?.section3_button_text_jp,
            section3_button_link: myData?.section3_button_link,
            section3_image: myData?.section3_image,
            created_at: myData?.created_at,
            updated_at: myData?.updated_at
        }

        let zhData = {
            id: myData?.id,
            slug: myData?.slug,
            page_title: myData?.page_title_zh,
            page_tagline: myData?.page_tagline_zh,
            background_image: myData?.background_image,
            section_title: myData?.section_title_zh,
            section_description: myData?.section_description_zh,
            section2_title: myData?.section2_title_zh,
            section2_tagline: myData?.section2_tagline_zh,
            section3_title: myData?.section3_title_zh,
            section3_description: myData?.section3_description_zh,
            section3_button_text: myData?.section3_button_text_zh,
            section3_button_link: myData?.section3_button_link,
            section3_image: myData?.section3_image,
            created_at: myData?.created_at,
            updated_at: myData?.updated_at
        }

        if (myLang == 'en') {
            setContent(engData)
        }
        if (myLang == 'jp') {
            setContent(jpData)
        }
        if (myLang == 'zh') {
            setContent(zhData)
        }

    }

    const router = useRouter()
    return (
        <>
            <div className="main-page">
                <div className="main-wrapper mt-0">
                    <div className="career-position static-banner">
                        <div className="main-image mb-4">
                            <img src={IMAGE_BASE_URL+content?.background_image} className="img-fluid" alt="" />
                        </div>
                        {/*-------/1st section----*/}
                        {/*---------career section--------------*/}
                        <div className="main-banner career-banner">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12 col-md-9 col-lg-8 col-xl-8 mx-auto">
                                        <div className="career-details text-center">
                                            <div className="text-center main-title mb-0">
                                                <h2 className="font-semibold">{content?.page_title}</h2>
                                                <p className="">{content?.page_tagline}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*-------/1st section----*/}
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                {/*-------2nd section----*/}
                                <div className="bg-white main-background mb-5">
                                    <div className="about-details p-3">
                                        <div className="text-center main-title mb-4">
                                            <h2 className="font-semibold">{content?.section_title}</h2>
                                        </div>
                                        <div dangerouslySetInnerHTML={{ __html: content?.section_description}} />
                                    </div>
                                </div>
                                {/*-------/2nd section----*/}
                                {/*-------3rd section----*/}
                                <div className="col-12">
                                    <div className="col-12 million-service py-5">
                                        <div className="text-center main-title mb-2">
                                            <h2 className="font-semibold">{content?.section2_title}</h2>
                                            <p className="mb-0">{content?.section2_tagline}</p>
                                        </div>
                                        <div className="row">
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fad"
                                                            data-icon="user-check"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 640 512"
                                                            className="svg-inline--fa fa-user-check fa-w-20 fa-2x"
                                                        >
                                                            <g className="fa-group">
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M636.6 159.6a12 12 0 0 1-.1 16.8L495.2 316.6a11.86 11.86 0 0 1-16.8-.1l-81.7-82.3a11.86 11.86 0 0 1 .1-16.8l28.1-27.9a11.86 11.86 0 0 1 16.8.1l45.5 45.8 104.8-104a11.86 11.86 0 0 1 16.8.1z"
                                                                    className="fa-secondary"
                                                                />
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M224 256A128 128 0 1 0 96 128a128 128 0 0 0 128 128zm89.6 32h-16.7a174.08 174.08 0 0 1-145.8 0h-16.7A134.43 134.43 0 0 0 0 422.4V464a48 48 0 0 0 48 48h352a48 48 0 0 0 48-48v-41.6A134.43 134.43 0 0 0 313.6 288z"
                                                                    className="fa-primary"
                                                                />
                                                            </g>
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.VerifiedReviewsByPetParents}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fas"
                                                            data-icon="address-card"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 576 512"
                                                            className="svg-inline--fa fa-address-card fa-w-18 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M528 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-352 96c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zm112 236.8c0 10.6-10 19.2-22.4 19.2H86.4C74 384 64 375.4 64 364.8v-19.2c0-31.8 30.1-57.6 67.2-57.6h5c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h5c37.1 0 67.2 25.8 67.2 57.6v19.2zM512 312c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-64c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-64c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.SitterBackgroundOrIdentityChecks}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fas"
                                                            data-icon="handshake"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 640 512"
                                                            className="svg-inline--fa fa-handshake fa-w-20 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M434.7 64h-85.9c-8 0-15.7 3-21.6 8.4l-98.3 90c-.1.1-.2.3-.3.4-16.6 15.6-16.3 40.5-2.1 56 12.7 13.9 39.4 17.6 56.1 2.7.1-.1.3-.1.4-.2l79.9-73.2c6.5-5.9 16.7-5.5 22.6 1 6 6.5 5.5 16.6-1 22.6l-26.1 23.9L504 313.8c2.9 2.4 5.5 5 7.9 7.7V128l-54.6-54.6c-5.9-6-14.1-9.4-22.6-9.4zM544 128.2v223.9c0 17.7 14.3 32 32 32h64V128.2h-96zm48 223.9c-8.8 0-16-7.2-16-16s7.2-16 16-16 16 7.2 16 16-7.2 16-16 16zM0 384h64c17.7 0 32-14.3 32-32V128.2H0V384zm48-63.9c8.8 0 16 7.2 16 16s-7.2 16-16 16-16-7.2-16-16c0-8.9 7.2-16 16-16zm435.9 18.6L334.6 217.5l-30 27.5c-29.7 27.1-75.2 24.5-101.7-4.4-26.9-29.4-24.8-74.9 4.4-101.7L289.1 64h-83.8c-8.5 0-16.6 3.4-22.6 9.4L128 128v223.9h18.3l90.5 81.9c27.4 22.3 67.7 18.1 90-9.3l.2-.2 17.9 15.5c15.9 13 39.4 10.5 52.3-5.4l31.4-38.6 5.4 4.4c13.7 11.1 33.9 9.1 45-4.7l9.5-11.7c11.2-13.8 9.1-33.9-4.6-45.1z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.MeetAndGreetsToFindThePerfectFit}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="far"
                                                            data-icon="user-check"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 640 512"
                                                            className="svg-inline--fa fa-user-check fa-w-20 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M637 161.1l-19.1-19.2c-4-4.1-10.6-4.1-14.6 0L500.2 245.6l-47.4-47.7c-4-4.1-10.6-4.1-14.6 0L419 217.1c-4 4.1-4 10.6 0 14.7l73.8 74.3c4 4.1 10.6 4.1 14.6 0L637 175.8c4-4 4-10.6 0-14.7zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96zm89.6 256c-28.8 0-42.4 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.ReservationProtectionForEveryBooking}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fas"
                                                            data-icon="user-headset"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 448 512"
                                                            className="svg-inline--fa fa-user-headset fa-w-14 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M320 352h-23.1a174.08 174.08 0 0 1-145.8 0H128A128 128 0 0 0 0 480a32 32 0 0 0 32 32h384a32 32 0 0 0 32-32 128 128 0 0 0-128-128zM48 224a16 16 0 0 0 16-16v-16c0-88.22 71.78-160 160-160s160 71.78 160 160v16a80.09 80.09 0 0 1-80 80h-32a32 32 0 0 0-32-32h-32a32 32 0 0 0 0 64h96a112.14 112.14 0 0 0 112-112v-16C416 86.13 329.87 0 224 0S32 86.13 32 192v16a16 16 0 0 0 16 16zm160 0h32a64 64 0 0 1 55.41 32H304a48.05 48.05 0 0 0 48-48v-16a128 128 0 0 0-256 0c0 40.42 19.1 76 48.35 99.47-.06-1.17-.35-2.28-.35-3.47a64.07 64.07 0 0 1 64-64z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.SupportForPetParentsAndSitters}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="far"
                                                            data-icon="user-check"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 640 512"
                                                            className="svg-inline--fa fa-user-check fa-w-20 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M637 161.1l-19.1-19.2c-4-4.1-10.6-4.1-14.6 0L500.2 245.6l-47.4-47.7c-4-4.1-10.6-4.1-14.6 0L419 217.1c-4 4.1-4 10.6 0 14.7l73.8 74.3c4 4.1 10.6 4.1 14.6 0L637 175.8c4-4 4-10.6 0-14.7zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96zm89.6 256c-28.8 0-42.4 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.VetAdviceForSittersDuringBookings}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fas"
                                                            data-icon="users-cog"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 640 512"
                                                            className="svg-inline--fa fa-users-cog fa-w-20 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M610.5 341.3c2.6-14.1 2.6-28.5 0-42.6l25.8-14.9c3-1.7 4.3-5.2 3.3-8.5-6.7-21.6-18.2-41.2-33.2-57.4-2.3-2.5-6-3.1-9-1.4l-25.8 14.9c-10.9-9.3-23.4-16.5-36.9-21.3v-29.8c0-3.4-2.4-6.4-5.7-7.1-22.3-5-45-4.8-66.2 0-3.3.7-5.7 3.7-5.7 7.1v29.8c-13.5 4.8-26 12-36.9 21.3l-25.8-14.9c-2.9-1.7-6.7-1.1-9 1.4-15 16.2-26.5 35.8-33.2 57.4-1 3.3.4 6.8 3.3 8.5l25.8 14.9c-2.6 14.1-2.6 28.5 0 42.6l-25.8 14.9c-3 1.7-4.3 5.2-3.3 8.5 6.7 21.6 18.2 41.1 33.2 57.4 2.3 2.5 6 3.1 9 1.4l25.8-14.9c10.9 9.3 23.4 16.5 36.9 21.3v29.8c0 3.4 2.4 6.4 5.7 7.1 22.3 5 45 4.8 66.2 0 3.3-.7 5.7-3.7 5.7-7.1v-29.8c13.5-4.8 26-12 36.9-21.3l25.8 14.9c2.9 1.7 6.7 1.1 9-1.4 15-16.2 26.5-35.8 33.2-57.4 1-3.3-.4-6.8-3.3-8.5l-25.8-14.9zM496 368.5c-26.8 0-48.5-21.8-48.5-48.5s21.8-48.5 48.5-48.5 48.5 21.8 48.5 48.5-21.7 48.5-48.5 48.5zM96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm224 32c1.9 0 3.7-.5 5.6-.6 8.3-21.7 20.5-42.1 36.3-59.2 7.4-8 17.9-12.6 28.9-12.6 6.9 0 13.7 1.8 19.6 5.3l7.9 4.6c.8-.5 1.6-.9 2.4-1.4 7-14.6 11.2-30.8 11.2-48 0-61.9-50.1-112-112-112S208 82.1 208 144c0 61.9 50.1 112 112 112zm105.2 194.5c-2.3-1.2-4.6-2.6-6.8-3.9-8.2 4.8-15.3 9.8-27.5 9.8-10.9 0-21.4-4.6-28.9-12.6-18.3-19.8-32.3-43.9-40.2-69.6-10.7-34.5 24.9-49.7 25.8-50.3-.1-2.6-.1-5.2 0-7.8l-7.9-4.6c-3.8-2.2-7-5-9.8-8.1-3.3.2-6.5.6-9.8.6-24.6 0-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h255.4c-3.7-6-6.2-12.8-6.2-20.3v-9.2zM173.1 274.6C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.ATeamOfTrustAndSafetyExperts}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                                                <div className="col-12 main-section px-3 py-4 rounded-lg about-us-content">
                                                    <div className="about-img">
                                                        <svg
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fas"
                                                            data-icon="book-open"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 576 512"
                                                            className="svg-inline--fa fa-book-open fa-w-18 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M542.22 32.05c-54.8 3.11-163.72 14.43-230.96 55.59-4.64 2.84-7.27 7.89-7.27 13.17v363.87c0 11.55 12.63 18.85 23.28 13.49 69.18-34.82 169.23-44.32 218.7-46.92 16.89-.89 30.02-14.43 30.02-30.66V62.75c.01-17.71-15.35-31.74-33.77-30.7zM264.73 87.64C197.5 46.48 88.58 35.17 33.78 32.05 15.36 31.01 0 45.04 0 62.75V400.6c0 16.24 13.13 29.78 30.02 30.66 49.49 2.6 149.59 12.11 218.77 46.95 10.62 5.35 23.21-1.94 23.21-13.46V100.63c0-5.29-2.62-10.14-7.27-12.99z"
                                                                className=""
                                                            />
                                                        </svg>
                                                    </div>
                                                    <p className="state-label mb-0">
                                                        {strings.OngoingSitterEducationResources}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/*-------/3rd section----*/}
                                <div className="col-12">
                                    <div className="col-12 py-5">
                                        <div className="row">
                                            <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                                <div className="about-details">
                                                    <div className="main-title partner-title mb-4 mt-3">
                                                        <h2 className="font-semibold">{content?.section3_title}</h2>
                                                    </div>
                                                    <div dangerouslySetInnerHTML={{ __html: content?.section3_description}} />
                                                    <div className="my-4 sitter-btn">
                                                        <button onClick={() => router.push('/search-sitter')} className="btn btn-primary py-2 px-4">{content?.section3_button_text}
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                                <div className="about-img1 text-center">
                                                    <div className="about-img1 text-center p-3 main-background bg-white rounded-15">
                                                        <img src={IMAGE_BASE_URL+content?.section3_image} className="img-fluid rounded-15" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>

    );

};

About.getInitialProps = async (ctx) => {
    let data = {};
    const api = new API();
    await api
        .getAboutUsPage()
        .then(async (res) => {
            data = await res.data.response
        })
    return { data: data }
}

export default About;
