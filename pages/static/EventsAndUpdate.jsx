import React, { useEffect, useState } from 'react';
import API from '../../api/Api'
import Link from 'next/link';
import {useRouter} from "next/router";
import moment from "moment";
import {strings} from "../../public/lang/Strings";

const api = new API();
const EventsAndUpdate = () => {
    const router = useRouter();
    const [press, setPress] = useState(true);
    const [pressData, setPressData] = useState([]);
    const [eventData, setEventData] = useState([]);
    // const [data, setData] = useState<I_DATA>()
    const [lang, setLang] = useState('en');

    useEffect( () => {
        let myLang =  localStorage.getItem('language');
        if (myLang) {
            setLang(myLang)
        }
    }, []);

    const seLanguageContent = async (newsFeed, myLang) => {
        if (myLang == 'en' && newsFeed?.length) {
            let engData = [];
            await newsFeed?.map(async (myData) => await engData.push({
                id: myData?.id,
                title: myData?.title_en,
                created_at: myData?.created_at,
                description: myData?.description_en,
                category: {
                    name: myData?.category?.name ?? myData?.category?.name_en,
                }
            }));
            setPressData(engData)

        }
        if (myLang == 'jp' && newsFeed?.length) {
            let jpData = [];
            await newsFeed?.map(async (myData) => await jpData.push({
                id: myData?.id,
                title: myData?.title_jp,
                created_at: myData?.created_at,
                description: myData?.description_jp,
                category: {
                    name: myData?.category?.name ?? myData?.category?.name_jp,
                },
            }));
            setPressData(jpData);
        }
        if (myLang == 'zh' && newsFeed?.length) {
            let zhData = [];
            await newsFeed?.map(async (myData) => await zhData.push({
                id: myData?.id,
                title: myData?.title_zh,
                created_at: myData?.created_at,
                description: myData?.description_zh,
                category: {
                    name: myData?.category?.name ?? myData?.category?.name_zh,
                },
            }));
            setPressData(zhData);
        }
    };

    useEffect(() => {
        const getEvents = () => {
            api.getEvents()
                .then((response) => {
                    // console.log('get envents', response.data.response);
                    setEventData(response.data.response);
                    // console.log(eventData)
                })
                .catch((error) => console.log(error));
        };
        getEvents();
    }, []);

    const LongText = ({ pathname, content,limit, id }) => {
        if (content.length <= limit) {
            // there is nothing more to show
            return <div>{content}</div>
        }
        // In the final case, we show a text with ellipsis and a `Read more` button
        const toShow = content.substring(3, limit) + "...";
        return <div>
            {toShow}
            <Link href={pathname}><a className='text-primary'>{strings.ReadMore}</a></Link>
        </div>
    };

    return (
        <>
            <div>
                <div className="main-wrapper">
                    <div className="booking-sitter py-2">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                    <div className="single-news single-service">
                                        <nav aria-label="breadcrumb">
                                            <ol className="breadcrumb">
                                                <li className="breadcrumb-item">
                                                    {" "}
                                                    <Link href="/">
                                                        {strings.Home}
                                                    </Link>
                                                </li>
                                                <li className="breadcrumb-item active" aria-current="page">
                                                {strings.EventsAndUpdates}
                                                </li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container news-sec">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <div className="single-news-info release-padding mt-2 mb-4">
                                    <h3 className="font-semibold mb-0">{strings.EventsAndUpdates}</h3>
                                </div>
                                <div className="row tabs-design">
                                    <div className="col-12">
                                        <div className="col-12 bg-white main-background">
                                            <div className="release-content py-3">
                                                {eventData.map((content) => <>
                                                    <div className="col-12 mb-3">
                                                        <div className="row">
                                                            <div className="col-12 col-md-4 col-lg-4 col-xl-3 pl-0">
                                                                <div className="col-12 release-date border-right h-100 pl-0">
                                                                    <h6 className="mb-0 font-14 bg-light px-3 py-2 rounded-lg border-secondary">{content.event_date}</h6>
                                                                </div>
                                                            </div>
                                                            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
                                                                <div className="news-details">
                                                                    <Link href={{ pathname: "/static/Single-News-Event/", query: { id: content.id } }}>
                                                                        <a>
                                                                            {content.title}
                                                                        </a>
                                                                    </Link>
                                                                    <div className="mt-2">
                                                                        <span className="p-0 bg-white"><LongText pathname={"/static/Single-News-Event?id="+ content.id} content={content.description} limit={230} id={content.id} /></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>)}
                                            </div>
                                            {/*------------/press release details--------------------*/}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*------------------categories section--------------------*/}
                </div>
            </div>
        </>

    )
};
export default EventsAndUpdate
