import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import API from "../../../../api/Api";
import Cookies from "universal-cookie";
import Link from "next/link";
import {ReactSearchAutocomplete} from "react-search-autocomplete";
import {strings} from "../../../../public/lang/Strings";

const SingleFAQPage = (props) => {
    const cookie = new Cookies();
    const api = new API();
    const router = useRouter();
    const [singleFAQ, setSingleFAQ] = useState({});
    const [relatedFAQ, setRelatedFAQ] = useState([]);
    const [recentFAQ, setRecentFAQ] = useState([]);
    const [suggestions, setSuggestions] = useState([]);
    const [keyword, setKeyword] = useState(null);
    const [value, setValue] = useState(null);
    const token = cookie.get("token");

    /**
     * @function to get all questions for suggestions
     */
    useEffect(() => {
        const getAllFAQs = () => {
            api.getAllFAQs({keyword : null})
                .then((response) => {
                    response.data.response.map((cat) => {
                        cat.subcategory.map((subCat) => {
                            subCat.faq.map((faq)=> {
                                let data = [];
                                data.push({id: faq.id, name: faq.title})
                                setSuggestions(data)
                            })
                        })
                    })
                })
                .catch((error) => console.log(error));
        }
        getAllFAQs();
    },[])

    /**
     * @function will get all details of single FAQ before page load.
     */
    useEffect(() => {
        console.log(router.query.SingleFAQpage)
        if (router.query.SingleFAQpage) {
            api
                .getSingleFAQ(router.query.SingleFAQpage)
                .then((res) => {
                    if (res.data.response) {
                        setSingleFAQ(res.data.response);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
            api
                .getRelatedFAQ({ faq: router.query.SingleFAQpage })
                .then((res) => {
                    if (res.data.response) {
                        setRelatedFAQ(res.data.response);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
            getRecentView(router.query.SingleFAQPage)
        }
    }, [router.query]);

    /**
     *
     * @param id contains id of current question
     * @function will get all recent FAQs which searched recently.
     */
    const getRecentView = (id) => {
        const array = cookie.get('get_recent_view') ? cookie.get('get_recent_view') : [];
        let newArr = [...array];
        if (array.length) {
            if (!array.includes(id)) {
                newArr.push(id);
                cookie.set('get_recent_view', newArr, { path: '/' });
            }
        }else {
            cookie.set('get_recent_view', newArr, { path: '/' });
        }
        api
            .getRecentViewedFAQ({ faq: newArr, current: [id] })
            .then((res) => {
                if (res.data.response) {
                    setRecentFAQ(res.data.response);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    /**
     *
     * @param string contains string of key which want to search
     * @param results contains related results
     * @function will change value of input field in state
     */
    const handleOnSearch = (string, results) => {
        setValue(string)
    }

    /**
     *
     * @param item is an object contains name and id of suggestion
     * @function will change keyword to search FAQs
     */
    const handleOnSelect = (item) => {
        // the item selected
        setKeyword(item.name)
        router.push({
            pathname: '/static/Help',
            query: {name: item.name}
        })
     }

    /**
     * @function to search FAQs
     */
    const OnSearch = (value) => {
        router.push({
            pathname: '/static/Help',
            query: {name: value}
        })
    }

    /**
     * @function contains element of suggestion maximum 10
     */
    const formatResult = (item) => {
        return (
            <>
                <span style={{ display: 'block', textAlign: 'left' }}>{item.name}</span>
            </>
        )
    }

    return (
        <div className="main-wrapper mt-0">
            {/*-------1st section----*/}
            <div className="main-image">
                <img src={`/images/banner2.png`} className="img-fluid" alt="" />
            </div>
            <div className="main-banner help-banner">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                            <div className="spot-top-details text-center">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="paw"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512"
                                    className="svg-inline--fa fa-paw fa-w-16 fa-2x"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M256 224c-79.41 0-192 122.76-192 200.25 0 34.9 26.81 55.75 71.74 55.75 48.84 0 81.09-25.08 120.26-25.08 39.51 0 71.85 25.08 120.26 25.08 44.93 0 71.74-20.85 71.74-55.75C448 346.76 335.41 224 256 224zm-147.28-12.61c-10.4-34.65-42.44-57.09-71.56-50.13-29.12 6.96-44.29 40.69-33.89 75.34 10.4 34.65 42.44 57.09 71.56 50.13 29.12-6.96 44.29-40.69 33.89-75.34zm84.72-20.78c30.94-8.14 46.42-49.94 34.58-93.36s-46.52-72.01-77.46-63.87-46.42 49.94-34.58 93.36c11.84 43.42 46.53 72.02 77.46 63.87zm281.39-29.34c-29.12-6.96-61.15 15.48-71.56 50.13-10.4 34.65 4.77 68.38 33.89 75.34 29.12 6.96 61.15-15.48 71.56-50.13 10.4-34.65-4.77-68.38-33.89-75.34zm-156.27 29.34c30.94 8.14 65.62-20.45 77.46-63.87 11.84-43.42-3.64-85.21-34.58-93.36s-65.62 20.45-77.46 63.87c-11.84 43.42 3.64 85.22 34.58 93.36z"
                                        className=""
                                    />
                                </svg>
                                <h3 className="text-white mb-3">{strings.WhatAreYouLookingFor}</h3>
                                <div className="input-group group-btn mb-3">
                                    <div className="col pr-0">
                                        <ReactSearchAutocomplete
                                            styling={{borderRadius: 0}}
                                            items={suggestions}
                                            placeholder="Type here to search"
                                            onSearch={handleOnSearch}
                                            onSelect={handleOnSelect}
                                            autoFocus
                                            formatResult={formatResult}
                                        />
                                    </div>
                                    <div className="input-group-append">
                                        <span onClick={()=> OnSearch(value)} className="input-group-text">{strings.Search}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*-------/1st section----*/}
            {/*-------2nd section----*/}
            <div className="container mb-3">
                <div className="row">
                    <div className="col-12 mx-auto">
                        <div className="pay-tabs help-content">
                            {/*----------cards details---------*/}
                            <div className="row">
                                <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                                    <div className="single-info">
                                        <h3 className="font-semibold mb-3">
                                            {singleFAQ.title}
                                        </h3>
                                        <p className="text-black">{strings.WelcomePetLover}</p>
                                        <p className="text-black">
                                            {singleFAQ.description}
                                        </p>
                                    </div>
                                    {!token && <div className="single-info">
                                        <p className="text-black">{strings.HeresHowToGetStarted_C}</p>
                                        <p className="text-black mb-2">
                                            1. {strings.GoToPetcation_D_comAndSelectBecomeASitterThenScrollDownThePageToSelectGetStarted}
                                        </p>
                                        <p className="text-black">
                                            2. {strings.ChooseOneOrMoreServicesYoudLikeToProvideThenSelectSaveAndContinue}
                                        </p>
                                        <div className="row">
                                            <div className="col-12 col-md-10 col-lg-9 col-xl-9 mt-3 mb-4">
                                                <div className="bg-white main-content m-0">
                                                    <div className="login-details">
                                                        <div className="logo-img mb-2">
                                                            <img
                                                                src="images/logo.png"
                                                                className="img-fluid"
                                                                alt=""
                                                            />
                                                        </div>
                                                        <p className="font-14">
                                                            {strings.CreateYourFreeAccountToday}
                                                        </p>
                                                    </div>
                                                    <div className="login-form">
                                                        <form>
                                                            <div className="form-row">
                                                                <div className="form-group col-sm-6">
                                                                    <label>{strings.Firstname}</label>
                                                                    <input
                                                                        className="form-control"
                                                                        id="fname"
                                                                        type="text"
                                                                        value={''}
                                                                    />
                                                                </div>
                                                                <div className="form-group col-sm-6">
                                                                    <label>{strings.Lastname}</label>
                                                                    <input
                                                                        className="form-control"
                                                                        id="lname"
                                                                        type="text"
                                                                        value={''}
                                                                    />
                                                                </div>
                                                                <div className="form-group col-sm-6">
                                                                    <label>{strings.Emailaddress}</label>
                                                                    <input
                                                                        className="form-control"
                                                                        id="email"
                                                                        type="email"
                                                                        value={''}
                                                                    />
                                                                </div>
                                                                <div className="form-group col-sm-6">
                                                                    <label>{strings.DateofBirth}</label>
                                                                    <input
                                                                        className="form-control"
                                                                        id="dob"
                                                                        type="text"
                                                                        value={''}
                                                                    />
                                                                </div>
                                                                <div className="form-group col-sm-6">
                                                                    <label>{strings.Password}</label>
                                                                    <input
                                                                        className="form-control"
                                                                        id="password"
                                                                        type="password"
                                                                        value={''}
                                                                    />
                                                                </div>
                                                                <div className="form-group col-sm-6">
                                                                    <label>{strings.ConfirmPassword}</label>
                                                                    <input
                                                                        className="form-control"
                                                                        id="confirm-password"
                                                                        type="password"
                                                                        value={''}
                                                                    />
                                                                </div>
                                                                <div className="custom-check">
                                                                    <label className="check ">
                                                                        <input type="checkbox" name="is_name1" />
                                                                        <span className="checkmark" />{strings.IAgreeToTermsOfServiceAndPrivacyPolicyConfirmThatIAm18YearsOf}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div className="text-center my-3">
                                                                <button className="btn btn-primary">
                                                                    {strings.Joinnow}
                                                                </button>
                                                            </div>
                                                            <div className="text-center create-account">
                                                                <p className="font-12 mb-0 font-medium">
                                                                    Already have an account ?{" "}
                                                                    <a href="#" className="font-medium">
                                                                        {strings.Login}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="social-icns text-center my-2">
                                                                <div className="social-img">
                                                                    <a href="#">
                                                                        <img
                                                                            src="/images/facebook.png"
                                                                            className="img-fluid"
                                                                        />
                                                                    </a>
                                                                </div>
                                                                <div className="social-img">
                                                                    <a href="#">
                                                                        <img
                                                                            src="/images/google-plus.png"
                                                                            className="img-fluid"
                                                                        />
                                                                    </a>
                                                                </div>
                                                                <div className="social-img">
                                                                    <a href="#">
                                                                        <img
                                                                            src="/images/line.png"
                                                                            className="img-fluid"
                                                                        />
                                                                    </a>
                                                                </div>
                                                                <div className="social-img">
                                                                    <a href="#">
                                                                        <img
                                                                            src="/images/twitter.png"
                                                                            className="img-fluid"
                                                                        />
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="text-black mb-2">
                                            3. {strings.ChooseOneOrMoreServicesYoudLikeToProvideThenSelectSaveAndContinue}
                                        </p>
                                        <p className="text-black mb-2">
                                            4. {strings.GoToPetcation_D_comAndSelectBecomeASitterThenScrollDownThePageToSelectGetStarted}
                                        </p>
                                        <p className="text-black mb-2">
                                            5. {strings.GoToPetcation_D_comAndSelectBecomeASitterThenScrollDownThePageToSelectGetStarted}
                                        </p>
                                    </div>}
                                </div>
                                <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                                    <div className="help-details main-padding">
                                        <h5 className="font-semibold">{strings.RelatedQuestions}</h5>
                                        <ul>
                                            {relatedFAQ.length ? relatedFAQ.map((faq, index) => <li key={index}>
                                                <Link href={`/static/faq/single-faq/${faq.id}`} >
                                                    <a>
                                                        {faq.title}
                                                    </a>
                                                </Link>
                                            </li>) : strings.NoRelatedQuestions}
                                        </ul>
                                        <div className="all-quest">
                                            <a>{strings.ViewAllQuestions}</a>
                                        </div>
                                    </div>
                                    <div className="help-details main-padding">
                                        <h5 className=" font-semibold">{strings.RecentlyViewed}</h5>
                                        <ul>
                                            {recentFAQ.length ? recentFAQ.map((faq, index) => <li key={index}>
                                                <Link href={`/static/faq/single-faq/${faq.id}`} >
                                                    <a>
                                                        {faq.title}
                                                    </a>
                                                </Link>
                                            </li>) : strings.NoRecentQuestions}
                                        </ul>
                                        <div className="all-quest">
                                            <a>{strings.ViewAllQuestions}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*----------/cards details---------*/}
                        </div>
                    </div>
                </div>
            </div>
            {/*-------/2nd section----*/}
        </div>
    )
}

export default SingleFAQPage;
