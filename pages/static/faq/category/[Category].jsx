import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import API from "../../../../api/Api";
import Link from "next/link";
import {ReactSearchAutocomplete} from "react-search-autocomplete";
import {strings} from "../../../../public/lang/Strings";

const CategoryFAQ = () => {
    const api = new API();
    const router = useRouter();
    const [categoryFAQ, setCategoryFAQ] = useState({});
    const [keyword, setKeyword] = useState(null);
    const [value, setValue] = useState(null);
    const [suggestions, setSuggestions] = useState([]);

    /**
     * @function to get all questions for suggestions
     */
    useEffect(() => {
        const getAllFAQs = () => {
            api.getAllFAQs({keyword : null})
                .then((response) => {
                    response.data.response.map((cat) => {
                        cat.subcategory.map((subCat) => {
                            subCat.faq.map((faq)=> {
                                let data = [];
                                data.push({id: faq.id, name: faq.title})
                                setSuggestions(data)
                            })
                        })
                    })
                })
                .catch((error) => console.log(error));
        }
        getAllFAQs();
    },[])

    /**
     * @function to get all FAQs of subcategory
     */
    useEffect(() => {
        console.log(router.query.Category)
        if (router.query.Category) {
            api
                .getSubCategoryFAQ({ subcategory_id: router.query.Category, keyword : keyword })
                .then((res) => {
                    if (res.data.response) {
                        setCategoryFAQ(res.data.response[0]);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }, [router.query, keyword]);

    /**
     *
     * @param string contains string of key which want to search
     * @param results contains related results
     * @function will change value of input field in state
     */
    const handleOnSearch = (string, results) => {
        setValue(string)
    }

    /**
     *
     * @param item is an object contains name and id of suggestion
     * @function will change keyword to search FAQs
     */
    const handleOnSelect = (item) => {
        // the item selected
        setKeyword(item.name)
        setValue(item.name)
    }

    /**
     * @function contains element of suggestion maximum 10
     */
    const formatResult = (item) => {
        return (
            <>
                <span style={{ display: 'block', textAlign: 'left' }}>{item.name}</span>
            </>
        )
    }

    return (
        <div className="main-wrapper mt-0">
            {/*-------1st section----*/}
            <div className="main-image">
                <img src="/images/banner2.png" className="img-fluid" alt="" />
            </div>
            <div className="main-banner help-banner">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                            <div className="spot-top-details text-center">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="paw"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512"
                                    className="svg-inline--fa fa-paw fa-w-16 fa-2x"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M256 224c-79.41 0-192 122.76-192 200.25 0 34.9 26.81 55.75 71.74 55.75 48.84 0 81.09-25.08 120.26-25.08 39.51 0 71.85 25.08 120.26 25.08 44.93 0 71.74-20.85 71.74-55.75C448 346.76 335.41 224 256 224zm-147.28-12.61c-10.4-34.65-42.44-57.09-71.56-50.13-29.12 6.96-44.29 40.69-33.89 75.34 10.4 34.65 42.44 57.09 71.56 50.13 29.12-6.96 44.29-40.69 33.89-75.34zm84.72-20.78c30.94-8.14 46.42-49.94 34.58-93.36s-46.52-72.01-77.46-63.87-46.42 49.94-34.58 93.36c11.84 43.42 46.53 72.02 77.46 63.87zm281.39-29.34c-29.12-6.96-61.15 15.48-71.56 50.13-10.4 34.65 4.77 68.38 33.89 75.34 29.12 6.96 61.15-15.48 71.56-50.13 10.4-34.65-4.77-68.38-33.89-75.34zm-156.27 29.34c30.94 8.14 65.62-20.45 77.46-63.87 11.84-43.42-3.64-85.21-34.58-93.36s-65.62 20.45-77.46 63.87c-11.84 43.42 3.64 85.22 34.58 93.36z"
                                        className=""
                                    />
                                </svg>
                                <h3 className="text-white mb-3">{strings.WhatAreYouLookingFor}</h3>
                                <div className="input-group group-btn mb-3">
                                    <div className="col pr-0">
                                        <ReactSearchAutocomplete
                                            styling={{borderRadius: 0}}
                                            items={suggestions}
                                            placeholder="Type here to search"
                                            onSearch={handleOnSearch}
                                            onSelect={handleOnSelect}
                                            autoFocus
                                            formatResult={formatResult}
                                        />
                                    </div>
                                    <div className="input-group-append">
                                        <span onClick={()=> value ? setKeyword(value) : setKeyword(null)} className="input-group-text">{strings.Search}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*-------/1st section----*/}
            {/*-------2nd section----*/}
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-11 mx-auto">
                        <div className="pay-tabs help-content">
                            {/*----------cards details---------*/}
                            <div className="row">
                                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                    {categoryFAQ ? <div className="help-details main-padding">
                                        <h4 className="font-semibold">{categoryFAQ.title}</h4>
                                        <ul>
                                            {categoryFAQ.faq && categoryFAQ.faq.map((val, index) => <li key={index}>
                                                <Link href={`/static/faq/single-faq/${val.id}`} >
                                                    <a>
                                                        {val.title}
                                                    </a>
                                                </Link>
                                            </li>)}
                                        </ul>
                                    </div>: strings.NoResultFound}
                                </div>
                            </div>
                            {/*----------/cards details---------*/}
                        </div>
                    </div>
                </div>
            </div>
            {/*-------/2nd section----*/}
        </div>

    )
}

export default CategoryFAQ;
