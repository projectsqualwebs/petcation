 import React, {useEffect, useState} from "react"
 import {strings} from "../../public/lang/Strings";
 import API from "../../api/Api";
 const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;

 const Terms = ({data}) =>{
     const [content, setContent] = useState()
     const [lang, setLang] = useState('en')

     useEffect(async () => {
         let myLang = await localStorage.getItem('language')
         if (myLang) {
             setLang(myLang)
         }
     }, [])

     useEffect(() => {
         console.log(data)
         seLanguageContent(data, lang)
     }, [data, lang])

     const seLanguageContent = (myData, myLang) => {

         let engData = {
             id: myData?.id,
             slug: myData?.slug,
             page_title: myData?.page_title_en,
             page_tagline: myData?.page_tagline_en,
             background_image: myData?.background_image,
             content: myData?.content_en,
             created_at: myData?.created_at,
             updated_at:myData?.updated_at
         }

         let jpData = {
             id: myData?.id,
             slug: myData?.slug,
             page_title: myData?.page_title_jp,
             page_tagline: myData?.page_tagline_jp,
             background_image: myData?.background_image,
             content: myData?.content_jp,
             created_at: myData?.created_at,
             updated_at:myData?.updated_at
         }

         let zhData = {
             id: myData?.id,
             slug: myData?.slug,
             page_title: myData?.page_title_zh,
             page_tagline: myData?.page_tagline_zh,
             background_image: myData?.background_image,
             content: myData?.content_zh,
             created_at: myData?.created_at,
             updated_at:myData?.updated_at
         }

         if (myLang == 'en') {
             setContent(engData)
         }
         if (myLang == 'jp') {
             setContent(jpData)
         }
         if (myLang == 'zh') {
             setContent(zhData)
         }

     }
    return(
        <>
            <div className="main-page static-banner">
                <div className="main-wrapper position-relative mt-0 mb-5">
                    <div className="main-image">
                        <img src={IMAGE_BASE_URL+content?.background_image} className="img-fluid" alt="" />
                    </div>
                    <div className="main-banner career-banner">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-md-9 col-lg-8 col-xl-8 mx-auto">
                                    <div className="career-details text-center">
                                        <div className="text-center main-title mb-0">
                                            <h2 className="text-white font-semibold ">{content?.page_title}</h2>
                                            <p className="">{content?.page_tagline}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container mt-3">
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                        <div className="bg-white main-background">
                            <div className="privacy-details" dangerouslySetInnerHTML={{ __html: content?.content}} />
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
 }
 Terms.getInitialProps = async (ctx) => {
     let data = {};
     const api = new API();
     await api
         .getTermsPage()
         .then(async (res) => {
             data = await res.data.response
         })
     console.log(data)
     return { data: data }
 }
 export default Terms;
