import React, { useState } from "react";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import { numberInput } from "../../utils/Helper";
import { useSnackbar } from "react-simple-snackbar";
import { errorOptions, successOptions } from "../../public/appData/AppData";
import { useRef } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import axios from "axios";
const secretKey = process.env.NEXT_PUBLIC_RECAPTCHA_SECRET_KEY;
const siteKey = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;

const Contact = () => {
  let api = new API();
  const [openErr, closeErr] = useSnackbar(errorOptions);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);

  const initialState = {
    name: "",
    email: "",
    phone_number: "",
    subject: "",
    message: "",
  };
  const [formData, setFormData] = useState(initialState);
  const [err, setErr] = useState({
    name: false,
    email: false,
    phone_number: false,
    subject: false,
    message: false,
    is_captcha_verified: false,
  });

  const [captchaerr, setCaptchaErr] = useState("");
  const recaptchaRef = useRef(null);
  // const RECAPTCHA_SECRET_KEY = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
  const RECAPTCHA_SECRET_KEY = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;
  const onChange = (event) => {
    // console.log(event.target.value, event.target.id)
    setErr({
      ...err,
      [event.target.id]: false,
    });
    setFormData({
      ...formData,
      [event.target.id]: event.target.value,
    });
  };

  const userContactUsSubmit = async (formData) => {
    console.log("hii", err?.is_captcha_verified);

    if (!formData.name) {
      setErr({ ...err, name: true, is_captcha_verified: false });
      recaptchaRef?.current?.reset();

      return false;
    }
    if (!formData.phone_number) {
      setErr({ ...err, phone_number: true, is_captcha_verified: false });

      recaptchaRef?.current?.reset();
      return false;
    }
    if (!formData.email) {
      setErr({ ...err, email: true, is_captcha_verified: false });
      recaptchaRef?.current?.reset();
      return false;
    }
    if (!formData.subject) {
      setErr({ ...err, subject: true, is_captcha_verified: false });

      recaptchaRef?.current?.reset();
      return false;
    }
    if (!formData.message) {
      setErr({ ...err, message: true, is_captcha_verified: false });

      recaptchaRef?.current?.reset();
      return false;
    }
    if (!err?.is_captcha_verified) {
      setCaptchaErr("Click on I'm not robot");
      return false;
    }
    api
      .userContactUsSubmit(formData)
      .then((res) => {
        // console.log(res.data.response)
        if (res?.data?.response) {
          openSuccess(strings.SuccessfullySubmitted);
          setFormData({ ...initialState });
          recaptchaRef?.current?.reset();
          setErr({ ...err, is_captcha_verified: false });
        }
      })
      .catch((err) => {
        recaptchaRef?.current?.reset();
        setErr({ ...err, is_captcha_verified: false });
        openErr(error.response?.data?.message);
      });
  };

  const onReCAPTCHAChange = (token) => {
    api.verifyRecaptcha(token)
        .then((res) => {
          if (res.data.success) {
            setErr({ ...err, is_captcha_verified: true });
          } else {
            setErr({ ...err, is_captcha_verified: false });
          }
        })
        .catch((err) => {
          setErr({ ...err, is_captcha_verified: false });
          recaptchaRef?.current?.reset();
        });
  };

  return (
    <>
      <div className="main-page">
        <div className="main-wrappper mt-0">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                <div className="bg-white main-content p-4 rounded-15">
                  <div className="contact-us">
                    <div className="main-title partner-title mb-4">
                      <h2 className="font-semibold">{strings.ContactUs}</h2>
                      <p className="mb-0">
                        {strings.HeyLetUsKnowHowWeCanHelpYou}
                      </p>
                    </div>
                  </div>
                  <div className="login-form">
                    <div className="form-row">
                      <div className="form-group col-sm-6">
                        <label>{strings.Name}</label>
                        {err.name ? (
                          <label className={"error-text"}>
                            {strings.EnterYourName}
                          </label>
                        ) : null}
                        <input
                          className={`form-control ${
                            err.name ? "invalid" : ""
                          }`}
                          placeholder={strings.EnterYourName}
                          id="name"
                          type="text"
                          value={formData.name}
                          onChange={onChange}
                        />
                      </div>
                      <div className="form-group col-sm-6">
                        <label>{strings.Contactnumber}</label>
                        {err.phone_number ? (
                          <label className={"error-text"}>
                            {strings.EnterContactNumber}
                          </label>
                        ) : null}
                        <input
                          className={`form-control ${
                            err.phone_number ? "invalid" : ""
                          }`}
                          placeholder={strings.EnterContactNumber}
                          id="phone_number"
                          type="text"
                          value={formData.phone_number}
                          maxLength={10}
                          onChange={onChange}
                          onKeyPress={numberInput}
                        />
                      </div>
                      <div className="form-group col-sm-12">
                        <label>{strings.Emailaddress}</label>
                        {err.email ? (
                          <label className={"error-text"}>
                            {strings.EnterYourEmailAddress}
                          </label>
                        ) : null}
                        <input
                          className={`form-control ${
                            err.email ? "invalid" : ""
                          }`}
                          placeholder={strings.EnterYourEmailAddress}
                          id="email"
                          type="email"
                          value={formData.email}
                          onChange={onChange}
                        />
                      </div>
                      <div className="form-group col-sm-12">
                        <label>{strings.Subject}</label>
                        {err.subject ? (
                          <label className={"error-text"}>
                            {strings.EnterYourSubject}
                          </label>
                        ) : null}
                        <input
                          className={`form-control ${
                            err.subject ? "invalid" : ""
                          }`}
                          placeholder={strings.EnterYourSubject}
                          id="subject"
                          type="text"
                          value={formData.subject}
                          onChange={onChange}
                        />
                      </div>
                      <div className="form-group col-sm-12">
                        <label>{strings.YourMessage}</label>
                        {err.message ? (
                          <label className={"error-text"}>
                            {strings.EnterYourMessage}
                          </label>
                        ) : null}
                        <textarea
                          className={`form-control ${
                            err.message ? "invalid" : ""
                          }`}
                          placeholder={strings.EnterYourMessage}
                          rows={4}
                          id="message"
                          type="text"
                          value={formData.message}
                          onChange={onChange}
                        />
                      </div>
                    </div>
                    {/* <ReCAPTCHA
                      ref={recaptchaRef}
                      sitekey={siteKey}
                      onChange={onReCAPTCHAChange}
                    /> */}
                    {!err?.is_captcha_verified ? (
                      <span className="text-danger small">
                        <b>{captchaerr}</b>
                      </span>
                    ) : (
                      ""
                    )}
                    <div className="d-flex justify-content-center mt-1">
                      <ReCAPTCHA
                        ref={recaptchaRef}
                        sitekey={siteKey}
                        onChange={onReCAPTCHAChange}
                      />
                    </div>
                    <div className="text-center my-3 mb-2">
                      <button
                        className="btn btn-primary"
                        onClick={() => userContactUsSubmit(formData)}>
                        {strings.Send}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="padding my-5">
              <div className="container">
                <div className="row">
                  <div className="col-12 col-sm-8 col-lg-8 col-xl-9 mx-auto">
                    <div className="text-center main-title mb-4">
                      <h2 className="font-semibold">
                        {strings.NeedAssistance_Q}
                      </h2>
                      <p className="mb-0">
                        {
                          strings.OurTeamIsAlwaysHereToProvideAssistanceForYourQueries
                        }
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-12 pb-5">
                  <div className="row justify-content-center">
                    <div className="col-12 col-md-3">
                      <a href="mailto:support@petcation.com">
                        <div className="bg-white px-3 py-4 rounded-15 why-choose text-center">
                          <div className="icons-verify">
                            <img
                              src="../../images/verified.png"
                              width="40px"
                              className="img-fluid"
                            />
                          </div>
                          <h5 className="mt-3 mb-1  font-semibold">
                            {strings.EmailAt}
                          </h5>
                          <p className="font-14 mb-0">support@pecation.com</p>
                        </div>
                      </a>
                    </div>
                    <div className="col-12 col-md-3">
                      <a href="tel:+81330190668">
                        <div className="bg-white px-3 py-4 rounded-15 why-choose text-center">
                          <div className="icons-verify">
                            <img
                              src="../../images/support.png"
                              width="40px"
                              className="img-fluid"
                            />
                          </div>
                          <h5 className="mt-3 mb-1  font-semibold">
                            {strings.CallAt}
                          </h5>
                          <p className="font-14 mb-0">+81330190668</p>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Contact;
