import React, {useEffect, useState} from "react";
import Link from "next/link";
import {strings} from "../../public/lang/Strings";
import API from "../../api/Api";
import Select from 'react-select';
import {numberInput, scrollToExactView} from "../../utils/Helper";
import { useSnackbar } from "react-simple-snackbar";
import {errorOptions, successOptions} from "../../public/appData/AppData";

const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;

const Career = () => {
    let api = new API();
    const [openErr, closeErr] = useSnackbar(errorOptions);
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const initialState = {
        first_name: '',
        last_name: "",
        email:"",
        phone_number:"",
        document: "",
        category : null
    };
    const initialErr = {
        first_name : false,
        last_name: false,
        email: false,
        phone_number: false,
        document: false,
        category : false,
    };
    const [careerPageBanner, setCareerPageBanner] = useState({});
    const [careerPageOpening, setCareerPageOpening] = useState([]);
    const [categoryOptions, setCategoryOptions] = useState([]);
    const [formData, setFormData] = useState(initialState);
    const [err, setErr] = useState(initialErr);

    const customStyles = {
        control: (provided, state) => ({
            ...provided,
            border : err.category ? '2px solid red' : provided.border,
            boxShadow: state.isFocused ? null : provided.boxShadow,
            '&:hover': {
                borderColor: state.isFocused ? null : provided.borderColor
            }
        })
    };

    const getCategoryLanguageContent = async (data, myLang) => {
        if (myLang == 'en' && data?.length) {
            let engData = [];
            await data?.map(async (myData) => await engData.push({
                value: myData?.title_en,
                label: myData?.title_en,
                description : myData?.description_en,
                image : myData?.image
            }));
            return engData;
        }
        if (myLang == 'jp' && data?.length) {
            let jpData = [];
            await data?.map(async (myData) => await jpData.push({
                value: myData?.title_jp,
                label: myData?.title_jp,
                description : myData?.description_jp,
                image : myData?.image
            }));
            return jpData
        }
        if (myLang == 'zh' && data?.length) {
            let zhData = [];
            await data?.map(async (myData) => await zhData.push({
                value: myData?.title_zh,
                label: myData?.title_zh,
                description : myData?.description_zh,
                image : myData?.image
            }));
            return zhData
        }
    };

    const getBannerLanguageContent = async (data, myLang) => {
        if (myLang == 'en' && !!data) {
            return {
                title: data?.page_title_en,
                description: data?.page_tagline_en,
                banner : data?.background_image
            };
        }
        if (myLang == 'jp' && !!data) {
            return {
                title: data?.page_title_jp,
                description: data?.page_tagline_jp,
                banner : data?.background_image
            }
        }
        if (myLang == 'zh' && !!data) {
            return {
                title: data?.page_title_zh,
                description: data?.page_tagline_zh,
                banner : data?.background_image
            }
        }
    };

    const onChange = (event) => {
        // console.log(event.target.value, event.target.id)
        console.log({formData});
        setErr({
            ...err,
            [event.target.id]: false
        });
        setFormData({
            ...formData,
            [event.target.id]: event.target.value
        })
    };

    const handleSelect = (value) => {
        setErr(initialErr);
        setFormData({...formData, category : value})
    };

    const onFileChange = (event) => {
        if (event.target.files) {
            let files;
            files = event?.target?.files;
            const reader = new FileReader();
            reader.onload = () => {
                console.log(reader.result);
            };
            if (files[0]) {
                reader.readAsDataURL(files[0]);
                const formData = new FormData();
                formData.append("image", files[0]);
                formData.append("path", "career");
                api
                    .uploadFile(formData)
                    .then((json) => {
                        console.log(json.data.response);
                        setErr({
                            ...err,
                            document: false
                        });
                        setFormData({
                            ...formData,
                            document: json.data.response
                        });
                        openSuccess(strings.FileUploadedSuccessfully)
                    })
                    .catch((error) => console.log(error));
            }
        }
    };

    const userSubmitCareerForm = (formData) => {

        const data = {...formData};

        if (!formData.category?.value) {
            setErr({...err, category: true});
            scrollToExactView("category");
            return false
        }

        if (!formData.first_name) {
            setErr({...err, first_name: true});
            scrollToExactView("first_name");
            return false
        }
        if (!formData.last_name) {
            setErr({...err, last_name: true});
            scrollToExactView("last_name");
            return false
        }
        if (!formData.email) {
            setErr({...err, email: true});
            scrollToExactView("email");
            return false
        }
        if (!formData.phone_number) {
            setErr({...err, phone_number: true});
            scrollToExactView("phone_number");
            return false
        }

        if (!formData.document) {
            setErr({...err, document: true});
            return false
        }

        data.category = formData.category.value;

        api
            .userSubmitCareerForm(data)
            .then((res) => {
                // console.log(res.data.response)
                if (res?.data?.response) {
                    openSuccess(strings.SuccessfullySubmitted);
                    setFormData({...initialState})
                }
            })
            .catch(err => openErr(error.response?.data?.message))
    };

    const getCareerPageData = () =>{
        api.getCareerPageData().then((res)=>{
            if(res.data.response) {
                const response = res.data.response;
                getBannerLanguageContent(response.career , "en").then((langRes)=>{
                    setCareerPageBanner(langRes)
                });
                getCategoryLanguageContent(response.openings , "en").then((langRes)=>{
                    setCareerPageOpening(langRes);
                    setCategoryOptions(langRes)
                });

            }
        })
    };

    useEffect(()=>{
        getCareerPageData();
    },[]);

    return(
        <>
            <div className="main-wrapper mt-0">
                {/*-------1st section----*/}
                <div className="career-position static-banner">
                    <div className="main-image mb-4">
                        <img src={IMAGE_BASE_URL +  (careerPageBanner?.banner ?? "/images/banner.png")} className="img-fluid" alt="" />
                    </div>
                    {/*-------/1st section----*/}
                    {/*---------career section--------------*/}
                    <div className="main-banner career-banner">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-md-9 col-lg-8 col-xl-8 mx-auto">
                                    <div className="career-details text-center">
                                        <div className="text-center main-title mb-0">
                                            <h2 className="text-white font-semibold ">{careerPageBanner?.title}</h2>
                                            <p className="">{careerPageBanner?.description}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*---------/career section--------------*/}
                {/*-------2nd section---------------------*/}
                <div className="container ">
                    <div className="row mt-5">
                        <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                            {/*----*/}
                            {!!careerPageOpening.length &&
                                careerPageOpening.map((opening,index) => {
                                    if((index + 1) % 2 !== 0) {
                                        return (
                                            <div className="career-impact">
                                                <div className="row">
                                                    <div className="col-12 col-md-7 col-lg-7 col-xl-7">
                                                        <div className="career-image">
                                                            <img src={IMAGE_BASE_URL + (opening?.image || "/images/img5.png")} className="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                    <div className="col-12 col-md-5 col-lg-5 col-xl-5 my-auto">
                                                        <div className="career-content">
                                                            <h4 className="font-medium">{opening?.label}</h4>
                                                            <p  dangerouslySetInnerHTML={{ __html: opening?.description }} className="font-14"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    } else {
                                        return (
                                            <div className="career-impact">
                                                <div className="row">
                                                    <div className="col-12 col-md-5 col-lg-5 col-xl-5 my-auto">
                                                        <div className="career-content">
                                                            <h4 className="font-medium">{opening?.label}</h4>
                                                            <p  dangerouslySetInnerHTML={{ __html: opening?.description }} className="font-14"/>
                                                        </div>
                                                    </div>
                                                    <div className="col-12 col-md-7 col-lg-7 col-xl-7">
                                                        <div className="career-image">
                                                            <img src={IMAGE_BASE_URL + (opening?.image || "/images/img5.png")} className="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    }

                                })
                            }


                            {/*--/--*/}
                            {/*----*/}

                            <div className="career-impact">
                                <div id='applyNow' className="col-12 col-md-7 mx-auto py-5">
                                    <div className="bg-white main-content p-4 rounded-15">
                                        <div className="contact-us">
                                            <div className="text-center main-title mb-4">
                                                <h2 className="font-semibold">{strings.ApplyNow}</h2>
                                                <p className="mb-0">{strings.HeyLetUsKnowHowWeCanHelpYou}</p>
                                            </div>
                                        </div>
                                        <div className="login-form">
                                                <div className="form-row">
                                                    <div className="form-group col-sm-12">
                                                        <label>{strings.ApplyingFor}</label>
                                                        {err.category ? <label className={"error-text"}>{strings.SelectAnOpening}</label> : null}
                                                        <Select
                                                            className={` ${err.category ? 'invalid' : ''}`}
                                                            value={formData.category}
                                                            id="category"
                                                            styles={customStyles}
                                                            onChange={handleSelect}
                                                            options={categoryOptions}
                                                            placeholder={strings.SelectOpening}
                                                        />
                                                    </div>
                                                    <div className="form-group col-sm-6">
                                                        <label>{strings.FirstName}</label>
                                                        {err.first_name ? <label className={"error-text"}>{strings.EnterFirstName}</label> : null}
                                                        <input
                                                            className={`form-control ${err.first_name ? 'invalid' : ''}`}
                                                            placeholder={strings.EnterFirstName}
                                                            id="first_name"
                                                            type="text"
                                                            value={formData.first_name}
                                                            onChange={onChange}
                                                        />
                                                    </div>
                                                    <div className="form-group col-sm-6">
                                                        <label>{strings.LastName}</label>
                                                        {err.last_name ? <label className={"error-text"}>{strings.EnterLastName}</label> : null}
                                                        <input
                                                            className={`form-control ${err.last_name ? 'invalid' : ''}`}
                                                            placeholder={strings.EnterLastName}
                                                            id="last_name"
                                                            type="text"
                                                            value={formData.last_name}
                                                            onChange={onChange}
                                                        />
                                                    </div>
                                                    <div className="form-group col-sm-6">
                                                        <label>{strings.Emailaddress}</label>
                                                        {err.email ? <label className={"error-text"}>{strings.EnterYourEmailAddress}</label> : null}
                                                        <input
                                                            className={`form-control ${err.email ? 'invalid' : ''}`}
                                                            placeholder={strings.EnterYourEmailAddress}
                                                            id="email"
                                                            type="email"
                                                            value={formData.email}
                                                            onChange={onChange}
                                                        />
                                                    </div>
                                                    <div className="form-group col-sm-6">
                                                        <label>{strings.Contactnumber}</label>
                                                        {err.phone_number ? <label className={"error-text"}>{strings.EnterContactNumber}</label> : null}
                                                        <input
                                                            className={`form-control ${err.phone_number ? 'invalid' : ''}`}
                                                            placeholder={strings.EnterContactNumber}
                                                            id="phone_number"
                                                            type="text"
                                                            value={formData.phone_number}
                                                            maxLength={10}
                                                            onChange={onChange}
                                                            onKeyPress={numberInput}
                                                        />
                                                    </div>

                                                    <div className="form-group col-12 upload-resume">
                                                        {!formData?.document ? <div className="col-12 px-3 py-4 rounded-lg border border-success">
                                                            <div className="row align-items-center flex-column justify-content-center">
                                                                <div className="col-12 text-center mb-3">
                                                                    <img  src="../../images/upload.png" width="54px" className="img-fluid mb-3" />
                                                                    <h6 className="font-weight-medium mb-0">{strings.UploadYourResume}</h6>
                                                                </div>
                                                                <div className="col-12 text-center upload-btn-resume">
                                                                    <button className="mb-0 btn btn-sm btn-warning rounded-lg position-relative">
                                                                        {strings.UploadCV}
                                                                        <input className=" opacity-0 position-absolute w-100 h-100 top left-0" id="document_file" type="file" onChange={onFileChange} />
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>:
                                                            <div className="name col-auto text-center border border-info bg- py-1 mt-3 rounded-lg d-flex justify-content-between">
                                                                <p className="font-weight-medium my-0 py-1">
                                                                    {formData?.document.split('/')[formData?.document.split('/')?.length - 1]}
                                                                </p>
                                                                    <span className="font-weight-bold ml-2 cursor-pointer py-1 text-danger font-weight-bolder" onClick={() => setFormData({...formData, document: ''})}>&#10005;</span>
                                                            </div>}
                                                    </div>
                                                </div>
                                                {err.document ? <label className={"error-text"}>{strings.SelectDocument}</label> : null}
                                                <div className="text-center my-3">
                                                    <button className="btn btn-primary" onClick={()=> userSubmitCareerForm(formData)}>{strings.SubmitApplication}</button>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*--/--*/}
                        </div>
                    </div>
                </div>
                {/*-------/2nd section-------------------*/}
            </div>
        </>
    )
}
export default Career;
