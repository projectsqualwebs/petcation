import React from "react"
import {strings} from "../../public/lang/Strings";

const Safety = () => {
    return(
        <>
            <div className="main-page static-banner">
                <div className="main-wrapper position-relative mt-0 mb-5">
                    <div className="main-image">
                        <img src="/images/banner2.png" className="img-fluid" alt="" />
                    </div>
                    <div className="main-banner career-banner">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-md-9 col-lg-8 col-xl-8 mx-auto">
                                    <div className="career-details text-center">
                                        <div className="text-center main-title mb-0">
                                            <h2 className="text-white font-semibold ">{strings.safety}</h2>
                                            <p className="">{strings.LastUpdated_C} 25 September 2022</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                        <div className="bg-white main-background">
                            <div className="privacy-details">
                                <ol>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                    <li>
                                        <h6 className="font-semibold">
                                            {strings.HowDoWeCollectAndUseYourInformation_Q}
                                        </h6>
                                        <p>
                                            {strings.loremIpsum50}
                                        </p>
                                        <hr />
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>

    )
}

export default Safety
