import React, { useEffect, useState } from 'react';
import API from '../../api/Api';
import { useRouter } from "next/router";
import Link from "next/link";
import {strings} from "../../public/lang/Strings";

const api = new API();

const SingleNewsEvent = (props) => {
    const [newsData, setNewsData] = useState({});
    const router = useRouter();
    useEffect(() => {
        const id = router.query.id
        const getSingleNews = (id) => {
            api.getSingleNews(id)
                .then((response) => {
                    setNewsData(response.data.response);
                    console.log(response.data.response)
                })
                .catch((error) => console.log(error));
        }
        getSingleNews(id);
    }, [router.query])
    return (
        <>
            <div>
                <div className="main-wrapper">
                    <div className="booking-sitter py-2">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                    <div className="single-news single-service">
                                        <nav aria-label="breadcrumb">
                                            <ol className="breadcrumb">
                                                <li className="breadcrumb-item">
                                                    {" "}
                                                    <Link href="/">
                                                        {strings.Home}
                                                    </Link>
                                                </li>
                                                {newsData && <li className="breadcrumb-item">
                                                    {" "}
                                                    <Link href={{pathname: '/static/EventsAndUpdate'}}>
                                                        {strings.EventsAndUpdates}
                                                    </Link>
                                                </li>}

                                                {newsData && <li className="breadcrumb-item active" aria-current="page">
                                                    {" "}

                                                    {newsData.title}

                                                </li>}

                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {newsData && <div className="container">
                        <div className="row">
                            <div className="col-12 mt-3">
                                <div className="col-12 bg-white main-background px-3 mb-2">
                                    <div className="single-news-info padding py-0">
                                        <small className="mb-1 font-normal text-muted font-italic font-weight-medium">{newsData.event_date}</small>
                                        <h3 className="font-semibold mb-0">
                                            {newsData.title}
                                        </h3>
                                    </div>
                                </div>
                                <div className="col-12 bg-white main-background px-3">
                                    <div className="single-news-info padding py-0">
                                        <p className="mb-0">
                                            {newsData.description}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>}

                </div>
            </div>
        </>

    )
}

export default SingleNewsEvent;
