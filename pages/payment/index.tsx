import { useRouter } from "next/router";
import React from "react";
import { useEffect, useState } from "react";
import {
  errorOptions,
  pet,
  petSize,
  successOptions,
} from "../../public/appData/AppData";
import API from "../../api/Api";
import I_SINGLE_REQUEST_DETAIL from "../../models/requestSitter.interface";
import { useSnackbar } from "react-simple-snackbar";
import "react-day-picker/lib/style.css";
import "rc-time-picker/assets/index.css";
import { AxiosResponse } from "axios";
import Res from "../../models/response.interface";
import { strings } from "../../public/lang/Strings";
import I_CARD_DETAILS from "../../models/requestSitter.interface";
import Cookies from "universal-cookie";
import I_BANK_DETAILS from "../../models/requestSitter.interface";
import { Modal } from "react-bootstrap";
import { getCurrencySign } from "../../api/Constants";
import AddCouponModal from "../../components/common/AddCouponModal";
import boolean from "async-validator/dist-types/validator/boolean";
import OverlayLoader from "../../components/common/OverlayLoader";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

const api = new API();

const PaymentScreen: React.FC<{}> = () => {
  const router = useRouter();
  const cookies = new Cookies();
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [showCouponModal, setShowCouponModal] = useState<boolean>(false);
  const [successView, setSuccessView] = useState<boolean>(false);

  const [cardData, setCardData] = useState<I_CARD_DETAILS[]>([]);
  const [bankData, setBankData] = useState<I_BANK_DETAILS[]>([]);
  const [couponCode, setCouponcode] = useState<string>("");
  const [coupons, setCoupons] = useState<any>([]);
  const [loading, setLoading] = useState(false);

  const [requestData, setRequestData] = useState<I_SINGLE_REQUEST_DETAIL>();

  const [errors, setErrors] = useState<any>("");
  const [isCardPaymentView, setIsCardPaymentView] = useState(true);

  useEffect(() => {
    if (router.query.id) {
      getRequestDetail(router.query.id);
      getCoupons();
    }
    if (router.query?.type) {
      setIsCardPaymentView(false);
    }
  }, [router.query]);

  useEffect(() => {
    getCards();
    getBankAccounts();
  }, []);

  const getCoupons = () => {
    api
      .getCoupons(1)
      .then((res) => {
        if (res.data.status == 200) {
          setCoupons(res.data.response);
        }
      })
      .catch((error) => {
        console.log("error is", error);
      });
  };

  const getRequestDetail = (id: any) => {
    api
      .getSingleRequestDetail(id)
      .then((response: AxiosResponse<Res<I_SINGLE_REQUEST_DETAIL>>) => {
        let data = response.data.response;
        setRequestData(data);
        setCouponcode(data.coupon_code);
        if (data.payment_status) {
          router.replace("/user/reservation?index=2").then((r) => {});
        }
      })
      .catch((error) => console.log(error));
  };

  const getCards = () => {
    api
      .getAllCard()
      .then((json: AxiosResponse<Res<I_CARD_DETAILS[]>>) => {
        setCardData(json.data.response);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const getBankAccounts = () => {
    api
      .getAllBankAccount()
      .then((json: AxiosResponse<Res<I_BANK_DETAILS[]>>) => {
        setBankData(json.data.response);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const confirmPayment = () => {
    setLoading(true);
    if (!cardData.length) {
      openError(strings.SelectBankCard);
      setLoading(false);
    } else {
      let card = cardData.find((val) => val.is_default == 1);
      if (card) {
        api
          .confirmPayment(requestData.id, { card_id: card.id })
          .then((res) => {
            if (res.data.status == 200) {
              openSuccess(strings.SuccessfullyPaidForTheJob);
              setLoading(false);
              setSuccessView(true);
            } else {
              openError(res.data.message);
              setLoading(false);
            }
          })
          .catch((error) => {
            openError(error.response.data.message);
            console.log("error is", error.response.data.message);
            setLoading(false);
          });
      } else {
        openError(strings.SelectBankCard);
        setLoading(false);
      }
    }
  };

  const changeCoupon = (code) => {
    if (!router.query.id) {
      return;
    }
    api
      .removeCoupon(router.query.id)
      .then((json: any) => {
        if (code) {
          api
            .applyCoupon({ coupon_code: code, booking_id: router.query.id })
            .then((json: any) => {
              if (json.data.status == 200) {
                getRequestDetail(router.query.id);
                openSuccess(strings.SuccessfullyAppliedCoupon);
              } else {
                openError(json.data.message);
              }
            })
            .catch((error) => {
              setErrors(error.response.data.errors);
            });
        }
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const applyCoupon = (code) => {
    setCouponcode("");
    setLoading(true);
    if (!router.query.id) {
      return;
    }

    if (code) {
      api
        .applyCoupon({ coupon_code: code, booking_id: router.query.id })
        .then((json: any) => {
          if (json.data.status == 200) {
            setLoading(false);
            getRequestDetail(router.query.id);
            openSuccess(strings.SuccessfullyAppliedCoupon);
          } else {
            openError(json.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setCouponcode("");
          setErrors(error.response.data.message);
        });
    } else {
      openError(strings.EnterCouponCode);
    }
  };

  const removeCoupon = () => {
    if (!router.query.id) {
      return;
    }
    if (couponCode) {
      api
        .removeCoupon(router.query.id)
        .then((json: any) => {
          getRequestDetail(router.query.id);
          setCouponcode("");
          openSuccess(strings.SuccessfullyAppliedCoupon);
        })
        .catch((error) => {
          console.log("error", error);
        });
    }
  };

  const ConfirmBooking = () => {
    confirmAlert({
      closeOnClickOutside: true,
      customUI: ({ onClose }) => {
        return (
          <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
            <div className="react-confirm-alert-body">
              <h3>
                <b>{strings.BookingCantbeedIteDonceItsBeenDone}</b>
              </h3>
              <br />
              <div className="react-confirm-alert-button-group">
                <button
                  className="btn btn-danger w-50"
                  aria-label="Yes"
                  onClick={() => {
                    onClose();
                  }}
                >
                  {strings.Cancel}
                </button>
                <button
                  className="btn btn-primary w-50"
                  aria-label="No"
                  onClick={() => {
                    confirmPayment();
                    onClose();
                  }}
                >
                  {strings.Confirm}
                </button>
              </div>
            </div>
          </div>
        );
      },
    });
  };

  return (
    <>
      <div className="main-page">
        {loading ? <OverlayLoader /> : ""}
        <div className="main-wrapper bottom">
          {/*-------------for mobile view--------------*/}
          <div className="bg-white main-background mb-0 pl-0 d-block d-md-none d-lg-none d-xl-none">
            <div className="container">
              <div className="row">
                <div className="col-10 mob-pay">
                  <small className="mb-1">
                    {requestData ? requestData.pets[0].pet_name : ""}&nbsp;
                    {requestData ? requestData.service.name : ""}
                    &nbsp;with&nbsp;
                    {requestData ? requestData.service.name : ""}&nbsp;
                    {requestData
                      ? requestData.user.firstname +
                        " " +
                        requestData.user.lastname
                      : ""}
                  </small>
                  <p className="font-12 mb-0">
                    For {requestData ? requestData.pets.length : 0}&nbps;
                    {requestData ? requestData.pet_type : ""}
                  </p>
                </div>
                <div className="col-2 pay-edit pr-0">
                  <div className="spot-info chat-float">
                    <div className="main-icon">
                      <div className="edit-icon">
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="pencil"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                          className="svg-inline--fa fa-pencil fa-w-16 fa-2x"
                        >
                          <path
                            fill="currentColor"
                            d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"
                            className=""
                          />
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/*-------------/for mobile view-------------*/}
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-11 col-xl-11 mx-auto">
                <h4 className="font-semibold mb-3 d-none d-md-block d-lg-block d-xl-block">
                  {strings.paymentInformation}
                </h4>
                {/*----------*/}
                <div className="row">
                  <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                    <div className="bg-white main-content top-margin">
                      <h5 className="font-medium mb-3">
                        {strings.selectPaymentMethod}
                      </h5>
                      <div className="pay-tabs">
                        <div>
                          <ul
                            className="nav nav-tabs"
                            id="myTab"
                            role="tablist"
                          >
                            <li className="nav-item" role="presentation">
                              <a
                                className={`nav-link ${
                                  isCardPaymentView === true ? "active" : ""
                                }`}
                                id="cards-tab"
                                data-toggle="tab"
                                onClick={() => setIsCardPaymentView(true)}
                                role="tab"
                              >
                                {strings.Cards}
                              </a>
                            </li>
                            <li className="nav-item" role="presentation">
                              <a
                                className={`nav-link ${
                                  isCardPaymentView === false ? "active" : ""
                                }`}
                                id="bank-tab"
                                data-toggle="tab"
                                onClick={() => setIsCardPaymentView(false)}
                                role="tab"
                              >
                                {strings.bankPayment}
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="myTabContent">
                          {/*----------cards details---------*/}
                          {isCardPaymentView === true ? (
                            <div
                              className="tab-pane fade show active"
                              id="cards"
                              role="tabpanel"
                              aria-labelledby="cards-tab"
                            >
                              <div className="cards-details">
                                <h5 className="font-semibold mb-3">
                                  {strings.savedCards}
                                </h5>
                                {cardData.map((item, index) => (
                                  <>
                                    <div className="row">
                                      <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                                        <div className="custom-check">
                                          <label className="check ">
                                            <input
                                              type="radio"
                                              name="is_name1"
                                              checked={item.is_default == 1}
                                              onChange={() => {
                                                let data = cardData.map(
                                                  (val) => {
                                                    let data = val;
                                                    return {
                                                      ...data,
                                                      is_default: 0,
                                                    };
                                                  }
                                                );
                                                data[index].is_default = 1;
                                                setCardData(data);
                                              }}
                                            />
                                            <span className="checkmark" />
                                            {cookies.get("firstname") +
                                              " " +
                                              cookies.get("lastname")}
                                          </label>
                                        </div>
                                      </div>
                                      <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                                        <div className="master-card">
                                          <p className="mb-0">
                                            {strings.MasterCard + " "}
                                            <span className="font-14 ml-3">
                                              **** **** **** {item.last4}
                                            </span>
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                    <hr />
                                  </>
                                ))}
                                <div className="card-button">
                                  <button
                                    onClick={() =>
                                      router.push({
                                        pathname: "user/payments-and-payouts",
                                        query: { id: router.query.id },
                                      })
                                    }
                                    className="btn btn-black"
                                  >
                                    {strings.Addnewcard}
                                  </button>
                                </div>
                              </div>
                            </div>
                          ) : (
                            <div
                              className="tab-pane fade show active"
                              id="bank"
                              role="tabpanel"
                              aria-labelledby="bank-tab"
                            >
                              <div className="cards-details">
                                <h5 className="font-semibold mb-3">
                                  {strings.savedBanks}
                                </h5>
                                {bankData.map((val) => (
                                  <>
                                    <div className="row">
                                      <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                                        <div className="custom-check">
                                          <label className="check ">
                                            <input
                                              type="radio"
                                              name="is_name2"
                                              defaultChecked={true}
                                            />
                                            <span className="checkmark" />
                                            {val.bank_name}
                                          </label>
                                        </div>
                                      </div>
                                      <div className="col-6 col-md-2 col-lg-2 col-xl-2 user-name">
                                        <div className="master-card">
                                          <p className="mb-0">
                                            {" "}
                                            {cookies.get("firstname") +
                                              " " +
                                              cookies.get("lastname")}
                                          </p>
                                        </div>
                                      </div>
                                      <div className="col-6 col-md-2 col-lg-2 col-xl-2 alignment">
                                        <div className="master-card">
                                          <p className="mb-0">
                                            {val.account_number}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                    <hr />
                                  </>
                                ))}
                                <div className="card-button">
                                  <button
                                    onClick={() =>
                                      router.push({
                                        pathname: "user/payments-and-payouts",
                                        query: { id: router.query.id, type: 2 },
                                      })
                                    }
                                    className="btn btn-black"
                                  >
                                    {strings.Addnewbankaccount}
                                  </button>
                                </div>
                              </div>
                            </div>
                          )}
                          {/*----------/bank details---------*/}
                        </div>
                      </div>
                      {/*-------coupon code-----*/}
                      <hr />
                      <div className="dog box row" style={{ display: "block" }}>
                        <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                          <div className="coupon-details">
                            <h6 className="mb-1 font-semibold text-muted">
                              {strings.AvailableCoupons}
                            </h6>
                            <p className="mb-0 font-12 text-muted">
                              {
                                strings.SelectToApplyCouponForToAvailDiscountOnYouBooking
                              }
                            </p>
                          </div>
                        </div>
                        <div className="col-12">
                          <ul className="chec-radio">
                            {coupons && coupons?.length ? (
                              coupons?.map((item, index) => (
                                <>
                                  <li key={index} className="pet-select">
                                    <label className="radio-inline w-50">
                                      {loading ? <OverlayLoader /> : ""}
                                      <input
                                        type="checkbox"
                                        key={`pet${index}`}
                                        id={`pet${index}`}
                                        name="select1"
                                        checked={
                                          couponCode ==
                                          item?.coupon?.coupon_code
                                            ? true
                                            : false
                                        }
                                        onChange={() => {
                                          if (couponCode) {
                                            setCouponcode(
                                              item?.coupon?.coupon_code
                                            );
                                            changeCoupon(
                                              item?.coupon?.coupon_code
                                            );
                                          } else {
                                            setCouponcode(
                                              item?.coupon?.coupon_code
                                            );
                                            applyCoupon(
                                              item?.coupon?.coupon_code
                                            );
                                          }
                                        }}
                                        className="pro-chx"
                                        defaultValue="yes"
                                      />
                                      <div className="select-radio text-center w-100">
                                        <div className="">
                                          <h5 className="mb-0">
                                            {getCurrencySign() +
                                              item?.coupon?.discount_amount +
                                              strings.OFF}
                                          </h5>
                                          <p className="main-amt mb-0">
                                            {strings.ForOrderOver +
                                              " " +
                                              getCurrencySign() +
                                              item?.coupon?.min_total}
                                          </p>
                                          <p className="code mb-0">
                                            {strings.Code_C +
                                              item?.coupon?.coupon_code}
                                          </p>
                                          {/*<hr />*/}
                                          {/*<ul className="coupon-list">*/}
                                          {/*    <li>{moment(item?.coupon?.start_date*1000).format("DD/MM/YYYY hh:mm A")} - {moment(item?.coupon?.end_date*1000).format("DD/MM/YYYY hh:mm A")}</li>*/}
                                          {/*    /!*<li>For all products</li>*!/*/}
                                          {/*</ul>*/}
                                        </div>
                                      </div>
                                    </label>
                                  </li>
                                </>
                              ))
                            ) : (
                              <div className="col-12 text-center padding">
                                <p className="font-13 mb-0 font-italic">
                                  {strings.noCoupons}
                                </p>
                              </div>
                            )}
                            {couponCode ? (
                              <div className="col-12 pl-0">
                                <button
                                  onClick={() =>
                                    couponCode ? removeCoupon() : null
                                  }
                                  className="btn btn-primary px-3"
                                >
                                  {strings.RemoveCoupon}
                                </button>
                              </div>
                            ) : null}
                            <div className="col-12 pl-0">
                              <span className="text-danger small">
                                {errors ? errors : ""}
                              </span>
                            </div>
                            <div className="col-12 pr-0 mr-0 card-button  d-flex justify-content-end">
                              <button
                                onClick={() => setShowCouponModal(true)}
                                className="btn btn-black px-3"
                              >
                                {strings.AddCouponCode}
                              </button>
                            </div>
                            {/**
                            <div className="row">
                              <div className="col-6 pr-0 mr-0 card-button  d-flex justify-content-start">
                                <span className="text-danger small">
                                  {errors ? errors : ""}
                                </span>
                              </div>
                              <div className="col-6 pr-0 mr-0 card-button  d-flex justify-content-end">
                                <button
                                  onClick={() => setShowCouponModal(true)}
                                  className="btn btn-black px-3">
                                  {strings.AddCouponCode}
                                </button>
                              </div>
                            </div>
                            */}
                          </ul>
                        </div>
                      </div>
                      <hr className="d-none d-md-block d-lg-block d-xl-block" />
                      {/*-------coupon code-----*/}
                      <div className="alignment d-none d-md-block d-lg-block d-xl-block">
                        <button
                          onClick={ConfirmBooking}
                          className="btn btn-primary"
                        >
                          {strings.confirmBooking}
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-md-4 col-lg-4 col-xl-4 d-none d-md-block d-lg-block d-xl-block">
                    <div className="bg-white main-content m-0">
                      <h5>{strings.bookingDetails}</h5>
                      <small className="mb-3">
                        {requestData ? requestData.service.name : ""}&nbsp;
                        {strings.with}&nbsp;{requestData?.pets?.length}&nbsp;
                        {
                          pet.find((val) => val?.key === requestData?.pet_type)
                            ?.label
                        }
                      </small>
                      <hr />
                      <div className="boarding-details">
                        <div className="d-flex justify-content-between mb-3">
                          <div>
                            <p className="font-14 text-muted mb-0">
                              {strings.ServiceFee}
                            </p>
                          </div>
                          <div>
                            <p className="font-14 text-muted mb-0">
                              {getCurrencySign() +
                                (requestData ? requestData.sub_total : "0")}
                            </p>
                          </div>
                        </div>
                        <div className="d-flex justify-content-between mb-3">
                          <div>
                            <p className="font-14 text-muted mb-0">
                              {strings.tax}
                            </p>
                          </div>
                          <div>
                            <p className="font-14 text-muted mb-0">
                              {getCurrencySign() +
                                (requestData ? requestData.tax : "0")}
                            </p>
                          </div>
                        </div>
                        {requestData && requestData.coupon_code ? (
                          <div className="d-flex justify-content-between mb-3">
                            <div>
                              <p className="font-14 text-muted mb-0">
                                {strings.discount}
                              </p>
                            </div>
                            <div>
                              <p className="font-14 text-muted mb-0">
                                -{" "}
                                {getCurrencySign() +
                                  (requestData ? requestData.discount : "")}
                              </p>
                            </div>
                          </div>
                        ) : null}

                        {requestData &&
                        requestData.service_discount !== "0.00" ? (
                          <div className="d-flex justify-content-between mb-3">
                            <div>
                              <p className="font-14 text-muted mb-0">
                                {strings.serviceDiscount}
                                <span className="help-tip chat-help-tip">
                                  <p>
                                    {
                                      strings.DiscountAddedBySitterAtTheTimeOfCreateService
                                    }
                                  </p>
                                </span>
                              </p>
                            </div>
                            <div>
                              <p className="font-14 text-muted mb-0">
                                -{" "}
                                {getCurrencySign() +
                                  (requestData
                                    ? requestData.service_discount
                                    : "")}
                              </p>
                            </div>
                          </div>
                        ) : null}

                        <div className="d-flex justify-content-between mb-3">
                          <div>
                            <p className="font-14 text-muted mb-0">
                              {strings.transportFee}
                            </p>
                          </div>
                          <div>
                            <p className="font-14 text-muted mb-0">
                              {getCurrencySign() +
                                (requestData ? requestData.amend_amount : "0")}
                            </p>
                          </div>
                        </div>
                        <hr />
                        <div className="d-flex justify-content-between">
                          <div>
                            <p className="font-14 font-semibold mb-0">
                              {strings.totalAmount}
                            </p>
                          </div>
                          <div>
                            <p className="font-14 font-semibold  mb-0">
                              {getCurrencySign() +
                                (requestData
                                  ? requestData.total_paid_amount
                                  : "0")}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/*----------*/}
              </div>
            </div>
          </div>
          <div className="bg-white main-background mb-0 d-block d-md-none d-lg-none d-xl-none">
            <div className="container">
              <div className="row">
                <div className="col-12 px-0">
                  <div className="d-flex justify-content-between button-design">
                    <div className="proceed-btn">
                      <a>
                        <button className="btn btn-primary">
                          {strings.confirmBooking}
                        </button>
                      </a>
                    </div>
                    <div className="payment-amt">
                      <h6>
                        {getCurrencySign() +
                          (requestData ? requestData.total_paid_amount : "0")}
                      </h6>
                      <svg
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fal"
                        data-icon="info-circle"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 512 512"
                        className="svg-inline--fa fa-info-circle fa-w-16 fa-2x"
                      >
                        <path
                          fill="currentColor"
                          d="M256 40c118.621 0 216 96.075 216 216 0 119.291-96.61 216-216 216-119.244 0-216-96.562-216-216 0-119.203 96.602-216 216-216m0-32C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm-36 344h12V232h-12c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h48c6.627 0 12 5.373 12 12v140h12c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12h-72c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12zm36-240c-17.673 0-32 14.327-32 32s14.327 32 32 32 32-14.327 32-32-14.327-32-32-32z"
                          className=""
                        />
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        show={successView}
        id="successView"
        className="modal-child"
        aria-labelledby="settings"
        tabIndex="-1"
        scrollable
      >
        <div className="main-page mobile-bg">
          <div className="col-12">
            <div className="row justify-content-center">
              <div className="bg-white main-content booking-success m-0 p-4">
                <div className="booking-align">
                  <div className="d-block d-md-none d-lg-none d-xl-none mb-2">
                    <a className="navbar-brand logo">
                      <img src="images/logo.png" />
                    </a>
                  </div>
                  <h3 className="font-semibold">{strings.bookingSuccess}</h3>
                  {/*<p className="font-14 mb-0">{strings.bookingFor} {requestData ? requestData.pet_type === 1 ? 'Dog' : requestData.pet_type === 2 ? 'Cat' : requestData.pet_type === 3 ? 'Bird' : requestData.pet_type === 4 ? 'Reptiles' : 'Small animals' : 'sitting'} {strings.isSuccessful}</p>*/}
                </div>
                <hr className="main-border" />
                <div className="booking-success-details">
                  <h5 className="font-medium mb-3">{strings.ookingDetails}</h5>
                  <h6 className="font-medium mb-3">{strings.bookedFor}</h6>
                  {requestData &&
                    requestData.pets.map((val, index) => (
                      <div key={index} className="col-12 mb-2 p-0">
                        <div className="row">
                          <div className="col-md-5 german-img">
                            <img
                              src={val.pet_image}
                              className="rounded-lg border-success border img-fluid"
                              alt=""
                            />
                          </div>
                          <div className="col-md my-auto pl-md-0">
                            <h6 className="font-medium mb-0">
                              {val.pet_name},{" "}
                              {val.age_year +
                                " " +
                                strings.yrs +
                                " " +
                                val.age_month +
                                " " +
                                strings.months}
                              {/*<span className="font-12 ml-2 font-normal text-muted">Siberian Husky</span>*/}
                            </h6>
                          </div>
                        </div>
                      </div>
                    ))}
                </div>
                {requestData &&
                  requestData.additional_services.map((item) => (
                    <div className="additional-services">
                      <h6 className="font-medium">
                        {strings.additionalService}
                      </h6>

                      <div className="d-flex justify-content-between">
                        <div>
                          <h6 className="font-14 font-medium mb-0">
                            {item.name}
                          </h6>
                        </div>
                        <div>
                          <h6 className="font-14 font-medium mb-0">
                            {getCurrencySign() + item.price}
                          </h6>
                        </div>
                      </div>
                    </div>
                  ))}
                <hr />
                <div className="subtotal">
                  <div className="d-flex justify-content-between mb-2">
                    <div>
                      <p className="font-12 text-muted mb-0">
                        {strings.ServiceFee}
                      </p>
                    </div>
                    <div>
                      <p className="font-12 text-muted mb-0">
                        {getCurrencySign() +
                          (requestData ? requestData.sub_total : "")}
                      </p>
                    </div>
                  </div>
                  <div className="d-flex justify-content-between mb-2">
                    <div>
                      <p className="font-12 text-muted mb-0">
                        {strings.transportationFee}
                      </p>
                    </div>
                    <div>
                      <p className="font-12 text-muted mb-0">
                        {getCurrencySign() +
                          (requestData ? requestData.amend_amount : "")}
                      </p>
                    </div>
                  </div>
                  <div className="d-flex justify-content-between mb-2">
                    <div>
                      <p className="font-12 text-muted mb-0">{strings.tax}</p>
                    </div>
                    <div>
                      <p className="font-12 text-muted mb-0">
                        {getCurrencySign() +
                          (requestData ? requestData.tax : "")}
                      </p>
                    </div>
                  </div>
                  {requestData && requestData.coupon_code ? (
                    <div className="d-flex justify-content-between mb-2">
                      <div>
                        <p className="font-12 text-muted mb-0">
                          {strings.discount}
                        </p>
                      </div>
                      <div>
                        <p className="font-12 text-muted mb-0">
                          -{" "}
                          {getCurrencySign() +
                            (requestData ? requestData.discount : "")}
                        </p>
                      </div>
                    </div>
                  ) : null}
                  {requestData && requestData.service_discount !== "0.00" ? (
                    <div className="d-flex justify-content-between mb-2">
                      <div>
                        <p className="font-12 text-muted mb-0">
                          {strings.serviceDiscount}
                          <span className="help-tip chat-help-tip">
                            <p>
                              {
                                strings.DiscountAddedBySitterAtTheTimeOfCreateService
                              }
                            </p>
                          </span>
                        </p>
                      </div>
                      <div>
                        <p className="font-12 text-muted mb-0">
                          -{" "}
                          {getCurrencySign() +
                            (requestData ? requestData.service_discount : "")}
                        </p>
                      </div>
                    </div>
                  ) : null}
                </div>
                <hr />
                <div className="d-flex justify-content-between">
                  <div>
                    <p className="font-14 font-semibold mb-0">
                      {strings.totalAmount}
                    </p>
                  </div>
                  <div>
                    <p className="font-14 font-semibold  mb-0">
                      {getCurrencySign() +
                        (requestData ? requestData.total_paid_amount : "")}
                    </p>
                  </div>
                </div>
                <hr />
                <div className="booking-button col-12 d-flex align-items-center justify-content-between p-0">
                  <button
                    onClick={() => {
                      router.replace("/search-sitter");
                    }}
                    className="btn btn-primary px-4 mr-1"
                  >
                    {strings.MakeAnotherBooking}
                  </button>
                  <button
                    onClick={() => {
                      router.replace("/user/dashboard");
                    }}
                    className="btn btn-primary px-4 ml-1"
                  >
                    {strings.GoToDashboard}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <AddCouponModal
        updateCoupon={() => getCoupons()}
        showModal={showCouponModal}
        hideModal={() => setShowCouponModal(false)}
      />
    </>
  );
};

export default PaymentScreen;
