import Reviews from "../../components/sitterProfile/Reviews";
import API from "../../api/Api";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { strings } from "../../public/lang/Strings";
import Cookies from "universal-cookie";
import moment from "moment";
import SpotReviewObject from "../../components/petSpot/singleSpot/SpotReviewObject";
import Select from "react-select";
import { reviewSorting, starRating } from "../../public/appData/AppData";

const SpotReviews = () => {
  const api = new API();
  const router = useRouter();
  const [data, setData] = useState<any>([]);
  const cookie = new Cookies();
  const [sitterReview, setSitterReview] = useState<boolean>(true);
  const [imageCounter, setImageCounter] = useState(0);
  const [lastImageUrl, setLastImageUrl] = useState("");
  const [filter, setFilter] = useState({
    spot_id: router.query.id,
    sort: 1,
    filter: 0,
  });

  useEffect(() => {
    if (router.query.id) {
      getSpotReviews(filter);
    }
  }, [router.query.id, filter]);

  const getSpotReviews = (filter: any) => {
    api
      .getSingleSpotReviews(filter)
      .then((res) => {
        setData(res.data.response);
        console.log(res.data.response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="container">
      <div className="sort-details py-2">
        <div className="col-12 mt-2 mb-0 px-0">
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-md-flex d-none  high-rating align-center ml-0 filter-design">
              <div className="d-flex align-items-center col-6 col-sm-auto col-md form-group mb-0">
                <label
                  className="form-label show show1"
                  style={{ whiteSpace: "nowrap" }}
                >
                  {strings.SortBy}
                </label>
                <Select
                  defaultValue={reviewSorting[0]}
                  onChange={(e) => {
                    console.log(e.value);
                    setFilter(prevFilter => ({
                      ...prevFilter,
                      sort: e.value, 
                    }));
                  }}
                  options={reviewSorting}
                  // styles={this.customStyles}
                />
              </div>
              <div className="d-flex align-items-center col-6 col-sm-auto col-md form-group mb-0">
                <label className="form-label show show1">
                  {strings.StarRating}
                </label>
                <Select
                  defaultValue={starRating[0]}
                  // value={this.state.closest_sitter}
                  // isSearchable={false}
                  onChange={(e) => {
                    console.log(e.value);
                    setFilter(prevFilter => ({
                      ...prevFilter,
                      filter: e.value, 
                    }));
                  }}
                  options={starRating}
                  // styles={this.customStyles}
                />
              </div>
            </div>
            <div className="col-md-auto col-12 p-0 align-center  mt-md-0">
              <div className="font-12 mb-0 w-100 align-center d-flex align-items-center justify-content-between d-lg-block"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="main-wrapper">
        {data ? (
          <div className="bg-white main-background">
            <h5 className="font-semibold mb-0">{strings.Reviews}</h5>
            <hr />
            {data?.length
              ? data?.map((value, index) => <SpotReviewObject value={value} />)
              : strings.ReviewsAreNotGivenYet}
          </div>
        ) : null}
      </div>
    </div>
  );
};
export default SpotReviews;
