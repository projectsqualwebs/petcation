// @ts-nocheck
import React, { useContext, useEffect, useState } from "react";
import SpotObjectWeb from "../../components/petSpot/searchSpot/SpotObjectWeb";
import { strings } from "../../public/lang/Strings";
import Select from "react-select";
import { pet, sitterSorting, sort } from "../../public/appData/AppData";
import { useSelector } from "react-redux";
import API from "../../api/Api";
import { useRouter } from "next/router";
import { I_SPOT } from "../../components/petSpot/SpotSlider";
import { GOOGLE_PLACES_API } from "../../api/Constants";
import MyMapComponent from "../../components/user/myProfile/Map";
import LocationSearchInput2 from "../../components/common/LocationSearchInput2";
import AppContext from "../../utils/AppContext";
import Loader from "../../components/common/Loader";

// import Autocomplete from "react-google-autocomplete";

interface LAT_LNG {
  lat: string;
  lng: string;
}
interface CATEGORY {
  id: number;
  label: string;
  value: string;
}
interface PET_TYPE {
  key: number;
  label: string;
  value: string;
}
interface SORT_BY {
  key: number;
  label: string;
  value: string;
}
interface I_FILTER {
  formatted_address: string;
  locality: string;
  postal_code: number;
  city: string;
  category: CATEGORY;
  pet_type: PET_TYPE;
  sort_by_review: SORT_BY;
  latLng: LAT_LNG;
}

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    fontSize: 14,
  }),

  control: (provided) => ({
    ...provided,
    // width: 120,
  }),
  singleValue: (provided, state) => {
    return { ...provided, fontSize: 12, color: "#383838", fontWeight: "500" };
  },
};

const api = new API();

export default function SearchSpot(props) {
  const router = useRouter();
  const context = useContext(AppContext);
  const [categories, setCategories] = useState<I_CATEGORY[]>([]);
  const [spotLatLng, setSpotLatLng] = useState([]);
  const [hoveredSpot, setHoveredSpot] = useState(null);
  // const [city, setCity] = useState();
  const spotData = useSelector((state: SpotData) => state);
  const [value, setValue] = useState<any>("");

  const [spots, setSpots] = useState<I_SPOT[]>([]);
  const [loading, setLoading] = useState(true);
  const [filter, setFilter] = useState<I_FILTER>({
    formatted_address: "",
    locality: "",
    postal_code: null,
    city: "",
    category: null,
    pet_type: null,
    sort_by_review: null,
    latLng: {
      lat: null,
      lng: null,
    },
  });
  const [data, setData] = useState<any>({
    category: null,
    pet_type: null,
    sort: null,
    latitude: null,
    longitude: null,
  });

  const resetFilter = () => {
    setFilter({
      ...filter,
      latLng: {
        lat: null,
        lng: null,
      },
    });
  };
  const [wishList, setWishlist] = useState<any>([]);

  useEffect(() => {
    getAllWishlist();
  }, []);
  const getAllWishlist = () => {
    api
      .getSpotLists()
      .then(async (res) => {
        await setWishlist(res.data.response);
      })
      .catch((err) => console.log(err.response.data.message));
  };

  useEffect(() => {
    if (!spotData.spotReducer.spotCategory.length) {
      getPetSpotCategories();
    } else {
      let data = spotData.spotReducer.spotCategory.response.map(
        (val, index) => {
          if (String(val.id) === String(router.query.id)) {
            setFilter({
              ...filter,
              category: { id: val.id, label: val.name, value: val.name },
            });
          }
          return { id: val.id, label: val.name, value: val.name };
        }
      );
      setCategories(data);
    }
  }, []);

  useEffect(() => {
    if (router.query.id) {
      let props = {
        category: Number(router.query.id),
        pet_type: filter?.pet_type?.key,
        sort: filter?.sort_by_review?.key,
        latitude: data?.latitude,
        longitude: data?.longitude,
      };
      getSpots(props);
      categories.map((cat) => {
        if (`${cat.id}` == router.query.id) {
          setFilter({
            ...filter,
            category: { id: cat.id, value: cat.label, label: cat.value },
          });
        }
      });
    } else if (context.state) {
      let that = context.state;
      setFilter({
        ...filter,
        formatted_address: that?.formatted_address,
        locality: that?.locality,
        postal_code: that?.postal_code,
        city: that?.city,
        latLng: that?.latLng,
      });
      setValue(that?.formatted_address);
      setData({
        ...data,
        latitude: that.latLng?.lat,
        longitude: that.latLng?.lng,
      });
      let props = {
        category: filter?.category?.id,
        pet_type: filter?.pet_type?.key,
        sort: filter?.sort_by_review?.key,
        latitude: that.latLng?.lat,
        longitude: that.latLng?.lng,
      };
      console.log({ props });
      getSpots(props);
      context.setState({});
    } else {
      handleSearchSpot();
    }
  }, [filter.pet_type, filter.sort_by_review]);

  //Get spot categories
  const getPetSpotCategories = () => {
    api
      .getPetSpotsCategory()
      .then((res) => {
        let data = res.data.response.map((val, index) => {
          if (String(val.id) === String(router.query.id)) {
            setFilter({
              ...filter,
              category: { id: val.id, label: val.name, value: val.name },
            });
            let sdata = {
              category: val?.id,
              pet_type: filter.pet_type?.key,
              sort: filter.sort_by_review?.key,
              latitude: data?.latitude,
              longitude: data?.longitude,
            };
            getSpots(sdata);
          }
          return { id: val.id, label: val.name, value: val.name };
        });
        setCategories(data);
      })
      .catch((error) => {});
  };

  const getSpots = (data) => {
    setLoading(true);
    api
      .getPetSpots(data)
      .then((res) => {
        if (res.data.response) {
          setSpots(res.data.response);
        }
        if (res.data.response?.length) {
          setSpotLatLng(
            res.data.response.map((val) => ({
              lat: Number(val.latitude),
              lng: Number(val.longitude),
              id: val.id,
            }))
          );
          setHoveredSpot({
            lat: Number(res.data.response[0].latitude),
            lng: Number(res.data.response[0].longitude),
            id: res.data.response[0].id,
          });
        } else {
          setSpotLatLng([]);
          setHoveredSpot(null);
        }
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleSearchSpot = () => {
    let data = {
      category: filter.category?.id,
      pet_type: filter.pet_type?.key,
      sort: filter.sort_by_review?.key,
      latitude: data?.latitude,
      longitude: data?.longitude,
    };
    getSpots(data);
  };

  return (
    <>
      <div className="main-wrapper search-page">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 mob-filter-tab">
              <div className="row">
                <div className="col">
                  <div
                    className="d-flex align-items-center justify-content-between col-12 form-group mb-0"
                    onClick={() => this.setState({ showMobileFilter: true })}
                  >
                    <label className="form-label show show1">Filter</label>
                    <svg
                      height="20"
                      width="20"
                      viewBox="0 0 20 20"
                      aria-hidden="true"
                      focusable="false"
                      className="css-tj5bde-Svg"
                    >
                      <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                    </svg>
                  </div>
                </div>
                <div className="col drop">
                  <div className="d-flex align-items-center justify-content-between col-12 form-group mb-0">
                    <Select
                      onChange={(e) => {
                        setFilter({
                          ...filter,
                          sort_by_review: e,
                        });
                      }}
                      value={filter.sort_by_review ? filter.sort_by_review : ""}
                      styles={customStyles}
                      isSearchable={false}
                      options={sort}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-8">
              <div className="filter-section">
                <div className=" col-12 filter-design">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                      <div className="row">
                        <div className="col-12 col-sm-6">
                          <div
                            id={"search_spot"}
                            key={"search_spot"}
                            className="form-group mb-0 search-text location-search-input"
                          >
                            <label className="mb-0 pb-0">
                              {strings.SearchLocation}
                            </label>
                            <LocationSearchInput2
                              key={"search_spot"}
                              value={value}
                              setFilter={(Address) => {
                                setValue(Address.value);
                                setFilter({
                                  ...filter,
                                  formatted_address: Address.value,
                                });
                              }}
                              setLatLng={(latLng) => {
                                setData({
                                  ...data,
                                  latitude: String(latLng?.lat),
                                  longitude: String(latLng?.lng),
                                });
                                let sdata = {
                                  category: filter.category?.id,
                                  pet_type: filter.pet_type?.key,
                                  sort: filter.sort_by_review?.key,
                                  latitude: String(latLng?.lat),
                                  longitude: String(latLng?.lng),
                                };
                                getSpots(sdata);
                              }}
                              setValue={(address) => {
                                if (address == "") {
                                  setValue(address);
                                  resetFilter();
                                }
                              }}
                            />
                          </div>
                        </div>
                        <div className="col-sm px-md-0">
                          <div className="d-flex justify-content-between filter-design">
                            <div className="col-sm form-group mb-0">
                              <label>{strings.Categories}</label>
                              <Select
                                className="w-100"
                                onChange={(e) => {
                                  setFilter({
                                    ...filter,
                                    category: e,
                                  });
                                  let sdata = {
                                    category: e?.id,
                                    pet_type: filter.pet_type?.key,
                                    sort: filter.sort_by_review?.key,
                                    latitude: data?.latitude,
                                    longitude: data?.longitude,
                                  };
                                  getSpots(sdata);
                                }}
                                value={filter.category ? filter.category : ""}
                                isSearchable={false}
                                options={categories}
                                styles={customStyles}
                              />
                            </div>
                            <div className="col-sm-auto form-group mb-0">
                              <label>{strings.petType}</label>
                              <Select
                                onChange={(e) => {
                                  setFilter({
                                    ...filter,
                                    pet_type: e,
                                  });
                                }}
                                value={filter.pet_type ? filter.pet_type : ""}
                                isSearchable={false}
                                options={pet}
                                styles={customStyles}
                              />
                            </div>
                            <div className="col-sm form-group mb-0">
                              <label>
                                {strings.Sortby + " " + strings.reviews}
                              </label>
                              <Select
                                onChange={(e) => {
                                  setFilter({
                                    ...filter,
                                    sort_by_review: e,
                                  });
                                }}
                                value={
                                  filter.sort_by_review
                                    ? filter.sort_by_review
                                    : ""
                                }
                                styles={customStyles}
                                isSearchable={false}
                                options={sort}
                              />
                            </div>
                            <div className="col-auto pl-0 mt-auto">
                              <button
                                onClick={() => handleSearchSpot()}
                                className="btn btn-save px-2"
                              >
                                {strings.Search}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="container-fluid">
                <div className="sort-details py-2">
                  <div className="row">
                    <div className="col-12 mt-2 mb-3 px-0">
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="d-md-flex d-none  high-rating align-center ml-0 filter-design">
                          <div>
                            <p className="font-12 mb-0 mt-3 mt-md-0">
                              {spots.length +
                                " " +
                                strings.resultsAsPerPreferences}{" "}
                              <a
                                className="cursor-pointer"
                                onClick={resetFilter}
                              >
                                ({strings.reset})
                              </a>
                            </p>
                          </div>
                          <div className="high-rating d-none">
                            <div className="form-label show show1">
                              {strings.SortBy} :
                            </div>
                            <div className="form-group">
                              <div className="category-selection charge-select">
                                <Select
                                  styles={customStyles}
                                  isSearchable={false}
                                  options={sort}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/*-------------code for web view------------------*/}
              <div className="col-12 px-2">
                <div className="row">
                  {loading ? (
                    <>
                      <Loader />
                    </>
                  ) : (
                    <>
                      {spots.length ? (
                        spots.map((item, index) => (
                          <div
                            className="col-12"
                            onMouseEnter={() =>
                              setHoveredSpot({
                                lat: Number(item?.latitude),
                                lng: Number(item?.longitude),
                                id: item?.id,
                              })
                            }
                          >
                            <SpotObjectWeb
                              type={2}
                              wishlist={wishList}
                              updateWishlist={() => getAllWishlist()}
                              key={index}
                              data={item}
                              updateSpots={() => handleSearchSpot()}
                            />
                          </div>
                        ))
                      ) : (
                        <div className="col-12 no-data p-0">
                          <div className="bg-white search-background p-5 mt-4 col-12">
                            <h3 className="text-uppercase font-weight-bold">
                              {strings.ResultNotFound}
                            </h3>
                          </div>
                        </div>
                      )}
                    </>
                  )}
                </div>
              </div>
            </div>
            <div className="col-12 col-md-4 map-full d-md-block d-none">
              <div className="mapouter h-100">
                <div className="gmap_canvas h-100">
                  <MyMapComponent
                    // serviceId={this.state.service.value}
                    data={spotLatLng}
                    draggable={false}
                    latlng={spotLatLng.length ? spotLatLng : []}
                    defaultCenter={
                      hoveredSpot
                        ? hoveredSpot
                        : { lat: 36.2048, lng: 138.2529 }
                    }
                    mapCenter={
                      hoveredSpot
                        ? hoveredSpot
                        : { lat: 36.2048, lng: 138.2529 }
                    }
                    mapZoom={10}
                    currentHoveringSitter={hoveredSpot}
                    isSpot={true}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
