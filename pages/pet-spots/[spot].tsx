import React, { useEffect, useState } from "react";
import SingleSpotHeaderMobile from "../../components/petSpot/singleSpot/SingleSpotHeaderMobile";
import SingleSpotHeader from "../../components/petSpot/singleSpot/SingleSpotHeader";
import GalleryView from "../../components/sitterProfile/GalleryView";
import SpotSlider, { I_SPOT } from "../../components/petSpot/SpotSlider";

import API from "../../api/Api";
import I_PET from "../../models/pet.interface";
import { strings } from "../../public/lang/Strings";
import { serviceData, sitterServices, pet, petSize } from "../../public/appData/AppData";
import { LightgalleryProvider, LightgalleryItem, withLightgallery, useLightgallery } from "react-lightgallery";
import "lightgallery.js/dist/css/lightgallery.css";
import { useSnackbar } from 'react-simple-snackbar';
import SpotRating from "../../components/common/SpotRating";
import MyMapComponent from "../../components/user/myProfile/Map";
import Link from "next/link";
import {useRouter} from "next/router";
import moment from "moment";
import RatingStars from "../../components/common/RatingStars";

let api = new API();
export interface I_Languages {
    language_id: number;
}
interface I_SERVICE {
    service_id: number;
}


export default function SingleSpot() {
    // const [spotDetail, setSpotDetail] = useState<SpotDetailInterface>();
    const [spotDetail, setSpotDetail] = useState<any>();
    const [reservationTypes, setReservationTypes] = useState(null);
    const [smokingCessions, setSmokingCessions] = useState<any>(null);
    const [parking, setParking] = useState<any>(null);
    const [langauges, setLanguage] = useState<any>(null);
    const [paymentMethods, setPaymentMethods] = useState<any>([]);
    const [featuredSpots, setFeaturedSpots] = useState<I_SPOT[]>([]);
    const [showModal, setShowModal] = useState(false);
    const [category, setCategory] = useState<any>();
    const [categoryName, setCategoryName] = useState<any>('');
    const [address, setAddress] = useState<any>({
        lat: '',
        lng: '',
    })

    const router = useRouter();
    const [openSnackbar] = useSnackbar();


    useEffect(() => {
        getPetSpotCategories();
    }, [category]);

    useEffect(() => {
        if (spotDetail) {
            setCategory(spotDetail.category_id);
            setAddress({
                lat: spotDetail.latitude,
                lng: spotDetail.longitude,
            })
        }
    }, [spotDetail]);

    const getPetSpotCategories = () => {
        api
            .getPetSpotsCategory()
            .then((res) => {
                res.data.response.map((val, index) => {
                    if (val.id == category) {
                        setCategoryName(val.name)
                    }
                })
            })
            .catch((error) => { });
    };

    useEffect(() => {
        getReservationTypes();
        getSmokingCession();
        getParkings();
        getLanguages();
        getPaymentMethods();
        getFeaturedPetSpot()
    }, []);


    useEffect(() => {

        if (router.query.spot) {
            getSpotDetail(router.query.spot)
        }
    }, [router.query]);

    const getFeaturedPetSpot = () => {
        api
            .getFeaturedPetSpots({})
            .then((res) => {
                if (res.data.response) {
                    setFeaturedSpots(res.data.response);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const getReservationTypes = () => {
        api
            .getReservationTypes()
            .then((res) => {
                setReservationTypes(res.data.response);
            })
            .catch((error) => {
            });
    };

    const getSmokingCession = () => {
        api
            .getSmokingCession()
            .then((res) => {
                setSmokingCessions(res.data.response);
            })
            .catch((error) => {
            });
    };

    const getParkings = () => {
        api
            .getParking()
            .then((res) => {
                setParking(res.data.response);
            })
            .catch((error) => {
            });
    };

    const getLanguages = () => {
        api
            .getLangauges()
            .then((res) => {
                setLanguage(res.data.response);
            })
            .catch((error) => {
            });
    };

    const getPaymentMethods = () => {
        api
            .getPaymentMethods()
            .then((res) => {
                setPaymentMethods(res.data.response);
            })
            .catch((error) => {
            });
    };


    const getSpotDetail = (id) => {
        api
            .getPetSpotDetails(id)
            .then((res) => {
                if (res.data) {
                    setSpotDetail(res.data);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    const horizontalView = (props) => (
        <div className="row">
            <div className="col-xl-4 my-auto">
                <div className="spot-inner-details">
                    <p className="text-muted mb-0">{props.name}</p>
                </div>
            </div>
            {props.isAward ? awardView() : <div className="col-xl-8">
                <div className="spot-inner-details">
                    <h6 className="mb-0 font-14">{props.value}</h6>
                </div>
            </div>}
        </div>
    )

    const awardView = () => (
        <div className="awards-details">
            <svg
                aria-hidden="true"
                focusable="false"
                data-prefix="fas"
                data-icon="shield-check"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
                className="svg-inline--fa fa-shield-check fa-w-16 fa-2x"
            >
                <path
                    fill="currentColor"
                    d="M466.5 83.7l-192-80a48.15 48.15 0 0 0-36.9 0l-192 80C27.7 91.1 16 108.6 16 128c0 198.5 114.5 335.7 221.5 380.3 11.8 4.9 25.1 4.9 36.9 0C360.1 472.6 496 349.3 496 128c0-19.4-11.7-36.9-29.5-44.3zm-47.2 114.2l-184 184c-6.2 6.2-16.4 6.2-22.6 0l-104-104c-6.2-6.2-6.2-16.4 0-22.6l22.6-22.6c6.2-6.2 16.4-6.2 22.6 0l70.1 70.1 150.1-150.1c6.2-6.2 16.4-6.2 22.6 0l22.6 22.6c6.3 6.3 6.3 16.4 0 22.6z"
                />
            </svg>
            <h6 className="mb-0 font-medium d-inline">2018{" " + strings.GOLD}</h6>
        </div>
    )

    // const OpenButtonWithHook = (props: any) => {
    //     const { openGallery } = useLightgallery();
    //     return (
    //         <button
    //             {...props}
    //             onClick={(e) => {
    //                 openGallery(spotDetail.images, 0)
    //             }}
    //             className="btn btn-sm btn-orange"
    //         >
    //             {"Show all " + spotDetail.images.length}
    //         </button>
    //     );
    // };

    const getImages = () => {
        let array = [];
        let imgArr = spotDetail.images.map((val) => val.path)
        for (let i = 0; i < imgArr.length; i++) {
            if (i === 4) {
                array.push(
                    <div
                        key={`image_${i}`}
                        style={{ display: "block" }}
                        className="col-md-6 col-sm-3 holder-sm spot-grid col-6 image-card"
                    >
                        <LightgalleryItem group={imgArr} src={imgArr[i]} thumb={imgArr[i]}>
                            {imgArr.length > 5 ? <div className='show-image-plus-option m-0 p-0'>
                                <img className="img-fluid" style={{opacity: 0.2}} src={imgArr[i]} alt="img" />
                                <span className='fw-bold'>+{imgArr.length - 5}</span>
                            </div> : <img className="img-fluid" src={imgArr[i]} alt="img" />}
                        </LightgalleryItem>
                    </div>
                );
            } else {
                array.push(
                    <div
                        key={`image${i}`}
                        style={{ display: i == 0 || i > 4 ? "none" : "block" }}
                        className="col-md-6 col-sm-3 holder-sm spot-grid col-6 image-card"
                    >
                        <LightgalleryItem group={imgArr} src={imgArr[i]} thumb={imgArr[i]}>
                            <img className="img-fluid" src={imgArr[i]} alt="img" />
                        </LightgalleryItem>
                    </div>
                );
            }
        }
        return array;
    };

    const scrollToTop = () => {
        if (window) {
            window.scroll({ top: 0, left: 0, behavior: "smooth" });
        }
    };

    const OpenButtonWithHook = props => {
        const { openGallery } = useLightgallery();
        return (
            <a
                {...props}
                onClick={() => openGallery("group2")}
                className='text-theme-primary cursor-pointer'
            >
               <small>{strings.ViewImages}</small>
            </a>
        );
    };


    return (
        <div className="main-wrapper bottom mt-0">
            {/*-------1st section----*/}
            <div className="main-image d-none d-md-block d-lg-block d-xl-block">
                <img src={'/images/banner2.png'}
                    className="img-fluid" />
            </div>
            {/*-------/1st section----*/}
            {/*-------2nd section----*/}
            <SingleSpotHeader showModal={() => setShowModal(true)} updateSpot={(id) => { router.query ? getSpotDetail(id) : '' }} data={spotDetail} />
            {/*-------/2nd section----*/}
            {/*-----3rd section------*/}
            <div className="container">
                {/* {spotDetail && <GalleryView images={spotDetail.images} />} */}
                <div className="bg-white main-background gallery-grid">
                    {spotDetail && <LightgalleryProvider>
                        {/* <OpenButtonWithHook /> */}
                        <div className="row">
                            <div className="col-md-6 col-sm-12 card-holder">
                                <div className="row">
                                    <div className="col-12 image-card">
                                        <LightgalleryItem
                                            group={spotDetail.images}
                                            src={spotDetail.images[0].path}
                                            thumb={spotDetail.images[0].path}
                                        >
                                            <img className="img-fluid cursor-pointer" src={spotDetail.images[0].path}
                                                alt="img" />
                                        </LightgalleryItem>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-12 d-sm-block d-none card-holder pr-4">
                                <div className="row cursor-pointer">{getImages().map((value) => value)}</div>
                            </div>
                        </div>
                    </LightgalleryProvider>}
                </div>
            </div>
            {/*-----/3rd section------*/}
            {/*-----4th section------*/}
            <div className="container">
                <div className="row">

                    <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                        <div className="bg-white main-background">
                            <h5 className="font-semibold font-20">{strings.Hi}</h5>
                            <p>
                                {spotDetail ? spotDetail.spot_description : ''}
                            </p>
                        </div>
                        <div className="bg-white main-background">
                            <h4 className="font-semibold mb-0">{strings.SpotDetails}</h4>
                            <hr />
                            {horizontalView({
                                name: categoryName + " " + strings.name,
                                value: spotDetail ? (spotDetail.english_name + ' | ' + spotDetail.spot_name) : ''
                            })}
                            <hr />
                            {horizontalView({ name: strings.gener, value: categoryName })}
                            <hr />
                            {horizontalView({ name: strings.Address, value: spotDetail ? spotDetail.address : '' })}
                          {spotDetail && spotDetail.operations && spotDetail.operations.means_of_transport && <hr />}
                            {spotDetail && spotDetail.operations && spotDetail.operations.means_of_transport && horizontalView({
                                name: strings.transportation,
                                value: spotDetail.operations.means_of_transport,
                            })}
                            <hr />
                            <div className="row">
                                <div className="col-xl-4 my-auto">
                                    <div className="spot-inner-details">
                                        <p className="text-muted mb-0">{strings.OpenHours}</p>
                                    </div>
                                </div>
                                <div className="col-xl-8">
                                    <div className="spot-inner-details">
                                        <div className="mb-2">
                                            {/*<p className="mb-1 font-12">[{spotDetail ? spotDetail.operations.means_of_transport:""}]</p>*/}
                                            <h6 className="mb-0 font-12">
                                                {spotDetail ? spotDetail.operations.business_hours : ''}
                                            </h6>
                                        </div>
                                        <div>
                                            <p className="mb-0 font-14">{(spotDetail ? spotDetail.operations.is_open_on_sunday ? strings.Open : strings.Closed : strings.Closed) + " " + strings.onSunday}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          {spotDetail && spotDetail.operations && spotDetail.operations.regular_holidays && <hr />}
                            {spotDetail && spotDetail.operations && spotDetail.operations.regular_holidays && horizontalView({
                                name: strings.spotHolidays,
                                value: spotDetail.operations.regular_holidays
                            })}
                            <hr />
                            <div className="row">
                                <div className="col-xl-4 my-auto">
                                    <div className="spot-inner-details">
                                        <p className="text-muted mb-0">{strings.Budget}</p>
                                    </div>
                                </div>
                                <div className="col-xl-8">
                                    <div className="spot-inner-details">
                                        <div className="mb-3">
                                            <h6 className="mb-2 font-14">{strings.noonCharges}</h6>
                                            <h6 className="mb-0 font-14">{spotDetail && sitterServices.length > spotDetail.budgets.noon_charge_id - 1 && sitterServices[spotDetail.budgets.noon_charge_id - 1] ? sitterServices[spotDetail.budgets.noon_charge_id - 1].price : 0}</h6>
                                        </div>
                                        <div>
                                            <h6 className="mb-2 font-14">{strings.nightCharges}</h6>
                                            <h6 className="mb-0 font-14">{spotDetail && sitterServices.length > spotDetail.budgets.night_charge_id - 1 && sitterServices[spotDetail.budgets.noon_charge_id - 1] ? sitterServices[spotDetail.budgets.night_charge_id - 1].price : 0}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            {horizontalView({
                                name: strings.methodOfPayment,
                                value: spotDetail && paymentMethods && paymentMethods.find((val) => val.id == spotDetail.budgets.payment_method_id)?.name
                            })}
                            <hr />
                            {horizontalView({
                                name: strings.serviceCharges,
                                value: spotDetail && spotDetail.budgets.service_charge + strings.P_Service_Charge
                            })}
                          <hr />
                          <div className="row">
                            <div className="col-xl-4 my-auto">
                              <div className="spot-inner-details">
                                <p className="text-muted mb-0">{strings.Availablefor}</p>
                              </div>
                            </div>
                            <div className="col-xl-8">
                              <div className="spot-inner-details">
                                {spotDetail && pet && spotDetail.pets.length ?
                                  <h6>{spotDetail.pets.map((item, index)=> pet.find((val) => val.key === item.id) ? spotDetail.pets.length-1 > index ? `${pet.find((val) => val.key === item.id).label}, ` :pet.find((val) => val.key === item.id).label : null)}</h6>
                                : strings.Unconfirmed}
                              </div>
                            </div>
                          </div>
                            <hr />
                            <div className="row">
                                <div className="col-xl-4 my-auto">
                                    <div className="spot-inner-details">
                                        <p className="text-muted mb-0">{strings.reservations}</p>
                                    </div>
                                </div>
                                <div className="col-xl-8">
                                    <div className="spot-inner-details">
                                        <h6 className="mb-0">{spotDetail && reservationTypes && reservationTypes.find((val) => val.id === spotDetail.amenities.reservation_type_id).name}</h6>
                                        <p className="mb-0 font-14">
                                            {spotDetail && spotDetail.amenities.reservation_info}
                                        </p>
                                    </div>
                                </div>
                            </div>
                          <hr />
                          <div className="row">
                            <div className="col-xl-4 my-auto">
                              <div className="spot-inner-details">
                                <p className="text-muted mb-0">{strings.smoking}</p>
                              </div>
                            </div>
                            <div className="col-xl-8">
                              <div className="spot-inner-details">
                                <h6 className="mb-0">{spotDetail && smokingCessions && smokingCessions.find((val) => val.id === spotDetail.amenities.smoking_cessation_id).name}</h6>
                                <p className="mb-0 font-14">
                                  {spotDetail && spotDetail.amenities.smoking_cessation_info}
                                </p>
                              </div>
                            </div>
                          </div>
                            <hr />
                            <div className="row">
                                <div className="col-xl-4 my-auto">
                                    <div className="spot-inner-details">
                                        <p className="text-muted mb-0">{strings.parking}</p>
                                    </div>
                                </div>
                                <div className="col-xl-8">
                                    <div className="spot-inner-details">
                                        <h6 className="mb-0">{spotDetail && parking && parking.find((val) => val.id === spotDetail.amenities.parking_id).name}</h6>
                                        <p className="mb-0 font-14">
                                            {spotDetail && spotDetail.amenities.parking_info}
                                        </p>
                                    </div>
                                </div>
                            </div>
                          {spotDetail && spotDetail.amenities && spotDetail.amenities.service_amenities_info && <hr />}
                            {spotDetail && spotDetail.amenities && spotDetail.amenities.service_amenities_info && <div className="row">
                                <div className="col-xl-4 my-auto">
                                    <div className="spot-inner-details">
                                        <p className="text-muted mb-0">{strings.serviceAmenities}</p>
                                    </div>
                                </div>
                                <div className="col-xl-8">
                                    <div className="spot-inner-details">
                                        <h6 className="mb-0">{strings.Has} {spotDetail && spotDetail.amenities.services.map((item, index) => serviceData && serviceData.find((val) => val.value === item.service_id) ? spotDetail.amenities.services.length-1 > index ? `${serviceData.find((val) => val.value === item.service_id).label}, ` : serviceData.find((val) => val.value === item.service_id).label : null)} service</h6>
                                        <p className="mb-0 font-14">
                                            {spotDetail && spotDetail.amenities.service_amenities_info}
                                        </p>
                                    </div>
                                </div>
                            </div>}
                            <hr />
                            <div className="row">
                                <div className="col-xl-4 my-auto">
                                    <div className="spot-inner-details">
                                        <p className="text-muted mb-0">{strings.languages}</p>
                                    </div>
                                </div>
                                <div className="col-xl-8">
                                    <div className="spot-inner-details">
                                        {spotDetail && spotDetail.amenities ?
                                            <h6 className="mb-0">{spotDetail.amenities.languages.map((item, index) => langauges && langauges.find((val) => val.id === item.language_id) ? spotDetail.amenities.languages.length-1 > index ? `${langauges.find((val) => val.id === item.language_id).name}, ` : langauges.find((val) => val.id === item.language_id).name : null)}</h6>
                                         : null}
                                        <p className="mb-0 font-14">{spotDetail && spotDetail.amenities && spotDetail.amenities.language_info}</p>
                                    </div>
                                </div>
                            </div>
                          {spotDetail && spotDetail.amenities && (spotDetail.amenities.homepage || spotDetail.amenities.facebook_url || spotDetail.amenities.instagram_url || spotDetail.amenities.twitter_url) && <hr />}
                          {spotDetail && spotDetail.amenities && (spotDetail.amenities.homepage || spotDetail.amenities.facebook_url || spotDetail.amenities.instagram_url || spotDetail.amenities.twitter_url) && <div className="row">
                            <div className="col-xl-4 my-auto">
                              <div className="spot-inner-details">
                                <p className="text-muted mb-0">{strings.URLs}</p>
                              </div>
                            </div>
                            <div className="col-xl-8">
                              <div className="row mb-2">
                                {spotDetail && spotDetail.amenities && spotDetail.amenities.homepage ? <div className="col-sm-6 my-1">
                                  <a target='_blank' href={spotDetail.amenities.homepage}>
                                  <div className="fb-icon cursor-pointer">
                                    <img src="/images/sitter.png" className="img-fluid" />
                                    {strings.Homepage}
                                  </div>
                                  </a>
                                </div>: null}
                                {spotDetail && spotDetail.amenities && spotDetail.amenities.facebook_url ? <div className="col-sm-6 my-1">
                                  <a target='_blank' href={spotDetail.amenities.facebook_url}>
                                  <div className="fb-icon cursor-pointer">
                                    <img src="/images/facebook.png" className="img-fluid" />
                                    {strings.Facebook}
                                  </div>
                                  </a>
                                </div>: null}
                                {spotDetail && spotDetail.amenities && spotDetail.amenities.twitter_url ? <div className="col-sm-6 my-1">
                                  <a target='_blank' href={spotDetail.amenities.twitter_url}>
                                  <div className="fb-icon">
                                    <img src="/images/twitter.png" className="img-fluid" />
                                    {strings.Twitter}
                                  </div>
                                  </a>
                                </div>: null}
                                {spotDetail && spotDetail.amenities && spotDetail.amenities.instagram_url ? <div className="col-sm-6 my-1">
                                  <a target='_blank' href={spotDetail.amenities.instagram_url}>
                                  <div className="fb-icon">
                                    <img src="/images/instagram.png" className="img-fluid" />
                                    {strings.Instagram}
                                  </div>
                                  </a>
                                </div>: null}
                              </div>
                            </div>
                          </div>}
                          {spotDetail && spotDetail.amenities.remark && <hr />}
                          {spotDetail && spotDetail.amenities.remark && horizontalView({
                            name: strings.Remark,
                            value: spotDetail && spotDetail.amenities.remark
                          })}
                        </div>
                    </div>
                    <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                        <div className="bg-white main-background d-none d-md-block d-lg-block d-xl-block">
                            <h5 className="font-semibold mb-0">{strings.Reviews}</h5>
                            <hr/>
                            {spotDetail?.overall_reviews?.length ? spotDetail?.overall_reviews?.map((value, index) => <div>
                                <div className="user-review-details">
                                    <div className="row mb-2">
                                        <div className="col-12 col-md">
                                            <div className='d-flex justify-content-between'>
                                                <div className="d-flex">
                                                    <div className="sitter-profile-img">
                                                        <img src={value?.user?.profile_picture} className="img-fluid" />
                                                    </div>
                                                    <div className="sitter-review-details ml-2 my-auto">
                                                        <h5 className="mb-0 font-medium">{value?.user?.firstname + ' ' + value?.user?.lastname}</h5>
                                                        <p className="font-12 mb-0">
                                                            {moment(value.created_at).format('DD-MM-YYYY') + " | " + moment(value.created_at).format('HH:MM a')}
                                                        </p>
                                                        <div className="d-flex rating-star">
                                                            <RatingStars rating={value.rating} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <LightgalleryProvider>
                                                        <div>
                                                            {value?.images?.length ? value?.images?.map((p, idx) => (<>
                                                                {idx === 0 && <OpenButtonWithHook />}
                                                                <div style={{display: "none"}}>
                                                                    <LightgalleryItem group={"group2"} src={p?.image} thumb={p?.image}>
                                                                        <img src={p?.image} style={{ width: "100%" }} />
                                                                    </LightgalleryItem>
                                                                </div>
                                                            </>)) : null}
                                                        </div>
                                                        <div className="buttons mt-4">
                                                        </div>
                                                    </LightgalleryProvider>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p className="font-14 ">{value.review}</p>
                                </div>
                                <hr />
                            </div>) : strings.ReviewsAreNotGivenYet}
                            {spotDetail?.overall_reviews?.length > 0 ? <div className="col-12 px-0 d-flex justify-content-between align-items-center">
                                <div className="read-more">
                                    <p className="mb-0"><Link href={{pathname: '/pet-spots/spot-reviews', query: {id: spotDetail?.id}}}>{strings.ReadMoreReviews}</Link></p>
                                </div>
                            </div> : null}
                        </div>
                        <div className="bg-white main-background d-none d-md-block d-lg-block d-xl-block">
                            <h5 className="font-semibold mb-3">{strings.Location}</h5>
                            <div className="mapouter mb-3 map-short">
                                <div className="gmap_canvas">
                                    <MyMapComponent
                                        draggable={false}
                                        latlng={{lat: Number(address.lat), lng: Number(address.lng)}}
                                        defaultCenter={{lat: Number(address.lat), lng: Number(address.lng)}}
                                    />
                                </div>
                            </div>
                            <p className="mb-2 font-semibold">{spotDetail && spotDetail.address} </p>
                            {/*<p className="mb-0">*/}
                            {/*  301 Biscayne Blvd, Miami, FL 33132, United States*/}
                            {/*</p>*/}
                        </div>
                    </div>
                </div>
            </div>
            {/*-----/4th section------*/}
            {/*----------5th section---------------*/}
            <SpotSlider spots={featuredSpots?.filter(val => val.id !== spotDetail?.id) ?? []} />
            {/*----------/5th section---------------*/}
            {/*----------Back to top button---------------*/}
            <div className="col-12 text-right back-to-top-btn">
                <div onClick={()=>scrollToTop()} className="btn-top top-scroll">
                    <div className="top-icon"></div>
                </div>
            </div>
            {/*----------Back to top button---------------*/}
            {showModal ? <SpotRating spotId={router.query ? router.query.spot : ''} showModal={showModal} onHide={() => setShowModal(false)} updateSpot={(id) => { router.query ? getSpotDetail(id) : '' }} data={spotDetail} /> : null}
        </div>
    );
}
