import React, {useEffect, useRef, useState} from "react";
import "react-multi-carousel/lib/styles.css";
import "react-day-picker/lib/style.css";
import 'react-toastify/dist/ReactToastify.css';
import Script from 'next/script'
import ReactStars from "react-rating-stars-component";
import BecomePetSitter from "../components/homepage/BecomePetSitter";
import {strings} from "../public/lang/Strings";
import Link from "next/link";
import Lottie from 'react-lottie';
import animation from '../public/images/confirm.json'

class Review extends React.Component {

    render() {
        const privacyOptions = {
            loop: true,
            autoplay: true,
            animationData: animation,
            rendererSettings: {
                preserveAspectRatio: 'xMidYMid slice'
            }
        };
        return (
            <div>
                <div className="col-12 review-form p-0">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 review-form-head">
                                <div className="container-fluid">
                                    <div className="col-12 d-flex align-items-center justify-content-between px-0">
                                        <nav className="navbar navbar-expand-lg">
                                            <a className="navbar-brand logo">
                                                <img src="/images/logo.svg" width="140px" />
                                            </a>
                                        </nav>
                                        <button className="btn btn-blank fw-bold">Save and exit</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 review-form-body">
                                <div className="container">
                                    <div className="col-md-9 mx-auto">
                                        <div className="col-12 d-none">
                                            <div className="col-12 heading text-center">
                                                <h3 className="fw-bold mb-3">How was your experience with Taylor</h3>
                                                <p>Your host be able to read your review for 14 days or until tey write once too.</p>
                                            </div>
                                            <div className="col-12 content text-center">
                                                <div className="row justify-content-center">
                                                    <ReactStars
                                                        count={5}
                                                        size={46}
                                                        edit={true}
                                                        activeColor="#20847e"
                                                    />
                                                </div>
                                                <h6 className="font-weight-medium">Great</h6>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-4">
                                                <h3 className="fw-bold mb-2">Share how things went</h3>
                                                <p>Tell your osd and further guests wat you loved about your stay</p>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-12 p-0 mb-3">
                                                    <h6 className="fw-bold mb-0">Cleanliness</h6>
                                                    <ReactStars
                                                        count={5}
                                                        size={30}
                                                        edit={true}
                                                        activeColor="#20847e"
                                                    />
                                                </div>
                                                <div className="col-12 p-0 mb-3">
                                                    <h6 className="fw-bold mb-0">Accuracy</h6>
                                                    <ReactStars
                                                        count={5}
                                                        size={30}
                                                        edit={true}
                                                        activeColor="#20847e"
                                                    />
                                                </div>
                                                <div className="col-12 p-0 mb-3">
                                                    <h6 className="fw-bold mb-0">Check-in</h6>
                                                    <ReactStars
                                                        count={5}
                                                        size={30}
                                                        edit={true}
                                                        activeColor="#20847e"
                                                    />
                                                </div>
                                                <div className="col-12 p-0 mb-3">
                                                    <h6 className="fw-bold mb-0">Communication</h6>
                                                    <ReactStars
                                                        count={5}
                                                        size={30}
                                                        edit={true}
                                                        activeColor="#20847e"
                                                    />
                                                </div>
                                                <div className="col-12 p-0 mb-3">
                                                    <h6 className="fw-bold mb-0">Location</h6>
                                                    <ReactStars
                                                        count={5}
                                                        size={30}
                                                        edit={true}
                                                        activeColor="#20847e"
                                                    />
                                                </div>
                                                <div className="col-12 p-0 mb-3">
                                                    <h6 className="fw-bold mb-0">Value</h6>
                                                    <ReactStars
                                                        count={5}
                                                        size={30}
                                                        edit={true}
                                                        activeColor="#20847e"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-2">
                                                <h3 className="fw-bold mb-2">Write a private note</h3>
                                                <p>This feedback is just for Taylor - Share what you really appreciated plus anything they can do make the next stay event better.</p>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-12 form-group p-0 mb-3">
                                                    <textarea className="form-control" placeholder="Write here"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-2">
                                                <h3 className="fw-bold mb-2">Write a public note</h3>
                                                <p>After the review period ends, {"we'll"} publish this on {"Taylor's"} listing</p>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-12 form-group p-0 mb-3">
                                                    <textarea className="form-control" placeholder="Write here"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 text-center p-0 d-none">
                                            <div className="col-12 heading mb-2">
                                                <div className="col-md-8 mx-auto mb-4">
                                                    <h3 className="fw-bold mb-2">Share more details about your experience</h3>
                                                    <p>Please answer a few more questions to elp Taylor create the best experience for others. {"We'll"} only share your feedback with Taylor and it {"won't"} effect their rating.</p>
                                                </div>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-md-7 mx-auto mb-2">
                                                    <img className="img-fluid" width="280px" src="/images/cafe.png" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-4">
                                                <h3 className="fw-bold mb-2">Was the check-in process smooth?</h3>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                                checked={true}
                                                            />
                                                            <span className="checkmark"></span>
                                                            Extremely smooth
                                                        </label>
                                                    </div>
                                                    <ul className="chec-radio mt-3">
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                    defaultValue="yes"
                                                                    defaultChecked
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Easy to find
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Responsive sitter
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Detailed instruction
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Other (specify)
                                                                </div>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Very smooth
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Moderately smooth
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Somewhat smooth
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Not at all smooth
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-4">
                                                <h3 className="fw-bold mb-2">Was the place clean?</h3>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                                checked={true}
                                                            />
                                                            <span className="checkmark"></span>
                                                            Extremely clean
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Very clean
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Moderately clean
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Somewhat clean
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Not at all clean
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-4">
                                                <h3 className="fw-bold mb-2">Was the place comfortable?</h3>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                                checked={true}
                                                            />
                                                            <span className="checkmark"></span>
                                                            Extremely comfortable
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Very comfortable
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Moderately comfortable
                                                        </label>
                                                    </div>
                                                    <ul className="chec-radio mt-3">
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                    defaultValue="yes"
                                                                    defaultChecked
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Not well maintained
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Noisy
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Space too small
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Not private
                                                                </div>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Somewhat comfortable
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Not at all comfortable
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-4">
                                                <h3 className="fw-bold mb-2">How was your ost?</h3>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                                checked={true}
                                                            />
                                                            <span className="checkmark"></span>
                                                            Extremely accommodating
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Very accommodating
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Moderately accommodating
                                                        </label>
                                                    </div>
                                                    <ul className="chec-radio mt-3">
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                    defaultValue="yes"
                                                                    defaultChecked
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Not well maintained
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Noisy
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Space too small
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li className="pet-select mob-select">
                                                            <label className="radio-inline">
                                                                <input
                                                                    type="checkbox"
                                                                    id="selection"
                                                                    name="select-pro"
                                                                    className="pro-chx"
                                                                />
                                                                <div className="select-radio text-center">
                                                                    Not private
                                                                </div>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Somewhat accommodating
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-4 pb-2 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-bold font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Not at all accommodating
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 d-none">
                                            <div className="col-12 heading mb-4">
                                                <h3 className="fw-bold mb-2">Did the place ave the amenities?</h3>
                                                <p>Did you find these amenities at Taylor's place when you visited.</p>
                                            </div>
                                            <div className="col-12 content">
                                                <div className="col-12 mb-5 px-0">
                                                <h5 className="fw-bold mb-3">Was there a Pet pool?</h5>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                                checked={true}
                                                            />
                                                            <span className="checkmark"></span>
                                                            Yes
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Yes, but there were issues
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            No, I {"couldn't"} find it
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Somewhat accommodating
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                                <div className="col-12 mb-5 px-0">
                                                <h5 className="fw-bold mb-3">Was there a Pet pool?</h5>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                                checked={true}
                                                            />
                                                            <span className="checkmark"></span>
                                                            Yes
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Yes, but there were issues
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            No, I {"couldn't"} find it
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mb-3 px-1">
                                                    <div className="custom-check">
                                                        <label className="check fw-medium font-16">
                                                            <input
                                                                type="radio"
                                                                name="is_name1"
                                                            />
                                                            <span className="checkmark"></span>
                                                            Somewhat accommodating
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mb-5 p-0 text-center">
                                            <div className="col-12 heading mb-2">
                                                <div className="col-md-8 mx-auto mb-5">
                                                    <Lottie
                                                        options={privacyOptions}

                                                        speed={1}
                                                    />
                                                    <h3 className="fw-bold mb-2">Thank you for your review</h3>
                                                    <p>Your review will help your Host to improve future experiences more smooth and comfortable.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 review-form-foot">
                                <div className="container">
                                    <div className="col-md-9 mx-auto">
                                        <div className="col-md-12 mx-auto">
                                            <div className="row justify-content-between">
                                                <button className="btn btn-blank h-40 fw-bold p-0">Back</button>
                                                <button className="btn btn-primary">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Review;
