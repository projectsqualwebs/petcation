import React, {useEffect,useState} from "react";
import ChatBooking from "../../components/chat/ChatBooking";
import ChatBox from "../../components/chat/ChatBox";
import ChatParticipants from "../../components/chat/ChatParticipants";
import { I_CHAT_PARTICIPANTS } from "../../models/chat.interface";
import {useRouter} from "next/router";

const Chat: React.FC<{}> = () => {
  const [participants, setParticipants] = useState<I_CHAT_PARTICIPANTS>(null);
  const [active, setActive] = useState<I_CHAT_PARTICIPANTS>(null);
  const [updateMsg, setUpdateMsg] = useState(false);

  const updateParticipant = (value: I_CHAT_PARTICIPANTS) => {
    setParticipants(value);
  };


  return (
      <div className="main-wrapper my-md-3 my-0">
        <div className="container-fluid overflow-hidden">
          <div className="row chat-row">
              <ChatParticipants
                  active={active}
                  setActive={setActive}
                  updateParticipant={updateParticipant}
                  updateMsg={updateMsg}
              />
            {participants && <>
              <ChatBox updateParticipant={()=>setUpdateMsg(!updateMsg)} participants={participants} />
              <ChatBooking participants={participants} />
            </>}
          </div>
        </div>
      </div>
  );
};

export default Chat;
