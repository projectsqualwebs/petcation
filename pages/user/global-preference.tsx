import React from "react";
import { strings } from "../../public/lang/Strings";
import User from ".";
import {getCurrencyCode, getCurrencySign} from "../../api/Constants";

export default function GlobalPreference() {
  return (
    <div className="col-12 col-md-8 col-lg-8 col-xl-8  column1">
      <div className="col-12 content-main px-3 px-md-0 mt-3 mt-md-0">
        <div className="bg-white main-background global-details">
            <h5 className="font-20 font-semibold d-none d-lg-block">{strings.Globalpreferences}</h5>
          <div className="booking-for  selections">
            <div className="row align-items-center">
              <div className="col">
                <h6 className="font-medium mb-1">{strings.Language}</h6>
                <p className="font-14 mb-0">
                  {strings.SetyourpreferedlanguageforPetcatiaon}
                </p>
              </div>
              <div className="col-auto">
                <div className="form-group mb-0">
                  <div className="">
                    <div className="dropdown">
                      <select
                        className="form-control mySelect"
                        id="night-charge"
                        tabIndex={-98}
                      >
                        <option>{strings.English}</option>
                      </select>
                      <button
                        type="button"
                        className="btn dropdown-toggle btn-light"
                        data-toggle="dropdown"
                        role="combobox"
                        aria-owns="bs-select-1"
                        aria-haspopup="listbox"
                        aria-expanded="false"
                        data-id="night-charge"
                        title="English"
                      >
                        <div className="filter-option">
                          <div className="filter-option-inner">
                            <div className="filter-option-inner-inner">
                              {strings.English}
                            </div>
                          </div>{" "}
                        </div>
                      </button>
                      <div
                        className="dropdown-menu"
                        style={{
                          maxHeight: "1421.27px",
                          overflow: "hidden",
                          minHeight: 0,
                        }}
                      >
                        <div
                          className="inner show"
                          role="listbox"
                          id="bs-select-1"
                          tabIndex={-1}
                          aria-activedescendant="bs-select-1-0"
                          style={{
                            maxHeight: "1421.27px",
                            overflowY: "auto",
                            minHeight: 0,
                          }}
                        >
                          <ul
                            className="dropdown-menu inner show"
                            role="presentation"
                            style={{ marginTop: 0, marginBottom: 0 }}
                          >
                            <li className="selected active">
                              <a
                                role="option"
                                className="dropdown-item active selected"
                                id="bs-select-1-0"
                                tabIndex={0}
                                aria-setsize={1}
                                aria-posinset={1}
                                aria-selected="true"
                              >
                                <span className="text">{strings.English}</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div className="row align-items-center">
              <div className="col">
                <h6 className="font-medium mb-1">{strings.Country}</h6>
                <p className="font-14 mb-0">
                  {strings.SetyourpreferedlanguageforPetcatiaon}
                </p>
              </div>
              <div className="col-auto">
                <div className="form-group mb-0">
                  <div className="">
                    <div className="dropdown">
                      <select
                        className="form-control  w-10"
                        id="night-charge"
                        tabIndex={-98}
                      >
                        <option>{strings.Japan}</option>
                        <option>{strings.US}</option>
                      </select>
                      <button
                        type="button"
                        className="btn dropdown-toggle btn-light"
                        data-toggle="dropdown"
                        role="combobox"
                        aria-owns="bs-select-2"
                        aria-haspopup="listbox"
                        aria-expanded="false"
                        data-id="night-charge"
                        title="US"
                      >
                        <div className="filter-option">
                          <div className="filter-option-inner">
                            <div className="filter-option-inner-inner">{strings.US}</div>
                          </div>{" "}
                        </div>
                      </button>
                      <div
                        className="dropdown-menu"
                        style={{
                          maxHeight: "1501.27px",
                          overflow: "hidden",
                          minHeight: 0,
                        }}
                      >
                        <div
                          className="inner show"
                          role="listbox"
                          id="bs-select-2"
                          tabIndex={-1}
                          aria-activedescendant="bs-select-2-1"
                          style={{
                            maxHeight: "1501.27px",
                            overflowY: "auto",
                            minHeight: 0,
                          }}
                        >
                          <ul
                            className="dropdown-menu inner show"
                            role="presentation"
                            style={{ marginTop: 0, marginBottom: 0 }}
                          >
                            <li className="selected active">
                              <a
                                role="option"
                                className="dropdown-item active selected"
                                id="bs-select-2-0"
                                tabIndex={0}
                                aria-setsize={2}
                                aria-posinset={1}
                                aria-selected="true"
                              >
                                <span className="text">{strings.Japan}</span>
                              </a>
                            </li>
                            <li className="selected active">
                              <a
                                role="option"
                                className="dropdown-item active selected"
                                id="bs-select-2-1"
                                tabIndex={0}
                                aria-setsize={2}
                                aria-posinset={2}
                                aria-selected="true"
                              >
                                <span className="text">{strings.US}</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div className="row align-items-center">
              <div className="col">
                <h6 className="font-medium mb-1">{strings.Currency}</h6>
                <p className="font-14 mb-0">
                  {strings.Selectcurrencyforallyourtransactions}
                </p>
              </div>
              <div className="col-auto">
                <div className="form-group mb-0">
                  <div className="">
                    <div className="dropdown">
                      <select
                        className="form-control mySelect"
                        id="night-charge"
                        tabIndex={-98}
                      >
                        <option>{getCurrencySign() + ' ' + getCurrencyCode()}</option>
                      </select>
                      <button
                        type="button"
                        className="btn dropdown-toggle btn-light"
                        data-toggle="dropdown"
                        role="combobox"
                        aria-owns="bs-select-3"
                        aria-haspopup="listbox"
                        aria-expanded="false"
                        data-id="night-charge"
                        title="¥ JPY"
                      >
                        <div className="filter-option">
                          <div className="filter-option-inner">
                            <div className="filter-option-inner-inner">{getCurrencySign() + ' ' + getCurrencyCode()}</div>
                          </div>{" "}
                        </div>
                      </button>
                      <div
                        className="dropdown-menu"
                        style={{
                          maxHeight: "1581.27px",
                          overflow: "hidden",
                          minHeight: 0,
                        }}
                      >
                        <div
                          className="inner show"
                          role="listbox"
                          id="bs-select-3"
                          tabIndex={-1}
                          aria-activedescendant="bs-select-3-0"
                          style={{
                            maxHeight: "1581.27px",
                            overflowY: "auto",
                            minHeight: 0,
                          }}
                        >
                          <ul
                            className="dropdown-menu inner show"
                            role="presentation"
                            style={{ marginTop: 0, marginBottom: 0 }}
                          >
                            <li className="selected active">
                              <a
                                role="option"
                                className="dropdown-item active selected"
                                id="bs-select-3-0"
                                tabIndex={0}
                                aria-setsize={1}
                                aria-posinset={1}
                                aria-selected="true"
                              >
                                <span className="text">{getCurrencySign() + ' ' + getCurrencyCode()}</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

GlobalPreference.getLayout = function (page) {
  return <User>{page}</User>;
};
