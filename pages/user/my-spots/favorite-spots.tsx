import React from "react";
import User from "../index";
import SpotObject from "../../../components/user/mySpots/SpotObject";
import {pet, sort, spots} from "../../../public/appData/AppData";
import { strings } from "../../../public/lang/Strings";
import Link from "next/dist/client/link";
import API from "../../../api/Api";
import SpotObjectWeb from "../../../components/petSpot/searchSpot/SpotObjectWeb";
import Select from "react-select";
import CreateWishlist from "../../../components/common/CreateWishlist";
import {confirmAlert} from "react-confirm-alert";
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

type T_SPOT = {
  spot: {
    spot_id: number;
    id: number;
    spot_name: string;
    address: string;
    city: string;
    postal_code: number;
    created_at: string;
    overall_rate: number;
    total_review: number;
    repeat_client: number;
    images: {
      path: string;
    }[];
    user_id: number;
  }
};

interface Istate {
    spots: T_SPOT[];
    showModal: boolean;
    wishList: any[];
    selectedWishlist: any;
    data: any;
}
interface I_props {

}

const api = new API();
export default class FavoriteSpots extends React.Component<I_props,Istate> {
  constructor(props) {
    super(props);
    this.state = {
      spots: [],
      showModal: false,
      wishList: [],
      selectedWishlist: null,
        data: null,
    };
  }
  componentDidMount() {
    // this.getSpots();
    this.getAllWishList(null);
  }

  // getSpots = () => {
  //   api
  //     .getFavoriteSpots()
  //     .then((res) => {
  //       this.setState({
  //         spots: res.data.response,
  //       });
  //     })
  //     .catch((error) => {});
  // };

    deleteWishlistApiCall = (id) => {
      api
          .deleteWishlist(id)
          .then(res => {
              this.setState({selectedWishlist: null})
              this.getAllWishList(null);
          })
          .catch(err => console.log(err.response.data.message))
    }

    deleteWishlist = (props) => {
        confirmAlert({
            closeOnClickOutside: false,
            customUI: ({onClose}) => {
                return (
                    <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
                        <div className="react-confirm-alert-body">
                            <h4>{strings.Delete + " " + props.label}</h4>
                            <p>{strings.areYouSure}</p>
                            <div className="react-confirm-alert-button-group">
                                <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                                    this.deleteWishlistApiCall(props.value)
                                    onClose();
                                }}>{strings.Delete}
                                </button>
                                <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                                    onClose();
                                }}>{strings.Cancel}
                                </button>
                            </div>
                        </div>
                    </div>
                );
            }
        });
    }
  getAllWishList = (value) => {
    api
        .getSpotLists()
        .then(res => {
          this.setState({wishList : res.data.response.map((val, index) =>
                  ({
                      label: val.name,
                      value: val.id,
                      key: index+1
                  })
              )})
            if (value === null && res.data.response.length) {
                this.setState({selectedWishlist: {
                        label: res.data.response[0].name,
                        value: res.data.response[0].id,
                        key: 1
                    }})
                this.getSingleWishlist(res.data.response[0].id)
            } else if (value === null && !res.data.response.length) {
                this.setState({
                    spots: res.data.response,
                    selectedWishlist: null
                })
            }
          if (this.state.selectedWishlist == null && res.data.response.length) {
            this.setState({selectedWishlist: {
                    label: res.data.response[0].name,
                    value: res.data.response[0].id,
                    key: 1
                }})
            this.getSingleWishlist(res.data.response[0].id)
          } else if (value != null && res.data.response.length) {
              this.setState({selectedWishlist: {
                      label: res.data.response.find(val => val.id == value).name,
                      value: res.data.response.find(val => val.id == value).id,
                      key: res.data.response.findIndex(val => val.id == value) + 1
                  }})
          }
          // this.state.wishList(res.data.response)
        })
        .catch(err => console.log(err.response.data.message))
  }
  deletePetSpot = (val) => {
    api
        .deleteSpot(val)
        .then((res) => {
          this.getAllWishList(null)
        })
        .catch((error) => { });
  };
  getSingleWishlist = (id) => {
    api
        .getSingleList(id)
        .then(res => {
            this.setState({
                spots: res.data.response,
            });
        })
        .catch(err => console.log(err.response.data.message))
  }

  render() {
    return (
      <div className="col-12 col-md-8 col-lg-8 col-xl-8  column1">
        <div className="bg-white main-background">
            <div className="col-12 mb-3">
                <div className="row align-items-center">
                    <div className="col">
                        <h4 className="font-20 font-semibold mb-0">{strings.FavouriteSpot}</h4>
                    </div>
                    <div className="col-auto">
                        <button onClick={()=> this.setState({showModal: true})} className="btn btn-primary px-2">{strings.CreateNewWishlist}</button>
                    </div>
                </div>
            </div>
          <div className="col-sm px-md-0">
            <div className="d-flex justify-content-between filter-design">
              <div className="col-sm form-group mb-0">
                <label>{strings.SelectWishlist}</label>
                <Select
                    className="w-50"
                    onChange={(e)=>{
                      this.setState({selectedWishlist: e})
                      this.getSingleWishlist(e.value)
                    }}
                    value={this.state.selectedWishlist ? this.state.selectedWishlist :''}
                    isSearchable={false}
                    options={this.state.wishList}
                />
              </div>
            </div>
          </div>
            <hr />
            {this.state.selectedWishlist ? <div className='d-flex justify-content-between p-2 mb-3'>
                <h6>{this.state.selectedWishlist.label}</h6>
                <div>
                    <a className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                       onClick={() => {
                           this.setState({data: this.state.selectedWishlist});
                           this.setState({showModal: true});
                       }}
                    >
                        <svg className="text-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path
                                fill="currentColor"
                                d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"/>
                        </svg>
                    </a>
                    <a className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"
                       onClick={() => this.deleteWishlist(this.state.selectedWishlist)}
                    >
                        <svg
                            viewBox="0 0 448 512"
                        >
                            <path
                                fill="currentColor"
                                d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"
                            ></path>
                        </svg>
                    </a>
                </div>
            </div> : <h6>{strings.PleaseCreateNewWishlist}</h6>}
          {this.state.spots && this.state.spots?.length ? this.state.spots?.map((value, index) => (
            <SpotObject
                type={2}
                key={index}
                spot={value}
                listId={this.state.selectedWishlist?.value}
                updateSpot={(listId)=>this.getSingleWishlist(listId)}
                // deleteSpot={(id) => this.deletePetSpot(id)}
                deleteSpot={()=> {}}
            />
          )) : <div className="text-center padding">
              <p className="font-13 mb-0 font-italic">{strings.ResultNotFound}</p>
          </div>}
        </div>
        <CreateWishlist
            showModal={this.state.showModal}
            hideModal={() => this.setState({showModal: false})}
            reRender={(value) => this.getAllWishList(value)}
            data={this.state.data}
            setData={()=>{
                this.setState({data: null})
            }}
        />
      </div>
    );
  }
  static getLayout(page) {
    return <User>{page}</User>;
  }
}
