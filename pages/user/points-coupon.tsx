import React, { useEffect, useState } from "react";
import User from ".";
import { errorOptions, successOptions } from "../../public/appData/AppData";
import { useSnackbar } from "react-simple-snackbar";
import API from "../../api/Api";
import { strings } from "../../public/lang/Strings";
import AddCouponModal from "../../components/common/AddCouponModal";
import moment from "moment";
import boolean from "async-validator/dist-types/validator/boolean";
import { getCurrencySign } from "../../api/Constants";

let api = new API();

interface I_STATE {
  coupons: any[];
  expiredCoupons: any[];
}

export default function PointsAndCoupon({}, state: I_STATE) {
  const [coupons, setCoupons] = useState<any>(null);
  const [expiredCoupons, setExpiredCoupons] = useState<any>(null);
  const [showCouponModal, setShowCouponModal] = useState<boolean>(false);
  const [unusedCoupons, setUnusedCoupons] = useState<boolean>(true);

  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);

  useEffect(() => {
    getCoupons();
    api
      .getCoupons(0)
      .then((res) => {
        if (res.data.status == 200) {
          setExpiredCoupons(res.data.response);
        }
      })
      .catch((error) => {
        console.log("error is", error);
      });
  }, []);

  const getCoupons = () => {
    api
      .getCoupons(1)
      .then((res) => {
        if (res.data.status == 200) {
          setCoupons(res.data.response);
        }
      })
      .catch((error) => {
        console.log("error is", error);
      });
  };

  return (
    <>
      <div className="col-12 col-md-8 col-lg-8 col-xl-8 column1">
        <div className="tabs-main main-background">
          <div className="payment d-none d-md-block d-lg-block d-xl-block">
            <h5 className="font-20 font-semibold mb-3">
              {strings.PointsAndcoupon}
            </h5>
          </div>
          <div className="row tabs-design">
            <div className="col-12 px-0 px-md-3">
              <div className="pay-tabs">
                <div className="pay-tabs">
                  <ul
                    className="nav nav-tabs border-0 mb-0"
                    id="myTab"
                    role="tablist">
                    <li className="nav-item" role="presentation">
                      <a
                        className={`nav-link ${
                          unusedCoupons === true ? "active" : ""
                        }`}
                        id="unused-tab"
                        data-toggle="tab"
                        onClick={() => setUnusedCoupons(true)}
                        role="tab"
                        aria-selected="true">
                        {strings.UnusedCoupons}
                      </a>
                    </li>
                    <li className="nav-item" role="presentation">
                      <a
                        className={`nav-link ${
                          unusedCoupons === false ? "active" : ""
                        }`}
                        id="expired-tab"
                        data-toggle="tab"
                        onClick={() => setUnusedCoupons(false)}
                        role="tab"
                        aria-selected="false">
                        {strings.ExpiredCoupons}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 content-main px-3 px-md-0 mt-3 mt-md-0">
          <div className="bg-white main-background">
            <div className="tab-content" id="myTabContent">
              {/*----------*/}
              {unusedCoupons === true ? (
                <div className="tab-pane fade active show">
                  <div className="row">
                    {coupons && coupons?.length ? (
                      coupons?.map((item, index) => (
                        <div key={index} className="col-sm-6">
                          <div className="unused-coupons">
                            <h5 className="mb-2">
                              {getCurrencySign() +
                                item?.coupon?.discount_amount +
                                " " +
                                strings.OFF}
                            </h5>
                            <p className="main-amt mb-0">
                              {strings.ForOrderOver +
                                " " +
                                getCurrencySign() +
                                item?.coupon?.min_total}
                            </p>
                            <p className="code mb-0">
                              {strings.Code_C + item?.coupon?.coupon_code}
                            </p>
                            <hr />
                            <ul className="coupon-list">
                              <li>
                                {moment(item?.coupon?.start_date * 1000).format(
                                  "MM/DD/YYYY hh:mm A"
                                )}{" "}
                                -{" "}
                                {moment(item?.coupon?.end_date * 1000).format(
                                  "MM/DD/YYYY hh:mm A"
                                )}
                              </li>
                            </ul>
                          </div>
                        </div>
                      ))
                    ) : (
                      <div className="col-12 text-center padding">
                        <p className="font-13 mb-0 font-italic">
                          {strings.noCoupons}
                        </p>
                      </div>
                    )}
                  </div>
                  <div className="col-12 book-fixed px-3 px-md-0">
                    <hr className="mt-0 d-none d-lg-block mb-3" />
                    <button
                      className="btn btn-primary"
                      data-toggle="modal"
                      data-target="#coupon"
                      onClick={() => setShowCouponModal(true)}>
                      {strings.AddCoupon}
                    </button>
                  </div>
                </div>
              ) : (
                <div className="tab-pane fade active show">
                  <div className="row">
                    {expiredCoupons && expiredCoupons.length ? (
                      expiredCoupons.map((item, index) => (
                        <div key={index} className="col-sm-6">
                          <div className="unused-coupons expired">
                            <h5 className="mb-2">
                              {getCurrencySign() +
                                item?.coupon?.discount_amount +
                                " " +
                                strings.OFF}
                            </h5>
                            <p className="main-amt mb-0">
                              {strings.ForOrderOver +
                                " " +
                                getCurrencySign() +
                                item?.coupon?.min_total}
                            </p>
                            <p className="code mb-0">
                              {strings.Code_C + item?.coupon?.coupon_code}
                            </p>
                            <hr />
                            <ul className="coupon-list">
                              <li>
                                {moment(item?.coupon?.start_date * 1000).format(
                                  "DD/MM/YYYY hh:mm A"
                                )}{" "}
                                -{" "}
                                {moment(item?.coupon?.end_date * 1000).format(
                                  "DD/MM/YYYY hh:mm A"
                                )}
                              </li>
                              {/*<li>For all products</li>*/}
                            </ul>
                          </div>
                        </div>
                      ))
                    ) : (
                      <div className="col-12 text-center padding">
                        <p className="font-13 mb-0 font-italic">
                          {strings.noCoupons}
                        </p>
                      </div>
                    )}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        <AddCouponModal
          updateCoupon={() => getCoupons()}
          showModal={showCouponModal}
          hideModal={() => setShowCouponModal(false)}
        />
      </div>
    </>
  );
}

PointsAndCoupon.getLayout = function (page) {
  return <User>{page}</User>;
};
