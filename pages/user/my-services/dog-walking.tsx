import React from "react";
import User from "..";
import ManageServiceHeader from "../../../components/user/myService/ManageServiceHeader";
import Tab from "react-bootstrap/Tab";
import DogWalkingServiceFee from "../../../components/user/myService/dogWalking/DogWalkingServiceFee";
import DogWalkingDiscounts from "../../../components/user/myService/dogWalking/DogWalkingDiscounts";
import DogWalkingPreference from "../../../components/user/myService/dogWalking/DogWalkingPreference";
import {strings} from "../../../public/lang/Strings";

interface IState {
  selectedIndex: number;
}
export default class DogWalking extends React.Component<{}, IState> {
  private parent: React.RefObject<HTMLInputElement>;
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 0,
    };
    this.parent = React.createRef();
    this.handleTabChange = this.handleTabChange.bind(this);
  }

  handleTabChange = (index: number) => {
    this.setState(
      {
        selectedIndex: index,
      },
      () => {
        this.parent.current.scrollIntoView({ behavior: "smooth" });
      }
    );
  };

  render() {
    return (
      <div
        ref={this.parent}
        className="col-12 col-md-8 col-lg-8 col-xl-8 column1"
      >
        <ManageServiceHeader
            service={strings.dogWalking}
            description={
              ""
            }
            index={this.state.selectedIndex}
            handleChangeIndex={this.handleTabChange}
        />
        <div className="bg-white main-background single-service">


          <Tab.Container
            activeKey={this.state.selectedIndex}
            id="left-tabs-example"
            defaultActiveKey={0}
            transition={true}
          >
            <Tab.Content>
              <Tab.Pane eventKey={0}>
                <DogWalkingServiceFee
                  handleTabChange={this.handleTabChange.bind(this)}
                />
              </Tab.Pane>
              <Tab.Pane eventKey={1}>
                <DogWalkingPreference
                  handleTabChange={this.handleTabChange.bind(this)}
                />
              </Tab.Pane>
              <Tab.Pane eventKey={2}>
                <DogWalkingDiscounts />
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
        </div>
      </div>
    );
  }

  static getLayout(page) {
    return <User>{page}</User>;
  }
}
