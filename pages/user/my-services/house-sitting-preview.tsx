import Link from "next/link";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import User from "..";
import API from "../../../api/Api";
import {pets, petSize} from "../../../public/appData/AppData";
import {
  D_DISTANCE,
  D_FLEXIBILITY,
  D_FREQUENT_BREAKS,
  D_TRANSPORTATION,
} from "../../../public/appData/StaticData";
import {strings} from "../../../public/lang/Strings";
import {getCurrencySign} from "../../../api/Constants";

const api = new API();
export default function HouseSittingPreview() {
  const [data, setData] = useState<any>();
  const [transportation, setTransportation] = useState<any>();
  const [transportFee, setTransportFee] = useState<any>();

  useEffect(() => {
    api
      .getHouseSittingPreview()
      .then((res) => {
        setData(res.data.response);
        let preferences = res.data.response.house_sitting_service_preferences;
        let transport_preference = preferences.transport_preferences;
        if (transport_preference.length) {
          setTransportFee(transport_preference)
          let transport = D_TRANSPORTATION.filter((val)=> transport_preference.find(item => item.status == 1 && item.transport_mode_id == val.value))
          setTransportation(transport)
      }})
      .catch((error) => {});
  }, []);

  if (data) {
    return (
      <div className="col-12 col-md-8 col-lg-8 col-xl-8 column1">
        <div className=" single-service">
          <div className="service-title mb-2">
            <h5 className="font-20  mb-0 font-semibold">
              {strings.houseSitting + ' ' + strings.Preview}
            </h5>
          </div>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link href="/user/my-services">
                  <a href="javascript:void(0)">
                    <i className="fas fa-angle-left" /> {strings.AllServices}
                  </a>
                </Link>
              </li>
              {console.log({data})}
              <li className="breadcrumb-item">
                <Link href={{
                  pathname: "/user/my-services/house-sitting",
                  query: {
                    serviceId: data.house_sitting_service_fee.sitter_house_sitting_service_id
                  },
                }}>
                  <a href="javascript:void(0)">
                    <i className="fas fa-angle-left" /> {strings.ManageService}
                  </a>
                </Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                {strings.Preview}
              </li>
            </ol>
          </nav>
        </div>
        {/*---------------services and fees------------------*/}
        <div className="bg-white main-background">
          <div className="service-title mb-2">
            <h6 className="preview-head">{strings.ServiceAndFee_C}</h6>
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.Whatpetswouldyoupreferforthisservice}
            </p>
            <p className="mb-0 font-12">
              {strings.SelectThePetsYouWillBeWillingToServiceForHouseSitting}
            </p>
          </div>
          <div className="row">
            {data.house_sitting_service_fee.service_pets.map((v) => (
              <div className="col-6 col-md">
                <div className="preview-details">
                  <h6 className="mb-0 font-medium">
                    {pets.find((value) => value.value == v.pet_type).label}
                  </h6>
                  {v.fees.map(value =>
                      <div className="">
                        <p className="w-100 d-flex justify-content-between align-items-center mb-1">
                          <small>{petSize.find(val => val.value === value.pet_size_id).label}:</small><small> {getCurrencySign()} {value.service_charge}</small>
                        </p>
                      </div>
                  )}
                </div>
              </div>
            ))}
          </div>
          <hr />
          <div className="extra-charge">
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.WouldYouLikeToChargeExtraWhenProviding + ' ' + strings.houseSitting + ' ' + strings.serviceDuringHolidays_Q}
            </p>
            <p className="mb-2 font-12">
              {strings.EnterValueByWhichYouWouldWantPricesToIncreaseDuringHolidaysInBelowBoxLookAtlistOfHolidaysHere}
            </p>
            <div className="preview-details">
              <h6 className="mb-0 font-medium">
                {strings.Range+" "}
                <span>
                  {data.house_sitting_service_fee.holiday_extra_charges}%
                </span>
              </h6>
            </div>
          </div>
          <hr />
          <div className="fees-content my-3">
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.CacellationPolicy}
            </p>
            <p className="mb-0 font-12">
              {strings.SelectCancellationPolicyForThisServiceUserWillSeeTheSameAtTheTimeOfBooking}
            </p>
            <div className="booking-for m-0">
              <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  {data.house_sitting_service_fee.cancellation_policy == 1 ? (
                      <div className="row service-charges mb-0 pb-0">
                        <div className="custom-check ml-0">
                          <h6 className="font-medium mb-2">{strings.Flexible}</h6>
                          <p className="font-12">
                            {strings.ForBoardingAndHouseSittingYouWillGetAFullRefundIfYouCancelBeforeTheStayBegins}
                          </p>
                          <p className="font-12 mb-0">
                            {strings.ForWalksDayCareAndDropinVisitsYouWillGetAFullRefundIfYouCancelBeforeTheDaysServiceIsDelivered}
                          </p>
                        </div>
                      </div>
                  ) : data.house_sitting_service_fee.cancellation_policy == 2 ? (
                      <div className="row service-charges mb-0 pb-0">
                        <div className="custom-check ml-0">
                          <h6 className="font-medium mb-2">{strings.Moderate}</h6>
                          <p className="font-12">
                            {strings.YouCancelWithin48HoursOfBooking}
                          </p>
                          <p className="font-12 mb-0">
                            {strings.YouHavenotAlreadyCancelled3ReservationsInTheLast12months}
                          </p>
                          <p className="font-12 mb-0">
                            {strings.TheReservationYouareCancellingDoesnotOverlapWithAnotherReservationInYourAccountWhenRefund}
                          </p>
                          <p className="font-12 mb-0">
                            {strings.TheReservationYouAreCancellingDoesnotOverlapWith}
                          </p>
                        </div>
                      </div>
                  ) : (
                      <div className="row service-charges mb-0 pb-0">
                        <div className="custom-check ml-0">
                          <h6 className="font-medium mb-2">{strings.Strict}</h6>
                          <p className="font-12">
                            {strings.YouwllGetAFullRefundIfYouCancelBy1200NoonOneWeekBeforeTheStayBegins}
                          </p>
                        </div>
                      </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-white main-background">
          <div className="service-title mb-2">
            <h6 className="preview-head">{strings.Prefrences_C}</h6>
            <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-0">
                {strings.YourAvailabilityWhilePetIsAtHouseSitting}
              </h6>
              <p className="font-12 mb-1">
                {strings.Youcanselectfromtheoptionsparts}
              </p>
              <h6 className="mb-0 font-medium">
                {
                  D_FLEXIBILITY.find(
                    (value) =>
                      data.house_sitting_service_preferences.flexibility_id ==
                      value.value
                  ).label
                }
              </h6>
            </div>
            <hr />
            <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-0">
                {strings.whenhostingpetsinyourhomehowfrequentlycanyouprovidebreaks}
              </h6>
              <p className="font-12 mb-1">
                {strings.howfrequentlycanyouprovidepottybreaks}
              </p>
              <h6 className="mb-0 font-medium">
                {
                  D_FREQUENT_BREAKS.find(
                    (value) =>
                      data.house_sitting_service_preferences
                        .frequently_break_id == value.value
                  ).label
                }
              </h6>
            </div>
            <hr />
            <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-1">
                {strings.SpecificRequiredForPetSittingAtClientsPlace_Q}
              </h6>
              <h6 className="font-medium">
                {data.house_sitting_service_preferences
                  .pet_citing_at_client_place_required == 1
                  ? strings.Yes
                  : strings.No}
              </h6>
              {data.house_sitting_service_preferences ? data.house_sitting_service_preferences.pet_citing_at_client_place_required == 1 ? <p className="font-15 mb-1 mr-5">{data.house_sitting_service_preferences.description}</p> : null : null}
            </div>
            <hr />
            <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-1">
                {strings.Canyoupickuppetfromclientplace}
              </h6>
              <h6 className="font-medium">
                {data.house_sitting_service_preferences.pickup_from_client_home == 1
                    ? strings.Yes
                    : strings.No}
              </h6>
              {data.house_sitting_service_preferences ? data.house_sitting_service_preferences.pickup_from_client_home == 1 ? <div className="d-flex pet-pickup-details">
                <div className="mr-5 font-14">
                  <p className="font-15 mb-1 mr-5"> {strings.ByWhatMeans_Q}</p>
                  <h6 className="mb-0 font-14 font-medium">
                    {transportation ? transportation.length ? transportation.map((val, index)=> val.label + `${index == transportation.length-1 ? '' : ', '}`) : null : null}
                  </h6>
                </div>
              </div> : null : null}
            </div>
            {data.house_sitting_service_preferences ? data.house_sitting_service_preferences.pickup_from_client_home == 1 ? <hr />:null:null}
            {data.house_sitting_service_preferences ? data.house_sitting_service_preferences.pickup_from_client_home == 1 ? <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-1">
                {strings.Doyouchargetransportationfee}
              </h6>
              <h6 className="font-medium">
                {data.house_sitting_service_preferences
                  .has_transportation_fee == 1
                  ? strings.Yes
                  : strings.No}
              </h6>
              {data.house_sitting_service_preferences.has_transportation_fee ==
              1 ? (
                <div className="d-flex pet-pickup-details">
                  <div className="mr-5 font-14">
                    <p className="font-15 mb-1 mr-5"> {strings.transportationFee}</p>
                    <table>
                      {transportation && transportation.length ? transportation.map((val, index) => <tr key={index}>
                        <td><h6 className="mb-0 font-14 font-medium mr-5">{val.label}</h6></td>
                        <td>￥{transportFee.find(item => item.transport_mode_id == val.value).price_start_from}</td>
                      </tr>) : null}
                    </table>
                  </div>
                </div>
              ) : null}
            </div> : null : null}
          </div>
        </div>
        {data.house_sitting_service_discount ? (
          <div className="bg-white main-background">
            <div className="service-title mb-2">
              <h6 className="preview-head">{strings.Discount_C}</h6>
              <div className="main-padding pb-0">
                <h6 className="font-semibold mb-1">
                  {strings.Specialdiscountforyourguests}
                </h6>
                {data.house_sitting_service_discount.first_booking_offered ==
                1 ? (
                  <div className="discount-check">
                    <div className="custom-check">
                      <label className="check pl-0">
                        <p className="font-medium mb-1">{strings.offonfirstbookingwithyou}</p>
                        <p className="font-12 mb-0">
                          {strings.Allowofonbookingamount}
                        </p>
                      </label>
                    </div>
                  </div>
                ) : (
                  <div className="discount-check">
                    <div className="custom-check">
                      <label className="check pl-0">
                        <p className="font-medium mb-1">{strings.offonfirstbookingwithyou}</p>
                        <p className="font-12 mb-0">
                          {strings.Allowofonbookingamount}
                        </p>
                      </label>
                    </div>
                  </div>
                )}
              </div>
              <hr />

              <div className="main-padding pb-0">
                <div className="stay-discount">
                  <h5>{strings.Lengthofstaydiscount}</h5>
                  <p className="font-12 mb-2">
                    {strings.Allowofonbookingamount}
                  </p>
                  <div className="col-12 px-0">
                    <div className="row">
                      <div className="col-auto">
                        <div className="weekly-discount mb-2 mb-md-0">
                          <h6 className="font-14 font-medium mb-0">
                            {strings.WeeklyDiscount_C + data.house_sitting_service_discount.weekly_discount_percentage+'%'}
                          </h6>
                        </div>
                      </div>
                      <div className="col-auto">
                        <div className="weekly-discount mb-0">
                          <h6 className="font-14 font-medium mb-0">
                            {strings.MonthlyDiscount_C + data.house_sitting_service_discount.monthly_discount_percentage+'%'}
                          </h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  } else {
    return null;
  }
}

HouseSittingPreview.getLayout = function (page) {
  return <User>{page}</User>;
};
