import Link from "next/link";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import User from "..";
import API from "../../../api/Api";
import {pets, petSize} from "../../../public/appData/AppData";
import {
  D_DISTANCE,
  D_FLEXIBILITY,
  D_FREQUENT_BREAKS,
  D_TRANSPORTATION,
  D_WALK_COUNT,
} from "../../../public/appData/StaticData";
import {strings} from "../../../public/lang/Strings";
import {getCurrencySign} from "../../../api/Constants";

const api = new API();
export default function DogWalkingPreview() {
  const [data, setData] = useState<any>();
  const [transportation, setTransportation] = useState<any>();
  const [transportFee, setTransportFee] = useState<any>();
  useEffect(() => {
    api
      .getDogWalkingPreview()
      .then((res) => {
        setData(res.data.response);
        let preferences = res.data.response.pet_walking_service_preferences;
        let transport_preference = preferences.transport_preferences;
        if (transport_preference.length) {
          setTransportFee(transport_preference)
          let transport = D_TRANSPORTATION.filter((val)=> transport_preference.find(item => item.status == 1 && item.transport_mode_id == val.value))
          setTransportation(transport)
          // console.log('transportation', transport)
        }
      })
      .catch((error) => {});
  }, []);
  if (data) {
    return (
      <div className="col-12 col-md-8 col-lg-8 col-xl-8 column1">
        <div className=" single-service">
          <div className="service-title mb-2">
            <h5 className="font-20  mb-0 font-semibold">
              {strings.dogWalking + ' ' +  strings.servicePreview}
            </h5>
          </div>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link href="/user/my-services">
                  <a href="javascript:void(0)">
                    <i className="fas fa-angle-left" /> {strings.AllServices}
                  </a>
                </Link>
              </li>
              <li className="breadcrumb-item">
                <Link href={{
                  pathname: "/user/my-services/dog-walking",
                  query: {
                    serviceId: data.pet_walking_service_fee.sitter_pet_walking_services_id
                  },
                }}>
                  <a href="javascript:void(0)">
                    <i className="fas fa-angle-left" /> {strings.ManageService}
                  </a>
                </Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                {strings.Preview}
              </li>
            </ol>
          </nav>
        </div>
        {/*---------------services and fees------------------*/}
        <div className="bg-white main-background">
          <div className="service-title mb-2">
            <h6 className="preview-head">{strings.ServiceAndFee_C} </h6>
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.Whatpetswouldyoupreferforthisservice}
            </p>
            <p className="mb-0 font-12">
              {strings.SelectThePetsYouWillBeWillingToServiceFor + strings.dogWalking}
            </p>
          </div>
          <div className="row">
            {data.pet_walking_service_fee.service_pets.map((v) => (
                <div className="col-6 col-md my-1">
                  <div className="preview-details">
                  <p className="mb-1 font-medium">
                    {pets.find((value) => value.value == v.pet_type).label}
                  </p>
                  {v.fees.map(value =>
                      <div className="">
                        <p className="w-100 d-flex justify-content-between align-items-center mb-1">
                          <small>{petSize.find(val => val.value === value.pet_size_id).label}:</small><small> {getCurrencySign()} {value.service_charge}</small>
                        </p>
                      </div>
                  )}
                </div>
              </div>
            ))}
          </div>
          <hr />
          <div className="extra-charge">
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.WouldYouLikeToChargeExtraWhenProviding + ' ' + strings.dogWalking + ' ' + strings.duringHolidays_Q}
            </p>
            <p className="mb-2 font-12">
              {strings.EnterValueByWhichYouWouldWantPricesToIncreaseDuringHolidaysInBelowBoxLookAtlistOfHolidaysHere}
            </p>
            <div className="preview-details">
              <h6 className="mb-0 font-medium">
                {strings.Range}{" "}
                <span>
                  {data.pet_walking_service_fee.holiday_extra_charges}%
                </span>
              </h6>
            </div>
          </div>
          <hr />
          <div className="fees-content my-3">
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.CacellationPolicy}{" "}
            </p>
            <p className="mb-0 font-12">
              {strings.SelectCancellationPolicyForThisServiceUserWillSeeTheSameAtTheTimeOfBooking}
            </p>
            <div className="booking-for">
              <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  {data.pet_walking_service_fee.cancellation_policy == 1 ? (
                      <div className="row service-charges mb-0 pb-0">
                      <div className="custom-check ml-0">
                        <h6 className="font-medium mb-2">{strings.Flexible}</h6>
                        <p className="font-12">
                          {strings.ForBoardingAndHouseSittingYouWillGetAFullRefundIfYouCancelBeforeTheStayBegins}
                        </p>
                        <p className="font-12 mb-0">
                          {strings.ForWalksDayCareAndDropinVisitsYouWillGetAFullRefundIfYouCancelBeforeTheDaysServiceIsDelivered}
                        </p>
                      </div>
                    </div>
                  ) : data.pet_walking_service_fee.cancellation_policy == 2 ? (
                      <div className="row service-charges mb-0 pb-0">
                      <div className="custom-check ml-0">
                        <h6 className="font-medium mb-2">{strings.Moderate}</h6>
                        <p className="font-12">
                          {strings.YouCancelWithin48HoursOfBooking}
                        </p>
                        <p className="font-12 mb-0">
                          {strings.YouHavenotAlreadyCancelled3ReservationsInTheLast12months}
                        </p>
                        <p className="font-12 mb-0">
                          {strings.TheReservationYouareCancellingDoesnotOverlapWithAnotherReservationInYourAccountWhenRefund}
                        </p>
                        <p className="font-12 mb-0">
                          {strings.TheReservationYouAreCancellingDoesnotOverlapWith}
                        </p>
                      </div>
                    </div>
                  ) : (
                      <div className="row service-charges mb-0 pb-0">
                      <div className="custom-check ml-0">
                        <h6 className="font-medium mb-2">{strings.Strict}</h6>
                        <p className="font-12">
                          {strings.YouwllGetAFullRefundIfYouCancelBy1200NoonOneWeekBeforeTheStayBegins}
                        </p>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="bg-white main-background">
          <div className="service-title mb-2">
            <h6 className="preview-head">{strings.Prefrences_C} </h6>
            <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-0">
                {strings.CapacityAtOneTime}
              </h6>
              <p className="font-12 mb-1">
                {strings.HowMayDogsYouCanTakeForWalkTogether}
              </p>
              <h6 className="mb-0 font-medium">{`${
                D_WALK_COUNT.find(
                  (v) =>
                    v.value ==
                    data.pet_walking_service_preferences.dogs_walk_together
                ).label
              }`}</h6>
            </div>
            <hr />
            <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-0">
                {strings.WouldYouAllowToExtendWalkTimingsIfAskedAfterReachingClientsPlace}
              </h6>

              <h6 className="mb-0 font-medium">
                {data.pet_walking_service_preferences.extend_walk == 1
                  ? strings.Yes
                  : strings.No}
              </h6>
            </div>
            <hr />
            {data.pet_walking_service_preferences.extend_walk == 1 ? <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-1">
                {strings.AdditionChargesForExtendingTime}
              </h6>
              <h6 className="font-medium"><span className='text-muted'>{data.pet_walking_service_preferences.extended_charges_applied ? (strings.ExtraCharges_C + ' '): strings.No}</span>{data.pet_walking_service_preferences.extended_charges_applied ? (getCurrencySign() + data.pet_walking_service_preferences.extended_price_per_mint +'/' + strings.mins ): strings.No}</h6>
            </div>: null}
            {data.pet_walking_service_preferences.extend_walk == 1 ?<hr />:null}
            <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-1">
                {strings.Canyoupickuppetfromclientplace}
              </h6>
              <h6 className="font-medium">
                {data.pet_walking_service_preferences.pickup_from_client_home == 1
                    ? strings.Yes
                    : strings.No}
              </h6>
              {data.pet_walking_service_preferences && data.pet_walking_service_preferences.pickup_from_client_home ? <div className="d-flex pet-pickup-details">
                <div className="mr-5 font-14">
                  <p className="font-15 mb-1 mr-5"> {strings.ByWhatMeans_Q}</p>
                  <h6 className="mb-0 font-14 font-medium">
                    {/*{()=>getServicePreference()}*/}
                    {transportation ? transportation.length ? transportation.map((val, index)=> val.label + `${index == transportation.length-1 ? '' : ', '}`) : null : null}
                  </h6>
                </div>
              </div> : null}
            </div>
            {data.pet_walking_service_preferences && data.pet_walking_service_preferences.pickup_from_client_home ? <hr /> : null}
                {data.pet_walking_service_preferences && data.pet_walking_service_preferences.pickup_from_client_home ? <div className="main-padding pb-0">
              <h6 className="font-semibold text-muted font-14 mb-1">
                {strings.Doyouchargetransportationfee}
              </h6>
              <h6 className="font-medium">
                {data.pet_walking_service_preferences.has_transportation_fee ==
                1
                  ? strings.Yes
                  : strings.No}
              </h6>
              {data.pet_walking_service_preferences.has_transportation_fee ==
              1 ? (
                <div className="d-flex pet-pickup-details">
                  <div className="mr-5 font-14">
                    <p className="font-15 mb-1 mr-5"> {strings.transportFee}</p>
                    <table>
                      {transportation && transportation.length ? transportation.map((val, index) => <tr key={index}>
                        <td><h6 className="mb-0 font-14 font-medium mr-5">{val.label}</h6></td>
                        <td>{getCurrencySign() + transportFee.find(item => item.transport_mode_id == val.value).price_start_from}</td>
                      </tr>) : null}
                    </table>
                  </div>
                </div>
              ) : null}
            </div>: null}
          </div>
        </div>

        <div className="bg-white main-background">
          <div className="service-title mb-2">
            <h6 className="preview-head">{strings.Discount_C} </h6>
            <div className="main-padding pb-0">
              <h6 className="font-semibold mb-1">
                {strings.Specialdiscountforyourguests}
              </h6>
              {data.pet_walking_serice_discount && data.pet_walking_serice_discount.first_booking_offered == 1 ? (
                <div className="discount-check">
                  <div className="custom-check">
                    <label className="check pl-0">
                      <p className="font-medium mb-1">{strings.offonfirstbookingwithyou}</p>
                      <p className="font-12 mb-0">
                        {strings.Allowofonbookingamount}
                      </p>
                    </label>
                  </div>
                </div>
              ) : (
                <div className="discount-check">
                  <div className="custom-check">
                    <label className="check pl-0">
                      <p className="font-medium mb-1">{strings.NoOffersOnThisService}</p>
                      <p className="font-12 mb-0">
                        {strings.Allowofonbookingamount}
                      </p>
                    </label>
                  </div>
                </div>
              )}
            </div>
            <hr />
            <div className="main-padding pb-0">
              <div className="stay-discount">
                <h5>{strings.DiscountOnNumberOfDaysForAService}</h5>
                {data.offers.length ? data.offers.map((value) => (
                  <>
                    <div className="weekly-discount mb-2">
                      <h6 className="text-muted font-14 font-semibold">
                        {strings.Booking_C + value.number_of_bookings}
                      </h6>
                    </div>
                    <div className="weekly-discount mb-3">
                      <h6 className="text-muted font-14 font-semibold">
                        {strings.Discount_C + value.discount_percentage + '%'}
                      </h6>
                    </div>
                  </>
                )) : <p className="font-12 mb-0">{strings.DiscountNotProvided}</p>}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
}

DogWalkingPreview.getLayout = function (page) {
  return <User>{page}</User>;
};
