import React from "react";
import User from ".";
import EarningTab from "../../components/user/paymentHistory/EarningTab";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import moment from "moment";
import ReactPaginate from "react-paginate";
import I_SINGLE_REQUEST_DETAIL from "../../models/requestSitter.interface";
import { useRouter } from "next/router";

interface IState {
  tab: number;
  params: {
    start_date?: number;
    end_date?: number;
    sort_by: number;
    earning: number;
    transaction_type: number;
  };
  transactionData: Transaction;
  currentPage: number;
}

type Transaction = {
  current_page: number;
  total: number;
  data: I_SINGLE_REQUEST_DETAIL[];
  first_page_url: string;
  last_page_url: string;
  next_page_url: string;
};
interface IProps {
  router?: {
    query?: {
      index?: string;
    };
  };
}

let api = new API();

export default function PaymentHistoryWithRouter(props) {
  const router = useRouter();
  return <PaymentHistory {...props} router={router} />;
}
PaymentHistoryWithRouter.getLayout = function (page) {
  return <User>{page}</User>;
};

class PaymentHistory extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      tab: 1,
      params: {
        sort_by: null,
        earning: 1,
        transaction_type: null,
      },
      transactionData: null,
      currentPage: 1,
    };
  }

  componentDidMount() {
    if (this.props?.router?.query?.index) {
      this.setState(
        {
          params: {
            ...this.state.params,
            ["transaction_type"]: parseInt(this.props?.router?.query?.index),
          },
        },
        () => {
          this.getTransactionHistory();
        }
      );
    }
  }
  componentDidUpdate(prevProps: Readonly<IProps>): void {
    if (prevProps?.router?.query?.index !== this.props?.router?.query?.index) {
      this.setState(
        {
          params: {
            ...this.state.params,
            ["transaction_type"]: parseInt(this.props?.router?.query?.index),
          },
        },
        () => {
          this.getTransactionHistory();
        }
      );
    }
  }

  getTransactionHistory() {
    if (!this.state.params?.transaction_type) {
      return false;
    }
    let param: any = this.state.params;
    api
      .paymentHistory(param, this.state.currentPage)
      .then((res) => {
        if (res.data.status == 200) {
          this.setState({ transactionData: res.data.response });
        } else {
        }
      })
      .catch((error) => {
        console.log("error is", error);
      });
  }

  handleTabChange = (tab) => {
    this.setState(
      {
        tab: tab,
        params: {
          ...this.state.params,
          earning: tab,
        },
        currentPage: 1,
      },
      () => {
        this.getTransactionHistory();
      }
    );
  };

  getComponentByIndex = () => {
    switch (this.state.params.earning ?? 1) {
      case 1:
        return (
          <EarningTab
            handleSortby={this.handleSortby}
            data={this.state.transactionData}
            trasactionType={this.state?.params?.transaction_type}
            earning={this.state.params?.earning}
            handleFrom={(val) => this.handleDate(val, 1)}
            handleTo={(val) => this.handleDate(val, 2)}
          />
        );
      case 2:
        return (
          <EarningTab
            handleSortby={this.handleSortby}
            data={this.state.transactionData}
            trasactionType={this.state?.params?.transaction_type}
            earning={this.state.params?.earning}
            handleFrom={(val) => this.handleDate(val, 1)}
            handleTo={(val) => this.handleDate(val, 2)}
          />
        );
    }
  };

  handleSortby = (val) => {
    this.setState(
      {
        params: {
          ...this.state.params,
          sort_by: val.value,
        },
      },
      () => {
        this.getTransactionHistory();
      }
    );
  };

  handleDate(date, type) {
    date !== null
      ? this.setState(
          {
            params: {
              ...this.state.params,
              [type == 1 ? "start_date" : "end_date"]: moment(date).format("X"),
            },
          },
          () => {
            this.getTransactionHistory();
          }
        )
      : this.setState(
          {
            params: {
              earning: this.state.params.earning,
              sort_by: this.state.params.sort_by,
              transaction_type: this.state.params.transaction_type,
            },
          },
          () => {
            this.getTransactionHistory();
          }
        );
  }

  handlePageChange = (val) => {
    if (val.selected == 0) {
      this.setState({ currentPage: 1 }, () => {
        this.getTransactionHistory();
      });
    } else {
      this.setState({ currentPage: val.selected + 1 }, () => {
        this.getTransactionHistory();
      });
    }
  };

  render() {
    let { currentPage, transactionData } = this.state;
    return (
      <div className="col-12 col-md-8 col-lg-8 col-xl-8 column1">
        <div className="tabs-main main-background">
          <div className="payment d-none d-md-block d-lg-block d-xl-block">
            <h5 className="font-20 font-semibold mb-3">
              {strings.PaymentHistory}
            </h5>
          </div>
          <div className="row tabs-design">
            <div className="col-12 px-0 px-md-3">
              <div className="pay-tabs">
                <ul className="nav nav-tabs mb-0" id="myTab" role="tablist">
                  <li className="nav-item" role="presentation">
                    <a
                      className={
                        "nav-link " + (this.state.tab == 1 ? "active" : "")
                      }
                      id="earning-tab"
                      data-toggle="tab"
                      onClick={() => this.handleTabChange(1)}
                      role="tab">
                      {strings.completed}
                    </a>
                  </li>
                  <li className="nav-item" role="presentation">
                    <a
                      className={
                        "nav-link " + (this.state.tab == 2 ? "active" : "")
                      }
                      id="withdrawals-tab"
                      data-toggle="tab"
                      onClick={() => this.handleTabChange(2)}
                      role="tab">
                      {strings.cancelled}
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 content-main px-3 px-md-0">
          <div className="bg-white main-background">
            {this.getComponentByIndex()}
          </div>
          <div className="row ">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <ReactPaginate
                previousLinkClassName={"pagination__link"}
                nextLinkClassName={"pagination__link"}
                disabledClassName={"pagination__link--disabled"}
                containerClassName="pagination justify-content-center"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                activeClassName="active"
                initialPage={currentPage - 1}
                breakLabel="..."
                forcePage={currentPage - 1}
                nextLabel={false}
                previousLabel={false}
                onPageChange={(val) => this.handlePageChange(val)}
                pageRangeDisplayed={1}
                pageCount={transactionData ? transactionData.total / 10 : 1}
                renderOnZeroPageCount={null}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
  // static getLayout(page) {
  //   return <User>{withRouter(page)}</User>;
  // }
}
