import React, { useEffect, useState } from "react";
import User from ".";
import MeetingRequests from "../../components/user/dashboard/meetingRequests/MeetingRequests";
import Portfolio from "../../components/user/dashboard/Portfolio";
import NewsFeed from "../../components/user/dashboard/NewsFeed";
import {
  errorOptions,
  sitterStates,
  successOptions,
} from "../../public/appData/AppData";
import BookingRequestMobile from "../../components/user/dashboard/bookingRequests/BookingRequestMobile";
import StatusBar from "../../components/user/dashboard/StatusBar";
import API from "../../api/Api";
import withAuth from "../../components/hoc/withAuth";
import { AxiosError } from "axios";
import { useSnackbar } from "react-simple-snackbar";
import Link from "next/link";
import { strings } from "../../public/lang/Strings";
import { getCurrencySign, U_INVITE_URL } from "../../api/Constants";

const api = new API();
function Dashboard() {
  const [isSitter, setIsSitter] = useState(false);
  const [dashboardData, setDashboardData] = useState(null);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState("");
  const [emailErr, setEmailErr] = useState(false);
  const [newsFeed, setNewsFeed] = useState<any>([]);

  useEffect(() => {
    getServices();
    getDashboard();
  }, []);
  useEffect(() => {
    api
      .getLandingPageNewsRoom()
      .then((res) => {
        setNewsFeed(res.data.response?.data);
      })
      .catch((error) => console.log(error));
  }, []);

  const getDashboard = async () => {
    api
      .getDashboard({ filter_by: null })
      .then((response: any) => {
        setDashboardData(response.data.response);
      })
      .catch((error: AxiosError) => {
        //console.log(error);
      });
  };

  const handleCopy = (textToCopy) => {
    navigator.clipboard.writeText(textToCopy)
      .then(() => {
        console.log('Text copied to clipboard');
      })
      .catch((error) => {
        console.error('Failed to copy text:', error);
      });
  };


  const getServices = async () => {
    let that = this;
    await api
      .getService()
      .then((response) => {
        response.data.response.some((element: any) => {
          if (element.is_active == 1) {
            setIsSitter(true);
          }
        });
      })
      .catch((error) => console.log(error));
  };

  const copyUrl = (e) => {
    e.preventDefault();
    const el = document.createElement("input");
    el.value = `${U_INVITE_URL + dashboardData?.referral_token}`;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopied(true);
    openSuccess(strings.CopiedToClipboard);
  };

  const onClickInviteMail = (value) => {
    if (!email) {
      openError(strings.EnterEmailAddress);
      setEmailErr(true);
    } else {
      location.href = `mailto:${email}?subject=${
        dashboardData.firstname +
        " " +
        dashboardData.lastname +
        " " +
        strings.profile
      }&body=${
        encodeURIComponent(U_INVITE_URL + dashboardData?.referral_token) || ""
      }`;
    }
  };

  return (
    <div className="col-12 col-md-8 col-lg-8 col-xl-8 column1">
      <div className="col-12 px-3 px-md-0">
        <StatusBar
          states={sitterStates}
          isSitter={isSitter}
          dashboard={dashboardData}
        />

      {dashboardData?.user_document.length < 2 ? (
        
        <div className="alert alert-danger" role="alert">
       <div className="w-100 border-0">
          <span>
            <small className="m-2">
              <b>{strings.Tobecomeasitterneedtouploadthedocuments}</b>
            </small>
          </span>
        </div>
      </div>
      ) : (
        <>
          {dashboardData?.user_document.length >= 2 && dashboardData?.is_verified != 1 && (
           <div className="alert alert-danger" role="alert">
             <div className=" w-100 border-0">
               <span>
                 <small className="m-0">
                   <b>
                     {dashboardData?.user_document[0]?.status === 2 || dashboardData?.user_document[1]?.status
                       ? strings.ProfilerejectedPleaseuploadvaliddetailsanddocuments
                       : strings.Yourprofileisunderreviewandwillbeapprovedsoon}
                   </b>
                 </small>
               </span>
             </div>
           </div>
          )}
        </>
      )}


        <BookingRequestMobile isSitter={isSitter} />

        <div className="row">
          {/*<BookingRequests isSitter={isSitter} />*/}
          <MeetingRequests isSitter={isSitter} />
        </div>
        <Portfolio />
        {dashboardData?.is_welcome_coupon_used == 0 ?
        <div className="points-details mb-4">
          <div className="circle"></div>
          <div className="circle tr"></div>
          <div className="circle br"></div>
          <div className="circle bl"></div>
             <div className="row align-items-center">
            <div className="col-12 col-md-8 col-lg-8 col-xl-9">
              <h5 className="text-uppercase fw-bold font-bold font-weight-bold mb-4 bg-warning w-auto d-inline-block py-2 px-3 rounded-5">
                Discout coupon
              </h5>
              <div className="invitions mb-3">
                <h5 className="text-white font-semibold">
                  {strings.WELCOMETOPETCATION}{" "}
                </h5>
                <h6>{strings.LetsMakeADaySpecialForYouPets}</h6>
              </div>
            </div>
            <div className="col-12 col-md-3 col-lg-3 col-xl-3">
              <div className="book-now text-center">
                <p className="mb-0 font-14">
                  {getCurrencySign()}1500 <br />
                  {strings.OffOnFirstBooking}
                </p>

                {/*<Link href="/search-sitter">
                  <button className="btn btn-yellow">{strings.Makeyourfirstbooking}</button>
                </Link>*/}
                 {/* copy code functionality */}
                 <button
                  className="btn btn-yellow"
                  onClick={() => handleCopy(dashboardData?.welcome_coupon)}
                >
                  <span className="small font-weight-bold">{dashboardData?.welcome_coupon}{" "}</span>
                  {/*<span className="small bg-light">copy</span>*/}
                </button>
              </div>
            </div>
          </div>
        </div>: " "}

        <div className="earn-background">
          <div className="earn-section">
            <p className="mb-0 font-14 text-white">
              {strings.EarnA}
              <span className="font-semibold">
                {getCurrencySign()}1500 {strings.COUPON}{" "}
              </span>
              {strings.forInvitingOtherPetsitterToJoinThePlatform}
            </p>
          </div>

          <div className="row email-row">
            <div className="col-xl-7">
              <div className="input-group group-btn">
                <input
                  type="text"
                  className={`form-control bg-white ${
                    emailErr ? "invalid" : ""
                  }`}
                  placeholder={strings.PleaseEnterEmailHereForYourReferrals}
                  value={email}
                  onChange={(e) => {
                    setEmailErr(false);
                    setEmail(e.target.value);
                  }}
                />
                <div
                  className="input-group-append"
                  onClick={() => onClickInviteMail(email)}
                >
                  <span className="input-group-text email">
                    {strings.InviteViaEmail}
                  </span>
                </div>
              </div>
            </div>
            {/*<div className="col-xl-3">*/}
            {/*  <div className="input-group-text email">*/}
            {/*    <a href="#">{strings.InviteViaTwitter}</a>*/}
            {/*  </div>*/}
            {/*</div>*/}
          </div>

          <div className="copy-url">
            <div className="d-flex">
              <div className="referral-details my-auto">
                <h6 className="font-12 font-semibold mb-0 my-auto">
                  {strings.ORCopyBelowReferralURLAndShare}
                </h6>
              </div>
              <div className="refferal-link">
                <a href="#" className="font-10">
                  {U_INVITE_URL + dashboardData?.referral_token}{" "}
                </a>
                <span className="font-12" onClick={copyUrl}>
                  {copied ? strings.Copied : strings.CopyURLByClickingHere}
                </span>
              </div>
            </div>
            <p className="font-10 mb-0">
              {
                strings.ReferredFriendMustHaveCreatedTheirPetcationAccountThrough
              }
            </p>
          </div>
        </div>
        <NewsFeed newsFeed={newsFeed} />
      </div>
    </div>
  );
}

Dashboard.getLayout = function getLayout(page) {
  return <User>{page}</User>;
};

export default Dashboard;
