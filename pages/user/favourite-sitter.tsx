import Link from "next/link";
import React, { useState, useEffect } from "react";
import {
    cities,
    errorOptions,
    favouriteSorting,
    petSize,
    serviceData,
    sort,
    successOptions
} from "../../public/appData/AppData";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import {getCurrencySign, GOOGLE_PLACES_API} from "../../api/Constants";
import MyMapComponent from "../../components/user/myProfile/Map";
import { useSnackbar } from 'react-simple-snackbar';
import {confirmAlert} from "react-confirm-alert";
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import SitterObject from "../../components/SearchSitter/SitterObject";
import {json} from "stream/consumers";
import Select from "react-select";
import CreateWishlist from "../../components/common/CreateWishlist";

interface SORTING_FILTER {
    key: number;
    value: number;
    label: string;
}
let api = new API();

const FavouriteSitter: React.FC<{}> = () => {
    let [latlng, setLatLng] = useState<any[]>([]);
    const [cities, setCities] = useState<any>([{ key: 0, value: 0, label: strings.AllCities }]);
    const [selectedSorting, setSelectedSorting] = useState<SORTING_FILTER>(favouriteSorting[0])
    const [selectedService, setSelectedService] = useState<SORTING_FILTER>({ key: 0, value: 0, label: strings.AllServices })
    const [selectedCities, setSelectedCities] = useState<SORTING_FILTER>({ key: 0, value: 0, label: strings.AllCities })
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [openError, closeError] = useSnackbar(errorOptions);
    const [currentHoveringSitter, setCurrentHoveringSitter] = useState(null)
    const [defaultCenter, setDefaultCenter] = useState({lat: 36.2048, lng: 138.2529})
    // wishlist
    const [sitterData, setSitterData] = useState<any[]>([]);
    const [showModal, setShowModal] = useState<any>(false);
    const [wishList, setWishList] = useState<any>([]);
    const [selectedWishlist, setSelectedWishlist] = useState<any>(null);
    const [data, setData] = useState(null);


    useEffect(() => {
        getCities()
        getAllWishList(null)
    }, []);

    // useEffect(() => {
    //     if (selectedService.value === 0) {
    //         let data = {
    //             city: selectedCities.value === 0 ? '' : selectedCities.label,
    //             sort: selectedSorting.value
    //         }
    //         getFavoriteSitters(data, selectedWishlist?.value)
    //     } else {
    //         let data = {
    //             service_id: selectedService.value,
    //             city: selectedCities.value === 0 ? '' : selectedCities.label,
    //             sort: selectedSorting.value
    //         }
    //         getFavoriteSitters(data, selectedWishlist?.value)
    //     }
    // }, [selectedSorting, selectedCities, selectedService]);

    const getFavoriteSitters = (data, id) => {
        api
            .getSingleSitterList(data, id)
            .then(res => {
                let address = res.data.response.map((val) => { return { lat: Number(val.sitter.address.map_latitude), lng: Number(val.sitter.address.map_longitude), id: val.sitter.id } })
                setSitterData(res.data.response);
                setLatLng(address);
            })
            .catch(err => console.log(err.response.data.message))
    };

    const getCities = () => {
        api
            .getCities(1)
            .then((json) => {
                let city = json.data.response.map((val, index) => ({ key: index+1, value: index+1, label: val.name }))
                let data = [{ key: 0, value: 0, label: strings.AllCities }].concat(city);
                setCities(data);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const handleBookmarkSitter = (sitter: any) => {
        api.markUnmarkSitter({ sitter_id: sitter.id }).then((res) => {
            if (res.data.status === 200) {
                let data = {
                    service_id: selectedService.value,
                    city: selectedCities.value === 0 ? '' : selectedCities.label,
                    sort: selectedSorting.value
                }
                getFavoriteSitters(data, selectedWishlist?.value);
                openSuccess(strings.unmarkSitter);
            }
        }).catch((error) => {
            console.log('eror is', error)
            //openSnackbar(strings.errorUpdatingStatus);
        })
    }

    const getSingleWishlist = (id) => {
        if (selectedService.value === 0) {
            let data = {
                city: selectedCities.value === 0 ? '' : selectedCities.label,
                sort: selectedSorting.value
            }
            console.log({data})
            getFavoriteSitters(data, id)
        } else {
            let data = {
                service_id: selectedService.value,
                city: selectedCities.value === 0 ? '' : selectedCities.label,
                sort: selectedSorting.value
            }
            console.log({data})
            getFavoriteSitters(data, id)
        }
    }

    const getCityFilteredSingleWishlist = (fdata) => {
        if (selectedService.value === 0) {
            let data = {
                city: fdata.value === 0 ? '' : fdata.label,
                sort: selectedSorting.value
            }
            console.log({data})
            getFavoriteSitters(data, selectedWishlist.value)
        } else {
            let data = {
                service_id: selectedService.value,
                city: fdata.value === 0 ? '' : fdata.label,
                sort: selectedSorting.value
            }
            console.log({data})
            getFavoriteSitters(data, selectedWishlist.value)
        }
    }

    const getServiceFilteredSingleWishlist = (fdata) => {
        if (fdata.value === 0) {
            let data = {
                city: selectedCities.value === 0 ? '' : selectedCities.label,
                sort: selectedSorting.value
            }
            console.log({data})
            getFavoriteSitters(data, selectedWishlist.value)
        } else {
            let data = {
                service_id: fdata.value,
                city: selectedCities.value === 0 ? '' : selectedCities.label,
                sort: selectedSorting.value
            }
            console.log({data})
            getFavoriteSitters(data, selectedWishlist.value)
        }
    }

    const getSortingFilteredSingleWishlist = (fdata) => {
        if (selectedService.value === 0) {
            let data = {
                city: selectedCities.value === 0 ? '' : selectedCities.label,
                sort: fdata.value
            }
            console.log({data})
            getFavoriteSitters(data, selectedWishlist?.value)
        } else {
            let data = {
                service_id: selectedService.value,
                city: selectedCities.value === 0 ? '' : selectedCities.label,
                sort: fdata.value
            }
            console.log({data})
            getFavoriteSitters(data, selectedWishlist?.value)
        }
    }

    const deleteWishlistApiCall = (id) => {
        api
            .deleteSitterWishlist(id)
            .then(res => {
                if (wishList.length)
                getAllWishList(null);
            })
            .catch(err => console.log(err.response?.data?.message))
    }

    const deleteWishlist = (props) => {
        confirmAlert({
            closeOnClickOutside: false,
            customUI: ({onClose}) => {
                return (
                    <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
                        <div className="react-confirm-alert-body">
                            <h4>{strings.Delete + " " + props.label}</h4>
                            <p>{strings.areYouSure}</p>
                            <div className="react-confirm-alert-button-group">
                                <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                                    deleteWishlistApiCall(props.value)
                                    onClose();
                                }}>{strings.Delete}
                                </button>
                                <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                                    onClose();
                                }}>{strings.Cancel}
                                </button>
                            </div>
                        </div>
                    </div>
                );
            }
        });
    }

    const getAllWishList = (value) => {
        api
            .getSitterLists()
            .then(res => {
                let wishlist = res.data.response.map((val, index) => ({
                        label: val.name,
                        value: val.id,
                        key: index+1
                    }));
                if (value === null && res.data.response.length) {
                    setSelectedWishlist({
                        label: res.data.response[0].name,
                        value: res.data.response[0].id,
                        key: 1
                    })
                    getSingleWishlist(res.data.response[0].id)
                } else if (value === null && !res.data.response.length) {
                    setSelectedWishlist(null)
                    setSitterData([]);
                    setLatLng([]);
                }
                setWishList(wishlist)
                if (selectedWishlist == null && res.data.response.length) {
                    setSelectedWishlist({
                            label: res.data.response[0].name,
                            value: res.data.response[0].id,
                            key: 1
                        })
                    getSingleWishlist(res.data.response[0].id) ///need to change
                } else if (value != null && res.data.response.length) {
                    setSelectedWishlist({
                            label: res.data.response.find(val => val.id == value).name,
                            value: res.data.response.find(val => val.id == value).id,
                            key: res.data.response.findIndex(val => val.id == value) + 1
                        })
                }
            })
            .catch(err => console.log(err.response.data.message))
    }

    const markAsFavourite = (listId, spotId) => {
        let data = {
            list_id:listId,
            sitter_id:spotId
        }
        api
            .markSitterAsFavourite(data)
            .then(res => {
                getSingleWishlist(listId)
                openSuccess(res.data.message)
            })
            .catch(err => openError(err.response.data.message))
    }

    return (
        <>
            <div className="main-wrapper my-3">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-8 col-lg-8 col-xl-8 pd-right">
                            <div className="col-12 fav-title px-0 mb-3 mt-2 d-none d-lg-block">
                                <div className="row align-items-center">
                                    <div className="col">
                                        <div className="d-inline mr-2">
                                            <Link href={'/user/dashboard/'}>
                                                <a>
                                                    <svg
                                                        aria-hidden="true"
                                                        focusable="false"
                                                        data-prefix="far"
                                                        data-icon="chevron-left"
                                                        role="img"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 256 512"
                                                        className="svg-inline--fa fa-chevron-left fa-w-8 fa-2x"
                                                    >
                                                        <path
                                                            fill="currentColor"
                                                            d="M231.293 473.899l19.799-19.799c4.686-4.686 4.686-12.284 0-16.971L70.393 256 251.092 74.87c4.686-4.686 4.686-12.284 0-16.971L231.293 38.1c-4.686-4.686-12.284-4.686-16.971 0L4.908 247.515c-4.686 4.686-4.686 12.284 0 16.971L214.322 473.9c4.687 4.686 12.285 4.686 16.971-.001z"
                                                            className=""
                                                        />
                                                    </svg>
                                                </a>
                                            </Link>
                                        </div>
                                        <h5 className="d-inline font-20">{strings.MyFavourites}</h5>
                                    </div>
                                    <div className="col-md-auto mt-auto">
                                        <button onClick={()=> setShowModal(true)} className="btn btn-primary btn1 px-2 py-2 py-md-1 h-auto">{strings.CreateNewWishlist}</button>
                                    </div>
                                </div>
                            </div>
                            <div className="bg-white main-background p-md-4 p-3">
                                <div className="col-12 px-0 mb-2 mb-md-0">
                                    <Select
                                        onChange={(e) => {
                                            setSelectedWishlist(e)
                                            getSingleWishlist(e.value)
                                        }}
                                        value={selectedWishlist}
                                        isSearchable={false}
                                        className="text-capitalize"
                                        options={wishList}
                                        isMulti={false}
                                    />
                                </div>
                                <div className="sort-filter">
                                    <div className="content-margin">
                                        <div className="col-12 px-0">
                                            <div className="row">
                                                <div className="col-auto my-auto d-none d-md-block d-lg-block d-xl-block">
                                                    <h6 className="sort-by mb-0">{strings.Sortby}</h6>
                                                </div>
                                                <div className="col">
                                                    <div className="row justify-content-between">
                                                        <div className="col-12 mb-2 mb-md-0 col-sm pr-md-0">
                                                            <Select
                                                                onChange={(e) => {
                                                                    setSelectedSorting(e)
                                                                    getSortingFilteredSingleWishlist(e)
                                                                }}
                                                                value={selectedSorting}
                                                                isSearchable={false}
                                                                options={favouriteSorting}
                                                                isMulti={false}
                                                            />
                                                        </div>
                                                        <div className="col-12 mb-2 mb-md-0 col-sm pr-md-0">
                                                            <Select
                                                                onChange={(e) => {
                                                                    setSelectedService(e)
                                                                    getServiceFilteredSingleWishlist(e)
                                                                }}
                                                                value={selectedService}
                                                                isSearchable={false}
                                                                options={[{ key: 0, value: 0, label: strings.Allservices } , ...serviceData]}
                                                                isMulti={false}
                                                            />
                                                        </div>
                                                        <div className="col-12 mb-2 mb-md-0 col-sm">
                                                            <Select
                                                                onChange={(e) => {
                                                                    setSelectedCities(e)
                                                                    getCityFilteredSingleWishlist(e)
                                                                }}
                                                                value={selectedCities}
                                                                isSearchable={false}
                                                                options={cities}
                                                                isMulti={false}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {selectedWishlist ?
                                    <div className='d-flex align-items-center py-2 mb-3 border-bottom'>
                                        <h6 className="mb-0 text-ch">{selectedWishlist.label}</h6>
                                        <div className="col-auto">
                                            <a className="text-secondary small mr-3 cursor-pointer text-decoration-underline"
                                               onClick={() => {
                                                   setData(selectedWishlist);
                                                   setShowModal( true);
                                               }}
                                            >
                                                Edit
                                            </a>
                                            <a className="text-danger small cursor-pointer text-decoration-underline"
                                               onClick={() => deleteWishlist(selectedWishlist)}
                                            >
                                                Remove
                                            </a>
                                        </div>
                                </div> : <div className="text-center padding">
                                    <p className="font-13 mb-0 font-italic">{strings.PleaseCreateNewWishlist}</p>
                                </div>}
                                {sitterData?.length ? sitterData.map((item, index) =>
                                    <>
                                        <div
                                            className="spot-fav-list"
                                            // onMouseLeave={()=> setCurrentHoveringSitter(null)}
                                            onMouseEnter={()=>{
                                                if(defaultCenter.lat != item?.sitter?.address?.map_latitude || defaultCenter.lng != item?.sitter?.address?.map_longitude) {
                                                    setCurrentHoveringSitter(item?.sitter);
                                                    setDefaultCenter({lat: item?.sitter?.address?.map_latitude, lng: item?.sitter?.address?.map_longitude});
                                                }
                                                }}
                                        >
                                            <div key={index} className="col-12 spot-fav-info">
                                                <div className="row justify-content-between">
                                                    <div className="col-12 col-md-9 pr-md-0">
                                                        <div className="row align-items-center">
                                                            <div className="col-auto pr-0">
                                                                <img src={item.sitter.profile_picture} className="img-fluid rounded-circle" width="50px"/>
                                                            </div>
                                                            <div className="col">
                                                                <div className="search-sitter-details my-auto">
                                                                    <h6 className="mb-1">{item.sitter.firstname + ' ' + item.sitter.lastname}</h6>
                                                                    <p className="font-12 mb-0">
                                                                        {item.sitter.address.address}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div className="col-12 mb-2 mt-2">
                                                                <div className="household-details">
                                                                    {item.sitter.address.live_in_house == 1 ? (
                                                                        <p className="mb-0 mr-3 font-12 d-inline text-muted">{strings.IsAnApartment}</p>
                                                                    ) : null}
                                                                    {item.sitter.address.non_smoking_household == 1 ? (
                                                                        <p className="mb-0 mr-3 font-12 d-inline text-muted">
                                                                            {strings.NonSmokingHousehold}
                                                                        </p>
                                                                    ) : null}
                                                                    {item.sitter.address.dog_other_pets == 1 ? (
                                                                        <p className="mb-0 mr-3 font-12 d-inline text-muted">{strings.HasNoDogs}</p>
                                                                    ) : null}
                                                                    {item.sitter.address.fenced_yard ? (
                                                                        <p className="mb-0 mr-3 font-12 d-inline text-muted">
                                                                            {strings.DoesNotHaveAYard}
                                                                        </p>
                                                                    ) : null}
                                                                    {item.sitter.address.dog_other_pets == 1 ? (
                                                                        <p className="mb-0 font-12 d-inline text-muted">{strings.HasPet}</p>
                                                                    ) : null}
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="d-flex featured-details">
                                                                    <div className="d-flex hotel-rating">
                                                                        {[1,2,3,4,5].map(val => <div className="rating-star ">
                                                                            <div className={item.sitter.overall_rate >= val ? "active" : ""}>
                                                                                <svg
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="fas"
                                                                                    data-icon="star"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 576 512"
                                                                                    className="svg-inline--fa fa-star fa-w-18 fa-2x"
                                                                                >
                                                                                    <path
                                                                                        fill="currentColor"
                                                                                        d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                                                                                        className=""
                                                                                    />
                                                                                </svg>
                                                                            </div>
                                                                        </div>)}
                                                                        <div className="my-auto">
                                                                            <h6 className="mb-0  font-semibold">
                                                                                {item.sitter.overall_rate}{" "}
                                                                                <span className="text-muted font-normal font-12">
                                            ({item.sitter.total_review + ' ' + strings.reviews})
                                          </span>
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-12 col-md-auto alignment align-fav-option">
                                                        <div className="col col-md-auto review-icon mb-md-3 mt-0 order-1 order-md-0">
                                                            <div className="vector-icon d-flex justify-content-end">
                                                                <div
                                                                    className="ellipse mx-0"
                                                                    data-toggle="modal"
                                                                    data-target="#send-invite">

                                                                    <a onClick={() =>{
                                                                        confirmAlert({
                                                                            closeOnClickOutside: true,
                                                                            customUI: ({ onClose }) => {
                                                                                return (
                                                                                    <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
                                                                                        <div className="react-confirm-alert-body">
                                                                                            <h1>{strings.RemoveSitterFromFavourite_Q}</h1>
                                                                                            <p>{strings.areYouSure}</p>
                                                                                            <div className="react-confirm-alert-button-group">
                                                                                                <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                                                                                                    markAsFavourite(selectedWishlist.value, item.sitter?.id)
                                                                                                    onClose();
                                                                                                }}>{strings.Yes}</button>
                                                                                                <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                                                                                                    onClose();
                                                                                                }}>{strings.No}</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                );
                                                                            }
                                                                        });
                                                                    }}>
                                                                        <div className="delete-icon">
                                                                            <svg
                                                                                aria-hidden="true"
                                                                                focusable="false"
                                                                                data-prefix="fal"
                                                                                data-icon="trash-alt"
                                                                                role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512"
                                                                                className="svg-inline--fa fa-trash-alt fa-w-14 fa-2x"
                                                                            >
                                                                                <path
                                                                                    fill="currentColor"
                                                                                    d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z"
                                                                                    className=""
                                                                                />
                                                                            </svg>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="search-sitter-content order-0 order-md-1">
                                                            <div className="contact-now">
                                                                <Link
                                                                    href={{
                                                                        pathname: "/booking",
                                                                        query: {
                                                                            sitterId: item.sitter.id,
                                                                            service: '',
                                                                            name: item.sitter.firstname + " " + item.sitter.lastname,
                                                                        },
                                                                    }}
                                                                    as={"/booking"}
                                                                >
                                                                    <button className="btn btn-primary w-auto px-3">{strings.Contactnow}</button>
                                                                </Link>
                                                            </div>
                                                            <div className="view-details mb-md-3">
                                                                <Link href={{ pathname: "/sitter-profile/" + item.sitter.id, query: {serviceId: ''} }}>
                                                                    {strings.Viewdetails}
                                                                </Link>
                                                            </div>
                                                            {/*<div className="row">*/}
                                                            {/*    <div className="col-12">*/}
                                                            {/*        <h6 className="mb-0  font-semibold">*/}
                                                            {/*            {getCurrencySign()}{item.sitter.price ?? 0}{" "}*/}
                                                            {/*            <span className="text-muted font-normal font-12"> / {strings.night} </span>*/}
                                                            {/*        </h6>*/}
                                                            {/*    </div>*/}
                                                            {/*</div>*/}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                ) : selectedWishlist ? <div className="text-center padding">
                                    <p className="font-13 mb-0 font-italic">{strings.noSitterFound}</p>
                                </div> : null}
                            </div>
                        </div>
                        <div className="col-12 col-md-4 col-lg-4 col-xl-4 d-none d-md-block">
                            <div className="col-12 position-sticky top-0">
                                <div className="map-info fav-map">
                                    <MyMapComponent
                                        draggable={false}
                                        data={latlng ? latlng : []}
                                        latlng={latlng ? latlng : ''}
                                        defaultCenter={{lat: Number(defaultCenter?.lat), lng: Number(defaultCenter?.lng)}}
                                        currentHoveringSitter={currentHoveringSitter}
                                        mapCenter={{lat: Number(defaultCenter?.lat), lng: Number(defaultCenter?.lng)}}
                                        mapZoom={10}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-block d-lg-none book-fixed">
                    <div className="col-12">
                        <button onClick={()=> setShowModal(true)} className="btn btn-primary px-3">
                            {strings.CreateNewWishlist}
                        </button>
                    </div>
                </div>
            </div>
            <CreateWishlist
                showModal={showModal}
                hideModal={() => setShowModal(false)}
                reRender={(value) => getAllWishList(value)}
                data={data}
                setData={()=>{
                    setData(null)
                }}
                isSitterWishlist={true}
            />
        </>
    );
};

export default (FavouriteSitter);
