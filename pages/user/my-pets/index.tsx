import { AxiosResponse } from "axios";
import Link from "next/link";
import Router from "next/router";
import React from "react";
import User from "..";
import API from "../../../api/Api";
import Loader from "../../../components/common/Loader";
import I_PET from "../../../models/pet.interface";
import Res from "../../../models/response.interface";
import { strings } from "../../../public/lang/Strings";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import AddPet from "./add-pet";
import {Dropdown, DropdownButton} from "react-bootstrap";

const api = new API();

interface IState {
  pets: I_PET[];
  loading: boolean;
}


interface Iprops {
  router: any,
}

class MyPetsUser extends React.Component<Iprops, IState> {
  constructor(props) {
    super(props);
    this.state = {
      pets: [],
      loading: true,
    };

    this.deletePet = this.deletePet.bind(this);
    this.getPets();
  }

  getPets = async () => {
    let that = this;
    api
      .getAllPets()
      .then((response: AxiosResponse<Res<I_PET[]>>) => {
        that.setState(
          {
            pets: response.data.response,
            loading: false,
          },
          () => {
            if (this.state.pets.length == 0) {
              Router.push("/user/my-pets/add-pet");
            }
          }
        );
      })
      .catch((error) => console.log(error));
  };

  deletePet = (id) => {
    confirmAlert({
      title: strings.deletePet,
      message: strings.areYouSure,
      buttons: [
        {
          label: strings.Yes,
          onClick: () => {
            api
                .deletePet(id)
                .then((response) => this.getPets())
                .catch((error) => console.log(error));
          }
        },
        {
          label: strings.No,
          onClick: () => {}
        }
      ]
    });
  };

  render() {
    return (
      <div className="col-12 col-md-8 col-lg-8 col-xl-8 column1">
        <div className="bg-white main-background">
          <h4 className="font-20 font-semibold mb-3 d-none d-md-block">{strings.Mypets}</h4>
          {this.state.loading ? (
            <Loader />
          ) : (
            this.state.pets.map((value) => (
              <>
                <div className="row">
                  <div className="col-12 col-md">
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="images">
                          <img src={value.pet_image} className="img-fluid rounded-15" />
                        </div>
                      </div>
                      <div className="col p-left my-auto">
                        <div className="mypet-details">
                          <h6 className="mb-0 font-20">{value.pet_name}</h6>
                          <p className="mb-1 mb-md-3">{value.breed.breed}</p>
                          <div className="d-flex flex-row flex-md-column align-items-center align-items-md-start">
                            <p className="mb-0 mr-3">{value.age_year + " " + strings.yearsOld}</p>
                            <p className="mb-0">{value.weight.name}</p>
                          </div>
                        </div>
                      </div>
                      <div className="col-auto">
                        <DropdownButton
                            className='pet-drop mob-drop mt-2 mt-md-0'
                            align="end"
                            title={<a>
                              <div className="ellipse">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fal"
                                    data-icon="ellipsis-h"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 320 512"
                                    className="svg-inline--fa fa-ellipsis-h fa-w-10 fa-2x"
                                >
                                  <path
                                      fill="currentColor"
                                      d="M192 256c0 17.7-14.3 32-32 32s-32-14.3-32-32 14.3-32 32-32 32 14.3 32 32zm88-32c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zm-240 0c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"
                                  />
                                </svg>
                              </div>
                            </a>}
                            id="dropdown-menu-align-end"
                        >
                          <Dropdown.Item
                              eventKey="0"
                              onClick={() => Router.replace({
                                pathname: "/user/my-pets/add-pet",
                                query: { id: value.id },})
                              }
                          >
                            <a>
                              {strings.Edit}
                            </a>
                          </Dropdown.Item>
                          <Dropdown.Item eventKey="1" onClick={() => this.deletePet(value.id)}>
                            <a>
                              {strings.Remove}
                            </a>
                          </Dropdown.Item>
                        </DropdownButton>
                      </div>
                    </div>
                  </div>
                </div>
                <hr />
              </>
            ))
          )}

          {this.state?.pets?.length ? <div className="col-12 p-0 d-none d-lg-block">
            <Link href="/user/my-pets/add-pet">
              <a>
                <button className="btn btn-primary">{strings.Addnewpet}</button>
              </a>
            </Link>
          </div> : <AddPet />}
        </div>
        {this.state?.pets?.length ?
            <div className="d-block d-lg-none book-fixed">
              <div className="col-12">
                <Link href="/user/my-pets/add-pet">
                <button className="btn btn-primary px-3">
                  {strings.Addnewpet}
                </button>
                </Link>
              </div>
            </div> : null
        }
      </div>
    );
  }

  static getLayout(page) {
    return <User>{page}</User>;
  }
}

export default MyPetsUser
