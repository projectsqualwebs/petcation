import User from ".";
import "react-day-picker/lib/style.css";
import ReservationView from "../../components/user/reservation/ReservationView";
import React, { useState } from "react";
import { useEffect } from "react";
import { useRouter } from "next/router";
import {strings} from "../../public/lang/Strings";

/**
 *
 * @function contains reservation parent view
 */
export default function Reservation() {
  const router = useRouter();

  const [currentTab, setCurrentTab] = useState(0);
  const [reservationFor, setReservationFor] = useState<string>("1");


  /**
   * @function will help to set current tab according to sitter or pet owner
   */
  useEffect(() => {
    setReservationFor(`${router.query.index}`);
    setCurrentTab(0);
    if(router.query?.activeTab) {
      setCurrentTab(Number(router.query?.activeTab));
    }
  }, [router.query]);

  return (
    <div className="col-12 col-md-8 col-lg-8 col-xl-8 column1">
      <div className="tabs-main main-background">
        <div className="row tabs-design">
          <div className="col-12 px-0 px-md-3">
            <div className="pay-tabs">
              <ul className="nav nav-tabs mb-0" id="myTab" role="tablist">
                <li className="nav-item" role="presentation">
                  <a
                      className={"nav-link cursor-pointer " + (currentTab == 0 ? "active" : "")}
                      id="request-tab"
                      data-toggle="tab"
                      role="tab"
                      onClick={() => setCurrentTab(0)}
                  >
                    Pending Request
                  </a>
                </li>
                <li className="nav-item" role="presentation">
                  <a
                      className={"nav-link cursor-pointer " + (currentTab == 1 ? "active" : "")}
                      id="booking-tab"
                      data-toggle="tab"
                      role="tab"
                      onClick={() => setCurrentTab(1)}
                  >
                    Accepted
                  </a>
                </li>
                <li className="nav-item" role="presentation">
                  <a
                      className={"nav-link cursor-pointer " + (currentTab == 3 ? "active" : "")}
                      id="booking-tab"
                      data-toggle="tab"
                      role="tab"
                      onClick={() => setCurrentTab(3)}
                  >
                    Upcoming Bookings
                  </a>
                </li>
                <li className="nav-item" role="presentation">
                  <a
                      className={"nav-link cursor-pointer " + (currentTab == 4 ? "active" : "")}
                      id="past-tab"
                      data-toggle="tab"
                      role="tab"
                      onClick={() => setCurrentTab(4)}
                  >
                    Past
                  </a>
                </li>
                <li className="nav-item" role="presentation">
                  <a
                      className={"nav-link cursor-pointer " + (currentTab == 2 ? "active" : "")}
                      id="cancelled-tab"
                      data-toggle="tab"
                      role="tab"
                      onClick={() => setCurrentTab(2)}
                  >
                    Cancelled
                  </a>
                </li>
                <li className="nav-item" role="presentation">
                  <a
                      className={"nav-link cursor-pointer " + (currentTab == 5 ? "active" : "")}
                      id="disputed-tab"
                      data-toggle="tab"
                      role="tab"
                      onClick={() => setCurrentTab(5)}
                  >
                    Disputed
                  </a>
                </li>
              </ul>
            </div>

          </div>
        </div>
      </div>
      <div className="bg-white main-background pending-request">
        <ReservationView type={currentTab} reservationFor={reservationFor} />
      </div>
    </div>
  );
}

Reservation.getLayout = function getLayout(page) {
  return <User>{page}</User>;
};
