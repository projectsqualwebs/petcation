import React, { useEffect, useState } from "react";
import { strings } from "../../public/lang/Strings";
import User from ".";
import { useRouter } from "next/router";
import { errorOptions, successOptions } from "../../public/appData/AppData";
import API from "../../api/Api";
import Cookies from "universal-cookie";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import OverlayLoader from "../../components/common/OverlayLoader";
import {useSnackbar} from "react-simple-snackbar";

let api = new API();

export default function PasswordSecurity() {
  const router = useRouter();
  const [resetView, setResetView] = useState(false);
  const [errors, setErrors] = useState<any>({});
  const [accountStatus, setAccountStatus] = useState<any>();
  const cookie = new Cookies();
  const id = cookie.get("id");
  const [loading , setLoading] = useState(false);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [openSuccess, closeSueess] = useSnackbar(successOptions);
  useEffect(() => {
    getSitterDetails(id);
  }, []);

  const getSitterDetails = (id) => {
    api
      .getSingleSitter(id)
      .then((res) => {
        setAccountStatus(res.data.response.account_status);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const deactivateAccount = (status) => {
    let data = { status: status == 1 ? 0 : 1 };
    api
      .deactivateAccount(data)
      .then((res) => {
        getSitterDetails(id);
      })
      .catch((error) => {
        setLoading(false)
        console.log(error);
      });
  };
  const onTextChange = (event) => {
    setResetObj({ ...resetObj, [event.target.id]: event.target.value });
    setErrors({ ...errors, [event.target.id]: null });
  };

  const [resetObj, setResetObj] = useState({
    current_password: "",
    new_password: "",
    confirm_password: "",
  });

  const onSubmit = (e) => {
    e.preventDefault();
    if (!resetObj.current_password) {
      setErrors({ ...errors, current_password: strings.enterOldPassword });
    } else if (!resetObj.new_password) {
      setErrors({ ...errors, new_password: strings.enterNewPassword });
    } else if (!resetObj.confirm_password) {
      setErrors({ ...errors, confirm_password: strings.enterConfirmPassword });
    } else if (resetObj.new_password !== resetObj.confirm_password) {
      setErrors({
        ...errors,
        confirm_password: strings.ConfirmPasswordNotMatched,
      });
    } else {
      api
        .changePassword(resetObj)
        .then((res) => {
          if (res.data.status == 200) {
            setErrors({});
            setResetObj({
              current_password: "",
              new_password: "",
              confirm_password: "",
            });
            openSuccess(res.data.message);
          } else {
            openError(res.data.message);
          }
        })
        .catch((error) => {
          setErrors({
            ...errors,
            current_password: error.response.data.message,
          });
          console.log(error.response.data.message);
        });
    }
  };

  const ActiveDectiveRequest = () => {
    confirmAlert({
      closeOnClickOutside: true,
      customUI: ({ onClose }) => {
        return (
          <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
            <div className="react-confirm-alert-body">
              <h1>
                {accountStatus == 1 ? strings.Deactivate : strings.Activate}
              </h1>
              <p>{strings.areYouSure}</p>
              <div className="react-confirm-alert-button-group">
                <button
                  className="btn btn-primary w-50"
                  aria-label="Yes"
                  onClick={() => {
                    setLoading(true);
                    deactivateAccount(accountStatus);
                    onClose();
                  }}>
                  {strings.Yes}
                </button>
                <button
                  className="btn btn-danger w-50"
                  aria-label="No"
                  onClick={() => {
                    onClose();
                  }}>
                  {strings.No}
                </button>
              </div>
            </div>
          </div>
        );
      },
    });
  };
  return (
  <>
  {loading ? <OverlayLoader/> : ""}
    <div className="col-12 col-md-8 col-lg-8 col-xl-8  column1">
      <div className="col-12 content-main px-3 px-md-0 mt-3 mt-md-0">
        <div className="bg-white main-background">
          <div className="password-details">
            <h5 className="font-semibold font-20 mb-3">
              {strings.PasswordAndSecurity}
            </h5>

            <div className="form-row">
              <div className="form-group col-sm-5">
                <input
                  className={`form-control ${
                    errors.current_password ? "invalid" : ""
                  }`}
                  type="password"
                  id="current_password"
                  placeholder={strings.CurrentPassword}
                  onChange={onTextChange}
                  value={resetObj.current_password}
                />
                {errors && errors.current_password ? (
                  <span
                    style={{
                      color: "#ff0000",
                      fontSize: "12px",
                    }}>
                    {" "}
                    {errors.current_password}
                  </span>
                ) : null}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-sm-5">
                <input
                  className={`form-control ${
                    errors.new_password ? "invalid" : ""
                  }`}
                  type="password"
                  id="new_password"
                  placeholder={strings.NewPassword}
                  onChange={onTextChange}
                  value={resetObj.new_password}
                />
                {errors && errors.new_password ? (
                  <span
                    style={{
                      color: "#ff0000",
                      fontSize: "12px",
                    }}>
                    {" "}
                    {errors.new_password}
                  </span>
                ) : null}
              </div>
              <div className="form-group col-sm-5">
                <input
                  className={`form-control ${
                    errors.confirm_password ? "invalid" : ""
                  }`}
                  type="password"
                  id="confirm_password"
                  placeholder={strings.ConfirmPassword}
                  onChange={onTextChange}
                  value={resetObj.confirm_password}
                />
                {errors && errors.confirm_password ? (
                  <span
                    style={{
                      color: "#ff0000",
                      fontSize: "12px",
                    }}>
                    {" "}
                    {errors.confirm_password}
                  </span>
                ) : null}
              </div>
            </div>
            <button onClick={onSubmit} className="btn btn-primary">
              {strings.UpdatePassword}
            </button>
          </div>
        </div>
        <div className="bg-white main-background">
          <div className="row align-items-center">
            <div className="col">
              <div className="deactivemb-0">
              <h5 className="font-semibold mb-1 font-20">{strings.Account}</h5>
                <p className="text-muted mb-0 font-14">
                  {accountStatus == 1 ? strings.Deactivate : strings.Activate}{" "}
                  {strings.myAccount}
                </p>
              </div>
            </div>
            <div className="col-auto alignment">
              <div className="deactive-btn">
                <button
                  className="btn btn-primary btn1 btn-accept btn-reject"
                  onClick={() => {
                    ActiveDectiveRequest();
                  }}>
                  {accountStatus == 1 ? strings.Deactivate : strings.Activate}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
}

PasswordSecurity.getLayout = function (page) {
  return <User>{page}</User>;
};
