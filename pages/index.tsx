import React, {useEffect, useRef, useState} from "react";
import "react-multi-carousel/lib/styles.css";
import "react-day-picker/lib/style.css";
import styles from "./index.module.css";
import { petSpotsSlider, responsive } from "../public/SliderResponsive";
import SitterSliderObject from "../components/homepage/SitterSliderObject";
import Carousel from "react-multi-carousel";
import PetSpotObject from "../components/homepage/PetSpotObject";
import NewsRoomObject from "../components/homepage/NewsRoomObject";
import EventAndNews from "../components/homepage/EventAndNews";
import FAQ from "../components/homepage/FAQ";
import SitterByRegion from "../components/homepage/SitterByRegion";
import BecomePetSitter from "../components/homepage/BecomePetSitter";
import Services from "../components/homepage/Services";
import WhyChoosePetcation from "../components/homepage/WhyChoosePetcation";
import PetcationForOwners from "../components/homepage/PetcationForOwners";
import PetcationForPetsitters from "../components/homepage/PetcationForPetsitters";
import { strings } from "../public/lang/Strings";
import { MainBanner } from "../components/homepage/MainBanner";
import Link from "next/link";
import API from "../api/Api";
import Cookies from "universal-cookie";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Script from 'next/script'
import Head from "next/head";



const cookies = new Cookies();

type select = {
    key: number;
    value: string;
    label: string;
};

type SpotData = {
    id: number,
    user_id: number,
    category_id: number,
    spot_name: string,
    postal_code: number,
    city: string,
    address: string,
    overall_rate: number,
    total_review; number,
    images:any
}

interface IState {
    spotData: SpotData[],
    explorePopularSitters: any;
    gmapsLoaded: boolean;
    newsRoom: any;
}

interface IProps {
    featuredSpots?: any,

}

let api = new API();

class Home extends React.Component<IProps,IState> {
    private carouselSitter: React.RefObject<Carousel>;
    private carouselNewsroom: React.RefObject<Carousel>;
    private carouselSpot: React.RefObject<Carousel>;


    constructor(props) {
        super(props);
        this.state = {
            spotData: [],
            explorePopularSitters: [],
            newsRoom: [],
            gmapsLoaded: false
        }

        this.carouselSitter = React.createRef();
        this.carouselSpot = React.createRef();
        this.carouselNewsroom = React.createRef();

        this.getSpotData = this.getSpotData.bind(this);
        this.getExplorePopularSitters = this.getExplorePopularSitters.bind(this);
        this.getNewsRoom = this.getNewsRoom.bind(this);
    }

    componentDidMount(): void {
        this.getSpotData()
        this.getExplorePopularSitters()
        this.getNewsRoom()
    }

    getExplorePopularSitters(){
        api.getExplorePopularSitters()
            .then((res) => {
                this.setState(
                    {
                        ...this.state,
                        explorePopularSitters:res.data.response
                    }
                )
            })
            .catch((error) => console.log(error))
    }
    getNewsRoom(){
        api.getLandingPageNewsRoom()
            .then((res) => {
                this.setState(
                    {
                        ...this.state,
                        newsRoom:res.data.response?.data
                    }
                )
            })
            .catch((error) => console.log(error))
    }
    getSpotData(){
        api.getPetSpots({})
            .then((res) => {
                this.setState(
                    {
                        ...this.state,
                        spotData:res.data.response
                    }
                )
            })
            .catch((error) => console.log(error));
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<{}>, snapshot?: any): void {
        if(prevProps.featuredSpots != this.props.featuredSpots){
            this.setState({spotData: this.props.featuredSpots})
        }
    }

    scrollToTop = () => {
        if (window) {
            window.scroll({ top: 0, left: 0, behavior: "smooth" });
        }
    };


    render() {
        const {spotData, explorePopularSitters, newsRoom} = this.state;
        let id = cookies.get('id');
        return (
            <div>
                <Head>
                    <meta name="theme-color" content="#FFFFFF"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
                    <meta name="HandheldFriendly" content="true" />
                </Head>
                <div className="main-wrapper my-0">
                    <div className="main-image  d-md-block d-block d-lg-block d-xl-block">
                        <img src="images/banner.png" className="img-fluid" alt="" />
                    </div>
                     <MainBanner />
                    <div>
                        <ToastContainer />
                    </div>
                    {/*<div className="rating-section">*/}
                    {/*    <div className="container">*/}
                    {/*        <div className="row">*/}
                    {/*            <div className="col-auto mx-auto">*/}
                    {/*                <div className="d-flex">*/}
                    {/*                    <div>*/}
                    {/*                        <h6 className="mb-0 text-white">{strings.Excellent} </h6>*/}
                    {/*                    </div>*/}
                    {/*                    <div className="d-flex rating-star">*/}
                    {/*                        <div className="active">*/}
                    {/*                            <svg*/}
                    {/*                                aria-hidden="true"*/}
                    {/*                                focusable="false"*/}
                    {/*                                data-prefix="fas"*/}
                    {/*                                data-icon="star"*/}
                    {/*                                role="img"*/}
                    {/*                                xmlns="http://www.w3.org/2000/svg"*/}
                    {/*                                viewBox="0 0 576 512"*/}
                    {/*                                className="svg-inline--fa fa-star fa-w-18 fa-2x"*/}
                    {/*                            >*/}
                    {/*                                <path*/}
                    {/*                                    fill="currentColor"*/}
                    {/*                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"*/}
                    {/*                                ></path>*/}
                    {/*                            </svg>*/}
                    {/*                        </div>*/}
                    {/*                        <div className="active">*/}
                    {/*                            <svg*/}
                    {/*                                aria-hidden="true"*/}
                    {/*                                focusable="false"*/}
                    {/*                                data-prefix="fas"*/}
                    {/*                                data-icon="star"*/}
                    {/*                                role="img"*/}
                    {/*                                xmlns="http://www.w3.org/2000/svg"*/}
                    {/*                                viewBox="0 0 576 512"*/}
                    {/*                                className="svg-inline--fa fa-star fa-w-18 fa-2x"*/}
                    {/*                            >*/}
                    {/*                                <path*/}
                    {/*                                    fill="currentColor"*/}
                    {/*                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"*/}
                    {/*                                ></path>*/}
                    {/*                            </svg>*/}
                    {/*                        </div>*/}
                    {/*                        <div className="active">*/}
                    {/*                            <svg*/}
                    {/*                                aria-hidden="true"*/}
                    {/*                                focusable="false"*/}
                    {/*                                data-prefix="fas"*/}
                    {/*                                data-icon="star"*/}
                    {/*                                role="img"*/}
                    {/*                                xmlns="http://www.w3.org/2000/svg"*/}
                    {/*                                viewBox="0 0 576 512"*/}
                    {/*                                className="svg-inline--fa fa-star fa-w-18 fa-2x"*/}
                    {/*                            >*/}
                    {/*                                <path*/}
                    {/*                                    fill="currentColor"*/}
                    {/*                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"*/}
                    {/*                                ></path>*/}
                    {/*                            </svg>*/}
                    {/*                        </div>*/}
                    {/*                        <div className="active">*/}
                    {/*                            <svg*/}
                    {/*                                aria-hidden="true"*/}
                    {/*                                focusable="false"*/}
                    {/*                                data-prefix="fas"*/}
                    {/*                                data-icon="star"*/}
                    {/*                                role="img"*/}
                    {/*                                xmlns="http://www.w3.org/2000/svg"*/}
                    {/*                                viewBox="0 0 576 512"*/}
                    {/*                                className="svg-inline--fa fa-star fa-w-18 fa-2x"*/}
                    {/*                            >*/}
                    {/*                                <path*/}
                    {/*                                    fill="currentColor"*/}
                    {/*                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"*/}
                    {/*                                ></path>*/}
                    {/*                            </svg>*/}
                    {/*                        </div>*/}
                    {/*                        <div className="active">*/}
                    {/*                            <svg*/}
                    {/*                                aria-hidden="true"*/}
                    {/*                                focusable="false"*/}
                    {/*                                data-prefix="fas"*/}
                    {/*                                data-icon="star"*/}
                    {/*                                role="img"*/}
                    {/*                                xmlns="http://www.w3.org/2000/svg"*/}
                    {/*                                viewBox="0 0 576 512"*/}
                    {/*                                className="svg-inline--fa fa-star fa-w-18 fa-2x"*/}
                    {/*                            >*/}
                    {/*                                <path*/}
                    {/*                                    fill="currentColor"*/}
                    {/*                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"*/}
                    {/*                                ></path>*/}
                    {/*                            </svg>*/}
                    {/*                        </div>*/}
                    {/*                    </div>*/}
                    {/*                    <div>*/}
                    {/*                        <h6 className="mb-0 text-white">*/}
                    {/*                            {strings.basedOn + ' ' + "9745" + " " + strings.reviews}*/}
                    {/*                        </h6>*/}
                    {/*                    </div>*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*</div>*/}

                    <Services />
                    <WhyChoosePetcation />
                    <PetcationForOwners />
                    <div className="bg-white padding">
                        <div className="container">
                            <div className="bg-white main-section bg-transparent shadow-none p-0">
                                <div className="col-12 px-2">
                                    <div className="row">
                                        <div className="col-12 ">
                                            <div className="main-title feature-title">
                                                <h2 className="font-semibold d-none d-md-block">{strings.exploreOurSitter}</h2>
                                                <h2 className="font-semibold d-block mb-0 d-md-none">{strings.ourSitter}</h2>
                                                {/*<p className="mb-0">{strings.Youcanfindspotsinplaces}</p>*/}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 home-slider-view">
                                    <div className="row carousel-slider">
                                        <div className="col-12 col-md-12 col-lg-12 col-xl-12 p-0">
                                            <div
                                                className="owl-carousel owl-loaded owl-drag"
                                                id="featured-slider"
                                            >
                                                <Carousel
                                                    ref={this.carouselSitter}
                                                    swipeable={true}
                                                    draggable={true}
                                                    autoPlay={false}
                                                    showDots={true}
                                                    ssr={true}
                                                    renderDotsOutside={true}
                                                    arrows={true}
                                                    dotListClass={styles.carousel_dot}
                                                    className={"owl-carousel"}
                                                    responsive={responsive}
                                                >
                                                    {explorePopularSitters.map((sitter, index) =>
                                                        <SitterSliderObject
                                                            key={`sitter-slider${index}`}
                                                            id={sitter.id}
                                                            name={sitter.firstname + " " + sitter.lastname}
                                                            city={"tokyo"}
                                                            rating={sitter.overall_rate}
                                                            image={sitter.profile_picture}
                                                        />
                                                    )}
                                                </Carousel>
                                                <div className="owl-nav right-0 disabled align-items-center">
                                                    <Link href="/search-sitter">
                                                        <a className="font-14 font-medium mr-2">{strings.ViewAll}</a>
                                                    </Link>
                                                    <div className="owl-prev">
                                                        <div className="prev">
                                                            <svg
                                                                onClick={() => this.carouselSitter.current.previous(null)}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-left"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-left fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zM256 472c-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216zm-12.5-92.5l-115.1-115c-4.7-4.7-4.7-12.3 0-17l115.1-115c4.7-4.7 12.3-4.7 17 0l6.9 6.9c4.7 4.7 4.7 12.5-.2 17.1L181.7 239H372c6.6 0 12 5.4 12 12v10c0 6.6-5.4 12-12 12H181.7l85.6 82.5c4.8 4.7 4.9 12.4.2 17.1l-6.9 6.9c-4.8 4.7-12.4 4.7-17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div className="owl-next">
                                                        <div className="next">
                                                            <svg
                                                                onClick={() => this.carouselSitter.current.next(null)}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-right"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-right fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zM256 40c118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216zm12.5 92.5l115.1 115c4.7 4.7 4.7 12.3 0 17l-115.1 115c-4.7 4.7-12.3 4.7-17 0l-6.9-6.9c-4.7-4.7-4.7-12.5.2-17.1l85.6-82.5H140c-6.6 0-12-5.4-12-12v-10c0-6.6 5.4-12 12-12h190.3l-85.6-82.5c-4.8-4.7-4.9-12.4-.2-17.1l6.9-6.9c4.8-4.7 12.4-4.7 17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="owl-dots disabled" />
                                            </div>
                                            <div className="slider_nav disabled"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <PetcationForPetsitters />
                    {spotData &&

                    <div className="bg-white padding">
                        <div className="container">
                            <div className="bg-white main-section bg-transparent shadow-none p-0">
                                <div className="col-12 px-2">
                                    <div className="row">
                                        <div className="col-12 ">
                                            <div className="main-title feature-title">
                                                <h2 className="font-semibold">{strings.petSpots}</h2>
                                                <p className="mb-0 d-none d-md-block">{strings.Exploreamazingplaces}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 home-slider-view">
                                    <div className="row carousel-slider">
                                        <div className="col-12 col-md-12 col-lg-12 col-xl-12 p-0">
                                            <div
                                                className="owl-carousel owl-loaded owl-drag"
                                                id="featured-slider"
                                            >
                                                <Carousel
                                                    ref={this.carouselSpot}
                                                    swipeable={true}
                                                    draggable={true}
                                                    autoPlay={false}
                                                    showDots={true}
                                                    ssr={true}
                                                    arrows={true}
                                                    renderDotsOutside={true}
                                                    dotListClass={styles.carousel_dot}
                                                    // className={"owl-carousel"}
                                                    responsive={petSpotsSlider}
                                                >
                                                    {spotData.map((item, index)=>(<PetSpotObject key={`spots_${index}`}
                                                                                                 name={item.spot_name}
                                                                                                 id={item.id}
                                                                                                 image={item.images && item.images.length ? item.images[0].path:''}
                                                                                                 descriptiom={item.city + ' | ' + item.address}
                                                    />))}
                                                </Carousel>
                                                <div className="owl-nav right-0 disabled align-items-center">
                                                    <Link href="/pet-spots">
                                                        <a className="font-14 font-medium mr-2">{strings.ViewAll}</a>
                                                    </Link>
                                                    <div className="owl-prev">
                                                        <div className="prev">
                                                            <svg
                                                                onClick={() => this.carouselSpot.current.previous(null)}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-left"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-left fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zM256 472c-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216zm-12.5-92.5l-115.1-115c-4.7-4.7-4.7-12.3 0-17l115.1-115c4.7-4.7 12.3-4.7 17 0l6.9 6.9c4.7 4.7 4.7 12.5-.2 17.1L181.7 239H372c6.6 0 12 5.4 12 12v10c0 6.6-5.4 12-12 12H181.7l85.6 82.5c4.8 4.7 4.9 12.4.2 17.1l-6.9 6.9c-4.8 4.7-12.4 4.7-17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div className="owl-next">
                                                        <div className="next">
                                                            <svg
                                                                onClick={() => this.carouselSpot.current.next(null)}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-right"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-right fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zM256 40c118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216zm12.5 92.5l115.1 115c4.7 4.7 4.7 12.3 0 17l-115.1 115c-4.7 4.7-12.3 4.7-17 0l-6.9-6.9c-4.7-4.7-4.7-12.5.2-17.1l85.6-82.5H140c-6.6 0-12-5.4-12-12v-10c0-6.6 5.4-12 12-12h190.3l-85.6-82.5c-4.8-4.7-4.9-12.4-.2-17.1l6.9-6.9c4.8-4.7 12.4-4.7 17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="owl-dots disabled" />
                                            </div>
                                            <div className="slider_nav disabled"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                    <BecomePetSitter id={id} />
                    <div className="bg-white padding">
                        <div className="container">
                            <div className="bg-white main-section bg-transparent shadow-none p-0">
                                <div className="col-12 px-2">
                                    <div className="row">
                                        <div className="col-12 ">
                                            <div className="main-title feature-title">
                                                <h2 className="font-semibold">{strings.newsroom}</h2>
                                                {/*<p className="mb-0">{strings.Youcanfindspotsinplaces}</p>*/}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 home-slider-view">
                                    <div className="row carousel-slider">
                                        <div className="col-12 col-md-12 col-lg-12 col-xl-12 p-0">
                                            <div
                                                className="owl-carousel owl-loaded owl-drag"
                                                id="featured-slider"
                                            >
                                                <Carousel
                                                    ref={this.carouselNewsroom}
                                                    swipeable={true}
                                                    draggable={true}
                                                    autoPlay={false}
                                                    ssr={true}
                                                    arrows={true}
                                                    renderDotsOutside={true}
                                                    dotListClass={styles.carousel_dot}
                                                    // className={"owl-carousel"}
                                                    responsive={petSpotsSlider}
                                                >
                                                    {newsRoom && newsRoom.map((data, index) => <NewsRoomObject
                                                        data={data}
                                                        key={index}
                                                    />)}
                                                </Carousel>
                                                <div className="owl-nav right-0 disabled align-items-center">
                                                    <Link href={{pathname: '/static/blog'}}>
                                                        <a className="font-14 font-medium mr-2">{strings.ViewAll}</a>
                                                    </Link>
                                                    <div className="owl-prev">
                                                        <div className="prev">
                                                            <svg
                                                                onClick={() => this.carouselNewsroom.current.previous(null)}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-left"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-left fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zM256 472c-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216zm-12.5-92.5l-115.1-115c-4.7-4.7-4.7-12.3 0-17l115.1-115c4.7-4.7 12.3-4.7 17 0l6.9 6.9c4.7 4.7 4.7 12.5-.2 17.1L181.7 239H372c6.6 0 12 5.4 12 12v10c0 6.6-5.4 12-12 12H181.7l85.6 82.5c4.8 4.7 4.9 12.4.2 17.1l-6.9 6.9c-4.8 4.7-12.4 4.7-17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div className="owl-next">
                                                        <div className="next">
                                                            <svg
                                                                onClick={() => this.carouselNewsroom.current.next(null)}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-right"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-right fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zM256 40c118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216zm12.5 92.5l115.1 115c4.7 4.7 4.7 12.3 0 17l-115.1 115c-4.7 4.7-12.3 4.7-17 0l-6.9-6.9c-4.7-4.7-4.7-12.5.2-17.1l85.6-82.5H140c-6.6 0-12-5.4-12-12v-10c0-6.6 5.4-12 12-12h190.3l-85.6-82.5c-4.8-4.7-4.9-12.4-.2-17.1l6.9-6.9c4.8-4.7 12.4-4.7 17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="owl-dots disabled" />
                                            </div>
                                            <div className="slider_nav disabled"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="booking-sitter">
                        <div className="padding my-md-4 mt-4">
                            <div className="container">
                                <div className="row">
                                    <EventAndNews />
                                    <FAQ />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bg-white padding">
                        <div className="container">
                            <SitterByRegion />
                        </div>
                    </div>
                </div>
                <div className="col-12 text-right back-to-top-btn">
                    <div onClick={this.scrollToTop} className="btn-top top-scroll">
                        <div className="top-icon"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
