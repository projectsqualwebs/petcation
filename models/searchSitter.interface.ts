export interface I_SEARCH_SITTER {
  address: Address;
  firstname: string;
  id: number;
  lastname: string;
  price: number;
  duration?: number;
  grooming?: number;
  medical?: number;
  size: number;
  profile_picture: string;
  overall_rate: number;
  total_review: number;
  is_favorite: boolean;
  experience_in_month: number;
  experience_in_year: number;
  label: string;
  distance : number;
  single_review:Singlereview;
  repeat_client:number;

}

type Address = {
  address: string;
  city: { id: number; name: string };
  city_id: 7;
  description: string;
  dog_other_pets: number;
  fenced_yard: number;
  hide_address: number;
  house_number: string;
  id: number;
  live_in_house: number;
  no_children_present: number;
  non_smoking_household: number;
  postcode: string;
  updated_at: string;
  map_latitude:string;
  map_longitude:string;
  latitude: string;
  longitude: string;
  prefecture:string;
};

type Singlereview= {
  id:number;
  review:string;
  sitter_id:number;
  user_id:number;
  user_info:User_info;
}

type User_info = {
  firstname:string;
  id:number;
  lastname:string;
  profile_picture:string;

}