export default interface I_SITTER_RESERVATION {
    updated_at: any;
    created_at: any;
    dispute: any;
  id: number;
  booking_user_id: number;
  booked_sitter_id: number;
  drop_of_date: string;
  drop_of_time_from: string;
  pickup_up_date: string;
  pickup_up_time_to: string;
  total_paid_amount: string;
  refund_amount: string;
  need_sitter_pickup?: any;
  amend_amount: string;
  payment_status: number;
  cancellation_policy:number;
  status: number;
  pets?: {
    age_month: number;
    age_year: number;
    amount: string;
    breed: {
      id: number;
      pet_type: number;
      breed: string;
      created_at: string;
      updated_at: string;
    }
    breed_id: number;
    created_at: string;
    id: number;
    pet_booking_id: number;
    pet_image: string;
    pet_name: string;
    sex: number;
    status: number;
    updated_at: string;
    weight: number;
  }[];
  sitter?: {
    id: number;
    firstname: string;
    lastname: string;
    profile_picture: string;
    address: {
      id: number;
      house_number: string;
      address: string;
      city_id: number;
      postcode: string;
      city: {
        id: number;
        name: string;
      };
    };
  };
  user?: {
    id: number;
    firstname: string;
    lastname: string;
    profile_picture: string;
    address: {
      id: number;
      house_number: string;
      address: string;
      city_id: number;
      postcode: string;
      city: {
        id: number;
        name: string;
      };
    };
  };
  service: {
    id: number;
    name: string;
  };
  owner_reviews: any;
  sitter_reviews: any;
  reason: string;
}
