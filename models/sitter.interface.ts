import SitterProfileHeader from "../components/sitterProfile/SitterProfileHeader";
import React from "react";
import {bool, number} from "prop-types";
import {booleanLiteral} from "@babel/types";

export interface I_SINGLE_SITTER {
  id: number;
  firstname: string;
  total_review: number;
  overall_rate: number;
  description: string;
  lastname: string;
  is_favorite:number;
  profile_picture: string;
  is_verified: number;
  cancellation_policy: number;
  experience_in_month: number;
  experience_in_year: number;
  isSitter:boolean;
  serviceId:string;
  preference: {
    online: boolean;
    pets_care: number;
    happy_customers: number;
    rehire_rate: number;
    response_within: string;
    job_completion: number;
    response_rate: number;

  };
  pets: {
    id: number;
    pet_type: {
      id: number;
      name: string;
    };
    user_id: number;
    breed_id: number;
    pet_image: string;
    pet_name: string;
    weight: number;
    age_year: number;
    age_month: number;
    sex: number;
    created_at: string;
    updated_at: string;
    breed: {
      id: number;
      breed: string;
    };
  }[];
  address: {
    id: number;
    house_number: string;
    address: string;
    longitude: string;
    latitude: string;
    city: string;
    postcode: string;
    description: string;
    hide_address: number;
    live_in_house: number;
    non_smoking_household: number;
    no_children_present: number;
    fenced_yard: number;
    dog_other_pets: number;
    updated_at: string;
    is_address_completed: number;
    map_latitude: string;
    map_longitude: string;
    distance: string;
    prefecture:string;
  };
  skills: {
    id: number;
    skill: string;
  }[];
  avg_rating: AVG_RATING;
  reviews: MY_REVIEWS[];
  owner_reviews: any;
  questions: {
    id: number;
    question: string;
  }[];
  images: string[];
  active_services: {
    service: {
      id: number;
      name: string;
    };
  }[];
  user_document: string;
}

 export interface AVG_RATING {
  cleanliness:  string;
  communication: string;
  check_in: string;
  accuracy: string;
  location: string;
  value: string;
}

export interface MY_REVIEWS {
  id: number;
  user_id: number;
  sitter_id: number;
  cleanliness: number;
  average_rating:number;
  communication: number;
  check_in: number;
  accuracy: number;
  location: number;
  value: number;
  review: string;
  created_at: string;
  comment: string;
  user_info: {
    id: number;
    firstname: string;
    lastname: string;
    profile_picture: string;
  }
}
export interface OWNER_REVIEWS {
  id: number;
  user_id: number;
  sitter_id: number;
  cleanliness: number;
  communication: number;
  check_in: number;
  accuracy: number;
  location: number;
  rating: number;
  review: string;
  created_at: string;
  comment: string;
  sitter_info: {
    id: number;
    firstname: string;
    lastname: string;
    profile_picture: string;
  }
}
// services interface
export interface I_CUSTOM_SERVICES {
  description: string;
  id: number;
  name: string;
  price: number;
}
export interface I_SERVICE_PET {
  custom_services: I_CUSTOM_SERVICES[];
  id: number;
  pet_type: number;
}

export interface I_BOARDING_SERVICE_FEE {
  cancellation_policy: number;
  holiday_extra_charges: number;
  id: number;
  sitter_boarding_service_id: number;
  service_pets: I_SERVICE_PET[];
}
export interface I_DAY_CARE_SERVICE_FEE {
  cancellation_policy: number;
  holiday_extra_charges: number;
  id: number;
  sitter_pet_day_care_services_id: number;
  service_pets: I_SERVICE_PET[];
}
export interface I_HOUSE_SITTING_SERVICE_FEE {
  cancellation_policy: number;
  holiday_extra_charges: number;
  id: number;
  sitter_house_sitting_service_id: number;
  service_pets: I_SERVICE_PET[];
}
export interface I_PET_WALKING_SERVICE_FEE {
  cancellation_policy: number;
  holiday_extra_charges: number;
  id: number;
  sitter_pet_walking_services_id: number;
  service_pets: I_SERVICE_PET[];
}
export interface I_DROP_IN_VISIT_SERVICE_FEE {
  cancellation_policy: number;
  holiday_extra_charges: number;
  id: number;
  sitter_drop_in_visits_services_id: number;
  service_pets: I_SERVICE_PET[];
}
export interface I_HOUSE_CALL_SERVICE_FEE {
  cancellation_policy: number;
  holiday_extra_charges: number;
  id: number;
  sitter_house_call_services_id: number;
  service_pets: I_SERVICE_PET[];
}
export interface I_GROOMING_SERVICE_FEE {
  cancellation_policy: number;
  holiday_extra_charges: number;
  id: number;
  sitter_grooming_services_id: number;
  service_pets: I_SERVICE_PET[];
}
export interface I_BOARDING_SERVICE {
  boarding_service_fee: I_BOARDING_SERVICE_FEE;
  created_at: string;
  id: number;
  sitter_service_id: number;
  updated_at: string;
}
export interface I_DAY_CARE_SERVICE {
  day_care_service_fee: I_DAY_CARE_SERVICE_FEE;
  created_at: string;
  id: number;
  sitter_service_id: number;
  updated_at: string;
}
export interface I_DROP_IN_VISIT_SERVICE {
  drop_in_visit_service_fee: I_DROP_IN_VISIT_SERVICE_FEE;
  created_at: string;
  id: number;
  sitter_service_id: number;
  updated_at: string;
}
export interface I_GROOMING_SERVICE {
  grooming_service_fee: I_GROOMING_SERVICE_FEE;
  created_at: string;
  id: number;
  sitter_service_id: number;
  updated_at: string;
}
export interface I_HOUSE_CALL_SERVICE {
  house_call_service_fee: I_HOUSE_CALL_SERVICE_FEE;
  created_at: string;
  id: number;
  sitter_service_id: number;
  updated_at: string;
}
export interface I_HOUSE_SIITING_SERVICE {
  house_sitting_service_fee: I_HOUSE_SITTING_SERVICE_FEE;
  created_at: string;
  id: number;
  sitter_service_id: number;
  updated_at: string;
}
export interface I_PET_WALKING_SERVICE {
  pet_walking_service_fee: I_PET_WALKING_SERVICE_FEE;
  created_at: string;
  id: number;
  sitter_service_id: number;
  updated_at: string;
}
export interface I_ACTIVE_SERVICES {
  boarding_service: I_BOARDING_SERVICE;
  day_care_service: I_DAY_CARE_SERVICE;
  drop_in_visit_service: I_DROP_IN_VISIT_SERVICE;
  grooming_service: I_GROOMING_SERVICE;
  house_call_service: I_HOUSE_CALL_SERVICE;
  house_sitting_service: I_HOUSE_SIITING_SERVICE;
  pet_walking_service: I_PET_WALKING_SERVICE;
  service: {
    id: number;
  name: string;
  };

  map(val: (val, index) => any): I_ACTIVE_SERVICES;
}
// services interface end----
export interface I_ADDRESS {
  accept_only_on_client_at_time: number;
  address: string;
  city: string;
  description: string;
  distance: string;
  does_not_own_a_cat: number;
  does_not_own_a_dog: number;
  does_not_own_caged_pets: number;
  dog_other_pets: number;
  dogs_on_bed: number;
  dogs_on_furniture: number;
  fenced_yard: number;
  hide_address: number;
  house_number: string;
  is_address_completed: number;
  latitude: string;
  live_in_house: number;
  longitude: string;
  map_latitude: string;
  map_longitude: string;
  no_children_5_year_old: number;
  no_children_12_year_old: number;
  no_children_present: number;
  non_smoking_household: number;
  postcode: string;
  updated_at: string;
}
export interface I_PETS {
  age_month: number;
  age_year: number;
  breed: {
    id: number;
    breed: string;
  }
  breed_id: number;
  created_at: string;
  id: number;
  pet_image: string;
  pet_name: string;
  pet_type: {
    id: number;
    name: string;
  }
  sex: number;
  updated_at: string;
  user_id: number;
  weight: number;
}
export interface I_PREFERENCE {
  happy_customers: number;
  job_completion: number;
  online: boolean;
  pets_care: number;
  response_rate: number;
  response_within: string;
}
export interface I_AVAILABILITY {
  active_services: I_ACTIVE_SERVICES[];
  address: I_ADDRESS;
  avg_rating: AVG_RATING;
  description: string;
  experience_in_month: string;
  experience_in_year: string;
  firstname: string;
  id: number;
  images: {
    id: number;
    path: string;
    user_id: number;
  }[];
  is_favorite: boolean;
  is_verified: number;
  lastname: string;
  overall_rate: number;
  pets: I_PETS[];
  preference: I_PREFERENCE;
  profile_picture: string;
  questions: {
    id: number;
    question: string;
  }[];
  reviews: MY_REVIEWS[];
  skills: {
    id: number;
    skill: string;
  }[];
  total_review: number;
  user_document: any;
}