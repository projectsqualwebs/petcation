import I_PET from "./pet.interface";
interface I_Languages {
    language_id: number;
}
interface I_SERVICES {
    service_id: number;
}
interface I_IMAGES {
    path: string;
}
interface I_OPERATIONS {

}
export default interface SingleSpotHeaderInterface {
    address: string;
    amenities: {
        facebook_url: string;
        homepage: string;
        id: number;
        instagram_url: string;
        language_info: string;
        languages: I_Languages[];
        parking_id: number;
        parking_info: string;
        remark: string;
        reservation_info: string;
        reservation_type_id: number;
        service_amenities_info: string;
        services: I_SERVICES[]
        smoking_cessation_id: number;
        smoking_cessation_info: string;
        twitter_url: string;
        user_spot_id: number;
    };
    budgets: {
        id: number;
        night_charge_id: number;
        noon_charge_id: number;
        payment_method_id: number;
        service_charge: number;
        user_spot_id: number;
    };
    category_id: number;
    city: string;
    created_at: string;
    english_name: string;
    id: number;
    images: I_IMAGES[]
    latitude: string;
    longitude: string;
    operations: {
        business_hours: string;
        id: number;
        is_open_on_sunday: number;
        means_of_transport: string;
        regular_holidays: string;
        user_spot_id: number;
    }
    overall_rate: number;
    pets: I_PET[]
    phone_number: string;
    postal_code: string;
    review_rating: {
        id: number;
        rating: number;
        review: string;
        spot_id: number;
        user_id: number;
    }
    spot_description: string;
    spot_name: string;
    total_review: number;
    is_favorite: boolean;
}
