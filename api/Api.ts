import axios from "axios";
import {
    U_ACTIVE_BOARDING,
    U_ACTIVE_DAY_CARE,
    U_ACTIVE_DROP_IN_VISITS,
    U_ACTIVE_GROOMING,
    U_ACTIVE_HOUSE_CALL,
    U_ACTIVE_HOUSE_SITTING,
    U_ACTIVE_PET_WALKING,
    U_ADD_PET,
    U_ADD_QUESTION,
    U_ADD_SKILL,
    U_ADD_SPOT,
    U_ARRANG_MEETUP,
    U_BOARDING_DISCOUNTS,
    U_BOARDING_PET_SERVICE_FEE,
    U_BOARDING_PREFERENCE,
    U_CHANGE_REQUEST_STATUS,
    U_DELETE_PET,
    U_DELETE_QUESTION,
    U_DELETE_SPOT,
    U_DOG_WALKING_DISCOUNT,
    U_DOG_WALKING_INFO,
    U_DOG_WALKING_SERVICE_FEE,
    U_DOG_WALKING_SERVICE_PREFERENCE,
    U_DROP_IN_VISITES_PREFERENCE,
    U_DROP_IN_VISITS_DISCOUNTS,
    U_DROP_IN_VISITS_SERVICE_FEE,
    U_FILTERED_AVAILABLE_SITTER,
    U_GET_ADDRESS,
    U_GET_ALL_PETS,
    U_GET_ALL_SPOT,
    U_GET_AVAILABILITY,
    U_GET_AVAILABILITY_BY_DATES,
    U_GET_AVAILABLE_SERVICES,
    U_GET_BASIC_INFO,
    U_GET_BOARDING_INFO,
    U_GET_BOARDING_PREVIEW,
    U_GET_BREED_WITH_TYPE,
    U_GET_CITIES,
    U_GET_COUTRIES,
    U_GET_DASHBOARD,
    U_GET_DOG_WALKING_PREVIEW,
    U_GET_DROP_IN_PREVIEW,
    U_GET_DROP_IN_VISITS_INFO,
    U_GET_GROOMING_INFO,
    U_GET_GROOMING_PREVIEW,
    U_GET_HOUSE_CALL_INFO,
    U_GET_HOUSE_CALL_PREVIEW,
    U_GET_HOUSE_CALL_SERVICE,
    U_GET_HOUSE_SITTING_INFO,
    U_GET_HOUSE_SITTING_PREVIEW,
    U_GET_LANGAUGES,
    U_GET_PARKING,
    U_GET_PAYMENT_METHODS,
    U_GET_PETS,
    U_GET_PET_DAY_CARE_PREVIEW,
    U_GET_PET_SERVICE,
    U_GET_PET_SPOTS,
    U_GET_QUESTIONS,
    U_GET_RESERVATION_TYPES,
    U_GET_SERVICES,
    U_GET_SINGLE_SITTER,
    U_GET_SINGLE_SPOT,
    U_GET_SINGLE_CATEGORY,
    U_GET_SITTER_AVAILABILITY,
    U_GET_SITTER_RESERVATIONS,
    U_GET_SKILLS,
    U_GET_SMOKING_CESSIONS,
    U_GET_THREAD_BOOKING,
    U_GET_USER_RESERVATIONS,
    U_GET_USER_SKILLS,
    U_GROOMING_SERVICE_DISCOUNT,
    U_GROOMING_SERVICE_FEE,
    U_GROOMING_SERVICE_PREFERENCE,
    U_HOUSE_CALL_DISCOUNT,
    U_HOUSE_CALL_PREFERENCE,
    U_HOUSE_CALL_SERVICE_FEE,
    U_HOUSE_SITTING_DISCOUNTS,
    U_HOUSE_SITTING_PREFERENCE,
    U_HOUSE_SITTING_SERVICE_FEE,
    U_IMAGE_UPLOAD,
    U_PET_ADDITIONAL_REQUEST,
    U_PET_AMOUNT_CALCULATION,
    U_PET_DAY_CARE_DISCOUNT,
    U_PET_DAY_CARE_INFO,
    U_PET_DAY_CARE_PREFERENCE,
    U_PET_DAY_CARE_SERVICE_FEES,
    U_PET_SERVICE_AVAIALABILITY,
    U_PET_SPOT_CATEGORIES,
    U_RATE_SITTER,
    U_REQUEST_SITTER,
    U_SAVE_ADDRESS,
    U_SAVE_BASIC_INFO,
    U_SAVE_LOCALITY,
    U_SAVE_PORTFOLIO,
    U_SEND_MESSAGE,
    U_SET_AVAILABLE_SERVICES,
    U_SINGLE_PET,
    U_UPDATE_PET,
    U_UPDATE_SPOT,
    U_UPLOAD_PROFILE_PICTURE,
    U_USER_LOGIN,
    U_USER_REGISTER,
    U_ADD_CARD,
    U_GET_CARD,
    U_DELETE_CARD,
    U_ADD_BANK_ACCOUNT,
    U_GET_BANK_ACCOUNT,
    U_GET_BANK_DETAILS,
    U_DELETE_BANK_ACCOUNT,
    U_GET_PET_SPOT_DETAILS,
    GOOGLE_PLACES_BASE_URL,
    GOOGLE_PLACES_API,
    U_UPLOAD_DOCUMENTS,
    U_GET_TRANSPORT_CHARGES,
    U_ADD_TRANSPORT_CHARGES,
    U_UPDATE_REQUEST_STATUS,
    U_BOOKMARK_SITTER,
    U_GET_REQUEST_DETAIL,
    U_CONFIRM_PAYMENT,
    U_PAYMENT_HISTORY,
    // U_CANCEL_BOOKING,
    U_GET_FAVORITE_SITTER,
    U_CHANGE_MEETUP_STATUS,
    U_GET_MEETUPS,
    U_CHAT_ACTION,
    U_GET_FAVORITE_SPOT,
    U_MARK_UNMARK_SPOT,
    U_MAKE_CARD__DEFAULT,
    U_MAKE_BANK_DEFAULT,
    U_RESET_PASSWORD,
    U_CHANGE_PASSWORD,
    U_FORGOT_PASSWORD,
    U_APPLY_COUPON,
    U_REMOVE_COUPON,
    U_GET_COUPON,
    U_READ_STATUS,
    U_REVIEW_SPOT,
    U_GET_REVIEW_SPOT,
    U_GET_NEWS,
    U_GET_EVENT,
    U_GET_SINGLE_NEWS,
    U_GET_ALL_FAQ,
    U_GET_REPORT_SITTER,
    U_GET_EARNED_AMOUNT,
    U_GET_LANDING_PAGE_FAQ,
    U_GET_SINGLE_FAQ,
    U_GET_SUB_CATEGORY_FAQ,
    U_GET_RELATED_FAQ,
    U_GET_RECENT_VIEWED_FAQ,
    U_GET_CANCEL_REQUEST,
    U_VERIFY_EMAIL,
    U_VERIFY_MOBILE,
    U_GET_EXPLORE_SITTER,
    U_IMAGE_MULTI_UPLOAD,
    U_GET_ALL_LANDING_PAGE_NEWS,
    U_GET_ALL_LANDING_NewRoom,
    U_GET_SINGLE_PAGE_NEWSROOM,
    U_SITTER_REVIEW_COMMENT,
    U_GET_SITTER_REVIEWS,
    U_DEACTIVATE_ACCOUNT,
    U_GET_REPORTED_SITTER,
    U_RATE_OWNER,
    U_SOCIAL_LOGIN,
    U_SOCIAL_REGISTER,
    U_CREATE_WISHLIST,
    U_GET_ALL_SPOT_LIST,
    U_GET_SINGLE_LIST,
    MARK_SPOT_AS_FAVOURITE,
    U_DELETE_WISHLIST,
    U_UPDATE_WISHLIST,
    U_UPDATE_SPOT_NOTE,
    U_GET_FEATURED_PET_SPOTS,
    U_GET_ADD_COUPON,
    U_RESEND_OTP,
    U_RESEND_MOBILE_OTP,
    U_REMOVE_PORTFOLIO_IMAGE,
    U_CONTACT_US_SUMBIT,
    U_CAREER_FORM_SUBMIT,
    U_CREATE_SITTER_WISHLIST,
    U_GET_ALL_SITTER_LIST,
    U_GET_SINGLE_SITTER_LIST,
    U_DELETE_SITTER_WISHLIST,
    U_UPDATE_SITTER_WISHLIST,
    MARK_SITTER_AS_FAVOURITE,
    U_GET_DELETE_SPOT_REVIEW,
    U_GET_SINGLE_SPOT_REVIEW,
    CMS_ABOUT_US,
    CMS_PRIVACY_POLICY,
    CMS_TERMS,
    CMS_COOKIE_POLICY,
    U_DELETE_BOARDING_PET_SERVICE_FEE,
    U_EMAIL_SUBCRIPTION,
    U_DISPUTE_BOOKING,
    U_GET_DISPUTE_BOOKING,
    U_GET_CAREER_PAGE,
    U_BOOKING_ADDITIONAL_INFO,
    U_ADD_BOOKING_NOTES,
    VERIFY_RECAPTCHA
} from "./Constants";
import Cookies from "universal-cookie";
import nextCookie from "next-cookies";

const baseURL = process.env.NEXT_PUBLIC_U_BASE_URL;

const headers = (token) => {
    const headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
    };
    return headers;
};

const request = async (method, path, body = null, ctx = null) => {
    const cookies = new Cookies();
    let token = await cookies.get("token");
    if (token == null && ctx) {
        token = nextCookie(ctx);
    }
    const url = `${baseURL}${path}`;
    var options = {method, url, headers: await headers(token)};

    if (body) {
        options = Object.assign(options, {data: body});
    }

    return axios(options);
};

export default class API {
    registerUser(data) {
        return request("POST", U_USER_REGISTER, data);
    }

    loginUser(data) {
        return request("POST", U_USER_LOGIN, data);
    }

    // social login and register
    socialLogin(data) {
        return request("POST", U_SOCIAL_LOGIN, data);
    }

    socialRegister(data) {
        return request("POST", U_SOCIAL_REGISTER, data);
    }

    resetPassword(data) {
        return request("POST", U_RESET_PASSWORD, data);
    }

    verifyEmail(data) {
        return request("POST", U_VERIFY_EMAIL, data);
    }

    verifyMobile(data) {
        return request("POST", U_VERIFY_MOBILE, data);
    }

    changePassword(data) {
        return request("POST", U_CHANGE_PASSWORD, data);
    }

    forgotPassword(data) {
        return request("POST", U_FORGOT_PASSWORD, data);
    }

    deactivateAccount(data) {
        return request("POST", U_DEACTIVATE_ACCOUNT, data);
    }

    getDashboard(data) {
        return request("POST", U_GET_DASHBOARD, data);
    }

    getEarnedAmount() {
        return request("GET", U_GET_EARNED_AMOUNT);
    }

    getBasicInfo() {
        return request("GET", U_GET_BASIC_INFO);
    }

    getAddress() {
        return request("GET", U_GET_ADDRESS);
    }

    saveBasicInfo(data) {
        return request("POST", U_SAVE_BASIC_INFO, data);
    }

    saveAddress(data) {
        return request("POST", U_SAVE_ADDRESS, data);
    }

    addQuestion(data) {
        return request("POST", U_ADD_QUESTION, data);
    }

    deleteQuestion(val) {
        return request("DELETE", U_DELETE_QUESTION + val);
    }

    getQuestions() {
        return request("GET", U_GET_QUESTIONS);
    }

    getSkills() {
        return request("GET", U_GET_SKILLS);
    }

    getUserSkills() {
        return request("GET", U_GET_USER_SKILLS);
    }

    addSkill(data) {
        return request("POST", U_ADD_SKILL, data);
    }

    saveLocalityInfo(data) {
        return request("POST", U_SAVE_LOCALITY, data);
    }

    savePortfolio(data) {
        return request("POST", U_SAVE_PORTFOLIO, data);
    }

    //AVAILABILITY
    getAvailableServices() {
        return request("GET", U_GET_AVAILABLE_SERVICES);
    }

    setAvailableService(data) {
        return request("POST", U_SET_AVAILABLE_SERVICES, data);
    }

    getAvailability(data) {
        return request("POST", U_GET_AVAILABILITY, data);
    }

    getAvailabilityByDate(data) {
        return request("POST", U_GET_AVAILABILITY_BY_DATES, data);
    }

    //MY PETS
    addPet(data) {
        return request("POST", U_ADD_PET, data);
    }

    getAllPets() {
        return request("GET", U_GET_ALL_PETS);
    }

    getSinglePets(id) {
        return request("GET", U_SINGLE_PET + id);
    }

    updatePet(data, id) {
        return request("POST", U_UPDATE_PET + id, data);
    }

    deletePet(id) {
        return request("DELETE", U_DELETE_PET + id);
    }

    //Service
    getService() {
        return request("GET", U_GET_SERVICES);
    }

    activeBoarding(data) {
        return request("POST", U_ACTIVE_BOARDING, data);
    }

    activeDayCare(data) {
        return request("POST", U_ACTIVE_DAY_CARE, data);
    }

    activeHouseSitting(data) {
        return request("POST", U_ACTIVE_HOUSE_SITTING, data);
    }

    activeDropInVisits(data) {
        return request("POST", U_ACTIVE_DROP_IN_VISITS, data);
    }

    activePetWalking(data) {
        return request("POST", U_ACTIVE_PET_WALKING, data);
    }

    activeGrooming(data) {
        return request("POST", U_ACTIVE_GROOMING, data);
    }

    activeHouseCall(data) {
        return request("POST", U_ACTIVE_HOUSE_CALL, data);
    }

    //House call
    getHouseCallInfo(val) {
        return request("GET", U_GET_HOUSE_CALL_INFO + val);
    }

    getHouseCallService(data) {
        return request("POST", U_GET_HOUSE_CALL_SERVICE, data);
    }

    houseCallServiceFee(data) {
        return request("POST", U_HOUSE_CALL_SERVICE_FEE, data);
    }

    deleteHouseCallServiceFee(pet_type, id) {
        return request("DELETE", U_HOUSE_CALL_SERVICE_FEE + '/' + pet_type + '/' + id);
    }

    houseCallPreference(data) {
        return request("POST", U_HOUSE_CALL_PREFERENCE, data);
    }

    houseCallDiscount(data) {
        return request("POST", U_HOUSE_CALL_DISCOUNT, data);
    }

    getHouseCallPreview() {
        return request("GET", U_GET_HOUSE_CALL_PREVIEW);
    }

    //Grooming
    getGroomingService(data) {
        return request("POST", U_GET_PET_SERVICE, data);
    }

    getGroomingInfo(val) {
        return request("GET", U_GET_GROOMING_INFO + val);
    }

    groomingServiceFee(data) {
        return request("POST", U_GROOMING_SERVICE_FEE, data);
    }

    deleteGroomingServiceFee(pet_type, id) {
        return request("DELETE", U_GROOMING_SERVICE_FEE + '/' + pet_type + '/' + id);
    }

    groomingServicePrefernce(data) {
        return request("POST", U_GROOMING_SERVICE_PREFERENCE, data);
    }

    groomingServiceDiscount(data) {
        return request("POST", U_GROOMING_SERVICE_DISCOUNT, data);
    }

    getGroomingPreview() {
        return request("GET", U_GET_GROOMING_PREVIEW);
    }

    //BOARDING
    boardingServiceFee(data) {
        return request("POST", U_BOARDING_PET_SERVICE_FEE, data);
    }

    deleteBoardingServiceFee(pet_type, id) {
        return request("DELETE", U_BOARDING_PET_SERVICE_FEE + '/' + pet_type + '/' + id);
    }

    boardingPreference(data) {
        return request("POST", U_BOARDING_PREFERENCE, data);
    }

    boardingDiscount(data) {
        return request("POST", U_BOARDING_DISCOUNTS, data);
    }

    getBoardingInfo(id) {
        return request("GET", U_GET_BOARDING_INFO + id);
    }

    getBoardingPreview() {
        return request("GET", U_GET_BOARDING_PREVIEW);
    }

    //HOUSE SITTING
    getHouseSittingInfo(id) {
        return request("GET", U_GET_HOUSE_SITTING_INFO + id);
    }

    houseSittingServiceFee(data) {
        return request("POST", U_HOUSE_SITTING_SERVICE_FEE, data);
    }

    deleteHouseSittingServiceFee(pet_type, id) {
        return request("DELETE", U_HOUSE_SITTING_SERVICE_FEE + '/' + pet_type + '/' + id);
    }

    houseSittingDiscount(data) {
        return request("POST", U_HOUSE_SITTING_DISCOUNTS, data);
    }

    houseSittingPreference(data) {
        return request("POST", U_HOUSE_SITTING_PREFERENCE, data);
    }

    getHouseSittingPreview() {
        return request("GET", U_GET_HOUSE_SITTING_PREVIEW);
    }

    //Drop in visits
    getDropInVisitsInfo(id) {
        return request("GET", U_GET_DROP_IN_VISITS_INFO + id);
    }

    dropInVisitsServiceFee(data) {
        return request("POST", U_DROP_IN_VISITS_SERVICE_FEE, data);
    }

    deleteDropInVisitsServiceFee(pet_type, id) {
        return request("DELETE", U_DROP_IN_VISITS_SERVICE_FEE + '/' + pet_type + '/' + id);
    }

    dropInVisitPreference(data) {
        return request("POST", U_DROP_IN_VISITES_PREFERENCE, data);
    }

    dropInVisitDiscounts(data) {
        return request("POST", U_DROP_IN_VISITS_DISCOUNTS, data);
    }

    getDropInVisitsPreview() {
        return request("GET", U_GET_DROP_IN_PREVIEW);
    }

    //Pet day care
    getPetDayCareInfo(id) {
        return request("GET", U_PET_DAY_CARE_INFO + id);
    }

    petDayCateServiceFee(data) {
        return request("POST", U_PET_DAY_CARE_SERVICE_FEES, data);
    }

    deletePetDayCateServiceFee(pet_type, id) {
        return request("DELETE", U_PET_DAY_CARE_SERVICE_FEES + '/' + pet_type + '/' + id);
    }

    petDayCarePreference(data) {
        return request("POST", U_PET_DAY_CARE_PREFERENCE, data);
    }

    petDayCareDiscount(data) {
        return request("POST", U_PET_DAY_CARE_DISCOUNT, data);
    }

    getPetDayCarePreview() {
        return request("GET", U_GET_PET_DAY_CARE_PREVIEW);
    }

    //Dog walking
    getDogWalkingInfo(id) {
        return request("GET", U_DOG_WALKING_INFO + id);
    }

    DogWalkingServiceFee(data) {
        return request("POST", U_DOG_WALKING_SERVICE_FEE, data);
    }

    deleteDogWalkingServiceFee(pet_type, id) {
        return request("DELETE", U_DOG_WALKING_SERVICE_FEE + '/' + pet_type + '/' + id);
    }

    dogWalkingPreference(data) {
        return request("POST", U_DOG_WALKING_SERVICE_PREFERENCE, data);
    }

    dogWalkingDiscount(data) {
        return request("POST", U_DOG_WALKING_DISCOUNT, data);
    }

    getDogWalkingPreview() {
        return request("GET", U_GET_DOG_WALKING_PREVIEW);
    }

    //SITTER REQUEST
    petAdditionalServices(data) {
        return request("POST", U_PET_ADDITIONAL_REQUEST, data);
    }

    petAmountCalculation(data) {
        return request("POST", U_PET_AMOUNT_CALCULATION, data);
    }

    sitterRequest(data) {
        return request("POST", U_REQUEST_SITTER, data);
    }

    changeRequestStatus(data) {
        return request("POST", U_CHANGE_REQUEST_STATUS, data);
    }

    petServiceAvailability(data) {
        return request("POST", U_PET_SERVICE_AVAIALABILITY, data);
    }

    cancelRequest(id, data) {
        return request("POST", U_GET_CANCEL_REQUEST + id, data)
    }

    sitterReviewComment(data) {
        return request("POST", U_SITTER_REVIEW_COMMENT, data);
    }

    //DASHOBOARD
    getSitterReservetions(data, page) {
        return request("POST", U_GET_SITTER_RESERVATIONS + "?page=" + page, data);
    }

    getUserReservations(data, page) {
        return request("POST", U_GET_USER_RESERVATIONS + "?page=" + page, data);
    }

    addBookingNotes(data) {
        return request("POST", U_ADD_BOOKING_NOTES , data);
    }//CHAT
    sendMessage(data) {
        return request("POST", U_SEND_MESSAGE, data);
    }

    chatAction(data) {
        return request("POST", U_CHAT_ACTION, data);
    }

    getThreadBooking(val) {
        return request("GET", U_GET_THREAD_BOOKING + val);
    }

    getSingleBooking(id) {
        return request("GET", U_BOOKING_ADDITIONAL_INFO + id);
    }

    disputeBooking(data) {
        return request("POST", U_DISPUTE_BOOKING, data);
    }

    getDisputeBooking(data, page) {
        return request("POST", U_GET_DISPUTE_BOOKING + "?page=" + page, data);
    }

    arrangeMeetup(data) {
        return request("POST", U_ARRANG_MEETUP, data);
    }

    updateMeetupStatus(data) {
        return request("POST", U_CHANGE_MEETUP_STATUS, data);
    }

    getMeetupRequest(id) {
        return request("GET", U_GET_MEETUPS + id);
    }

    getTransportChargeRequest(data) {
        return request("GET", U_GET_TRANSPORT_CHARGES + data);
    }

    addTransportChargeRequest(data) {
        return request("POST", U_ADD_TRANSPORT_CHARGES, data);
    }

    updateRequestStatus(data) {
        return request("POST", U_UPDATE_REQUEST_STATUS, data);
    }

    updateReadStatus(data) {
        return request("POST", U_READ_STATUS, data);
    }

    reportSitter(data) {
        return request("POST", U_GET_REPORT_SITTER, data);
    }

    //COMMON API
    getCoutires() {
        return request("GET", U_GET_COUTRIES);
    }

    getCities(id) {
        return request("GET", U_GET_CITIES + id);
    }

    getPets() {
        return request("GET", U_GET_PETS);
    }

    getBreedWithType(data) {
        return request("POST", U_GET_BREED_WITH_TYPE, data);
    }

    uploadFile(data) {
        return request("POST", U_IMAGE_UPLOAD, data);
    }

    uploadMultiFile(data) {
        return request("POST", U_IMAGE_MULTI_UPLOAD, data);
    }

    getFilteredAvialableSitter(data) {
        return request("POST", U_FILTERED_AVAILABLE_SITTER, data);
    }


    getFavoriteSitters(data) {
        return request("POST", U_GET_FAVORITE_SITTER, data);
    }

    uploadProfilePicture(data) {
        return request("POST", U_UPLOAD_PROFILE_PICTURE, data);
    }

    uploadDocument(data) {
        return request("POST", U_UPLOAD_DOCUMENTS, data);
    }

    //Spots
    getPetSpotsCategory() {
        return request("GET", U_PET_SPOT_CATEGORIES);
    }

    getPetSpots(data) {
        return request("POST", U_GET_PET_SPOTS, data);
    }

    getFeaturedPetSpots(data) {
        return request("POST", U_GET_FEATURED_PET_SPOTS, data);
    }

    markUnmarkSpot(data) {
        return request("POST", U_MARK_UNMARK_SPOT, data);
    }

    getPetSpotDetails(data) {
        return request("GET", U_GET_PET_SPOT_DETAILS + data);
    }

    getPaymentMethods() {
        return request("GET", U_GET_PAYMENT_METHODS);
    }

    getReservationTypes() {
        return request("GET", U_GET_RESERVATION_TYPES);
    }

    getSmokingCession() {
        return request("GET", U_GET_SMOKING_CESSIONS);
    }

    getParking() {
        return request("GET", U_GET_PARKING);
    }

    getLangauges() {
        return request("GET", U_GET_LANGAUGES);
    }

    addSpot(data) {
        return request("POST", U_ADD_SPOT, data);
    }

    updateSpot(id, data) {
        return request("POST", U_UPDATE_SPOT + id, data);
    }

    rateSitter(data) {
        return request("POST", U_RATE_SITTER, data);
    }

    rateOwner(data) {
        return request("POST", U_RATE_OWNER, data);
    }

    getSingleSitter(id) {
        return request("GET", U_GET_SINGLE_SITTER + id);
    }

    getSitterReviews(filter) {
        return request("POST", U_GET_SITTER_REVIEWS , filter);
    }

    markUnmarkSitter(data) {
        return request("POST", U_BOOKMARK_SITTER, data);
    }

    getAllSpots() {
        return request("GET", U_GET_ALL_SPOT);
    }

    getFavoriteSpots() {
        return request("GET", U_GET_FAVORITE_SPOT);
    }

    deleteSpot(val) {
        return request("DELETE", U_DELETE_SPOT + val);
    }

    getSingleSpot(id) {
        return request("GET", U_GET_SINGLE_SPOT + id);
    }
    
    getSingleCategory(value){
        return request("GET", U_GET_SINGLE_CATEGORY + value);
    }

    getSitterAvailability(data) {
        return request("POST", U_GET_SITTER_AVAILABILITY, data);
    }

    rateReviewSpot(data) {
        return request("POST", U_REVIEW_SPOT, data);
    }

    getReviewSpot(data) {
        return request("POST", U_GET_REVIEW_SPOT, data);
    }

    deleteSpotReview(id) {
        return request("DELETE", U_GET_DELETE_SPOT_REVIEW + id)
    }

    // getSingleSpotReviews(id) {
    //     return request("GET", U_GET_SINGLE_SPOT_REVIEW + id)
    // }
    getSingleSpotReviews(data) {
        return request("POST", U_GET_SINGLE_SPOT_REVIEW , data)
    }

    getEmailSubcription(email) {
        return request("POST", U_EMAIL_SUBCRIPTION, email);
    }

    //Payment and Bank account
    addNewCard(data) {
        return request("POST", U_ADD_CARD, data);
    }

    getAllCard() {
        return request("GET", U_GET_CARD);
    }

    deleteCard(data) {
        return request("POST", U_DELETE_CARD, data);
    }

    addNewBankAccount(data) {
        return request("POST", U_ADD_BANK_ACCOUNT, data);
    }

    getAllBankAccount() {
        return request("GET", U_GET_BANK_ACCOUNT);
    }

    getAllBankAccountDetails() {
        return request("GET", U_GET_BANK_DETAILS);
    }

    markCardAsDefault(data) {
        return request("POST", U_MAKE_CARD__DEFAULT, data);
    }

    makeBankAsDefault(data) {
        return request("POST", U_MAKE_BANK_DEFAULT, data);
    }

    deleteBankAccount(id) {
        return request("GET", U_DELETE_BANK_ACCOUNT + id);
    }

    getSingleRequestDetail(id) {
        return request("GET", U_GET_REQUEST_DETAIL + id);
    }

    confirmPayment(id, data) {
        return request("POST", U_CONFIRM_PAYMENT + id, data);
    }

    paymentHistory(data, page) {
        return request("POST", U_PAYMENT_HISTORY + `?page=${page}`, data);
    }

    // cancelBooking(id,data){
    //   return request("POST", U_CANCEL_BOOKING + id, data);
    // }


    getFormattedAddress = async (id) => {
        const apiUrl = `${GOOGLE_PLACES_BASE_URL}/details/json?placeid=${id}&key=${GOOGLE_PLACES_API}`;
        const result = await axios.request({
            method: "post",
            url: apiUrl,
        });
        return result;
    }

    getSitterProfileUrl = (id) => {
        const url = `${baseURL}sitter-profile/${id}?serviceId=1`;
        return url;
    }
    getSpotProfileUrl = (id) => {
        const url = `${baseURL}pet-spots/${id}`;
        return url;
    }

    applyCoupon(data) {
        return request("POST", U_APPLY_COUPON, data);
    }

    removeCoupon(id) {
        return request("GET", U_REMOVE_COUPON + id);
    }

    getCoupons(type) {
        return request("GET", U_GET_COUPON + type);
    }

    addCoupon(data) {
        return request("POST", U_GET_ADD_COUPON, data);
    }

    //News

    getNews() {
        return request("GET", U_GET_NEWS);
    }

    getEvents() {
        return request("GET", U_GET_EVENT);
    }

    getSingleNews(id) {
        return request("GET", U_GET_SINGLE_NEWS + id);
    }

    getLandingPageNewsRoom() {
        return request("GET", U_GET_ALL_LANDING_PAGE_NEWS);
    }

    getSingleNewsRoom(id) {
        return request("GET", U_GET_SINGLE_PAGE_NEWSROOM + id);
    }

    // Explore our sitter
    getExplorePopularSitters() {
        return request("GET", U_GET_EXPLORE_SITTER);
    }

    //FAQs
    getAllFAQs(data) {
        return request("POST", U_GET_ALL_FAQ, data)
    }

    getSingleFAQ(id) {
        return request("GET", U_GET_SINGLE_FAQ + id)
    }

    getLandingPageFAQ() {
        return request("GET", U_GET_LANDING_PAGE_FAQ)
    }

    getSubCategoryFAQ(data) {
        return request("POST", U_GET_SUB_CATEGORY_FAQ, data)
    }

    getRelatedFAQ(data) {
        return request("POST", U_GET_RELATED_FAQ, data)
    }

    getRecentViewedFAQ(data) {
        return request("POST", U_GET_RECENT_VIEWED_FAQ, data)
    }

    getReportedSitter(id) {
        return request("GET", U_GET_REPORTED_SITTER + id)
    }

    // wishlist API's
    createNewWishlist(data) {
        return request("POST", U_CREATE_WISHLIST, data);
    }

    getSpotLists() {
        return request("GET", U_GET_ALL_SPOT_LIST)
    }

    getSingleList(id) {
        return request("GET", U_GET_SINGLE_LIST + id)
    }

    markSpotAsFavourite(data) {
        return request("POST", MARK_SPOT_AS_FAVOURITE, data)
    }

    deleteWishlist(id) {
        return request('DELETE', U_DELETE_WISHLIST + id)
    }

    updateWishlist(id, data) {
        return request('POST', U_UPDATE_WISHLIST + id, data)
    }

    // sitter wishlist API's
    // wishlist API's
    createNewSitterWishlist(data) {
        return request("POST", U_CREATE_SITTER_WISHLIST, data);
    }

    getSitterLists() {
        return request("GET", U_GET_ALL_SITTER_LIST)
    }

    getSingleSitterList(data, id) {
        return request("POST", U_GET_SINGLE_SITTER_LIST + `/${id}`, data)
    }

    markSitterAsFavourite(data) {
        return request("POST", MARK_SITTER_AS_FAVOURITE, data)
    }

    deleteSitterWishlist(id) {
        return request('DELETE', U_DELETE_SITTER_WISHLIST + id)
    }

    updateSitterWishlist(id, data) {
        return request('POST', U_UPDATE_SITTER_WISHLIST + id, data)
    }

    // spot note
    updateSpotNote(data) {
        return request('POST', U_UPDATE_SPOT_NOTE, data)
    }

    resendEmailOTP(data) {
        return request('POST', U_RESEND_OTP, data)
    }

    resendMobileOTP(data) {
        return request('POST', U_RESEND_MOBILE_OTP, data)
    }

    deletePortfolioImage(data) {
        return request('POST', U_REMOVE_PORTFOLIO_IMAGE, data)
    }

    userContactUsSubmit(data) {
        return request('POST', U_CONTACT_US_SUMBIT, data)
    }

    userSubmitCareerForm(data) {
        return request('POST', U_CAREER_FORM_SUBMIT, data)
    }

    getCareerPageData() {
        return request('GET', U_GET_CAREER_PAGE)
    }

    // Static pages
    getAboutUsPage() {
        return request("GET", CMS_ABOUT_US)
    }

    // CMS_PRIVACY_POLICY
    getPrivacyPolicyPage() {
        return request("GET", CMS_PRIVACY_POLICY)}


    // CMS_TERMS
    getTermsPage() {
        return request("GET", CMS_TERMS)
    }

    // CMS_COOKIE_POLICY
    getCookiePolicyPage() {
        return request("GET", CMS_COOKIE_POLICY)
    }

    verifyRecaptcha(token) {
        return request('POST', VERIFY_RECAPTCHA, {token : token})
    }

    // Blog
    getCategoryBlog(){
         return request('Get' , U_GET_ALL_LANDING_NewRoom )
    }

}
