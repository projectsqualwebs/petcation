import {useEffect, useState} from "react";

export const GOOGLE_PLACES_BASE_URL = "https://maps.googleapis.com/maps/api/place";
//Our account
// export const GOOGLE_PLACES_API = "AIzaSyBhlm5dfswCn3ePLbHgiV1EuQ48k9SdYCw";
// other app
export const GOOGLE_PLACES_API = process.env.NEXT_PUBLIC_GOOGLE_PLACES_API
//Client account
// export const GOOGLE_PLACES_API = "AIzaSyAQ4od-vq76vmbKbyi0BHpzcp-3IphipEo";
// export const GOOGLE_PLACES_API =  "AIzaSyAXxx3dEFMdLPGlVKofPs5UNF_oHs07f7w";

export const U_FACEBOOK_ID = process.env.NEXT_PUBLIC_U_FACEBOOK_ID;
export const U_TWITTER_API_KEY = process.env.NEXT_PUBLIC_U_TWITTER_API_KEY;
export const U_TWITTER_SECRET_KEY = process.env.NEXT_PUBLIC_U_TWITTER_SECRET_KEY;

// let frontendEndPoint = 'http://localhost:3000/'
let frontendEndPoint = process.env.NEXT_PUBLIC_FRONTEND_POINT;

export const U_INVITE_URL = `${frontendEndPoint}signup?referral=`;
//Sitter profile frontend base URL
export const U_BASE_URL = process.env.NEXT_PUBLIC_U_BASE_URL;
// export const U_BASE_URL = 'http://54.151.160.83/petecation/public/api/';


export const U_USER_REGISTER = "user-register";
export const U_USER_LOGIN = "user-login";
export const U_SOCIAL_LOGIN = "social-login";
export const U_SOCIAL_REGISTER = "social-register";
export const U_GET_DASHBOARD = "user/dashboard";
export const U_SAVE_BASIC_INFO = "user/save-basic-info";
export const U_SAVE_ADDRESS = "user/save-address";
export const U_GET_BASIC_INFO = "user/get-basic-info";
export const U_GET_ADDRESS = "user/get-address";

export const U_FORGOT_PASSWORD = "forget-password";
export const U_CHANGE_PASSWORD = "user/change-password";
export const U_RESET_PASSWORD = "reset-password";
export const U_VERIFY_EMAIL = "email-verification";
export const U_VERIFY_MOBILE = "mobile-verification";


export const U_ADD_QUESTION = "user/add-questions";
export const U_DELETE_QUESTION = "user/delete-question/";
export const U_GET_QUESTIONS = "user/get-questions";

export const U_GET_SKILLS = "get-skills";
export const U_GET_USER_SKILLS = "user/get-skills";
export const U_ADD_SKILL = "user/add-skills";
export const U_SAVE_LOCALITY = "user/save-locality-info";
export const U_SAVE_PORTFOLIO = "user/save-portfolio-images";
export const U_GET_EARNED_AMOUNT = "user/get-earned-amount";



//MY PETS
export const U_ADD_PET = "user/add-new-pet";
export const U_GET_ALL_PETS = "user/get-all-pets";
export const U_SINGLE_PET = "user/get-single-pet/";
export const U_UPDATE_PET = "user/update-pet-info/";
export const U_DELETE_PET = "user/delete-pet-info/";

//MY Service
export const U_GET_SERVICES = "get-services";
export const U_GET_AVAILABLE_SERVICES = "sitter/services";
export const U_SET_AVAILABLE_SERVICES = "sitter/service-availability";
export const U_GET_AVAILABILITY = "sitter/availability-services";
export const U_GET_AVAILABILITY_BY_DATES = "sitter/date-availability-services";
export const U_BOOKMARK_SITTER = "mark-as-favorite"
export const U_GET_SITTER_REVIEWS = 'get-sitter-reviews';

//Boarding
export const U_ACTIVE_BOARDING = "user/boarding-service-manage";
export const U_BOARDING_PET_SERVICE_FEE = "user/boarding-service-fees";
export const U_DELETE_BOARDING_PET_SERVICE_FEE = "user/boarding-service-fees/";
export const U_BOARDING_PREFERENCE = "user/boarding-service-preferences";
export const U_BOARDING_DISCOUNTS = "user/boarding-service-discounts";
export const U_GET_BOARDING_INFO = "user/boarding-service-info/";
export const U_GET_BOARDING_PREVIEW = "user/all-boarding-service-info";

//House sitting
export const U_ACTIVE_HOUSE_SITTING = "user/house-sitting-service-manage";
export const U_GET_HOUSE_SITTING_INFO = "user/house-sitting-service-info/";
export const U_HOUSE_SITTING_SERVICE_FEE = "user/house-sitting-service-fees";
export const U_HOUSE_SITTING_DISCOUNTS = "user/house-sitting-service-discounts";
export const U_HOUSE_SITTING_PREFERENCE = "user/house-sitting-service-preferences";
export const U_GET_HOUSE_SITTING_PREVIEW = "user/all-house-sitting-service-info";

//DAY care
export const U_ACTIVE_DAY_CARE = "user/day-care-service-manage";

//Drop-in visit
export const U_ACTIVE_DROP_IN_VISITS = "user/drop-in-visit-service-manage";
export const U_GET_DROP_IN_VISITS_INFO = "user/drop-in-visit-service-info/";
export const U_DROP_IN_VISITS_SERVICE_FEE = "user/drop-in-visit-service-fees";
export const U_DROP_IN_VISITES_PREFERENCE = "user/drop-in-visit-service-preferences";
export const U_DROP_IN_VISITS_DISCOUNTS = "user/drop-in-visit-service-discounts";
export const U_GET_DROP_IN_PREVIEW = "user/all-drop-in-visit-service-info";

//PET walking visits

export const U_PET_DAY_CARE_INFO = "user/day-care-service-info/";
export const U_PET_DAY_CARE_SERVICE_FEES = "user/day-care-service-fees";
export const U_PET_DAY_CARE_PREFERENCE = "user/day-care-service-preferences";
export const U_PET_DAY_CARE_DISCOUNT = "user/day-care-service-discounts";
export const U_GET_PET_DAY_CARE_PREVIEW = "user/all-day-care-service-info";

//  Dog walking
export const U_ACTIVE_PET_WALKING = "user/walking-service-manage";
export const U_DOG_WALKING_INFO = "user/walking-service-info/";
export const U_DOG_WALKING_SERVICE_FEE = "user/walking-service-fees";
export const U_DOG_WALKING_SERVICE_PREFERENCE = "user/walking-service-preferences";
export const U_DOG_WALKING_DISCOUNT = "user/walking-service-discounts";
export const U_GET_DOG_WALKING_PREVIEW = "user/all-walking-service-info";

//Sitter Request
export const U_PET_ADDITIONAL_REQUEST = "sitter/pet-additional-services";
export const U_BOOKING_ADDITIONAL_INFO = "sitter/pet-request/";
export const U_PET_AMOUNT_CALCULATION = "sitter/pet-amount-calculations";
export const U_REQUEST_SITTER = "sitter/pet-request";
export const U_GET_REQUEST_DETAIL = "sitter/pet-request/";
export const U_CHANGE_REQUEST_STATUS = "change-sitter-request-status";
export const U_PET_SERVICE_AVAIALABILITY = "sitter/pet-service-availability";
export const U_CONFIRM_PAYMENT = "sitter/confirm-payment/";
export const U_PAYMENT_HISTORY = "payment-history";
// export const U_CANCEL_BOOKING = "cancel-booking/";
export const U_GET_CANCEL_REQUEST = 'sitter/cancel-booking/';
export const U_SITTER_REVIEW_COMMENT = 'sitter-review-comment';
export const U_DEACTIVATE_ACCOUNT = 'user/change-account-status';
export const U_DISPUTE_BOOKING = 'booking-dispute-resolution';
export const U_GET_DISPUTE_BOOKING = 'get-dispute-bookings';
export const U_ADD_BOOKING_NOTES = 'pet-booking-notes';

//GROOMING
export const U_ACTIVE_GROOMING = "user/grooming-service-manage";
export const U_GET_GROOMING_INFO = "user/grooming-service-info/";
export const U_GET_PET_SERVICE = "get-grooming-services";
export const U_GROOMING_SERVICE_FEE = "user/grooming-service-fees";
export const U_GROOMING_SERVICE_PREFERENCE = "user/grooming-service-preferences";
export const U_GROOMING_SERVICE_DISCOUNT = "user/grooming-service-discounts";
export const U_GET_GROOMING_PREVIEW = "user/all-grooming-service-info";

//House call
export const U_ACTIVE_HOUSE_CALL = "user/house-call-service-manage";
export const U_GET_HOUSE_CALL_INFO = "user/house-call-service-info/";
export const U_GET_HOUSE_CALL_SERVICE = "get-house-call-services";
export const U_HOUSE_CALL_SERVICE_FEE = "user/house-call-service-fees";
export const U_HOUSE_CALL_PREFERENCE = "user/house-call-service-preferences";
export const U_HOUSE_CALL_DISCOUNT = "user/house-call-service-discounts";
export const U_GET_HOUSE_CALL_PREVIEW = "user/all-house-call-service-info";

//Dashboard
export const U_GET_SITTER_RESERVATIONS = "get-sitter-reservations";
export const U_GET_USER_RESERVATIONS = "get-user-reservations";
export const U_REMOVE_PORTFOLIO_IMAGE = 'user/remove-portfolio-images';
export const U_CONTACT_US_SUMBIT = 'submit-contact';
export const U_CAREER_FORM_SUBMIT = 'submit-career';
export const U_GET_CAREER_PAGE = 'career';
//CHAT
export const U_SEND_MESSAGE = "send-message";
export const U_READ_STATUS = "thread-read-status";
export const U_GET_THREAD_BOOKING = "get-thread-bookings/";
export const U_ARRANG_MEETUP = "create-meet-up";
export const U_CHANGE_MEETUP_STATUS = "meet-up-status";
export const U_GET_MEETUPS = "get-meet-up/";
export const U_CHAT_ACTION = "chat-thread-status";
export const U_GET_REPORTED_SITTER = 'user/report-sitter-status/';


export const U_GET_TRANSPORT_CHARGES = "sitter/request-transport-amount/";
export const U_ADD_TRANSPORT_CHARGES = "sitter/request-transport-amount";

export const  U_UPDATE_REQUEST_STATUS = "change-sitter-request-status";

//PET SPOT
export const U_GET_ALL_SPOT = "sitter/spots";
export const U_GET_FAVORITE_SPOT = "get-favorite-spots";
export const U_PET_SPOT_CATEGORIES = "get-spot-categories";
export const U_GET_PET_SPOTS = "get-spots";
export const U_GET_FEATURED_PET_SPOTS = "get-feature-spots";
export const U_GET_PET_SPOT_DETAILS = "spot/";
export const U_GET_PAYMENT_METHODS = "get-payment-methods";
export const U_GET_RESERVATION_TYPES = "get-reservations";
export const U_GET_SMOKING_CESSIONS = "get-smoking-cessions";
export const U_GET_PARKING = "get-parkings";
export const U_GET_LANGAUGES = "get-languages";
export const U_ADD_SPOT = "sitter/create-spot";
export const U_UPDATE_SPOT = "sitter/update-spot/";
export const U_DELETE_SPOT = "sitter/remove-spot/";
export const U_GET_SINGLE_SPOT = "sitter/spot/";
export const U_GET_SINGLE_CATEGORY = "category-news-rooms/"
export const U_MARK_UNMARK_SPOT = "mark-spot-as-favorite";
export const U_REVIEW_SPOT = "spot-review-rate";
export const U_GET_REVIEW_SPOT = "get-spot-review-rate";
export const U_GET_DELETE_SPOT_REVIEW = "delete-spot-review-rate/";
export const U_GET_SINGLE_SPOT_REVIEW = 'single-spot-review';

//COMMON
export const U_RATE_SITTER = "sitter-review-rate";
export const U_RATE_OWNER = "owner-review-rate";
export const U_GET_CITIES = "get-cities/";
export const U_GET_COUTRIES = "get-countries";
export const U_GET_PETS = "get-pets";
export const U_GET_BREED_WITH_TYPE = "get-breeds";
export const U_IMAGE_UPLOAD = "upload";
export const U_IMAGE_MULTI_UPLOAD = "upload-multi";
export const U_UPLOAD_PROFILE_PICTURE = "user/save-profile-pic";
export const U_FILTERED_AVAILABLE_SITTER = "get-available-sitters";
export const U_GET_SINGLE_SITTER = "get-sitter-profile/";
export const U_GET_SITTER_AVAILABILITY = "sitter/pet-service-availability";
export const U_UPLOAD_DOCUMENTS = "user/upload-document";
export const U_GET_FAVORITE_SITTER = "get-favorite-sitters";
export const U_EMAIL_SUBCRIPTION = "email-subscription";

//Payment Card & Bank details
export const U_ADD_CARD = "user/add-card";
export const U_GET_CARD = "user/get-cards";
export const U_DELETE_CARD = "user/delete-card";

export const  U_ADD_BANK_ACCOUNT = "user/create-bank-account";
export const U_GET_BANK_ACCOUNT = "user/get-all-bank-accounts";
export const U_GET_BANK_DETAILS = "user/get-bank-details/";
export const U_DELETE_BANK_ACCOUNT = "user/delete-bank-account/";
export const U_MAKE_BANK_DEFAULT = 'user/default-bank-account';
export const U_MAKE_CARD__DEFAULT = 'user/make-default-card';

export const U_APPLY_COUPON = 'sitter/apply-coupon';
export const U_REMOVE_COUPON = 'sitter/remove-coupon/';
export const U_GET_COUPON = 'user/get-saved-coupons/';
export const U_GET_ADD_COUPON = 'user/add-coupon';
export const U_GET_NEWS = 'get-news-events/1';
export const U_GET_EVENT = 'get-news-events/2';
export const U_GET_SINGLE_NEWS="single-news-event/";
export const U_GET_REPORT_SITTER="user/report-sitter";

// FAQ
export const U_GET_ALL_FAQ="all-faqs";
export const U_GET_SINGLE_FAQ = 'single-faq/';
export const U_GET_LANDING_PAGE_FAQ = 'faq-landing';
export const U_GET_SUB_CATEGORY_FAQ = 'faqs-by-subcategory';
export const U_GET_RELATED_FAQ = 'related-faq';
export const U_GET_RECENT_VIEWED_FAQ = 'recent-viewed-faq';

//Landing Page API
export const U_GET_EXPLORE_SITTER = 'popular-sitters';
export const U_GET_ALL_LANDING_PAGE_NEWS = 'news-rooms';
export const U_GET_SINGLE_PAGE_NEWSROOM = 'news-rooms/';

//Blog
export const  U_GET_ALL_LANDING_NewRoom = "news-rooms-categories";
// pet spot wishlist
export const U_CREATE_WISHLIST  = 'create-spot-list';
export const U_GET_ALL_SPOT_LIST = 'sitter-spot-list';
export const U_GET_SINGLE_LIST = 'get-favorite-spots/';
export const MARK_SPOT_AS_FAVOURITE = 'mark-spot-as-favorite';
export const U_DELETE_WISHLIST = 'delete-spot-list/';
export const U_UPDATE_WISHLIST = 'update-spot-list/';
//spot note
export const U_UPDATE_SPOT_NOTE = 'sitter/update-spot-note';
// resend otps
export const U_RESEND_OTP = 'resend-email-otp';
export const U_RESEND_MOBILE_OTP = 'resend-mobile-otp';
// sitter wishlist
export const U_CREATE_SITTER_WISHLIST  = 'create-sitter-list';
export const U_GET_ALL_SITTER_LIST = 'sitter-list';
export const U_GET_SINGLE_SITTER_LIST = 'get-favorite-sitter';
export const MARK_SITTER_AS_FAVOURITE = 'mark-as-favorite';
export const U_DELETE_SITTER_WISHLIST = 'delete-sitter-list/';
export const U_UPDATE_SITTER_WISHLIST = 'update-sitter-list/';

// Static - CMS
export const CMS_ABOUT_US = 'about-us';
export const CMS_PRIVACY_POLICY = 'privacy-policy';
export const CMS_TERMS = 'terms';
export const CMS_COOKIE_POLICY = 'cookie-policy';

export const VERIFY_RECAPTCHA = 'verify-recaptcha';

export const SuccessOptions = {
    position: 'bottom-center',
    style: {
        zIndex: 1000,
        backgroundColor: '#20847e',
        color: 'white',
        fontSize: '20px',
        textAlign: 'center',
    },
    closeStyle: {
        color: 'white',
        fontSize: '16px',
    },
}

export const FailureOptions = {
    position: 'bottom-center',
    style: {
        backgroundColor: 'danger',
        color: 'white',
        fontSize: '20px',
        textAlign: 'center',
    },
    closeStyle: {
        color: 'white',
        fontSize: '16px',
    },
}

export const getCurrencySign = (set=null) => {
    // const [sign, setSign] = useState('¥');
    return "¥";
}
export const getCurrencyCode = () => {
    return 'JPY';
}
