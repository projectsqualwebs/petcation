import React, {useEffect, useState} from "react";
import { strings } from "../../public/lang/Strings";
import Link from 'next/link';
import moment from "moment";
const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;

type IProps = {
    key: number;
    data: {
        id: number;
        images: any;
        title_en: string;
        description_en: string;
        title_jp: string;
        description_jp: string;
        title_zh: string;
        description_zh: string;
        created_at: string;
        category: {
            name?: string;
            name_en?: string;
            name_jp?: string;
            name_zh?: string;
        };
    };
};

interface I_DATA {
    image: string;
    title: string;
    description: string;
    created_at: string;
    category: {
        name: string;
    };
}

const RelatedStoriesObject: React.FC<IProps> = (props: any) => {
    const [data, setData] = useState<I_DATA>()
    const [lang, setLang] = useState('en')

    useEffect( () => {
        let myLang =  localStorage.getItem('language')
        if (myLang) {
            setLang(myLang)
        }
    }, [])

    useEffect( () => {
        seLanguageContent(props?.data, lang)
    }, [props])

    const seLanguageContent = (myData, myLang) => {
        let engData = {
            image: myData?.images[0].path,
            title: myData?.title_en,
            description: myData?.description_en,
            created_at: myData?.created_at,
            category: {
                name: myData?.category?.name ?? myData?.category?.name_en,
            },
        }

        let jpData = {
            image: myData?.images[0].path,
            title: myData?.title_jp,
            description: myData?.description_jp,
            created_at: myData?.created_at,
            category: {
                name: myData?.category?.name ?? myData?.category?.name_jp,
            },
        }

        let zhData = {
            image: myData?.images[0].path,
            title: myData?.title_zh,
            description: myData?.description_zh,
            created_at: myData?.created_at,
            category: {
                name: myData?.category?.name ?? myData?.category?.name_zh,
            },
        }

        if (myLang == 'en') {
            setData(engData)
        }
        if (myLang == 'jp') {
            setData(jpData)
        }
        if (myLang == 'zh') {
            setData(zhData)
        }

    }
    return (
        <Link key={props.key+'link'} href={'/news/'+props.data.id}>
            <div className="col-12 col-md-4 col-lg-4 col-xl-4 cursor-pointer">
                <div className="stories">
                    <div className="blog-img single-img mt-0 mb-2">
                        <img src={data?.image ? IMAGE_BASE_URL+data?.image : "images/dog1.jpg"} className="img-fluid" alt="" style={{maxHeight: "235px"}} />
                    </div>
                    <a>
                        {data?.title}
                    </a>
                    <div className="blog-day">
                        <p className="blog-date">{moment(data?.created_at).format("DD-MM-YYYY")}</p>
                    </div>
                </div>
            </div>
        </Link>
    );
};

export default RelatedStoriesObject;
