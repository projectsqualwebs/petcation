import React, { useEffect, useState } from "react";
import { strings } from "../../../public/lang/Strings";
import { useSnackbar } from "react-simple-snackbar";
import API from "../../../api/Api";
import RatingStars from "../../common/RatingStars";
import { Dropdown, DropdownButton, Modal } from "react-bootstrap";
import SpotRating from "../../common/SpotRating";
import Cookies from "universal-cookie";
import SingleSpotHeaderInterface from "../../../models/SingleSpotHeaderInterface.interface";
import { errorOptions, successOptions } from "../../../public/appData/AppData";
// import { Popover } from 'react-tiny-popover'
import CreateWishlist from "../../common/CreateWishlist";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { U_FACEBOOK_ID } from "../../../api/Constants";
import { useRouter } from "next/router";
import Link from "next/link";

let api = new API();
const cookie = new Cookies();

interface I_PROPS {
  data: SingleSpotHeaderInterface;
  showModal: () => void;
  updateSpot: (id: any) => void;
}

/**
 *
 * @param props contains spotDetails for spot header
 * @function will render spot header for desktop view
 */
export default function SingleSpotHeader(props: I_PROPS) {
  const [copied, setCopied] = useState(false);
  const [openSnackbar] = useSnackbar();
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [wishList, setWishlist] = useState<any>([]);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<any>(false);
  //Auth
  var token = cookie.get("token");
  const router = useRouter();
  useEffect(() => {
    getAllWishlist();
  }, []);

  /**
   * @function : function will call api to mark and unmark our spot as favourite.
   *    @succes: update spot details by passing spot id.
   *    @failure: show error in snackbar.
   */
  const unmarkSpot = () => {
    api
      .markUnmarkSpot({ spot_id: props.data.id })
      .then((res) => {
        if (res.data.status === 200) {
          openSuccess(res.data.message);
          props.updateSpot(props.data.id);
        }
      })
      .catch((error) => {
        openSnackbar(strings.errorUpdatingStatus);
      });
  };

  const getAllWishlist = () => {
    api
      .getSpotLists()
      .then(async (res) => {
        await setWishlist(res.data.response);
      })
      .catch((err) => console.log(err.response.data.message));
  };

  const copyUrl = (e) => {
    e.preventDefault();
    const el = document.createElement("input");
    el.value = getLocation();
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopied(true);
    openSuccess(strings.CopiedToClipboard);
  };

  const getLocation = () => {
    if (typeof window !== "undefined") {
      return window.location.href;
    }
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(false);
  };

  const markAsFavourite = (listId, spotId) => {
    setAnchorEl(false);
    let data = {
      list_id: listId,
      spot_id: spotId,
    };
    api
      .markSpotAsFavourite(data)
      .then((res) => {
        props.updateSpot(props.data.id);
        openSuccess(res.data.message);
      })
      .catch((err) => openError(err.response.data.message));
  };
  return (
    <>
      <div className="container px-0 px-md-3">
        <div className="bg-white single-spot main-background mt-0 mt-md-3">
          <div className="row">
            <div className="col-12 col-md-12 co-lg-12 col-xl-12">
              <div className="single-spot-info">
                <h3 className="font-semibold mb-1">
                  {props.data
                    ? props.data.english_name + " | " + props.data.spot_name
                    : ""}
                </h3>
                <p className="mb-1">{props.data ? props.data.address : ""}</p>
                <div className="row align-items-center">
                  <div className="col col-md featured-details">
                    <div className="d-flex hotel-rating">
                      {[1, 2, 3, 4, 5].map((val, index) => (
                        <div className="rating-star">
                          <div
                            className={
                              val <= props?.data?.overall_rate ? "active" : ""
                            }
                          >
                            <svg
                              aria-hidden="true"
                              focusable="false"
                              data-prefix="fas"
                              data-icon="star"
                              role="img"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 576 512"
                              className="svg-inline--fa fa-star fa-w-18 fa-2x"
                            >
                              <path
                                fill="currentColor"
                                d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                              />
                            </svg>
                          </div>
                        </div>
                      ))}
                      <div
                        onClick={() =>
                          router.replace({
                            pathname: "/pet-spots/spot-reviews",
                            query: { id: props?.data?.id },
                          })
                        }
                      >
                        <h6 className="mb-0 cursor-pointer">
                          {props.data ? parseFloat(`${props.data.overall_rate}`).toFixed(1) : ""}
                          <span className="font-14 font-normal">
                            <Link
                              href={{
                                pathname: "/pet-spots/spot-reviews",
                                query: { id: props?.data?.id },
                              }}
                            >
                              {"(" +
                                props?.data?.total_review +
                                " " +
                                strings.reviews +
                                ")"}
                            </Link>
                          </span>
                        </h6>
                      </div>
                    </div>
                  </div>
                  <div className="col-auto col-md-auto alignment">
                    {token ? (
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="px-md-3 px-0 col-auto user-profile-icon">
                          <div className="d-flex profile-share-icon">
                            <div className="profile-share-drop">
                              <DropdownButton
                                className="bg-transparent"
                                align="end"
                                title={
                                  <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="share"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512"
                                    className="svg-inline--fa fa-share fa-w-16 fa-2x"
                                  >
                                    <path
                                      fill="currentColor"
                                      d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                                      className=""
                                    ></path>
                                  </svg>
                                }
                                id="dropdown-menu-align-end"
                              >
                                <Dropdown.Item
                                  href={`mailto:?subject=${
                                    props.data &&
                                    props.data.spot_name + ` ${strings.spot}`
                                  }&body=${
                                    encodeURIComponent(getLocation()) || ""
                                  }`}
                                >
                                  <a className="">
                                    <img src="/images/social-img4.png" />
                                    {strings.viaEmail}
                                  </a>
                                </Dropdown.Item>
                                <Dropdown.Item
                                  href={`https://api.whatsapp.com/send?text=${
                                    props.data &&
                                    props.data.spot_name +
                                      ` ${strings.spot}:\n` +
                                      getLocation()
                                  }`}
                                  data-action="share/whatsapp/share"
                                >
                                  <a className="">
                                    <img src="/images/social-img3.png" />
                                    {strings.viaWhatsapp}
                                  </a>
                                </Dropdown.Item>
                                {/*<Dropdown.Item*/}
                                {/*  href={`fb-messenger://share/?link=${getLocation()}&app_id=1497423670682445`}>*/}
                                {/*  <a className="">*/}
                                {/*    {" "}*/}
                                {/*    <img src="/images/social-img1.png" />*/}
                                {/*    {strings.viaMessenger}*/}
                                {/*  </a>*/}
                                {/*</Dropdown.Item>*/}
                                <Dropdown.Item onClick={copyUrl}>
                                  <a className="">
                                    {" "}
                                    <img src="/images/social-img5.png" />
                                    {!copied
                                      ? strings.CopyLink
                                      : strings.Copied}
                                  </a>
                                </Dropdown.Item>
                              </DropdownButton>
                            </div>
                          </div>
                        </div>
                        <div className="single-share-details cursor-pointer d-none">
                          <DropdownButton
                            className="bg-transparent"
                            align="end"
                            title={
                              <a className="icon-share-single">
                                <svg
                                  aria-hidden="true"
                                  focusable="false"
                                  data-prefix="fal"
                                  data-icon="share-square"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 576 512"
                                  className="svg-inline--fa fa-share-square fa-w-18 fa-2x"
                                >
                                  <path
                                    fill="currentColor"
                                    d="M566.633 169.37L406.63 9.392C386.626-10.612 352 3.395 352 32.022v72.538C210.132 108.474 88 143.455 88 286.3c0 84.74 49.78 133.742 79.45 155.462 24.196 17.695 58.033-4.917 49.7-34.51C188.286 304.843 225.497 284.074 352 280.54V352c0 28.655 34.654 42.606 54.63 22.63l160.003-160c12.489-12.5 12.489-32.76 0-45.26zM384 352V248.04c-141.718.777-240.762 15.03-197.65 167.96C154.91 393 120 351.28 120 286.3c0-134.037 131.645-149.387 264-150.26V32l160 160-160 160zm37.095 52.186c2.216-1.582 4.298-3.323 6.735-5.584 7.68-7.128 20.17-1.692 20.17 8.787V464c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h172.146c6.612 0 11.954 5.412 11.852 12.04-.084 5.446-4.045 10.087-9.331 11.396-9.462 2.343-18.465 4.974-27.074 7.914-1.25.427-2.555.65-3.876.65H48c-8.837 0-16 7.163-16 16v352c0 8.837 7.163 16 16 16h352c8.837 0 16-7.163 16-16v-50.002c0-3.905 1.916-7.543 5.095-9.812z"
                                  />
                                </svg>
                                &nbsp;
                                <span className="mb-0 font-normal d-inline font-share-icon">
                                  {strings.Share}
                                </span>
                              </a>
                            }
                            id="dropdown-menu-align-end"
                          >
                            <p className="mb-0 font-semibold font-12">
                              {strings.ShareThisProfile}
                            </p>
                            <Dropdown.Item
                              href={`mailto:?subject=${
                                props.data &&
                                props.data.spot_name + ` ${strings.spot}`
                              }&body=${
                                encodeURIComponent(getLocation()) || ""
                              }`}
                            >
                              <a className="">
                                <img src="/images/social-img4.png" />
                                {strings.viaEmail}
                              </a>
                            </Dropdown.Item>
                            <Dropdown.Item
                              href={`https://api.whatsapp.com/send?text=${
                                props.data &&
                                props.data.spot_name +
                                  ` ${strings.spot}:\n` +
                                  getLocation()
                              }`}
                              data-action="share/whatsapp/share"
                            >
                              <a className="">
                                <img src="/images/social-img3.png" />
                                {strings.viaWhatsapp}
                              </a>
                            </Dropdown.Item>
                            {/*<Dropdown.Item*/}
                            {/*  href={`fb-messenger://share/?link=${getLocation()}&app_id=1497423670682445`}>*/}
                            {/*  <a className="">*/}
                            {/*    {" "}*/}
                            {/*    <img src="/images/social-img1.png" />*/}
                            {/*    {strings.viaMessenger}*/}
                            {/*  </a>*/}
                            {/*</Dropdown.Item>*/}
                            <Dropdown.Item onClick={copyUrl}>
                              <a className="">
                                {" "}
                                <img src="/images/social-img5.png" />
                                {!copied ? strings.CopyLink : strings.Copied}
                              </a>
                            </Dropdown.Item>
                          </DropdownButton>
                        </div>
                        <div className="px-md-3 px-0 col-auto">
                          <Button
                            id="basic-button1"
                            aria-controls={open ? "basic-menu1" : undefined}
                            aria-haspopup="true"
                            className={"p-0s"}
                            aria-expanded={open ? "true" : undefined}
                            onClick={handleClick}
                          >
                            <div className="single-share-details cursor-pointer">
                              {props.data ? (
                                <a className="icon-share-single">
                                  {props.data.is_favorite && (
                                    <svg viewBox="0 0 512 512">
                                      <path
                                        fill={"#0b6963"}
                                        d="M0 190.9V185.1C0 115.2 50.52 55.58 119.4 44.1C164.1 36.51 211.4 51.37 244 84.02L256 96L267.1 84.02C300.6 51.37 347 36.51 392.6 44.1C461.5 55.58 512 115.2 512 185.1V190.9C512 232.4 494.8 272.1 464.4 300.4L283.7 469.1C276.2 476.1 266.3 480 256 480C245.7 480 235.8 476.1 228.3 469.1L47.59 300.4C17.23 272.1 .0003 232.4 .0003 190.9L0 190.9z"
                                      />
                                    </svg>
                                  )}
                                  {!props.data.is_favorite && (
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      viewBox="0 0 512 512"
                                    >
                                      <path d="M244 84L255.1 96L267.1 84.02C300.6 51.37 347 36.51 392.6 44.1C461.5 55.58 512 115.2 512 185.1V190.9C512 232.4 494.8 272.1 464.4 300.4L283.7 469.1C276.2 476.1 266.3 480 256 480C245.7 480 235.8 476.1 228.3 469.1L47.59 300.4C17.23 272.1 0 232.4 0 190.9V185.1C0 115.2 50.52 55.58 119.4 44.1C164.1 36.51 211.4 51.37 244 84C243.1 84 244 84.01 244 84L244 84zM255.1 163.9L210.1 117.1C188.4 96.28 157.6 86.4 127.3 91.44C81.55 99.07 48 138.7 48 185.1V190.9C48 219.1 59.71 246.1 80.34 265.3L256 429.3L431.7 265.3C452.3 246.1 464 219.1 464 190.9V185.1C464 138.7 430.4 99.07 384.7 91.44C354.4 86.4 323.6 96.28 301.9 117.1L255.1 163.9z" />
                                    </svg>
                                  )}
                                </a>
                              ) : null}
                              &nbsp;
                              {props.data ? (
                                <span className="mb-0 font-normal d-inline font-share-icon">
                                  {strings.Save}
                                </span>
                              ) : null}
                            </div>
                          </Button>
                          <Menu
                            id="basic-menu"
                            anchorEl={anchorEl}
                            open={anchorEl}
                            onClose={handleClose}
                            MenuListProps={{
                              "aria-labelledby": "basic-button",
                            }}
                          >
                            {wishList.map((val) => (
                              <MenuItem
                                onClick={() =>
                                  markAsFavourite(val.id, props.data.id)
                                }
                              >
                                {val.name}
                              </MenuItem>
                            ))}
                            <MenuItem
                              onClick={() => {
                                setShowModal(true);
                                handleClose();
                              }}
                            >
                              {strings.CreateNewList}
                            </MenuItem>
                          </Menu>
                        </div>
                        <div
                          className="px-md-3 px-0 col-auto single-share-details cursor-pointer"
                          data-toggle="modal"
                          data-target="#ratings"
                        >
                          <a
                            className="icon-share-single"
                            onClick={props.showModal}
                          >
                            <svg
                              aria-hidden="true"
                              focusable="false"
                              data-prefix="fal"
                              data-icon="comment-alt-edit"
                              role="img"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 512 512"
                              className="svg-inline--fa fa-comment-alt-edit fa-w-16 fa-2x"
                            >
                              <path
                                fill="currentColor"
                                d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 7.1 5.8 12 12 12 2.4 0 4.9-.7 7.1-2.4L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64zm32 352c0 17.6-14.4 32-32 32H293.3l-8.5 6.4L192 460v-76H64c-17.6 0-32-14.4-32-32V64c0-17.6 14.4-32 32-32h384c17.6 0 32 14.4 32 32v288zM336 105.4c-12.5-12.5-32.8-12.5-45.2 0l-126.1 126c-2 2-3.4 4.5-4.2 7.3l-16 61.2c-1.4 5.5.1 11.3 4.2 15.4 3 3 7.1 4.7 11.3 4.7 1.3 0 2.7-.2 4-.5l61.2-16c2.8-.7 5.3-2.2 7.3-4.2l126.1-126.1c12.5-12.5 12.5-32.8 0-45.2L336 105.4zM213 273.6l-30.6 8 8-30.6 75-75 22.6 22.6-75 75zm97.6-97.6L288 153.4l25.4-25.4 22.6 22.6-25.4 25.4z"
                              />
                            </svg>
                            &nbsp;
                            <span className="mb-0 font-normal d-inline font-share-icon">
                              {props.data
                                ? props.data.review_rating
                                  ? strings.EditReview
                                  : strings.WriteReview
                                : null}
                            </span>
                          </a>
                        </div>
                      </div>
                    ) : (
                      <div className="d-flex align-items-center justify-content-between">
                        <div
                          className="px-md-3 px-0 col-auto single-share-details cursor-pointer"
                          data-toggle="modal"
                          data-target="#ratings"
                        >
                          <Link href="/signin">
                            <a className="icon-share-single">
                              <a>
                                <svg
                                  aria-hidden="true"
                                  focusable="false"
                                  data-prefix="fal"
                                  data-icon="comment-alt-edit"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 512 512"
                                  className="svg-inline--fa fa-comment-alt-edit fa-w-16 fa-2x"
                                >
                                  <path
                                    fill="currentColor"
                                    d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 7.1 5.8 12 12 12 2.4 0 4.9-.7 7.1-2.4L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64zm32 352c0 17.6-14.4 32-32 32H293.3l-8.5 6.4L192 460v-76H64c-17.6 0-32-14.4-32-32V64c0-17.6 14.4-32 32-32h384c17.6 0 32 14.4 32 32v288zM336 105.4c-12.5-12.5-32.8-12.5-45.2 0l-126.1 126c-2 2-3.4 4.5-4.2 7.3l-16 61.2c-1.4 5.5.1 11.3 4.2 15.4 3 3 7.1 4.7 11.3 4.7 1.3 0 2.7-.2 4-.5l61.2-16c2.8-.7 5.3-2.2 7.3-4.2l126.1-126.1c12.5-12.5 12.5-32.8 0-45.2L336 105.4zM213 273.6l-30.6 8 8-30.6 75-75 22.6 22.6-75 75zm97.6-97.6L288 153.4l25.4-25.4 22.6 22.6-25.4 25.4z"
                                  />
                                </svg>
                              </a>
                              &nbsp;
                              <span className="mb-0 font-normal d-inline font-share-icon">
                                {props.data
                                  ? props.data.review_rating
                                    ? strings.EditReview
                                    : strings.WriteReview
                                  : null}
                              </span>
                            </a>
                          </Link>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <CreateWishlist
        showModal={showModal}
        hideModal={() => setShowModal(false)}
        reRender={(value) => getAllWishlist()}
        data={null}
        setData={() => {}}
      />
    </>
  );
}
