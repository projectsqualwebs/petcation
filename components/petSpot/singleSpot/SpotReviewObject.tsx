import moment from "moment";
import RatingStars from "../../common/RatingStars";
import React from "react";
import {
    LightgalleryProvider,
    LightgalleryItem,
    useLightgallery,
} from "react-lightgallery";
import "lightgallery.js/dist/css/lightgallery.css";


const SpotReviewObject = ({value}: any) => {
    const OpenButtonWithHook = (props) => {
        const { openGallery } = useLightgallery();

        const imagesToRender = value?.images.slice(0, 3);

        const remainingImagesCount = value?.images.length-3;

        return (
            <>
                {imagesToRender.map((val, index) => (
                    <div
                        className="mr-1 cursor-pointer"
                        onClick={() => openGallery("group2")}
                        key={index}>
                        <img src={val.image} alt="My Image" height={50} />
                    </div>
                ))}
                {remainingImagesCount > 0 && (
                    <div
                        className="position-relative d-flex align-items-center justify-content-center mx-0 px-0 cursor-pointer"
                        onClick={() => openGallery("group2")}>
                        <div className="position-absolute font-weight-bold text-white opacity-100">
                            <h5> +{remainingImagesCount}</h5>
                        </div>
                        <img
                            src={value?.images[3]?.image}
                            alt="My Image"
                            height={50}
                            className='opacity-50'
                        />
                    </div>
                )}
            </>
        );
    };

    return <div>
        <div className="user-review-details"> 
            <div className="row mb-2">
                <div className="col-12 col-md">
                    <div className="d-flex justify-content-between">
                        <div className="d-flex">
                            <div className="sitter-profile-img">
                                <img
                                    src={value?.user?.profile_picture}
                                    className="img-fluid"
                                />
                            </div>
                            <div className="sitter-review-details ml-2 my-auto">
                                <h5 className="mb-0 font-medium">
                                    {value?.user?.firstname +
                                    " " +
                                    value?.user?.lastname}
                                </h5>
                                <p className="font-12 mb-0">
                                    {moment(value.created_at).format(
                                        "DD-MM-YYYY"
                                    ) +
                                    " | " +
                                    moment(value.created_at).format("HH:MM a")}
                                </p>
                                <div className="d-flex rating-star">
                                    <RatingStars rating={value.rating} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p className="font-14 ">{value.review}</p>
        </div>
        {value?.images?.length ? <LightgalleryProvider>
            <div className="row mx-0 px-0">
                <OpenButtonWithHook />
                <div>
                    {value?.images?.length
                        ? value?.images?.map((p, idx) => (
                            <>
                                <div style={{ display: "none" }}>
                                    <LightgalleryItem
                                        group={"group2"}
                                        src={p?.image}
                                        thumb={p?.image}>
                                        <img
                                            src={p?.image}
                                            style={{ width: "100%" }}
                                        />
                                    </LightgalleryItem>
                                </div>
                            </>
                        ))
                        : null}
                </div>
                <div className="buttons mt-4"></div>
            </div>
        </LightgalleryProvider> : null}
        <hr />
    </div>
};

export default SpotReviewObject