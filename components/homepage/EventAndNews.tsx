import Link from "next/link";
import { newsAndEvents } from "../../public/appData/AppData";
import { strings } from "../../public/lang/Strings";
import { useEffect, useState } from "react";
import API from '../../api/Api'

const api = new API();
export default function EventAndNews() {
  const [eventData, setEventData] = useState([]);
  useEffect(() => {
    const getEvents = () => {
      api.getEvents()
        .then((response) => {
          setEventData(response.data.response);
        })
        .catch((error) => console.log(error));
    }
    // getNews();
    getEvents();
  }, [])

  return (
    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
      <div className="events-details latest-news">
        <div className="news-bg d-flex justify-content-between">
          <div>
            <h4 className="mb-0 font-semibold text-white">
              {strings.eventsAndUpdates}
            </h4>
          </div>
          <div className="my-auto">
            <Link href={'/static/EventsAndUpdate'}>
              <a className="text-white">{strings.Seeall}</a>
            </Link>
          </div>
        </div>

        <div className="scrollbar" id="style-8">
          <div className="force-overflow">
            {eventData.map((value, index) => (
              <Link
                key={index}
                href={{ pathname: "/static/Single-News-Event/", query: { id: value.id } }}>
                <div key={value.id} className="news-details cursor-pointer">
                  <span className="font-12">{value.event_date}</span>
                  <p className="font-14 font-medium mb-0">{value.title}</p>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
