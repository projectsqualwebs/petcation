import React from "react";
import {useRouter} from "next/router";
import Link from "next/link";
import {strings} from "../../public/lang/Strings";

type IProps = {
  name: string;
  descriptiom: string;
  image?: string,
  id: number
};

const PetSpotObject: React.FC<IProps> = (props: IProps) => {
  const router = useRouter();
  return (
    <div
      style={{ cursor: 'pointer',width: "247.5px", marginRight: "50px" }}
      className="owl-item "
    >
      <Link href={`/pet-spots/${props.id}`}>
      <div className="slider-item">
        <div className="slider-img">
          <img src={props.image ? props.image:"images/img2.jpg"} className="img-fluid" />
        </div>
        <div className="slider-content px-2">
          <div className="row">
            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
              <div className="exp-sitter-details py-2 text-truncate">
                <h6 className="mb-2 font-14 font-semibold text-truncate">{props.name}</h6>
              </div>
              <p className="exp-sitter-desc mb-3 font-10 line-2">{props.descriptiom}</p>
            </div>
          </div>
        </div>
        <div>
            <button className="btn btn-primary w-100">{strings.ViewSpot}</button>
        </div>
      </div>
      </Link>
    </div>
  );
};

export default PetSpotObject;
