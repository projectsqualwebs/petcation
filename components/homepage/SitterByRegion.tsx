import {useContext, useEffect, useState} from "react";
import API from "../../api/Api";
import AppContext from "../../utils/AppContext";
import {useRouter} from "next/router";
import {strings} from "../../public/lang/Strings";
import Geocode from "react-geocode";
import {GOOGLE_PLACES_API} from "../../api/Constants";

let api = new API();
export default function SitterByRegion() {
    const [cities, setCities] = useState<any>([]);
    const value: any = useContext(AppContext);
    const router = useRouter();

    useEffect(() => {
        Geocode.setApiKey(GOOGLE_PLACES_API);
        getCities()
    }, []);

    const getCities = () => {
        api
            .getCities(1)
            .then((json) => {
                setCities(json.data.response);
            })
            .catch((error) => {
                console.log(error);
            });
    };

  const searchByRegion = (lat, lng, region) => {
      Geocode.fromLatLng(lat, lng).then(
          (response) => {
              const address = response.results[0].formatted_address;
              if(address) {
                  let data = {
                      lat: String(lat),
                      lng: String(lng),
                      city : region,
                      defaultValue: region,
                  };
                  value.setState(data);
                  router.push({
                      pathname: "/search-sitter",
                  }).then(r => {});
              } else {
                  console.log(response)
              }
          },
          (error) => {
              console.error(error);
          }
      );
  };
    return (
        <div className="regions">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12 col-sm-8 col-lg-8 col-xl-8 mx-auto">
                        <div className="text-center main-title">
                            <h2 className="font-semibold">{strings.SearchSittersByRegion}</h2>
                        </div>
                    </div>
                </div>
                <div  className="row justify-content-center">
                    {cities.map((city, index)=>{
                        return(<>
                            {(index +1)%4 == 1 && <div className="p-2 region-col col-6 col-md-3 col-lg-3 col-xl-3">
                                <div className="booking-sitter rounded-lg px-1 py-2 sitter-region text-center">
                                    <ul className="mb-0">
                                        <li>
                                            <span className='cursor-pointer' onClick={()=>searchByRegion(city.latitude, city.longitude, city.name)}>{city.name}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>}
                        {(index +1)%4 == 2 && <div className="p-2 region-col col-6 col-md-3 col-lg-3 col-xl-3">
                            <div className="booking-sitter rounded-lg px-1 py-2 sitter-region text-center">
                                <ul className="mb-0">
                                    <li>
                                        <span className='cursor-pointer' onClick={()=>searchByRegion(city.latitude, city.longitude, city.name)}>{city.name}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>}
                        {(index +1)%4 == 3 && <div className="p-2 region-col col-6 col-md-3 col-lg-3 col-xl-3">
                            <div className="booking-sitter rounded-lg px-1 py-2 sitter-region text-center">
                                <ul className="mb-0">
                                    <li>
                                        <span className='cursor-pointer' onClick={()=>searchByRegion(city.latitude, city.longitude, city.name)}>{city.name}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>}
                        {(index +1)%4 == 0 && <div className="p-2 region-col col-6 col-md-3 col-lg-3 col-xl-3">
                            <div className="booking-sitter rounded-lg px-1 py-2 sitter-region text-center">
                                <ul className="mb-0">
                                    <li>
                                        <span className='cursor-pointer' onClick={()=>searchByRegion(city.latitude, city.longitude, city.name)}>{city.name}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>}</>)
                    })}
                </div>
            </div>
        </div>
    );
}
