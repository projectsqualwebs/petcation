import React, {useEffect, useState} from "react";
import { strings } from "../../public/lang/Strings";
import Link from 'next/link';
import moment from "moment";
const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;

type IProps = {
  key: number;
  data: {
    id: number;
    images: any;
    title_en: string;
    description_en: string;
    title_jp: string;
    description_jp: string;
    title_zh: string;
    description_zh: string;
    created_at: string;
    category: {
      name?: string;
      name_en?: string;
      name_jp?: string;
      name_zh?: string;
    };
  };
};

interface I_DATA {
  image: string;
  title: string;
  description: string;
  created_at: string;
  category: {
    name: string;
  };
}

const NewsRoomObject: React.FC<IProps> = (props: any) => {
  const [data, setData] = useState<I_DATA>()
  const [lang, setLang] = useState('en')

  useEffect( () => {
    let myLang =  localStorage.getItem('language')
    if (myLang) {
      setLang(myLang)
    }
  }, [])

  useEffect( () => {
    seLanguageContent(props?.data, lang)
  }, [props])

  const seLanguageContent = (myData, myLang) => {
    let engData = {
      image: myData?.images[0].path,
      title: myData?.title_en,
      description: myData?.description_en,
      created_at: myData?.created_at,
      category: {
        name: myData?.category?.name ?? myData?.category?.name_en,
      },
    }

    let jpData = {
      image: myData?.images[0].path,
      title: myData?.title_jp,
      description: myData?.description_jp,
      created_at: myData?.created_at,
      category: {
        name: myData?.category?.name ?? myData?.category?.name_jp,
      },
    }

    let zhData = {
      image: myData?.images[0].path,
      title: myData?.title_zh,
      description: myData?.description_zh,
      created_at: myData?.created_at,
      category: {
        name: myData?.category?.name ?? myData?.category?.name_zh,
      },
    }

    if (myLang == 'en') {
      setData(engData)
    }
    if (myLang == 'jp') {
      setData(jpData)
    }
    if (myLang == 'zh') {
      setData(zhData)
    }

  }
  return (
    <Link key={props.key+'link'} href={'/news/'+props.data.id}>
      <div key={props.key} className="events-details cursor-pointer m-1">
        <div className="event-img">
          <img src={data?.image ? IMAGE_BASE_URL+data?.image : "images/img2.jpg"} className="img-fluid" />
        </div>
        <div className="care-details">
          <div className="d-flex justify-content-between">
            <div className="care-btn ml-0">
              <h6 className="mb-0">{data?.category?.name}</h6>
            </div>
          </div>
        </div>
        <div className="event-title pb-0">
          <h6 className="font-semibold line-2">{data?.title}</h6>
          <p className="font-12">{data?.description?.substring(3)}</p>
          <div className="col-12 px-0">
            <div className="row align-items-center justify-content-between">
              <div className="col-auto">
                <a className="btn btn-primary border-btn px-3 py-1">
                  {strings.ReadMore}
                </a>
              </div>
              <div className="col-auto">
                <p className="m-0 font-12 text-black">{moment(data?.created_at).format("DD-MM-YYYY")}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default NewsRoomObject;
