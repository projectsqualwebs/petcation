import Link from "next/link";
import { useEffect, useState } from "react";
import API from "../../api/Api";
import { strings } from "../../public/lang/Strings";
import { Accordion } from "react-bootstrap";

const api = new API();

export default function FAQ() {
  const [faq, setFaq] = useState([]);
  useEffect(() => {
    const getAllFAQs = () => {
      api
        .getLandingPageFAQ()
        .then((response) => {
          setFaq(response.data.response);
        })
        .catch((error) => console.log(error));
    };
    getAllFAQs();
  }, []);

  const LongText = ({ content, limit, id }) => {
    if (content.length <= limit) {
      return <div>{content}</div>;
    }
    const toShow = content.substring(0, limit) + "...";
    return (
      <div>
        {toShow}
        <Link href={`/static/faq/single-faq/${id}`}>
          <a className="text-primary">{strings.ReadMore}</a>
        </Link>
      </div>
    );
  };
  return (
    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
      <div className="events-details latest-news">
        <div className="news-bg d-flex justify-content-between">
          <div>
            <h4 className="mb-0 font-semibold text-white">{strings.faqs}</h4>
          </div>
          <div className="my-auto">
            <Link href={"/static/Help/"}>
              <a className=" text-white">{strings.Seeall}</a>
            </Link>
          </div>
        </div>
        <div className="scrollbar" id="style-8">
          <div className="force-overflow">
           
              <Accordion>
                {faq.map((value, index) => (
                  <div key={index} className="panel-default availability mt-2">
                    <Accordion.Item eventKey={`${index}`} className="">
                      <Accordion.Header className="card ml-2 mr-2 mb-0 pb-0">
                        <h3 className="panel-title mb-0">
                          <a
                            className="collapsed my-0 py-0"
                            role="button"
                            title=""
                            data-toggle="collapse"
                            data-parent="#accordion"
                            // href={"#collapse" + value.id}
                            aria-expanded="true"
                            aria-controls={"collapse" + value.id}>
                            <Link href={`/static/faq/single-faq/${value.id}`}>
                              {value.title}
                            </Link>
                          </a>
                        </h3>
                      </Accordion.Header>
                      <Accordion.Body className="card mt-0 pt-0 ml-2 mr-2">
                        <div className="ml-2">
                        
                            <p>
                              <LongText
                                content={value.description}
                                limit={130}
                                id={value.id}
                              />{" "}
                            </p>
                     
                        </div>
                      </Accordion.Body>
                    </Accordion.Item>
                  </div>
                ))}
              </Accordion>
            </div>
      
        </div>
        {/**
        <div className="scrollbar" id="style-8">
          <div className="force-overflow">
            <div className="accordion">
              <div
                className="panel-group"
                id="accordion"
                role="tablist"
                aria-multiselectable="true">
                {faq.map((value, index) => (
                  <div key={index} className="panel panel-default">
                    <div
                      className="panel-heading"
                      role="tab"
                      id={"heading" + value.id}>
                      <h3 className="panel-title mb-0">
                        <a
                          className="collapsed"
                          role="button"
                          title=""
                          data-toggle="collapse"
                          data-parent="#accordion"
                          // href={"#collapse" + value.id}
                          aria-expanded="true"
                          aria-controls={"collapse" + value.id}>
                          <Link href={`/static/faq/single-faq/${value.id}`}>
                            {value.title}
                          </Link>
                        </a>
                      </h3>
                    </div>
                    <div
                      id={"collapse" + value.id}
                      className="panel-collapse collapse"
                      role="tabpanel"
                      aria-labelledby={"heading" + value.id}>
                      <div className="panel-body content">
                        <p>
                          <LongText
                            content={value.description}
                            limit={130}
                            id={value.id}
                          />{" "}
                        </p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
           
          </div>
          
        </div>
         */}
      </div>
    </div>
  );
}
