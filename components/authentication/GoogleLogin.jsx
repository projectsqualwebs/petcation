import React from "react";
import ReactGoogleLogin from "react-google-login";
import Router, { useRouter } from "next/router";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import {errorOptions, successOptions} from "../../public/appData/AppData";
import {deleteCookie} from "../../utils/Helper";
import {EventEmitter} from "../../public/EventEmitter";

export default function GoogleLogin() {
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [openError, closeError] = useSnackbar(errorOptions);
    const api = new API();
    const onSuccess = (res) => {
            api
                .socialLogin({ type: 2, google_id: res.googleId })
                .then((json) => {
                    let arr = [];
                    if(json.data.status == 200) {
                        // if (json.data.response?.email_verified_at == null) {
                        //     Router.push({
                        //         pathname: "/verify",
                        //         query: {userData: JSON.stringify(json.data.response), type:  1}
                        //     }, '/verify').then(r =>{})
                        // } else if (json.data.response?.phone_verified_at == null) {
                        //     Router.push({
                        //         pathname: "/verify",
                        //         query: {userData: JSON.stringify(json.data.response), type: 2}
                        //     }, '/verify').then(r => {
                        //     })
                        // } else {
                            document.cookie = `id=${json.data.response.id}; path=/`;
                            document.cookie = `token=${json.data.response.token}; path=/`;
                            document.cookie = `firstname=${json.data.response.firstname}; path=/`;
                            document.cookie = `lastname=${json.data.response.lastname}; path=/`;
                            document.cookie = `phone_number=${json.data.response.phone_number}; path=/`;
                            document.cookie = `profile_picture=${json.data.response.profile_picture}; path=/`;
                            document.cookie = `email=${json.data.response.email}; path=/`;
                            document.cookie = `get_recent_view=${arr}; path=/`;
                            // EventEmitter.dispatch('updateUserDetail')
                            Router.push("/user/dashboard").then((res) => openSuccess("Logged in successfully."));
                        // }
                    }

                })
                .catch((err) => {
                    openError(err.response.data.message)
                    router.push({pathname: '/signup', query: {google_id: res.googleId}}).then(r =>{})
                })
            // router.push({pathname:'/'})
    }
    const router = useRouter();
    const onFailure = (res) => {
        console.log(res);
    };
    const clientId = '26688619210-iu9ue0d3rk5j05l4rr5kjeoc8oe4h329.apps.googleusercontent.com';

    return (
        <ReactGoogleLogin
            clientId={clientId}
            render={renderProps => (
                <div className="col-12 col-md-6 col-lg-6 col-xl-6" onClick={renderProps.onClick}>
                    <div className="signup-content">
                          <a href="#" />
                            <div className="row social-icns">
                              <a href="#"></a>
                              <div className="col-auto my-auto">
                                <a href="#"></a>
                                <div className="social-img">
                                  <a href="#" />
                                  <a href="#">
                                    <img
                                      src="images/google-plus.png"
                                      className="img-fluid"
                                    />
                                  </a>
                                </div>
                              </div>
                              <div className="col pl-0 my-auto">
                                <h6 className="mb-0">
                                  {strings.ContinuewithGoogle}
                                </h6>
                              </div>
                            </div>
                    </div>
                </div>
            )}
            buttonText="Login"
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={'single_host_origin'}
        />
    );
}

