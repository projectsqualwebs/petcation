import React, {useState, useEffect} from "react";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { strings } from "../../public/lang/Strings";
import Router, {useRouter} from "next/router";
import {U_FACEBOOK_ID} from "../../api/Constants";
import {EventEmitter} from "../../public/EventEmitter";
import { useSnackbar } from "react-simple-snackbar";
import {errorOptions, successOptions} from "../../public/appData/AppData";
import API from "../../api/Api";


export default function LoginFacebook(props) {
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [openError, closeError] = useSnackbar(errorOptions);
    const api = new API();
    const onSuccess= (res) => {
        console.log('Login Success: currentUser:', res);
        if (res?.status === 'unknown') {
            openError(res.status)
        }
        else {
            api
                .socialLogin({ type: 1, facebook_id: res.userID })
                .then((json) => {
                    let arr = [];
                    console.log('login response',json.data.response)
                    if(json.data.status == 200) {
                        if (json.data.response?.email_verified_at == null) {
                            Router.push({
                                pathname: "/verify",
                                query: {userData: JSON.stringify(json.data.response), type:  2}
                            }, '/verify').then(r =>{})
                        } else if (json.data.response?.phone_verified_at == null) {
                            Router.push({
                                pathname: "/verify",
                                query: {userData: JSON.stringify(json.data.response), type: 2}
                            }, '/verify').then(r => {
                            })
                        } else {
                            document.cookie = `id=${json.data.response.id}; path=/`;
                            document.cookie = `token=${json.data.response.token}; path=/`;
                            document.cookie = `firstname=${json.data.response.firstname}; path=/`;
                            document.cookie = `lastname=${json.data.response.lastname}; path=/`;
                            document.cookie = `phone_number=${json.data.response.phone_number}; path=/`;
                            document.cookie = `profile_picture=${json.data.response.profile_picture}; path=/`;
                            document.cookie = `email=${json.data.response.email}; path=/`;
                            document.cookie = `get_recent_view=${arr}; path=/`;
                            // EventEmitter.dispatch('updateUserDetail')
                            Router.push("/user/dashboard").then((res) => openSuccess("Logged in successfully."));
                        }
                    }

                })
                .catch((err) => {
                    openError(err.response.data.message)
                    router.push({pathname:'/signup', query: {facebook_id: res.userID}})
                })
        }
    };

    const router = useRouter();
    

    const onFailure = (res) => {
        console.log('Login failed: res:', res);
    };
    const appId = U_FACEBOOK_ID;

    return (
        <FacebookLogin
            appId={appId}
            render={renderProps =>(
                <div className="col-12 col-md-6 col-lg-6 col-xl-6" onClick={renderProps.onClick}>
                    <div className="signup-content">
                        <a href="#" />
                        <div className="row social-icns">
                            <a href="#"></a>
                            <div className="col-auto my-auto">
                                <a href="#"></a>
                                <div className="social-img">
                                  <a href="#" />
                                  <a href="#">
                                    <img
                                      src="images/facebook.png"
                                      className="img-fluid"
                                    />
                                  </a>
                                </div>
                            </div>
                            <div className="col pl-0 my-auto">
                                <h6 className="mb-0">
                                  {strings.ContinuewithFacebook}
                                </h6>
                              </div>
                        </div>
                    </div>
                </div>

            )}
            autoLoad={false}
            fields="name,email,picture"
            callback={onSuccess}
            onFailure={onFailure}
        />
            );
}
