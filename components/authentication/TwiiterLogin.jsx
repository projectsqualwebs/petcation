import React, {useState} from "react";
import TwitterLogin from "react-twitter-login";
import {strings} from "../../public/lang/Strings";
import {U_TWITTER_API_KEY, U_TWITTER_SECRET_KEY} from "../../api/Constants";
import Router from "next/router";
import API from "../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import {errorOptions, successOptions} from "../../public/appData/AppData";



export default function ReactTwitterLogin() {
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [openError, closeError] = useSnackbar(errorOptions);
    const api = new API();

    const authHandler = (error, authData) => {
        //if (error) return console.error(error);
        console.log(error, authData?.twitterId);
        if (authData) {
            api
                .socialLogin({ type: 2, twitter_id:  authData?.twitterId})
                .then((json) => {
                    let arr = [];
                    console.log('login response',json.data.response)
                    if(json.data.status == 200) {
                        if (json.data.response?.email_verified_at == null) {
                            Router.push({
                                pathname: "/verify",
                                query: {userData: JSON.stringify(json.data.response), type:  1}
                            }, '/verify').then(r =>{})
                        } else if (json.data.response?.phone_verified_at == null) {
                            Router.push({
                                pathname: "/verify",
                                query: {userData: JSON.stringify(json.data.response), type: 2}
                            }, '/verify').then(r => {
                            })
                        } else {
                            // if (!json.data.response.email_verified_at) {
                            //   Router.push({pathname: "/verify", query: {userData: JSON.stringify(json.data.response), type: 1}},'/verify')
                            // } else if (!json.data.response.phone_verified_at) {
                            //   Router.push({pathname: "/verify", query: {userData: JSON.stringify(json.data.response),type: 2}}, '/verify')
                            // } else {
                            document.cookie = `id=${json.data.response.id}; path=/`;
                            document.cookie = `token=${json.data.response.token}; path=/`;
                            document.cookie = `firstname=${json.data.response.firstname}; path=/`;
                            document.cookie = `lastname=${json.data.response.lastname}; path=/`;
                            document.cookie = `phone_number=${json.data.response.phone_number}; path=/`;
                            document.cookie = `profile_picture=${json.data.response.profile_picture}; path=/`;
                            document.cookie = `email=${json.data.response.email}; path=/`;
                            document.cookie = `get_recent_view=${arr}; path=/`;
                            // EventEmitter.dispatch('updateUserDetail')
                            Router.push("/user/dashboard").then((res) => openSuccess("Logged in successfully."));
                            // }
                        }
                    }

                })
                .catch((err) => {
                    openError(err.response.data.message)
                    Router.push({pathname: '/signup', query: {twitter_id: authData?.twitterId}}).then(r => {})
                })
        }
    };
    const onFailed = (error) => {
        alert(error);
    };

    return (
        <>
            <TwitterLogin
                authCallback={authHandler}
                consumerKey={'wyEILxLECpduvwTFXIidr1G51'} // We created this, remember?
                consumerSecret={'GegJtvbAOPwi5SvkdCO9LMUSx5Oug2Gat6Gltv35hugghDIzPp'} // We created this, remember?
                buttonTheme={"dark"}
                onFailure={onFailed}
                callbackUrl={"http://localhost:3000/"}
                redirectUri={"http://localhost:3000/"}

                // requestTokenUrl={"http://localhost:3000/"}
                children={
                    <div className="signup-content">
                        <a href="#" />
                        <div className="row social-icns">
                            <a href="#"></a>
                            <div className="col-auto my-auto">
                                <a href="#"></a>
                                <div className="social-img">
                                    <a href="#" />
                                    <a href="#">
                                        <img
                                            src="images/twitter.png"
                                            className="img-fluid"
                                        />
                                    </a>
                                </div>
                            </div>
                            <div className="col pl-0 my-auto">
                                <h6 className="mb-0">
                                    {strings.ContinuewithTwitter}
                                </h6>
                            </div>
                        </div>
                    </div>
                }
            />
        </>
    );
}
