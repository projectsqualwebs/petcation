
import React from "react";
import { strings } from "../../../../public/lang/Strings";
import MeetingRequestObject, {I_MEETUP_REQUEST} from "./MeetingRequestObject";
import {useEffect, useState} from "react";
import API from "../../../../api/Api";
import Link from 'next/link';
import boolean from "async-validator/dist-types/validator/boolean";

let api = new API();

export default function MeetingRequests(isSitter:any) {
  const [myRequest, setMyRequest] = useState<I_MEETUP_REQUEST[]>([]);
  const [incomingRequest, setIncomingRequest] = useState<I_MEETUP_REQUEST[]>([]);
  const [requestByMe, setRequestByMe] = useState<boolean>(true);


  useEffect(()=>{
      getMeetupRequest(1);
      getMeetupRequest(2);
  },[])

  const getMeetupRequest = (type: number) => {
     api.getMeetupRequest(type).then((res)=>{
       if(res.data.status == 200){
         if(type == 1){
           setMyRequest(res.data.response)
         }else {
           setIncomingRequest(res.data.response)
         }
       }
     })
  }

  return (
    <div className="col-12 mb-4">
      <div className="bg-white main-background pb-1 h-100">
        <div className="col-12 mb-3 px-0">
          <div className="row align-items-center">
          <div className="col">
            <h6 className="mb-0 font-semibold">{strings.MeetingRequests}</h6>
          </div>
          <div className="col-auto alignment">
            <div className="view-all">
              <Link href="/chat">
                <a className="font-14">
                {strings.ViewAll}
                </a>
              </Link>
            </div>
          </div>
        </div>
        </div>
        <div className="pay-tabs dashboard-tab mb-3">
          <ul className="nav nav-tabs mb-0" id="myTab" role="tablist">
            <li className="nav-item" role="presentation">
              <a
                className={requestByMe === true ? "nav-link active" : "nav-link"}
                id="cards-tab1"
                data-toggle="tab"
                onClick={() => { setRequestByMe(true) }}
                role="tab"
              >
                {strings.Requestbyme}
              </a>
            </li>
            {isSitter.isSitter == true ? <li className="nav-item" role="presentation">
              <a
                className={requestByMe !== true ? "nav-link active" : "nav-link"}
                id="bank-tab1"
                data-toggle="tab"
                onClick={() => { setRequestByMe(false) }}
                role="tab"
              >
                {strings.Requestforme}
              </a>
            </li> : null}
          </ul>
        </div>
        <div className="tab-content" id="myTabContent">
          {requestByMe === true ? <div className="tab-pane fade show active">
              {myRequest.length > 0 ? myRequest.map((value, index) => (
                <MeetingRequestObject key={index} type={true} request={value} />
              )):<div className="col-12 text-center padding">
                <p className="font-13 mb-0 font-italic text-center">{strings.noNewRequest}</p>
              </div>}
          </div>
          :
          <div className="tab-pane fade show active">
              {incomingRequest.length > 0 ? incomingRequest.map((value,index) => (
                <MeetingRequestObject key={index} type={false} request={value} updateRequest={(type)=> type ? getMeetupRequest(1):getMeetupRequest(2)} />
              )):<div className="col-12 text-center padding">
                <p className="font-13 mb-0 font-italic">{strings.noRequest}</p>
                </div>}
          </div>}
        </div>
      </div>
    </div>
  );
}
