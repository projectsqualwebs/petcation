import React from "react";
import { strings } from "../../../../public/lang/Strings";
import {I_SINGLE_SITTER} from "../../../../models/sitter.interface";
import moment from "moment";
import { useSnackbar } from "react-simple-snackbar";
import { errorOptions, successOptions } from "../../../../public/appData/AppData";
import API from "../../../../api/Api";
import {Dropdown, DropdownButton} from "react-bootstrap";

export type I_USER = {
 id: number;
 firstname: string;
 lastname: string;
 profile_image: string;
    address: {
        id: number;
        live_in_house: number;
        non_smoking_household: number;
        no_children_present: number;
        fenced_yard: number;
        dog_other_pets: number;
    }
}

export type I_MEETUP_REQUEST = {
  chat_line_id: number;
  chat_thread_id: number;
  date: string;
  time: string;
  id: number;
  location: string;
  message: string;
  requested_by: I_USER;
  requested_for: I_USER;
  status: number;
};

interface IProps {
  request: I_MEETUP_REQUEST;
  type: boolean;
  updateRequest?: any
}

let api = new API();

const MeetingRequestObject: React.FC<IProps> = ({ request, type,updateRequest }: IProps) => {
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);


  const updateMeetupStatus = (id: number,status:number) => {
    let data = {
      meet_up_id: id,
      status: status
    };
    api
        .updateMeetupStatus(data)
        .then((res) => {
             if(res.data.status == 200){
                  openSuccess(strings.SuccessfullyUpdatedStatus);
                   updateRequest(type);
             }else {
                  openError(strings.errorUpdatingStatus);
             }
        })
        .catch((error) => openError(strings.errorUpdatingStatus));
  }

  return (
      <>
          <div className="booking-request-details request-padding">
              <div className="col-12">
                  <div className="row align-items-center">
                      <div className="col-auto meeting-day text-center">
                          <div>
                              <p className="text-white mb-0 font-8">{moment(request.date).format("ddd, DD'MMM YYYY")}</p>
                              <p className="text-white mb-0 font-8">{request?.time}</p>
                          </div>
                      </div>
                      <div className="col pl-1">
                          <div className="row align-items-center">
                              <div className="col">
                                  <div className="meeting-details ml-2">
                                      <h6 className="mb-2 font-12 font-medium line-2">
                                          {request.message}
                                      </h6>
                                      <p className="font-10 mb-0">
                                          <b>{strings.Client + ":"}</b> <span>{type ? (request.requested_for.firstname + ' ' + request.requested_for.lastname):(request.requested_by.firstname + ' ' + request.requested_by.lastname)}</span>
                                      </p>
                                  </div>
                              </div>
                              <div className="col-auto p-0 column-left alignment">
                                  <div>
                                      {!type ? (
                                          request.status == 0 ?
                                              <div className="dropdown pet-drop">
                                                  <DropdownButton
                                                      className='bg-transparent'
                                                      align="end"
                                                      title={<a className="menu dropdown-toggle">
                                                          <div className="ellipse">
                                                              <svg
                                                                  aria-hidden="true"
                                                                  focusable="false"
                                                                  data-prefix="fal"
                                                                  data-icon="ellipsis-h"
                                                                  role="img"
                                                                  xmlns="http://www.w3.org/2000/svg"
                                                                  viewBox="0 0 320 512"
                                                                  className="svg-inline--fa fa-ellipsis-h fa-w-10 fa-2x"
                                                              >
                                                                  <path
                                                                      fill="currentColor"
                                                                      d="M192 256c0 17.7-14.3 32-32 32s-32-14.3-32-32 14.3-32 32-32 32 14.3 32 32zm88-32c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zm-240 0c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"
                                                                      className=""
                                                                  ></path>
                                                              </svg>
                                                          </div>
                                                      </a>}
                                                      id="dropdown-menu-align-end">
                                                      <Dropdown.Item onClick={()=> updateMeetupStatus(request.id, 1)}>
                                                          <a>
                                                              {strings.Accept}
                                                          </a>
                                                      </Dropdown.Item>
                                                      <Dropdown.Item onClick={()=> updateMeetupStatus(request.id, 2)}>
                                                          <a>
                                                              {strings.Reject}
                                                          </a>
                                                      </Dropdown.Item>
                                                  </DropdownButton>
                                              </div>: null
                                      ) : (
                                          <div className="dropdown pet-drop">
                                              <a className="menu dropdown-toggle">
                                                  <div className="ellipse tick-icon mr-0">
                                                      <svg
                                                          aria-hidden="true"
                                                          focusable="false"
                                                          data-prefix="far"
                                                          data-icon="check"
                                                          role="img"
                                                          xmlns="http://www.w3.org/2000/svg"
                                                          viewBox="0 0 512 512"
                                                          className="svg-inline--fa fa-check fa-w-16 fa-2x"
                                                      >
                                                          <path
                                                              fill="currentColor"
                                                              d="M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z"
                                                          />
                                                      </svg>
                                                  </div>
                                              </a>
                                          </div>
                                      )}
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div className="col-12 px-0">
                          <hr className="mb-md-3 mb-2"/>
                          <p className="mb-0 font-10 ">
                              <b>{strings.Meetat}:</b> {request.location}
                          </p>
                      </div>
                  </div>
              </div>
          </div>
      </>
  );
};

export default MeetingRequestObject;
