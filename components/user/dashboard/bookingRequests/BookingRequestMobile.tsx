import React, {useRef} from "react";
import {errorOptions, requestsArray} from "../../../../public/appData/AppData";
import { strings } from "../../../../public/lang/Strings";
import BookingRequestMobileObject from "./BookingRequestMobileObject";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useState } from "react";
import API from "../../../../api/Api";
import I_SITTER_RESERVATION from "../../../../models/sitterReservations.interface";
import Loader from "../../../common/Loader";
import { useSnackbar } from 'react-simple-snackbar';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import {petSpotsSlider, requestSlider} from "../../../../public/SliderResponsive";
import Link from "next/link";

/**
 *
 * @param isSitter contains boolean value to differentiate between sitter and pet owner.
 * @function booking request object for mobile view.
 */
const BookingRequestMobile = (isSitter: any) => {
    var carousel = useRef(null);
  const api = new API();
  const router = useRouter();

  const [userReservations, setUserReservations] = useState<
      I_SITTER_RESERVATION[]
      >([]);
  const [sitterReservations, setSitterReservations] = useState<
      I_SITTER_RESERVATION[]
      >([]);
  const [uLoader, setULoader] = useState<boolean>(true);
  const [sLoader, setSLoader] = useState<boolean>(true);
  const [bookingByMe, setBookingByMe] = useState<boolean>(true);
  const [bankAdded, setBankAdded] = useState<boolean>(false);
  const [openError, closeError] = useSnackbar(errorOptions);

  /**
   * to initialize data before page load.
   */
  useEffect(() => {
    getUserReservations();
    getSitterReservations();
    getBankAccounts();
  }, []);

  /**
   * function ->
   *    success-> get all bank accounts.
   *    failure-> return error.
   */
  const getBankAccounts = () => {
    api
        .getAllBankAccount()
        .then((json) => {
          if (json.data.response && json.data.response.length) {
            setBankAdded(true);
          }
        })
        .catch((error) => {
          if (error.response && error.response.data) {
            openError(error.response.data.message)
          }
        });
  };

  /**
   * @function to get user reservations via API call
   */
  const getUserReservations = () => {
    let data = JSON.stringify({
      keyword: "",
      status: 0,
    });
    api
        .getUserReservations(data, 1)
        .then((response) => {
          setUserReservations(response.data.response.data);
          setULoader(false);
        })
        .catch((error) => console.log(error));
  };

  /**
   * @function to get sitter reservations via API call
   */
  const getSitterReservations = () => {
    let data = JSON.stringify({
      keyword: "",
      status: 0,
    });
    api
        .getSitterReservetions(data, 1)
        .then((response) => {
          setSitterReservations(response.data.response.data);
          setSLoader(false);
        })
        .catch((error) => setSLoader(false));
  };

  /**
   * function to accept request.
   * @param id to provide identiity of a perticular request.
   * @success
   *    success -> Request accepted.
   *    faliure -> show error and request will not accepted.
   */
  const acceptRequest = (id: any) => {
    setSLoader(true);
    let data = JSON.stringify({
      request_id: id,
      status: 1,
      reason: ''
    });
    api
        .changeRequestStatus(data)
        .then((json) => {
          getSitterReservations();
        })
        .catch((error) => {
          setSLoader(false);
        });
  };
  /**
   * funtion to withdraw the request.
   * @param id  to provide identity of a perticular request.
   * @param data  contains parameter for which we withdraw the request.
   * @success
   *    success -> withdraw booking.
   *    faliure -> booking will exist.
   */
  const withdrawRequest = (id: any, data: any) => {
    setULoader(true);
    api
        .cancelRequest(id, {
          pets: data.userPets,
          additional_services : data.additionalServices,
          reason: data?.reason,
        })
        .then((json)=> {
          getUserReservations();
        })
        .catch((error)=> setSLoader(false))
  };
  /**
   * funtion to reject the request.
   * @param id  to provide id of a perticular request.
   * @param data  contains parameter for which we reject the request.
   * @success
   *    success -> reject booking.
   *    faliure -> booking will exist.
   */
  const RejectRequest = (id: any) => {
    setSLoader(true);
    let data = JSON.stringify({
      request_id: id,
      status: 2,
      reason: ''
    });
    api
        .changeRequestStatus(data)
        .then((json) => {
          getSitterReservations();
        })
        .catch((error) => {
          setSLoader(false);
        });
  };

  return (
    <div className="d-block mb-4">
        <div className="bg-white main-background dash-request pb-1 h-100">
          <div className="col-12 px-0 mb-md-3 mb-0">
            <div className="row align-items-center">
              <div className="col">
                <h6 className="mb-0 font-semibold">
                  {strings.LatestBookingRequests}
                </h6>
              </div>
              <div className="col-auto alignment">
                <div className="view-all">
                  <Link
                    href={isSitter.isSitter == true ? "/user/reservation?index=1" : "/user/reservation?index=2"}>
                    <a className="font-14">

                    {strings.ViewAll}
                  </a></Link>
                </div>
              </div>
            </div>
          </div>
          <div className="pay-tabs dashboard-tab">
            <ul className="nav nav-tabs mb-0" id="myTab4" role="tablist">
              <li className="nav-item" role="presentation">
                <a
                    className={bookingByMe === true ? "nav-link active" : "nav-link"}
                    id="mob-home-tab1"
                    data-toggle="tab"
                    onClick={() => { setBookingByMe(true) }}
                    role="tab"
                    aria-selected="true"
                >
                  {strings.Bookingbyme}
                </a>
              </li>
              {isSitter.isSitter == true ? <li className="nav-item" role="presentation">
                <a
                    className={bookingByMe !== true ? "nav-link active" : "nav-link"}
                    id="mob-profile-tab1"
                    data-toggle="tab"
                    onClick={() => { setBookingByMe(false) }}
                    role="tab"
                    aria-selected="false"
                >
                  {strings.Bookingforme}
                </a>
              </li> : null}
            </ul>
          </div>
            <div className="tab-content">
                {bookingByMe === true ? <div className="tab-pane fade active show">
                        {uLoader ? (
                            <Loader />
                        ) : (
                            <div className="card-slider-spot">
                                <div className="row carousel-slider">
                                    <div className="col-12 px-0">
                                        {userReservations.length ? <div
                                            className="owl-carousel owl-loaded owl-drag"
                                            id="featured-slider2"
                                        >
                                            <Carousel
                                                ref={carousel}
                                                swipeable={true}
                                                draggable={false}
                                                autoPlay={false}
                                                showDots={false}
                                                ssr={true}
                                                renderDotsOutside={false}
                                                arrows={false}
                                                className={"owl-carousel"}
                                                responsive={requestSlider}
                                            >
                                                {
                                                    userReservations.map((value, index) => (
                                                        <BookingRequestMobileObject
                                                            key={index}
                                                            asSitter={false}
                                                            request={value}
                                                            withdrawRequest={(id, data) => withdrawRequest(id, data)}
                                                            bankAdded={bankAdded}
                                                        />))
                                                }
                                            </Carousel>
                                            <div className="owl-nav disabled">
                                                <div className="owl-prev">
                                                    <div className="prev">
                                                        <svg
                                                            onClick={() => carousel.current.previous()}
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fal"
                                                            data-icon="arrow-circle-left"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512"
                                                            className="svg-inline--fa fa-arrow-circle-left fa-w-16 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zM256 472c-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216zm-12.5-92.5l-115.1-115c-4.7-4.7-4.7-12.3 0-17l115.1-115c4.7-4.7 12.3-4.7 17 0l6.9 6.9c4.7 4.7 4.7 12.5-.2 17.1L181.7 239H372c6.6 0 12 5.4 12 12v10c0 6.6-5.4 12-12 12H181.7l85.6 82.5c4.8 4.7 4.9 12.4.2 17.1l-6.9 6.9c-4.8 4.7-12.4 4.7-17.1 0z"
                                                            />
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div className="owl-next">
                                                    <div className="next">
                                                        <svg
                                                            onClick={() => carousel.current.next()}
                                                            aria-hidden="true"
                                                            focusable="false"
                                                            data-prefix="fal"
                                                            data-icon="arrow-circle-right"
                                                            role="img"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512"
                                                            className="svg-inline--fa fa-arrow-circle-right fa-w-16 fa-2x"
                                                        >
                                                            <path
                                                                fill="currentColor"
                                                                d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zM256 40c118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216zm12.5 92.5l115.1 115c4.7 4.7 4.7 12.3 0 17l-115.1 115c-4.7 4.7-12.3 4.7-17 0l-6.9-6.9c-4.7-4.7-4.7-12.5.2-17.1l85.6-82.5H140c-6.6 0-12-5.4-12-12v-10c0-6.6 5.4-12 12-12h190.3l-85.6-82.5c-4.8-4.7-4.9-12.4-.2-17.1l6.9-6.9c4.8-4.7 12.4-4.7 17.1 0z"
                                                            />
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-dots disabled" />
                                        </div>
                                            : <div className="col-12">
                      <div className="col-12 bg-white p-3 py-md-5 py-4 text-center">
                                                <p className="font-13 mb-0 font-italic w-100 text-center">{strings.noNewReservation}</p>
                    </div>
                                            </div>}
                                        <div className="slider_nav"></div>
                                    </div>
                                </div>
                            </div>)}
                    </div> :
                    <div className="tab-pane fade active show">
                        {sLoader ? (
                            <Loader />
                        ) : (
                            <div className="card-slider-spot">
                                <div className="row carousel-slider">
                                    <div className="col-12 px-0">
                                        {sitterReservations.length ? <div
                                                className="owl-carousel owl-loaded owl-drag"
                                                id="featured-slider2"
                                            >
                                                <Carousel
                                                    ref={carousel}
                                                    swipeable={true}
                                                    draggable={false}
                                                    autoPlay={false}
                                                    showDots={false}
                                                    ssr={true}
                                                    renderDotsOutside={false}
                                                    arrows={false}
                                                    className={"owl-carousel"}
                                                    responsive={requestSlider}
                                                >
                                                    {
                                                        sitterReservations.map((value, index) => (
                                                            <BookingRequestMobileObject
                                                                key={index}
                                                                acceptRequest={acceptRequest}
                                                                asSitter={true}
                                                                request={value}
                                                                rejectRequest={RejectRequest}
                                                                bankAdded={bankAdded}
                                                            />))
                                                    }
                                                </Carousel>
                                                <div className="owl-nav disabled">
                                                    <div className="owl-prev">
                                                        <div className="prev">
                                                            <svg
                                                                onClick={() => carousel.current.previous()}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-left"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-left fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zM256 472c-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216zm-12.5-92.5l-115.1-115c-4.7-4.7-4.7-12.3 0-17l115.1-115c4.7-4.7 12.3-4.7 17 0l6.9 6.9c4.7 4.7 4.7 12.5-.2 17.1L181.7 239H372c6.6 0 12 5.4 12 12v10c0 6.6-5.4 12-12 12H181.7l85.6 82.5c4.8 4.7 4.9 12.4.2 17.1l-6.9 6.9c-4.8 4.7-12.4 4.7-17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div className="owl-next">
                                                        <div className="next">
                                                            <svg
                                                                onClick={() => carousel.current.next()}
                                                                aria-hidden="true"
                                                                focusable="false"
                                                                data-prefix="fal"
                                                                data-icon="arrow-circle-right"
                                                                role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512"
                                                                className="svg-inline--fa fa-arrow-circle-right fa-w-16 fa-2x"
                                                            >
                                                                <path
                                                                    fill="currentColor"
                                                                    d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zM256 40c118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216zm12.5 92.5l115.1 115c4.7 4.7 4.7 12.3 0 17l-115.1 115c-4.7 4.7-12.3 4.7-17 0l-6.9-6.9c-4.7-4.7-4.7-12.5.2-17.1l85.6-82.5H140c-6.6 0-12-5.4-12-12v-10c0-6.6 5.4-12 12-12h190.3l-85.6-82.5c-4.8-4.7-4.9-12.4-.2-17.1l6.9-6.9c4.8-4.7 12.4-4.7 17.1 0z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="owl-dots disabled" />
                                            </div>
                                            : <div className="col-12">
                                                <div className="col-12 bg-white p-3 py-md-5 py-4 text-center">
                                                    <p className="font-13 mb-0 font-italic w-100 text-center">{strings.noNewReservation}</p>
                                                </div>
                                            </div>}
                                        <div className="slider_nav"></div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>}
            </div>
        </div>
    </div>
  );
};

export default BookingRequestMobile;
