import React from "react";
import {Props} from "react-select";
import {strings} from "../../../../public/lang/Strings";
import I_SITTER_RESERVATION from "../../../../models/sitterReservations.interface";
import {confirmAlert} from "react-confirm-alert";
import 'react-confirm-alert/src/react-confirm-alert.css';
import API from "../../../../api/Api";
import {useState} from "react";
import {errorOptions} from "../../../../public/appData/AppData";
import {useSnackbar} from 'react-simple-snackbar';
import TransportFeeModal from "../../../common/TransporFeeModal";
import {
    I_CHAT_BOOKING,
} from "../../../../models/chat.interface";
import {AxiosResponse} from "axios";
import Res from "../../../../models/response.interface";
import RequestCancelModal from "../../../common/RequestCancelModal";
import {getCurrencySign} from "../../../../api/Constants";
import RequestViewModal from "../../../common/RequestViewModal";
import moment from "moment";
import ChatBookingCountdown from "../../../chat/ChatBookingCountdown";


interface IProps {
    request: I_SITTER_RESERVATION;
    asSitter: boolean;
    acceptRequest?: (id: number) => void;
    withdrawRequest?: (id: number, data: any) => void;
    rejectRequest?: (id: number) => void;
    bankAdded: boolean;
}

/**
 *
 * @param param0 all parameters contains data, identification
 * @function acceptRequest to accept request
 * @function withdrawRequest to withdraw request.
 * @function rejectRequest to reject request.
 * @returns dynamic view of our page.
 */
const BookingRequestMobileObject: React.FC<IProps> = ({
                                                          request,
                                                          asSitter,
                                                          acceptRequest,
                                                          withdrawRequest,
                                                          rejectRequest, bankAdded
                                                      }: IProps) => {
    let api = new API();
    const key = asSitter ? "user" : "sitter";
    const [showModal, setShowModal] = useState<any>();
    const [showCancelModal, setShowCancelModal] = useState<any>();
    const [bookingId, setBookingId] = useState<any>();
    const [bookingService, setBookingService] = useState<any>();
    const [bookingServiceId, setBookingServiceId] = useState<any>();
    const [openError, closeError] = useSnackbar(errorOptions);
    const [openSnackbar] = useSnackbar();
    const [viewModal, setViewModal] = useState<boolean>(false);

    //Api call for add transport fee
    /**
     *
     * @param amount contains amount of transportation charges
     * function ->
     *    success -> add transport charge and accept request
     *    failure -> return error
     */
    const handleAddCharge = (amount: any) => {
        if (!amount) {
            openSnackbar("Enter amount")
        } else {
            api
                .addTransportChargeRequest({request_id: bookingId, amount: amount})
                .then((res: AxiosResponse<Res<I_CHAT_BOOKING[]>>) => {
                    openSnackbar(res.data.message)
                    setShowModal(false)
                    acceptRequest(request.id)
                })
                .catch((error) => {
                    if (error.response && error.response.data) {
                        openError(error.response.data.message)
                    }
                });
        }
    }

    /**
     * fuction for confirmation alert
     * @param request provide information for reject request
     * trigger to open confirmaion popup
     * success ->
     *    two options yes or no
     *      yes -> will reject request
     *      no -> do nothing and close popup
     */
    const confirmRejectRequest = (request) => {
        confirmAlert({
            closeOnClickOutside: true,
            customUI: ({onClose}) => {
                return (
                    <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
                        <div className="react-confirm-alert-body">
                            <h1>{strings.RejectRequest_Q}</h1>
                            <p>{strings.areYouSure}</p>
                            <div className="react-confirm-alert-button-group">
                                <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                                    rejectRequest(request.id)
                                    onClose();
                                }}>{strings.Yes}</button>
                                <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                                    onClose();
                                }}>{strings.No}</button>
                            </div>
                        </div>
                    </div>
                );
            }
        });
    };

    const has12HoursPassed = (updatedAt) => {
        const updatedAtMoment = moment(updatedAt);
        const now = moment();
        const duration = moment.duration(now.diff(updatedAtMoment));
        return duration.asHours() >= 12;
    };

    console.log("request",request.updated_at)

    return (
        <div className="col-12 dash-col dash-col1 p-3 w-100">
            <div
                className="booking-request-details request-padding"
                style={{borderRadius: "0"}}
            >
                <div className="col-12">
                    <div className="row align-items-center">
                        <div className="col-auto p-0 search-sitter-img"
                             >
                            <img src={request[key].profile_picture} className="img-fluid rounded-circle"/>
                        </div>
                        <div className="col booking-title cursor-pointer" onClick={() => setViewModal(true)}>
                            <h6 className="font-14 mb-0 font-medium">
                                {request[key].firstname + " - " + request.service.name}
                            </h6>
                            {request[key].address ? <p className="font-10 mb-0 line-2 w-100">{`${strings.Address +
                            ":" +
                            request[key].address.address +
                            " " +
                            request[key].address.city.name +
                            " " +
                            request[key].address.postcode
                            }`}</p> : null}
                            {!has12HoursPassed(request.updated_at) &&
                            <p className="font-10 mb-0">
                                <ChatBookingCountdown time={request.updated_at}/>
                            </p>
                            }
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-between my-2">
                    <div className="from-details">
                        <p className="font-10 mb-0">{strings.From}</p>
                        <p className="font-12 font-semibold mb-0">{moment(new Date(request.drop_of_date)).format("MM/DD/YYYY")}</p>
                    </div>
                    <div className="to-details">
                        <p className="font-10 mb-0">{strings.To}</p>
                        <p className="font-12 font-semibold mb-0">{moment(new Date(request.pickup_up_date)).format("MM/DD/YYYY")}</p>
                    </div>
                </div>
                <div className="row align-items-center">
                    <div className="col">
                        <div className="total-details">
                            <p className="font-10 mb-0">{strings.BookingAmount}</p>
                            <p className="font-12 font-semibold mb-0">{getCurrencySign() + request.total_paid_amount}</p>
                        </div>
                    </div>
                    <div className="col-auto">
                        <div className="row justify-content-end">
                            {!has12HoursPassed(request.updated_at) &&
                            <div className="col-auto pl-0 pr-2">
                                {asSitter ? (request?.need_sitter_pickup === 1 ? <button
                                        onClick={() => {
                                            if (bankAdded) {
                                                setBookingId(request.id);
                                                setBookingService(request.service.name);
                                                setBookingServiceId(request.service.id);
                                                setShowModal(true);
                                                acceptRequest = (acceptRequest)
                                            } else {
                                                openError(strings.PleaseAddYourBankAccountDetailsBeforeAcceptingTheBooking)
                                            }
                                        }}
                                        className="btn btn-primary btn1">
                                        {strings.Accept}
                                    </button> : <button
                                        onClick={() => {
                                            if (bankAdded) {
                                                acceptRequest(request.id)
                                            } else {
                                                openError(strings.PleaseAddYourBankAccountDetailsBeforeAcceptingTheBooking)
                                            }
                                        }}
                                        className="btn btn-primary btn1">
                                        {strings.Accept}
                                    </button>) :
                                    <button
                                        onClick={() => setShowCancelModal(true)}
                                        className="btn btn-primary btn1">
                                        {strings.Withdraw}
                                    </button>}
                            </div>}

                            {has12HoursPassed(request.updated_at) &&
                            <div className="col-auto pl-0 pr-2">
                                <button className=" no-pointer btn btn-primary btn1 mob-icon-btn">
                                    <span className="d-none d-md-block">{strings.TimeOut}</span>
                                </button>
                            </div>
                            }

                            {!has12HoursPassed(request.updated_at) &&
                            <div className="col-auto pl-0">
                                {asSitter ? <button
                                    onClick={() => {
                                        confirmRejectRequest(request)
                                    }
                                    }
                                    className="btn btn-primary btn1 mob-icon-btn">
                                    <span className="d-none d-md-block">{strings.Reject}</span>
                                    <span className="d-block d-md-none">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" strokeWidth="2" fill="none"
                         strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1"><line x1="18" y1="6" x2="6"
                                                                                                   y2="18"></line><line
                        x1="6" y1="6" x2="18" y2="18"></line></svg>
                  </span>
                                </button> : <label>{strings.Pending}...</label>}
                            </div>}

                        </div>
                    </div>
                </div>
                <small onClick={() => setViewModal(true)}
                       className="text-primary cursor-pointer"
                >{strings.viewDetails}</small>
            </div>
            <RequestViewModal showModal={viewModal} hideModal={() => setViewModal(false)} request={request}
                              asSitter={asSitter}/>
            <RequestCancelModal showModal={showCancelModal} hideModal={() => setShowCancelModal(false)}
                                request={request} paid={false}
                                withdrawRequest={(id, data) => withdrawRequest(id, data)}/>
            <TransportFeeModal bookingServiceId={bookingServiceId} bookingService={bookingService} id={bookingId}
                               showModal={showModal} hideModal={() => setShowModal(false)}
                               handleAddCharge={(amount) => handleAddCharge(amount)}/>
        </div>
    );
};

export default BookingRequestMobileObject;
