import { strings } from "../../../public/lang/Strings";
import React, {useEffect, useState} from "react";
import {AxiosError, AxiosResponse} from "axios";
import Res from "../../../models/response.interface";
import IDashboardData from "../../../models/dashboard.interface";
import API from "../../../api/Api";
import {getCurrencySign} from "../../../api/Constants";
import {Dropdown, DropdownButton} from "react-bootstrap";

type states = {
  bookingForMe: number;
  pBookingForMe: number;
  bookingByMe: number;
  pBookingByMe: number;
  totalEarning: number;
  pTotalEarning: number;
  totalReferral: number;
  pTotalReferral: number;
};

interface IProps {
  states: states;
  isSitter: boolean;
  dashboard: any;
}

const api = new API();

const StatusBar: React.FC<IProps> = ({ states, isSitter, dashboard }: IProps) => {
  const [dashboardData, setDashboardData] = useState(null);
  const [selectedBBM, setSelectedBBM] = useState(2);
  const [selectedBTM, setSelectedBTM] = useState(2);
  const [earning, setEarnings] = useState(2);
  const [referral, setReferral] = useState(2);

  useEffect(() => {
    setDashboardData(dashboard)
  },[dashboard])

  const getLastPercentageEarning = (earning, total_percentage_daily, total_percentage_weekly, total_percentage_monthly, total_percentage_yearly) => {
    if (earning == 1 && total_percentage_daily > 0) {
      return <p className={`green mb-0 font-10`}>{total_percentage_daily + strings.P_fromYesterday}</p>;
    }
    else if (earning == 1 && total_percentage_daily === 0) {
      return <p className={`mb-0 font-10`}>{total_percentage_daily + strings.P_fromYesterday}</p>;
    }
    else if (earning == 1 && total_percentage_daily < 0) {
      return <p className={`red mb-0 font-10`}>{total_percentage_daily + strings.P_fromYesterday}</p>;
    }
    else if (earning == 2 && total_percentage_weekly > 0) {
      return <p className={`green mb-0 font-10`}>{total_percentage_weekly + strings.P_fromLastWeek}</p>;
    }
    else if (earning == 2 && total_percentage_weekly === 0) {
      return (<p className={`mb-0 font-10`}>{total_percentage_weekly + strings.P_fromLastWeek}</p>)
    }
    else if (earning == 2 && total_percentage_weekly < 0) {
      return <p className={`red mb-0 font-10`}>{total_percentage_weekly + strings.P_fromLastWeek}</p>;
    }
    else if (earning == 3 && total_percentage_monthly > 0) {
      return <p className={`green mb-0 font-10`}>{total_percentage_monthly + strings.P_fromLastMonth}</p>;
    }
    else if (earning == 3 && total_percentage_monthly === 0) {
      return <p className={`mb-0 font-10`}>{total_percentage_monthly + strings.P_fromLastMonth}</p>;
    }
    else if (earning == 3 && total_percentage_monthly < 0) {
      return <p className={`red mb-0 font-10`}>{total_percentage_monthly + strings.P_fromLastMonth}</p>;
    }
    else if (earning == 4 && total_percentage_yearly > 0) {
      return <p className={`green mb-0 font-10`}>{total_percentage_yearly + strings.P_fromLastYear}</p>;
    }
    else if (earning == 4 && total_percentage_yearly === 0) {
      return <p className={`mb-0 font-10`}>{total_percentage_yearly + strings.P_fromLastYear}</p>;
    }
    else if (earning == 4 && total_percentage_yearly < 0) {
      return <p className={`red mb-0 font-10`}>{total_percentage_yearly + strings.P_fromLastYear}</p>;
    }
  }

  return (
    <div className="col-12 px-0 mt-4 mt-md-0">
      <div className="row dash-row">
        <div className={`col-md-4 col-lg-4 pr-md-2 dash-col ${isSitter == true ? "col-xl" : "col-xl"}`}>
          <div className="bg-white main-background">
            <div className="week-details">
              <div>
                <div className="total-booking d-none d-md-block d-lg-block d-xl-block">
                  <p className="font-14 mb-2">{strings.TotalEarnings}</p>
                </div>
                <div className="total-booking d-block d-md-none d-lg-none d-xl-none">
                  <p className="font-14 mb-2">{strings.TotalEarnings}</p>
                </div>
              </div>
              <div className="total-number">
                <div className="display">
                  <div>
                    <h3 className=" font-semibold">{getCurrencySign() + (dashboardData ? (earning == 1) ? dashboardData?.statics?.paid_amount_to_me_daily : (earning == 2) ? dashboardData?.statics?.paid_amount_to_me_weekly : (earning == 3) ? dashboardData?.statics?.paid_amount_to_me_monthly : (dashboardData?.statics?.paid_amount_to_me_yearly ?? 0) :0)}</h3>
                  </div>
                  <DropdownButton
                      className='bg-transparent'
                      align="end"
                      title={earning == 1 ? strings.Daily : earning == 2 ? strings.Weekly : earning == 3 ? strings.Monthly : strings.Yearly}
                      id="dropdown-menu-align-end"
                  >
                    <Dropdown.Item eventKey="0" onClick={()=>setEarnings(1)}>
                        {strings.Daily}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="1" onClick={()=>setEarnings(2)}>
                      {strings.Weekly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="2" onClick={()=>setEarnings(3)}>
                      {strings.Monthly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="3" onClick={()=>setEarnings(4)}>
                      {strings.Yearly}
                    </Dropdown.Item>
                  </DropdownButton>
                </div>
                {getLastPercentageEarning(earning, dashboardData?.statics?.total_earning_percentage_daily,dashboardData?.statics?.total_earning_percentage_weekly,dashboardData?.statics?.total_earning_percentage_monthly,dashboardData?.statics?.total_earning_percentage_yearly)}

              </div>
            </div>
          </div>
        </div>
        <div className={`col-md-4 col-lg-4 px-md-2 dash-col ${isSitter == true ? "col-xl-auto" : "col-xl-auto"}`}>
          <div className="bg-white main-background">
            <div className="week-details">
              <div>
                <div className="total-booking d-none d-md-block d-lg-block d-xl-block">
                  <p className="font-14 mb-2">{strings.Bookingbyme}</p>
                </div>
                <div className="total-booking d-block d-md-none d-lg-none d-xl-none">
                  <p className="font-14 mb-2">{strings.Bookingbyme}</p>
                </div>
              </div>
              <div className="total-number">
                <div className="display">
                  <div>
                    <h3 className=" font-semibold">{dashboardData ? selectedBBM == 1 ? dashboardData?.statics?.bookings_from_me_daily:selectedBBM == 2 ? dashboardData?.statics?.bookings_from_me_weekly:selectedBBM == 3 ? dashboardData?.statics?.bookings_from_me_monthly: (dashboardData?.statics?.bookings_from_me_yearly ?? 0) :0}</h3>
                  </div>
                  <DropdownButton
                      className='bg-transparent'
                      align="end"
                      title={selectedBBM == 1 ? strings.Daily : selectedBBM == 2 ? strings.Weekly : selectedBBM == 3 ? strings.Monthly : strings.Yearly}
                      id="dropdown-menu-align-end"
                  >
                    <Dropdown.Item eventKey="0" onClick={()=>setSelectedBBM(1)}>
                      {strings.Daily}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="1" onClick={()=>setSelectedBBM(2)}>
                      {strings.Weekly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="2" onClick={()=>setSelectedBBM(3)}>
                      {strings.Monthly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="3" onClick={()=>setSelectedBBM(4)}>
                      {strings.Yearly}
                    </Dropdown.Item>
                  </DropdownButton>
                </div>
                {getLastPercentageEarning(selectedBBM, dashboardData?.statics?.booking_by_me_percentage_daily,dashboardData?.statics?.booking_by_me_percentage_weekly,dashboardData?.statics?.booking_by_me_percentage_monthly,dashboardData?.statics?.booking_by_me_percentage_yearly)}
              </div>
            </div>
          </div>
        </div>
        {isSitter == true ? <div className={`col-md-4 col-lg-4 px-md-2 dash-col ${isSitter == true ? "col-xl-auto" : "col-xl-auto"}`}>
          <div className="bg-white main-background">
            <div className="week-details">
              <div>
                <div className="total-booking d-none d-md-block d-lg-block d-xl-block">
                  <p className="font-14 mb-2">{strings.Bookingforme}</p>
                </div>
                <div className="total-booking d-block d-md-none d-lg-none d-xl-none">
                  <p className="font-14 mb-2">{strings.Bookingforme}</p>
                </div>
              </div>
              <div className="total-number">
                <div className="display">
                  <div>
                    <h3 className=" font-semibold">{dashboardData ? selectedBTM == 1 ? dashboardData?.statics?.bookings_to_me_daily:selectedBTM == 2 ? dashboardData?.statics?.bookings_to_me_weekly:selectedBTM == 3 ? dashboardData?.statics?.bookings_to_me_monthly: (dashboardData?.statics?.bookings_to_me_yearly ?? 0) :0}</h3>
                  </div>
                  <DropdownButton
                      className='bg-transparent'
                      align="end"
                      title={selectedBTM == 1 ? strings.Daily : selectedBTM == 2 ? strings.Weekly : selectedBTM == 3 ? strings.Monthly : strings.Yearly}
                      id="dropdown-menu-align-end"
                  >
                    <Dropdown.Item eventKey="0" onClick={()=>setSelectedBTM(1)}>
                      {strings.Daily}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="1" onClick={()=>setSelectedBTM(2)}>
                      {strings.Weekly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="2" onClick={()=>setSelectedBTM(3)}>
                      {strings.Monthly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="3" onClick={()=>setSelectedBTM(4)}>
                      {strings.Yearly}
                    </Dropdown.Item>
                  </DropdownButton>
                </div>
                {getLastPercentageEarning(selectedBTM, dashboardData?.statics?.booking_for_me_percentage_daily,dashboardData?.statics?.booking_for_me_percentage_weekly,dashboardData?.statics?.booking_for_me_percentage_monthly,dashboardData?.statics?.booking_for_me_percentage_yearly)}
              </div>
            </div>
          </div>
        </div> : null}
        <div className={`col-md-4 col-lg-4 pl-md-2 dash-col ${isSitter == true ? "col-xl-3" : "col-xl-4"}`}>
          <div className="bg-white main-background">
            <div className="week-details">
              <div>
                <div className="total-booking d-none d-md-block d-lg-block d-xl-block">
                  <p className="font-14 mb-2">{strings.TotalSpent}</p>
                </div>
                <div className="total-booking d-block d-md-none d-lg-none d-xl-none">
                  <p className="font-14 mb-2">{strings.TotalSpent}</p>
                </div>
              </div>
              <div className="total-number">
                <div className="display">
                  <div>
                    <h3 className=" font-semibold">
                      <h3 className=" font-semibold">{getCurrencySign() + (dashboardData ? referral == 1 ? dashboardData?.statics?.paid_amount_from_me_daily : referral == 2 ? dashboardData?.statics?.paid_amount_from_me_weekly : referral == 3 ? dashboardData?.statics?.paid_amount_from_me_monthly: (dashboardData?.statics?.paid_amount_from_me_yearly ?? 0) :0)}</h3>
                    </h3>
                  </div>
                  <DropdownButton
                      className='bg-transparent'
                      align="end"
                      title={referral == 1 ? strings.Daily : referral == 2 ? strings.Weekly : referral == 3 ? strings.Monthly : strings.Yearly}
                      id="dropdown-menu-align-end"
                  >
                    <Dropdown.Item eventKey="0" onClick={()=>setReferral(1)}>
                      {strings.Daily}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="1" onClick={()=>setReferral(2)}>
                      {strings.Weekly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="2" onClick={()=>setReferral(3)}>
                      {strings.Monthly}
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="3" onClick={()=>setReferral(4)}>
                      {strings.Yearly}
                    </Dropdown.Item>
                  </DropdownButton>
                </div>
                {getLastPercentageEarning(referral, dashboardData?.statics?.total_spent_percentage_daily,dashboardData?.statics?.total_spent_percentage_weekly,dashboardData?.statics?.total_spent_percentage_monthly,dashboardData?.statics?.total_spent_percentage_yearly)}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StatusBar;
