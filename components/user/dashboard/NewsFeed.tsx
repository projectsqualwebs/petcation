import { strings } from "../../../public/lang/Strings";
import moment from "moment";
import Link from "next/link"
import {useEffect, useState} from "react";
import API from "../../../api/Api";
const IMAGE_BASE_URL = process.env.NEXT_PUBLIC_U_IMAGE_BASE_URL;
interface I_PROPS {
    newsFeed: {
        id: number;
        images: any;
        title_en: string;
        description_en: string;
        title_jp: string;
        description_jp: string;
        title_zh: string;
        description_zh: string;
        created_at: string;
        category: {
            name?: string;
            name_en?: string;
            name_jp?: string;
            name_zh?: string;
        };
    }[]
}

interface I_DATA {
    image: string;
    title: string;
    description: string;
    created_at: string;
    category: {
        name: string;
    };
}
export default function NewsFeed({newsFeed}: I_PROPS) {
    const api = new API();
    const [data, setData] = useState([])
    const [lang, setLang] = useState('en')

    useEffect( () => {
        let myLang =  localStorage.getItem('language')
        if (myLang) {
            setLang(myLang)
        }
    }, [])

    useEffect( () => {
        seLanguageContent(newsFeed, lang).then(r => {})
    }, [newsFeed])

    const seLanguageContent = async (newsFeed, myLang) => {
        if (myLang == 'en' && newsFeed?.length) {
            let engData = []
            await newsFeed?.map(async (myData) => await engData.push({
                id: myData?.id,
                image: myData?.images[0].path,
                title: myData?.title_en,
                description: myData?.description_en,
                created_at: myData?.created_at,
                category: {
                    name: myData?.category?.name ?? myData?.category?.name_en,
                }
            }))
            setData(engData)
        }
        if (myLang == 'jp' && newsFeed?.length) {
            let jpData = []
            await newsFeed?.map(async (myData) => await jpData.push({
                id: myData?.id,
                image: myData?.images[0].path,
                title: myData?.title_jp,
                description: myData?.description_jp,
                created_at: myData?.created_at,
                category: {
                    name: myData?.category?.name ?? myData?.category?.name_jp,
                },
            }))
            setData(jpData)
        }
        if (myLang == 'zh' && newsFeed?.length) {
            let zhData = []
            await newsFeed?.map(async (myData) => await zhData.push({
                id: myData?.id,
                image: myData?.images[0].path,
                title: myData?.title_zh,
                description: myData?.description_zh,
                created_at: myData?.created_at,
                category: {
                    name: myData?.category?.name ?? myData?.category?.name_zh,
                },
            }))
            setData(zhData)
        }
    }

  return (
    <div className="bg-white main-background pr-0">
      <div className="row mb-3">
        <div className="col-12 col-md-7 col-lg-7 col-xl-8 my-auto">
          <h6 className="mb-0 font-semibold">{strings.NewsFeeds}</h6>
        </div>
      </div>
      <div className="news-content">
        <div className="news-section">
          {data?.length ? data?.map((val,index) => <div className="col-12 news-feeds-details">
            <Link href={'/news/'+ val?.id}><div className="row align-items-center cursor-pointer">
              <div className="col-auto news-img h-auto pr-0">
                <img src={val?.image ? IMAGE_BASE_URL+val?.image : "images/img2.jpg"} className="img-fluid" alt="" />
              </div>
              <div className="col news-details pl-1">
                <h6 className="font-12 mb-2">
                  <span>{val.title}</span>
                </h6>
                <p className="mb-0 font-8">{strings.PostedOn + " " + moment(val?.created_at).format('MM/DD/YYYY')}</p>
              </div>
            </div></Link>
          </div>) : null}
        </div>
      </div>
    </div>
  );
}
