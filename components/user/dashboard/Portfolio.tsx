import React, { useState } from "react";
import API from "../../../api/Api";
import { strings } from "../../../public/lang/Strings";
import UploadFileModal from "../../common/UploadfileModal";
import { dataURLtoFile } from "../../../utils/Helper";
import { AxiosError, AxiosResponse } from "axios";
import Res from "../../../models/response.interface";
import IDashboardData from "../../../models/dashboard.interface";
import { useEffect } from "react";
import { useRef } from "react";
import { errorOptions, successOptions } from "../../../public/appData/AppData";
import { useSnackbar } from "react-simple-snackbar";
import Dropzone from "react-dropzone";

interface PortfolioImages {
  id: number;
  path: string;
  user_id: number;
}

const api = new API();
export default function Portfolio() {
  const [dashboard, setDashboard] = useState<IDashboardData>();
  const [images, setImages] = useState<PortfolioImages[]>([]);
  const [openCropper, setOpenCropper] = useState<boolean>(false);
  const [cropper, setCropper] = useState<any>();
  const [uploadedFilePath, setUploadedFilePath] = useState<any>();
  const [selecteImage, setSelectedImage] = useState<string>();
  const inputRef = useRef(null);
  const [openErr, closeErr] = useSnackbar(errorOptions);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);

  const [disableButton, setDisableButton] = useState(false);
  const [maxFileCountError, setMaxFileCountError] = useState(false);
  const [errorFiles, setErrorFiles] = useState([]);

  useEffect(() => {
    getDashboard();
  }, []);

  const getDashboard = async () => {
    api
      .getDashboard({ filter_by: null })
      .then((response: AxiosResponse<Res<IDashboardData>>) => {
        setDashboard(response.data.response);
        setImages(response.data.response.images);
      })
      .catch((error: AxiosError) => {});
  };

  const savePortfolio = (data: string[]) => {
    api
      .savePortfolio({ images: data })
      .then((res) => {})
      .catch((error) => {});
  };

  const deletePortfolioImage = (id: number) => {
    api
      .deletePortfolioImage({ image_id: id })
      .then((res) => {
        getDashboard().then((r) =>
          openSuccess(strings.ImageDeletedSuccessfully)
        );
      })
      .catch((error) => {});
  };

  const onFileChange = (event) => {
    if (event.dataTransfer || event.target.files) {
      let files;
      if (event.dataTransfer) {
        files = event.dataTransfer.files;
      } else if (event.target) {
        files = event.target.files;
      }
      const reader = new FileReader();
      reader.onload = () => {
        setUploadedFilePath(reader.result);
      };
      if (files[0].size < 5 * 1024 * 1024) {
        reader.readAsDataURL(files[0]);
        setOpenCropper(true);
      } else {
        openErr(strings.UnableToUploadImageSizeHigher);
        return;
      }
    }
  };

  const onUpload = (data:any) => {
    const formData = new FormData();
    data.map((value)=>{
      formData.append('images[]', value)
    })
    formData.append("path", "pets");
    api
        .uploadMultiFile(formData)
        .then((json) => {
          let arr = images?.map((image) => image?.path);
          console.log(json.data.response)
          savePortfolio([...arr, ...json.data.response]);
          getDashboard().then((r) =>
              openSuccess(strings.ImageUploadedSuccessfully)
          );
          setDisableButton(false)

        })
        .catch((error) => console.log(error));
  };

  const onSuccess = (acceptedFiles) => {
    setDisableButton(true);
    setMaxFileCountError(false);
    setErrorFiles([]);
    if (acceptedFiles.length > 5) {
      setMaxFileCountError(true);
      setDisableButton(false);
      return false
    }
    let fileSizeMoreThan10 = acceptedFiles.filter(file => file.size > 10e6);
    if (fileSizeMoreThan10.length) {
      onUpload(acceptedFiles.filter(file => file.size < 10e6));
      setErrorFiles(acceptedFiles.filter(file => file.size > 10e6));
    } else {
      onUpload(acceptedFiles)
    }
  }

  return (
    <div className="bg-white main-background">
      <div className="col-12 mb-2 px-0">
        <div className="row">
          <div className="col-6 col-md-7 col-lg-7 col-xl-8 my-auto">
            <h6 className="mb-0 font-semibold">{strings.MyPortfolio}</h6>
          </div>

          <div className="col-6 col-md-5 col-lg-5 col-xl-4 alignment">
            <Dropzone accept={{['image/*']: ['.jpeg', '.png', '.jpg', '.webp']}} onDrop={acceptedFiles => onSuccess(acceptedFiles)}>
              {({getRootProps, getInputProps}) => (
                  <div className="col-12 form-group m-0 p-0">
                    <section>
                      <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <button
                            onClick={() => {}}
                            className="btn btn-primary btn1">
                          {strings.Addmoreimages}
                        </button>
                      </div>
                    </section>
                  </div>
              )}
            </Dropzone>
          </div>
          <div className='col-12'>
            {maxFileCountError ? <div className='ml-0 d-flex justify-content-end'>
              <small className='text-danger text-right w-100 my-2'>*{strings.YouCanSelectMaximum5FilesAtATime}</small>
            </div> : null}
            {errorFiles?.length ? <div className='ml-0 d-flex justify-content-end'>
              <small className='text-danger text-right w-100 my-2'>*{errorFiles.map((file, index) => file?.name + (index+1 === errorFiles?.length ? ' ' : ', ') ) + ' ' + strings.areMoreThan10MB}</small>
            </div> : null}
          </div>
        </div>
      </div>
      {/*<hr />*/}
      <div className="row gallery-view justify-content-center">
        {images.map((value) => (
          <div className="gallery-image">
            <div className="dash-image show-image-delete-option">
              <img src={value.path} className="img-fluid" alt="" />
              <a
                className="btn btn-accept text-danger p-1 px-2"
                onClick={() => deletePortfolioImage(value.id)}>
                <svg viewBox="0 0 448 512">
                  <path
                    fill="currentColor"
                    d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                </svg>
              </a>
            </div>
          </div>
        ))}
      </div>
      <UploadFileModal
        onInitialized={(instance) => {
          setCropper(instance);
        }}
        path={uploadedFilePath}
        showModal={openCropper}
        zoomable={false}
        aspectRatio={16 / 9}
        setImage={(v) => {
          var file = dataURLtoFile(v, "image");
          const formData = new FormData();
          formData.append("image", file);
          formData.append("path", "pets");
          api
            .uploadFile(formData)
            .then((json) => {
              let arr = images?.map((image) => image?.path);
              savePortfolio([...arr, json.data.response]);
              getDashboard().then((r) =>
                openSuccess(strings.ImageUploadedSuccessfully)
              );
            })
            .catch((error) => console.log(error));
        }}
        preview=".img-preview"
        guides={false}
        viewMode={1}
        dragMode="move"
        cropBoxMovable={true}
        hideModal={() => setOpenCropper(false)}
      />
    </div>
  );
}
