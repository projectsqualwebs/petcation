import React, { useEffect, useState } from "react";
import {
  I_BOARDING_SERVICE_PET,
  I_PET_ADDITIONAL_SERVICE,
} from "../../../models/boardingService.interface";
import { strings } from "../../../public/lang/Strings";
import { getCurrencySign } from "../../../api/Constants";
import { Modal } from "react-bootstrap";
let initialState: I_PET_ADDITIONAL_SERVICE = {
  name: "",
  description: "",
  price: 0,
};
interface I_PROPS {
  data: any;
  addData: (id, data) => void;
  removeData: (id, index) => void;
  editData: (id, data, index) => void;
  showModal: boolean;
  setShowModal: () => void;
  additionalEditService: any;
}
export default function AdditonalServiceSidebar({
  showModal,
  setShowModal,
  data,
  addData,
  removeData,
  editData,
  additionalEditService,
}: I_PROPS) {
  const [customService, setCustomService] =
    useState<I_PET_ADDITIONAL_SERVICE>(initialState);
  const [errors, setErrors] = useState<any>({});
  const [myIndex, setMyIndex] = useState<number>(null);
  useEffect(() => {
    if (additionalEditService) {
      setCustomService(additionalEditService);
      let allIds = data.custom_services.map((val) => val.id);
      setMyIndex(allIds.indexOf(additionalEditService.id));
    } else {
      setCustomService(initialState);
      setMyIndex(null);
    }
  }, [additionalEditService]);
  const onTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setErrors({});
    setCustomService({ ...customService, [e.target.name]: e.target.value });
  };
  const validateAndAdd = () => {
    if (customService.name == "") {
      setErrors({ name: true });
    } else if (customService.description == "") {
      setErrors({ description: true });
    } else if (customService.price == 0) {
      setErrors({ price: true });
    } else {
      setErrors({});
      addData(data.pet_type, customService);
      setCustomService(initialState);
      setShowModal();
    }
  };
  const validateAndEdit = () => {
    if (customService.name == "") {
      setErrors({ name: true });
    } else if (customService.description == "") {
      setErrors({ description: true });
    } else if (customService.price == 0) {
      setErrors({ price: true });
    } else {
      setErrors({});
      editData(data.pet_type, customService, myIndex);
      setCustomService(initialState);
      setMyIndex(null);
      setShowModal();
    }
  };
  return (
    <Modal
      show={showModal}
      className="sidebar-modal modal service-modal left services fade"
      id="services"
      tabIndex={-1}
      role="dialog"
      aria-hidden="true">
      {" "}
      <div className="modal-dialog modal-sm" role="document">
        {" "}
        <div className="modal-content">
          {" "}
          <div className="modal-header border-0 py-3">
            {" "}
            <div className="close-button">
              {" "}
              <button
                type="button"
                className="close"
                onClick={() => setShowModal()}>
                {" "}
                <span aria-hidden="true" className="font-12 font-regular">
                  {" "}
                  × {strings.Close}{" "}
                </span>{" "}
              </button>{" "}
            </div>{" "}
          </div>{" "}
          <div className="modal-body">
            {" "}
            <div className="optional-details mt-3">
              {" "}
              <h5>
                {" "}
                {
                  strings.WantToOfferOptionalCustomServiceWhenCustomerBookService_Q
                }{" "}
              </h5>{" "}
            </div>{" "}
            <div className="row">
              {" "}
              <div className="col-12 col-md-10 col-lg-10 col-xl-10 mx-auto">
                {" "}
                <div className="optional-details">
                  {" "}
                  <div className="optional">
                    {" "}
                    <div className="d-flex justify-content-between mb-2">
                      {" "}
                      <div>
                        {" "}
                        <p className="font-10 mb-0">
                          {strings.NameOfTheService}
                        </p>{" "}
                      </div>{" "}
                      <div className="help-tip">
                        {" "}
                        <p>{strings.NameOfTheService}</p>{" "}
                      </div>{" "}
                    </div>{" "}
                    <div className="form-group mb-0">
                      {" "}
                      <input
                        className={
                          "form-control valid-control " +
                          (errors.name ? "invalid" : "")
                        }
                        placeholder={strings.EnterNameOfService}
                        type="text"
                        name="name"
                        onChange={onTextChange}
                        value={customService.name}
                      />{" "}
                    </div>{" "}
                  </div>{" "}
                  <div className="optional">
                    {" "}
                    <div className="d-flex justify-content-between mb-2">
                      {" "}
                      <div>
                        {" "}
                        <p className="font-10 mb-0">
                          {strings.DescribeTheService}
                        </p>{" "}
                      </div>{" "}
                      <div className="help-tip">
                        {" "}
                        <p>{strings.DescribeTheService}</p>{" "}
                      </div>{" "}
                    </div>{" "}
                    <div className="form-group mb-0">
                      {" "}
                      <input
                        className={
                          "form-control valid-control " +
                          (errors.description ? "invalid" : "")
                        }
                        placeholder="Enter description of service here"
                        type="text"
                        name="description"
                        onChange={onTextChange}
                        value={customService.description}
                      />{" "}
                    </div>{" "}
                  </div>{" "}
                  <div className="optional">
                    {" "}
                    <div className="d-flex justify-content-between mb-2">
                      {" "}
                      <div>
                        {" "}
                        <p className="font-10 mb-0">
                          {strings.PriceOfTheService}
                        </p>{" "}
                      </div>{" "}
                      <div className="help-tip">
                        {" "}
                        <p>{strings.PriceOfTheService}</p>{" "}
                      </div>{" "}
                    </div>{" "}
                    <div className="form-group mb-0">
                      {" "}
                      <input
                        className={
                          "form-control valid-control " +
                          (errors.price ? "invalid" : "")
                        }
                        placeholder={strings.EnterPriceOfService}
                        type="number"
                        name="price"
                        onChange={onTextChange}
                        value={customService.price}
                      />{" "}
                    </div>{" "}
                  </div>{" "}
                </div>{" "}
                <div>
                  {" "}
                  <a href="#" className="font-10 text-white">
                    {" "}
                    {strings.AddAnotherCustomServiceOffering}{" "}
                  </a>{" "}
                </div>{" "}
                <button
                  onClick={myIndex == null ? validateAndAdd : validateAndEdit}
                  className="btn btn-primary my-2 mr-2">
                  {" "}
                  {myIndex == null ? strings.Add : strings.Update}{" "}
                </button>{" "}
              </div>{" "}
            </div>{" "}
            {/*{data &&*/} {/* data.custom_services.map((value, index) => (*/}{" "}
            {/* <>*/} {/* <div className="col-12 p-0">*/}{" "}
            {/* <hr className="bg-white opacity-50"/>*/} {/* </div>*/}{" "}
            {/* <div className="d-flex justify-content-between align-items-center bg-white rounded-lg mt-2 py-2 px-2">*/}{" "}
            {/* <p className="mb-0 font-weight-bold pl-1">*/}{" "}
            {/* {value.name}*/}{" "}
            {/* <span className="d-block font-12 font-weight-medium">{" " + getCurrencySign() + value.price}</span>*/}{" "}
            {/* </p>*/} {/* <div>*/}{" "}
            {/* <a className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"*/}{" "}
            {/* onClick={() => {*/} {/* setCustomService(value);*/}{" "}
            {/* setMyIndex(index);*/} {/* }}*/} {/* >*/}{" "}
            {/* <svg className="text-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">*/}{" "}
            {/* <path*/} {/* fill="currentColor"*/}{" "}
            {/* d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"/>*/}{" "}
            {/* </svg>*/} {/* </a>*/}{" "}
            {/* <a className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"*/}{" "}
            {/* href="#"*/}{" "}
            {/* onClick={() => removeData(data.pet_type, index)}*/} {/* >*/}{" "}
            {/* <svg*/} {/* viewBox="0 0 448 512"*/} {/* >*/} {/* <path*/}{" "}
            {/* fill="currentColor"*/}{" "}
            {/* d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"*/}{" "}
            {/* ></path>*/} {/* </svg>*/} {/* </a>*/} {/* </div>*/}{" "}
            {/* </div>*/} {/* </>*/} {/* ))}*/}{" "}
          </div>{" "}
        </div>{" "}
      </div>{" "}
    </Modal>
  );
}
