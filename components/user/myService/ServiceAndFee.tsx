import Select from "react-select";
import React, { useState } from "react";
import { strings } from "../../../public/lang/Strings";
import { I_BOARDING_SERVICE_PET } from "../../../models/boardingService.interface";
import {getCurrencySign} from "../../../api/Constants";

const ServiceAndFee = () => {
  const [service, setService] = useState<I_BOARDING_SERVICE_PET[]>([]);
  const [activeService, setActiveService] = useState({
    dog: false,
    cat: false,
    birds: false,
    reptile: false,
    smallAnimals: false,
  });

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    switch (event.target.name) {
      case "dog":
        setActiveService({ ...activeService, dog: event.target.checked });
        addPetService(event.target.checked, 1);
        break;
    }
  };

  const getServiceObject = (id) => {
    return {
      pet_type: id,
      fees: [
        {
          capacity: 0,
          pet_size_id: 0,
          service_charge: "0",
          earning_amount: "0",
        },
      ],
    };
  };

  const removePetService = (id) => {
    let serviceData = [...service];
    let index = serviceData.findIndex((value) => value.pet_type == id);
    serviceData.splice(index, 1);
    setService(serviceData);
  };

  const addPetService = (boolean, id) => {
    if (boolean) {
      setService([...service, getServiceObject(id)]);
    } else {
      removePetService(id);
    }
  };

  return (
    <div className="tab-content" id="myTabContent">
      <div
        className="tab-pane fade active show"
        id="home"
        role="tabpanel"
        aria-labelledby="home-tab"
      >
        <div className="fees-content my-3">
          <h6 className="font-semibold font-14 mb-0">
            {strings.whatEverPetServiceYouProvide}
          </h6>
          <p className="mb-0 font-12">{strings.boardingServicedescription}</p>
          <div className="booking-for m-0">
            <div className="row  service-charges pb-0">
              <div className="col-12">
                <div className="custom-check mb-2">
                  <label className="check ">
                    <input
                      type="checkbox"
                      className="class1"
                      name="dog"
                      defaultChecked={false}
                      defaultValue="dog1"
                      checked={activeService.dog}
                      onChange={onChange}
                    />
                    <span className="checkmark" />
                    {strings.Dog}
                  </label>
                </div>
                <div
                  className="hidden"
                  id="hidden_fields_one"
                  style={activeService.dog ? {} : { display: "none" }}
                >
                  <div className="row">
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Capacity}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.HandlingPetsAtMentionedPrice}</p>
                              </div>
                      </div>
                      <div className="num-block skin-2">
                        <div className="num-in">
                          <span className="minus">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                          <input
                            type="text"
                            className="in-num"
                            defaultValue={1}
                            readOnly
                          />
                          <span className="plus">
                              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12M12 18V6" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.PetSize}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.SizeYouAreGoingToHandleInThisPrice}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <div className="category-selection charge-select">
                          <Select isSearchable={false} />
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-2">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.ChargesX1}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.AddChargesAsPerThePetSelected}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <input
                          className="form-control"
                          placeholder="¥100"
                          type="number"
                        />
                      </div>
                    </div>
                    <div className="col-6 col-md-2">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Earnings}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.TheAmountYouWillReceiveForTheService}</p>
                              </div>
                      </div>
                      <div className="earning-details">
                        <p className="font-14 mb-0">{getCurrencySign()}85</p>
                      </div>
                    </div>
                  </div>
                  <div data-toggle="modal" data-target="#services">
                    <a  className="font-10 text-muted text-center text-md-left d-block">
                      {strings.WantToAddOptionalServicesAndChargesAlongWithDogBoarding}
                    </a>
                  </div>
                </div>
              </div>
            </div>

            <div className="row  service-charges pb-0">
              <div className="col-12">
                <div className="custom-check mb-2">
                  <label className="check ">
                    <input
                      type="checkbox"
                      name="cat"
                      defaultChecked={false}
                      checked={activeService.dog}
                      onChange={onChange}
                    />
                    <span className="checkmark" />
                    {strings.Cat}
                  </label>
                </div>
                <div
                  className="hidden"
                  id="hidden_fields_one"
                  style={activeService.cat ? { display: "none" } : {}}
                >
                  <div className="row">
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Capacity}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.HandlingPetsAtMentionedPrice}</p>
                              </div>
                      </div>
                      <div className="num-block skin-2">
                        <div className="num-in">
                          <span className="minus">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                          <input
                            type="text"
                            className="in-num"
                            defaultValue={1}
                            readOnly
                          />
                          <span className="plus">
                              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12M12 18V6" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.PetSize}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.SizeYouAreGoingToHandleInThisPrice}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <div className="category-selection charge-select">
                          <Select isSearchable={false} />
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.ChargesX1}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.AddChargesAsPerThePetSelected}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <input
                          className="form-control"
                          placeholder="¥100"
                          type="number"
                        />
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Earnings}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.TheAmountYouWillReceiveForTheService}</p>
                              </div>
                      </div>
                      <div className="earning-details">
                        <p className="font-14 mb-0">{getCurrencySign()}85</p>
                      </div>
                    </div>
                  </div>
                  <div data-toggle="modal" data-target="#services">
                    <a  className="font-10 text-muted text-center text-md-left d-block">
                      {strings.WantToAddOptionalServicesAndChargesAlongWithDogBoarding}
                    </a>
                  </div>
                </div>
              </div>
            </div>
            {/*--------/cat-----------*/}
            {/*--------birds-----------*/}
            <div className="row  service-charges pb-0">
              <div className="col-12">
                <div className="custom-check mb-2">
                  <label className="check ">
                    <input
                      type="checkbox"
                      name="is_name1"
                      defaultChecked={true}
                    />
                    <span className="checkmark" />
                    {strings.Birds}
                  </label>
                </div>
                <div className="capacity">
                  <div className="row">
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Capacity}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.HandlingPetsAtMentionedPrice}</p>
                              </div>
                      </div>
                      <div className="num-block skin-2">
                        <div className="num-in">
                          <span className="minus">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                          <input
                            type="text"
                            className="in-num"
                            defaultValue={1}
                            readOnly
                          />
                          <span className="plus">
                              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12M12 18V6" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.PetSize}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.SizeYouAreGoingToHandleInThisPrice}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <div className="category-selection charge-select">
                          <Select isSearchable={false} />
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.ChargesX1}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.AddChargesAsPerThePetSelected}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <input
                          className="form-control"
                          placeholder="¥100"
                          type="number"
                        />
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Earnings}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.TheAmountYouWillReceiveForTheService}</p>
                              </div>
                      </div>
                      <div className="earning-details">
                        <p className="font-14 mb-0">{getCurrencySign()}85</p>
                      </div>
                    </div>
                  </div>
                  <div data-toggle="modal" data-target="#services">
                    <a  className="font-10 text-muted text-center text-md-left d-block">
                      + {strings.WantToAddOptionalServicesAndChargesAlongWithDogBoarding}
                    </a>
                  </div>
                </div>
              </div>
            </div>
            {/*--------/birds-----------*/}
            {/*--------Reptiles-----------*/}
            <div className="row  service-charges pb-0">
              <div className="col-12">
                <div className="custom-check mb-2">
                  <label className="check ">
                    <input type="checkbox" name="is_name1" />
                    <span className="checkmark" />
                    {strings.Reptiles}
                  </label>
                </div>
                <div className="capacity">
                  <div className="row">
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Capacity}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.HandlingPetsAtMentionedPrice}</p>
                              </div>
                      </div>
                      <div className="num-block skin-2">
                        <div className="num-in">
                          <span className="minus">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                          <input
                            type="text"
                            className="in-num"
                            defaultValue={1}
                            readOnly
                          />
                          <span className="plus">
                              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12M12 18V6" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.PetSize}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.SizeYouAreGoingToHandleInThisPrice}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <div className="category-selection charge-select">
                          <Select isSearchable={false} />
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.ChargesX1}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.AddChargesAsPerThePetSelected}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <input
                          className="form-control"
                          placeholder="¥100"
                          type="number"
                        />
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Earnings}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.TheAmountYouWillReceiveForTheService}</p>
                              </div>
                      </div>
                      <div className="earning-details">
                        <p className="font-14 mb-0">{getCurrencySign()}85</p>
                      </div>
                    </div>
                  </div>
                  <div data-toggle="modal" data-target="#services">
                    <a  className="font-10 text-muted text-center text-md-left d-block">
                      {strings.WantToAddOptionalServicesAndChargesAlongWithDogBoarding}
                    </a>
                  </div>
                </div>
              </div>
            </div>

            <div className="row  service-charges pb-0">
              <div className="col-12">
                <div className="custom-check mb-2">
                  <label className="check ">
                    <input type="checkbox" name="is_name1" />
                    <span className="checkmark" />
                    {strings.Smallanimals}
                  </label>
                </div>
                <div className="capacity">
                  <div className="row">
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Capacity}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.HandlingPetsAtMentionedPrice}</p>
                              </div>
                      </div>
                      <div className="num-block skin-2">
                        <div className="num-in">
                          <span className="minus">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                          <input
                            type="text"
                            className="in-num"
                            defaultValue={1}
                            readOnly
                          />
                          <span className="plus">
                              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none"><path d="M6 12h12M12 18V6" stroke="#FF8A65" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.PetSize}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.SizeYouAreGoingToHandleInThisPrice}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <div className="category-selection charge-select">
                          <Select isSearchable={false} />
                        </div>
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-3">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.ChargesX1}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.AddChargesAsPerThePetSelected}</p>
                              </div>
                      </div>
                      <div className="form-group mb-0">
                        <input
                          className="form-control"
                          placeholder="¥100"
                          type="number"
                        />
                      </div>
                    </div>
                    <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                      <div className="d-flex justify-content-between mb-2">
                        <div>
                          <p className="font-12 mb-0">{strings.Earnings}</p>
                        </div>
                        <div className="help-tip">
                                <p>{strings.TheAmountYouWillReceiveForTheService}</p>
                              </div>
                      </div>
                      <div className="earning-details">
                        <p className="font-14 mb-0">{getCurrencySign()}85</p>
                      </div>
                    </div>
                  </div>
                  <div data-toggle="modal" data-target="#services">
                    <a  className="font-10 text-muted text-center text-md-left d-block">
                      + {strings.WantToAddOptionalServicesAndChargesAlongWithDogBoarding}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="extra-charge">
          <p className="font-semibold text-muted font-14 mb-0">
            {strings.WouldYouLikeToChargeExtraWhenProvidingBoardingServiceDuringHolidays}
          </p>
          <p className="mb-0 font-12">
            {strings.EnterValueInByWhichYouWouldWantPricesToIncreaseDuringHolidaysInBelowBoxLookAtListOfHolidaysHere}
          </p>
          <div className="d-flex service-charges">
            <div className="charge-input">
              <input className="form-control" id="charge" placeholder={"10"} />
            </div>
            <div className="ml-3">
              <p className="font-12 mb-0">
                {strings.RangeYouCan} <br />
                {strings.increaseIs0to100}
              </p>
            </div>
          </div>
        </div>
        <hr />
        <div className="fees-content my-3">
          <p className="font-semibold text-muted font-14 mb-0">
            {strings.CacellationPolicy}
          </p>
          <p className="mb-0 font-12">
            {strings.SelectCancellationPolicyForThisServiceUserWillSeeTheSameAtTheTimeOfBooking}
          </p>
          <div className="booking-for m-0">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                {/*--------Flexible-----------*/}
                <div className="row  service-charges pb-0">
                  <div className="custom-check">
                    <label className="check  mb-2">
                      <input
                        type="radio"
                        name="is_name1"
                        defaultChecked={true}
                      />
                      <span className="checkmark" />
                      {strings.Flexible}
                    </label>
                    <p className="font-12">
                      {strings.ForBoardingAndHouseSittingYouWillGetAFullRefundIfYouCancelBeforeTheStayBegins}
                    </p>
                    <p className="font-12 mb-0">
                      {strings.ForWalksDayCareAndDropinVisitsYouWillGetAFullRefundIfYouCancelBeforeTheDaysServiceIsDelivered}
                    </p>
                  </div>
                </div>
                {/*--------/Flexible-----------*/}
                {/*--------Flexible-----------*/}
                <div className="row  service-charges pb-0">
                  <div className="custom-check">
                    <label className="check mb-2">
                      <input
                        type="radio"
                        name="is_name1"
                        defaultChecked={true}
                      />
                      <span className="checkmark" />
                      {strings.Flexible}
                    </label>
                    <p className="font-12 mb-2">
                      {strings.YouCancelWithin48HoursOfBooking + " "}
                    </p>
                    <p className="font-12  mb-2">
                      {strings.YouHavenotAlreadyCancelled3ReservationsInTheLast12months}
                    </p>
                    <p className="font-12  mb-2">
                      {strings.TheReservationYouareCancellingDoesnotOverlapWithAnotherReservationInYourAccountWhenRefund}
                    </p>
                    <p className="font-12  mb-0">
                      {strings.YouwllGetAFullRefundIfYouCancelBy1200Noon3daysBeforeTheStayBegins}
                    </p>
                  </div>
                </div>
                {/*--------/Flexible-----------*/}
                {/*--------Flexible-----------*/}
                <div className="row  service-charges pb-0">
                  <div className="custom-check">
                    <label className="check mb-2">
                      <input
                        type="radio"
                        name="is_name1"
                        defaultChecked={true}
                      />
                      <span className="checkmark" />
                      {strings.Strict}
                    </label>
                    <p className="font-12 mb-2">
                      {strings.YouwllGetAFullRefundIfYouCancelBy1200NoonOneWeekBeforeTheStayBegins}
                    </p>
                  </div>
                </div>
                {/*--------/Flexible-----------*/}
              </div>
            </div>
          </div>
        </div>
        <hr />
      </div>

      <div
        className="tab-pane fade"
        id="profile"
        role="tabpanel"
        aria-labelledby="profile-tab"
      >
        <div className="booking-for m-0">
          <div className="preferences-details">
            <div className="main-padding">
              <h6 className="font-semibold font-14">
                {strings.Youravailabilitywhilepetisatboarding}
              </h6>
              <p className="font-12">
                {strings.YouCanSelectFromTheOptionsParttimeAndfulltimeSelectingFulltimeMeansYouAreAvailableAllTheTimeAtHomeWhenBoardingThePet}
              </p>
              <div className="form-group">
                <div className="category-selection charge-select">
                  <div className="dropdown bootstrap-select form-control mySelect">
                    <select className="form-control mySelect" tabIndex={-98}>
                      <option>{strings.Fulltime}</option>
                    </select>
                    <button
                      type="button"
                      className="btn dropdown-toggle btn-light"
                      data-toggle="dropdown"
                      role="combobox"
                      aria-owns="bs-select-4"
                      aria-haspopup="listbox"
                      aria-expanded="false"
                      title="Fulltime"
                    >
                      <div className="filter-option">
                        <div className="filter-option-inner">
                          <div className="filter-option-inner-inner">
                            {strings.Fulltime}
                          </div>
                        </div>{" "}
                      </div>
                    </button>
                    <div className="dropdown-menu ">
                      <div
                        className="inner show"
                        role="listbox"
                        id="bs-select-4"
                        tabIndex={-1}
                      >
                        <ul
                          className="dropdown-menu inner show"
                          role="presentation"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="main-padding">
              <h6 className="font-medium font-14">
                {strings.whenhostingpetsinyourhomehowfrequentlycanyouprovidebreaks}
              </h6>
              <p className="font-12">
                {strings.howfrequentlycanyouprovidepottybreaks}
              </p>
              <div className="form-group">
                <div className="category-selection charge-select">
                  <div className="dropdown bootstrap-select form-control mySelect">
                    <select className="form-control mySelect" tabIndex={-98}>
                      <option>{strings._0_2_hrs}</option>
                    </select>
                    <button
                      type="button"
                      className="btn dropdown-toggle btn-light"
                      data-toggle="dropdown"
                      role="combobox"
                      aria-owns="bs-select-5"
                      aria-haspopup="listbox"
                      aria-expanded="false"
                      title="0-2 hrs"
                    >
                      <div className="filter-option">
                        <div className="filter-option-inner">
                          <div className="filter-option-inner-inner">
                           {strings._0_2_hrs}
                          </div>
                        </div>{" "}
                      </div>
                    </button>
                    <div className="dropdown-menu ">
                      <div
                        className="inner show"
                        role="listbox"
                        id="bs-select-5"
                        tabIndex={-1}
                      >
                        <ul
                          className="dropdown-menu inner show"
                          role="presentation"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-6">
                <h6 className="font-medium font-14 mb-3">
                  {strings.Canyoupickuppetfromclientplace}
                </h6>
                <div className="form-group">
                  <div className="category-selection charge-select">
                    <div className="dropdown bootstrap-select form-control mySelect">
                      <select className="form-control mySelect" tabIndex={-98}>
                        <option>{strings.Yes}</option>\<option>{strings.No}</option>
                      </select>
                      <button
                        type="button"
                        className="btn dropdown-toggle btn-light"
                        data-toggle="dropdown"
                        role="combobox"
                        aria-owns="bs-select-6"
                        aria-haspopup="listbox"
                        aria-expanded="false"
                        title="Yes"
                      >
                        <div className="filter-option">
                          <div className="filter-option-inner">
                            <div className="filter-option-inner-inner">{strings.Yes}</div>
                          </div>{" "}
                        </div>
                      </button>
                      <div className="dropdown-menu ">
                        <div
                          className="inner show"
                          role="listbox"
                          id="bs-select-6"
                          tabIndex={-1}
                        >
                          <ul
                            className="dropdown-menu inner show"
                            role="presentation"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-3">
                <h6 className="font-medium font-14 mb-3">{strings.ByWhatMeans_Q}</h6>
                <div className="form-group">
                  <div className="category-selection charge-select">
                    <div className="dropdown bootstrap-select form-control mySelect">
                      <select className="form-control mySelect" tabIndex={-98}>
                        <option>{strings.CAR}</option>
                      </select>
                      <button
                        type="button"
                        className="btn dropdown-toggle btn-light"
                        data-toggle="dropdown"
                        role="combobox"
                        aria-owns="bs-select-7"
                        aria-haspopup="listbox"
                        aria-expanded="false"
                        title="CAR"
                      >
                        <div className="filter-option">
                          <div className="filter-option-inner">
                            <div className="filter-option-inner-inner">{strings.CAR}</div>
                          </div>{" "}
                        </div>
                      </button>
                      <div className="dropdown-menu ">
                        <div
                          className="inner show"
                          role="listbox"
                          id="bs-select-7"
                          tabIndex={-1}
                        >
                          <ul
                            className="dropdown-menu inner show"
                            role="presentation"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-3">
                <h6 className="font-medium font-14 mb-3">{strings.HowFar_Q}</h6>
                <div className="form-group">
                  <div className="category-selection charge-select">
                    <div className="dropdown bootstrap-select form-control mySelect">
                      <select className="form-control mySelect" tabIndex={-98}>
                        <option>{strings._15Kms}</option>\<option>{strings._20Kms}</option>
                      </select>
                      <button
                        type="button"
                        className="btn dropdown-toggle btn-light"
                        data-toggle="dropdown"
                        role="combobox"
                        aria-owns="bs-select-8"
                        aria-haspopup="listbox"
                        aria-expanded="false"
                        title="15Kms"
                      >
                        <div className="filter-option">
                          <div className="filter-option-inner">
                            <div className="filter-option-inner-inner">
                              {strings._15Kms}
                            </div>
                          </div>{" "}
                        </div>
                      </button>
                      <div className="dropdown-menu ">
                        <div
                          className="inner show"
                          role="listbox"
                          id="bs-select-8"
                          tabIndex={-1}
                        >
                          <ul
                            className="dropdown-menu inner show"
                            role="presentation"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="tab-pane fade"
        id="contact"
        role="tabpanel"
        aria-labelledby="contact-tab"
      >
        <div className="preferences-details">
          <div className="booking-for m-0">
            <h5 className="font-semibold">{strings.Specialdiscountforyourguests}</h5>
            <div className="discount-check">
              <div className="custom-check">
                <label className="check ">
                  <input
                    type="radio"
                    className="class1"
                    name="is_name2"
                    defaultChecked={true}
                    defaultValue="dog"
                  />
                  <span className="checkmark" />
                  <h6>{strings.offonfirstbookingwithyou}</h6>
                  <p className="font-12">
                    {strings.Allowofonbookingamount}
                  </p>
                </label>
              </div>
            </div>
            <div className="discount-check">
              <div className="custom-check">
                <label className="check ">
                  <input
                    type="radio"
                    className="class1"
                    name="is_name2"
                    defaultValue="dog"
                  />
                  <span className="checkmark" />
                  <h6>{strings.Noofferonthisservice}</h6>
                  <p className="font-12">
                    {strings.Allowofonbookingamount}
                  </p>
                </label>
              </div>
            </div>
            <hr />
            <div className="stay-discount">
              <h5>{strings.Lengthofstaydiscount}</h5>
              <p className="font-12">
                {strings.Allowofonbookingamount}
              </p>
              <div className="weekly-discount mb-4">
                <h6 className="text-muted font-14 font-semibold">
                  {strings.Weeklydiscount}
                </h6>
                <input
                  className="form-control mb-3"
                  id="weekly-discount"
                  name="discount"
                />
                <h6 className="font-normal tip">{strings.Ex21_P}</h6>
                <p className="font-12 mb-0">
                  {strings.Thisdiscountwillapplytoanyreservation}
                </p>
              </div>
              <div className="weekly-discount mb-3">
                <h6 className="text-muted font-14 font-semibold">
                  {strings.Monthlydiscount}
                </h6>
                <input
                  className="form-control mb-3"
                  id="weekly-discount"
                  name="discount"
                />
                <h6 className="font-normal tip">{strings.Ex49_P}</h6>
                <p className="font-12 mb-0">
                  {strings.Thisdiscountwillapplytoanyreservation}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ServiceAndFee;
