import Select from "react-select";
import React, { useEffect, useState } from "react";
import { strings } from "../../../../public/lang/Strings";
import {
  I_BOARDING_SERVICE,
  I_BOARDING_SERVICE_PET,
  I_PET_ADDITIONAL_SERVICE,
} from "../../../../models/boardingService.interface";
import {
  errorOptions,
  petSize,
  select,
  serviceFeeObject,
  successOptions,
} from "../../../../public/appData/AppData";
import {
  deepClone,
  getPercent,
  getServiceObject,
} from "../../../../utils/Helper";
import SaveServiceView from "../SaveServiceView";
import AdditonalServiceSidebar from "../AdditionalServiceSidebar";
import API from "../../../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import { useRouter } from "next/router";
import { AxiosResponse } from "axios";
import Res from "../../../../models/response.interface";
import { getCurrencySign } from "../../../../api/Constants";

type input = React.ChangeEvent<HTMLInputElement>;
const api = new API();

interface I_Props {
  serviceId: number;
  handleTabChange: (x: number) => void;
}

const ServiceFee: React.FC<I_Props> = ({
  serviceId,
  handleTabChange,
}: I_Props) => {
  const router = useRouter();
  const [showAdditionalServiceModal, setShowAdditionalServiceModal] =
    useState(false);
  const [sitterBoardingServiceId, setSitterBoardingServiceId] =
    useState<any>(null);
  const [sitterHouseSittingServiceId, setSitterHouseSittingServiceId] =
    useState<any>(null);
  const [sitterDropInServiceId, setSitterDropInServiceId] = useState<any>(null);
  const [sitterPetDayCareServiceId, setSitterPetDayCareServiceId] =
    useState<any>(null);
  const [sitterServiceId, setSitterServiceId] = useState<number>();
  const [service, setService] = useState<I_BOARDING_SERVICE_PET[]>([]);
  const [holidayCharges, setHolidayCharges] = useState<string>("0");
  const [cancellationPolicy, setCancellationPolicy] = useState<number>(1);
  const [sideBarData, setSideBarData] = useState<I_BOARDING_SERVICE_PET>();
  const [errors, setErrors] = useState<any>({});
  const [openSnackbar, closeSnackbar] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [edit, setEdit] = useState();
  const [myIndexService, setMyIndexService] = useState();
  const [additionalEditService, setAdditionalEditService] = useState<any>(null);
  const [activeService, setActiveService] = useState({
    dog: false,
    cat: false,
    birds: false,
    reptile: false,
    smallAnimals: false,
  });

  useEffect(() => {
    if (router.query?.serviceId) {
      setSitterServiceId(Number(router.query.serviceId));
      getServiceInfo(serviceId);
    }
  }, [router.query]);

  const getServiceInfo = (id) => {
    var call;
    switch (id) {
      case 1:
        call = api.getBoardingInfo(1);
        break;
      case 2:
        call = api.getHouseSittingInfo(1);
        break;
      case 3:
        call = api.getDropInVisitsInfo(1);
        break;
      case 4:
        call = api.getPetDayCareInfo(1);
        break;
    }
    call
      .then((response: AxiosResponse<Res<I_BOARDING_SERVICE>>) => {
        setService(response.data.response.service_pets);
        setHolidayCharges(`${response.data.response.holiday_extra_charges}`);
        setCancellationPolicy(response.data.response.cancellation_policy);
        switch (id) {
          case 1:
            setSitterBoardingServiceId(
              response.data.response.sitter_boarding_service_id
            );
            break;
          case 2:
            setSitterHouseSittingServiceId(
              response.data.response.sitter_house_sitting_service_id
            );
            break;
          case 3:
            setSitterDropInServiceId(
              response.data.response.sitter_drop_in_visits_services_id
            );
            break;
          case 4:
            setSitterPetDayCareServiceId(
              response.data.response.sitter_pet_day_care_services_id
            );
            break;
        }
        let data = response.data.response.service_pets;
        let object = { ...activeService };
        for (let i = 0; i < data.length; i++) {
          switch (data[i].pet_type) {
            case 1:
              object.dog = true;
              break;
            case 2:
              object.cat = true;
              break;
            case 3:
              object.birds = true;
              break;
            case 4:
              object.reptile = true;
              break;
            case 5:
              object.smallAnimals = true;
              break;
          }
        }
        setActiveService({ ...activeService, ...object });
      })

      .catch((error) => console.log(error));
  };

  const getIndexById = (id) => {
    let serviceData = [...service];
    return serviceData.findIndex((value) => value.pet_type == id);
  };

  const onChange = (event: input) => {
    switch (event.target.name) {
      case "dog":
        setActiveService({ ...activeService, dog: event.target.checked });
        addPetService(event.target.checked, 1);
        break;
      case "cat":
        setActiveService({ ...activeService, cat: event.target.checked });
        addPetService(event.target.checked, 2);
        break;
      case "birds":
        setActiveService({ ...activeService, birds: event.target.checked });
        addPetService(event.target.checked, 3);
        break;
      case "reptile":
        setActiveService({ ...activeService, reptile: event.target.checked });
        addPetService(event.target.checked, 4);
        break;
      case "smallAnimals":
        setActiveService({
          ...activeService,
          smallAnimals: event.target.checked,
        });
        addPetService(event.target.checked, 5);
        break;
    }
  };

  const removePetService = (id) => {
    let serviceData = [...service];
    // serviceData.splice(getIndexById(id), 1);
    let index = getIndexById(id);
    if (serviceData[index]?.id) {
      var call;
      switch (serviceId) {
        case 1:
          call = api.deleteBoardingServiceFee(
            serviceData[index]?.pet_type,
            sitterBoardingServiceId
          );
          break;
        case 2:
          call = api.deleteHouseSittingServiceFee(
            serviceData[index]?.pet_type,
            sitterHouseSittingServiceId
          );
          break;
        case 3:
          call = api.deleteDropInVisitsServiceFee(
            serviceData[index]?.pet_type,
            sitterDropInServiceId
          );
          break;
        case 4:
          call = api.deletePetDayCateServiceFee(
            serviceData[index]?.pet_type,
            sitterPetDayCareServiceId
          );
          break;
      }
      call
        .then((res) => {
          openSnackbar(res.data?.message);
        })
        .catch((error) => openError(error.response.data.message));
    }
    serviceData.splice(getIndexById(id), 1);
    setService(serviceData);
  };

  const addPetService = (boolean, id) => {
    if (boolean) {
      setService([...service, getServiceObject(id)]);
    } else {
      removePetService(id);
    }
  };

  const onPetSizeChange = (data: select, id: number, index: number) => {
    let serviceData = [...service];
    serviceData[getIndexById(id)].fees[index].pet_size_id = data.key;
    setService(serviceData);
  };

  const getFeesFromService = (id) => {
    let serviceData = [...service];
    if (getIndexById(id) != -1) {
      return serviceData[getIndexById(id)].fees;
    }
    return [];
  };

  const addPetServiceFees = (id) => {
    let serviceData = [...service];
    var selectedSize = null;
    petSize.forEach((size) => {
      let hasIt = serviceData[getIndexById(id)].fees.some(
        (fees) => fees.pet_size_id == size.value
      );
      if (hasIt == false) {
        selectedSize = size.value;
      }
    });
    if (selectedSize) {
      serviceData[getIndexById(id)].fees.push({
        ...serviceFeeObject,
        pet_size_id: selectedSize,
      });
      setService(serviceData);
    } else {
      openError(strings.CannotAddMore);
    }
  };

  const increaseCapacity = (id, index) => {
    let serviceData = JSON.parse(JSON.stringify(service));
    let mIndex = getIndexById(id);
    serviceData[mIndex].fees[index].capacity =
      serviceData[mIndex].fees[index].capacity + 1;
    setService(serviceData);
  };

  const decreaseCapacity = (id, index) => {
    let serviceData: I_BOARDING_SERVICE_PET[] = JSON.parse(
      JSON.stringify(service)
    );
    let mIndex = getIndexById(id);

    if (serviceData[mIndex].fees[index].capacity > 1) {
      serviceData[mIndex].fees[index].capacity =
        serviceData[mIndex].fees[index].capacity - 1;
    }
    setService(serviceData);
  };

  const onChargeChange = (id, index, e: input) => {
    let serviceData: I_BOARDING_SERVICE_PET[] = deepClone(service);
    serviceData[getIndexById(id)].fees[index].service_charge = e.target.value;
    serviceData[getIndexById(id)].fees[index].earning_amount = getPercent(
      e.target.value
    )
      .toFixed(0)
      .toString();

    setService(serviceData);
  };

  const onTextChange = (e: input) => {
    e.preventDefault();
    if (Object.keys(errors).includes(e.target.name)) {
      let newValue = { ...errors };
      delete newValue[e.target.name];
      setErrors(newValue);
    }
    switch (e.target.name) {
      case "holidayCharges":
        if (e.target.value !== "-" && e.target.value !== "e") {
          setHolidayCharges(e.target.value);
        }

        break;
    }
  };

  const changeCancellationPolicy = (e: input) => {
    switch (e.target.id) {
      case "flexible":
        setCancellationPolicy(1);
        break;
      case "moderate":
        setCancellationPolicy(2);
        break;
      case "strict":
        setCancellationPolicy(3);
        break;
    }
  };

  const deletePetServiceFee = (id: number, index: number) => {
    let serviceData: I_BOARDING_SERVICE_PET[] = deepClone(service);
    serviceData[getIndexById(id)].fees.splice(index, 1);
    setService(serviceData);
  };

  const addAdditionalService = (id: number, data: I_PET_ADDITIONAL_SERVICE) => {
    let serviceData: I_BOARDING_SERVICE_PET[] = deepClone(service);
    serviceData[getIndexById(id)].custom_services.push(data);
    setService(serviceData);
    setSideBarData(serviceData[getIndexById(id)]);
  };

  const editAdditionalService = (
    id: number,
    data: I_PET_ADDITIONAL_SERVICE,
    index: number
  ) => {
    let serviceData: I_BOARDING_SERVICE_PET[] = deepClone(service);
    let serviceIndex = getIndexById(id);
    serviceData[serviceIndex].custom_services[index].description =
      data.description;
    serviceData[serviceIndex].custom_services[index].name = data.name;
    serviceData[serviceIndex].custom_services[index].price = data.price;
    setService(serviceData);
    setSideBarData(serviceData[serviceIndex]);
  };

  const removeAdditonalService = (id: number, index: number) => {
    let serviceData: I_BOARDING_SERVICE_PET[] = deepClone(service);
    serviceData[getIndexById(id)].custom_services.splice(index, 1);
    setService(serviceData);
    setSideBarData(serviceData[getIndexById(id)]);
  };

  const filterPetSize = (id: number) => {
    let serviceData = [...service];
    return petSize.filter((size) => {
      let hasIt = serviceData[getIndexById(id)].fees.some(
        (fees) => fees.pet_size_id == size.value
      );
      return !hasIt;
    });
  };

  const validateData = () => {
    service.forEach((value, index) => {
      value.fees.forEach((mValue, mIndex) => {
        if (Number(mValue.service_charge) < 10) {
          return false;
        }
      });
    });

    return true;
  };
  const saveServiceAndFee = async () => {
    if (!holidayCharges) {
      setErrors({ ...errors, holiday_extra_charges: true });
      openError(strings.PleaseAddHolidayServiceCharge);
      return false;
    }
    if (!service.length) {
      openError(strings.SelectAtleastOneServicePet);
      return false;
    }
    let payload: any = {
      service_pets: service,
      holiday_extra_charges: holidayCharges,
      cancellation_policy: cancellationPolicy,
    };
    var call;
    switch (serviceId) {
      case 1:
        payload.boarding_service_id = sitterServiceId;
        call = api.boardingServiceFee(JSON.stringify(payload));
        break;
      case 2:
        payload.house_sitting_service_id = sitterServiceId;
        call = api.houseSittingServiceFee(JSON.stringify(payload));
        break;
      case 3:
        payload.drop_in_visit_service_id = sitterServiceId;
        call = api.dropInVisitsServiceFee(JSON.stringify(payload));
        break;
      case 4:
        payload.day_care_service_id = sitterServiceId;
        call = api.petDayCateServiceFee(JSON.stringify(payload));
        break;
    }

    if (service.length > 0) {
      if (validateData) {
        call
          .then((response) => {
            openSnackbar(strings.SavedSuccessfully);
            handleTabChange(1);
          })
          .catch((error) => openError(error.response.data.message));
      }
    } else {
      openError(strings.AddAtleastOneService);
    }
  };

  return (
    <>
      <div className="tab-content" id="myTabContent">
        <div
          className="tab-pane fade active show"
          id="home"
          role="tabpanel"
          aria-labelledby="home-tab">
          <div className="fees-content mb-3">
            <h6 className="font-semibold font-14 mb-0">
              {strings.whatEverPetServiceYouProvide}
            </h6>
            <p className="mb-0 font-12">
              {strings.boardingServicedescription +
                (serviceId === 1
                  ? strings.nightBoarding
                  : serviceId === 2
                  ? strings.houseSitting
                  : serviceId === 4
                  ? strings.petDayCare
                  : "")}
            </p>
            <div className="booking-for">
              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          className="class1"
                          name="dog"
                          defaultValue="dog1"
                          checked={activeService.dog}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Dog}
                      </label>
                    </div>
                    <div
                      className="hidden cap-div"
                      id="hidden_fields_one"
                      style={activeService.dog ? {} : { display: "none" }}>
                      {getFeesFromService(1).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Capacity}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.HandlingPetsAtMentionedPrice}</p>
                                </div>
                              </div>
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span
                                    onClick={() => decreaseCapacity(1, index)}
                                    className="minus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                  <input
                                    id={`dog${index}`}
                                    key={index}
                                    type="text"
                                    readOnly={true}
                                    className="in-num"
                                    value={value.capacity}
                                  />
                                  <span
                                    onClick={() => increaseCapacity(1, index)}
                                    className="plus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12M12 18V6"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetSize}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {strings.SizeYouAreGoingToHandleInThisPrice}
                                  </p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 1, index)
                                    }
                                    options={filterPetSize(1)}
                                    value={petSize.find(
                                      (v) => value.pet_size_id == v.key
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(1, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(1).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(1, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}

                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 1 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(1)]
                                        );
                                        setShowAdditionalServiceModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>

                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}

                      <div className="row align-items-center justify-content-between">
                        <div className="col-md-auto">
                          <button
                            onClick={() => addPetServiceFees(1)}
                            className="a btn btn-primary add-more-btn">
                            + {strings.Addmore}
                          </button>
                        </div>
                        <div className="col-md-auto">
                          <div data-toggle="modal" data-target="#services">
                            <a
                              onClick={() => {
                                setSideBarData(service[getIndexById(1)]);
                                setAdditionalEditService(null);
                                setShowAdditionalServiceModal(true);
                              }}
                              className="font-10 text-muted text-center text-md-left d-block cursor-pointer">
                              {strings.WantToAddOptionalServicesAndChargesAlongWith +
                                (serviceId === 1
                                  ? strings.DogBoarding_Q
                                  : serviceId === 2
                                  ? strings.houseSitting_Q
                                  : serviceId === 4
                                  ? strings.petDayCare_Q
                                  : "")}
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          name="cat"
                          checked={activeService.cat}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Cat}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={activeService.cat ? {} : { display: "none" }}>
                      {getFeesFromService(2).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Capacity}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.HandlingPetsAtMentionedPrice}</p>
                                </div>
                              </div>
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span
                                    onClick={() => decreaseCapacity(2, index)}
                                    className="minus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                  <input
                                    id={`cat${index}`}
                                    key={index}
                                    type="text"
                                    readOnly={true}
                                    className="in-num"
                                    value={value.capacity}
                                  />
                                  <span
                                    onClick={() => increaseCapacity(2, index)}
                                    className="plus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12M12 18V6"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetSize}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {strings.SizeYouAreGoingToHandleInThisPrice}
                                  </p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 2, index)
                                    }
                                    options={filterPetSize(2)}
                                    value={petSize.find(
                                      (v) => value.pet_size_id == v.key
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(2, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(2).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(2, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}

                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 2 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(2)]
                                        );
                                        setShowAdditionalServiceModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}

                      <div className="row align-items-center justify-content-between">
                        <div className="col-md-auto">
                          <button
                            onClick={() => addPetServiceFees(2)}
                            className="a btn btn-primary add-more-btn">
                            + {strings.Addmore}
                          </button>
                        </div>
                        <div className="col-md-auto">
                          <div data-toggle="modal" data-target="#services">
                            <a
                              onClick={() => {
                                setSideBarData(service[getIndexById(2)]);
                                setAdditionalEditService(null);
                                setShowAdditionalServiceModal(true);
                              }}
                              className="font-10 text-muted text-center text-md-left d-block cursor-pointer">
                              {strings.WantToAddOptionalServicesAndChargesAlongWith +
                                (serviceId === 1
                                  ? strings.CatBoarding_Q
                                  : serviceId === 2
                                  ? strings.houseSitting_Q
                                  : serviceId === 4
                                  ? strings.petDayCare_Q
                                  : "")}
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/*--------/cat-----------*/}

              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          name="birds"
                          checked={activeService.birds}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Birds}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={activeService.birds ? {} : { display: "none" }}>
                      {getFeesFromService(3).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Capacity}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.HandlingPetsAtMentionedPrice}</p>
                                </div>
                              </div>
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span
                                    onClick={() => decreaseCapacity(3, index)}
                                    className="minus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                  <input
                                    id={`cat${index}`}
                                    key={index}
                                    type="text"
                                    readOnly={true}
                                    className="in-num"
                                    value={value.capacity}
                                  />
                                  <span
                                    onClick={() => increaseCapacity(3, index)}
                                    className="plus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12M12 18V6"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetSize}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {strings.SizeYouAreGoingToHandleInThisPrice}
                                  </p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 3, index)
                                    }
                                    options={filterPetSize(3)}
                                    value={petSize.find(
                                      (v) => value.pet_size_id == v.key
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(3, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(3).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(3, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 3 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(3)]
                                        );
                                        setShowAdditionalServiceModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                   <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}
                      <div className="row align-items-center justify-content-between">
                        <div className="col-md-auto">
                          <button
                            onClick={() => addPetServiceFees(3)}
                            className="a btn btn-primary add-more-btn">
                            + {strings.Addmore}
                          </button>
                        </div>
                        <div className="col-md-auto">
                          <div data-toggle="modal" data-target="#services">
                            <a
                              onClick={() => {
                                setSideBarData(service[getIndexById(3)]);
                                setAdditionalEditService(null);
                                setShowAdditionalServiceModal(true);
                              }}
                              className="font-10 text-muted text-center text-md-left d-block cursor-pointer">
                              {strings.WantToAddOptionalServicesAndChargesAlongWith +
                                (serviceId === 1
                                  ? strings.BirdsBoarding_Q
                                  : serviceId === 2
                                  ? strings.houseSitting_Q
                                  : serviceId === 4
                                  ? strings.petDayCare_Q
                                  : "")}
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/*--------/birds-----------*/}
              {/*--------Reptiles-----------*/}
              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          name="reptile"
                          checked={activeService.reptile}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Reptiles}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={activeService.reptile ? {} : { display: "none" }}>
                      {getFeesFromService(4).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Capacity}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.HandlingPetsAtMentionedPrice}</p>
                                </div>
                              </div>
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span
                                    onClick={() => decreaseCapacity(4, index)}
                                    className="minus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                  <input
                                    id={`cat${index}`}
                                    key={index}
                                    type="text"
                                    readOnly={true}
                                    className="in-num"
                                    value={value.capacity}
                                  />
                                  <span
                                    onClick={() => increaseCapacity(4, index)}
                                    className="plus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12M12 18V6"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetSize}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {strings.SizeYouAreGoingToHandleInThisPrice}
                                  </p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 4, index)
                                    }
                                    options={filterPetSize(4)}
                                    value={petSize.find(
                                      (v) => value.pet_size_id == v.key
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(4, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(4).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(4, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 4 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(4)]
                                        );
                                        setShowAdditionalServiceModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512"
                                    
                                        onClick={() =>
                                          deletePetServiceFee(4, index)
                                        }>
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}
                      <div className="row align-items-center justify-content-between">
                        <div className="col-md-auto">
                          <button
                            onClick={() => addPetServiceFees(4)}
                            className="a btn btn-primary add-more-btn">
                            + {strings.Addmore}
                          </button>
                        </div>
                        <div className="col-md-auto">
                          <div data-toggle="modal" data-target="#services">
                            <a
                              onClick={() => {
                                setSideBarData(service[getIndexById(4)]);
                                setAdditionalEditService(null);
                                setShowAdditionalServiceModal(true);
                              }}
                              className="font-10 text-muted text-center text-md-left d-block cursor-pointer">
                              {strings.WantToAddOptionalServicesAndChargesAlongWith +
                                (serviceId === 1
                                  ? strings.ReptilesBoarding_Q
                                  : serviceId === 2
                                  ? strings.houseSitting_Q
                                  : serviceId === 4
                                  ? strings.petDayCare_Q
                                  : "")}
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          checked={activeService.smallAnimals}
                          onChange={onChange}
                          type="checkbox"
                          name="smallAnimals"
                        />
                        <span className="checkmark" />
                        {strings.Smallanimals}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={
                        activeService.smallAnimals ? {} : { display: "none" }
                      }>
                      {getFeesFromService(5).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Capacity}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.HandlingPetsAtMentionedPrice}</p>
                                </div>
                              </div>
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span
                                    onClick={() => decreaseCapacity(5, index)}
                                    className="minus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                  <input
                                    id={`cat${index}`}
                                    key={index}
                                    type="text"
                                    readOnly={true}
                                    className="in-num"
                                    value={value.capacity}
                                  />
                                  <span
                                    onClick={() => increaseCapacity(5, index)}
                                    className="plus">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="32"
                                      height="32"
                                      viewBox="0 0 24 24"
                                      fill="none">
                                      <path
                                        d="M6 12h12M12 18V6"
                                        stroke="#FF8A65"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"></path>
                                    </svg>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetSize}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {strings.SizeYouAreGoingToHandleInThisPrice}
                                  </p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 5, index)
                                    }
                                    options={filterPetSize(5)}
                                    value={petSize.find(
                                      (v) => value.pet_size_id == v.key
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(5, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(5).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(5, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 5 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(5)]
                                        );
                                        setShowAdditionalServiceModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"
                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}
                      <div className="row align-items-center justify-content-between">
                        <div className="col-md-auto">
                          <button
                            onClick={() => addPetServiceFees(5)}
                            className="a btn btn-primary add-more-btn">
                            + {strings.Addmore}
                          </button>
                        </div>
                        <div className="col-md-auto">
                          <div data-toggle="modal" data-target="#services">
                            <a
                              onClick={() => {
                                setSideBarData(service[getIndexById(5)]);
                                setAdditionalEditService(null);
                                setShowAdditionalServiceModal(true);
                              }}
                              className="font-10 text-muted text-center text-md-left d-block cursor-pointer">
                              {strings.WantToAddOptionalServicesAndChargesAlongWith +
                                (serviceId === 1
                                  ? strings.SmallAnimalsBoarding_Q
                                  : serviceId === 2
                                  ? strings.houseSitting_Q
                                  : serviceId === 4
                                  ? strings.petDayCare_Q
                                  : "")}
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr />
          <div className="extra-charge">
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.WouldYouLikeToChargeExtraWhenProviding +
                " " +
                (serviceId === 1
                  ? strings.Boarding
                  : serviceId === 2
                  ? strings.houseSitting
                  : serviceId === 4
                  ? strings.petDayCare
                  : "") +
                strings.ServiceDuringHolidays}
              <span className="sign" style={{ color: "red" }}>
                *
              </span>
            </p>
            <p className="mb-0 font-12">
              {
                strings.EnterValueByWhichYouWouldWantPricesToIncreaseDuringHolidaysInBelowBoxLookAtlistOfHolidaysHere
              }
            </p>
            <div className="d-flex service-charges">
              <div className="charge-input">
                <input
                  value={holidayCharges}
                  onChange={onTextChange}
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  type="text"
                  min={0}
                  maxLength={3}
                  max={100}
                  name="holidayCharges"
                  className={
                    "form-control valid-control " +
                    (errors.holiday_extra_charges ? "invalid" : " ")
                  }
                  id="charge"
                  placeholder={"0"}
                />
              </div>
              <div className="ml-3">
                <p className="font-12 mb-0">
                  {strings.RangeYouCan} <br />
                  {strings.increaseIs0to100}
                </p>
              </div>
            </div>
          </div>
          <hr />
          <div className="fees-content my-3">
            <h5 className="font-semibold mb-1">{strings.CancellationPolicy}</h5>
            <p className="mb-0 font-12">
              {
                strings.SelectCancellationPolicyForThisServiceUserWillSeeTheSameAtTheTimeOfBooking
              }
            </p>
            <div className="col-12">
              <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  <div className="row  service-charges">
                    <div className="custom-check">
                      <label className="check  mb-2">
                        <input
                          type="radio"
                          id="flexible"
                          checked={cancellationPolicy == 1}
                          onChange={changeCancellationPolicy}
                          name="is_name1"
                        />
                        <span className="checkmark left-0" />
                        {strings.Flexible}
                      </label>
                      <p className="font-12">
                        {
                          strings.ForBoardingAndHouseSittingYouWillGetAFullRefundIfYouCancelBeforeTheStayBegins
                        }
                      </p>
                      <p className="font-12 mb-0">
                        {
                          strings.ForWalksDayCareAndDropinVisitsYouWillGetAFullRefundIfYouCancelBeforeTheDaysServiceIsDelivered
                        }
                      </p>
                    </div>
                  </div>
                  <hr />
                  {/*--------/Flexible-----------*/}
                  {/*--------Flexible-----------*/}
                  <div className="row service-charges">
                    <div className="custom-check">
                      <label className="check mb-2">
                        <input
                          type="radio"
                          name="is_name1"
                          id="moderate"
                          checked={cancellationPolicy == 2}
                          onChange={changeCancellationPolicy}
                        />
                        <span className="checkmark left-0" />
                        {strings.Moderate}
                      </label>
                      <p className="font-12 mb-2">
                        {strings.YouCancelWithin48HoursOfBooking}
                      </p>
                      <p className="font-12  mb-2">
                        {
                          strings.YouHavenotAlreadyCancelled3ReservationsInTheLast12months
                        }
                      </p>
                      <p className="font-12  mb-2">
                        {
                          strings.TheReservationYouareCancellingDoesnotOverlapWithAnotherReservation
                        }
                      </p>
                      <p className="font-12  mb-0">
                        {strings.YouWillGetAFullRefundIfYouCancelBy1200Noon3}
                      </p>
                    </div>
                  </div>
                  <hr />
                  {/*--------/Flexible-----------*/}
                  {/*--------Flexible-----------*/}
                  <div className="row service-charges mb-0">
                    <div className="custom-check">
                      <label className="check mb-2">
                        <input
                          type="radio"
                          name="is_name1"
                          id="strict"
                          checked={cancellationPolicy == 3}
                          onChange={changeCancellationPolicy}
                        />
                        <span className="checkmark left-0" />
                        {strings.Strict}
                      </label>
                      <p className="font-12 mb-2">
                        {strings.YouWillGetAFullRefundIfYouCancelBy1200NoonOne}
                      </p>
                    </div>
                  </div>
                  {/*--------/Flexible-----------*/}
                </div>
              </div>
            </div>
          </div>
          <hr />
        </div>
      </div>
      <SaveServiceView
        preview={null}
        onPreview={() => {}}
        onSave={(preview) => saveServiceAndFee()}
      />

      <AdditonalServiceSidebar
        showModal={showAdditionalServiceModal}
        setShowModal={() => setShowAdditionalServiceModal(false)}
        data={sideBarData}
        addData={addAdditionalService}
        removeData={removeAdditonalService}
        editData={editAdditionalService}
        additionalEditService={additionalEditService}
      />
    </>
  );
};

export default ServiceFee;
