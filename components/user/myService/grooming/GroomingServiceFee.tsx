import Select from "react-select";
import React, { useEffect, useState } from "react";
import { strings } from "../../../../public/lang/Strings";
import {
  I_BOARDING_SERVICE,
  I_BOARDING_SERVICE_PET,
  I_PET_ADDITIONAL_SERVICE,
} from "../../../../models/boardingService.interface";
import {
  durations,
  errorOptions,
  petSize,
  select,
  serviceFeeObject,
  successOptions,
} from "../../../../public/appData/AppData";
import {
  deepClone,
  getDropInServiceObject,
  getGroomingServiceObject,
  getPercent,
  getServiceObject,
} from "../../../../utils/Helper";
import SaveServiceView from "../SaveServiceView";
import AdditonalServiceSidebar from "../AdditionalServiceSidebar";
import API from "../../../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import { useRouter } from "next/router";
import { AxiosResponse } from "axios";
import Res from "../../../../models/response.interface";
import {
  I_DROP_IN_SERVICE,
  I_DROP_IN_SERVICE_PET,
} from "../../../../models/dropInService.interface";
import {
  I_GROOMING_PET,
  I_GROOMING_SERVICE,
} from "../../../../models/groomingService.interface";
import { getCurrencySign } from "../../../../api/Constants";
import { trTR } from "@mui/material/locale";

type input = React.ChangeEvent<HTMLInputElement>;
const api = new API();

interface I_Props {
  handleTabChange: (x: number) => void;
}

const GroomingServiceFee: React.FC<I_Props> = ({
  handleTabChange,
}: I_Props) => {
  const router = useRouter();
  const [showAdditionalModal, setShowAdditionalModal] = useState(false);
  const [sitterServiceId, setSitterServiceId] = useState<number>();
  const [service, setService] = useState<I_GROOMING_PET[]>([]);
  const [holidayCharges, setHolidayCharges] = useState<string>("0");
  const [cancellationPolicy, setCancellationPolicy] = useState<number>(1);
  const [sideBarData, setSideBarData] = useState<I_GROOMING_PET>();
  const [errors, setErrors] = useState<any>({});
  const [openSnackbar, closeSnackbar] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [dogService, setDogService] = useState<any>([]);
  const [catService, setCatService] = useState<any>([]);
  const [birdService, setBirdService] = useState<any>([]);
  const [reptileService, setReptileService] = useState<any>([]);
  const [smallAnimalService, setSmallAnimalService] = useState<any>([]);
  const [additionalEditService, setAdditionalEditService] = useState<any>(null);
  const [sitterGroomingServiceId, setSitterGroomingServiceId] =
    useState<any>(null);

  const [activeService, setActiveService] = useState({
    dog: false,
    cat: false,
    birds: false,
    reptile: false,
    smallAnimals: false,
  });

  useEffect(() => {
    if (router?.query?.serviceId) {
      setSitterServiceId(Number(router?.query?.serviceId));
      getServiceInfo();
    }
  }, [router?.query]);

  const getServiceInfo = () => {
    api
      .getGroomingInfo(1)
      .then((response: AxiosResponse<Res<I_GROOMING_SERVICE>>) => {
        setService(response.data.response.service_pets);
        setHolidayCharges(`${response.data.response.holiday_extra_charges}`);
        setCancellationPolicy(response.data.response.cancellation_policy);
        setSitterGroomingServiceId(
          response.data.response.sitter_grooming_services_id
        );
        let data = response.data.response.service_pets;
        let object = { ...activeService };
        for (let i = 0; i < data.length; i++) {
          switch (data[i].pet_type) {
            case 1:
              object.dog = true;
              getServiceByPet(1, true);
              break;
            case 2:
              object.cat = true;
              getServiceByPet(2, true);
              break;
            case 3:
              getServiceByPet(3, true);
              object.birds = true;
              break;
            case 4:
              getServiceByPet(4, true);
              object.reptile = true;
              break;
            case 5:
              getServiceByPet(5, true);
              object.smallAnimals = true;
              break;
          }
        }
        setActiveService({ ...activeService, ...object });
      })

      .catch((error) => console.log(error));
  };

  const getIndexById = (id) => {
    let serviceData = [...service];
    return serviceData.findIndex((value) => value.pet_type == id);
  };

  const onChange = (event: input) => {
    switch (event.target.name) {
      case "dog":
        event.target.checked
          ? getServiceByPet(1, false)
          : addPetService(false, 1, null);
        setActiveService({ ...activeService, dog: event.target.checked });
        break;
      case "cat":
        event.target.checked
          ? getServiceByPet(2, false)
          : addPetService(false, 2, null);
        setActiveService({ ...activeService, cat: event.target.checked });
        break;
      case "birds":
        event.target.checked
          ? getServiceByPet(3, false)
          : addPetService(false, 3, null);
        setActiveService({ ...activeService, birds: event.target.checked });
        break;
      case "reptile":
        event.target.checked
          ? getServiceByPet(4, false)
          : addPetService(false, 4, null);
        setActiveService({ ...activeService, reptile: event.target.checked });
        break;
      case "smallAnimals":
        event.target.checked
          ? getServiceByPet(5, false)
          : addPetService(false, 5, null);
        setActiveService({
          ...activeService,
          smallAnimals: event.target.checked,
        });
        break;
    }
  };

  const removePetService = (id) => {
    let serviceData = [...service];
    let index = getIndexById(id);
    if (serviceData[index]?.id) {
      api
        .deleteGroomingServiceFee(
          serviceData[index]?.pet_type,
          sitterGroomingServiceId
        )
        .then((res) => {
          openSnackbar(res.data?.message);
        })
        .catch((error) => openError(error.response.data.message));
    }
    serviceData.splice(index, 1);
    setService(serviceData);
  };

  const addPetService = (boolean, id, service_id) => {
    if (boolean) {
      setService([...service, getGroomingServiceObject(id, service_id)]);
    } else {
      removePetService(id);
    }
  };

  const onPetSizeChange = (data: any, id: number, index: number) => {
    let serviceData = [...service];
    serviceData[getIndexById(id)].fees[index].grooming_service_id = data.id;
    setService(serviceData);
  };

  const getFeesFromService = (id) => {
    let serviceData = [...service];
    if (getIndexById(id) != -1) {
      return serviceData[getIndexById(id)].fees;
    }
    return [];
  };

  const addPetServiceFees = async (id) => {
    let serviceData = [...service];
    let currServices = null;
    switch (id) {
      case 1:
        currServices = [...dogService];
        break;
      case 2:
        currServices = [...catService];
        break;
      case 3:
        currServices = [...birdService];
        break;
      case 4:
        currServices = [...reptileService];
        break;
      case 5:
        currServices = [...smallAnimalService];
        break;
    }
    var selectedSize = null;
    await currServices.forEach((size) => {
      let hasIt = serviceData[getIndexById(id)].fees.some(
        (fees) => fees.grooming_service_id == size.id
      );
      if (hasIt == false) {
        selectedSize = size.id;
      }
    });
    if (selectedSize) {
      serviceData[getIndexById(id)].fees.push({
        earning_amount: "640",
        service_charge: "800",
        grooming_service_id: selectedSize,
      });
      setService(serviceData);
    } else {
      openError(strings.CannotAddMore);
    }
  };

  const onChargeChange = (id, index, e: input) => {
    let serviceData: I_GROOMING_PET[] = deepClone(service);
    serviceData[getIndexById(id)].fees[index].service_charge = e.target.value;
    serviceData[getIndexById(id)].fees[index].earning_amount = getPercent(
      e.target.value
    )
      .toFixed(0)
      .toString();

    setService(serviceData);
  };

  const onTextChange = (e: input) => {
    switch (e.target.name) {
      case "holidayCharges":
        if (e.target.value !== "-" && e.target.value !== "e") {
          setHolidayCharges(e.target.value);
        }

        break;
    }
  };

  const changeCancellationPolicy = (e: input) => {
    switch (e.target.id) {
      case "flexible":
        setCancellationPolicy(1);
        break;
      case "moderate":
        setCancellationPolicy(2);
        break;
      case "strict":
        setCancellationPolicy(3);
        break;
    }
  };

  const deletePetServiceFee = (id: number, index: number) => {
    let serviceData: I_GROOMING_PET[] = deepClone(service);
    serviceData[getIndexById(id)].fees.splice(index, 1);
    console.log(serviceData);
    setService(serviceData);
  };

  const addAdditionalService = (id: number, data: I_PET_ADDITIONAL_SERVICE) => {
    let serviceData: I_GROOMING_PET[] = deepClone(service);
    serviceData[getIndexById(id)].custom_services.push(data);
    setService(serviceData);
    setSideBarData(serviceData[getIndexById(id)]);
  };

  const editAdditionalService = (
    id: number,
    data: I_PET_ADDITIONAL_SERVICE,
    index: number
  ) => {
    let serviceData: I_GROOMING_PET[] = deepClone(service);
    let serviceIndex = getIndexById(id);
    serviceData[serviceIndex].custom_services[index].description =
      data.description;
    serviceData[serviceIndex].custom_services[index].name = data.name;
    serviceData[serviceIndex].custom_services[index].price = data.price;
    setService(serviceData);
    setSideBarData(serviceData[serviceIndex]);
  };

  const removeAdditonalService = (id: number, index: number) => {
    let serviceData: I_GROOMING_PET[] = deepClone(service);
    serviceData[getIndexById(id)].custom_services.splice(index, 1);
    setService(serviceData);
    setSideBarData(serviceData[getIndexById(id)]);
  };

  const filterPetSize = (id: number, data: any) => {
    let serviceData = [...service];

    return data.filter((size) => {
      let hasIt = serviceData[getIndexById(id)].fees.some(
        (fees) => fees.grooming_service_id == size.id
      );
      return !hasIt;
    });
  };

  const validateData = () => {
    service.forEach((value, index) => {
      value.fees.forEach((mValue, mIndex) => {
        if (Number(mValue.service_charge) < 10) {
          return false;
        }
      });
    });

    return true;
  };
  const saveServiceAndFee = () => {
    if (!service) {
      openError(strings.AddAtleastOneService);
    }
    if (!holidayCharges) {
      openError(strings.PleaseAddHolidayCharges);
    }
    let payload: any = {
      service_pets: service,
      holiday_extra_charges: holidayCharges,
      cancellation_policy: cancellationPolicy,
    };

    if (service.length > 0) {
      if (validateData) {
        payload.sitter_grooming_service_id = sitterServiceId;
        api
          .groomingServiceFee(JSON.stringify(payload))
          .then((response) => {
            openSnackbar(strings.SavedSuccessfully);
            handleTabChange(1);
          })
          .catch((error) => openError(error.response.data.message));
      }
    } else {
      openError(strings.AddAtleastOneService);
    }
  };

  const getServiceByPet = (id: number, isFirstTime) => {
    api
      .getGroomingService({
        pet_type: id,
      })
      .then((res) => {
        switch (id) {
          case 1:
            setDogService(res.data.response);
            break;
          case 2:
            setCatService(res.data.response);
            break;
          case 3:
            setBirdService(res.data.response);
            break;
          case 4:
            setReptileService(res.data.response);
            break;
          case 5:
            setSmallAnimalService(res.data.response);
            break;
        }
        isFirstTime == true
          ? null
          : addPetService(true, id, res.data.response[0].id);
      })
      .catch((error) => console.log(error));
  };

  return (
    <>
      <div className="tab-content" id="myTabContent">
        <div
          className="tab-pane fade active show"
          id="home"
          role="tabpanel"
          aria-labelledby="home-tab">
          <div className="fees-content my-3">
            <h6 className="font-semibold font-14 mb-0">
              {strings.whatEverPetServiceYouProvide}
            </h6>
            <p className="mb-0 font-12">
              {strings.boardingServicedescription + strings.PetGrooming}
            </p>
            <div className="booking-for">
              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          className="class1"
                          name="dog"
                          defaultValue="dog1"
                          checked={activeService.dog}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Dog}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      id="hidden_fields_one"
                      style={activeService.dog ? {} : { display: "none" }}>
                      {getFeesFromService(1).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetService}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.PetService}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 1, index)
                                    }
                                    getOptionLabel={(v) => v.name}
                                    getOptionValue={(v) => v.id}
                                    options={filterPetSize(1, dogService)}
                                    value={dogService.find(
                                      (v) => v.id == value.grooming_service_id
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>

                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(1, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(1).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(1, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 1 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(1)]
                                        );
                                        setShowAdditionalModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}

                      <button
                        onClick={() => addPetServiceFees(1)}
                        className="a btn btn-primary add-more-btn">
                        + {strings.Addmore}
                      </button>

                      <div data-toggle="modal" data-target="#services">
                        <a
                          onClick={() => {
                            setSideBarData(service[getIndexById(1)]);
                            setAdditionalEditService(null)
                            setShowAdditionalModal(true);
                          }}
                          className="font-10 text-muted text-center text-md-left d-block">
                          {strings.WantToAddOptionalServicesAndChargesAlongWith +
                            strings.petGrooming_Q}
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          name="cat"
                          checked={activeService.cat}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Cat}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={activeService.cat ? {} : { display: "none" }}>
                      {getFeesFromService(2).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetService}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.PetService}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 2, index)
                                    }
                                    getOptionLabel={(v) => v.name}
                                    getOptionValue={(v) => v.id}
                                    options={filterPetSize(2, catService)}
                                    value={catService.find(
                                      (v) => v.id == value.grooming_service_id
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>

                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(2, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(2).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(2, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 2 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(2)]
                                        );
                                        setShowAdditionalModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}

                      <button
                        onClick={() => addPetServiceFees(2)}
                        className="a btn btn-primary add-more-btn">
                        + {strings.Addmore}
                      </button>
                      <div data-toggle="modal" data-target="#services">
                        <a
                          onClick={() => {
                            setSideBarData(service[getIndexById(2)]);
                            setAdditionalEditService(null)
                            setShowAdditionalModal(true);
                          }}
                          className="font-10 text-muted text-center text-md-left d-block">
                          +{" "}
                          {strings.WantToAddOptionalServicesAndChargesAlongWith +
                            strings.petGrooming_Q}
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/*--------/cat-----------*/}

              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          name="birds"
                          checked={activeService.birds}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Birds}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={activeService.birds ? {} : { display: "none" }}>
                      {getFeesFromService(3).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetService}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.PetService}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 3, index)
                                    }
                                    getOptionLabel={(v) => v.name}
                                    getOptionValue={(v) => v.id}
                                    options={filterPetSize(3, birdService)}
                                    value={birdService.find(
                                      (v) => v.id == value.grooming_service_id
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>

                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(3, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(3).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(3, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 3 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(3)]
                                        );
                                        setShowAdditionalModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}

                      <button
                        onClick={() => addPetServiceFees(3)}
                        className="a btn btn-primary add-more-btn">
                        + {strings.Addmore}
                      </button>
                      <div data-toggle="modal" data-target="#services">
                        <a
                          onClick={() => {
                            setSideBarData(service[getIndexById(3)]);
                            setAdditionalEditService(null)
                            setShowAdditionalModal(true);
                          }}
                          className="font-10 text-muted text-center text-md-left d-block">
                          {strings.WantToAddOptionalServicesAndChargesAlongWith +
                            strings.petGrooming_Q}
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/*--------/birds-----------*/}
              {/*--------Reptiles-----------*/}
              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          type="checkbox"
                          name="reptile"
                          checked={activeService.reptile}
                          onChange={onChange}
                        />
                        <span className="checkmark" />
                        {strings.Reptiles}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={activeService.reptile ? {} : { display: "none" }}>
                      {getFeesFromService(4).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetService}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.PetService}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 4, index)
                                    }
                                    getOptionLabel={(v) => v.name}
                                    getOptionValue={(v) => v.id}
                                    options={filterPetSize(4, reptileService)}
                                    value={reptileService.find(
                                      (v) => v.id == value.grooming_service_id
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>

                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(4, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(4).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(4, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 4 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(4)]
                                        );
                                        setShowAdditionalModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}

                      <button
                        onClick={() => addPetServiceFees(4)}
                        className="a btn btn-primary add-more-btn">
                        + {strings.Addmore}
                      </button>
                      <div data-toggle="modal" data-target="#services">
                        <a
                          onClick={() => {
                            setSideBarData(service[getIndexById(4)]);
                            setAdditionalEditService(null)
                            setShowAdditionalModal(true);
                          }}
                          className="font-10 text-muted text-center text-md-left d-block">
                          {strings.WantToAddOptionalServicesAndChargesAlongWith +
                            strings.petGrooming_Q}
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-0 col-12 service-charges">
                <div className="row">
                  <div className="col-12">
                    <div className="custom-check mb-2">
                      <label className="check ">
                        <input
                          checked={activeService.smallAnimals}
                          onChange={onChange}
                          type="checkbox"
                          name="smallAnimals"
                        />
                        <span className="checkmark" />
                        {strings.Smallanimals}
                      </label>
                    </div>
                    <div
                      className="hidden"
                      style={
                        activeService.smallAnimals ? {} : { display: "none" }
                      }>
                      {getFeesFromService(5).map((value, index) => (
                        <div key={index} className="col-12 pet-opt pr-2 pl-4">
                          <div className="row">
                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.PetService}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.PetService}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <div className="category-selection charge-select">
                                  <Select
                                    onChange={(data) =>
                                      onPetSizeChange(data, 5, index)
                                    }
                                    getOptionLabel={(v) => v.name}
                                    getOptionValue={(v) => v.id}
                                    options={filterPetSize(
                                      5,
                                      smallAnimalService
                                    )}
                                    value={smallAnimalService.find(
                                      (v) => v.id == value.grooming_service_id
                                    )}
                                    isSearchable={false}
                                  />
                                </div>
                              </div>
                            </div>

                            <div className="form-group col-6 col-md">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.ChargesX1}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>{strings.AddChargesAsPerThePetSelected}</p>
                                </div>
                              </div>
                              <div className="form-group mb-0">
                                <input
                                  className={
                                    "form-control valid-control " +
                                    (Number(value.service_charge) < 10
                                      ? "invalid"
                                      : "")
                                  }
                                  placeholder="0"
                                  type="text"
                                  onChange={(e) => onChargeChange(5, index, e)}
                                  value={value.service_charge}
                                  min={"10"}
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="col-6 col-md-3 col-lg-3 col-xl-2">
                              <div className="d-flex justify-content-between mb-2">
                                <div>
                                  <p className="font-12 mb-0">
                                    {strings.Earnings}
                                  </p>
                                </div>
                                <div className="help-tip">
                                  <p>
                                    {
                                      strings.TheAmountYouWillReceiveForTheService
                                    }
                                  </p>
                                </div>
                              </div>
                              <div className="earning-details">
                                <p className="font-14 mb-0">
                                  {getCurrencySign() + value.earning_amount}
                                </p>
                              </div>
                            </div>
                            {getFeesFromService(5).length != 1 ? (
                              <div className="form-group col-12 col-md-auto my-auto">
                                <div className="delete-icon d-flex align-items-center">
                                  <svg
                                    className="mr-2"
                                    onClick={() =>
                                      deletePetServiceFee(5, index)
                                    }
                                    viewBox="0 0 448 512">
                                    <path
                                      fill="currentColor"
                                      d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                  </svg>
                                  <p className="m-0 text-danger">Delete</p>
                                </div>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      ))}
                      {service &&
                        service.map((item) => {
                          return (
                            item.pet_type == 5 &&
                            item.custom_services.map((val, index) => {
                              return (
                                <div
                                  className="d-flex justify-content-between align-items-center bg-white rounded-5 mt-2 py-2 px-2 border p-2 mb-3"
                                  key={index}>
                                  <p className="mb-0 font-semibold pl-1">
                                    {val.name}
                                    <span className="d-block font-12 font-weight-medium">
                                      {" "}
                                      ¥{val.price}
                                    </span>
                                  </p>
                                  <div className="pr-1">
                                    <a
                                      className="btn btn-accept btn-outline-primary border-secondary text-secondary p-1 px-2 mr-2 rounded-circle"
                                      onClick={() => {
                                        setSideBarData(
                                          service[getIndexById(5)]
                                        );
                                        setShowAdditionalModal(true);
                                        setAdditionalEditService(val);
                                      }}>
                                      {" "}
                                      <svg
                                        className="text-secondary"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        {" "}
                                        <path
                                          fill="currentColor"
                                          d="M421.7 220.3L188.5 453.4L154.6 419.5L158.1 416H112C103.2 416 96 408.8 96 400V353.9L92.51 357.4C87.78 362.2 84.31 368 82.42 374.4L59.44 452.6L137.6 429.6C143.1 427.7 149.8 424.2 154.6 419.5L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3zM492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75z"></path>{" "}
                                      </svg>{" "}
                                    </a>
                                    <a
                                      className="btn btn-accept btn-outline-primary border-danger text-danger p-1 px-2 rounded-circle"

                                      onClick={() =>
                                        removeAdditonalService(
                                          item.pet_type,
                                          index
                                        )
                                      }>
                                      <svg viewBox="0 0 448 512">
                                        <path
                                          fill="currentColor"
                                          d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              );
                            })
                          );
                        })}

                      <button
                        onClick={() => addPetServiceFees(5)}
                        className="a btn btn-primary add-more-btn">
                        + {strings.Addmore}
                      </button>
                      <div data-toggle="modal" data-target="#services">
                        <a
                          onClick={() => {
                            setSideBarData(service[getIndexById(5)]);
                            setAdditionalEditService(null)
                            setShowAdditionalModal(true);
                          }}
                          className="font-10 text-muted text-center text-md-left d-block">
                          {strings.WantToAddOptionalServicesAndChargesAlongWith +
                            strings.petGrooming_Q}
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr />
          <div className="extra-charge">
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.WouldYouLikeToChargeExtraWhenProviding +
                " " +
                strings.PetGrooming +
                strings.ServiceDuringHolidays}
            </p>
            <p className="mb-0 font-12">
              {
                strings.EnterValueByWhichYouWouldWantPricesToIncreaseDuringHolidaysInBelowBoxLookAtlistOfHolidaysHere
              }
            </p>
            <div className="d-flex service-charges">
              <div className="charge-input">
                <input
                  value={holidayCharges}
                  onChange={onTextChange}
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  type="number"
                  min={0}
                  maxLength={3}
                  max={100}
                  name="holidayCharges"
                  className={
                    "form-control valid-control " +
                    (errors.holiday_extra_charges ? "invalid" : " ")
                  }
                  id="charge"
                  placeholder={"0"}
                />
              </div>
              <div className="ml-3">
                <p className="font-12 mb-0">
                  {strings.RangeYouCan} <br />
                  {strings.increaseIs0to100}
                </p>
              </div>
            </div>
          </div>
          <hr />
          <div className="fees-content my-3">
            <p className="font-semibold text-muted font-14 mb-0">
              {strings.CacellationPolicy}
            </p>
            <p className="mb-0 font-12">
              {
                strings.SelectCancellationPolicyForThisServiceUserWillSeeTheSameAtTheTimeOfBooking
              }
            </p>
            <div className="booking-for">
              <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  <div className="row  service-charges pb-0">
                    <div className="custom-check">
                      <label className="check  mb-2">
                        <input
                          type="radio"
                          id="flexible"
                          checked={cancellationPolicy == 1}
                          onChange={changeCancellationPolicy}
                          name="is_name1"
                        />
                        <span className="checkmark" />
                        {strings.Flexible}
                      </label>
                      <p className="font-12">
                        {
                          strings.ForBoardingAndHouseSittingYouWillGetAFullRefundIfYouCancelBeforeTheStayBegins
                        }
                      </p>
                      <p className="font-12 mb-0">
                        {
                          strings.ForWalksDayCareAndDropinVisitsYouWillGetAFullRefundIfYouCancelBeforeTheDaysServiceIsDelivered
                        }
                      </p>
                    </div>
                  </div>
                  {/*--------/Flexible-----------*/}
                  {/*--------Flexible-----------*/}
                  <div className="row  service-charges pb-0">
                    <div className="custom-check">
                      <label className="check mb-2">
                        <input
                          type="radio"
                          name="is_name1"
                          id="moderate"
                          checked={cancellationPolicy == 2}
                          onChange={changeCancellationPolicy}
                        />
                        <span className="checkmark" />
                        {strings.Moderate}
                      </label>
                      <p className="font-12 mb-2">
                        {strings.YouCancelWithin48HoursOfBooking}
                      </p>
                      <p className="font-12  mb-2">
                        {
                          strings.YouHavenotAlreadyCancelled3ReservationsInTheLast12months
                        }
                      </p>
                      <p className="font-12  mb-2">
                        {
                          strings.TheReservationYouareCancellingDoesnotOverlapWithAnotherReservationInYourAccountWhenRefund
                        }
                      </p>
                      <p className="font-12  mb-0">
                        {
                          strings.YouwllGetAFullRefundIfYouCancelBy1200Noon3daysBeforeTheStayBegins
                        }
                      </p>
                    </div>
                  </div>
                  {/*--------/Flexible-----------*/}
                  {/*--------Flexible-----------*/}
                  <div className="row  service-charges pb-0">
                    <div className="custom-check">
                      <label className="check mb-2">
                        <input
                          type="radio"
                          name="is_name1"
                          id="strict"
                          checked={cancellationPolicy == 3}
                          onChange={changeCancellationPolicy}
                        />
                        <span className="checkmark" />
                        {strings.Strict}
                      </label>
                      <p className="font-12 mb-2">
                        {
                          strings.YouwllGetAFullRefundIfYouCancelBy1200NoonOneWeekBeforeTheStayBegins
                        }
                      </p>
                    </div>
                  </div>
                  {/*--------/Flexible-----------*/}
                </div>
              </div>
            </div>
          </div>
          <hr />
        </div>
      </div>
      <SaveServiceView
        preview={null}
        onPreview={() => {}}
        onSave={saveServiceAndFee}
      />

      <AdditonalServiceSidebar
        showModal={showAdditionalModal}
        setShowModal={() => setShowAdditionalModal(false)}
        editData={editAdditionalService}
        data={sideBarData}
        addData={addAdditionalService}
        removeData={removeAdditonalService}
        additionalEditService={additionalEditService}
      />
    </>
  );
};

export default GroomingServiceFee;
