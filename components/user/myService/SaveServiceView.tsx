import Link from "next/link";
import React from "react";
import {useRouter} from "next/router";
import {strings} from "../../../public/lang/Strings";

export const SaveServiceView = (props) => {
  const router = useRouter()
  const getLink = () => {
    switch (props.preview) {
      case 1:
        return "/user/my-services/boarding-preview";
      case 2:
        return "/user/my-services/house-sitting-preview";
      case 3:
        return "/user/my-services/drop-in-visits-preview";
      case 4:
        return "/user/my-services/pet-day-care-preview";
      case 5:
        return "/user/my-services/dog-walking-preview";
      case 6:
        return "/user/my-services/grooming-preview";
      case 7:
        return "/user/my-services/house-call-preview";
      default:
        return "/user/my-services/boarding-preview";
    }
  };
  return (
    <div className="text-right mt-3">
      <div className="d-none d-lg-block">
        {props?.error ? <label className='mx-3 text-danger'>*{props?.error}</label> : null}
        {props.preview != null &&
            <a onClick={()=> props.onPreview(getLink())} className="preview pre-mob cursor-pointer mr-3">
              {strings.Preview}
            </a>
        }
        <button onClick={()=>props.onSave(false)} className="btn btn-primary">
          {strings.SaveandContinue}
        </button>
      </div>
      <div className="d-block d-lg-none book-fixed">
        <div className="col-12 px-4">
          <div className="row align-items-center">
            {props.preview != null &&
            <div className="col px-2">
              <a onClick={()=> props.onPreview(getLink())} className="btn btn-outline-primary w-100 mb-0 mr-3">
                {strings.Preview}
              </a>
            </div>
            }
            <div className="col px-2">
              <button onClick={()=>props.onSave(false)} className="btn btn-primary px-3">
                {strings.SaveandContinue}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SaveServiceView;
