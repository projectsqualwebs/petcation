import React, { createRef, useState } from "react";
import Select from "react-select";
import { strings } from "../../../public/lang/Strings";
import { useEffect } from "react";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
import formatDate from "react-day-picker/moment";
import parseDate from "react-day-picker/moment";
import moment from "moment";
import Helmet from "react-helmet";
import Cookies from "universal-cookie";
import { getCurrencySign } from "../../../api/Constants";
import PaymentHistoryModel from "../../common/PaymentHistoryModel";
import {TimeFormat} from "../../../utils/Helper";

const cookies = new Cookies();

const EarningTab = ({
  handleFrom,
  handleTo,
  data,
  handleSortby,
  trasactionType,
  earning,
}) => {
  const [from, setFrom] = useState(undefined);
  const [to, setTo] = useState(undefined);
  const userId = cookies.get("id");
  let toInput = createRef<any>();
  const [showModal, setShowModal] = useState<any>();
  const [showCancelModal, setShowCancelModal] = useState<any>(false);
  const [viewModal, setViewModal] = useState<boolean>();
  const [singleData, setSingleData] = useState();

  const showFromMonth = () => {
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), "months") < 2) {
      toInput.current.getDayPicker().showMonth(from);
    }
  };

  const handleFromChange = (from) => {
    // Change the from date and focus the "to" input field
    handleFrom(from);
    setFrom(from);
  };

  const handleToChange = (to) => {
    handleTo(to);
    setTo(to);
  };

  useEffect(() => {
    showFromMonth();
  }, [to]);

  const noCardView = () => {
    return (
      <>
        <div className="text-center padding">
          <p className="font-12 mb-0 font-italic">
            {strings.NoTransactionHistoryFound}
          </p>
        </div>
      </>
    );
  };

  return (
    <div className="tab-content" id="myTabContent">
      <div
        className="tab-pane fade active show"
        id="earnings"
        role="tabpanel"
        aria-labelledby="earning-tab">
        <div className="row my-2 mb-4">
          <div className="col-12 col-md my-auto">
            <div className="transactions-title">
              <h6 className="mb-0">
                {(data ? data.total : 0) + " " + strings.Transactions}
              </h6>
            </div>
          </div>
          <div className="col-12 col-md-auto alignment pro-date">
            <div className="row align-items-center">
              <div className="col-md">
                <div className="row align-items-center">
                  <div className="col pr-0">
                    <div className="col-12 InputFromTo px-0">
                      <div className="row align-items-center">
                        <div className="col">
                          <DayPickerInput
                            value={from}
                            placeholder={strings.From}
                            format="LL"
                            formatDate={formatDate.formatDate}
                            parseDate={parseDate.parseDate}
                            dayPickerProps={{
                              selectedDays: [from, { from, to }],
                              disabledDays: { after: to },
                              toMonth: to,
                              modifiers: { start: from, end: to },
                              numberOfMonths: 1,
                              onDayClick: () =>
                                toInput.current.getInput().focus(),
                            }}
                            onDayChange={handleFromChange}
                          />
                        </div>
                        <div className="col-auto px-0">
                          <b className="">—</b>
                        </div>
                        <div className="col">
                          <DayPickerInput
                            ref={toInput}
                            value={to}
                            placeholder={strings.To}
                            format="LL"
                            formatDate={formatDate.formatDate}
                            parseDate={parseDate.parseDate}
                            dayPickerProps={{
                              selectedDays: { from, to },
                              disabledDays: { before: from },
                              modifiers: { start: from, end: to },
                              month: from,
                              fromMonth: from,
                              numberOfMonths: 1,
                            }}
                            onDayChange={handleToChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-auto">
                    <svg
                      onClick={() => {
                        // handleTo(null);
                        handleFrom(null);
                        setFrom(undefined);
                        setTo(undefined);
                      }}
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                      stroke="currentColor"
                      strokeWidth="2"
                      fill="none"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="css-i6dzq1">
                      <polyline points="23 4 23 10 17 10"></polyline>
                      <polyline points="1 20 1 14 7 14"></polyline>
                      <path d="M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15"></path>
                    </svg>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-auto font-12 text-left">
                <Select
                  onChange={handleSortby}
                  options={[
                    { label: strings.AmountDesc, value: 1 },
                    { label: strings.AmountAsc, value: 2 },
                  ]}
                />
              </div>
            </div>
          </div>
        </div>
        {data && data.data.length
          ? data.data.map((item, index) => (
              <div className="row pay-history">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  <div className="booking-request-details">
                    <div className="booking-bg">
                      <div className="row">
                        <div className="col-xl-12">
                          <div className="row">
                            <div className="col-12 col-md-8 col-lg-9 col-xl-5">
                              <div className="d-flex justify-content-between">
                                <div className="from-details">
                                  <p className="font-10 mb-0">{strings.From}</p>
                                  <p className="font-12 font-semibold mb-0">
                                    {item
                                      ? moment(new Date(item.drop_of_date)).format("MM/DD/YYYY") +
                                        ", " +
                                        TimeFormat(item.drop_of_time_from)
                                      : ""}
                                  </p>
                                </div>
                                <div className="to-details">
                                  <p className="font-10 mb-0">{strings.To}</p>
                                  <p className="font-12 font-semibold mb-0">
                                    {item
                                      ? moment(new Date(item.pickup_up_date)).format("MM/DD/YYYY") +
                                        ", " +
                                        TimeFormat(item.pickup_up_time_to)
                                      : ""}
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="col-12 mt-1 mb-md-0 col-md-8 col-lg-3 col-xl-3 offset-xl-4 alignment">
                              <div className="d-flex justify-content-md-end flex-md-column flex-row justify-content-between">
                                <p className="font-12 mb-0">
                                  {userId && userId == item.user.id
                                    ? earning == trasactionType
                                      ? strings.totalPayableAmount
                                      : strings.TotalRefundableAmount
                                    : earning == trasactionType
                                    ? strings.RefundedAmount
                                    : strings.YourExpectedEarnings}
                                </p>
                                <p
                                  className="font-12 font-semibold mb-0"
                                  style={
                                    earning == trasactionType
                                      ? { color: "red" }
                                      : { color: "green" }
                                  }>
                                  {getCurrencySign() +
                                    (userId && userId == item.user.id
                                      ? item.total_paid_amount
                                      : item.sitter_earning_amount)}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row listing">
                      <div className="col-xl-12 py-1">
                        <div className="row align-items-center">
                          <div className="col-xl-8">
                            <div
                              className="booking-title ">
                              <h6 className="font-14 mb-1 font-medium">
                                {item.user.firstname + " " + item.user.lastname}{" "}
                                - {item.service.name}
                              </h6>
                              <p className="mb-0 font-10">
                                {strings.Address}:{" "}
                                {item.user.address
                                  ? item.user.address.house_number +
                                    " " +
                                    item.user.address.address +
                                    " " +
                                    item.user.address.city +
                                    " " +
                                    item.user.address.postcode
                                  : ""}
                              </p>
                                <small onClick={() => {
                                    setSingleData(item);
                                    setShowModal(true);
                                }} className="text-primary pointer">{strings.viewDetails}</small>
                            </div>
                          </div>
                          <div className="col-xl-4 alignment mt-2 mt-md-0">
                            <div className="d-flex justify-content-between">
                              <div className="total-details text-left text-md-right">
                                <p className="font-12 mb-0">
                                  {strings.PetcationFee}
                                </p>
                                <p className="font-12 mb-0">
                                  {getCurrencySign() +
                                    item.petcation_fee_amount}
                                </p>
                              </div>
                              <div className="total-details">
                                <p className="font-12 mb-0">
                                  {strings.ListingPrice}
                                </p>
                                <p className="font-12 mb-0">
                                  {getCurrencySign() + item.service_charge}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))
          : noCardView()}
      </div>
      <PaymentHistoryModel
        showModal={showModal}
        data={singleData}
        hideModal={() => setShowModal(false)}
      />
    </div>
  );
};
export default EarningTab;
