import { useRouter } from "next/router";
import React from "react";
import {strings} from "../../../public/lang/Strings";

export default function SuccessView(props) {
  const router = useRouter();
  return (
    <div className="bg-white main-background successful mb-0">
      <div className="row justify-content-center">
        <div className="col-12 col-md-10 col-lg-10 col-xl-10">
          <div className="successfully-details text-center">
            <div className="success-checkmark text-center mb-4">
              <img
                src="/images/confirm.png"
                className="img-fluid"
                alt="checkmark"
              />
            </div>
            <h5 className="font-20 font-semibold">{strings.Spot + " " + (props.updated ? strings.updated : strings.listed) + " " + strings.successfully}</h5>
            <p className="font-14 font-medium">
              {strings.WeHaveReceivedYourSpotListingRequestOurModeratorWillGetBackToYouOnceReviewedYourSpotProfile}
            </p>
            <div className="spot-btn1">
              <button
                onClick={() => {
                  props.setStateNull()
                  props.changePage(1);
                }}
                className="btn btn-primary"
              >
                {strings.AddAnotherSpot}
              </button>
            </div>
            <div className="back">
              <a href="/user/my-spots" className="font-14 font-semibold">
                {strings.GoToSpotList}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
