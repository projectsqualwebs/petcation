import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import {strings} from "../../../public/lang/Strings";

interface IState {
  means_of_transport: string;
  business_hours: string;
  regular_holidays: string;
  is_open_on_sunday: number;
}
export default function SpotTransportation(props: any) {
  const [errors, setErrors] = useState<any>({});
  const [transportation, setTransportation] = useState<IState>({
    means_of_transport: "",
    business_hours: "",
    regular_holidays: "",
    is_open_on_sunday: 0,
  });

  useEffect(() => {
    if (props.data) {
      setTransportation({ ...props.data });
    }
  }, [props]);

  const onChange = (e: React.ChangeEvent<HTMLInputElement> | any) => {
    setTransportation({ ...transportation, [e.target.name]: e.target.value });
    if (Object.keys(errors).includes(e.target.name)) {
      let newValue = { ...errors };
      delete newValue[e.target.name];
      setErrors(newValue);
    }
  };

  const onSubmit = () => {
    // if (transportation.means_of_transport == "") {
    //   setErrors({ ...errors, means_of_transport: true });
    //   return false;
    // }
    //
    // if (transportation.business_hours == "") {
    //   setErrors({ ...errors, business_hours: true });
    //   return false;
    // }
    //
    // if (transportation.regular_holidays == "") {
    //   setErrors({ ...errors, regular_holidays: true });
    //   return false;
    // }
    props.updateOperations(transportation);
    props.changePage(4);
  };
  return (
    <div className="select-spot-category content-padding">
      <h5 className="font-semibold mb-3">
        {strings.TransportationOrOperationalDetails}
      </h5>
      <div className="booking-for">
        <div className="row">
          <div className="col-12 col-md-12 col-lg-12 col-xl-12">
            <div className="info-form">
              <h6 className="font-medium mb-3">{strings.MeansOfTransport}</h6>
              <div className="form-group">
                <textarea
                  className={
                    "form-control " +
                    (errors.means_of_transport ? "is-invalid" : "")
                  }
                  rows={6}
                  id="trasnport"
                  typeof="text"
                  value={transportation.means_of_transport}
                  onChange={onChange}
                  name="means_of_transport"
                  placeholder={strings._5minsWalkFromTheWestExitofJREbisuStation}
                  defaultValue={""}
                />
              </div>
              <hr />
              <h6 className="font-medium mb-3">{strings.BusinessHours}</h6>
              <div className="row">
                <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                  <div className="form-group">
                    <textarea
                      className={
                        "form-control " +
                        (errors.business_hours ? "is-invalid" : "")
                      }
                      rows={6}
                      value={transportation.business_hours}
                      name="business_hours"
                      onChange={onChange}
                      id="business-hours"
                      typeof="text"
                    />
                  </div>
                </div>
                <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                  <div className="hours-bg hours-details">
                    <p className="mb-1 font-medium">{strings.Example}</p>
                    <p className="mb-1 font-medium">{strings.B_MondaytoGold_B}</p>
                    <p className="mb-1 font-medium">
                      {strings._1100_1500LO1800TheNext030LO2400}
                    </p>
                    <p className="mb-1 font-medium">{strings.SatSunChu}</p>
                    <p className="mb-0 font-medium">11:00~21:00</p>
                  </div>
                </div>
              </div>
              <hr />
              <h6 className="font-medium mb-3">{strings.RegularHoliday}</h6>
              <div className="form-group">
                <input
                  className={
                    "form-control " +
                    (errors.regular_holidays ? "is-invalid" : "")
                  }
                  id="trasnport"
                  type="text"
                  name="regular_holidays"
                  onChange={onChange}
                  value={transportation.regular_holidays}
                  placeholder={strings.ExampleSundayMonday}
                />
              </div>
              <div className="hours-details mb-3">
                <p className="mb-2 font-medium">
                  {strings.IfYouaAreOpenOnSundayPleaseCheckTheFollowing}
                </p>
                <div className="custom-check">
                  <label className="check ">
                    <input
                      checked={transportation.is_open_on_sunday == 1}
                      onChange={() => {
                        setTransportation({
                          ...transportation,
                          is_open_on_sunday:
                            transportation.is_open_on_sunday == 1 ? 0 : 1,
                        });
                      }}
                      type="checkbox"
                      name="is_name2"
                    />
                    <span className="checkmark" />
                    {strings.OpenOnSunday}
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="modal-footer px-0 pb-0 justify-content-between">
        <button onClick={()=> props.changePage(2)} className="btn btn-primary btn1 btn-edit">
          {strings.Back}
        </button>
        <button onClick={() => onSubmit()} className="btn btn-primary">
          {strings.SaveAndProceed}
        </button>
      </div>
    </div>
  );
}
