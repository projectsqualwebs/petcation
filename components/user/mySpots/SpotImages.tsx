import React from "react";
import { strings } from "../../../public/lang/Strings";
import { useEffect } from "react";
import { useRef } from "react";
import { useState } from "react";
import API from "../../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import {errorOptions, pets} from "../../../public/appData/AppData";
import ReactMultiImageUploadModal from '../../common/ReactMultiImageUploadModal';

const api = new API();
export default function SpotImages(props: any) {
  const [openError, closeError] = useSnackbar(errorOptions);
  const [images, setImages] = useState<string[]>([]);
  const [showModal, setShowModal] = useState<boolean>(false)

  useEffect(() => {
    if (props.data) {
      setImages(props.data);
    }
  }, [props.data]);

  const onSubmit = () => {
    if (images.length == 0) {
      openError(strings.SelectAtleastOneImage);
      return false;
    }

    props.updateImages(images);
  };

  const deleteImage = (index) => {
    let image = images.filter((val,i)=> i !=index)
    setImages(image);
  }

  const onUpload = (data:any) => {
   // let fileArr:any[] = [];
    const formData = new FormData();
    data.map((value)=>{
      formData.append('images[]', value)
    })
    formData.append("path", "pets");

      api
        .uploadMultiFile(formData)
        .then((json) => {
          setImages([...images , ...json.data.response]);
          setShowModal(false)
        })
        .catch((error) => console.log(error));
  };

  return (
      <div className="select-spot-category content-padding">
        <h5 className="font-semibold mb-3">{strings.Spotimages}<span className="sign">*</span></h5>
        <div className="booking-for">
          <div className="spot-images">
            <div className="row align-items-center">
              {images.map((value, index) => (
                <div
                      key={index}
                      // onClick={() => {
                      //   let img = [...images];
                      //   img.splice(index, 1);
                      //   setImages([...img]);
                      // }}
                      className="col-6 col-md-4 col-lg-4 col-xl-4 position-relative mb-2"
                  >
                    <a onClick={()=>deleteImage(index)} className={'bg-danger position-absolute top-0 p-1 px-2 rounded-lg m-1 d-flex'}>
                      <svg xmlns="http://www.w3.org/2000/svg" className="mb-0" width="16" height="16" viewBox="0 0 24 24" fill="none"><path d="m9.17 14.83 5.66-5.66M14.83 14.83 9.17 9.17M9 22h6c5 0 7-2 7-7V9c0-5-2-7-7-7H9C4 2 2 4 2 9v6c0 5 2 7 7 7Z" stroke="#FFFFFF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                    </a>
                    <div className="spot-images">
                      <img src={value} key={index} className="img-fluid w-100" />
                    </div>
                  </div>
              ))}
            </div>
            <button
              onClick={() => setShowModal(true)}
              className="btn btn-primary btn-black mb-3 py-1 mt-3 mt-md-0 h-auto py-2"
            >
              {strings.Addmore}
            </button>
          </div>
        </div>
        <div className="modal-footer px-0 pb-0 justify-content-between">
          <button onClick={()=> props.changePage(5)} className="btn btn-primary btn1 btn-edit">
            {strings.Back}
          </button>
          <button onClick={() => onSubmit()} className="btn btn-primary">
            {strings.Confirm}
          </button>
        </div>
        <ReactMultiImageUploadModal
          showModal={showModal}
          hideModal={()=>setShowModal(false)}
          onUpload={onUpload}
          dataSources={[]}
        />
      </div>
  );
}
