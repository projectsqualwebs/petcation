import React from "react";
import { useState } from "react";
import { strings } from "../../../public/lang/Strings";
import { numberInput } from "../../../utils/Helper";
import { useEffect } from "react";
import MyMapComponent from "../myProfile/Map";
import {GOOGLE_PLACES_API} from "../../../api/Constants";
import LocationSearchInput2 from "../../common/LocationSearchInput2";
import {Add} from "iconsax-react";

interface I_State {
  spot_name: string;
  english_name: string;
  spot_description: string;
  phone_number: string;
  address: string;
  postal_code: string;
  locality: string;
  city: string;
  defaultCenter?: any;
}

export default function SpotBasicInfo(props) {
  const [errors, setErrors] = useState<any>({});
  const [address, setAddress] = useState<any>({});
  const [latlng, setLatlng] = useState<any>({
      lat: '',
      lng: ''
  })
  const [value, setValue] = useState<any>('');
  const [basicDetails, setBasicDetails] = useState<I_State>({
    spot_name: "",
    english_name: "",
    spot_description: "",
    phone_number: "",
    address: "",
    postal_code: "",
    locality: '',
    city: "",
  });
  useEffect(() => {
    if (props.data) {
      setBasicDetails({
          spot_name: props?.data?.spot_name,
          english_name: props?.data?.english_name,
          spot_description: props?.data?.spot_description,
          phone_number: props?.data?.phone_number,
          address: props?.data?.address,
          postal_code: props?.data?.postal_code,
          locality: props?.data?.locality,
          city: props?.data?.city,
      });
      setLatlng({
          lat: parseFloat(props?.data?.latitude),
          lng: parseFloat(props?.data?.longitude)
      })
      console.log(props.data)
    }
  }, [props]);

  // useEffect(() => {
  //   if (address.address) {
  //     setBasicDetails({ ...basicDetails, ...address });
  //   }
  // }, [address]);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    setBasicDetails({ ...basicDetails, [e.target.name]: e.target.value });
    if (Object.keys(errors).includes(e.target.name)) {
      let newValue = { ...errors };
      delete newValue[e.target.name];
      setErrors(newValue);
    }
  };

  const onSubmit = () => {
    setErrors({});
    if (basicDetails.spot_name == "") {
      setErrors({ ...errors, spot_name: true });
      return false;
    }

    if (basicDetails.english_name == "") {
      setErrors({ ...errors, english_name: true });
      return false;
    }

    if (
      basicDetails.phone_number == "" ||
      basicDetails.phone_number.length < 8
    ) {
      setErrors({ ...errors, phone_number: true });
      return false;
    }

    if (basicDetails.address == "") {
      setErrors({ ...errors, address: true });
      return false;
    }

    if (basicDetails.city == "") {
      setErrors({ ...errors, city: true });
      return false;
    }

    const data = {
        spot_name: basicDetails.spot_name,
        english_name: basicDetails.english_name,
        spot_description: basicDetails.spot_description,
        phone_number: basicDetails.phone_number,
        address: basicDetails.address,
        postal_code: basicDetails.postal_code,
        locality: basicDetails.locality,
        city: basicDetails.city,
        latitude: String(latlng.lat),
        longitude: String(latlng.lng),
    }

    props.updateBasicInfo({ ...data });
    props.changePage(3);
  };

  const resetAddress = () => {
    setBasicDetails({
      ...basicDetails,
      address: '',
      locality:'',
      postal_code: '',
      city: '',
    })
  }

  return (
    <div className="select-spot-category content-padding">
      <h5 className="font-semibold">
        {strings.basicinfo}<span className="sign">*</span>
      </h5>
      <div className="booking-for">
        <h6 className="font-medium mb-1">{strings.AddABusiness}</h6>
        <p className="font-12">
          *{strings.PleaseNotThisBusinessPageWillNotAppearInSearchUntilItHasBeenApprovedByOurModerators}
        </p>
        <div className="row">
          <div className="col-12 col-md-6 col-lg-6 col-xl-6">
            <div className="info-form">
                <div className="form-group">
                  <label>{strings.SpotName}<span className="sign">*</span></label>
                  <input
                    className={
                      "form-control " + (errors.spot_name ? "invalid" : "")
                    }
                    name="spot_name"
                    placeholder={strings.EnterSpotName}
                    type="text"
                    value={basicDetails.spot_name}
                    onChange={onChange}
                  />
                </div>
                <div className="form-group">
                  <label>{strings.EnglishName}<span className="sign">*</span></label>
                  <input
                    className={
                      "form-control " + (errors.english_name ? "invalid" : "")
                    }
                    name="english_name"
                    placeholder={strings.name}
                    type="text"
                    value={basicDetails.english_name}
                    onChange={onChange}
                  />
                </div>
                <div className="form-group">
                  <label>{strings.SpotDescription}</label>
                  <textarea
                      className={
                        "form-control " +
                        (errors.spot_description ? "is-invalid" : "")
                      }
                      rows={4}
                      value={basicDetails.spot_description}
                      name="spot_description"
                      onChange={(val:any)=>onChange(val)}
                      id="spot_description"
                      typeof="text"
                  />
                </div>
                <div className="form-group">
                  <label>{strings.PhoneNumber}<span className="sign">*</span></label>
                  <input
                    className={
                      "form-control " + (errors.phone_number ? "invalid" : "")
                    }
                    id="phone-no"
                    name="phone_number"
                    value={basicDetails.phone_number}
                    onChange={onChange}
                    type="text"
                    placeholder={strings.PhoneNumber}
                    maxLength={10}
                    onKeyPress={numberInput}
                  />
                </div>
                <div className="form-group location-search-input">
                  <label>{strings.Address}<span className="sign">*</span></label>
                  <LocationSearchInput2
                      key={'address_input3'}
                    className={
                      "form-control " + (errors.address ? "invalid" : "")
                    }
                      setFilter={(Address)=>
                          setBasicDetails({
                              ...basicDetails,
                              address: Address.value,
                              locality:Address.locality,
                              postal_code: Address.postalCode,
                              city: Address.city,
                          })
                      }
                      value={value || basicDetails.address}
                    setLatLng={(latLng)=>{
                      setLatlng({
                        lat: latLng.lat,
                        lng: latLng.lng,
                      })}
                    }
                    setValue={(address)=> {
                      setValue(address)
                      if(address == ''){
                        resetAddress()
                        setValue('')
                      }
                      if (Object.keys(errors).includes("address")) {
                        let newValue = { ...errors };
                        delete newValue['address'];
                        setErrors(newValue);
                      }
                    }}
                  />
                </div>
                <div className="form-group">
                  <label>{strings.postalCode}</label>
                  <input
                    className={
                      "form-control"
                    }
                    id="postal-code"
                    type="text"
                    placeholder="code"
                    value={basicDetails.postal_code}
                    onChange={onChange}
                    name="postal_code"
                  />
                </div>
                <div className="form-group">
                  <label>{strings.Locality}</label>
                  <input
                    className={
                      "form-control "
                    }
                    id="locality"
                    type="text"
                    placeholder={strings.Locality}
                    value={basicDetails.locality}
                    onChange={onChange}
                    name="locality"
                  />
                </div>

                <div className="form-group">
                    <label>{strings.City}<span className="sign">*</span></label>
                    <input
                      className={
                      "form-control " + (errors.city ? "invalid" : "")
                    }
                      id="city"
                      placeholder={strings.City}
                      type="text"
                      name="city"
                      value={basicDetails.city}
                      onChange={onChange}
                    />
                </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-lg-6 col-xl-6">
             <MyMapComponent
                 key={'map'}
              draggable={false}
              onChangeLatLng={(e) => {}}
              latlng={latlng.lat ? {
                lat: latlng.lat,
                lng: latlng.lng,
              } : {lat: 36.2048, lng: 138.2529}}
              defaultCenter={latlng.lat ? {
                  lat: latlng.lat,
                  lng: latlng.lng,
              } : {lat: 36.2048, lng: 138.2529}}
              mapZoom={10}
            />
          </div>
        </div>
      </div>
      <div className="modal-footer px-0 pb-0 justify-content-between">
        <button onClick={()=> props.changePage(1)} className="btn btn-primary btn1 btn-edit">
          {strings.Back}
        </button>
        <button onClick={() => onSubmit()} className="btn btn-primary">
          {strings.SaveAndProceed}
        </button>
      </div>
    </div>
  );
}
