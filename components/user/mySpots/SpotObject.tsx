import React, {useState} from "react";
import Carousel from "react-multi-carousel";
import {
  responsive,
  singleResponsiveness,
} from "../../../public/SliderResponsive";
import "react-multi-carousel/lib/styles.css";
import { strings } from "../../../public/lang/Strings";
import { useSnackbar } from 'react-simple-snackbar';
import API from "../../../api/Api";
import Link from "next/link";

import {useRouter} from "next/router";
import {U_BASE_URL, U_FACEBOOK_ID} from "../../../api/Constants";
import {errorOptions, successOptions} from "../../../public/appData/AppData";
import {confirmAlert} from "react-confirm-alert";
import 'react-confirm-alert/src/react-confirm-alert.css';
import {Dropdown, DropdownButton} from "react-bootstrap"; // Import css

type T_SPOT = {
  spot_id: number;
  id: number;
  spot_name: string;
  address: string;
  city: string;
  postal_code: number;
  created_at: string;
  overall_rate: number;
  total_review: number;
  repeat_client: number;
  images: {
    path: string;
  }[];
  spot: {
    spot_id: number;
    id: number;
    spot_name: string;
    address: string;
    city: string;
    postal_code: number;
    created_at: string;
    overall_rate: number;
    total_review: number;
    repeat_client: number;
    images: {
      path: string;
    }[];
    user_id: number;
    note: string;
  }
};

interface IProps {
  spot: any;
  deleteSpot?: (val: number) => void;
  updateSpot: (listId?: number) => void;
  type: number;
  listId: number;
}

const api = new API();

const SpotObject: React.FC<IProps> = ({ spot, deleteSpot, updateSpot, listId, type }: IProps) => {
  const [openSnackbar] = useSnackbar();
  const router = useRouter();
  const [copied, setCopied] = useState(false);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [note, setNote] = useState('');

  const unmarkSpotApiCall = (spot_id) => {
    let data = {
      list_id: listId,
      spot_id: spot_id
    }
    api
        .markSpotAsFavourite(data)
        .then(res => {
          updateSpot(listId)
          openSuccess(res.data.message)
        })
        .catch(err => console.log(err.response.data.message))
  };

  const unmarkSpot = (spot_id) => {
    confirmAlert({
      closeOnClickOutside: false,
      customUI: ({onClose}) => {
        return (
            <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
              <div className="react-confirm-alert-body">
                <h4>{strings.RemoveSpotFromWishlist}</h4>
                <p>{strings.areYouSure}</p>
                <div className="react-confirm-alert-button-group">
                  <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                    unmarkSpotApiCall(spot_id)
                    onClose();
                  }}>{strings.Delete}
                  </button>
                  <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                    onClose();
                  }}>{strings.Cancel}
                  </button>
                </div>
              </div>
            </div>
        );
      }
    });
  }

  /**
   * @params e contains event which help in prevent default
   * @function will copy url of sitter profile.
   */
  const copyUrl = (e, id) => {
    e.preventDefault();
    const el = document.createElement("input");
    el.value = `${U_BASE_URL}pet-spots/${id}`;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopied(true);
    openSuccess(strings.CopiedToClipboard)
  }

  /**
   *
   */
  const addNote = (id) => {
    api
        .updateSpotNote({spot_id: id, note: note})
        .then(res => {
          updateSpot()
          openSuccess(res.data.message)
        })
        .catch(err => openError(err.response.data.message))
  }
  const RatingStar = ({rating}) => {
    return (
      <div className="rating-star d-flex">
        <div className={rating > 0 ? 'active' : ''}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="star"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 576 512"
            className="svg-inline--fa fa-star fa-w-18 fa-2x"
          >
            <path
              fill="currentColor"
              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
            />
          </svg>
        </div>
        <div className={rating > 1 ? 'active' : ''}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="star"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 576 512"
            className="svg-inline--fa fa-star fa-w-18 fa-2x"
          >
            <path
              fill="currentColor"
              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
            />
          </svg>
        </div>
        <div className={rating > 2 ? 'active' : ''}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="star"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 576 512"
            className="svg-inline--fa fa-star fa-w-18 fa-2x"
          >
            <path
              fill="currentColor"
              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
            />
          </svg>
        </div>
        <div className={rating > 3 ? 'active' : ''}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="star"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 576 512"
            className="svg-inline--fa fa-star fa-w-18 fa-2x"
          >
            <path
              fill="currentColor"
              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
            />
          </svg>
        </div>
        <div className={rating > 4 ? 'active' : ''}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="star"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 576 512"
            className="svg-inline--fa fa-star fa-w-18 fa-2x"
          >
            <path
              fill="currentColor"
              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
            />
          </svg>
        </div>
      </div>
    )
  }

  return (
      <>
        {spot.spot ? <>
          <div className="row">
            <div className="col-12 col-md-10 spot-banner-slide">
              <div className="row">
                <div className="col-sm-5">

                  {spot.spot.images && spot.spot.images.length > 0 ? (
                      <div className="carousel slide">
                        <Carousel
                            arrows={null}
                            showDots={true}
                            swipeable={true}
                            draggable={true}
                            autoPlay={true}
                            responsive={singleResponsiveness}
                        >
                          {spot.spot.images.map((value, index) => (
                              <img
                                  key={index}
                                  className="d-block rounded-15"
                                  src={value.path}
                                  alt="First slide"
                              />
                          ))}
                        </Carousel>
                      </div>
                  ) : (
                      <img
                          key={"1"}
                          className="d-block w-100 rounded-15"
                          src={'/images/img2.jpg'}
                          alt="First slide"
                      />
                  )}
                </div>
                <div className="col-sm-7 p-left my-auto">
                  <div className="row">
                    <div className="col">
                      <div className="myspot-details main-padding cursor-pointer">
                        <Link href={`/pet-spots/${spot.spot.id}`}>
                          <a>
                        <h6 className="mb-0 font-semibold">{spot?.spot?.spot_name} </h6>
                        <p className="font-12 mb-0">{spot?.spot?.address}</p>
                            <div className="price-details py-1">
                              <div className="row">
                                {spot?.spot?.budgets?.night_charge ? <div className="col-md-6">
                                  <h6 className="mb-0 d-flex align-items-center"><img className="mr-2" width="16px" src="/images/half-moon.png"/>{spot?.spot?.budgets?.night_charge?.range}</h6>
                                </div> : null}
                                {spot?.spot?.budgets?.noon_charge ? <div className="col-md-6">
                                  <h6 className="mb-0 d-flex align-items-center"><img className="mr-2" width="16px" src="/images/sun-clock.png"/>{spot?.spot?.budgets?.noon_charge?.range}</h6>
                                </div> : null}
                              </div>
                            </div>
                        </a>
                        </Link>
                      </div>
                      <div className="myspot-details">
                        <div>
                          <div className="col-6 col-md-12 col-lg-12 pr-0">
                            
                          </div>
                          <h6 className="font-12 font-semibold">
                            <span className="font-normal">{strings.Categories} </span>
                            {spot?.spot?.category?.name}
                          </h6>
                          
                        
                          <div className="d-flex hotel-rating spot-rate">
                              <RatingStar rating={spot?.spot?.overall_rate ?? 0}/>
                            <div>
                              <h6 className="mb-0 font-14 d-inline">
                                {spot?.spot?.overall_rate ?? 0}{" "}
                                <span className="text-muted">{`(${spot?.spot?.total_review ?? 0} ${strings.Reviews})`}</span>
                              </h6>{" "}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-auto d-block d-md-none mt-3">
                      <div className="dropdown pet-drop mob-drop d-flex justify-content-end flex-column">
                        <DropdownButton
                            align="end"
                            className="col-auto d-flex justify-content-end mb-1 p-0"
                            title={<a className="menu dropdown-toggle w-auto">
                              <div className="ellipse">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fal"
                                    data-icon="ellipsis-h"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 320 512"
                                    className="svg-inline--fa fa-ellipsis-h fa-w-10 fa-2x"
                                >
                                  <path
                                      fill="currentColor"
                                      d="M192 256c0 17.7-14.3 32-32 32s-32-14.3-32-32 14.3-32 32-32 32 14.3 32 32zm88-32c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zm-240 0c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"
                                  />
                                </svg>
                              </div>
                            </a>}
                            id="dropdown-menu-align-end"
                        >
                          {type == 1 && <Dropdown.Item eventKey="0" onClick={() => router.push('/user/my-spots/update-pet-spot?id=' + spot.id)}>
                            <a>
                              {strings.Edit}
                            </a>
                          </Dropdown.Item>}
                          <Dropdown.Item eventKey="1" onClick={() => {type == 1 ? deleteSpot(spot.id):unmarkSpot(`${spot.spot_id}`)}}>
                            <a>
                              {strings.Remove}
                            </a>
                          </Dropdown.Item>
                        </DropdownButton>
                        <DropdownButton
                            className='col-auto pet-drop mob-drop profile-share-drop p-0'
                            align="end"
                            title={<a className="menu">
                              <div className="ellipse">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="share"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512"
                                    className="svg-inline--fa fa-share fa-w-16 fa-2x"
                                >
                                  <path
                                      fill="currentColor"
                                      d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                                      className=""
                                  ></path>
                                </svg>
                              </div>
                            </a>}
                            id="dropdown-menu-align-end"
                        >
                          <p className="mb-0 font-semibold font-12">{strings.ShareThisProfile}</p>
                          <Dropdown.Item eventKey="0" href={`mailto:?subject=${spot.spot_name + ' profile'}&body=${encodeURIComponent(`${U_BASE_URL}pet-spots/${spot.id}`) || ''}`}>
                            <a className="">
                              <img src="/images/social-img4.png" />{strings.viaEmail}
                            </a>
                          </Dropdown.Item>
                          <Dropdown.Item eventKey="1" href={`https://api.whatsapp.com/send?text=${spot.spot_name + ' profile:\n' + (`${U_BASE_URL}pet-spots/${spot.id}`)}`}  data-action="share/whatsapp/share">
                            <a className="">
                              <img src="/images/social-img3.png" />{strings.viaWhatsapp}
                            </a>
                          </Dropdown.Item>
                          {/*<Dropdown.Item eventKey="2" href={`fb-messenger://share/?link=${`${U_BASE_URL}pet-spots/${spot.id}`}&app_id=${U_FACEBOOK_ID}`}>*/}
                          {/*  <a className="">*/}
                          {/*    {" "}*/}
                          {/*    <img src="/images/social-img1.png" />{strings.viaMessenger}*/}
                          {/*  </a>*/}
                          {/*</Dropdown.Item>*/}
                          <Dropdown.Item eventKey="3" onClick={(e) => copyUrl(e, spot.id)}>
                            <a className="">
                              {" "}
                              <img src="/images/copy.png" />
                              {strings.CopyLink}
                            </a>
                          </Dropdown.Item>
                        </DropdownButton>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
            </div>
            <div className="col-12 col-md-2 pr-0 d-none d-md-block">
              <div className="dropdown pet-drop mob-drop d-flex justify-content-end flex-column">
                <DropdownButton
                    align="end"
                    className="col-auto d-flex justify-content-end mb-3"
                    title={<a className="menu dropdown-toggle w-auto">
                      <div className="ellipse">
                        <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fal"
                            data-icon="ellipsis-h"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            className="svg-inline--fa fa-ellipsis-h fa-w-10 fa-2x"
                        >
                          <path
                              fill="currentColor"
                              d="M192 256c0 17.7-14.3 32-32 32s-32-14.3-32-32 14.3-32 32-32 32 14.3 32 32zm88-32c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zm-240 0c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"
                          />
                        </svg>
                      </div>
                    </a>}
                    id="dropdown-menu-align-end"
                >
                  {type == 1 && <Dropdown.Item eventKey="0" onClick={() => router.push('/user/my-spots/update-pet-spot?id=' + spot.id)}>
                    <a>
                      {strings.Edit}
                    </a>
                  </Dropdown.Item>}
                  <Dropdown.Item eventKey="1" onClick={() => {type == 1 ? deleteSpot(spot.id):unmarkSpot(`${spot.spot_id}`)}}>
                    <a>
                      {strings.Remove}
                    </a>
                  </Dropdown.Item>
                </DropdownButton>
                <DropdownButton
                    className='col-auto pet-drop mob-drop profile-share-drop'
                    align="end"
                    title={<a className="menu">
                      <div className="ellipse">
                        <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fas"
                            data-icon="share"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512"
                            className="svg-inline--fa fa-share fa-w-16 fa-2x"
                        >
                          <path
                              fill="currentColor"
                              d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                              className=""
                          ></path>
                        </svg>
                      </div>
                    </a>}
                    id="dropdown-menu-align-end"
                >
                  <p className="mb-0 font-semibold font-12">{strings.ShareThisProfile}</p>
                  <Dropdown.Item eventKey="0" href={`mailto:?subject=${spot.spot_name + ' profile'}&body=${encodeURIComponent(`${U_BASE_URL}pet-spots/${spot.id}`) || ''}`}>
                    <a className="">
                      <img src="/images/social-img4.png" />{strings.viaEmail}
                    </a>
                  </Dropdown.Item>
                  <Dropdown.Item eventKey="1" href={`https://api.whatsapp.com/send?text=${spot.spot_name + ' profile:\n' + (`${U_BASE_URL}pet-spots/${spot.id}`)}`}  data-action="share/whatsapp/share">
                    <a className="">
                      <img src="/images/social-img3.png" />{strings.viaWhatsapp}
                    </a>
                  </Dropdown.Item>
                  {/*<Dropdown.Item eventKey="2" href={`fb-messenger://share/?link=${`${U_BASE_URL}pet-spots/${spot.id}`}&app_id=${U_FACEBOOK_ID}`}>*/}
                  {/*  <a className="">*/}
                  {/*    {" "}*/}
                  {/*    <img src="/images/social-img1.png" />{strings.viaMessenger}*/}
                  {/*  </a>*/}
                  {/*</Dropdown.Item>*/}
                  <Dropdown.Item eventKey="3" onClick={(e) => copyUrl(e, spot.id)}>
                    <a className="">
                      {" "}
                      <img src="/images/copy.png" />
                      {strings.CopyLink}
                    </a>
                  </Dropdown.Item>
                </DropdownButton>
              </div>
            </div>
          </div>
          <hr/>
          
        </>:<>
          <div className="row">
            <div className="col-12 col-md-10 spot-banner-slide">
               
              <div className="row ">
                <div className="col-sm-5">

                  {spot.images && spot.images.length > 0 ? (
                      <div className="carousel slide">
                        <Carousel
                            arrows={null}
                            showDots={true}
                            swipeable={true}
                            draggable={true}
                            autoPlay={true}
                            responsive={singleResponsiveness}
                        >
                          {spot.images.map((value, index) => (
                              <img
                                  key={index}
                                  className="d-block rounded-15"
                                  src={value.path}
                                  alt="First slide"
                              />
                          ))}
                        </Carousel>
                      </div>
                  ) : (
                      <img
                          key={"1"}
                          className="d-block w-100 rounded-15"
                          src={'/images/img2.jpg'}
                          alt="First slide"
                      />
                  )}
                </div>
                <div className="col-sm-7 p-left my-auto ">
                  <div className="row">
                    <div className="col">
                      <div className="myspot-details main-padding cursor-pointer">
                        <Link href={`/pet-spots/${spot.id}`}>
                          <a>
                        <h6 className="mb-0 font-semibold">{spot?.spot_name} </h6>
                        <p className="font-12 mb-0">{spot?.address}</p>
                            <div className="price-details py-1">
                              <div className="row">
                                {spot?.budgets?.night_charge ? <div className="col-md-6">
                                  <h6 className="mb-0 d-flex align-items-center"><img className="mr-2" width="16px" src="/images/half-moon.png"/>{spot?.budgets?.night_charge?.range}</h6>
                                </div> : null}
                                {spot?.budgets?.noon_charge ? <div className="col-md-6">
                                  <h6 className="mb-0 d-flex align-items-center"><img className="mr-2" width="16px" src="/images/sun-clock.png"/>{spot?.budgets?.noon_charge?.range}</h6>
                                </div> : null}
                              </div>
                            </div>
                        </a>
                        </Link>
                      </div>
                      <div className="myspot-details">
                        <div>
                          <div className="col-6 col-md-12 col-lg-12 pr-0">
                            
                          </div>
                          <h6 className="font-12 font-semibold">
                            <span className="font-normal">{strings.Categories} </span>
                            {spot.category?.name}
                          </h6>
                          <h6 className="font-12 font-semibold">
                            <span className="font-normal">{strings.Availablefor} </span>
                             {spot?.pets?.length ? spot?.pets?.map((v, i) => {
                          if (i == spot.pets.length - 1) {
                            return v.name;
                          } else {
                            return v.name + ", ";
                          }
                        }) : strings.Unconfirmed}
                          </h6>
                          <div className="d-flex hotel-rating spot-rate">
                              <RatingStar rating={spot.overall_rate ?? 0}/>
                            <div>
                              <h6 className="mb-0 font-14 d-inline">
                                {spot.overall_rate ?? 0}{" "}
                                <span className="text-muted">{`(${spot.total_review ?? 0} ${strings.Reviews})`}</span>
                              </h6>{" "}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-auto d-block d-md-none mt-3">
                      <div className="dropdown pet-drop mob-drop d-flex justify-content-end flex-column">
                        <DropdownButton
                            align="end"
                            className="col-auto d-flex justify-content-end mb-1 p-0"
                            title={<a className="menu dropdown-toggle w-auto">
                              <div className="ellipse">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fal"
                                    data-icon="ellipsis-h"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 320 512"
                                    className="svg-inline--fa fa-ellipsis-h fa-w-10 fa-2x"
                                >
                                  <path
                                      fill="currentColor"
                                      d="M192 256c0 17.7-14.3 32-32 32s-32-14.3-32-32 14.3-32 32-32 32 14.3 32 32zm88-32c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zm-240 0c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"
                                  />
                                </svg>
                              </div>
                            </a>}
                            id="dropdown-menu-align-end"
                        >
                          {type == 1 && <Dropdown.Item eventKey="0" onClick={() => router.push('/user/my-spots/update-pet-spot?id=' + spot.id)}>
                            <a>
                              {strings.Edit}
                            </a>
                          </Dropdown.Item>}
                          <Dropdown.Item eventKey="1" onClick={() => {type == 1 ? deleteSpot(spot.id):unmarkSpot(`${spot.spot_id}`)}}>
                            <a>
                              {strings.Remove}
                            </a>
                          </Dropdown.Item>
                        </DropdownButton>
                        <DropdownButton
                            className='col-auto pet-drop mob-drop profile-share-drop p-0'
                            align="end"
                            title={<a className="menu">
                              <div className="ellipse">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="share"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512"
                                    className="svg-inline--fa fa-share fa-w-16 fa-2x"
                                >
                                  <path
                                      fill="currentColor"
                                      d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                                      className=""
                                  ></path>
                                </svg>
                              </div>
                            </a>}
                            id="dropdown-menu-align-end"
                        >
                          <p className="mb-0 font-semibold font-12">{strings.ShareThisProfile}</p>
                          <Dropdown.Item eventKey="0" href={`mailto:?subject=${spot.spot_name + ' profile'}&body=${encodeURIComponent(`${U_BASE_URL}pet-spots/${spot.id}`) || ''}`}>
                            <a className="">
                              <img src="/images/social-img4.png" />{strings.viaEmail}
                            </a>
                          </Dropdown.Item>
                          <Dropdown.Item eventKey="1" href={`https://api.whatsapp.com/send?text=${spot.spot_name + ' profile:\n' + (`${U_BASE_URL}pet-spots/${spot.id}`)}`}  data-action="share/whatsapp/share">
                            <a className="">
                              <img src="/images/social-img3.png" />{strings.viaWhatsapp}
                            </a>
                          </Dropdown.Item>
                          {/*<Dropdown.Item eventKey="2" href={`fb-messenger://share/?link=${`${U_BASE_URL}pet-spots/${spot.id}`}&app_id=${U_FACEBOOK_ID}`}>*/}
                          {/*  <a className="">*/}
                          {/*    {" "}*/}
                          {/*    <img src="/images/social-img1.png" />{strings.viaMessenger}*/}
                          {/*  </a>*/}
                          {/*</Dropdown.Item>*/}
                          <Dropdown.Item eventKey="3" onClick={(e) => copyUrl(e, spot.id)}>
                            <a className="">
                              {" "}
                              <img src="/images/copy.png" />
                              {strings.CopyLink}
                            </a>
                          </Dropdown.Item>
                        </DropdownButton>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
              {spot.note ?
                  <div className='p-2 px-3 font-24 d-block font-semibold bg-light'>
                    <span className='font-weight-bold'>{strings.Note}:- </span><span className='font-normal'>{spot.note}</span>
                  </div>
                  :<div className='d-flex pt-3 justify-content-between'>
                    <div className="input-group mb-3">
                  <input
                      type="text"
                      className="form-control"
                      name="review_reply"
                      placeholder="Write a note"
                      aria-label="Write a note"
                      aria-describedby="basic-addon2"
                      value={note}
                      onChange={(e)=> {
                        e.preventDefault()
                        setNote(e.target.value)
                      }}
                  />
                  <div className="input-group-append">
                    <button onClick={() => addNote(spot.id)} className="btn btn-primary confirm-btn mt-0">
                      {strings.AddNote}
                    </button>
                  </div>
                </div>
              </div>}
            </div>
            <div className="col-12 col-md-2 pr-0 d-none d-md-block">
              <div className="dropdown pet-drop mob-drop d-flex justify-content-end flex-column">
                <DropdownButton
                    align="end"
                    className="col-auto d-flex justify-content-end mb-3"
                    title={<a className="menu dropdown-toggle w-auto">
                      <div className="ellipse">
                        <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fal"
                            data-icon="ellipsis-h"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            className="svg-inline--fa fa-ellipsis-h fa-w-10 fa-2x"
                        >
                          <path
                              fill="currentColor"
                              d="M192 256c0 17.7-14.3 32-32 32s-32-14.3-32-32 14.3-32 32-32 32 14.3 32 32zm88-32c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zm-240 0c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"
                          />
                        </svg>
                      </div>
                    </a>}
                    id="dropdown-menu-align-end"
                >
                  {type == 1 && <Dropdown.Item eventKey="0" onClick={() => router.push('/user/my-spots/update-pet-spot?id=' + spot.id)}>
                    <a>
                      {strings.Edit}
                    </a>
                  </Dropdown.Item>}
                  <Dropdown.Item eventKey="1" onClick={() => {type == 1 ? deleteSpot(spot.id):unmarkSpot(`${spot.spot_id}`)}}>
                    <a>
                      {strings.Remove}
                    </a>
                  </Dropdown.Item>
                </DropdownButton>
                <DropdownButton
                    className='col-auto pet-drop mob-drop profile-share-drop'
                    align="end"
                    title={<a className="menu">
                      <div className="ellipse">
                        <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fas"
                            data-icon="share"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512"
                            className="svg-inline--fa fa-share fa-w-16 fa-2x"
                        >
                          <path
                              fill="currentColor"
                              d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                              className=""
                          ></path>
                        </svg>
                      </div>
                    </a>}
                    id="dropdown-menu-align-end"
                >
                  <p className="mb-0 font-semibold font-12">{strings.ShareThisProfile}</p>
                  <Dropdown.Item eventKey="0" href={`mailto:?subject=${spot.spot_name + ' profile'}&body=${encodeURIComponent(`${U_BASE_URL}pet-spots/${spot.id}`) || ''}`}>
                    <a className="">
                      <img src="/images/social-img4.png" />{strings.viaEmail}
                    </a>
                  </Dropdown.Item>
                  <Dropdown.Item eventKey="1" href={`https://api.whatsapp.com/send?text=${spot.spot_name + ' profile:\n' + (`${U_BASE_URL}pet-spots/${spot.id}`)}`}  data-action="share/whatsapp/share">
                    <a className="">
                      <img src="/images/social-img3.png" />{strings.viaWhatsapp}
                    </a>
                  </Dropdown.Item>
                  {/*<Dropdown.Item eventKey="2" href={`fb-messenger://share/?link=${`${U_BASE_URL}pet-spots/${spot.id}`}&app_id=${U_FACEBOOK_ID}`}>*/}
                  {/*  <a className="">*/}
                  {/*    {" "}*/}
                  {/*    <img src="/images/social-img1.png" />{strings.viaMessenger}*/}
                  {/*  </a>*/}
                  {/*</Dropdown.Item>*/}
                  <Dropdown.Item eventKey="3" onClick={(e) => copyUrl(e, spot.id)}>
                    <a className="">
                      {" "}
                      <img src="/images/copy.png" />
                      {strings.CopyLink}
                    </a>
                  </Dropdown.Item>
                </DropdownButton>
              </div>
            </div>
          </div>
          <hr/>
        </>}
      </>
  );
};

export default SpotObject;
