import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
    InfoWindow
} from "react-google-maps";
import React, {useEffect, useRef, useState} from "react";
import { useRouter } from "next/router";
import {GOOGLE_PLACES_API} from "../../../api/Constants";
import { compose, withProps, withHandlers, withState } from "recompose";



const CustomMarker = (props) => {
    const router = useRouter();
    const { info, lat, lng, currentHoveringSitter, id, isSpot } = props;
    return (
        <Marker
            position={{ lat, lng }} >
                <InfoWindow>
                    <div onClick={()=> isSpot == true ? router.push(`/pet-spots/${id}`) : router.push(`/sitter-profile/${id}?serviceId=1`) } className={`marker-cus ${currentHoveringSitter?.id == id ? 'selected' : ''}`}>
                        <img className='mb-2' src="/images/map-marker.png"/>
                        <p>{info}</p>
                    </div>
                </InfoWindow>
        </Marker>
    );
}


const MyMapComponent = compose(
    withProps({
        googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_PLACES_API}&v=3.exp&libraries=geometry,drawing,places`,
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div className="map-full" style={{ height: `540px` }} />,
        mapElement: <div style={{ height: `100%` }} />
    }),
    withState("zoom", "onZoomChange", 8),
    withHandlers((props) => {
        const refs = {
            map: undefined
        };

        return {
            onMapMounted: () => ref => {
                refs.map = ref;
            },
            onZoomChanged: () => () => {
                var bounds = refs.map.getBounds();
                var ne = bounds.getNorthEast(); // LatLng of the north-east corner
                var sw = bounds.getSouthWest(); // LatLng of the south-west corner
                props?.handlingMapOperation ? props.handlingMapOperation(ne, sw) : null; ///// need to include
            },
            onBoundsChanged: () => () => {
                var bounds = refs.map.getBounds();
                var ne = bounds.getNorthEast(); // LatLng of the north-east corner
                var sw = bounds.getSouthWest(); // LatLng of the south-west corner
                props?.handlingMapOperation ? props.handlingMapOperation(ne, sw) : null;
            }
        };
    }),
    withScriptjs,
    withGoogleMap,

)(props =>
    <GoogleMap
        key={props?.key ? props?.key : 'google_map'}
        defaultZoom={props.mapZoom ? props.mapZoom : 10}
        zoom={props.mapZoom ? props.mapZoom : 10}
        defaultCenter={props?.mapCenter ? props?.mapCenter : {lat: 35.1194434, lng: 135.4522967}}
        center={props?.mapCenter ? props?.mapCenter : props.defaultCenter}
        onZoomChanged={props.onZoomChanged}
        ref={props.onMapMounted}
        onDragEnd={props.onBoundsChanged}
        >
        {!!props.latlng ? props.latlng.length ? props.latlng.map((val, index)=>
                <CustomMarker currentHoveringSitter={props.currentHoveringSitter} key={index} info={index+1} id={props.data ? props.data[index].id:''} lat={val.lat} lng={val.lng} isSpot={props?.isSpot ? props?.isSpot : false} />
            ):<Marker
                onDragEnd={props?.draggable ? props.onChangeLatLng : null}
                animation={2}
                draggable={props.draggable}
                position={{lat: props.latlng.lat, lng: props.latlng.lng}}
            />
            :<Marker
                onDragEnd={props?.draggable ? props.onChangeLatLng : null}
                animation={2}
                draggable={props.draggable}
                position={{lat: 36.2048, lng: 138.2529}}
            />
        }

    </GoogleMap>
);



export default MyMapComponent;
