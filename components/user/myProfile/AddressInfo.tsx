import React, { useEffect, useState } from "react";
import API from "../../../api/Api";
import { strings } from "../../../public/lang/Strings";
import { useSnackbar } from "react-simple-snackbar";
import { errorOptions, successOptions } from "../../../public/appData/AppData";
import MyMapComponent from "./Map";
import LocalityInfo from "./LocalityInfo";
import { useRouter } from 'next/router';
import LocationSearchInput2 from "../../common/LocationSearchInput2";
import {GOOGLE_PLACES_API} from "../../../api/Constants";
import Geocode from "react-geocode";
import Loader from "../../common/Loader";

interface IState {
  house_number: string;
  address: string;
  city: string;
  prefecture:string,
  locality: string;
  postcode: string;
  hide_address: number;
  map_address: string;
  latitude: string;
  longitude: string;
}

let initialState = {
  house_number: "",
  address: "",
  city: "",
  locality: '',
  postcode: "",
  country_id: null,
  hide_address: 0,
  map_address: '',
  latitude: null,
  longitude: null,
  prefecture:"",
};

interface I_PROPS {
  address: any;
  locality: any;
}

type T_LAT_LNG = {
  lat: string;
  lng: string;
};

const api = new API();
const AddressInfo: React.FC<I_PROPS> = (props) => {
  const router = useRouter();
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [address, setAddress] = useState<IState>(
    props.address ? props.address : initialState
  );
  const [latlng, setLatLng] = useState<T_LAT_LNG>({lat: '', lng: ''});
  const [maplatlng, setMapLatLng] = useState<T_LAT_LNG>({lat: '35.1194434', lng: '135.4522967'});
  const [errors, setErrors] = useState<any>({});
  const [value, setValue] = useState<string>('');
  const [mapSearch, setMapsearch] = useState<string>('')
  const [showMap, setShowMap] = useState(false)

  useEffect(() => {
    Geocode.setApiKey(GOOGLE_PLACES_API);
    if (props.address) {
      setAddress(props.address);
      if (props.address?.map_latitude) {
        setMapLatLng({
          lat: props.address.map_latitude,
          lng: props.address.map_longitude,
        });
        coordinates_to_address(Number(props.address.map_latitude), Number(props.address.map_longitude))
      }
      if (props.address.latitude) {
        setLatLng({
          lat: props.address.latitude,
          lng: props.address.longitude,
        });
      }
      setValue(props.address.address)
    }
  }, [props.address]);
  useEffect(() => {
    setTimeout(() => setShowMap(true), 1000)
  }, []);

  const coordinates_to_address = (lat, lng) => {
    Geocode.fromLatLng(lat, lng).then(
        (response) => {
          const address = response;
          if(address) {
            setMapsearch(address?.results[0]?.formatted_address)
          } else {
            console.log(response)
          }
        },
        (error) => {
          console.error(error);
        }
    );
  }

  const onTextChange = (event) => {
    setAddress({ ...address, [event.target.name]: event.target.value });
  };

  const saveAddress = (locality: any) => {
   
    setErrors({});
    let data = JSON.stringify({
      ...address,
      latitude: latlng ? String(latlng.lat) : '',
      longitude: latlng ? String(latlng.lng) : '',
      map_latitude: maplatlng ? maplatlng.lat : null,
      map_longitude: maplatlng ? maplatlng.lng : null,
      map_address: address.map_address,
      ...locality
    });
    api
      .saveAddress(data)
      .then((json) => {
        openSuccess(strings.SavedSuccessfully);
        router.push("/user/my-services").then(r => {})
      })
      .catch((error) => {
        if (error.response.status == 422) {
          setErrors(error.response.data.errors);
          if (error.response.data.errors.description) {
            openError(error.response.data.errors.description);
          }
        }
      });
  };


  return (
    <>
      <div className="pay-tabs">
        <div className="basic-info">
          <div className="col-12 pb-md-3 px-0">
            <div className="row align-items-center">
              <div className="col">
                <h5 className="mb-0 font-semibold">{strings.AddressInfo}</h5>
              </div>
            </div>
          </div>
          <hr className="d-block d-md-none d-lg-none d-xl-none" />
          <div className="row">
            <div className="col-12 col-md-5">
              <div className="basic-info-details">
                <p className="mb-0">{strings.HomeAddressLine}</p>
              </div>
            </div>
            <div className="col-12 col-md">
              <div className="basic-info-details location-search-input">
                <LocationSearchInput2
                    key={'address1'}
                  className={"form-control mb-0 " + (errors.address ? "invalid" : "")}
                  value={value}
                  setFilter={(Address)=>{
                    setAddress({
                      ...address,
                      city: Address.city,
                      prefecture: Address.prefecture,
                      locality: Address.locality,
                      postcode: Address.postalCode,
                      address: Address.value,
                    })
                    setValue(Address.value)
                  }
                  }
                  setLatLng={(latLng)=>{
                    setAddress({
                      ...address,
                      latitude: String(latLng.lat),
                      longitude: String(latLng.lng),
                    })
                    setLatLng({lat: String(latLng.lat), lng: String(latLng.lng)})
                  }
                  }
                  setValue={(Address)=> {
                    setValue(Address)
                    if(Address == ''){
                      setAddress({...initialState, map_address: address.map_address});
                      setLatLng({lat: '', lng: ''})
                    }
                  }}
                />
              </div>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-12 col-md-5">
              <div className="basic-info-details">
                <p className="mb-0">{strings.AptHouseNo}</p>
              </div>
            </div>
            <div className="col-12 col-md">
              <div className="basic-info-details">
                <input
                  type="text"
                  className={
                    "form-control mb-0 " +
                    (errors.house_number ? "invalid" : "")
                  }
                  name="house_number"
                  placeholder="House No."
                  value={address.house_number}
                  onChange={onTextChange}
                />
              </div>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-12 col-md-5">
              <div className="basic-info-details">
                <p className="mb-0">{strings.City}</p>
              </div>
            </div>
            <div className="col-12 col-md">
              <div className="basic-info-details">
                <div className="basic-info-details">
                  <input
                    type="text"
                    className={
                      "form-control mb-0 " + (errors.city ? "invalid" : "")
                    }
                    placeholder="City"
                    name="city"
                    onChange={onTextChange}
                    value={address.city}
                    disabled={true}
                  />
                </div>
              </div>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-12 col-md-5">
              <div className="basic-info-details">
                <p className="mb-0">{strings.Prefecture}</p>
              </div>
            </div>
            <div className="col-12 col-md">
              <div className="basic-info-details">
                <input
                  type="text"
                  className={
                    "form-control mb-0 " + (errors.prefecture ? "invalid" : "")
                  }
                  name="prefecture"
                  onChange={onTextChange}
                  placeholder="Prefecture"
                  value={address.prefecture}
                  disabled={true}
                />
              </div>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-12 col-md-5">
              <div className="basic-info-details">
                <p className="mb-0">{strings.ZIPpostalpostcode}</p>
              </div>
            </div>
            <div className="col-12 col-md">
              <div className="basic-info-details">
                <input
                  type="text"
                  className={
                    "form-control mb-0 " + (errors.postcode ? "invalid" : "")
                  }
                  name="postcode"
                  onChange={onTextChange}
                  placeholder="Post Code"
                  value={address.postcode}
                  // disabled={true}
                />
              </div>
            </div>
          </div>
          <hr />
          <div className="custom-check mt-4 location-search-input">
            <label className="check ">
              <input
                checked={address.hide_address===1}
                onChange={(e) =>
                  setAddress({ ...address, hide_address: address.hide_address === 1 ? 0 : 1 })
                }
                type="checkbox"
                name="is_name1"
              />
              <span className="checkmark" />
              {strings.HideMyExactAddressWhenShowingMyProfileToTheClientIUnderstandTheLocalityNeedsToShow}
            </label>
          </div>
          <hr />
          <div className="basic-details mt-0">
            <h5 className="font-semibold">{strings.SearchLocationOnMap}</h5>
          </div>
          <div className='location-search-input profile-map'>
            <LocationSearchInput2
                key={'address2'}
              className={"form-control mb-3 " + (errors.address ? "invalid" : "")}
              value={mapSearch}
              setFilter={(Address)=>{
                setAddress({
                    ...address,
                  map_address: Address.value,
                })
              }
              }
              setLatLng={(latLng)=>{
                setMapLatLng({ lat: String(latLng.lat), lng: String(latLng.lng) })
              }
              }
              setValue={(value)=> {
                setMapsearch(value)
                if(value == ''){
                  setAddress({...address, map_address: ''});
                  setLatLng({lat: '', lng: ''})
                }
              }}
            />
          </div>
          {/*{console.log({lat: Number(maplatlng?.lat), lng: Number(maplatlng?.lng)})}*/}
          {showMap ? <MyMapComponent
            onChangeLatLng={(e) => {
              setMapLatLng({ lat: String(e.latLng.lat()), lng: String(e.latLng.lng()) });
              coordinates_to_address(e.latLng.lat(), e.latLng.lng())
            }}
            draggable={true}
            defaultCenter={{lat: Number(maplatlng?.lat), lng: Number(maplatlng?.lng)}}
            mapCenter={{lat: Number(maplatlng?.lat), lng: Number(maplatlng?.lng)}}
            latlng={{lat: Number(maplatlng?.lat), lng: Number(maplatlng?.lng)}}
            mapZoom={15}
          /> : <Loader />}
        </div>
      </div>
      <LocalityInfo callApi={saveAddress} locality={props.locality} />
    </>
  );
};

export default AddressInfo;
