import React from "react";
import { strings } from "../../../public/lang/Strings";
import I_AVAILABLE_SERVICE from "../../../models/availableService.interface";
import { withSnackbar } from "react-simple-snackbar";
import {Accordion} from "react-bootstrap";
import {pet} from "../../../public/appData/AppData";
import {getCurrencySign} from "../../../api/Constants";

interface IState {
  availableService: I_AVAILABLE_SERVICE[];
}

interface IProps {
  availableService: I_AVAILABLE_SERVICE[];
  setAvailableService: (weekID, serviceId, isAvailable, index) => void;
  openSnackbar: withSnackbar;
}

class ProfileAvailability extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      availableService: this.props.availableService,
    };
  }

  isChecked = (id, index) => {
    return this.state.availableService[index].service.availability.some(
      (mValue) => mValue.weekday_id == id
    );
  };

  componentDidUpdate(prevProps) {
    if (prevProps.availableService !== this.props.availableService) {
      this.setState({
        availableService: this.props.availableService,
      });
    }
  }
  render() {
    return (
      <div className="pay-tabs">
        {this.state.availableService.length > 0 &&
            <div className="available-details">
              <h5 className="font-semibold pb-1">{strings.Defaultavailability}</h5>
              <Accordion className="accordion">
                {this.state.availableService.map((value, index) => (
                    <div key={index} className="panel panel-default availability">
                    <Accordion.Item eventKey={`${index}`} className='m-0 p-0 border-none'>
                      <Accordion.Header className="card panel-heading mb-0 pb-0">
                        <h3 className="panel-title mb-0">
                          <a className="">
                            {value.service.name}
                          </a>
                        </h3>
                      </Accordion.Header>
                      <Accordion.Body className='card mt-0 pt-0'>
                        <div className="panel-body content">
                          <div className="display">
                            <div className="custom-check my-2 my-md-0">
                              <label className="check ">
                                <input
                                    type="checkbox"
                                    className="class1"
                                    name="is_name2"
                                    defaultValue="dog"
                                    checked={this.isChecked(2, index)}
                                    onChange={(e) =>
                                        this.props.setAvailableService(
                                            2,
                                            value.service.id,
                                            e.target.checked,
                                            index
                                        )
                                    }
                                />
                                <span className="checkmark"/> {strings.Monday}
                              </label>
                            </div>
                            <div className="custom-check my-2 my-md-0">
                              <label className="check ">
                                <input
                                    type="checkbox"
                                    className="class1"
                                    name="is_name2"
                                    defaultValue="dog"
                                    checked={this.isChecked(3, index)}
                                    onChange={(e) =>
                                        this.props.setAvailableService(
                                            3,
                                            value.service.id,
                                            e.target.checked,
                                            index
                                        )
                                    }
                                />
                                <span className="checkmark"/> {strings.Tuesday}
                              </label>
                            </div>
                            <div className="custom-check my-2 my-md-0">
                              <label className="check ">
                                <input
                                    type="checkbox"
                                    className="class1"
                                    name="is_name2"
                                    defaultValue="dog"
                                    checked={this.isChecked(4, index)}
                                    onChange={(e) =>
                                        this.props.setAvailableService(
                                            4,
                                            value.service.id,
                                            e.target.checked,
                                            index
                                        )
                                    }
                                />
                                <span className="checkmark"/> {strings.Wednesday}
                              </label>
                            </div>
                            <div className="custom-check my-2 my-md-0">
                              <label className="check ">
                                <input
                                    type="checkbox"
                                    className="class1"
                                    name="is_name2"
                                    defaultValue="dog"
                                    checked={this.isChecked(5, index)}
                                    onChange={(e) =>
                                        this.props.setAvailableService(
                                            5,
                                            value.service.id,
                                            e.target.checked,
                                            index
                                        )
                                    }
                                />
                                <span className="checkmark"/> {strings.Thursday}
                              </label>
                            </div>
                            <div className="custom-check my-2 my-md-0">
                              <label className="check ">
                                <input
                                    type="checkbox"
                                    className="class1"
                                    name="is_name2"
                                    defaultValue="dog"
                                    checked={this.isChecked(6, index)}
                                    onChange={(e) =>
                                        this.props.setAvailableService(
                                            6,
                                            value.service.id,
                                            e.target.checked,
                                            index
                                        )
                                    }
                                />
                                <span className="checkmark"/> {strings.Friday}
                              </label>
                            </div>
                            <div className="custom-check my-2 my-md-0">
                              <label className="check ">
                                <input
                                    type="checkbox"
                                    className="class1"
                                    name="is_name2"
                                    defaultValue="dog"
                                    checked={this.isChecked(7, index)}
                                    onChange={(e) =>
                                        this.props.setAvailableService(
                                            7,
                                            value.service.id,
                                            e.target.checked,
                                            index
                                        )
                                    }
                                />
                                <span className="checkmark"/> {strings.Saturday}
                              </label>
                            </div>
                            <div className="custom-check my-2 my-md-0">
                              <label className="check ">
                                <input
                                    type="checkbox"
                                    className="class1"
                                    name="is_name2"
                                    defaultValue="dog"
                                    checked={this.isChecked(1, index)}
                                    onChange={(e) =>
                                        this.props.setAvailableService(
                                            1,
                                            value.service.id,
                                            e.target.checked,
                                            index
                                        )
                                    }
                                />
                                <span className="checkmark"/> {strings.Sunday}
                              </label>
                            </div>
                          </div>
                        </div>
                      </Accordion.Body>
                    </Accordion.Item>
                    </div>
                ))}
              </Accordion>
            </div>
        }
      </div>
    );
  }
}

export default withSnackbar(ProfileAvailability);
