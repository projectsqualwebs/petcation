import React, { useEffect, useState} from "react";
import IDashboard from "../../../models/dashboard.interface";
import { strings } from "../../../public/lang/Strings";
import "react-day-picker/lib/style.css";
import DayPickerInput from "react-day-picker/DayPickerInput";
import parseDate from "react-day-picker/moment";
import formatDate from "react-day-picker/moment";
import API from "../../../api/Api";
import { AxiosError, AxiosResponse } from "axios";
import Res from "../../../models/response.interface";
import YearMonthForm from "../../common/YearMonthForm";
import { useSnackbar } from "react-simple-snackbar";
import {
  errorOptions,
  experienceOption,
  successOptions,
} from "../../../public/appData/AppData";
import moment from "moment";
import { dataURLtoFile, numberInput } from "../../../utils/Helper";
import Modal from "react-bootstrap/Modal";
import { useRef } from "react";
import UploadFileModal from "../../common/UploadfileModal";
import { useRouter } from "next/router";
import { EventEmitter } from "../../../public/EventEmitter";
import { Card } from "react-bootstrap";
import Cookies from "universal-cookie";
import { deleteCookie } from "../../../utils/Helper";
import Loader from "../../common/Loader";
import Select from "react-select";
import Link from "next/link";


let defaultState = {
  id: 0,
  experience_in_month: null,
  experience_in_year: null,
  firstname: "",
  lastname: "",
  description: "",
  email: "",
  email_verified_at: null,
  facebook_id: null,
  google_id: null,
  twitter_id: null,
  line_id: "",
  phone_number: null,
  dob: null,
  profile_picture: "",
  primary_contact_person: null,
  primary_contact_number: null,
  secondary_contact_person: null,
  secondary_contact_number: null,
  created_at: "",
  updated_at: "",
  passport_image: "",
  liscense_image: "",
  passport_number: "",
  is_verified: 0,
  liscense_number: "",
  user_document: [
    {
      document_number: "",
      path: "",
      document_type: 1,
      id: null,
      status: 0,
    },
    {
      document_number: "",
      path: "",
      document_type: 1,
      id: null,
      status: 0,
    },
  ],
  user_skills: [],
};

const api = new API();

interface I_SELECT {
  key: number;
  value: string;
  label: string;
}

/**
 *
 * @param props contains function to change our tab according to our flow.
 * this function contains view of basic info page.
 */
const BasicInfo = (props: any) => {
  const cookies = new Cookies();
  const router = useRouter();
  const [info, setInfo] = useState<IDashboard>(defaultState);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [errors, setErrors] = useState<any>({});
  const [month, setMonth] = useState();

  const [selectedType, setSelectedType] = useState<number>(1);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [skills, setSkills] = useState<any>([]);
  const [userSkills, setUserSkills] = useState<any>([]);

  const [documentLoading, setDocumentLoading] = useState<number>(0);
  const promiseInfo = useRef<any>({});
  const [experience, setExperience] = useState<I_SELECT>(null);
  const highlightRef = useRef(null);

  /**
   *
   * function is to change the value in state from our different text fields.
   * @param event : contains value changed in text field of their respecive field name.
   * @success : change the state of our component.
   */
  const onTextChange = (event) => {
    event.preventDefault();
    setInfo({ ...info, [event.target.name]: event.target.value });
    if (Object.keys(errors).includes(event.target.name)) {
      let newValue = { ...errors };
      delete newValue[event.target.name];
      setErrors(newValue);
    }
  };
  const onExpChange = (event) => {
    setExperience(event);
    setInfo({ ...info, experience_in_month: 0 });
    if (errors.experience_in_year) {
      let newValue = { ...errors };
      delete newValue?.experience_in_year;
      delete newValue?.experience_in_month;
      setErrors(newValue);
    }
  };

  /**
   * here we call api's before page load
   * in useEffect to get basic info skills and user skills
   */
  useEffect(() => {

    getBasicInfo();
    getSkills();
    if (router.query.scroll==='1' && highlightRef.current ) {
      highlightRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  }, []);



  /**
   * funtion to show modal
   * @promise:
   *    resolve: will open modal
   *    reject: will not do any action revert back to initial stage.
   */
  const showModal = async () => {
    return new Promise((resolve, reject) => {
      promiseInfo.current = {
        resolve,
        reject,
      };
      setOpenModal(true);
    });
  };

  /**
   * function contains modal actions to update profile
   * @updateProfile: funtion will update user profile.
   * @isEdit is boolean value taken from state of the component.
   */
  const modalAction = async () => {

      try {
        if (!info.firstname) {
          let err = { ...errors };
          err.firstname = strings.Required;
          setErrors(err);
          return false;
        }
        if (!info.lastname) {
          let err = { ...errors };
          err.lastname = strings.Required;
          setErrors(err);
          return false;
        }
        if (!info.description) {
          let err = { ...errors };
          err.description = strings.Required;
          setErrors(err);
          return false;
        }
        if (!info.phone_number) {
          let err = { ...errors };
          err.phone_number = strings.Required;
          setErrors(err);
          return false;
        }
        if (!info.dob) {
          let err = { ...errors };
          err.dob = strings.Required;
          setErrors(err);
          return false;
        }
        if (!experience?.value) {
          let err = { ...errors };
          err.experience_in_year = strings.selectExperienceError;
          setErrors(err);
          return false;
        }
        if (experience?.key === 8 && !info.experience_in_month) {
          let err = { ...errors };
          err.experience_in_month = strings.Required;
          setErrors(err);
          return false;
        }
        if (!info.primary_contact_person) {
          let err = { ...errors };
          err.primary_contact_person = strings.Required;
          setErrors(err);
          return false;
        }
        if (!info.primary_contact_number) {
          let err = { ...errors };
          err.primary_contact_number = strings.Required;
          setErrors(err);
          return false;
        }
        if (info.secondary_contact_person && !info.secondary_contact_number) {
          let err = { ...errors };
          err.secondary_contact_number = strings.Required;
          setErrors(err);
          return false;
        }

        updateProfile();
      } catch (err) {
        console.log(err);
        setOpenModal(false);
      }

  };

  /**
   * @funtion this funtion will update user profile, it contains api call
   * this function will also update user details in our cookies.
   * @success: function will update data in cookies and backend.
   * @failure: function will respond error message in user display.
   */
  const updateProfile = () => {
    let stringy = JSON.stringify(info);
    let a = JSON.parse(stringy);
    a.skills = userSkills;
    a.experience_in_year = experience?.value;
    if (a.user_document.length && a.user_document.length > 1) {
      // a.user_document = []
      let x = info.user_document[a.user_document.length - 1];
      x.document_number = info.passport_number;
      x.path = info.passport_image;
      let y = info.user_document[a.user_document.length - 2];
      y.document_number = info.liscense_number;
      y.path = info.liscense_image;
      a.user_document = [x, y];
    }
    let data = JSON.stringify(a);
    api
      .saveBasicInfo(data)
      .then((json) => {
        cookies.set("lastname", json.data.response.lastname, { path: "/" });
        document.cookie = `firstname=${json.data.response.firstname}; path=/`;
        document.cookie = `phone_number=${json.data.response.phone_number}; path=/`;
        EventEmitter.dispatch("updateUserDetail", json.data.response);
        openSuccess(strings.SavedSuccessfully);
        props.handleTabChange();
        setErrors({});
        // console.log(data);
      })
      .catch((error: AxiosError) => {
        if (error.response.status == 422) {
          setErrors(error.response.data.errors);
        } else {
          setErrors({});
        }
      });
  };

  /**
   * this function will help to get all basic information of user from the server.
   */
  const getBasicInfo = async () => {
    api
      .getBasicInfo()
      .then((response: AxiosResponse<Res<IDashboard>>) => {
        if (response.data.response.user_document) {
          if (response.data.response?.user_document?.length > 0) {
            let userDetail = response.data.response;
            userDetail.passport_number =
              response.data.response?.user_document[0]?.document_number ==
              "undefined"
                ? ""
                : response.data.response?.user_document[0]?.document_number;
            userDetail.passport_image =
              response.data.response?.user_document[0]?.path ?? "";
            userDetail.liscense_number =
              response.data.response?.user_document[1]?.document_number ==
              "undefined"
                ? ""
                : response.data.response?.user_document[1]?.document_number;
            userDetail.liscense_image =
              response.data.response?.user_document[1]?.path ?? "";
            setInfo(userDetail);
          } else {
            let userDetail = response.data.response;
            userDetail.passport_number = "";
            userDetail.passport_image = "";
            userDetail.liscense_number = "";
            userDetail.liscense_image = "";
            setInfo(userDetail);
          }
          let userData = response.data.response;
          setExperience(
            experienceOption.find(
              (val) => val?.value === userData?.experience_in_year
            )
          );
        } else {
          setInfo(response.data.response);
        }
        setUserSkills(
          response.data.response.user_skills.map((val) => val.skill_id)
        );
      })
      .catch((error) => console.log(error));
  };

  /**
   * @params: month contains the value of month of our custom date picker.
   * function will change the value of month of our custom calendar.
   */
  const handleYearMonthChange = (month) => {
    setMonth(month);
  };

  /**
   * funtion will upload our document to the server.
   * @params: v contians document.
   * @success: Document uploaded successfully.
   * @failure: give error in response or display it on user screen.
   */
  const uploadDoument = (v, type) => {
    var file = v;
    console.log(v);
    const formData = new FormData();
    formData.append("document_type", String(type));
    formData.append("document", file);
    api
      .uploadDocument(formData)
      .then((json) => {
        let detail = info;
        if (type == 1) {
          detail.user_document[0] = json.data.response;
          detail.passport_image = json.data.response.path;
        } else if (type == 2) {
          detail.user_document[1] = json.data.response;
          detail.liscense_image = json.data.response.path;
        }
        openSuccess(strings.SuccessfullyUploadedImage);
        setInfo(detail);
        setDocumentLoading(0);
      })
      .catch((error) => {
        setDocumentLoading(0);
        openError(strings.UploadFailed);
      });
  };

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      fontSize: 14,
    }),

    control: (provided) => ({
      ...provided,
      width: "100%",
      borderColor: "#cfd7de",
      height: "100%",
      marginBottom: 0,
      paddingBottom: 0,
    }),
    singleValue: (provided, state) => {
      return { ...provided, fontSize: 12, color: "#383838", fontWeight: "500" };
    },
  };

  /**
   * funtions change the file in local which are going to upload by another funtion.
   * @event: contains our file which we want to upload.
   * @type: contains type of file
   *          @1: for passport file.
   *          @2: for license file.
   */
  const onFileChange = (event, type) => {
    if (event.dataTransfer || event.target.files) {
      let files;
      if (event.dataTransfer) {
        files = event.dataTransfer.files;
      } else if (event.target) {
        files = event.target.files;
      }
      const reader = new FileReader();
      reader.onload = () => {
        setDocumentLoading(type);
      };
      setSelectedType(type);
      if (files[0]) {
        reader.readAsDataURL(files[0]);
        uploadDoument(files[0], type);
      }
    }
  };

  /**
   * getSkills helps to get list of all skills.
   * @success: set all skill in our component state.
   * @failure: give error in console.
   */
  const getSkills = () => {
    api
      .getSkills()
      .then((res) => {
        setSkills(res.data.response);
      })
      .catch((error) => console.log(error));
  };

  /**
   * addSkill function will update user skills.
   * @params: id contains user id.
   * @success: User Skills are successfully updated.
   * @failure: console error and skills are not updated at the server end.
   */
  const addSkill = (id, e) => {
    if (userSkills.find((val) => val === id)) {
      let newSkills = userSkills.filter((val) => val !== id);
      setUserSkills(newSkills);
    } else {
      let newSkills = [...userSkills, id];
      setUserSkills([...newSkills]);
    }
  };

  /**
   * funtion will help to reupload our user documents.
   * @params: props contains boolean value to check the field for which we are changing the file.
   *
   */
  const setUserDocument = (props) => {
    if (props == 1) {
      setInfo({ ...info, liscense_image: "" });
    } else {
      setInfo({ ...info, passport_image: "" });
    }
  };

  return (
    <>
      <div>
        <div className="pay-tabs">
          <div className="basic-info">
            <div className="col-12 pb-3 px-0">
            {info?.user_document.length < 2 ? (
                    <div className="alert alert-danger" role="alert">
                           <div className="w-100 border-0">
                              <span>
                                <small className="m-2">
                                  <b>{strings.Tobecomeasitterneedtouploadthedocuments}</b>
                                </small>
                              </span>
                            </div>
                          </div>
                  ) : (
                    <>
                      {info?.user_document.length >= 2 && info?.is_verified != 1 && (
                       <div className="alert alert-danger" role="alert">
                         <div className=" w-100 border-0">
                           <span>
                             <small className="m-0">
                               <b>
                                 {info?.user_document[0]?.status === 2 || info?.user_document[1]?.status
                                   ? strings.ProfilerejectedPleaseuploadvaliddetailsanddocuments
                                   : strings.Yourprofileisunderreviewandwillbeapprovedsoon}
                               </b>
                             </small>
                           </span>
                         </div>
                       </div>
                      )}
                    </>
                  )}
              <div className="row align-items-center">
                <div className="col">
                  <h5 className="mb-0 font-semibold">{strings.basicinfo}</h5>
                </div>

              <div className="col-auto d-none d-lg-block">
                  <button onClick={modalAction} className="btn btn-primary btn-lg">
                       {strings.Save}
                  </button>
              </div>
              </div>
            </div>
            <div >
              <div className="row align-items-center">
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.Firstname}
                      <small className="text-danger">*</small>
                    </p>
                    {errors?.firstname ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">
                          *{errors?.firstname}
                        </small>
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="basic-info-details">
                    <input
                      type="text"
                      className={
                        "form-control mb-0 " +
                        (errors?.firstname ? "invalid" : "")
                      }
                      name="firstname"
                      placeholder="First Name"
                      value={info.firstname}
                      onChange={onTextChange}

                    />
                  </div>
                </div>
              </div>
              <hr />
              <div className="row align-items-center">
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.Lastname}
                      <small className="text-danger">*</small>
                    </p>
                    {errors?.lastname ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">
                          *{errors?.lastname}
                        </small>
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="basic-info-details">
                    <input
                      type="text"
                      name="lastname"
                      placeholder="Last Name"
                      value={info.lastname}
                      onChange={onTextChange}
                      className={
                        "form-control mb-0 " +
                        (errors?.lastname ? "invalid" : "")
                      }
                    />
                  </div>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.ProfileDescription}
                      <small className="text-danger">*</small>
                    </p>
                    {errors?.description ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">
                          *{errors?.description}
                        </small>
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="basic-info-details">
                    <textarea
                      name="description"
                      rows={5}
                      placeholder="Profile description"
                      value={info.description}
                      onChange={onTextChange}
                      className={
                        "form-control mb-0 " +
                        (errors?.description ? "invalid" : "")
                      }

                    />
                  </div>
                </div>
              </div>
              <hr />
              <div
                className="row align-items-center"

              >
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.Emailaddress}
                      <small className="text-danger">*</small>
                    </p>
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="basic-info-details">
                    <input
                      type="text"
                      placeholder="Email"
                      value={info.email}
                      readOnly={true}
                      className={
                        "form-control mb-0 " + (errors.email ? "invalid" : "")
                      }
                    />
                  </div>
                </div>
              </div>
              <hr />
              <div
                className="row align-items-center"

              >
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.Contactnumber}
                      <small className="text-danger">*</small>
                    </p>
                    {errors?.phone_number ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">
                          *{errors?.phone_number}
                        </small>
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="basic-info-details">
                    <input
                      type="text"
                      maxLength={11}
                      onKeyPress={numberInput}
                      name="phone_number"
                      placeholder="Phone Number"
                      value={info?.phone_number}
                      onChange={onTextChange}
                      className={
                        "form-control mb-0 " +
                        (errors.phone_number ? "invalid" : "")
                      }

                    />
                  </div>
                </div>
              </div>
              <hr />
              <div
                className="row align-items-center"

              >
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.Birthdate}
                      <small className="text-danger">*</small>
                    </p>
                    {errors?.dob ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">*{errors?.dob}</small>
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="basic-info-details">
                    <div className="form-control mb-0">
                      <DayPickerInput
                        formatDate={formatDate.formatDate}
                        parseDate={parseDate.parseDate}
                        dayPickerProps={{
                          modifiers: {
                            disabled: [
                              {
                                after: new Date(),
                              },
                            ],
                          },
                          month: month,
                          captionElement: ({ date, localeUtils }) => (
                            <YearMonthForm
                              isDob={true}
                              before={true}
                              date={date ? date : new Date()}
                              localeUtils={localeUtils}
                              onChange={handleYearMonthChange}
                            />
                          ),
                        }}
                        inputProps={{
                          style: {
                            border: 0,
                            background: "transparent",
                          },
                          readOnly: true,
                        }}
                        placeholder="DD/MM/YYYY"
                        format="DD/MM/yyyy"
                        value={info.dob}
                        onDayChange={(date) =>
                          setInfo({
                            ...info,
                            dob: `${moment(date).format("DD/MM/yyyy")} `,
                          })
                        }
                      />
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div className="row align-items-center">
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.Experience}
                      <small className="text-danger">*</small>
                    </p>
                    {errors?.experience_in_year ? (
                      <label className={"error-text m-0 d-block"}>
                        {errors?.experience_in_year}
                      </label>
                    ) : null}
                    {errors?.experience_in_month ? (
                      <label className={"error-text m-0 d-block"}>
                        {errors?.experience_in_month}
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="row">
                    <div
                      className={`basic-info-details ${
                        experience?.key === 8 ? "col-6" : "col-12"
                      }`}
                    >
                      <Select
                        value={experience}
                        isSearchable={false}
                        onChange={onExpChange}
                        options={experienceOption}
                        className="w-100"
                        styles={customStyles}
                      />
                    </div>
                    {experience?.key === 8 ? (
                      <div className="basic-info-details col-6">
                        <input
                          type="text"
                          name="experience_in_month"
                          placeholder="Enter years"
                          onKeyPress={numberInput}
                          maxLength={2}
                          value={info.experience_in_month}
                          onChange={onTextChange}
                          className={
                            "form-control mb-0 " +
                            (errors.experience_in_month ? "invalid" : "")
                          }

                        />
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <hr />
              <div
                className="row align-items-center"

              >
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">
                      {strings.EmergencyContact1}
                      <small className="text-danger">*</small>
                    </p>
                    {errors?.primary_contact_person ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">
                          *{errors?.primary_contact_person}
                        </small>
                      </label>
                    ) : null}
                    {errors?.primary_contact_number ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">
                          *{errors?.primary_contact_number}
                        </small>
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="row">
                    <div className="col-6 basic-info-details w-100">
                      <input
                        type="text"
                        maxLength={25}
                        name="primary_contact_person"
                        placeholder="Name"
                        value={info.primary_contact_person}
                        onChange={onTextChange}
                        className={
                          "form-control mb-0 " +
                          (errors.primary_contact_person ? "invalid" : "")
                        }

                      />
                    </div>
                    <div className="col-6 basic-info-details w-100">
                      <input
                        type="text"
                        maxLength={11}
                        onKeyPress={numberInput}
                        name="primary_contact_number"
                        placeholder="Number"
                        value={info.primary_contact_number}
                        onChange={onTextChange}
                        className={
                          "form-control mb-0 " +
                          (errors.primary_contact_number ? "invalid" : "")
                        }

                      />
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div
                className="row align-items-center"

              >
                <div className="col-12 col-md-5">
                  <div className="basic-info-details">
                    <p className="mb-0">{strings.EmergencyContact2}</p>
                    {errors?.secondary_contact_number ? (
                      <label className="m-0 d-block">
                        <small className="text-danger">
                          *{errors?.secondary_contact_number}
                        </small>
                      </label>
                    ) : null}
                  </div>
                </div>
                <div className="col-12 col-md">
                  <div className="row">
                    <div className="col-6 basic-info-details">
                      <input
                        type="text"
                        maxLength={25}
                        name="secondary_contact_person"
                        placeholder="Name"
                        value={info.secondary_contact_person}
                        onChange={onTextChange}
                        className={"form-control mb-0 "}

                      />
                    </div>
                    <div className="col-6 basic-info-details">
                      <input
                        type="text"
                        maxLength={11}
                        onKeyPress={numberInput}
                        name="secondary_contact_number"
                        placeholder="Number"
                        value={info.secondary_contact_number}
                        onChange={onTextChange}
                        className={
                          "form-control mb-0 " +
                          (errors.secondary_contact_number ? "invalid" : "")
                        }

                      />
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div
                className="row"

              >
                <div className="col-12 col-md-8 col-lg-8 col-xl-8">
                  <div className="question-details">
                    <h5 className="font-semibold">{strings.Selectskills}</h5>
                    {skills.map((value: any, index: number) => (
                      <div key={index} className="custom-check mt-3">
                        <label className="check ">
                          <input
                            onChange={(e) => {
                              addSkill(value.id, e);
                            }}
                            checked={userSkills?.find((v) => v == value.id)}
                            type="checkbox"
                          />
                          <span className="checkmark"></span>
                          {value.skill}
                        </label>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <hr />
            </div>
          </div>
          {/*----------/cards details---------*/}
        </div>
        <div className="col-12 px-0" ref={highlightRef}>
          <div className="row">
            <div className="col-12 col-md-6 col-lg-6 col-xl-6">
              <div className="pt-0 bg-white main-background">
                <h6>
                  {strings.Profileverification}<span className="text-danger">*</span>&nbsp; (
                  {info.user_document.length
                    ? info.user_document[0].status == 0
                      ? <span className="text-danger"><small>{strings.WaitingForApproval}</small></span>
                      : info.user_document[0].status == 1
                      ? strings.Verified
                      : strings.Rejected
                    : <span className="text-danger">{strings.Upload}</span>}
                  )
                </h6>
                <p className="font-12 mb-0">{strings.uploadImageText}</p>
                {/*<div className="form-group">*/}
                {/*  <div className="category-selection charge-select" style={{pointerEvents:isEdit ? 'auto':'none'}}>*/}
                {/*    <label>{strings.passportNumber} {errors?.passport_number ? <small className='text-danger'>*{errors?.passport_number}</small> : null}</label>*/}
                {/*    <input*/}
                {/*        className={`form-control ${errors?.passport_number ? 'invalid' : ''}`}*/}
                {/*        placeholder={strings.EnterHere}*/}
                {/*        id="passport_number"*/}
                {/*        max-length="20"*/}
                {/*        name="passport_number"*/}
                {/*        value={info.passport_number ? info.passport_number : ''}*/}
                {/*        onChange={(e) => {*/}
                {/*          setInfo({ ...info, ['passport_number']: e.target.value })*/}
                {/*          setErrors({})*/}
                {/*        }}*/}
                {/*        disabled={!isEdit}*/}
                {/*    />*/}
                {/*  </div>*/}
                {/*</div>*/}
                {documentLoading === 1 ? <Loader /> : null}
                {documentLoading !== 1 && info.passport_image == "" && (
                  <div
                    className="upload-doc profile-doc"

                  >
                    <div className="file">
                      <div>
                        <div>
                          <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="far"
                            data-icon="arrow-to-top"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 384 512"
                            className="svg-inline--fa fa-arrow-to-top fa-w-12 fa-2x"
                          >
                            <path
                              fill="currentColor"
                              d="M35.5 279.9l148-148.4c4.7-4.7 12.3-4.7 17 0l148 148.4c4.7 4.7 4.7 12.3 0 17l-19.6 19.6c-4.8 4.8-12.5 4.7-17.1-.2L218 219.2V468c0 6.6-5.4 12-12 12h-28c-6.6 0-12-5.4-12-12V219.2l-93.7 97.1c-4.7 4.8-12.4 4.9-17.1.2l-19.6-19.6c-4.8-4.7-4.8-12.3-.1-17zM12 84h360c6.6 0 12-5.4 12-12V44c0-6.6-5.4-12-12-12H12C5.4 32 0 37.4 0 44v28c0 6.6 5.4 12 12 12z"
                            />
                          </svg>
                        </div>
                        {strings.clickToUpload}
                        <br />
                        {strings.idImage}
                        <input
                          accept=".png,.jpg,.jpeg,.doc,.pdf"
                          type="file"
                          className="form-control"
                          placeholder="passport_image"
                          id="passport_image"
                          alt="new"
                          onChange={(e) =>{onFileChange(e, 1)}}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {info.passport_image != "" && (
                  <div className="form-group">
                    <div
                      className="category-selection charge-select"

                    >
                      <label>{strings.UploadedId}</label>
                      <Card>
                        <div className="d-flex align-items-center p-3 justify-content-between p-1 px-2">
                          <p className="mb-0 text-truncate pr-2">
                            {
                              info.passport_image.split("/")[
                                info.passport_image.split("/").length - 1
                              ]
                            }
                          </p>
                          {info.is_verified == 1 ? "" :<a
                                onClick={() => setUserDocument(0)}
                                style={{ fontSize: "16px", color: "white" }}
                              >
                                <svg viewBox="0 0 448 512">
                                  <path
                                    fill="red"
                                    d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"
                                  ></path>
                                </svg>
                          </a> }
                        </div>
                      </Card>
                    </div>
                  </div>
                )}
                <hr />
                {info.passport_image != "" && (
                  <button
                    onClick={() => window.open(info.passport_image, "_blank")}
                    className="btn btn-primary"
                  >
                    {strings.ViewDocument}
                  </button>
                )}
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-6 col-xl-6">
              <div className="pt-0 bg-white main-background">
                <h6>
                  {strings.Liscenseverification}<span className="text-danger">*</span> &nbsp; (
                  {info.user_document.length > 1
                    ? info.user_document[1].status == 0
                      ? <span className="text-danger"><small>{strings.WaitingForApproval}</small></span>
                      : info.user_document[1].status == 1
                      ? strings.Verified
                      : strings.Rejected
                    : <span className="text-danger">{strings.Upload}</span>}
                  )
                </h6>
                <p className="font-12 mb-0">{strings.uploadliscenseText}</p>
                {/* <div className="form-group"> */}
                 {/* <div className="category-selection charge-select" style={{pointerEvents:isEdit ? 'auto':'none'}}>
                   <label>{strings.LiscenseNumber} {errors?.liscense_number ? <small className='text-danger'>*{errors?.liscense_number}</small> : null}</label>
                   <input
                        className={`form-control ${errors?.liscense_number ? 'invalid' : ''}`}
                        placeholder={strings.EnterHere}
                        id="liscense_number"
                        name="liscense_number"
                        value={info.liscense_number ? info.liscense_number : ''}
                        onChange={(e) => {
                          setInfo({ ...info, ['liscense_number']: e.target.value })
                          setErrors({})
                        }}
                        disabled={!isEdit}
                    />
                  </div> */}
                {/* </div> */}
                {documentLoading === 2 ? <Loader /> : null}
                {info.liscense_image == "" && documentLoading !== 2 && (
                  <div
                    className="upload-doc profile-doc"

                  >
                    <div className="file">
                      <div> 
                        <div>
                          <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="far"
                            data-icon="arrow-to-top"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 384 512"
                            className="svg-inline--fa fa-arrow-to-top fa-w-12 fa-2x"
                          >
                            <path
                              fill="currentColor"
                              d="M35.5 279.9l148-148.4c4.7-4.7 12.3-4.7 17 0l148 148.4c4.7 4.7 4.7 12.3 0 17l-19.6 19.6c-4.8 4.8-12.5 4.7-17.1-.2L218 219.2V468c0 6.6-5.4 12-12 12h-28c-6.6 0-12-5.4-12-12V219.2l-93.7 97.1c-4.7 4.8-12.4 4.9-17.1.2l-19.6-19.6c-4.8-4.7-4.8-12.3-.1-17zM12 84h360c6.6 0 12-5.4 12-12V44c0-6.6-5.4-12-12-12H12C5.4 32 0 37.4 0 44v28c0 6.6 5.4 12 12 12z"
                            />
                          </svg>
                        </div>
                        {strings.clickToUpload} 
                        <br />
                        {strings.idImage}
                        <input
                          accept=".png,.jpg,.jpeg,.doc,.pdf"
                          type="file"
                          name="license_image"
                          onChange={(e) => { onFileChange(e, 2)}}
                  
                        />
                      </div>
                    </div>
                  </div>
                )}
                {info.liscense_image != "" && (
                  <div
                    className="form-group"

                  >
                    <div className="category-selection charge-select">
                      <label>{strings.UploadedLicense}</label>
                      
                      <Card>
                        <div className="d-flex align-items-center p-3 justify-content-between p-1 px-2">
                          <p className="mb-0 text-truncate pr-2">
                            {
                              info.liscense_image.split("/")[
                                info.liscense_image.split("/").length - 1
                              ]
                            }
                          </p>
                          {info.is_verified == 1 ? "" :<a
                               onClick={() => setUserDocument(0)}
                               style={{ fontSize: "16px", color: "white" }}
                              >
                               <svg viewBox="0 0 448 512">
                                 <path
                                   fill="red"
                                    d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"
                                  ></path>
                               </svg>
                              </a>
                          }
                        </div>
                      </Card>
                    </div>
                  </div>
                )}
                <hr />
                {info.liscense_image != "" && (
                  <button
                    onClick={() => window.open(info.liscense_image, "_blank")}
                    className="btn btn-primary"
                  >
                    {strings.ViewDocument}
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 pb-3 px-0 d-flex justify-content-end align-items-end">

                <div className="col-auto d-none d-lg-block">
                  <button onClick={modalAction} className="btn btn-primary btn-lg">
                                    {strings.Save}
                                  </button>
                </div>
              </div>
      </div>



      <Modal
        show={openModal}
        dialogClassName="modal-dialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <div role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{strings.Confirmation}</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => promiseInfo.current.reject("rejected")}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div>
                <h6>{strings.AreYouSureYouWantToSaveTheprofileDetails}</h6>
              </div>
              <hr />
              <div className="row">
                <div className="col-6 col-md-6 col-lg-6 col-xl-6 my-auto">
                  <div className="cancel-details" data-dismiss="modal">
                    <a onClick={() => promiseInfo.current.reject("rejected")}>
                      {strings.Cancel}
                    </a>
                  </div>
                </div>
                <div className="col-6 col-md-6 col-lg-6 col-xl-6 alignment">
                  <button
                    onClick={() => {
                      promiseInfo.current.resolve(true);
                    }}
                    className="btn btn-primary px-3 py-2 mr-2"
                  >
                    {strings.Yes}
                  </button>
                  <button
                    onClick={() => promiseInfo.current.reject("rejected")}
                    className="btn btn-primary px-3 py-2"
                  >
                    {strings.No}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};
export default BasicInfo;
