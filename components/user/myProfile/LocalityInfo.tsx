import setDate from "date-fns/esm/fp/setDate/index.js";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import API from "../../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import { errorOptions, successOptions } from "../../../public/appData/AppData";
import {strings} from "../../../public/lang/Strings";

const initialState = {
  description: "",
  live_in_house: 0,
  non_smoking_household: 0,
  no_children_present: 0,
  fenced_yard: 0,
  dog_other_pets: 0,
  accept_only_on_client_at_time: 0,
  does_not_own_a_cat: 0,
  does_not_own_a_dog: 0,
  does_not_own_caged_pets: 0,
  dogs_on_bed: 0,
  dogs_on_furniture: 0,
  no_children_5_year_old: 0,
  no_children_12_year_old: 0,
};

interface I_PROPS {
  locality: any;
  callApi: (data: any) => void;
}

const api = new API();
const LocalityInfo: React.FC<I_PROPS> = (props) => {
  const [data, setData] = useState<any>(
    props.locality != null ? props.locality : initialState
  );
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);

  const onChecked = (e) => {
    setData({ ...data, [e.target.name]: e.target.checked == true ? 1 : 0 });
  };

  useEffect(() => {
    console.log({locality: props.locality, data: data})
    if (props.locality) {
      setData(props.locality);
    }
  }, [props.locality]);

  const saveLocality = () => {
    props.callApi(data);
  };

  return (
    <div className="px-0">
      <div className="basic-details mt-0">
        <h5 className="font-semibold">{strings.Extrainfoaboutlocality}</h5>
      </div>
      <div className="row">
        <div className="col-12 col-md-auto">
          <div className="locality-content row">
            <div className='col-md-4'>
              <h6>{strings.HousingConditions}</h6>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.non_smoking_household == 1}
                      name="non_smoking_household"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.NonSmokingHousehold}
                </label>
              </div>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      checked={data.live_in_house == 1}
                      type="checkbox"
                      name="live_in_house"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.LivesInAHouse}
                </label>
              </div>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.fenced_yard == 1}
                      name="fenced_yard"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.HasAFencedYard}
                </label>
              </div>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.dogs_on_bed == 1}
                      name="dogs_on_bed"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.DogsAllowedOnBed}
                </label>
              </div>
              <div className="custom-check mb-0">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.dogs_on_furniture == 1}
                      name="dogs_on_furniture"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.DogsAllowedOnFurniture}
                </label>
              </div>
            </div>
            <div className='col-md-4 my-2 my-md-0'>
              <h6>{strings.PetsInTheHome}</h6>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.dog_other_pets == 1}
                      name="dog_other_pets"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.HasPet}
                </label>
              </div>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.accept_only_on_client_at_time == 1}
                      name="accept_only_on_client_at_time"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.AcceptOnlyOneClientAtATime}
                </label>
              </div>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.does_not_own_a_cat == 1}
                      name="does_not_own_a_cat"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.DoesNotOwnACat}
                </label>
              </div>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.does_not_own_a_dog == 1}
                      name="does_not_own_a_dog"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.DoesNotOwnADog}
                </label>
              </div>
              <div className="custom-check mb-0">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.does_not_own_caged_pets == 1}
                      name="does_not_own_caged_pets"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.DoesNotOwnCagedPets}
                </label>
              </div>
            </div>
            <div className='col-md-4'>
              <h6>Children in the home</h6>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.no_children_present == 1}
                      name="no_children_present"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.NoChildrenPresent}
                </label>
              </div>
              <div className="custom-check mb-2">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.no_children_5_year_old == 1}
                      name="no_children_5_year_old"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.NoChildren05YearsOld}
                </label>
              </div>
              <div className="custom-check mb-0">
                <label className="check ">
                  <input
                      type="checkbox"
                      checked={data.no_children_12_year_old == 1}
                      name="no_children_12_year_old"
                      onChange={onChecked}
                  />
                  <span className="checkmark" />
                  {strings.NoChildren612YearsOld}
                </label>
              </div>
            </div>
            <div className='col-12 mt-4' >
              <div className="col-12 px-0">
              <textarea
                  className="form-control"
                  placeholder="Write your details"
                  rows={4}
                  value={data.description ? data.description : ""}
                  onChange={(e) => {
                    setData({ ...data, description: e.target.value });
                  }}
                  defaultValue={""}
              />
              </div>
            </div>
          </div>
          <div className="mt-3">
          </div>
        </div>

        <div className="col-12 d-none d-lg-block">
          <button onClick={saveLocality} className="btn btn-primary">
            {strings.Save}
          </button>
        </div>
      </div>
      <div className="d-block d-md-none book-fixed">
        <div className="col-12">
          <button onClick={saveLocality} className="btn btn-primary px-3">
            {strings.Save}
          </button>
        </div>
      </div>
    </div>
  );
};

export default LocalityInfo;
