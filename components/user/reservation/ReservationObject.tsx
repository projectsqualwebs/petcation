import React, {useEffect, useState} from "react";
import I_SITTER_RESERVATION from "../../../models/sitterReservations.interface";
import {strings} from "../../../public/lang/Strings";
import ContactView from "./ContactView";
import RequestView from "./RequestView";
import {AxiosResponse} from "axios";
import Res from "../../../models/response.interface";
import {I_CHAT_BOOKING} from "../../../models/chat.interface";
import {useSnackbar} from 'react-simple-snackbar';
import API from "../../../api/Api";
import TransportFeeModal from "../../common/TransporFeeModal";
import {useRouter} from "next/router";
import RequestCancelModal from "../../common/RequestCancelModal";
import OwnerRating from "../../common/OwnerRating";
import RateSitter from "../../common/RateSitter";
import RequestViewModal from "../../common/RequestViewModal";
import RequestRejectModal from "../../common/RequestRejectModal";
import {getCurrencySign} from "../../../api/Constants";
import {errorOptions, successOptions} from "../../../public/appData/AppData";
import moment from "moment";
import ChatBookingCountdown from "../../chat/ChatBookingCountdown";

type reservationObject = {
    image: string;
    from: string;
    to: string;
    paymentStatus: string;
    total: string;
    name: string;
    clientName: string;
    address: string;
};

interface IProps {
    data: I_SITTER_RESERVATION;
    request: boolean;
    type: Number;
    asSitter: boolean;
    getReservations: () => void;
    setLoader: (state) => void;
}

let api = new API();
/**
 *
 * @param data contains reservation data
 * @param request contains boolean value
 * @param asSitter contains boolean value to differentiate between siiter and pet owner
 * @param changeRequestStatus is a function for sitter end
 * @param withdrawRequest is a funtion to withdraw request at user end.
 * @function contains view of a particular reservation object.
 */
const ReservationObject: React.FC<IProps> = ({
                                                 getReservations,
                                                 data,
                                                 request,
                                                 asSitter,
                                                 type,
                                                 setLoader,
                                             }: IProps) => {
    const key = asSitter ? "user" : "sitter";

    const [showModal, setShowModal] = useState(false);
    const [bookingId, setBookingId] = useState<any>();
    const [cancelModal, setCancelModal] = useState(false);
    const [showOwnerRating, setShowOwnerRating] = useState<boolean>(false);
    const [showSitterRating, setShowSitterRating] = useState<boolean>(false);
    const [viewModal, setViewModal] = useState<boolean>(false);
    const [rejectModal, setRejectModal] = useState<boolean>(false);
    const [openError, closeError] = useSnackbar(errorOptions);
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [loading, setLoading] = useState(false);

    const [openSnackbar] = useSnackbar();
    const router = useRouter()

    /**
     * @function to call API to Add transport fee
     */
    const handleAddCharge = (amount) => {
        if (!amount) {
            openSnackbar(strings.EnterAmount)
        } else {
            setLoading(true)
            api
                .addTransportChargeRequest({request_id: data.id, amount: amount})
                .then((res: AxiosResponse<Res<I_CHAT_BOOKING[]>>) => {
                    changeRequestStatus(data.id, '', 1);
                    setShowModal(false);
                    setLoading(false);
                    openSuccess(res.data.message)
                })
                .catch((error) => setLoading(false));
        }
    };

    /**
     * @function to accept request and it will show transport fee modal
     */
    const onAccept = () => {
        if (data?.need_sitter_pickup === 1) {
            setShowModal(true);
        } else {
            changeRequestStatus(data.id, '', 1)
        }
    };

    /**
     * @function to reject request from the sitter end
     */
    const onReject = () => {
        setRejectModal(true)
    };

    /**
     * @function to move next step for payment at the pet owners end.
     */
    const onPayment = () => {
        router.push({
            pathname: "/payment",
            query: {id: data.id}
        });
    };

    /**
     * funtion to withdraw the request.
     * @param id  to provide identity of a perticular request.
     * @param data  contains parameter for which we withdraw the request.
     * @success
     *    success -> withdraw booking.
     *    faliure -> booking will exist.
     */
    const withdrawRequest = (id: any, data: any) => {
        setLoading(true);
        api
            .cancelRequest(id, {
                pets: data.userPets,
                additional_services: data.additionalServices,
                reason: data?.reason
            })
            .then((res) => {
                setLoading(false);
                setCancelModal(false);
                setLoader(true);
                getReservations();
                openSuccess(res.data.message)
            })
            .catch((error) => setLoading(false))
    };

    /**
     *
     * @param id contains request id
     * @param status contains status key to accept reject
     *        status == 1 : to accept request
     *        status == 2 : to reject request
     * @function
     *      @success will change the request status will accept or reject the request at the sitter end.
     *      @failure show error to user which notify them that what mistake they made and will reject the request.
     *
     */
    const changeRequestStatus = (id, reason, status) => {
        setLoader(true);
        setLoading(true);
        let data = JSON.stringify({
            request_id: id,
            reason: reason,
            status: status,
        });
        api
            .changeRequestStatus(data)
            .then((res) => {
                setRejectModal(false);
                setLoading(false)
                openSuccess(res.data.message);
                getReservations();
            })
            .catch((error) => {
                setLoader(false);
                setLoading(false);
                if (error.response && error.response.data) {
                    openError(error.response.data.message)
                }
            });
    };

    const has12HoursPassed = (updatedAt) => {
        const updatedAtMoment = moment(updatedAt);
        const now = moment();
        const duration = moment.duration(now.diff(updatedAtMoment));
        return duration.asHours() >= 12;
    };

    return (
        <>
            <div className="booking-request-details">
                <div className="booking-bg">
                    <div className="image1">
                        <img
                            src={asSitter ? (data.user ? data.user.profile_picture : '') : (data.sitter ? data.sitter.profile_picture : '')}/>
                        <small onClick={() => setViewModal(true)}
                               className="text-primary pointer">{strings.viewDetails}</small>
                    </div>
                    <div className="row">
                        <div className="col-xl-10 offset-xl-2 pd-left">
                            <div className="row justify-content-between">
                                <div className="col-12 col-md-8 col-lg-auto pd-left">
                                    <div className="row justify-content-between">
                                        <div className="col-auto from-details">
                                            <p className="font-10 mb-0">{strings.From}</p>
                                            <p className="font-12 font-semibold mb-0">
                                                {moment(new Date(data.drop_of_date)).format("MM/DD/YYYY")}
                                            </p>
                                        </div>
                                        <div className="col-auto to-details">
                                            <p className="font-10 mb-0">{strings.To}</p>
                                            <p className="font-12 font-semibold mb-0">
                                                {moment(new Date(data.pickup_up_date)).format("MM/DD/YYYY")}
                                            </p>
                                        </div>
                                    </div>
                                </div> 
                                
                                <div className="col-12 col-md-8 col-lg-auto alignment pr-4">
                                    <div className="row justify-content-between">
                                        <div className="col-auto total-details pr-4">
                                            <p className="font-10 mb-0">{strings.status}</p>
                                            {type !== 5 ?
                                                <p className="font-12 font-semibold mb-0">
                                                    {
                                                        data.status == 0 ? strings.Pending
                                                            : (data.status == 1 && data.payment_status === 0) ? request ? strings.acceptedBySitter : strings.paymentPending
                                                            : (data.status == 1 && data.payment_status === 1) ? strings.paymentDone
                                                                : (data.status == 2 && data.payment_status === 0) ? strings.Rejected
                                                                    : (data.status == 2 && data.payment_status === 1) ? strings.Cancelled
                                                                        : data.status == 3 ? strings.Paid : strings.Completed
                                                    }
                                                </p>
                                                :
                                                <p className="font-12 font-semibold mb-0">
                                                    {
                                                        data.dispute?.status == 0 ? strings.Initiated
                                                            : data.dispute?.status == 1 ? strings.InProgress
                                                            : strings.Completed
                                                    }
                                                </p>
                                            }
                                        </div>
                                        <div className="col-auto total-details">
                                            <p className="font-10 mb-0">{strings.Total}</p>
                                            <p className="font-12 font-semibold mb-0">
                                                {getCurrencySign() + " " + data.total_paid_amount}
                                            </p>
                                        </div>
                                        {data.status == 2 && data.payment_status === 1 &&
                                        <div className="col-auto total-details">
                                            <p className="font-10 mb-0">Refund</p>
                                            <p className="font-12 font-semibold mb-0">
                                                {getCurrencySign() + " " + data.refund_amount}
                                            </p>
                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xl-10 offset-xl-2 pd-left mt-2">
                        <div className="row align-items-center">
                            <div className="col-xl-8 pl-2">
                                <div className="booking-title">
                                    {asSitter ? <h6 className="font-14 mb-0">
                                        {(data.user ? data?.pets?.map((val, index) => data.pets?.length == index + 1 ? val.pet_name : val.pet_name + ', ') : '') + " - " + data.service.name + " | " + strings.Client_C + " " + (data.user ? (data.user.firstname + ' ' + data.user.lastname) : '')}
                                    </h6> : <h6 className="font-14 mb-0">
                                        {(data.sitter ? data?.pets?.map((val, index) => data.pets?.length == index + 1 ? val.pet_name : val.pet_name + ', ') : '') + " - " + data.service.name + " | " + strings.Sitter_C + " " + (data.sitter ? (data.sitter.firstname + ' ' + data.sitter.lastname) : '')}
                                    </h6>}
                                    {asSitter && <div className="mb-0 mt-1 font-10">
                                        {data.user ? (
                                            data.user?.address?.address ? <p className="font-10 mb-0">
                                                {`${
                                                    strings.Address +
                                                    ":" +
                                                    (data.user.address ? data.user.address.address : '') +
                                                    " " +
                                                    (data.user.address ? data.user.address?.city : '') +
                                                    " " +
                                                    (data.user.address ? data.user.address?.postcode : '')
                                                }`}
                                            </p> : null
                                        ) : null}
                                       
                                        {data.status != 2 &&  !has12HoursPassed(data.created_at) &&
                                        <p className="font-10 mb-0">
                                            <ChatBookingCountdown time={data?.created_at}/> 
                                        </p>
                                        }
                                        {(type !== 5 && data?.status == 2) ?
                                            <p className="font-10 mb-0"><b>Reason:</b> {data?.reason}</p> : null}
                                        {type === 5 ?
                                            <p className="font-10 mb-0"><b>Reason:</b> {data?.dispute?.reason}
                                            </p> : null}
                                    </div>}
                                    {!asSitter && <div className="mb-0 font-10">
                                        {data.sitter ? (data.sitter?.address?.address ? <p className="font-10">
                                                {`${
                                                    strings.Address +
                                                    ": " +
                                                    (data.sitter ? data.sitter.address.address : '') +
                                                    " " +
                                                    (data.sitter ? data.sitter.address?.city : '') +
                                                    " " +
                                                    (data.sitter ? data.sitter.address?.postcode : '')
                                                }`}
                                            </p> : null
                                        ) : null}
                                        {data.status != 2 && !has12HoursPassed(data.created_at) &&
                                        <p className="font-10 mb-0">
                                            <ChatBookingCountdown time={data?.created_at}/>
                                        </p>
                                        }
                                        {(type !== 5 && data?.status == 2) ?
                                            <p className="font-10 mb-0"><b>{strings.Reason_C}</b> {data?.reason}
                                            </p> : null}
                                        {type === 5 ?
                                            <p className="font-10 mb-0"><b>Reason:</b> {data?.dispute?.reason}
                                            </p> : null}
                                    </div>}
                                </div>
                            </div>

                            {type !== 5 &&
                            <>
                                {has12HoursPassed(data.created_at) ?
                                    <div className="col-xl-4 d-flex justify-content-center ">
                                        <button className=" no-pointer btn btn-primary btn1 btn-accept btn-reject mr-2">
                                            <span className="d-none d-md-block">{strings.TimeOut}</span>
                                        </button>
                                    </div>
                                    :
                                    <div className="col-xl-4  my-auto">
                                        {
                                            data.status == 0 ? request ?
                                                <RequestView withdrawRequest={() => setCancelModal(true)}
                                                             onPayment={onPayment}
                                                             request={request} data={data} onAccept={onAccept}
                                                             onReject={onReject} asSitter={asSitter}/> :
                                                <RequestView onPayment={onPayment} request={request} data={data}
                                                             onAccept={onAccept} onReject={onReject} asSitter={asSitter}
                                                             withdrawRequest={() => setCancelModal(true)}/>
                                                : data.status == 1 ? request ?
                                                <RequestView asSitter={asSitter}
                                                             withdrawRequest={() => setCancelModal(true)}
                                                             onPayment={onPayment} request={request} data={data}
                                                             onAccept={onAccept}
                                                             onReject={onReject}/> : ((data.payment_status == 0 && asSitter) ? null :
                                                    <RequestView asSitter={asSitter}
                                                                 withdrawRequest={() => setCancelModal(true)}
                                                                 onPayment={onPayment} request={request} data={data}
                                                                 onAccept={onAccept} onReject={onReject}/>)
                                                : data.status == 3 ? <ContactView data={data} asSitter={asSitter}
                                                                                  withdrawRequest={() => setCancelModal(true)}/> : data.status == 4 ?
                                                    <div className="d-flex justify-content-end pr-1 accept-details">
                                                        <button className="btn btn-primary btn1"
                                                                onClick={() => asSitter ? setShowOwnerRating(true) : setShowSitterRating(true)}>{asSitter ? data?.owner_reviews?.length ? strings.Edit : strings.Provide : data?.sitter_reviews?.length ? strings.Edit : strings.Provide}&nbsp;{strings.Review}</button>
                                                    </div> : null
                                        }
                                    </div>}
                            </>
                            }
                        </div>
                    </div>
                </div>
            </div>
            <OwnerRating bookingId={data?.id} userId={data?.booking_user_id} getThreadBooking={() => {
                getReservations()
            }} onHide={() => setShowOwnerRating(false)} showModal={showOwnerRating} myReview={data?.owner_reviews}/>
            <RateSitter bookingId={data?.id} sitterId={data?.booked_sitter_id} showModal={showSitterRating}
                        hideModal={() => setShowSitterRating(false)} getThreadBooking={() => getReservations()}
                        myReview={data?.sitter_reviews}/>
            <RequestCancelModal showModal={cancelModal} hideModal={() => setCancelModal(false)} request={data}
                                paid={false} withdrawRequest={(id, data) => withdrawRequest(id, data)}
                                loading={loading}/>
            <RequestRejectModal showModal={rejectModal} hideModal={() => setRejectModal(false)} request={data}
                                withdrawRequest={(id, reason) => changeRequestStatus(data.id, reason, 2)}
                                loading={loading}/>
            <RequestViewModal showModal={viewModal} hideModal={() => setViewModal(false)} request={data}
                              asSitter={asSitter}/>
            <TransportFeeModal bookingServiceId={data.service.id} bookingService={data.service.name} id={data.id}
                               showModal={showModal} hideModal={() => setShowModal(false)}
                               handleAddCharge={(amount) => handleAddCharge(amount)} loading={loading}/>
        </>
    );
};

export default ReservationObject;
