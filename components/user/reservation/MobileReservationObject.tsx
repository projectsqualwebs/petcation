import React, { useEffect, useState } from "react";
import { strings } from "../../../public/lang/Strings";
import ContactView from "./ContactView";
import I_SITTER_RESERVATION from "../../../models/sitterReservations.interface";
import { getCurrencySign } from "../../../api/Constants";
import API from "../../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import { errorOptions, successOptions } from "../../../public/appData/AppData";
import RequestCancelModal from "../../common/RequestCancelModal";
import OwnerRating from "../../common/OwnerRating";
import RateSitter from "../../common/RateSitter";
import RequestViewModal from "../../common/RequestViewModal";
import RequestRejectModal from "../../common/RequestRejectModal";
import TransportFeeModal from "../../common/TransporFeeModal";
import { useRouter } from "next/router";

import RequestView from "./RequestView";
import {I_CHAT_BOOKING} from "../../../models/chat.interface";
import {AxiosResponse} from "axios";
import Res from "../../../models/response.interface";

interface IProps {
  data: I_SITTER_RESERVATION;
  request: boolean;
  asSitter: boolean;
  getReservations: () => void;
  setLoader: (state) => void;
}
let api = new API();
const MobileReservationObject: React.FC<IProps> = ({
  getReservations,
  data,
  request,
  asSitter,
  setLoader,
}: IProps) => {
  const [showModal, setShowModal] = useState(false);
  const [bookingId, setBookingId] = useState<any>();
  const [cancelModal, setCancelModal] = useState(false);
  const [showOwnerRating, setShowOwnerRating] = useState<boolean>(false);
  const [showSitterRating, setShowSitterRating] = useState<boolean>(false);
  const [viewModal, setViewModal] = useState<boolean>(false);
  const [rejectModal, setRejectModal] = useState<boolean>(false);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [loading, setLoading] = useState(false);
  const [openSnackbar] = useSnackbar();
  const router = useRouter();
  const onPayment = () => {
    router.push({
      pathname: "/payment",
      query: { id: data.id },
    });
  };

  /**
   * @function to call API to Add transport fee
   */
  const handleAddCharge = (amount) => {
    if(!amount){
      openSnackbar(strings.EnterAmount)
    }else {
      setLoading(true)
      api
          .addTransportChargeRequest({request_id: data.id, amount: amount})
          .then((res: AxiosResponse<Res<I_CHAT_BOOKING[]>>) => {
            changeRequestStatus(data.id,'', 1);
            setShowModal(false);
            setLoading(false);
            openSuccess(res.data.message)
          })
          .catch((error) => setLoading(false));
    }
  }

  const onAccept = () => {
    if (data?.need_sitter_pickup === 1) {
      setShowModal(true);
    } else {
      changeRequestStatus(data.id, "", 1);
    }
  };
  const onReject = () => {
    setRejectModal(true);
  };
  const withdrawRequest = (id: any, data: any) => {
    setLoading(true);
    api
      .cancelRequest(id, {
        pets: data.userPets,
        additional_services: data.additionalServices,
        reason: data?.reason,
      })
      .then((res) => {
        setLoading(false);
        setCancelModal(false);
        setLoader(true);
        getReservations();
        openSuccess(res.data.message);
      })
      .catch((error) => setLoading(false));
  };
  const changeRequestStatus = (id, reason, status) => {
    setLoader(true);
    setLoading(true);
    let data = JSON.stringify({
      request_id: id,
      reason: reason,
      status: status,
    });
    api
      .changeRequestStatus(data)
      .then((res) => {
        setRejectModal(false);
        setLoading(false);
        openSuccess(res.data.message);
        getReservations();
      })
      .catch((error) => {
        setLoader(false);
        setLoading(false);
        if (error.response && error.response.data) {
          openError(error.response.data.message);
        }
      });
  };
  return (
    <>
      <div className="col-12">
        <div
          className="booking-request-details request-padding"
          style={{ borderRadius: 0 }}>
          <div className="col-12 p-0">
            <div className="row align-items-center">
              <div className="col-auto pr-0">
                <div className="col-12 p-0 search-sitter-img">
                  <img
                    className="img-fluid"
                    src={
                      asSitter
                        ? data.user
                          ? data.user.profile_picture
                          : ""
                        : data.sitter
                        ? data.sitter.profile_picture
                        : ""
                    }
                  />
                </div>
              </div>
              <div className="col booking-title mt-2 pl-2">
                <h6 className="font-14 mb-0 font-medium">
                  {asSitter
                    ? (data.sitter
                        ? data.sitter.firstname + " " + data.sitter.lastname
                        : "") +
                      " | " +
                      strings.Client +
                      ":" +
                      (data.user
                        ? data.user.firstname + " " + data.user.lastname
                        : "")
                    : (data.user
                        ? data.user.firstname + " " + data.user.lastname
                        : "") +
                      " | " +
                      strings.Client +
                      ":" +
                      (data.sitter
                        ? data.sitter.firstname + " " + data.sitter.lastname
                        : "")}
                </h6>
                {/* {console.log("data.sitter.address",data.sitter.address)} */}
                <p className="font-10 mb-0">
                  {strings.Address +
                    ":" +
                    (asSitter
                      ? data.user
                        ? data.user.address
                          ? data.user.address.address
                          : ""
                        : ""
                      : data.sitter
                      ? data.sitter.address
                        ? data.sitter.address.address
                        : ""
                      : "")}
                </p>
              </div>
            </div>
          </div>
          <hr className="my-2" />
          <div className="d-flex justify-content-between mb-2">
            <div className="from-details">
              <p className="font-10 mb-0">{strings.From}</p>
              <p className="font-12 font-semibold mb-0">
                {data.drop_of_date + " " + data.drop_of_time_from}
              </p>
            </div>
            <div className="to-details">
              <p className="font-10 mb-0">{strings.To}</p>
              <p className="font-12 font-semibold mb-0">
                {data.pickup_up_date + " " + data.pickup_up_time_to}
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="total-details">
                <p className="font-10 mb-0">{strings.Total}</p>
                <p className="font-12 font-semibold mb-0">
                  {getCurrencySign() + data.total_paid_amount}{" "}
                  <span className="paid">{strings.Paid}</span>
                </p>
              </div>
            </div>
            <div className="col-12 mt-2">
              <div className="row">
                <div className="col">
                  <div className="d-flex align-items-center pr-1 accept-details">
                    {/*
                  <button className="btn btn-primary btn1 btn-accept"
                          data-toggle="modal" data-target="#accept">{strings.Accept}
                  </button>
                  <button className="btn btn-primary btn1 btn-accept btn-reject mr-2" data-toggle="modal"
                          data-target="#reject">{strings.Reject}
                  </button>
                */}
                    {data.status == 0 ? (
                      request ? (
                        <RequestView
                          withdrawRequest={() => setCancelModal(true)}
                          onPayment={onPayment}
                          request={request}
                          data={data}
                          onAccept={onAccept}
                          onReject={onReject}
                          asSitter={asSitter}
                        />
                      ) : (
                        <RequestView
                          onPayment={onPayment}
                          request={request}
                          data={data}
                          onAccept={onAccept}
                          onReject={onReject}
                          asSitter={asSitter}
                          withdrawRequest={() => setCancelModal(true)}
                        />
                      )
                    ) : data.status == 1 ? (
                      request ? (
                        <RequestView
                          asSitter={asSitter}
                          withdrawRequest={() => setCancelModal(true)}
                          onPayment={onPayment}
                          request={request}
                          data={data}
                          onAccept={onAccept}
                          onReject={onReject}
                        />
                      ) : data.payment_status == 0 && asSitter ? null : (
                        <RequestView
                          asSitter={asSitter}
                          withdrawRequest={() => setCancelModal(true)}
                          onPayment={onPayment}
                          request={request}
                          data={data}
                          onAccept={onAccept}
                          onReject={onReject}
                        />
                      )
                    ) : data.status == 3 ? (
                      <ContactView
                        data={data}
                        asSitter={asSitter}
                        withdrawRequest={() => setCancelModal(true)}
                      />
                    ) : data.status == 4 ? (
                      <div className="d-flex justify-content-end pr-1 accept-details">
                        <button
                          className="btn btn-primary btn1"
                          onClick={() =>
                            asSitter
                              ? setShowOwnerRating(true)
                              : setShowSitterRating(true)
                          }>
                          {asSitter
                            ? data?.owner_reviews?.length
                              ? strings.Edit
                              : strings.Provide
                            : data?.sitter_reviews?.length
                            ? strings.Edit
                            : strings.Provide}
                          &nbsp;{strings.Review}
                        </button>
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="col-auto">
                  <div className="d-flex justify-content-end vector-icon">
                    {/**
                    <div className="ellipse">
                      <a href="http://159.65.142.31/petcation-design/chat-screen.html">
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="far"
                          data-icon="comment-alt-lines"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                          className="svg-inline--fa fa-comment-alt-lines fa-w-16 fa-2x">
                          <path
                            fill="currentColor"
                            d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 7.1 5.8 12 12 12 2.4 0 4.9-.7 7.1-2.4L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64zm16 352c0 8.8-7.2 16-16 16H288l-12.8 9.6L208 428v-60H64c-8.8 0-16-7.2-16-16V64c0-8.8 7.2-16 16-16h384c8.8 0 16 7.2 16 16v288zm-96-216H144c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16zm-96 96H144c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h128c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16z"
                            className=""></path>
                        </svg>
                      </a>
                    </div>
                    */}
                    {/**
                    <div
                      className="ellipse"
                      data-toggle="modal"
                      data-target="#confirm">
                      <svg
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="far"
                        data-icon="exclamation-triangle"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 576 512"
                        className="">
                        <path
                          fill="currentColor"
                          d="M248.747 204.705l6.588 112c.373 6.343 5.626 11.295 11.979 11.295h41.37a12 12 0 0 0 11.979-11.295l6.588-112c.405-6.893-5.075-12.705-11.979-12.705h-54.547c-6.903 0-12.383 5.812-11.978 12.705zM330 384c0 23.196-18.804 42-42 42s-42-18.804-42-42 18.804-42 42-42 42 18.804 42 42zm-.423-360.015c-18.433-31.951-64.687-32.009-83.154 0L6.477 440.013C-11.945 471.946 11.118 512 48.054 512H527.94c36.865 0 60.035-39.993 41.577-71.987L329.577 23.985zM53.191 455.002L282.803 57.008c2.309-4.002 8.085-4.002 10.394 0l229.612 397.993c2.308 4-.579 8.998-5.197 8.998H58.388c-4.617.001-7.504-4.997-5.197-8.997z"
                          className=""></path>
                      </svg>
                    </div>
                    */}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6 my-auto d-none">
              <div className="d-flex justify-content-end vector-icon">
                {data.status === 0 && (
                  <div>
                    <button
                      className="btn btn-primary btn1 btn-accept btn-reject"
                      data-toggle="modal"
                      data-target="#reject"
                      // onClick={()=> changeRequestStatus(data.id, 2)}
                    >
                      {strings.Reject}
                    </button>
                    <button
                      className="btn btn-primary btn1 btn-accept"
                      data-toggle="modal"
                      data-target="#accept"
                      // onClick={()=>changeRequestStatus(data.id, 1)}
                    >
                      {strings.Accept}
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <OwnerRating
        bookingId={data?.id}
        userId={data?.booking_user_id}
        getThreadBooking={() => {
          getReservations();
        }}
        onHide={() => setShowOwnerRating(false)}
        showModal={showOwnerRating}
        myReview={data?.owner_reviews}
      />
      <RateSitter
        bookingId={data?.id}
        sitterId={data?.booked_sitter_id}
        showModal={showSitterRating}
        hideModal={() => setShowSitterRating(false)}
        getThreadBooking={() => getReservations()}
        myReview={data?.sitter_reviews}
      />
      <RequestCancelModal
        showModal={cancelModal}
        hideModal={() => setCancelModal(false)}
        request={data}
        paid={false}
        withdrawRequest={(id, data) => withdrawRequest(id, data)}
        loading={loading}
      />
      <RequestRejectModal
        showModal={rejectModal}
        hideModal={() => setRejectModal(false)}
        request={data}
        withdrawRequest={(id, reason) =>
          changeRequestStatus(data.id, reason, 2)
        }
        loading={loading}
      />
      <RequestViewModal
        showModal={viewModal}
        hideModal={() => setViewModal(false)}
        request={data}
        asSitter={asSitter}
      />
      <TransportFeeModal
        bookingServiceId={data.service.id}
        bookingService={data.service.name}
        id={data.id}
        showModal={showModal}
        hideModal={() => setShowModal(false)}
        handleAddCharge={(amount) => handleAddCharge(amount)}
        loading={loading}
      />
    </>
  );
};

export default MobileReservationObject;
