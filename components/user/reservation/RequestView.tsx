import React, {useState} from "react";
import { strings } from "../../../public/lang/Strings";
import RateSitter from "../../common/RateSitter";
import {useRouter} from "next/router";

const RequestView = ({ onAccept, onReject, data, request, onPayment, asSitter, withdrawRequest }: any) => {
  //rating modal
  const [showRatingModal, setShowRatingModal] = useState<boolean>(false);
  const router = useRouter();

  let status = data.status;
  let paymentDone =  data.paymentDone;

  return (
    <div className="d-flex justify-content-end pr-1 accept-details">
      {status===0 && asSitter && <div>
        <button
          className="btn btn-primary btn1 btn-accept btn-reject mr-2"
          data-toggle="modal"
          data-target="#reject"
          onClick={onReject}
        >
          {strings.Reject}
        </button>
        <button
          className="btn btn-primary btn1 btn-accept mr-2"
          data-toggle="modal"
          data-target="#accept"
          onClick={onAccept}
        >
          {strings.Accept}
        </button>
      </div> }
      {status === 0 && !asSitter && <div>
        <button
            className="btn btn-primary btn1 btn-accept mr-2"
            data-toggle="modal"
            data-target="#accept"
            onClick={withdrawRequest}
        >
          {strings.Withdraw}
        </button>
      </div>}
      {
        status === 1 && !request && data.payment_status===0  && <button
            className="btn btn-primary btn1 btn-accept mr-2"
            data-toggle="modal"
            onClick={withdrawRequest}
        >
          {strings.Withdraw}
        </button>
      }
      {
        status === 1 && !request && data.payment_status===0  && <button
            className="btn btn-primary btn1 btn-accept mr-2"
            data-toggle="modal"
            data-target="#btn btn-primary btn1 btn-accept"
            onClick={onPayment}
        >
          {strings.pay}
        </button>
      }
      {
        status === 4 && !asSitter &&  <button
            className="btn btn-primary btn1 btn-danger mr-2"
            data-toggle="modal"
            data-target="#rejected"
            onClick={()=>setShowRatingModal(true)}
        >
          {strings.GiveReview}
        </button>
      }
      {/*<RateSitter id={data.id} showModal={showRatingModal} hideModal={() => setShowRatingModal(false)} />*/}
    </div>
  );
};
export default RequestView;
