import React, { useEffect, useState } from 'react';
import { Modal } from "react-bootstrap";
import { numberInput } from "../../utils/Helper";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import { D_TRANSPORTATION, D_TRANSPORTATION_PREFERENCE } from "../../public/appData/StaticData";
import ActionLoader from "./ActionLoader";

let api = new API();

export default function ViewNotesModal({notes, pet , showModal, closeModal, loading, reFetchData}) {
    const [petNotes, setPetNotes] = useState<any>([]);
    const [error, setError] = useState({
        message: ""
    });

    useEffect(()=>{
        if(!!notes?.length && pet?.id) {
            const filteredNotes = notes.filter(val => val.pet_id === pet.id);
            setPetNotes(filteredNotes)
        }
    },[notes, pet]);

    return (
        <Modal
            show={showModal}
            id="settings"
            className="modal-child"
            aria-labelledby="settings"
            tabIndex="-1"
            scrollable
        >
            <Modal.Header className="p-3">
                <Modal.Title as="h5" className="fw-medium">
                    {strings.AddedNotes}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="p-3">
                <div className="col-12 form-details row">
                    {!!petNotes.length &&
                        petNotes.map(note =>{
                            return (
                                <div className="card col-12 p-1 mb-2">
                                    <p className="p-2 mb-0">{note.message}</p>
                                </div>
                            )
                        })
                    }
                    {!petNotes.length &&
                            <div className="d-flex col-12 p-3 align-items-center justify-content-center">
                                <p className="p-2 mb-0">No notes found.</p>
                            </div>
                    }
                </div>
                <hr />
                <div className="d-flex justify-content-between flex-row">
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={()=>{
                            closeModal();
                        }}
                        className="btn float-end"
                    >
                        {strings.Cancel}
                    </button>
                </div>
            </Modal.Body>
        </Modal>
    )
}
