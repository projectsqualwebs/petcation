import React, {useContext, useEffect} from "react";
import {useState} from "react";
import {Modal} from "react-bootstrap";
import {sitterSorting} from "../../public/appData/AppData";
import AppContext from "../../utils/AppContext";
import {useRouter} from "next/router";
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import {strings} from "../../public/lang/Strings";

interface I_PROPS {
    data: any;
    showModal: boolean;
    hideModal: () => void;
}

interface I_OTHER_FILTERS {
    transportation: number;
    closest_sitter: any;
    has_house: number;
    has_fenced_yard: number;
    dogs_allowed_on_furniture: number;
    dogs_allowed_on_bed: number;
    non_smoking_home: number;
    does_not_own_a_dog: number;
    does_not_own_a_cat: number;
    does_not_own_caged_pets: number;
    accepts_only_one_client_at_a_time: number;
    has_no_children: number;
    sort_by_rating: number;
    sort_by_review: number;
    no_children_0_5_years_old: number;
    no_children_6_12_years_old: number;
    minPrice: number;
    maxPrice: number;
}

/**
 *
 * @param props contains sitter data
 * @function contains view of our.
 */
export default function SearchSitterOtherFilterModal({data, showModal, hideModal}: I_PROPS) {
    const router = useRouter();
    const value: any = useContext(AppContext);
    const [filter, setFilter] = useState<any>({})
    const [otherFilter, setOtherFilter] = useState<I_OTHER_FILTERS>({
        transportation: 1,
        closest_sitter: sitterSorting[0],
        has_house: 0,
        has_fenced_yard: 0,
        dogs_allowed_on_furniture: 0,
        dogs_allowed_on_bed: 0,
        non_smoking_home: 0,
        does_not_own_a_dog: 0,
        does_not_own_a_cat: 0,
        does_not_own_caged_pets: 0,
        accepts_only_one_client_at_a_time: 0,
        has_no_children: 0,
        sort_by_rating: 0,
        sort_by_review: 0,
        no_children_0_5_years_old: 0,
        no_children_6_12_years_old: 0,
        minPrice: 0,
        maxPrice: 5000,
    });

    useEffect(() => {
        setFilter(data)

    }, [showModal])

    /**
     * @function to.
     */
    const handleSearch = () => {
        let data = {
            city: filter.city,
            locality: filter.locality,
            postal_code: filter.postal_code,
            formatted_address: filter.formatted_address,
            lat: filter.lat,
            lng: filter.lng,
            pet: filter.pet,
            service: filter.service,
            petSize: filter.petSize,
            medical_service_id: filter.medical_service_id,
            grooming_service_id: filter.grooming_service_id,
            checkInDate: filter.checkInDate,
            checkOutDate: filter.checkOutDate,
            defaultValue: filter.defaultValue,
            transportation: otherFilter.transportation,
            closest_sitter: otherFilter.closest_sitter,
            has_house: otherFilter.has_house,
            has_fenced_yard: otherFilter.has_fenced_yard,
            dogs_allowed_on_furniture: otherFilter.dogs_allowed_on_furniture,
            dogs_allowed_on_bed: otherFilter.dogs_allowed_on_bed,
            non_smoking_home: otherFilter.non_smoking_home,
            does_not_own_a_dog: otherFilter.does_not_own_a_dog,
            does_not_own_a_cat: otherFilter.does_not_own_a_cat,
            does_not_own_caged_pets: otherFilter.does_not_own_caged_pets,
            accepts_only_one_client_at_a_time: otherFilter.accepts_only_one_client_at_a_time,
            has_no_children: otherFilter.has_no_children,
            sort_by_rating: otherFilter.sort_by_rating,
            sort_by_review: otherFilter.sort_by_review,
            no_children_0_5_years_old: otherFilter.no_children_0_5_years_old,
            no_children_6_12_years_old: otherFilter.no_children_6_12_years_old,
            minPrice: otherFilter.minPrice,
            maxPrice: otherFilter.maxPrice,
        };
        value.setState(data);
        router.push({
            pathname: "/search-sitter",
        }).then(r => {
        });
    };

    return (
        <Modal size='lg' show={showModal}>
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">{strings.ChooseOtherFilters}</h5>
                    <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={() => hideModal()}
                    >
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div className="modal-body">
                    <div className='m-2'>
                        <h6 className="mb-2 filter-title">{strings.HousingConditions}</h6>
                        <div className="filter-dropdown-items row pb-0">
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                has_house: otherFilter.has_house == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.has_house == 1}
                                        type="checkbox"
                                        name="more3"/>
                                    <span className="checkmark"/>
                                    {strings.HasHouseExcludesApartments}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                has_fenced_yard: otherFilter.has_fenced_yard == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.has_fenced_yard == 1}
                                        type="checkbox"
                                        name="more4"/>
                                    <span className="checkmark"/>
                                    {strings.HasFencedYard}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                dogs_allowed_on_furniture: otherFilter.dogs_allowed_on_furniture == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.dogs_allowed_on_furniture == 1}
                                        type="checkbox"
                                        name="more5"/>
                                    <span className="checkmark"/>
                                    {strings.DogsAllowedOnFurniture}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                dogs_allowed_on_bed: otherFilter.dogs_allowed_on_bed == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.dogs_allowed_on_bed == 1}
                                        type="checkbox"
                                        name="more6"/>
                                    <span className="checkmark"/>
                                    {strings.DogsAllowedOnBed}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                non_smoking_home: otherFilter.non_smoking_home == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.non_smoking_home == 1}
                                        type="checkbox"
                                        name="more6"/>
                                    <span className="checkmark"/>
                                    {strings.NonSmokingHome}
                                </label>
                            </div>
                            <hr/>
                        </div>
                        <h6 className="mb-2 filter-title mt-2">{strings.PetsInTheHome}</h6>
                        <div className="filter-dropdown-items pb-0 row">
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                does_not_own_a_dog: otherFilter.does_not_own_a_dog == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.does_not_own_a_dog == 1}
                                        type="checkbox"
                                        name="more7"/>
                                    <span className="checkmark"/>
                                    {strings.DoesnotOwnADog}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                does_not_own_a_cat: otherFilter.does_not_own_a_cat == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.does_not_own_a_cat == 1}
                                        type="checkbox"
                                        name="more8"/>
                                    <span className="checkmark"/>
                                    {strings.DoesnotOwnACat}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                accepts_only_one_client_at_a_time: otherFilter.accepts_only_one_client_at_a_time == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.accepts_only_one_client_at_a_time == 1}
                                        type="checkbox"
                                        name="more9"/>
                                    <span className="checkmark"/>
                                    {strings.AcceptsOnlyOneClientAtATime}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                does_not_own_caged_pets: otherFilter.does_not_own_caged_pets == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.does_not_own_caged_pets == 1}
                                        type="checkbox"
                                        name="more9"/>
                                    <span className="checkmark"/>
                                    {strings.DoesNotOwnCagedPets}
                                </label>
                            </div>
                            <hr/>
                        </div>
                        {/*<a href="#" className="filter-more">*/}
                        {/*  + More*/}
                        {/*</a>*/}
                        <h6 className="mb-2 filter-title mt-2">{strings.ChildrenInTheHome}</h6>
                        <div className="filter-dropdown-items pb-0 row">
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                has_no_children: otherFilter.has_no_children == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.has_no_children == 1}
                                        type="checkbox"
                                        name="more10"/>
                                    <span className="checkmark"/>
                                    {strings.HasNoChildren}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                no_children_0_5_years_old: otherFilter.no_children_0_5_years_old == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.no_children_0_5_years_old == 1}
                                        type="checkbox"
                                        name="more11"/>
                                    <span className="checkmark"/>
                                    {strings.NoChildren05YearsOld}
                                </label>
                            </div>
                            <div className="custom-check mb-2 col-md-6">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                no_children_6_12_years_old: otherFilter.no_children_6_12_years_old == 1 ? 0 : 1,
                                            })
                                        }
                                        checked={otherFilter.no_children_6_12_years_old == 1}
                                        type="checkbox"
                                        name="more12"/>
                                    <span className="checkmark"/>
                                    {strings.NoChildren612YearsOld}
                                </label>
                            </div>
                            <hr/>
                        </div>
                        {/*<a href="#" className="filter-more">*/}
                        {/*  - Less*/}
                        {/*</a>*/}
                    </div>
                    <div className="row mx-2">
                        <div className="col-sm-auto col-md-6 form-group px-0 mb-2 ">
                            <div className='mx-0 px-0'>
                                <label className='mx-0 px-0'><h6 className='mx-0 px-0'>{strings.Price}</h6></label>
                            </div>
                            <div className='mx-3'>
                                <small className="font-10 float-left">{strings.B_Min_B}</small>
                                <small className="font-10 float-right">{strings.B_Max_B}</small>
                            </div>

                            <div className="mt-3 mb-1 mx-3">
                                <Range
                                    step={25}
                                    value={[otherFilter.minPrice, otherFilter.maxPrice]}
                                    min={0}
                                    max={5000}
                                    trackStyle={[{backgroundColor: "#20847e"}]}
                                    handleStyle={[{border: "2px solid #20847e"},{border: "2px solid #20847e"}]}
                                    onChange={(e) => {
                                        setOtherFilter({
                                            ...otherFilter,
                                            minPrice: e[0],
                                            maxPrice: e[1]
                                        })
                                    }}
                                />{" "}
                            </div>
                            <div className='mx-3'>
                                <small className="font-10 float-left">
                                    {otherFilter.minPrice}
                                </small>
                                <small className="font-10 float-right">
                                    {otherFilter.maxPrice}
                                </small>
                            </div>
                        </div>
                        <div className="custom-check mb-1 col-12 px-0">
                            <h6 className="mb-2 filter-title mt-2">{strings.HasTransportation}</h6>
                                <div className='row'>
                                <div className="custom-check mb-2 col-auto">
                                    <label className="check ">
                                        <input
                                            onChange={() =>
                                                setOtherFilter({
                                                    ...otherFilter,
                                                    transportation: 1
                                                })
                                            }
                                            checked={otherFilter.transportation == 1}
                                            type="checkbox"
                                            name="more12"/>
                                        <span className="checkmark"/>
                                        {strings.Yes}
                                    </label>
                                </div>
                                <div className="custom-check mb-2 col-auto">
                                <label className="check ">
                                    <input
                                        onChange={() =>
                                            setOtherFilter({
                                                ...otherFilter,
                                                transportation: 0
                                            })
                                        }
                                        checked={otherFilter.transportation == 0}
                                        type="checkbox"
                                        name="more12"/>
                                    <span className="checkmark"/>
                                    {strings.No}
                                </label>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <div className="d-flex my-0 py-0 justify-content-end">
                        <button onClick={() => handleSearch()} className="btn btn-primary px-3 py-2">{strings.Next}</button>
                    </div>
                </div>
            </div>
        </Modal>
    );
}
