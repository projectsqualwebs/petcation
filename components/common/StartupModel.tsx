import React from "react";
import { Modal } from "react-bootstrap";
import { strings } from "../../public/lang/Strings";
import { getCurrencySign } from "../../api/Constants";
import Link from "next/link";

export default function StartupModel({showModal, closeModal}){
    return (
        <Modal
            show={showModal}
            id="settings"
            aria-labelledby="settings"
            dialogClassName="modal-md modal-dialog-centered"
            tabIndex="-1"
            scrollable
            onHide={() => closeModal()}
        >
           <Modal.Header className="border-0 py-1">

                     <button
                       type="button"
                       className="close"
                       data-dismiss="modal"
                       aria-label="Close"
                       onClick={()=>closeModal()}
                     >

                         <span aria-hidden="true">&times;</span>

                     </button>
            </Modal.Header>
            <Modal.Body className="p-0 ml-2 mr-2">

                <div className="row justify-content-center main-wrapper">
                  <div className="points-details">
                            <div className="circle"></div>
                            <div className="circle tr"></div>
                            <div className="circle br"></div>
                            <div className="circle bl"></div>
                               <div className="row align-items-center">
                              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <h5 className="text-uppercase fw-bold font-bold font-weight-bold mb-4 bg-warning w-auto d-inline-block py-2 px-3 rounded-5">
                                   {strings.WelcomeToPetcation}
                                </h5>
                                <div className="invitions mb-3">
                                  <h6>{strings.LetsMakeADaySpecialForYouPets}</h6>
                                </div>
                                <div className="gift-card">
                                  <h6 className="font-semibold">
                                    {strings.Registerandget}
                                  </h6>
                                  <h6 className="font-medium mb-0">
                                    {" "}
                                    <span className="font-semibold">
                                      {getCurrencySign()}1500{" "}
                                    </span>
                                    {strings.offonyourfirstbooking}
                                  </h6>
                                </div>
                              </div>
                            </div>
                          </div>
                </div>
                <div className="row">
                      <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                          <div className="text-center">
                                 <Link href="/signup">
                                    <a
                                      onClick={() => {
                                      closeModal();
                                      }}
                                      className="btn btn-primary w-100">
                                        {strings.Joinnow}
                                    </a>
                                 </Link>
                           </div>
                      </div>
                       <div className="col-12 col-md-12 col-lg-12 col-xl-12 mt-1">
                          <p className="text-center text-decoration-underline cursor-pointer"
                              style={{ color: 'black' }} onClick={() => {
                                  closeModal();
                          }}> {strings.Skip}
                          </p>
                       </div>
                </div>
            </Modal.Body>
        </Modal>

    )
}

