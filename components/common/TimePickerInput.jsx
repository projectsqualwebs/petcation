import React, { useState, useEffect, useRef } from "react";
import { ArrowLeft2, Clock, TickSquare } from "iconsax-react";
import { strings } from "../../public/lang/Strings";

const TimePickerInput2 = (props) => {
    const wrapperRef = useRef(null);
    const [hour, setHour] = useState(null);
    const [minute, setMinute] = useState(null);
    const [amPm, setAmPm] = useState("AM");
    const [isActive, setIsActive] = useState(false);
    const [timeValue, setTimeValue] = useState(null);
    const [tickActive, setTickActive] = useState(false);
    const [errors, setErrors] = useState({
        hour: false,
        minute: false,
        amPm: false,
    });
    const selectTime = (amPmType) => {
        let errors = { ...errors };
        // if (props.pickDropTimeFrom) {
        //   const splitStepNew1 = props.pickDropTimeFrom.split(" ");
        //   let newAmPm = splitStepNew1[1];
        //   const splitStepNew2 = splitStepNew1[0].split(":");
        //   let newhour = splitStepNew2[0];
        //   let newMinute = splitStepNew2[1];
        //   if (newAmPm === amPmType) {
        //     if (amPmType === "PM") {
        //       if (Number(newhour) <= Number(hour) && Number(hour) <= 12) {
        //       } else {
        //         errors.hour = true;
        //       }
        //     }
        //     if (amPmType === "AM") {
        //       console.log("hello");
        //       if (Number(newhour) <= Number(hour) && Number(hour) < 12) {
        //       } else {
        //         errors.hour = true;
        //       }
        //     }
        //   } else if (newAmPm == "PM" && amPmType == "AM") {
        //     errors.hour = true;
        //   }
        //   if (newhour === hour && newMinute >= minute) {
        //     errors.hour = true;
        //   }
        // }

        if (!hour || hour < 1 || hour > 12) {
            errors.hour = true;
        } else {
            errors.hour = false;
        }
        if (!minute || minute < 0 || minute > 59) {
            errors.minute = true;
        } else {
            errors.minute = false;
        }

        if (!amPmType) {
            errors.amPm = true;
        } else {
            errors.amPm = false;
        }

        setErrors(errors);
        if (errors.minute || errors.hour || errors.amPm) {
            return false;
        }
        const amPm = amPmType;

        if (minute < 10 || hour < 10) {
            if (minute < 10 && hour < 10) {
                let min = `0${Number(minute)}`;
                let hou = `0${Number(hour)}`;
                props.setDropOffTime(hou, min, amPm);
                setTimeValue(`${hou}:${min} ${amPm}`);
                setIsActive(false);
            } else if (minute < 10) {
                let min = `0${Number(minute)}`;
                props.setDropOffTime(hour, min, amPm);
                setTimeValue(`${hour}:${min} ${amPm}`);
                setIsActive(false);
            } else if (hour < 10) {
                let hou = `0${Number(hour)}`;
                props.setDropOffTime(hou, minute, amPm);
                setTimeValue(`${hou}:${minute} ${amPm}`);
                setIsActive(false);
            }
        } else {
            props.setDropOffTime(hour, minute, amPm);
            setTimeValue(`${hour}:${minute} ${amPm}`);
            setIsActive(false);
        }
    };

    const handleClickOutside = (event) => {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setIsActive(false);
        }
    };

    useEffect(() => {
        if (hour && minute && amPm) {
            setTickActive(true);
        } else {
            setTickActive(false);
        }
    }, [hour, minute, amPm]);

    useEffect(() => {
        if (props?.hideTime === true) {
            setIsActive(false);
        }

        if (props.value) {
            const timeValueProp = props.value;
            setTimeValue(timeValueProp);
            const splitStep1 = timeValueProp.split(" ");
            setAmPm(splitStep1[1]);
            const splitStep2 = splitStep1[0].split(":");
            setHour(splitStep2[0]);
            setMinute(splitStep2[1]);
        } else {
            if (props.value === "") {
                setTimeValue("");
                setAmPm("");
                setHour("");
                setMinute("");
            }
        }

        document.addEventListener("click", handleClickOutside, false);
        return () => {
            document.removeEventListener("click", handleClickOutside, false);
        };
    }, [props.value, props.hideTime]);

    return (
        <>
            <div ref={wrapperRef}>
                <div className={"cus-picker " + (!props?.disable && isActive ? "show" : "") + (props?.disable ? "disabled": '')}>
                    <div className="head d-flex justify-content-between align-items-center">
                        <h6 className="mb-0">{strings.SelectTime}</h6>
                        {/* <TickSquare variant="Linear" onClick={() => selectTime()} /> */}
                    </div>
                    <div className="body">
                        <ArrowLeft2 className="pointer" variant="Bold" />
                        <div className="col-12 timer">
                            <div className="row align-items-center">
                                <div className="col-auto pl-0 pr-1">
                                    <input
                                        id={"hour" + props.picker}
                                        name={"hour" + props.picker}
                                        type="text"
                                        className={
                                            "form-control form-square mb-1 " +
                                            (errors.hour ? "is-invalid" : "")
                                        }
                                        placeholder="00"
                                        maxLength={2}
                                        step={2}
                                        min={1}
                                        max={12}
                                        value={hour}
                                        onChange={(e) => {
                                            if (e.target.value < 13) {
                                                setHour(e.target.value);
                                                setErrors({ ...errors, hour: false, minute: false });
                                                setMinute("00");
                                            }
                                        }}
                                        onKeyPress={(event) => {
                                            if (!/[0-9]/g.test(event.key)) {
                                                event.preventDefault();
                                            }
                                        }}
                                    />
                                    <p className="mb-0 text-center">Hour</p>
                                </div>
                                <div className="col-auto text-center px-2">
                                    <h4 className="mb-4 text-center">:</h4>
                                </div>
                                <div className="col-auto pr-0 pl-1">
                                    <input
                                        id={"minute" + props.picker}
                                        name={"minute" + props.picker}
                                        type="text"
                                        className={
                                            "form-control form-square mb-1 " +
                                            (errors.minute ? "is-invalid" : "")
                                        }
                                        placeholder="00"
                                        maxLength={2}
                                        step={2}
                                        value={minute}
                                        min={0}
                                        max={59}
                                        onChange={(e) => {
                                            if (e.target.value < 60) {
                                                setErrors({ ...errors, minute: false });
                                                setMinute(e.target.value);
                                            }
                                        }}
                                        onKeyPress={(event) => {
                                            if (!/[0-9]/g.test(event.key)) {
                                                event.preventDefault();
                                            }
                                        }}
                                    />
                                    <p className="mb-0 text-center">Minutes</p>
                                </div>
                                <div className="col-auto pr-0">
                                    <div className="checkbox-group grade-slc w-100 mb-0">
                                        <div className="checkbox">
                                            <label className="checkbox-wrapper">
                                                <input
                                                    className="checkbox-input"
                                                    id={"school_standard" + props.picker}
                                                    name={"school_standard" + props.picker}
                                                    type="radio"
                                                    value={"AM"}
                                                    checked={amPm === "AM"}
                                                    onClick={(e) => {
                                                        setAmPm("AM");
                                                        selectTime("AM");
                                                    }}
                                                />
                                                <span className="checkbox-tile">
                          <span className="checkbox-label">AM</span>
                        </span>
                                            </label>
                                        </div>
                                        <div className="checkbox">
                                            <label className="checkbox-wrapper">
                                                <input
                                                    className="checkbox-input"
                                                    id={"school_standard" + props.picker}
                                                    name={"school_standard" + props.picker}
                                                    type="radio"
                                                    value={"PM"}
                                                    onClick={(e) => {
                                                        setAmPm("PM");
                                                        selectTime("PM");
                                                    }}
                                                    checked={amPm === "PM"}
                                                />
                                                <span className="checkbox-tile">
                          <span className="checkbox-label">PM</span>
                        </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onClick={() => (props.onFocusInput ? props.onFocusInput() : null)}>
                    {/* <Clock className="icon-field" variant="linear" /> */}
                    <input
                        id={"name" + props.picker}
                        className={"form-control"}
                        name={"name" + props.picker}
                        type={"text"}
                        value={timeValue}
                        readOnly={true}
                        onFocus={() => {
                            setTimeout(() => {
                                setIsActive(true);
                            }, 400);
                        }}
                        placeholder={
                            props?.placeholder ? props.placeholder : strings.SelectTime
                        }
                    />
                </div>
            </div>
        </>
    );
};

export default TimePickerInput2;
