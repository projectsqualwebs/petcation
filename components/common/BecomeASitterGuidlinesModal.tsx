import React from "react";
import { Modal } from "react-bootstrap";
import {strings} from "../../public/lang/Strings";
import {useRouter} from "next/router";

export default function BecomeASitterGuidelinesModal(props: any) {
    let router = useRouter();
    return (
        <Modal
            show={props.showModal}
            id="settings"
            className="modal-child"
            aria-labelledby="settings"
            tabIndex="-1"
            scrollable
        >
            <Modal.Header className="p-3">
                <Modal.Title as="h4" className="fw-medium">
                    {strings.StepsToBecomeASitter}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="p-3">
                <div className="col-12 form-details">
                    <ol>
                        <li>{strings.CompleteProfileAndUploadDocument}</li>
                        <li>{strings.AddServices}</li>
                        <li>{strings.ManageAvailability}</li>
                        <li>{strings.GetYourProfileVerifiedFromAdmin}</li>
                    </ol>

                </div>
                <div className="d-flex justify-content-between flex-row">
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={props.hideModal}
                        className="btn float-end"
                    >
                        {strings.Close}
                    </button>
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={() => {
                            router.push("/user/my-profile").then(r => props.hideModal())
                        }}
                        className="btn btn-primary float-end"
                    >
                        {strings.Proceed}
                    </button>
                </div>
            </Modal.Body>
        </Modal>
    );
}
