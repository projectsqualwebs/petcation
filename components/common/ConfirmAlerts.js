import React from 'react'
import {strings} from "../../public/lang/Strings";

export const confirmWithdrawRequest = (request) => {
    confirmAlert({
      closeOnClickOutside: true,
      customUI: ({ onClose }) => {
        return (
          <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
            <div className="react-confirm-alert-body">
              <h1>{strings.WithdrawRequest}</h1>
              <p>{strings.areYouSure}</p>
              <div className="react-confirm-alert-button-group">
                <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                  onClose();
                }}>{strings.Yes}</button>
                <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                  onClose();
                }}>{strings.No}</button>
              </div>
            </div>
          </div>
        );
      }
    });
  }

export const confirmRejectRequest = (request) => {
    confirmAlert({
      closeOnClickOutside: true,
      customUI: ({ onClose }) => {
        return (
          <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
            <div className="react-confirm-alert-body">
              <h1>Reject Request?</h1>
              <p>Are you sure?</p>
              <div className="react-confirm-alert-button-group">
                <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                  rejectRequest(request.id)
                  onClose();
                }}>Yes</button>
                <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                  onClose();
                }}>No</button>
              </div>
            </div>
          </div>
        );
      }
    });
  }


