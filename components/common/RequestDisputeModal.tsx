import React, { useEffect, useState } from 'react';
import { Modal } from "react-bootstrap";
import API from "../../api/Api";
import {strings} from "../../public/lang/Strings";
import {petSize} from "../../public/appData/AppData";
import ActionLoader from "./ActionLoader";

let api = new API();

/**
 *
 * @param props contains request data for which we want to cancel
 * @function contains our request cancel modal.
 */
export default function RequestDisputeModal(props:any) {
    const [reasonError, setReasonError] = useState<boolean>(false);
    const [reason, setReason] = useState<string>('');

    useEffect(()=> {
        setReason('')
    }, [props]);

    return (
        <Modal
            show={props.showModal}
            id="settings"
            className="modal-child"
            aria-labelledby="settings"
            tabIndex="-1"
            scrollable
        >
            <Modal.Header className="p-3">
                <Modal.Title as="h4" className="fw-medium">
                    {strings.areYouSure}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="p-3">
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                        <h6>{strings.ReasonOfDispute}</h6>
                        <textarea
                            className={`form-control ${reasonError ? "invalid" : ''}`}
                            placeholder={strings.Message}
                            rows={4}
                            value={reason}
                            onChange={(e) => {setReasonError(false); setReason(e.target.value)}}
                            required
                        />
                    </div>
                </div>
                <hr />
                {props?.loading ? <ActionLoader/> : <div className="d-flex justify-content-between flex-row">
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={() => {
                            if(!reason) {
                                setReasonError(true);
                                return false
                            }
                            props.hideModal();
                            props.handleSubmit({
                                booking_id : props.request?.id,
                                reason: reason
                            });
                        }}
                        className="btn btn-primary float-end"
                    >
                        {strings.Dispute}
                    </button>
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={props.hideModal}
                        className="btn float-end"
                    >
                        {strings.Cancel}
                    </button>
                </div>}
            </Modal.Body>
        </Modal>
    )
}
