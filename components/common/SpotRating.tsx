import { Modal } from "react-bootstrap";
import React, { useEffect } from "react";
import ReactStars from "react-rating-stars-component";
import { useState } from "react";
// import RatingStars from "./RatingStars";
import API from "../../api/Api";
import Cookies from "universal-cookie";
import { strings } from "../../public/lang/Strings";
import Dropzone from "react-dropzone";
import Loader from "./Loader";
// import Link from "next/link";
const cookie = new Cookies();

const api = new API();
/**
 *
 * @param props contains rating object to get data
 * @function contains modal of spot rating
 */
export default function SpotRating(props) {
  const [starKeyForce, setStarKeyForce] = useState(0);

  const [data, setData] = useState({
    review: "",
    rating: 0,
  });
  const [spotId, setSpotId] = useState("");
  const [errorFiles, setErrorFiles] = useState([]);
  const [images, setImages] = useState([]);
  const [disableButton, setDisableButton] = useState(false);
  const [maxFileCountError, setMaxFileCountError] = useState(false);
  const [errors, setErrors] = useState({
    review: false,
    rating: false,
  });
  /**
   * To initialize data
   */
  useEffect(() => {
    setSpotId(props.spotId);
    if (props.data.review_rating) {
      setData({
        rating: props.data.review_rating.rating,
        review: props.data.review_rating.review,
      });
      props.data?.review_rating?.images?.length
        ? setImages(props.data?.review_rating?.images?.map((e) => e.image))
        : null;
    }
  }, [props.spotId, props.data.review_rating]);

  useEffect(() => {
    setStarKeyForce((prev) => prev + 1);
  }, [data.rating]);

  /**
   * function to set spot rating in state.
   * @newRating contains count of selected rating stars.
   */
  const ratingChanged = (newRating) => {
    setErrors({ ...errors, rating: false });
    setData({ ...data, rating: newRating });
  };

  /**
   * function to RATE SPOT.
   * @function
   *      success-> will rate our selected spot.
   *      failure-> console error and reject rating request.
   */
  const handleRating = () => {
    if (!data?.rating) {
      setErrors({ ...errors, rating: true });
      return;
    }
    if (!data?.review) {
      setErrors({ ...errors, review: true });
      return false;
    }

    api
      .rateReviewSpot({
        spot_id: spotId,
        review: data.review,
        rating: data.rating,
        images: images,
      })
      .then((res) => {
        props.updateSpot(spotId);
        setErrorFiles([]);
        props.onHide();
      })
      .catch((error) => {
        console.log("error:-", error);
      });
  };

  const onSuccess = (acceptedFiles) => {
    setDisableButton(true);
    setMaxFileCountError(false);
    if (acceptedFiles.length > 5) {
      setMaxFileCountError(true);
      setDisableButton(false);
      return false;
    }
    let fileSizeMoreThan10 = acceptedFiles.filter((file) => file.size > 10e6);
    if (fileSizeMoreThan10.length) {
      onUpload(acceptedFiles.filter((file) => file.size < 10e6));
      setErrorFiles(acceptedFiles.filter((file) => file.size > 10e6));
    } else {
      onUpload(acceptedFiles);
    }
  };

  const onUpload = (data: any) => {
    const formData = new FormData();
    data.map((value) => {
      formData.append("images[]", value);
    });
    formData.append("path", "spot");
    api
      .uploadMultiFile(formData)
      .then((json) => {
        setImages([...images, ...json.data.response]);
        setDisableButton(false);
      })
      .catch((error) => console.log(error));
  };

  const deleteImage = (index) => {
    let image = images.filter((val, i) => i != index);
    setImages(image);
  };

  return (
    <Modal
      show={props.showModal}
      size={"lg"}
      dialogClassName="modal-dialog"
      aria-labelledby="example-custom-modal-styling-title"
    >
      <div role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">
              {props.data.review_rating
                ? strings.EditFeedBack
                : strings.ProvideFeedBack}
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={() => props.onHide()}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <div className="modal-body">
            <div className="meetup-details">
              <form>
                <div className="row">
                  <div className="col-12">
                    <fieldset className="d-flex justify-content-center ">
                      <ReactStars
                        count={5}
                        onChange={ratingChanged}
                        size={44}
                        edit={true}
                        activeColor="#20847e"
                        value={data.rating}
                        key={starKeyForce}
                      />
                    </fieldset>
                    <div className="col-12 form-group">
                      <label>
                        {strings.Message}
                        <span className="text-danger">*</span>
                      </label>
                      {errors.review ? (
                        <label>
                          <span className="text-danger">
                            {strings.Required}
                          </span>
                        </label>
                      ) : (
                        ""
                      )}

                      <textarea
                        className={`form-control ${
                          errors.review ? "is-invalid" : ""
                        }`}
                        placeholder={strings.Message}
                        rows={4}
                        value={data.review}
                        name="review"
                        onChange={(e) => {
                          setErrors({ ...errors, [e.target.name]: false });
                          setData({ ...data, review: e.target.value });
                        }}
                        required
                      />
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-12 form-group mt-2">
              <div className="spot-images">
                <div className="row align-items-center">
                  {images.map((value, index) => (
                    <div
                      key={index}
                      className="col-12 col-md-4 col-lg-4 col-xl-4 position-relative mb-2"
                    >
                      <a
                        onClick={() => deleteImage(index)}
                        className={
                          "bg-danger position-absolute top-0 p-1 px-2 rounded-lg m-1 d-flex"
                        }
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="mb-0"
                          width="16"
                          height="16"
                          viewBox="0 0 24 24"
                          fill="none"
                        >
                          <path
                            d="m9.17 14.83 5.66-5.66M14.83 14.83 9.17 9.17M9 22h6c5 0 7-2 7-7V9c0-5-2-7-7-7H9C4 2 2 4 2 9v6c0 5 2 7 7 7Z"
                            stroke="#FFFFFF"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          ></path>
                        </svg>
                      </a>
                      <div className="spot-images">
                        <img
                          src={value}
                          key={index}
                          className="img-fluid w-100"
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <Dropzone
              accept={{ ["image/*"]: [".jpeg", ".png", ".jpg", ".webp"] }}
              onDrop={(acceptedFiles) => onSuccess(acceptedFiles)}
            >
              {({ getRootProps, getInputProps }) => (
                <div className="col-12 form-group mt-3">
                  <section>
                    <div {...getRootProps()}>
                      <input {...getInputProps()} />
                      <div className="upload-doc profile-doc">
                        <div className="file">
                          <div>
                            <div>
                              <svg
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="far"
                                data-icon="arrow-to-top"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 384 512"
                                className="svg-inline--fa fa-arrow-to-top fa-w-12 fa-2x"
                              >
                                <path
                                  fill="currentColor"
                                  d="M35.5 279.9l148-148.4c4.7-4.7 12.3-4.7 17 0l148 148.4c4.7 4.7 4.7 12.3 0 17l-19.6 19.6c-4.8 4.8-12.5 4.7-17.1-.2L218 219.2V468c0 6.6-5.4 12-12 12h-28c-6.6 0-12-5.4-12-12V219.2l-93.7 97.1c-4.7 4.8-12.4 4.9-17.1.2l-19.6-19.6c-4.8-4.7-4.8-12.3-.1-17zM12 84h360c6.6 0 12-5.4 12-12V44c0-6.6-5.4-12-12-12H12C5.4 32 0 37.4 0 44v28c0 6.6 5.4 12 12 12z"
                                />
                              </svg>
                            </div>
                            {strings.clickToUpload} {strings.dragFilesHere}
                            <br />
                            {strings.acceptedImage}
                            <br />
                            {strings.fileShouldBeLessThan10MB}
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              )}
            </Dropzone>
          </div>
          <div className="modal-footer">
            <div className="text-center d-flex justify-content-between w-100">
              {maxFileCountError ? (
                <div className="ml-0">
                  <small className="text-danger text-center w-100 my-2">
                    *{strings.YouCanSelectMaximum5FilesAtATime}
                  </small>
                </div>
              ) : null}

              {errorFiles?.length ? (
                <div className="ml-0">
                  <small className="text-danger text-center w-100 my-2">
                    *
                    {errorFiles.map(
                      (file, index) =>
                        file?.name +
                        (index + 1 === errorFiles?.length ? " " : ", ")
                    ) +
                      " " +
                      strings.areMoreThan10MB}
                  </small>
                </div>
              ) : null}
              <div>
                {disableButton ? (
                  <Loader />
                ) : (
                  <>
                    <button
                      onClick={() => handleRating()}
                      type="button"
                      className="btn btn-primary"
                      data-dismiss="modal"
                    >
                      {errorFiles?.length
                        ? `(${errorFiles?.length}) ${strings.SkipAnd} ` +
                          strings.RateNow
                        : strings.RateNow}
                    </button>
                    {console.log("rating", errors?.rating)}
                    {errors?.rating ? (
                      <span className="text-danger text-align-end mx-2">
                        {console.log("hii")}
                        <small>{strings.RatingRequire}</small>
                      </span>
                    ) : (
                      ""
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
}
