import React, { useEffect, useState } from 'react';
import { Modal } from "react-bootstrap";
import API from "../../api/Api";
import {strings} from "../../public/lang/Strings";
import {petSize} from "../../public/appData/AppData";
import ActionLoader from "./ActionLoader";

let api = new API();

/**
 *
 * @param props contains request data for which we want to cancel
 * @function contains our request cancel modal.
 */
export default function RequestCancelModal(props:any) {
    const [additionalServices, setAdditionalServices] = useState<any>([]);
    const [userPets, setUserPets] = useState<any>([]);
    const [reasonError, setReasonError] = useState<boolean>(false);
    const [reason, setReason] = useState<string>('');
    const [loading, setLoading] = useState(false);

    useEffect(()=> {
        if(props.request) {
            props.request.pets.map((val, index) => {
                let arr = [];
                arr.push(parseInt(val.id));
                setUserPets([...arr]);
            })
            if(props.request.additional_services){
                props.request.additional_services.map((service, index) => {
                    let s_arr = [];
                    s_arr.push(parseInt(service.id));
                    setAdditionalServices([...s_arr]);
                })
            }
            setReason('')
        }
    }, [props]);

    useEffect(() => {
        setLoading(false)
    }, [props.showModal]);

    return (
        <Modal
            show={props.showModal}
            id="settings"
            className="modal-child"
            aria-labelledby="settings"
            tabIndex="-1"
            scrollable
        >
            <Modal.Header className="p-3">
                <Modal.Title as="h4" className="fw-medium">
                    {strings.areYouSure}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>

                <div className="raw">
                    <h6>{strings.ReasonOfCancellation}</h6>
                    <textarea
                        className={`form-control ${reasonError ? "invalid" : ''}`}
                        placeholder={strings.Message}
                        rows={4}
                        value={reason}
                        onChange={(e) => {setReasonError(false); setReason(e.target.value)}}
                        required
                    />
                </div>

                <hr/>
                {props.request.payment_status == 1 && <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="">
                        <h5 className="font-semibold mb-3">
                            {props.request?.service?.name + " " + strings.Cancellationpolicy}
                        </h5>
                        {props.request.cancellation_policy == 1 && <>
                            <li>{strings.YouWllGetAFullRefundIfYouCancelBeforeTheDaysServiceIsDelivered}</li>
                            <li>{strings.IfYouCancelAfterTheStayOrWalkBegins}</li>
                        </>
                        }
                        {props.request.cancellation_policy == 2 && <>
                            <li>{strings.YouCancelWithin48HoursOfBooking}</li>
                            <li>{strings.TheReservationYouAreCancellingDoesnotOverlapWith}</li>
                            <li>{strings.IfYouCancelAfter1200Noon3DaysBeforeTheStayBegins}</li>
                        </>
                        }
                        {props.request.cancellation_policy == 3 && <>
                            <li>{strings.IfYouCancelAfter1200NoonOneWeekBeforeTheStayBegins}</li>
                        </>
                        }
                    </div>
                    <hr />
                </div>}
                {props?.loading ? <ActionLoader/> : <div className="d-flex justify-content-between flex-row">
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={() => {
                            if(!reason) {
                                setReasonError(true);
                                return false
                            }
                            props.withdrawRequest(props.request.id, {
                                userPets: userPets,
                                additionalServices : additionalServices,
                                reason: reason
                            });
                        }}
                        className="btn btn-primary float-end"
                    >
                        {props.request.payment_status ? strings.Cancel : strings.Withdraw}
                    </button>
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={props.hideModal}
                        className="btn float-end"
                    >
                        {props.request.payment_status ? strings.Back : strings.Cancel}
                    </button>
                </div>}
            </Modal.Body>
        </Modal>
    )
}
