import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import API from "../../api/Api";
import { strings } from "../../public/lang/Strings";
import { petSize } from "../../public/appData/AppData";
import moment from "moment";
import { getCurrencySign } from "../../api/Constants";
import Link from "next/link";
import { TimeFormat } from "../../utils/Helper";
import AddNotesModal from "./AddNotesModal";
import ViewNotesModal from "./viewNotesModal";
import Loader from "./Loader";

let api = new API();

/**
 *
 * @param props contains request data for which we want to cancel
 * @function contains our request cancel modal.
 */
export default function RequestViewModal(props: any) {
  const [additionalServices, setAdditionalServices] = useState<any>([]);
  const [userPets, setUserPets] = useState<any>([]);
  const [showAddNotesModal, setShowAddNotesModal] = useState<boolean>(false);
  const [showViewNotesModal, setShowViewNotesModal] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [selectedPet, setSelectedPet] = useState<any>(null);
  const [data, setData] = useState<any>({});

  useEffect(() => {
    if (data.pets?.length && props.showModal === true) {
      data.pets?.map((val, index) => {
        let arr = [];
        arr.push(parseInt(val.id));
        setUserPets([...arr]);
      });
      if (data.additional_services) {
        data.additional_services.map((service, index) => {
          let s_arr = [];
          s_arr.push(parseInt(service.id));
          setAdditionalServices([...s_arr]);
        });
      }
    }
  }, [props.request]);

  useEffect(() => {
    if (props.request && props.showModal === true) {
      getSingleBooking();
    }
  }, [props.request, props.showModal]);

  const getSingleBooking = () => {
    api.getSingleBooking(props.request?.id).then((res) => {
      if (res.data) {
        setLoading(false);
        setData(res.data.response);
      }
    });
  };

  const reFetchData = () => {
    setSelectedPet(null);
    getSingleBooking();
  };

  const dateRangeIncludesToday = (startDate, endDate) => {
    const momentStartDate = moment(startDate).format("YYYY-MM-DD");
    const momentEndDate = moment(endDate).format("YYYY-MM-DD");
    if (
      momentStartDate &&
      !momentEndDate.includes("valid") &&
      momentEndDate &&
      !momentEndDate.includes("valid")
    ) {
      const today = moment();
      return today.isBetween(momentStartDate, momentEndDate, null, "[]");
    }
  };

  return (
    <>
      <Modal
        show={props.showModal}
        id="settings"
        className="modal-child"
        aria-labelledby="contained-modal-title-vcenter"
        tabIndex="-1"
        size={"xl"}
        centered
        scrollable
      >
        <Modal.Header className="p-3">
          <Modal.Title as="h4" className="fw-medium">
            {strings.BookingDetails}
          </Modal.Title>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
            onClick={() => props.hideModal()}
          >
            <span aria-hidden="true" className="">
              ×
            </span>
          </button>
        </Modal.Header>

        {loading ? (
          <Loader />
        ) : (
          <Modal.Body className="p-3">
            <div className="row ">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                {data?.want_to_receive_media === 1 ? (
                  <div className="logo d-flex justify-content-end">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-cloud-check"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M10.354 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"
                      />
                      <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383zm.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z" />
                    </svg>
                    <small className="">&nbsp;{strings.Petownerrequestedforvideosandphotosofpet}</small>
                  </div>
                ) : (
                  ""
                )}
                <h5 className="mob-h5 mb-2">{strings.PetsDetails}</h5>
                <div className="question-details px-2">
                  <div className="dog box" style={{ display: "block" }}>
                    <ul className="chec-radio mt-1">
                      {userPets &&
                        data.pets?.map((value, index) => (
                          <>
                            {value.status == 1 && (
                              <li key={index} className="pet-select">
                                <label className="radio-inline">
                                  <input
                                    type="checkbox"
                                    key={`pet${index}`}
                                    id={`pet${index}`}
                                    name="select1"
                                    disabled={true}
                                    className="pro-chx"
                                    defaultValue="yes"
                                  />
                                  <div className="select-radio text-center">
                                    <div>
                                      <div className="search-sitter-img">
                                        <img
                                          src={value.pet_image}
                                          className="img-fluid mb-2"
                                        />
                                      </div>
                                      <h6 className="font-14 mb-1 font-medium">
                                        {`${value.pet_name}, ${
                                          value.age_year + strings.yrs
                                        }`}
                                        <br />{" "}
                                        {`${
                                          petSize.find(
                                            (val) => val.value == value.weight
                                          ).label
                                        }`}
                                      </h6>
                                      <p className="font-12 mb-0">
                                        {value.breed?.breed}
                                      </p>
                                    </div>
                                  </div>
                                </label>
                              </li>
                            )}
                          </>
                        ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <hr />

            <div className="row">
              <div className="col-12 col-sm-6 mb-2 mb-md-0">
                <div className="db-service">
                  <h5 className="font-20 mb-1">
                    {data.service?.name} {" " + strings.service}
                  </h5>
                  <div className="d-flex align-items-center justify-content-between col-10 mb-2">
                    <h6 className="me-3 mb-0">Booking date range : </h6>
                    <p className="font-14 mb-0 text-muted">
                      {strings.From + " "}
                      {moment(new Date(data?.drop_of_date)).format(
                        "MM/DD/YYYY"
                      )}{" "}
                      &nbsp;{strings.To + " "}{" "}
                      {moment(new Date(data?.pickup_up_date)).format(
                        "MM/DD/YYYY"
                      )}
                    </p>
                  </div>
                  <div className="d-flex align-items-center justify-content-between col-10 mb-2">
                    <h6 className="me-3 mb-0 ">Check-in time : </h6>
                    <p className="font-14 mb-0  text-muted">
                      {strings.From + " "}
                      {moment(data?.drop_of_time_from, "hh:mm:ss").format(
                        "hh:mm A"
                      )}{" "}
                      &nbsp;{strings.To + " "}{" "}
                      {moment(data?.drop_of_time_to, "hh:mm:ss").format(
                        "hh:mm A"
                      )}
                    </p>
                  </div>
                  <div className="d-flex align-items-center justify-content-between col-10 mb-2">
                    <h6 className="me-3 mb-0">Check-out time : </h6>
                    <p className="font-14 mb-0 text-muted">
                      {strings.From + " "}
                      {moment(data?.pickup_up_time_from, "hh:mm:ss").format(
                        "hh:mm A"
                      )}{" "}
                      &nbsp;{strings.To + " "}{" "}
                      {moment(data?.pickup_up_time_to, "hh:mm:ss").format(
                        "hh:mm A"
                      )}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-12 col-sm-6 py-md-1 ">
                <div className="d-flex align-items-center justify-content-md-end service-pending">
                  <div className="my-auto mr-md-3 mr-2">
                    <p className="mb-0 font-12 text-muted">
                      {strings.PostedOn}
                      {moment(data.createdAt).format("MM/DD/YYYY")}
                    </p>
                  </div>
                  <div>
                    {data.status == 0 ? (
                      <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                        {strings.Pending}
                      </button>
                    ) : null}
                    {data.status == 1 ? (
                      <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                        {strings.Unpaid}
                      </button>
                    ) : null}
                    {data.status == 3 ? (
                      <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                        {strings.Paid}
                      </button>
                    ) : null}
                    {data.status == 4 ? (
                      <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                        {strings.completed}
                      </button>
                    ) : null}
                    {data.status == 2 ? (
                      <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                        {strings.Rejected}
                      </button>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div className="row">
              <div className="col-12 col-md-6 col-lg-6 col-xl-6 mb-3 mb-md-0">
                <div className="additional-services booking-period">
                  <h5 className=" mob-h5 mb-md-2 mb-0">
                    {strings.OwnerDetails}
                    <Link href={`/sitter-profile/${data.user?.id}?serviceId=`}>
                      <h5 className="text-muted cursor-pointer mt-2 mb-0 px-2">
                        {data?.user
                          ? data.user?.firstname + " " + data.user?.lastname
                          : ""}
                      </h5>
                    </Link>
                  </h5>
                  <span className="font-14 mb-0 text-muted mx-2">
                    {data?.user &&
                      `  ${
                        (data.user?.address
                          ? data.user?.address?.address
                          : "") +
                        "  " +
                        (data.user?.address ? data.user?.address?.city : "") +
                        "  " +
                        (data.user?.address ? data.user?.address?.postcode : "")
                      }`}
                  </span>
                </div>
              </div>
              <div className="col-12 col-md-6 col-lg-6 col-xl-6 mb-3 mb-md-0">
                <div className="additional-services booking-period">
                  <h5 className="mob-h5 mb-md-2 mb-0">
                    {strings.SitterDetails}
                    <Link
                      href={`/sitter-profile/${data.sitter?.id}?serviceId=1`}
                    >
                      <h5 className="text-muted cursor-pointer mt-2 px-2">
                        {data?.sitter
                          ? data.sitter?.firstname + " " + data.sitter?.lastname
                          : ""}
                      </h5>
                    </Link>
                  </h5>

                  <span className="font-14 mb-0 text-muted px-2">
                    {data?.sitter &&
                      `  ${
                        (data.sitter?.address
                          ? data.sitter?.address?.address
                          : "") +
                        " " +
                        (data.sitter?.address
                          ? data.sitter?.address?.city
                          : "") +
                        " " +
                        (data.sitter?.address
                          ? data.sitter?.address?.postcode
                          : "")
                      }`}
                  </span>
                </div>
              </div>
            </div>
            <hr />
            <div className="row">
              <div className="col-12 col-md-6 col-lg-6 col-xl-6 mb-3">
                <div className="additional-services booking-period">
                  <h5 className="mob-h5 mb-md-2 mb-0">
                    {strings.BookingPeriod}
                  </h5>
                  <div className="d-flex px-2">
                    <div className="booked-from my-1 mr-3 pr-3">
                      <h6 className="font-14 mb-0">{strings.From}</h6>
                      <p className="mb-0">
                        {TimeFormat(data?.drop_of_time_from)} |{" "}
                        {moment(new Date(data?.drop_of_date)).format(
                          "MM/DD/YYYY"
                        )}
                      </p>
                    </div>
                    <div className="booked-from my-1">
                      <h6 className="font-14 mb-0">{strings.To}</h6>
                      <p className="mb-0">
                        {TimeFormat(data?.pickup_up_time_to)} |{" "}
                        {moment(new Date(data?.pickup_up_date)).format(
                          "MM/DD/YYYY"
                        )}
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-md-6 col-lg-6 col-xl-6 mb-3">
                <div className="additional-services booking-period">
                  <h5 className="mob-h5 mb-md-2 mb-0">
                    {strings.Transportation}
                  </h5>
                  <div className="d-flex mb-2 px-2">
                    <div className="booked-from my-1 mr-3 pr-3">
                      <h5 className="font-14 mb-0">
                        {strings.SitterNeedPickup}:{" "}
                        <span className="text-muted">
                          {data.need_sitter_pickup === 1 ? "Yes" : "No"}
                        </span>
                      </h5>
                    </div>
                  </div>
                  {data.need_sitter_pickup === 1 && (
                    <span className="font-14 mb-0 text-muted">
                      {data?.sitter &&
                        `  ${
                          (data.sitter?.address
                            ? data.sitter?.address?.address
                            : "") +
                          " " +
                          (data.sitter?.address
                            ? data.sitter?.address?.city
                            : "") +
                          " " +
                          (data.sitter?.address
                            ? data.sitter?.address?.postcode
                            : "")
                        }`}
                    </span>
                  )}
                </div>
              </div>
            </div>
            <hr />

            {data.message && (
              <>
                <div className="row">
                  <div className="col-12 col-md-12 col-lg-12 col-xl-12 mb-3">
                    <div className="additional-services booking-period">
                      <h5 className="mob-h5 mb-md-2 mb-0">
                        {strings.Instructions}
                      </h5>
                      <div className="d-flex px-2">
                        <div className="booked-from my-1 mr-3 pr-3">
                          <p className="mb-0">{data.message}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr />
              </>
            )}

            <div className="row">
              <div className="col-6 col-md-6 col-lg-6 col-xl-6 mb-3">
                <div className="additional-services booking-period">
                  <h5 className="mob-h5 mb-2">Transaction Details</h5>
                  <div className="row px-2">
                    <div className="col-6">
                      <h6 className="booked-from my-1 mr-3 pr-3">
                        Service Fee
                      </h6>
                    </div>
                    <div className="booked-from my-1">
                      <h6 className="text-muted mb-0">
                        {" "}
                        {" " +
                          getCurrencySign() +
                          " " +
                          data?.petcation_fee_amount}
                      </h6>
                    </div>
                    <div className="col-6">
                      <h6 className="booked-from my-1 mr-3 pr-3">
                        Service fee
                      </h6>
                    </div>
                    <div className="booked-from my-1">
                      <h6 className="text-muted mb-0">
                        {" " + getCurrencySign() + " " + data?.service_discount}
                      </h6>
                    </div>
                    <div className="col-6">
                      <h6 className="booked-from my-1 mr-3 pr-3">
                        Transportation Fee{" "}
                      </h6>
                    </div>
                    <div className="booked-from my-1">
                      <h6 className="text-muted mb-0">
                        {" " + getCurrencySign() + " " + data?.amend_amount}
                      </h6>
                    </div>
                    <div className="col-6">
                      <h6 className="booked-from my-1 mr-3 pr-3">Discount</h6>
                    </div>
                    <div className="booked-from my-1">
                      <h6 className="text-muted mb-0">
                        {" " + getCurrencySign() + " " + data?.discount}
                      </h6>
                    </div>
                    {data?.coupon_code ? (
                      <>
                        <div className="col-6">
                          <h6 className="booked-from my-1 mr-3 pr-3">
                            Coupon Code
                          </h6>
                        </div>
                        <div className="booked-from my-1">
                          <h6 className="text-muted mb-0">
                            {" " + getCurrencySign() + " " + data?.coupon_code}
                          </h6>
                        </div>
                      </>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
            <hr />

            <div className="row">
              {userPets &&
                data.pets?.map((value, index) => (
                  <>
                    <div className="col-sm-6 p-1">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-4 col-md-4 col-lg-4 col-xl-4">
                              {value.status == 1 && (
                                <label className="radio-inline">
                                  <div className="search-sitter-img h-20 w-20">
                                    <img
                                      src={value.pet_image}
                                      className="img-fluid mb-2"
                                    />
                                  </div>
                                  <h6 className="font-14 mb-1 font-medium text-center">
                                    {`${value.pet_name}, ${
                                      value.age_year + strings.yrs
                                    }`}
                                    <br />{" "}
                                    {`${
                                      petSize.find(
                                        (val) => val.value == value.weight
                                      ).label
                                    }`}
                                  </h6>
                                  <p className="font-12 mb-0 text-center">
                                    {value.breed?.breed}
                                  </p>

                                  {!dateRangeIncludesToday(
                                    data?.drop_of_date,
                                    data?.pickup_up_date
                                  ) && (
                                    <div className="d-flex p-2 flex-column align-items-left justify-content-left">
                                      {props.asSitter && (
                                        <div className="mt-1">
                                          <button
                                            onClick={() => {
                                              setShowAddNotesModal(true);
                                              setSelectedPet(value);
                                            }}
                                            className="btn btn-sm text-dark border-dark btn-outline-light  h-20 w-100"
                                          >
                                            add notes
                                          </button>
                                        </div>
                                      )}
                                    </div>
                                  )}
                                </label>
                              )}
                            </div>
                            <div className="col-8 col-md-8 col-lg-8 col-xl-8 mb-3">
                              <h4 className="text-center">Addes Notes</h4>
                              <div
                                className="col-12 ml-2"
                                style={{ height: "160px", overflow: "auto" }}
                              >
                                <ul style={{ listStyle: "disc" }}>
                                  {data?.notes?.length > 0 &&
                                    data?.notes.map((note) => {
                                      return (
                                        <li className="p-2 mb-0">
                                          {note.message}
                                        </li>
                                      );
                                    })}
                                </ul>

                                {!data?.notes?.length && (
                                  <div className="d-flex col-12 p-3 align-items-center justify-content-center">
                                    <p className="p-2 mb-0">No notes found.</p>
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                ))}
            </div>
            <hr/>
            <div className="d-flex justify-content-between flex-row">
              <button
                style={{ zIndex: 1000 }}
                onClick={props.hideModal}
                className="btn float-end"
              >
                {strings.Close}
              </button>
            </div>
          </Modal.Body>
        )}
      </Modal>
      <AddNotesModal
        closeModal={() => {
          setShowAddNotesModal(false);
        }}
        showModal={showAddNotesModal}
        pet={selectedPet}
        loading={loading}
        reFetchData={reFetchData}
      />
      <ViewNotesModal
        closeModal={() => {
          setShowViewNotesModal(false);
        }}
        showModal={showViewNotesModal}
        notes={data.notes}
        pet={selectedPet}
        loading={loading}
        reFetchData={reFetchData}
      />
    </>
  );
}
