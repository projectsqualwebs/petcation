import React from "react";
import { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import API from "../../api/Api";
import {successOptions, errorOptions} from "../../public/appData/AppData";
import { useSnackbar } from 'react-simple-snackbar';
import {strings} from "../../public/lang/Strings";

const api = new API();
export default function RateSitter(props: any) {
    const [message, setMessage] = useState('');
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [openError, closeError] = useSnackbar(errorOptions);

    useEffect(()=> {
        setMessage('')
    }, [props.participantId]);

    const reportSitter = (id) => {
        api.reportSitter({ reported_sitter: id, message: message })
            .then((res) => {
                props.setReported()
                props.hideModal()
                openSuccess(strings.ReportedSuccessfully_E)
            })
            .catch((error)=> openError(error.response.data.message))
    }


    return (
        <Modal
            show={props.showModal}
            id="settings"
            className="modal-child"
            aria-labelledby="settings"
            tabIndex="-1"
            scrollable
        >
            <Modal.Header className="p-3">
                <Modal.Title as="h4" className="fw-medium">
                    {strings.areYouSure}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="p-3">
                <div className="col-12 form-details">
                    <h5>{strings.ReportSitter_Q} </h5>
                    <div className="weekly-discount mb-2 mt-3">
                        <h6 className="text-muted font-14 font-semibold">
                            {strings.EnterMessage}
                        </h6>
                        <textarea
                            className={"form-control"}
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                            rows={4}
                            defaultValue={""}
                        />
                    </div>

                </div>
                <hr />
                <div className="d-flex justify-content-between flex-row">
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={() => reportSitter(props.participantId)}
                        className="btn btn-primary float-end"
                    >
                        {strings.Report}
                    </button>
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={props.hideModal}
                        className="btn float-end"
                    >
                        {strings.Cancel}
                    </button>
                </div>
            </Modal.Body>
        </Modal>
    );
}
