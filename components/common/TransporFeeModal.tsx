import React, { useEffect, useState } from 'react';
import { Modal } from "react-bootstrap";
import { numberInput } from "../../utils/Helper";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import { D_TRANSPORTATION, D_TRANSPORTATION_PREFERENCE } from "../../public/appData/StaticData";
import ActionLoader from "./ActionLoader";

let api = new API();

export default function TransportFeeModal(props: any) {
  const [charges, setCharges] = useState<string>('');
  const [hasFee, setHasFee] = useState<0 | 1>(0);
  const [transportPreference, setTransportPreference] = useState<any>(D_TRANSPORTATION_PREFERENCE);
  const [error, setError] = useState(false)

  useEffect(() => {
    setHasFee(0)
    if (props.bookingServiceId && props.showModal) {
      switch (props.bookingServiceId) {
        case 1:
          api.getBoardingInfo(2)
              .then((response: any) => {
                let data = response.data.response;
                setTransportPreference(data.transport_preferences)
                if (data.transport_preferences.length !== 0) {
                  setHasFee(1)
                }
              })
              .catch((error: any) => console.log(error));
          break;
        case 2:
          api.getHouseSittingInfo(2)
              .then((response: any) => {
                let data = response.data.response;
                setTransportPreference(data.transport_preferences)
                if (data.transport_preferences.length !== 0) {
                  setHasFee(1)
                }
              })
              .catch((error: any) => console.log(error));
          break;
        case 3:
          api.getDropInVisitsInfo(2)
              .then((response: any) => {
                let data = response.data.response;
                setTransportPreference(data.transport_preferences)
                if (data.transport_preferences.length !== 0) {
                  setHasFee(1)
                }
              })
              .catch((error: any) => console.log(error));
          break;
        case 4:
          api.getPetDayCareInfo(2)
              .then((response: any) => {
                let data = response.data.response;
                setTransportPreference(data.transport_preferences)
                if (data.transport_preferences.length !== 0) {
                  setHasFee(1)
                }
              })
              .catch((error: any) => console.log(error));
          break;
        case 5:
          api.getDogWalkingInfo(2)
              .then((response: any) => {
                let data = response.data.response;
                setTransportPreference(data.transport_preferences)
                if (data.transport_preferences.length !== 0) {
                  setHasFee(1)
                }
              })
              .catch((error: any) => console.log(error));
          break;
        case 6:
          api.getGroomingInfo(2)
              .then((response: any) => {
                let data = response.data.response;
                setTransportPreference(data.transport_preferences)
                if (data.transport_preferences.length !== 0) {
                  setHasFee(1)
                }
              })
              .catch((error: any) => console.log(error));
          break;
        case 7:
          api.getHouseCallInfo(2)
              .then((response: any) => {
                let data = response.data.response;
                setTransportPreference(data.transport_preferences)
                if (data.transport_preferences.length !== 0) {
                  setHasFee(1)
                }
              })
              .catch((error: any) => console.log(error));
          break;
      }
    }


  }, [props.bookingServiceId, props.showModal]);

  return (
    <Modal
      show={props.showModal}
      id="settings"
      className="modal-child"
      aria-labelledby="settings"
      tabIndex="-1"
      scrollable
    >
      <Modal.Header className="p-3">
        <Modal.Title as="h5" className="fw-medium">
          {strings.AddTransportationFees}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-3">
        <div className="col-12 form-details">

          <div className="weekly-discount mb-3">
            <h6 className="text-muted font-14 font-semibold">
              {strings.EnterAmount}*
            </h6>
            <input
              className={
                `form-control mb-1 w-100 ${error ? 'invalid' : ''}`
              }
              id="weekly-discount"
              name="discount"
              type="number"
              placeholder={strings.EnterHere}
              maxLength={3}
              value={charges}
              onKeyPress={numberInput}
              onChange={(e) => {
                  setError(false);
                  setCharges(e.target.value)
              }}
            />
          </div>
          {hasFee ? <p className='mb-2'>{strings.YourTransportChargesFor}{props.bookingService}{" " + strings.service}.</p>
              : ""
          }
          {hasFee ? <div className="col-12 px-0">
              {transportPreference && transportPreference.map((val: any, index: number) => (val.status == 1 ?
                  <div key={`transport_${index}`} className="col-12 border border-grey rounded-5 d-flex align-items-center justify-content-between py-2 px-3 mb-2">
                      <p className="fw-bold m-0">
                          {D_TRANSPORTATION[index].label}
                      </p>
                      <p className="fw-bold m-0">{val.price_start_from}</p>
                  </div> : null))}
          </div> : null}
        </div>
        <hr />
        {props?.loading ? <ActionLoader/> : <div className="d-flex justify-content-between flex-row">
          <button
            style={{ zIndex: 1000 }}
            onClick={() => {
                if (!charges) {
                    setError(true);
                    return false
                }
                props.handleAddCharge(charges)
            }}
            className="btn btn-primary float-end"
          >
            {strings.AddAccept}
          </button>
          <button
            style={{ zIndex: 1000 }}
            onClick={props.hideModal}
            className="btn float-end"
          >
            {strings.Cancel}
          </button>
        </div>}
      </Modal.Body>
    </Modal>
  )
}
