import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import API from "../../api/Api";
import { strings } from "../../public/lang/Strings";
import { petSize } from "../../public/appData/AppData";
import moment from "moment";
import { getCurrencySign } from "../../api/Constants";
import Link from "next/link";
import { TimeFormat } from "../../utils/Helper";

export default function PaymentHistoryModel(props: any) {
  return (
    <Modal
      show={props.showModal}
      id="settings"
      className="modal-child"
      aria-labelledby="contained-modal-title-vcenter"
      tabIndex="-1"
      size={"xl"}
      centered
      scrollable
    >
      <Modal.Header className="p-3">
        <Modal.Title as="h4" className="fw-medium">
          {strings.TransactionPDF}
        </Modal.Title>

        <button
          type="button"
          className="close"
          data-dismiss="modal"
          aria-label="Close"
          onClick={() => props.hideModal()}
        >
          <span aria-hidden="true" className="">
            ×
          </span>
        </button>
      </Modal.Header>
      <Modal.Body className="p-3">
        <div className="row">
          <div className="col-12 col-md-12 col-lg-12 col-xl-12 mb-3 my-3">
            {props?.data?.want_to_receive_media === 1 ? (
              <div className="logo d-flex justify-content-end">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-cloud-check"
                  viewBox="0 0 16 16"
                >
                  <path
                    fill-rule="evenodd"
                    d="M10.354 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"
                  />
                  <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383zm.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z" />
                </svg>
                <small className="">&nbsp;{strings.Petownerrequestedforvideosandphotosofpet}</small>
              </div>
            ) : (
              ""
            )}

            <h5 className="mob-h5 mb-2">{strings.PetsDetails}</h5>
            <div className="question-details">
              <div className="dog box" style={{ display: "block" }}>
                <ul className="chec-radio mt-1">
                  {props?.data?.pets?.map((value, index) => (
                    <>
                      <li key={index} className="pet-select">
                        <label className="radio-inline">
                          <input
                            type="checkbox"
                            key={`pet${index}`}
                            id={`pet${index}`}
                            name="select1"
                            disabled={true}
                            className="pro-chx"
                            defaultValue="yes"
                          />
                          <div className="select-radio text-center">
                            <div>
                              <div className="search-sitter-img">
                                <img
                                  src={value.pet_image}
                                  className="img-fluid mb-2"
                                />
                              </div>
                              <h6 className="font-14 mb-1 font-medium">
                                {`${value.pet_name} ${
                                  value.age_year + strings.yrs
                                }`}{" "}
                                <br />{" "}
                                {`${
                                  petSize.find(
                                    (val) => val.value == value.weight
                                  ).label
                                }`}
                              </h6>
                              <p className="font-12 mb-0">
                                {value?.breed?.breed}
                              </p>
                            </div>
                          </div>
                        </label>
                      </li>
                    </>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-12 col-sm-6 mb-2 mb-md-0">
            <div className="db-service">
              <h5 className="font-20 mb-1">{strings.ReservationDetails}</h5>

              <div className="d-flex align-items-center justify-content-between col-10 mb-2">
                <h6 className="me-3 mb-0">{strings.service} : </h6>
                <p className="font-14 mb-0 text-muted">
                  {props?.data?.service?.name}
                </p>
              </div>
              <div className="d-flex align-items-center justify-content-between col-10 mb-2">
                <h6 className="me-3 mb-0">Booking date range : </h6>
                <p className="font-14 mb-0 text-muted">
                  {strings.From + " "}
                  {moment(new Date(props?.data?.drop_of_date)).format(
                    "MM/DD/YYYY"
                  )}{" "}
                  &nbsp;{strings.To + " "}{" "}
                  {moment(new Date(props?.data?.pickup_up_date)).format(
                    "MM/DD/YYYY"
                  )}
                </p>
              </div>
              <div className="d-flex align-items-center justify-content-between col-10 mb-2">
                <h6 className="me-3 mb-0 ">Check-in time : </h6>
                <p className="font-14 mb-0  text-muted">
                  {strings.From + " "}
                  {moment(props?.data?.drop_of_time_from, "hh:mm:ss").format(
                    "hh:mm A"
                  )}{" "}
                  &nbsp;{strings.To + " "}{" "}
                  {moment(props?.data?.drop_of_time_to, "hh:mm:ss").format(
                    "hh:mm A"
                  )}
                </p>
              </div>
              <div className="d-flex align-items-center justify-content-between col-10 mb-2">
                <h6 className="me-3 mb-0">Check-out time : </h6>
                <p className="font-14 mb-0 text-muted">
                  {strings.From + " "}
                  {moment(props?.data?.pickup_up_time_from, "hh:mm:ss").format(
                    "hh:mm A"
                  )}{" "}
                  &nbsp;{strings.To + " "}{" "}
                  {moment(props?.data?.pickup_up_time_to, "hh:mm:ss").format(
                    "hh:mm A"
                  )}
                </p>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6" py-md-1>
            <div className="d-flex align-items-center justify-content-md-end service-pending">
              <div className="my-auto mr-md-3 mr-2">
                <p className="mb-0 font-12 text-muted">
                  {strings.PostedOn}
                  {moment(props?.data?.createdAt).format("MM/DD/YYYY")}
                </p>
              </div>
              <div>
                {props?.data?.status == 0 ? (
                  <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                    {strings.Pending}
                  </button>
                ) : null}
                {props?.data?.status == 1 ? (
                  <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                    {strings.Unpaid}
                  </button>
                ) : null}
                {props?.data?.status == 3 ? (
                  <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                    {strings.Paid}
                  </button>
                ) : null}
                {props?.data?.status == 4 ? (
                  <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                    {strings.completed}
                  </button>
                ) : null}
                {props?.data?.status == 2 ? (
                  <button className="btn py-0 py-md-1 btn-yellow radius mt-0">
                    {strings.Rejected}
                  </button>
                ) : null}
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-12 col-md-8 col-lg-8 col-xl-8 mb-3 my-3">
            <h5 className="mob-h5 mb-2">Payment Details</h5>
            <div className="row px-2">
              <div className="col-6">
                <h6 className="booked-from my-1 mr-3 pr-3">Service fee</h6>
              </div>
              <div className="booked-from my-1">
                <h6 className="text-muted mb-0">
                  {" " +
                    getCurrencySign() +
                    " " +
                    props?.data?.petcation_fee_amount}
                </h6>
              </div>
              <div className="col-6">
                <h6 className="booked-from my-1 mr-3 pr-3">Service discount</h6>
              </div>
              <div className="booked-from my-1">
                <h6 className="text-muted mb-0">
                  {" " +
                    getCurrencySign() +
                    " " +
                    props?.data?.service_discount}
                </h6>
              </div>
              <div className="col-6">
                <h6 className="booked-from my-1 mr-3 pr-3">
                  Transportation fee
                </h6>
              </div>
              <div className="booked-from my-1">
                <h6 className="text-muted mb-0">
                  {" " + getCurrencySign() + " " + props?.data?.amend_amount}
                </h6>
              </div>
              {props?.data?.coupon_code ? (
                <>
                  <div className="col-6">
                    <h6 className="booked-from my-1 mr-3 pr-3">Discount</h6>
                  </div>
                  <div className="booked-from my-1">
                    <h6 className="text-muted mb-0">
                      {" " + getCurrencySign() + " " + props?.data?.discount}
                    </h6>
                  </div>
                </>
              ) : null}
            </div>
          </div>
          {props?.data?.additional_services.length > 0 ? (
            <div className="col-12 col-md-6 col-lg-6 col-xl-6">
              {props?.data?.additional_services.length > 0 ? (
                <h5 className="mob-h5 mb-2 my-2">
                  {strings.additionalService}
                </h5>
              ) : (
                " "
              )}

              <div className="question-details">
                {props?.data?.additional_services.map((val, index) => (
                  <div key={index} className="custom-check">
                    <ul>
                      <li>
                        {val.name} | {getCurrencySign() + " " + val.price}
                      </li>
                    </ul>
                  </div>
                ))}
              </div>
            </div>
          ) : (
            ""
          )}
        </div>

        <hr />
        {/*<div>*/}
        {/*    <h5 className="font-semibold mb-3">*/}
        {/*        {strings.Message}*/}
        {/*    </h5>*/}
        {/*    <p>{props?.data?.?.message ? props?.data?.?.message : 'Message need from BE'}</p>*/}
        {/*</div>*/}
        {/*<hr />*/}
        <div className="d-flex justify-content-between flex-row">
          <button
            style={{ zIndex: 1000 }}
            onClick={() => props.hideModal()}
            className="btn float-end"
          >
            {strings.Close}
          </button>
        </div>
      </Modal.Body>
    </Modal>
  );
}
