import { Modal } from "react-bootstrap";
import React, { useState } from "react";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import {strings} from "../../public/lang/Strings";

const UploadFileModal = (props) => {
  const [cropper, setCropper] = useState<any>();

  const getCropData = (e) => {
    e.preventDefault();
    if (typeof cropper !== "undefined") {
      props.hideModal();
      props.setImage(cropper.getCroppedCanvas().toDataURL());
    }
  };

  return (
    <>
      <Modal
        show={props.showModal}
        id="settings"
        className="modal-child"
        aria-labelledby="settings"
        tabIndex="-1"
        scrollable
      >
        <Modal.Header className="p-3">
          <Modal.Title as="h5" className="fw-medium">
            {strings.CropImage}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="p-3">
          <hr />
          <div className="col-12 form-details">
            <Cropper
              src={props.path}
              onInitialized={(instance: any) => {
                setCropper(instance);
              }}
              zoomable={false}
              aspectRatio={props.aspectRatio ? props.aspectRatio : 1}
              preview=".img-preview"
              guides={false}
              viewMode={1}
              dragMode="move"
              cropBoxMovable={true}
            />
          </div>
          <hr />
          <div className="d-flex justify-content-between flex-row">
            <button
              style={{ zIndex: 1000 }}
              onClick={getCropData}
              className="btn btn-primary float-end"
            >
              {strings.Crop}
            </button>
            <button
              style={{ zIndex: 1000 }}
              onClick={() => props.hideModal()}
              className="btn float-end p-0"
            >
              {strings.Cancel}
            </button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default UploadFileModal;
