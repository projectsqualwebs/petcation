import React, {useEffect, useState} from "react";
import {Modal} from "react-bootstrap";
import 'rc-slider/assets/index.css';
import API from "../../api/Api";
import Cropper from "react-cropper";
import {errorOptions, successOptions} from "../../public/appData/AppData";
import {useSnackbar} from "react-simple-snackbar";
import {strings} from "../../public/lang/Strings";

interface I_PROPS {
    showModal: boolean;
    hideModal: () => void;
    updateCoupon: () => void;
}

/**
 *
 * @param props contains sitter data
 * @function contains view of our.
 */
export default function AddCouponModal({updateCoupon, showModal, hideModal}: I_PROPS) {
    const api = new API()
    const [code, setCode] = useState<string>()
    const [err, setErr] = useState<string>('')
    const [openSuccess, closeSuccess] = useSnackbar(successOptions);
    const [openError, closeError] = useSnackbar(errorOptions);

    /**
     * @function to.
     */
    const handleSubmit = () => {
        if (!code) {
            setErr(strings.PleaseEnterCouponCode);
            return false
        }
        api.addCoupon({coupon_code: code})
            .then(res => {
                setCode('');
                setErr('');
                updateCoupon();
                openSuccess(strings.CouponAddedSuccessfully)
                hideModal();
            })
            .catch(err => {
                if (err.response.data?.errors?.coupon_code){
                    setErr(err.response.data?.errors?.coupon_code[0])
//                     openError(err.response.data?.errors?.coupon_code[0])
                }else {
                    setErr(err.response.data.message)
                    openError(err.response.data.message)
                }

            })
    };

    return (
            <Modal
                show={showModal}
                className="modal-child"
                scrollable
            >
                <Modal.Header className="p-3">
                    <Modal.Title as={"h5"} className="fw-medium">
                        {strings.AddCoupon}
                    </Modal.Title>
                    <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={() => {
                            setCode('');
                            setErr('');
                            hideModal()
                        }}
                        >
                        <span aria-hidden="true">×</span>
                    </button>
                </Modal.Header>
                <Modal.Body className="p-3">
                    <h6>{strings.CouponCode}</h6>
                    <div className="row mb-1">
                        <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                            <input
                                className={`form-control ${err !== '' ? "invalid" : ''}`}
                                placeholder={strings.EnterYourCouponCode}
                                id="coupon1"
                                value={code}
                                onChange={(e) => {
                                    setErr('');
                                    setCode(e.target.value)
                                }}
                            />
                            {err ? <label><span className="sign text-danger">{err}</span></label> : null}
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <div className="d-flex justify-content-between flex-row">
                        <button
                            type='button'
                            onClick={() => handleSubmit()}
                            className="btn btn-primary">
                            {strings.Submit}
                        </button>
                    </div>
                </Modal.Footer>
            </Modal>
    );
}
