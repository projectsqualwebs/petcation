import { Modal } from "react-bootstrap";
import React, { useEffect } from "react";
import ReactStars from "react-rating-stars-component";
import { useState } from "react";
import { useSnackbar } from "react-simple-snackbar";
import API from "../../api/Api";
import Cookies from "universal-cookie";
import { errorOptions, successOptions } from "../../public/appData/AppData";
import { strings } from "../../public/lang/Strings";
const cookie = new Cookies();

interface I_PROPS {
  showModal: boolean;
  hideModal: () => void;
  reRender: (listId) => void;
  data?: {
    label: string;
    key: number;
    value: number;
  };
  isSitterWishlist?: boolean;
  setData?: () => void;
  isNull?: boolean;
}

const api = new API();
/**
 *
 * @param props contains functions and values object to get data
 * @function contains modal of spot rating
 */
export default function CreateWishlist(props: I_PROPS) {
  const [error, setError] = useState<boolean>(false);
  const [name, setName] = useState<string>("");
  const [listId, setListId] = useState<number>();
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);

  useEffect(() => {
    if (props.data) {
      setName(props?.data?.label);
      setListId(props?.data?.value);
    } else {
      setName("");
      setListId(null);
    }
  }, [props]);

  /**
   *
   * @param e contains value of input field.
   * @function will set data in state and set error false if error is true.
   */
  const onChange = (e) => {
    e.preventDefault();
    setName(e.target.value);
    console.log(e.target.value);
    setError(false);
  };

  function handleSubmit() {
    if (name == "") {
      setError(true);
      return false;
    }
    console.log(name);
    if (props?.isSitterWishlist) {
      api
        .createNewSitterWishlist({ name: name })
        .then((res) => {
          openSuccess(res.data.message);
          setName("");
          props.reRender(null);
          props.hideModal();
        })
        .catch((err) => openError(err.response?.data?.message));
    } else {
      api
        .createNewWishlist({ name: name })
        .then((res) => {
          openSuccess(res.data.message);
          setName("");
          props.reRender(null);
          props.hideModal();
        })
        .catch((err) => openError(err.response?.data?.message));
    }
  }
  if (name === "") {
    // document.addEventListener("keydown", function (event) {
    //   if (event.key === "Enter") {
    //     if (name === "") {
    //       event.preventDefault();
    //     }
    //   }
    // });
  }

  function handleEdit() {
    if (name == "") {
      setError(true);
      return false;
    }
    console.log(name);
    if (props?.isSitterWishlist) {
      api
        .updateSitterWishlist(listId, { name: name })
        .then((res) => {
          openSuccess(res.data.message);
          props.setData();
          setName("");
          props.reRender(listId);
          props.hideModal();
        })
        .catch((err) => openError(err.response?.data?.message));
    } else {
      api
        .updateWishlist(listId, { name: name })
        .then((res) => {
          openSuccess(res.data.message);
          props.setData();
          setName("");
          props.reRender(listId);
          props.hideModal();
        })
        .catch((err) => openError(err.response?.data?.message));
    }
  }

  return (
    <Modal
      show={props.showModal}
      dialogClassName="modal-dialog"
      aria-labelledby="example-custom-modal-styling-title">
      <div role="document">
        <div className="modal-content">
          <div className="modal-body">
            <div className="meetup-details">
              <div className="d-flex justify-content-between mb-2">
                <h5>
                  {props.data ? strings.Edit : strings.Create}
                  {" " + strings.Wishlist}
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={() => props.hideModal()}>
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="row">
                  <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="input-group mb-0">
                      <input
                        aria-label="Write a note"
                        aria-describedby="basic-addon2"
                        className={`form-control ${error ? "invalid" : ""}`}
                        id="name"
                        type="text"
                        name="name"
                        placeholder={strings.EnterName}
                        onChange={onChange}
                        value={name}
                      />
                      <div className="input-group-append">
                        {props.data ? (
                          <button
                            type="button"
                            data-dismiss="modal"
                            onClick={() => handleEdit()}
                            className="btn btn-primary confirm-btn mt-0">
                            {strings.Edit}
                          </button>
                        ) : (
                          <button
                            type="button"
                            data-dismiss="modal"
                            onClick={() => handleSubmit()}
                            className="btn btn-primary confirm-btn mt-0">
                            {strings.Submit}
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                </div>

            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
}
