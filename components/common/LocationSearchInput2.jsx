import React, {useEffect} from 'react';
import { useState } from "react";
import { Input, List } from "antd";

import useGoogle from "react-google-autocomplete/lib/usePlacesAutocompleteService";
import {getLatLng} from "react-places-autocomplete";

const LocationSearchInput2 = ({setFilter, setLatLng, value, setValue, className, key}) => {
    const {
        placesService,
        placePredictions,
        getPlacePredictions,
        isPlacePredictionsLoading,
    } = useGoogle({
        apiKey: process.env.NEXT_PUBLIC_GOOGLE_PLACES_API,
        language: "en",
    });
    const [lValue, setLValue] = useState('')
    useEffect(() => {
        setLValue(value)
    }, [value])

    return (
        <div key={key} className='col mx-0 px-0'>
            <input
                className={className}
                style={{ color: "black", width: '100%', height: "42px" }}
                value={lValue}
                placeholder="Enter Location"
                onChange={(evt) => {
                    getPlacePredictions({ input: evt.target.value, componentRestrictions: {country: ['jp']} });
                    setLValue(evt.target.value);
                    if (evt.target.value === '') {
                        setValue('')
                    }
                }}
            />
            <div style={{ zIndex: 1, display: placePredictions.length ? "block" : 'none', position: "absolute", width: '100%', paddingRight: "1rem"}}>
                {!isPlacePredictionsLoading && (
                    <List
                        className='bg-white'
                        dataSource={placePredictions}
                        options={['location']}
                        renderItem={(item) => {
                            return(
                            <List.Item onClick={() => {
                                placesService.getDetails(item, async (val) => {
                                    await setValue(val.formatted_address);
                                    await getLatLng(val).then(latLng => {
                                        setLatLng(latLng)
                                    });
                                    const postalCode = await val.address_components.find(c => c.types.includes('postal_code')) || {long_name: null};
                                    const locality = await val.address_components.find(c => c.types.includes('locality')) || {long_name: null};
                                    const city = await val.address_components.find(c => c.types[0] === 'locality' ) || {long_name: null};
                                    const prefecture = await val.address_components.find(c => c.types.includes('administrative_area_level_1')) || { long_name: null };
                                    // console.log("address", {formatted_address: val.formatted_address, locality: locality.long_name, postalCode: postalCode.long_name, city: city.long_name})
                                    let address = {
                                        value: val.formatted_address,
                                        locality: locality.long_name ? locality.long_name : '',
                                        postalCode: postalCode.long_name ? postalCode.long_name : '',
                                        city: city?.long_name ? city?.long_name : '',
                                        prefecture: prefecture.long_name ? prefecture.long_name : ''
                                    }
                                    await setLValue(val.formatted_address)
                                    await setFilter(address)
                                    await getPlacePredictions({ input: '', componentRestrictions: {country: ['jp']} });
                                })
                            }}>
                                <List.Item.Meta title={item.description} />
                            </List.Item>
                        )}}
                    />
                )}
            </div>
        </div>
    );
};
export default LocationSearchInput2;