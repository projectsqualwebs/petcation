import React from "react";
import { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import API from "../../api/Api";
import {strings} from "../../public/lang/Strings";
import Dropzone from 'react-dropzone'


const api = new API();
export default function ReactMultiImageUploadModal(props: any) {
  const [errorFiles, setErrorFiles] = useState([]);
  const [successFiles, setSuccessFiles] = useState([]);
  const onSuccess = (acceptedFiles) => {
    let fileSizeMoreThan10 = acceptedFiles.filter(file => file.size > 10e6)
    if (fileSizeMoreThan10.length) {
      setSuccessFiles(acceptedFiles.filter(file => file.size < 10e6))
      setErrorFiles(acceptedFiles.filter(file => file.size > 10e6))
    } else {
      props.onUpload(acceptedFiles)
    }
  }
  return (
      <Modal
        show={props.showModal}
        dialogClassName="modal-dialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <div role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{strings.UploadImages}</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => props.hideModal()}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>

            <div className="modal-body">
              <Dropzone accept={{['image/*']: ['.jpeg', '.png', '.jpg', '.webp']}} onDrop={acceptedFiles => onSuccess(acceptedFiles)}>
                {({getRootProps, getInputProps}) => (
                    <section className="cursor-pointer">
                      <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <div className="upload-doc profile-doc">
                          <div className="file">
                            <div>
                              <div>
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="far"
                                    data-icon="arrow-to-top"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 384 512"
                                    className="svg-inline--fa fa-arrow-to-top fa-w-12 fa-2x"
                                >
                                  <path
                                      fill="currentColor"
                                      d="M35.5 279.9l148-148.4c4.7-4.7 12.3-4.7 17 0l148 148.4c4.7 4.7 4.7 12.3 0 17l-19.6 19.6c-4.8 4.8-12.5 4.7-17.1-.2L218 219.2V468c0 6.6-5.4 12-12 12h-28c-6.6 0-12-5.4-12-12V219.2l-93.7 97.1c-4.7 4.8-12.4 4.9-17.1.2l-19.6-19.6c-4.8-4.7-4.8-12.3-.1-17zM12 84h360c6.6 0 12-5.4 12-12V44c0-6.6-5.4-12-12-12H12C5.4 32 0 37.4 0 44v28c0 6.6 5.4 12 12 12z"
                                  />
                                </svg>
                              </div>
                              {strings.clickToUpload} {strings.dragFilesHere}
                              <br />
                              {strings.acceptedImage}
                              <br />
                              {strings.fileShouldBeLessThan10MB}
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                )}
              </Dropzone>
            </div>
            {errorFiles?.length ? <>
              <small className='text-danger text-center w-100 my-2'>*{errorFiles.map((file, index) => file?.name + (index+1 === errorFiles?.length ? ' ' : ', ') ) + ' ' + strings.areMoreThan10MB}</small>
              <button className='btn btn-primary' onClick={() => {
                props.onUpload(successFiles)
                setErrorFiles([])
                setSuccessFiles([])
              }}>{strings.SkipThisFilesAndContinue}</button>
                </> : null}
          </div>
        </div>
      </Modal>
  );
}
