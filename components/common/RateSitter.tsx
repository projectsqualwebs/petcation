import React, {useEffect} from "react";
import { useState } from "react";
import { Modal } from "react-bootstrap";
import RatingStars from "./RatingStars";
import API from "../../api/Api";
import ReactStars from "react-rating-stars-component";
import {strings} from "../../public/lang/Strings";
import ActionLoader from "./ActionLoader";

interface I_PROPS {
  bookingId: number;
  sitterId: number;
  showModal: boolean;
  hideModal: () => void;
  getThreadBooking: () => void;
  myReview: I_RATING[];
}

interface I_RATING {
  pet_booking_id: number;
  sitter_id: number;
  cleanliness: number;
  communication: number;
  check_in: number;
  accuracy: number;
  location: number;
  value: number;
  review: string;
}

const api = new API();
/**
 *
 * @param props contains sitter data to get review and rating or their particular identity
 * @function contains view of our sitter rating model.
 */
export default function RateSitter(props: I_PROPS) {
  const [rating, setRating] = useState<I_RATING>({
    pet_booking_id: null,
    sitter_id: null,
    cleanliness: 0,
    communication: 0,
    check_in: 0,
    accuracy: 0,
    location: 0,
    value: 0,
    review: "",
  });
  const [loading, setLoading] = useState(false);

  useEffect(()=> {
    if(props.myReview && props.myReview.length) {
      setRating({
        pet_booking_id: props.bookingId,
        sitter_id: props.sitterId,
        cleanliness: props.myReview[0].cleanliness,
        communication: props.myReview[0].communication,
        check_in: props.myReview[0].check_in,
        accuracy: props.myReview[0].accuracy,
        location: props.myReview[0].location,
        value: props.myReview[0].value,
        review: props.myReview[0].review,
      })
    }else {
      setRating({
        pet_booking_id: props.bookingId,
        sitter_id: props.sitterId,
        cleanliness: 0,
        communication: 0,
        check_in: 0,
        accuracy: 0,
        location: 0,
        value: 0,
        review: "",
      })
    }


  },[props])

  /**
   * @function to give review or rating for sitter according to his job done.
   */
  const handleRating = () => {
    setLoading(true);
    api
      .rateSitter(rating)
      .then((res) => {
        if(res.data.status == 200) {
          props.getThreadBooking();
          props.hideModal();
          setLoading(false);
        }
      })
      .catch((error) => { setLoading(false) });
  };

  return (
    <Modal show={props.showModal}>
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">{strings.MyReview}</h5>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
            onClick={() => props.hideModal()}
          >
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="modal-body">
          <div className="confirm-details">
            <div className="accept-details">
              <div className="p-0 accept-booking">
                <div className="ratings-score">
                  <h6 className="mb-3">{strings.ProvideRatings}</h6>
                  <div className="row">
                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                      <div className="score score1 px-3 pt-3 pb-1 border mb-3">
                        <div className="score-details">
                          <h6 className="mb-0">
                            {strings.Cleanliness}{' '}
                            <span>{`${rating.cleanliness} / 5`}</span>
                          </h6>
                        </div>
                        <ReactStars
                          count={5}
                          onChange={(newRating) => setRating({ ...rating, cleanliness: newRating })}
                          size={25}
                          activeColor="#20847e"
                          value={rating.cleanliness}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                      <div className="score score1 px-3 pt-3 pb-1 border mb-3">
                        <div className="score-details">
                          <h6 className="mb-0">
                            {strings.Accuracy}{' '}
                            <span>{`${rating.accuracy} / 5`}</span>
                          </h6>
                        </div>
                        <ReactStars
                          count={5}
                          onChange={(newRating) => setRating({ ...rating, accuracy: newRating })}
                          size={25}
                          activeColor="#20847e"
                          value={rating.accuracy}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                      <div className="score score1 px-3 pt-3 pb-1 border mb-3">
                        <div className="score-details">
                          <h6 className="mb-0">
                            {strings.Communication}{' '}
                            <span>{`${rating.communication} / 5`}</span>
                          </h6>
                        </div>
                        <ReactStars
                          count={5}
                          onChange={(newRating) => setRating({ ...rating, communication: newRating })}
                          size={25}
                          activeColor="#20847e"
                          value={rating.communication}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                      <div className="score score1 px-3 pt-3 pb-1 border mb-3">
                        <div className="score-details">
                          <h6 className="mb-0">
                            {strings.Location}{' '} <span>{`${rating.location} / 5`}</span>
                          </h6>
                        </div>
                        <ReactStars
                          count={5}
                          onChange={(newRating) => setRating({ ...rating, location: newRating })}
                          size={25}
                          activeColor="#20847e"
                          value={rating.location}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                      <div className="score score1 px-3 pt-3 pb-1 border mb-3">
                        <div className="score-details">
                          <h6 className="mb-0">
                            {strings.Check_in}{' '} <span>{`${rating.check_in} / 5`}</span>
                          </h6>
                        </div>
                        <ReactStars
                          count={5}
                          onChange={(newRating) => setRating({ ...rating, check_in: newRating })}
                          size={25}
                          activeColor="#20847e"
                          value={rating.check_in}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                      <div className="score score1 px-3 pt-3 pb-1 border mb-3">
                        <div className="score-details">
                          <h6 className="mb-0">
                            {strings.Value}{' '}<span>{`${rating.value} / 5`}</span>
                          </h6>
                        </div>
                        <ReactStars
                          count={5}
                          onChange={(newRating) => setRating({ ...rating, value: newRating })}
                          size={25}
                          activeColor="#20847e"
                          value={rating.value}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="provide-review mt-0">
              <p className="mb-1">Explain your experience<small className="text-danger">*</small></p>
              <textarea
                className="form-control"
                placeholder="write your review here"
                rows={4}
                value={rating.review}
                onChange={(e) => { setRating({ ...rating, review: e.target.value }) }}
                required
              />
            </div>
            <hr />
            {loading ? <ActionLoader/> : <div className="row">
              <div className="col-6 col-md-6 col-lg-6 col-xl-6 my-auto">
                <div className="cancel-details" data-dismiss="modal">
                  <a type="button" onClick={() => props.hideModal()}>{strings.Cancel}</a>
                </div>
              </div>
              <div className="col-6 col-md-6 col-lg-6 col-xl-6 alignment">
                <button onClick={() => handleRating()} className="btn btn-primary px-3 py-2">{strings.Submit}</button>
              </div>
            </div>}
          </div>
        </div>
      </div>
    </Modal>
  );
}
