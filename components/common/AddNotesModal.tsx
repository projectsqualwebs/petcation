import React, { useEffect, useState } from 'react';
import { Modal } from "react-bootstrap";
import { numberInput } from "../../utils/Helper";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import { D_TRANSPORTATION, D_TRANSPORTATION_PREFERENCE } from "../../public/appData/StaticData";
import ActionLoader from "./ActionLoader";

let api = new API();

export default function AddNotesModal({pet, showModal, closeModal, loading, reFetchData}) {
    const [notes, setNotes] = useState<string>('');
    const [message, setMessage] = useState<string>('');
    const [error, setError] = useState({
        message: ""
    });

    const handleSubmit = () =>{
        const data = {
            "booking_id": pet.pet_booking_id,
            "pet_id": pet.id,
            "message": notes
        };

        if(!data.message) {
            setError({message : "Please add notes"});
            return false
        }

        api.addBookingNotes(data).then(res => {
            if(res.data.response) {
                reFetchData();
                setError({message : null});
                setNotes("");
                setMessage(res.data.message);
                setTimeout(()=>{
                    closeModal();
                },1000);
            }
        })
    };

    return (
        <Modal
            show={showModal}
            id="settings"
            className="modal-child"
            aria-labelledby="settings"
            tabIndex="-1"
            scrollable
        >
            <Modal.Header className="p-3">
                <Modal.Title as="h5" className="fw-medium">
                    {strings.AddNotes}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="p-3">
                <div className="col-12 form-details">
                    <div className="weekly-discount mb-3">
                        <h6 className="text-muted font-14 font-semibold">
                            {strings.AddNotes}*
                        </h6>
                        <input
                            className={
                                `form-control mb-1 w-100 ${error.message ? 'invalid' : ''}`
                            }
                            id="weekly-discount"
                            name="discount"
                            type="textarea"
                            placeholder={strings.EnterHere}
                            maxLength={250}
                            value={notes}
                            onChange={(e) => {
                                setError({message : null});
                                setNotes(e.target.value)
                            }}
                        />
                        {error.message &&
                        <span className="text-danger p-2">{error.message}
                        </span>}
                    </div>

                </div>
                <hr />
                {loading ? <ActionLoader/> : <div className="d-flex justify-content-between flex-row">
                    <button
                        style={{ zIndex: 1000 }}
                        className="btn btn-primary float-end"
                        onClick={()=>{
                            handleSubmit();
                        }}
                    >
                        {strings.AddNotes}
                    </button>
                    {message &&
                    <span className="text-success p-2">{message}
                        </span>}
                    <button
                        style={{ zIndex: 1000 }}
                        onClick={()=>{
                            closeModal();
                            setError({message : null});
                            setNotes("");
                        }}
                        className="btn float-end"
                    >
                        {strings.Cancel}
                    </button>
                </div>}
            </Modal.Body>
        </Modal>
    )
}
