import { Modal } from "react-bootstrap";
import React, { useEffect } from "react";
import ReactStars from "react-rating-stars-component";
import { useState } from "react";
import API from "../../api/Api";
import Cookies from "universal-cookie";
import {strings} from "../../public/lang/Strings";
import ActionLoader from "./ActionLoader";

const cookie = new Cookies();
const api = new API();
/**
 *
 * @param props contains rating object to get data
 * @function contains modal of spot rating
 */
export default function OwnerRating(props) {
    const [starKeyForce, setStarKeyForce] = useState(0);
    const [loading, setLoading] = useState(false);

    const [data, setData] = useState({
        review: '',
        rating: 0,
        pet_booking_id: null,
        owner_id: null
    })
    const [errors, setErrors] = useState({
        rating: false,
        review: false
    });

    useEffect(()=> {
        if(props.myReview && props.myReview.length) {
            setData({
                pet_booking_id: props.bookingId,
                owner_id: props.userId,
                rating: props.myReview[0].rating,
                review: props.myReview[0].review,
            })
        }else {
            setData({
                pet_booking_id: props.bookingId,
                owner_id: props.userId,
                rating: 0,
                review: "",
            })
        }


    },[props])


    useEffect(() => {
        setStarKeyForce(prev => prev + 1)
    }, [data.rating])

    /**
     * function to set spot rating in state.
     * @newRating contains count of selected rating stars.
     */
    const ratingChanged = (newRating) => {
        setData({ ...data, rating: newRating });
        let error = {...errors};
        error.rating = false;
        setErrors(error)
    };


    /**
     * @function to give review or rating for sitter according to his job done.
     */
    const handleRating = () => {
        let error = {...errors};
        if (data.rating == 0) {
            error.rating = true;
            setErrors(error)
            return false
        }
        if (!data.review) {
            error.review = true;
            setErrors(error)
            return false
        }
        setLoading(true);
        api
            .rateOwner(data)
            .then((res) => {
                setLoading(false)
                if(res.data.status == 200) {
                    props.getThreadBooking();
                    props.onHide();
                }
            })
            .catch((error) => { setLoading(false) });
    };

    /**
     *
     * @param e contains value of input field.
     * @function will set data in state and set error false if error is true.
     */
    const onChange = (e) => {
        setData({ ...data, review: e.target.value })
        let error = {...errors};
        error.review = false;
        setErrors(error)
    }

    return (
        <Modal
            show={props.showModal}
            dialogClassName="modal-dialog"
            aria-labelledby="example-custom-modal-styling-title"
        >
            <div role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{!props.myReview ? strings.EditRating : strings.ProvideRating}</h5>
                        <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-label="Close"
                            onClick={() => props.onHide()}
                        >
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="meetup-details">
                            <form>
                                <div className="row">
                                    <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                        <fieldset className="d-flex justify-content-center">
                                            <ReactStars
                                                count={5}
                                                onChange={ratingChanged}
                                                size={44}
                                                edit={true}
                                                activeColor="#20847e"
                                                value={data.rating}
                                                key={starKeyForce}
                                            />
                                        </fieldset>
                                            {errors.rating ? <small className='red text-center d-block mb-1'>*{strings.PleaseSelectStar}</small> : null}
                                        <textarea
                                            className={`form-control ${errors.review ? "invalid" : ''}`}
                                            placeholder={strings.Message}
                                            rows={4}
                                            value={data.review}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {loading ? <ActionLoader/> : <div className="text-center mt-3">
                                    <button
                                        onClick={() => handleRating()}
                                        type='button'
                                        className="btn btn-primary"
                                        data-dismiss="modal">
                                        {strings.RateNow} 
                                    </button>
                                </div>}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    );
}
