import React, { useRef } from "react";
import { useEffect } from "react";
import db from "../../service/Firebase";
import { ref, onValue } from "firebase/database";
import { useState } from "react";
import {
  I_CHAT_MESSAGE,
  I_CHAT_PARTICIPANTS,
} from "../../models/chat.interface";
import Cookies from "universal-cookie";
import API from "../../api/Api";
import moment from "moment";
import Modal from "react-modal";
import "react-day-picker/lib/style.css";
import DayPickerInput from "react-day-picker/DayPickerInput";
import parseDate from "react-day-picker/moment";
import formatDate from "react-day-picker/moment";
import TimePickerInput from "../common/TimePickerInput";
import {
  GOOGLE_PLACES_API,
  U_BASE_URL,
  U_FACEBOOK_ID,
  U_IMAGE_UPLOAD,
} from "../../api/Constants";
import {
  errorOptions,
  pet,
  successOptions,
} from "../../public/appData/AppData";
import { useSnackbar } from "react-simple-snackbar";
import { strings } from "../../public/lang/Strings";
import Link from "next/link";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import ReportSitterModal from "../common/ReportSitterModal";
import LocationSearchInput2 from "../common/LocationSearchInput2";
import { Dropdown, DropdownButton, ProgressBar } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import axios from "axios";
import image from "next/image";
import Loader from "../common/Loader";
import { TimeFormat } from "../../utils/Helper";
const axiosInstance = axios.create({
  baseURL: "http://localhost:8081/",
});

interface I_Props {
  participants: I_CHAT_PARTICIPANTS;
  updateParticipant: any;
}

type T_ARRANGE_MEET = {
  thread_id: string;
  date: string;
  time: string;
  location: string;
  text_message: string;
};

let initialState = {
  thread_id: "",
  date: "",
  time: "",
  location: "",
  text_message: "",
};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    border: "0px",
    padding: "0",
    zIndex: 100,
    backgroundColor: "transparent",
    transform: "translate(-50%, -50%)",
  },
  overlay: {
    backgroundColor: "rgba(0,0,0,0.75)",
    zIndex: 99,
  },
};

const api = new API();
const cookies = new Cookies();

const ChatBox: React.FC<I_Props> = ({
  participants,
  updateParticipant,
}: I_Props) => {
  const [messages, setMessages] = useState<I_CHAT_MESSAGE[]>([]);
  const [message, setMessage] = useState<string>("");
  const [userId, setUserId] = useState<number>(null);
  const [isOpen, setOpen] = useState<boolean>(false);
  const [arrangeMeetData, setArrangeMeetData] =
    useState<T_ARRANGE_MEET>(initialState);
  const [time, setTime] = useState<any>("");
  const [date, setDate] = useState<Date>();
  const [location, setLocation] = useState<string>("");
  const endMessage = useRef<any>();
  const [uploadedFilePath, setUploadedFilePath] = useState<any>();
  const [selectedImage, setSelectedImage] = useState<string>();
  let imgRef = React.useRef(null);

  const [openError] = useSnackbar(errorOptions);
  const [errors, setErrors] = useSnackbar<any>();
  const [lastMessageDate, setLastMessageDate] = useState(null);
  const [showReportModal, setShowReportModal] = useState<boolean>(false);
  const [value, setValue] = useState<string>("");
  const [uploadingImages, setUploadingImage] = useState([]);
  const [disableDatesOfArrangeMeetUp, setDisableDatesOfArrangeMeetUp] =
    useState<any>(null);
  const [reported, setReported] = useState<boolean>(false);
  const [showArrangeMeetUp, setShowArrangeMeetUp] = useState<boolean>(false);
  const [loading, setLoading] = useState(false);
  const [selectedParticipantUserId, setSelectedParticipantUserId] =
    useState<any>(null);

  useEffect(() => {
    getMessages(participants.id);
  }, [participants]);

  useEffect(() => {
    if (participants.user.id !== selectedParticipantUserId) {
      setSelectedParticipantUserId(participants.user.id);
      api
        .getReportedSitter(participants.user.id)
        .then((res) => {
          setReported(res.data.response);
        })
        .catch((error) => console.log(error.response));
    }
  }, [participants]);

  useEffect(() => {
    const id = cookies.get("id");
    setUserId(id);
  }, []);

  useEffect(() => {
    if (endMessage.current) {
      scrollToBottom();
    }
  }, [messages]);
  const scrollToBottom = () => {
    const element = document.getElementById("msg_history");
    element.scrollTop = element.scrollHeight;
  };

  const onFileChange = (event) => {
    event.preventDefault();
    if (event.dataTransfer || event.target.files) {
      let files;
      if (event.dataTransfer) {
        files = event.dataTransfer.files;
      } else if (event.target) {
        files = event.target.files;
      }

      if (files[0]) {
        for (let index = 0; index < event.target.files[`length`]; index++) {
          const reader = new FileReader();
          reader.onload = () => {
            setUploadedFilePath(reader.result);
          };

          reader.readAsDataURL(event.target.files[`${index}`]);
          const formData = new FormData();
          formData.append("image", event.target.files[`${index}`]);
          formData.append("path", "chat");

          axiosInstance
            .post(U_BASE_URL + U_IMAGE_UPLOAD, formData, {
              headers: {
                "Content-Type": "multipart/form-data",
              },
              onUploadProgress: async (data) => {
                let progress = Math.round((100 * data.loaded) / data.total);
                if (progress <= Number(1)) {
                  let arrObj = uploadingImages;
                  arrObj.push({ progress: progress, url: files[0] });
                  if (arrObj.find((val) => val.url != files[0])) {
                    await setUploadingImage(arrObj);
                  }
                } else if (progress == 100) {
                  let inProgressUploads = uploadingImages.filter(
                    (val) => val.url != files[0]
                  );
                  setUploadingImage(inProgressUploads);
                } else {
                  let inProgressUploads = uploadingImages.map((val) => {
                    let currentImage = val;
                    if (currentImage.url === files[0]) {
                      currentImage.progress = progress;
                      return currentImage;
                    }
                    return currentImage;
                  });
                  setUploadingImage(inProgressUploads);
                }
              },
            })
            .then((json) => {
              setSelectedImage(json.data.response);
              sendMessage(2, json.data.response);
            })
            .catch((error) => console.log(error));
        }
      }
    }
  };

  const getArrangeMeetUpLastDate = async (data) => {
    let requests = data.filter((val) => val.contain_request === 1);
    if (requests.length) {
      const maxDate = new Date(
        Math.max(
          ...requests.map((element) => {
            return new Date(element.request.drop_of_date);
          })
        )
      );

      // let maxMoment = '2023, 4, 12';
      let maxMoment = moment(maxDate).format("YYYY, MMM, D");
      let today = new Date().getTime();
      let date2 = new Date(maxMoment).getTime();
      let date = new Date(maxMoment);
      const previous = new Date(date.getTime());
      previous.setDate(date.getDate() - 1);
      let maxPrevious = moment(previous).format("YYYY, MMM, D");
      if (date2 > today) {
        setShowArrangeMeetUp(true);
      } else {
        setShowArrangeMeetUp(false);
      }
      setDisableDatesOfArrangeMeetUp({
        before: new Date(),
        after: new Date(maxPrevious),
      });
    }
  };
  const getMessages = async (threadId) => {
    setLoading(true);
    const messagesRef = ref(db, `chat_line/${threadId}`);
    onValue(messagesRef, (snapshot) => {
      var data = [];
      snapshot.forEach((element) => {
        data.push(element.val());
      });
      getArrangeMeetUpLastDate(data);
      setMessages(data);
      setLoading(false);
    });
  };

  const onTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMessage(e.target.value);
  };

  const sendMessage = (type, data) => {
    if (message != "" || type === 2) {
      setMessage("");
      let payload = {
        thread_id: participants.id,
        message: type === 2 ? data : message,
        documents: type == 2 ? [data] : [],
      };

      api
        .sendMessage(payload)
        .then((res) => {
          updateParticipant(participants);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const onModalClose = () => {
    setOpen(false);
    setDate(undefined);
    setArrangeMeetData(initialState);
    setLocation(undefined);
  };

  const callArrangeMeetup = () => {
    if (date == undefined) {
      setErrors({ ...errors, date: strings.SelectMeetingDate });
      openError(strings.SelectMeetingDate);
      return false;
    }

    if (location == "") {
      setErrors({ ...errors, location: strings.EnterMeetingLocation });
      openError(strings.EnterMeetingLocation);
      return false;
    }

    if (arrangeMeetData.text_message == "") {
      setErrors({ ...errors, text_message: strings.EnterMessage });
      openError(strings.EnterMessage);
      return false;
    }

    let data = JSON.stringify({
      ...arrangeMeetData,
      location: location,
      time: time,
      date: moment(date).format("MM/DD/YYYY"),
      thread_id: participants.id,
    });

    api
      .arrangeMeetup(data)
      .then((res) => {
        setOpen(false);
      })
      .catch((error) => console.log(error));
  };

  const updateMeetupStatus = (id: number, status: number) => {
    let data = {
      meet_up_id: id,
      status: status,
    };
    api
      .updateMeetupStatus(data)
      .then((res) => {
        setOpen(false);
        getMessages(participants.id);
      })
      .catch((error) => console.log(error));
  };

  const chatAction = (id, status) => {
    api
      .chatAction({ thread_id: id, status: status })
      .then((json) => {})
      .catch((error) => console.log(error));
  };

  function getNumberOfDays(start, end) {
    const date = new Date();
    const date1 = new Date(start);
    const date2 = new Date(end);

    // One day in milliseconds
    const oneDay = 1000 * 60 * 60 * 24;

    // Calculating the time difference between two dates
    const diffInTime = date2.getTime() - date1.getTime();
    const diffInTimeToday = date2.getTime() - date.getTime();

    // Calculating the no. of days between two dates
    const diffInDays = Math.floor(diffInTime / oneDay);
    const diffInDaysToday = Math.floor(diffInTimeToday / oneDay);

    if (diffInDaysToday == -1 && diffInDays !== 0) {
      return <p className="text-center">{strings.Today}</p>;
    } else if (diffInDaysToday == -2 && diffInDays !== 0) {
      return <p className="text-center">{strings.Yesterday}</p>;
    } else if (diffInDays !== 0) {
      return <p className="text-center">{end}</p>;
    } else {
      return null;
    }
  }
  const showDate = (lastMessageDate, newtime) => {
    return getNumberOfDays(lastMessageDate, newtime);
  };
  const showFirstDate = (props) => {
    const date = new Date(props);
    const today = new Date();

    // One day in milliseconds
    const oneDay = 1000 * 60 * 60 * 24;

    const diffInTimeToday = date.getTime() - today.getTime();
    const diffInDaysToday = Math.floor(diffInTimeToday / oneDay);

    if (diffInDaysToday == -1) {
      return <p className="text-center">{strings.Today}</p>;
    } else if (diffInDaysToday == -2) {
      return <p className="text-center">{strings.Yesterday}</p>;
    } else {
      return <p className="text-center">{props}</p>;
    }
  };

  const connectDisconnect = (id, status) => {
    confirmAlert({
      closeOnClickOutside: true,
      customUI: ({ onClose }) => {
        return (
          <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
            <div className="react-confirm-alert-body">
              <p>{strings.areYouSure}</p>
              <h6>{strings.YouWillNoLongerReceiveAnyMessageFromThisUser}</h6>

              <div className="react-confirm-alert-button-group">
                <button
                  className="btn btn-primary w-50"
                  aria-label="Yes"
                  onClick={() => {
                    chatAction(id, status == 1 ? 0 : 1);
                    onClose();
                  }}
                >
                  {strings.Yes}
                </button>
                <button
                  className="btn btn-danger w-50"
                  aria-label="No"
                  onClick={() => {
                    onClose();
                  }}
                >
                  {strings.No}
                </button>
              </div>
            </div>
          </div>
        );
      },
    });
  };

  return (
    <>
      <div
        className="col-md-6 col-lg-6 col-xl-6 col-sm-6 px-0 chat-box"
        id="main"
      >
        <div className="bg-white main-background">
          <div className="chat-top-details">
            <div className="row">
              <div className="col-6 col-md col-lg col-xl">
                <div className="d-flex single-grp-details review-details">
                  <div className="user-img group-img">
                    <img src={participants.user.profile_picture} alt="..." />
                  </div>
                  <div className="grup-single-name my-auto ml-2">
                    <Link
                      href={{
                        pathname: "/sitter-profile/" + participants.user.id,
                      }}
                    >
                      <h6 className="mb-0">
                        {participants.user.firstname +
                          " " +
                          participants.user.lastname}
                      </h6>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-auto alignment d-none d-md-none d-lg-block d-xl-block">
                <div className="row justify-content-between report-details">
                  {showArrangeMeetUp &&
                  participants.status == 1 &&
                  participants.disconnect == 0 ? (
                    <div
                      className="col-auto pr-2 pl-0"
                      data-toggle="modal"
                      data-target="#meet-up"
                    >
                      <button
                        onClick={() => setOpen(true)}
                        className="btn btn-primary chat mt-0"
                      >
                        {strings.ArrangeMeetUp}
                      </button>
                    </div>
                  ) : null}
                  <div className="col-auto pr-2 pl-0">
                    {reported == true ? (
                      <button className="btn btn-primary bg-transparent text-secondary border-secondary chat shade mt-0">
                        {strings.Reported}
                      </button>
                    ) : (
                      <button
                        className="btn btn-primary bg-transparent text-secondary border-secondary chat shade mt-0"
                        onClick={() => setShowReportModal(true)}
                      >
                        {strings.ReportSitter}
                      </button>
                    )}
                  </div>
                  <div className="col-auto pl-0">
                    {participants.status == 1 ? (
                      <div>
                        <button
                          onClick={() =>
                            connectDisconnect(
                              participants.id,
                              participants.status
                            )
                          }
                          className="btn btn-primary bg-transparent text-secondary border-secondary chat shade mt-0"
                        >
                          {participants.disconnect == 0
                            ? strings.Disconnect
                            : strings.Connect}
                          <span className="help-tip chat-help-tip">
                            <p>
                              {participants.disconnect == 0
                                ? strings.DisconnectAUserCanBeHelpfulIfYoureReceivingUnwantedMessages
                                : strings.ConnectAUserCanBeHelpfulIfYouWantToReceiveMessage}
                            </p>
                          </span>
                        </button>
                      </div>
                    ) : participants.disconnect == 1 ? (
                      <div>
                        <button className="btn btn-primary chat shade mt-0 bg-danger">
                          {strings.Blocked}
                        </button>
                      </div>
                    ) : (
                      <div>
                        <button
                          onClick={() =>
                            chatAction(
                              participants.id,
                              participants.status == 1 ? 0 : 1
                            )
                          }
                          className="btn btn-primary chat shade mt-0"
                        >
                          {strings.Connect}
                          <span className="help-tip chat-help-tip">
                            <p>
                              {
                                strings.ConnectAUserCanBeHelpfulIfYouWantToReceiveMessage
                              }
                            </p>
                          </span>
                        </button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              {/*------for mobile view---------------*/}
              <div className="col-6 col-md-6 d-block d-md-block d-lg-none d-xl-none">
                <div className="dropdown pet-drop mob-drop">
                  <DropdownButton
                    className="bg-transparent"
                    align="end"
                    title={
                      <a className="menu dropdown-toggle">
                        <div className="ellipse">
                          <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fal"
                            data-icon="ellipsis-h"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            className="svg-inline--fa fa-ellipsis-h fa-w-10 fa-2x"
                          >
                            <path
                              fill="currentColor"
                              d="M192 256c0 17.7-14.3 32-32 32s-32-14.3-32-32 14.3-32 32-32 32 14.3 32 32zm88-32c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zm-240 0c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"
                            />
                          </svg>
                        </div>
                      </a>
                    }
                    id="dropdown-menu-align-end"
                  >
                    <Dropdown.Item>
                      <a className="">
                        <img src="/images/social-img4.png" />
                        {strings.viaEmail}
                      </a>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <a className="">
                        <img src="/images/social-img3.png" />
                        {strings.viaWhatsapp}
                      </a>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <a className="">
                        {" "}
                        <img src="/images/social-img1.png" />
                        {strings.viaMessenger}
                      </a>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <a className="">
                        {" "}
                        <img src="/images/copy.png" />
                        {strings.CopyLink}
                      </a>
                    </Dropdown.Item>
                  </DropdownButton>
                </div>
                <div className="dropdown pet-drop d-none">
                  <a
                    className="menu dropdown-toggle"
                    href="#"
                    data-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <div className="ellipse"></div>
                  </a>
                  <div
                    className="dropdown-menu  animate slideIn"
                    x-placement="bottom-end"
                    style={{
                      position: "absolute",
                      transform: "translate3d(73px, 0px, 0px)",
                      top: 0,
                      left: 0,
                      willChange: "transform",
                    }}
                  >
                    <a href="#" className="dropdown-item">
                      {strings.ArrangeMeetUp}
                    </a>
                    <hr className="my-2" />
                    <button
                      className="dropdown-item"
                      onClick={() => setShowReportModal(true)}
                    >
                      {strings.ReportSitter}
                    </button>
                    <hr className="my-2" />

                    {participants.status == 0 ? (
                      <a
                        onClick={() =>
                          connectDisconnect(
                            participants.id,
                            participants.status
                          )
                        }
                        className="dropdown-item"
                      >
                        {strings.Disconnect}
                      </a>
                    ) : participants.disconnect == 1 ? (
                      <a
                        onClick={() =>
                          chatAction(
                            participants.id,
                            participants.status == 1 ? 0 : 1
                          )
                        }
                        className="dropdown-item"
                      >
                        {strings.Connect}
                      </a>
                    ) : (
                      <a className="dropdown-item">{strings.Blocked}</a>
                    )}
                  </div>
                </div>
              </div>
              {/*----------/for mobile view----------*/}
            </div>
          </div>
          <hr />
          <div className="scrollbar chat-scrl" id="msg_history">
            {loading ? (
              <Loader />
            ) : messages.length > 0 ? (
              messages.map((value, index) => (
                <div key={index}>
                  {index > 0
                    ? showDate(
                        moment(
                          new Date(
                            Number(messages[index - 1].send_timestamp) * 1000
                          )
                        ).format("MM/DD/YYYY"),
                        moment(
                          new Date(Number(value.send_timestamp) * 1000)
                        ).format("MM/DD/YYYY")
                      )
                    : showFirstDate(
                        moment(
                          new Date(Number(value.send_timestamp) * 1000)
                        ).format("MM/DD/YYYY")
                      )}
                  {value.contain_request == 0 &&
                    value.contain_meet_up === 0 &&
                    value.has_document == 0 && (
                      <div className="row">
                        <div
                          className={`col-12 col-md-10 col-lg-10 col-xl-10 ${
                            userId == value.sender_id
                              ? "offset-lg-2 offset-md-2 offset-xl-2"
                              : ""
                          }`}
                        >
                          <div
                            className={`chatbox ${
                              userId == value.sender_id
                                ? "right-chatbox"
                                : "left-chatbox"
                            }`}
                          >
                            <p className="mb-0 font-12">
                              {userId == value.sender_id
                                ? `Me • ${moment(
                                    new Date(
                                      Number(value.send_timestamp) * 1000
                                    )
                                  ).format("h:mm a")}`
                                : `${value.sender.firstname} ${
                                    value.sender.lastname
                                  } • ${moment(
                                    new Date(
                                      Number(value.send_timestamp) * 1000
                                    )
                                  ).format("h:mm a")}`}
                              {/* {moment(
                          new Date(Number(value.send_timestamp) * 1000)
                        ).format("MM/DD/YYYY")} */}
                            </p>
                            <h6 className="font-14">{value.message}</h6>
                          </div>
                        </div>
                      </div>
                    )}
                  {value.has_document == 1 && (
                    <div className="row">
                      <div
                        className={`col-12 col-md-10 col-lg-10 col-xl-10 ${
                          userId == value.sender_id
                            ? "offset-lg-2 offset-md-2 offset-xl-2"
                            : ""
                        }`}
                      >
                        <div
                          className={`chatbox ${
                            userId == value.sender_id
                              ? "right-chatbox"
                              : "left-chatbox"
                          }`}
                        >
                          {value.message.includes(".mp4") ||
                          value.message.includes(".mov") ? (
                            <video controls>
                              <source src={value.message} />
                            </video>
                          ) : (
                            <img
                              src={value.message}
                              onClick={() =>
                                window.open(value.message, "_blank")
                              }
                            />
                          )}
                          <p className="mb-0 font-12">
                            {moment(
                              new Date(Number(value.send_timestamp) * 1000)
                            ).format("MM/DD/YYYY")}
                          </p>
                        </div>
                      </div>
                    </div>
                  )}
                  {value.contain_meet_up == 1 && (
                    <div
                      className={`col-12 col-md-10 col-lg-10 col-xl-10  ${
                        userId == value.sender_id
                          ? "offset-lg-2 offset-md-2 offset-xl-2 d-inline-block"
                          : ""
                      }`}
                    >
                      <div
                        className={`chatbox meeting-request ${
                          userId == value.sender_id ? "right-chatbox" : ""
                        }`}
                      >
                        <h6 className="font-14 font-semibold">
                          {strings.MeetUpRequested}
                        </h6>
                        <p className="mb-0 font-12">
                          {userId == value.sender_id
                            ? strings.You
                            : value.sender.firstname +
                              " " +
                              value.sender.lastname}{" "}
                          {strings.requestedForMeetUpOn}
                        </p>

                        <div className="service-date my-2">
                          <p className="mb-0 font-12">
                            <b>{strings.Message}</b>
                          </p>
                          <p className="mb-0 font-12">
                            <span>{value.meet_up.message}</span>
                          </p>
                        </div>

                        <div className="service-date my-2">
                          <p className="mb-0 font-12">
                            <b>{strings.Date_O_Time}</b>
                          </p>
                          <p className="mb-0 font-12">
                            <span>
                              {moment(value.meet_up.date).format("MM/DD/YYYY")}{" "}
                              &nbsp; {value.meet_up.time}
                            </span>
                          </p>
                        </div>

                        {userId != value.meet_up.requested_by &&
                          value.meet_up.status == 0 && (
                            <div className="meeting-btn">
                              <button
                                onClick={() =>
                                  updateMeetupStatus(value.meet_up.id, 1)
                                }
                                className="btn btn-primary btn1 btn-accept"
                                data-toggle="modal"
                                data-target="#accept"
                              >
                                {strings.Accept}
                              </button>
                              <button
                                onClick={() =>
                                  updateMeetupStatus(value.meet_up.id, 2)
                                }
                                className="btn btn-primary btn1 btn-accept btn-reject"
                                data-toggle="modal"
                                data-target="#reject"
                              >
                                {strings.Decline}
                              </button>
                            </div>
                          )}
                        {userId == value.meet_up.requested_by &&
                          value.meet_up.status == 0 && (
                            <div className="meeting-btn">
                              <button
                                className="btn btn-primary btn1 btn-accept"
                                data-toggle="modal"
                                data-target="#accept"
                              >
                                {strings.Pending}
                              </button>
                            </div>
                          )}
                        {value.meet_up.status == 1 && (
                          <div className="meeting-btn">
                            <button
                              className="btn btn-primary btn1 btn-accept"
                              data-toggle="modal"
                              data-target="#accept"
                            >
                              {strings.Accepted}
                            </button>
                          </div>
                        )}
                        {value.meet_up.status == 2 && (
                          <div className="meeting-btn">
                            <button
                              className="btn btn-primary btn1 btn-accept"
                              data-toggle="modal"
                              data-target="#accept"
                            >
                              {strings.Rejected}
                            </button>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {value.contain_request == 1 && (
                    <div
                      className={`col-12 col-md-10 col-lg-10 col-xl-10 ${
                        userId == value.sender_id
                          ? "offset-lg-2 offset-md-2 offset-xl-2 d-inline-block"
                          : ""
                      }`}
                    >
                      <div
                        className={`chatbox meeting-request ${
                          userId == value.sender_id ? "right-chatbox" : ""
                        }`}
                      >
                        <h6 className="font-14 font-semibold">
                          {userId == value.sender_id
                            ? strings.NewRequestMade
                            : strings.NewRequestReceived}
                        </h6>
                        <p className="mb-0 font-12">
                          {strings.BookingFor} {value.request.service.name}
                          {" " + strings.for + " "}
                          {value.request.pets.length}{" "}
                          {value.request.pets.length
                            ? pet.find(
                                (val) => val?.key === value?.request?.pet_type
                              )?.label
                            : ""}
                          {strings.isSuccessfullyDoneForYEN}
                          {value.request.total_paid_amount}.
                        </p>

                        <div className="service-date my-2">
                          <p className="mb-0 font-12">
                            <b>{strings.Date_O_Time}</b>
                          </p>
                          <p className="mb-0 font-12">
                            <span>
                              {moment(
                                new Date(value.request.drop_of_date)
                              ).format("MM/DD/YYYY")}
                              &nbsp;|&nbsp;
                              {TimeFormat(value.request.drop_of_time_from)} -{" "}
                              {moment(
                                new Date(value.request.pickup_up_date)
                              ).format("MM/DD/YYYY")}
                              &nbsp;|&nbsp;
                              {TimeFormat(value.request.pickup_up_time_to)}
                            </span>
                          </p>
                        </div>
                        <div className="service-date">
                          <p className="mb-0 font-12">
                            <b>{strings.Pets}</b>
                          </p>
                          {value.request.pets.map((val, index2) => {
                            return (
                              <p key={index2} className="mb-0 font-12">
                                <span>{val.pet_name} | </span>{" "}
                                <span>{val.breed ? val.breed.breed : ""}</span>
                              </p>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  )}
                  {uploadingImages.length && messages.length - 1 === index
                    ? uploadingImages.map((image, index) => (
                        <div className="row w-100 h-100">
                          <div
                            className={`col-12 col-md-10 col-lg-10 col-xl-10 ${
                              userId == value.sender_id
                                ? "offset-lg-2 offset-md-2 offset-xl-2"
                                : ""
                            }`}
                          >
                            <div
                              className={`chatbox ${
                                userId == value.sender_id
                                  ? "right-chatbox"
                                  : "left-chatbox"
                              }`}
                            >
                              <ProgressBar
                                style={{ height: 15, width: 525 }}
                                now={image.progress}
                                label={`${image.progress}%`}
                              />
                            </div>
                          </div>
                        </div>
                      ))
                    : null}
                </div>
              ))
            ) : (
              <div className="text-center padding">
                <p className="font-13 mb-0 font-italic">{strings.noMsgYet}</p>
              </div>
            )}

            <div ref={endMessage} />
          </div>
          {participants.status == 1 ? (
            <div className="send-msg">
              <div className="row">
                <div className="col pd-right">
                  <div className="send-msg-bar">
                    <input
                      className="form-control"
                      id="send-msg"
                      placeholder="Type to send message"
                      type="text"
                      value={message}
                      onChange={onTextChange}
                      onKeyUp={(event) => {
                        if (event.keyCode === 13) {
                          sendMessage(1, message);
                        }
                      }}
                    />
                  </div>
                </div>
                <div
                  className="col-auto  right-padding alignment"
                  id="send-col2"
                >
                  <div className="d-flex">
                    <input
                      className={"d-none"}
                      ref={imgRef}
                      type="file"
                      name="file"
                      onChange={onFileChange}
                      multiple
                    />
                    <a
                      onClick={() => {
                        imgRef.current.click();
                      }}
                    >
                      <div className="plus-sign">
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fal"
                          data-icon="plus"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 384 512"
                          className="svg-inline--fa fa-plus fa-w-12 fa-2x"
                        >
                          <path
                            fill="currentColor"
                            d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"
                          />
                        </svg>
                      </div>
                    </a>

                    <div className="chat-send-btn d-none d-md-none d-lg-block d-xl-block">
                      <button
                        onClick={() => sendMessage(1, message)}
                        className="btn btn-primary"
                      >
                        {strings.Send}
                      </button>
                    </div>
                    {/*----------for mobile view-------------*/}
                    <div
                      onClick={() => sendMessage(1, message)}
                      className="plus-sign send-sign mr-0 d-block d-md-block d-lg-none d-xl-none"
                    >
                      <svg
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fal"
                        data-icon="paper-plane"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 512 512"
                        className="svg-inline--fa fa-paper-plane fa-w-16 fa-2x"
                      >
                        <path
                          fill="currentColor"
                          d="M464 4.3L16 262.7C-7 276-4.7 309.9 19.8 320L160 378v102c0 30.2 37.8 43.3 56.7 20.3l60.7-73.8 126.4 52.2c19.1 7.9 40.7-4.2 43.8-24.7l64-417.1C515.7 10.2 487-9 464 4.3zM192 480v-88.8l54.5 22.5L192 480zm224-30.9l-206.2-85.2 199.5-235.8c4.8-5.6-2.9-13.2-8.5-8.4L145.5 337.3 32 290.5 480 32l-64 417.1z"
                        />
                      </svg>
                    </div>
                    {/*----------/for mobile view------------*/}
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
      <Modal isOpen={isOpen} style={customStyles}>
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{strings.ArrangeMeetUp}</h5>
              <button
                type="button"
                onClick={onModalClose}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="meetup-details">
                <form>
                  <div className="row">
                    <div className="col-sm-6">
                      <h6>{strings.Date}</h6>
                      <div className="form-group">
                        <div className={"form-control"}>
                          <DayPickerInput
                            formatDate={formatDate.formatDate}
                            parseDate={parseDate.parseDate}
                            dayPickerProps={{
                              modifiers: {
                                disabled: [disableDatesOfArrangeMeetUp],
                              },
                            }}
                            inputProps={{
                              style: {
                                border: 0,
                                background: "transparent",
                              },
                              readOnly: true,
                            }}
                            onDayChange={(date) => {
                              setErrors({ ...errors, date: null });
                              setDate(date);
                            }}
                            placeholder="MM/DD/YYYY"
                            format="MM/DD/yyyy"
                            value={date}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <h6>{strings.Time}</h6>
                      <div className="form-group picker-down">
                        {/* <TimePicker
                          value={time}
                          onChange={(value) => setTime(value)}
                        /> */}
                        <DatePicker
                          className="form-control"
                          showTimeSelect
                          placeholderText="Select time"
                          showTimeSelectOnly
                          timeCaption="Select meet time"
                          required
                          timeIntervals={30}
                          dateFormat="h:mm a"
                          onChange={(selectedTime) => {
                            const formattedTime = selectedTime.toLocaleTimeString('en-US', {
                              hour: 'numeric',
                              minute: '2-digit',
                            });
                            setTime(formattedTime);
                          }}
                          picker={1}
                          value={time}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="location-details">
                    <h6>{strings.Location}</h6>
                    <div className={"col-12"}>
                      <div className={"row align-items-center"}>
                        <div className="col p-0 location-search-input">
                          <LocationSearchInput2
                            key={"chatbox"}
                            className={"form-control"}
                            value={value}
                            setFilter={(address) => setLocation(address.value)}
                            setLatLng={(latLng) => {}}
                            setValue={(address) => {
                              setValue(address);
                              if (address == "") {
                                setLocation("");
                              }
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="msg-area mt-3">
                    <h6>{strings.Message}</h6>
                    <textarea
                      className={"form-control"}
                      value={arrangeMeetData.text_message}
                      onChange={(e) =>
                        setArrangeMeetData({
                          ...arrangeMeetData,
                          text_message: e.target.value,
                        })
                      }
                      rows={4}
                      defaultValue={""}
                    />
                  </div>
                  <div className="text-center mt-3">
                    <button
                      onClick={(e) => {
                        e.preventDefault();
                        callArrangeMeetup();
                      }}
                      className="btn btn-primary"
                    >
                      {strings.Confirm}
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <ReportSitterModal
        showModal={showReportModal}
        hideModal={() => setShowReportModal(false)}
        participantId={participants.user.id}
        setReported={() => setReported(reported == true ? false : true)}
      />
    </>
  );
};

export default ChatBox;
