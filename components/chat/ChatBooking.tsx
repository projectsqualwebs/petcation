import React from "react";
import {
    I_CHAT_BOOKING,
    I_CHAT_PARTICIPANTS,
} from "../../models/chat.interface";
import API from "../../api/Api";
import {useEffect} from "react";
import {AxiosResponse} from "axios";
import Res from "../../models/response.interface";
import {useState} from "react";
import moment from "moment";
import Cookies from "universal-cookie";

import TransportFeeModal from "../common/TransporFeeModal";
import RequestCancelModal from "../common/RequestCancelModal";
import ChatBookingCountdown from "./ChatBookingCountdown";
import {useSnackbar} from 'react-simple-snackbar';
import {FailureOptions, SuccessOptions} from "../../api/Constants";
import {strings} from "../../public/lang/Strings";
import {useRouter} from "next/router";
import {errorOptions, successOptions} from "../../public/appData/AppData";
import {confirmAlert} from "react-confirm-alert";
import 'react-confirm-alert/src/react-confirm-alert.css';
import RateSitter from "../common/RateSitter";
import OwnerRating from "../common/OwnerRating";
import Loader from "../common/Loader";
import {TimeFormat} from "../../utils/Helper";
import RequestRejectModal from "../common/RequestRejectModal";
import RequestDisputeModal from "../common/RequestDisputeModal";

interface I_Props {
    participants: I_CHAT_PARTICIPANTS;
}

const api = new API();
const ChatBooking: React.FC<I_Props> = ({participants}: I_Props) => {
    const [chatBooking, setChatBooking] = useState<I_CHAT_BOOKING[]>([]);
    const [user, setUser] = useState<any>();
    const cookies = new Cookies();
    const userId = cookies.get("id");
    const fName = cookies.get("firstname");
    const lName = cookies.get("lastname");
    const profilePic = cookies.get("profile_picture");

    const [showModal, setShowModal] = useState<any>();
    const [bookingId, setBookingId] = useState<any>();
    const [bookingRequestId, setBookingRequestId] = useState<number>();
    const [sitterId, setSitterId] = useState<any>();
    const [bookingUserId, setBookingUserId] = useState<number>();
    const [myReview, setMyReview] = useState<any>();
    const [bookingService, setBookingService] = useState<any>();
    const [bookingServiceId, setBookingServiceId] = useState<any>();

    const [bankAdded, setBankAdded] = useState<boolean>(false);
    const [showDisputeModal, setShowDisputeModal] = useState<boolean>(false);
    //cancel modal
    const [cancelModal, setCancelModal] = useState<any>(false);
    //rating modal
    const [showRatingModal, setShowRatingModal] = useState<any>();
    const [ownerRatingModal, setShowOwnerRatingModal] = useState<boolean>(false);
    const [request, setRequest] = useState<any>();
    const [openError, closeError] = useSnackbar(errorOptions);
    const [openSnackbar, closeSnackbar] = useSnackbar(successOptions);
    const [loading, setLoading] = useState(false);
    const [actionLoading, setActionLoading] = useState(false);
    const router = useRouter();
    const [bookingParticipantId, setBookingParticipantId] = useState(null);


    useEffect(() => {
        if (participants.id !== bookingParticipantId) {
            setBookingParticipantId(participants.id)
            getThreadBooking();
        }
    }, [participants]);


    useEffect(() => {
        getBankAccounts();
    }, []);

    useEffect(() => {
        setUser({id: userId, firstname: fName, lastname: lName, profile_picture: profilePic})
    }, [fName, lName, userId, profilePic]);


    const getBankAccounts = () => {
        api
            .getAllBankAccount()
            .then((json) => {
                if (json.data.response && json.data.response.length) {
                    setBankAdded(true);
                }
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    openError(error.response.data.message)
                }
            });
    };

    //API call to get booking details
    const getThreadBooking = () => {
        setLoading(true);
        api
            .getThreadBooking(participants.id)
            .then((res: AxiosResponse<Res<I_CHAT_BOOKING[]>>) => {
                if (res.data.status === 200) {
                    setChatBooking(res.data.response);
                    // setUser(res.data.response[0] ? res.data.response[0].user : null);
                } else {
                    setChatBooking(res.data.response);
                }
                setLoading(false);
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    openError(error.response.data.message)
                }
                setLoading(false);
            });
    };


    //Button Laebel
    const getButtonLabel = (chatBooking) => {
        if (!user) {
            return
        }
        if (chatBooking.booking_user_id == userId) {
            if (chatBooking.status === 0) {
                return strings.RequestPending
            } else if (chatBooking.status === 1) {
                return strings.RequestAccepted
            } else if (chatBooking.status === 2) {
                return strings.RequestRejected
            }
        } else {
            if (chatBooking.status === 0) {
                return strings.AcceptRequest
            } else if (chatBooking.status === 1) {
                return strings.RequestAccepted
            } else if (chatBooking.status === 2) {
                return strings.RequestRejected
            }
        }
    };

    const has12HoursPassed = (updatedAt) => {
        const updatedAtMoment = moment(updatedAt);
        const now = moment();
        const duration = moment.duration(now.diff(updatedAtMoment));
        return duration.asHours() >= 12;
    };

    const disputeBooking = (data) => {
        setLoading(true);
        let payload = JSON.stringify(data);
        api
            .disputeBooking(payload)
            .then((res) => {
                console.log(res.data);
                setShowDisputeModal(false);
                setLoading(false);
                openSnackbar(res.data.message);
                getThreadBooking();
            })
            .catch((error) => {
                setLoading(false);
                if (error.response && error.response.data) {
                    openError(error.response.data.message);
                }
            });
    };

    //Api call for add transport fee
    const handleAddCharge = (amount) => {
        if (!amount) {
            openSnackbar(strings.EnterAmount)
        } else {
            setActionLoading(true);
            api
                .addTransportChargeRequest({request_id: bookingId, amount: amount})
                .then((res: AxiosResponse<Res<I_CHAT_BOOKING[]>>) => {
                    openSnackbar(res.data.message);
                    setShowModal(false);
                    setActionLoading(false);
                    updateBookingStatus({id: bookingId}, 1, '')
                })
                .catch((error) => {
                    if (error.response && error.response.data) {
                        openError(error.response.data.message)
                    }
                    setActionLoading(false);
                });
        }
    };

    //API call to update booking status
    const updateBookingStatus = (value, action, reason) => {
        api
            .updateRequestStatus({request_id: value.id, status: action, reason: reason})
            .then((res) => {
                if (res.data.status === 200) {
                    openSnackbar(res.data.message);
                    getThreadBooking();
                } else {
                    openSnackbar(res.data.message)
                }
            })
            .catch((error) => {
                // console.log(error.response.data.message)
                if (error.response && error.response.data) {

                    openError(error.response.data.message)
                }
            });
    }

    /**
     * funtion to withdraw the request.
     * @param id  to provide identity of a perticular request.
     * @param data  contains parameter for which we withdraw the request.
     * @success
     *    success -> withdraw booking.
     *    faliure -> booking will exist.
     */
    const withdrawRequest = (id: any, data: any) => {
        setActionLoading(true);
        api
            .cancelRequest(id, {
                pets: data.userPets,
                additional_services: data.additionalServices,
                reason: data?.reason,
            })
            .then((res) => {
                setActionLoading(false);
                setCancelModal(false);
                getThreadBooking();
            })
            .catch((error) => {
                setActionLoading(false);
                openError(error.response?.data?.message)
            })
    };

    const rejectRequest = (value) => {
        confirmAlert({
            closeOnClickOutside: true,
            customUI: ({onClose}) => {
                return (
                    <div className="react-confirm-alert reactConfirmAlertCustomUIModal">
                        <div className="react-confirm-alert-body">
                            <h1>{strings.RejectRequest_Q}</h1>
                            <p>{strings.areYouSure}</p>
                            <div className="react-confirm-alert-button-group">
                                <button className="btn btn-primary w-50" aria-label="Yes" onClick={() => {
                                    updateBookingStatus(value, 2, '')
                                    onClose();
                                }}>{strings.Yes}</button>
                                <button className="btn btn-danger w-50" aria-label="No" onClick={() => {
                                    onClose();
                                }}>{strings.No}</button>
                            </div>
                        </div>
                    </div>
                );
            }
        });
    }
    return (
        <div className="col-md-6 col-lg-3 col-xl-3 col-sm-3 chat-info">
            <div className="bg-white main-background ">
                <div className="actions-details">
                    <h6 className="font-medium mb-1">{strings.Actions}</h6>
                    <p className="font-14 mb-0">
                        {chatBooking.length && userId == chatBooking[0].booking_user_id ?
                            strings.ToBookThisSitterForYouRequestYouCanConfirmPayment
                            : strings.ToAcceptThisPetOwnerRequestYouCanClickOnAcceptButton}
                    </p>
                </div>
            </div>
            <div className="bg-white main-background right-scrl scrollbar">
                {loading ? <Loader/> : chatBooking.map((value, index) => {
                    return (
                        <div className="item-chat" key={'chat_' + index}>
                            <div key={'chat_' + index} className="booking-edit">
                                <div className="d-flex align-items-center justify-content-between">
                                    <h6 className="font-medium mb-1 d-inline-block">
                                        {strings.BookingDetails}
                                        {(value.status === 0 && !value.dispute && !has12HoursPassed(value.updated_at)) &&
                                        <p className="mt-2 mb-1">
                                            <ChatBookingCountdown time={value.updated_at}/>
                                        </p>
                                        }
                                    </h6>
                                    {(!value.dispute && !has12HoursPassed(value.updated_at)) &&
                                    <button
                                        onClick={() => {
                                            setRequest(value);
                                            setShowDisputeModal(true);
                                        }}
                                        className="btn confirm-btn btn-outline-primary border-danger text-danger">{strings.Dispute}
                                    </button>
                                    }
                                </div>
                                {value?.want_to_receive_media === 1 ? (
              <div className="logo d-flex justify-content-start">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-cloud-check"
                  viewBox="0 0 16 16"
                >
                  <path
                    fill-rule="evenodd"
                    d="M10.354 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"
                  />
                  <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383zm.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z" />
                </svg>
                <small className="text-danger">&nbsp;{strings.RecivedMeadia}</small>
              </div>
            ) : (
              ""
            )}
                                {/*<a>Edit</a>*/}
                                <p className="font-14 mb-1">{`${value.service.name} for`}</p>
                                <div className="dog-boarding">
                                    {value.pets.map((pet, ind) => (
                                        <p key={'pets_' + ind} className="mb-1 font-12">
                                           <b> {`${pet.pet_name} | ${pet.breed.breed} | ${pet.age_year} ${strings.YRS} | ${pet.weight + strings.kgs}`}</b>
                                        </p>
                                    ))}
                                </div>
                                <div className="address mb-2">
                                    <a href="#">{value.user.address?.address}</a>
                                </div>
                                <div className="from-date">
                                    <h6 className="font-12 font-normal mb-1">
                                        {strings.From_C}
                                        <span
                                            className="text-muted">{`${moment(new Date(value.drop_of_date)).format("MM/DD/YYYY")} | ${TimeFormat(value.drop_of_time_from)}`}</span>
                                    </h6>
                                    <h6 className="font-12 font-normal mb-2">
                                        {strings.To_C}
                                        <span
                                            className="text-muted">{`${moment(new Date(value.pickup_up_date)).format("MM/DD/YYYY")} | ${TimeFormat(value.pickup_up_time_from)}`}</span>
                                    </h6>
                                    <h6 className="font-12 font-normal mb-0">
                                        {strings.PostedOn_C} {moment(value.created_at).format("MM/DD/YYYY")}
                                    </h6>
                                </div>
                            </div>
                            {value.sitter.id === sitterId ? <div className="mt-3 mb-3 row">
                                    <div className={"col-6"}>
                                        <h6 className="font-12 font-normal">
                                            {strings.TransportationFee_C} <br/> {value.amend_amount}
                                        </h6>
                                    </div>
                                    <div className={"col-6"}>
                                        <h6 className="font-12 font-normal">
                                            {strings.PlatformFee_C} <br/> {value.petcation_fee_amount}
                                        </h6>
                                    </div>
                                    <div className={"col-6"}>
                                        <h6 className="font-12 font-normal ">
                                            {strings.ServiceFee_C} <br/> {value.sitter_earning_amount}
                                        </h6>
                                    </div>
                                    <div className={"col-6"}>
                                        <h6 className="font-12 font-normal">
                                            {strings.TotalFee_C} <br/> {value.total_paid_amount}
                                        </h6>
                                    </div>
                                    {value.status == 2 ? <div className={"col"}>
                                        <br/>
                                        <h6 className="font-12 font-normal mb-0">
                                            <b>{strings.ReasonOfCancellation_C}</b> <p className="m-0">{value.reason}</p>
                                        </h6>
                                    </div> : null}
                                </div> :
                                <div className="mt-3 mb-3 row">
                                    <div className={"col"}>
                                        <h6 className="font-12 font-normal mb-0">
                                            {strings.TransportationFee_C} {value.amend_amount}
                                        </h6>
                                    </div>
                                    <div className={"col"}>
                                        <h6 className="font-12 font-normal mb-0">
                                            {strings.ServiceFee_C} {value.sub_total}
                                        </h6>
                                    </div>
                                    <div className={"col"}>
                                        <h6 className="font-12 font-normal mb-0">
                                            {strings.TotalFee_C} {value.total_paid_amount}
                                        </h6>
                                    </div>
                                    {value.status == 2 ? <div className={"col"}>
                                        <br/>
                                        <h6 className="font-12 font-normal mb-0">
                                            <b>{strings.ReasonOfCancellation_C}</b> <p
                                            className="m-0">{value.reason}</p>
                                        </h6>
                                    </div> : null}
                                </div>}
                            <div className="col-12 mt-2 mb-3">
                                <div className="row justify-content-between align-items-center">
                                    {userId == value.booking_user_id && <>
                                        {value.status == 0 && <small
                                            className="border-warning text-warning border rounded-sm px-2 py-1 d-inline font-10">
                                            {strings.RequestPending}
                                        </small>}
                                        {value.payment_status == 0 && value.status == 1 && <a>
                                            <button onClick={() => {
                                                router.push({
                                                    pathname: "/payment",
                                                    query: {id: value.id}
                                                })
                                            }} className="btn btn-primary confirm-btn mr-1">
                                                {strings.pay}
                                            </button>
                                        </a>}
                                        {value.status == 0 && <a>
                                            <button onClick={() => {
                                                setRequest(value);
                                                setCancelModal(true);
                                            }
                                            } className="border-danger text-danger border rounded-sm px-2 py-1 d-inline font-10 ml-2">
                                                {strings.WithdrawRequest}
                                            </button>
                                        </a>}
                                        {value.status == 1 && <a>
                                            <button onClick={() => {
                                                setRequest(value);
                                                setCancelModal(true);
                                            }
                                            } className="border-danger text-danger border rounded-sm px-2 py-1 d-inline font-10 ml-2">
                                                {value.payment_status == 1 ? strings.CancelRequest : strings.WithdrawRequest}
                                            </button>
                                        </a>}
                                        {value.status == 3 && <small
                                            className="border-primary text-primary border rounded-sm px-2 py-1 d-inline font-10">
                                            {strings.UpcomingRequest}
                                        </small>}
                                        {value.status == 4 && <small
                                            className="border-success text-success border rounded-sm px-2 py-1 d-inline font-10">
                                            {strings.BookingCompleted}
                                        </small>}
                                        {value.status == 4 && <a>
                                            <button onClick={() => {
                                                setSitterId(value.booked_sitter_id);
                                                setBookingRequestId(value.id)
                                                setMyReview(value?.sitter_reviews)
                                                setShowRatingModal(true);
                                            }
                                            } className="btn btn-primary confirm-btn">
                                                {value?.sitter_reviews?.length ? strings.EditReview : strings.GiveReview}
                                            </button>
                                        </a>}
                                        {value.status == 2 && value.payment_status == 0 && <small
                                            className="border-danger text-danger border rounded-sm px-2 py-1 d-inline font-10">
                                            {strings.RequestRejected}
                                        </small>}
                                        {value.status == 2 && value.payment_status == 1 && <small
                                            className="border-danger text-danger border rounded-sm px-2 py-1 d-inline font-10">
                                            {strings.RequestCancelled}
                                        </small>}
                                    </>}
                                    {/* {console.log("value", value)} */}
                                    {userId != value.booking_user_id &&
                                    <>
                                        {!!value.status &&
                                        <>
                                            {value.status == 1 &&
                                            <small
                                                className="border-primary text-primary border rounded-sm px-2 py-1 d-inline font-10">
                                                {strings.acceptedByYou}</small>}
                                            {value.status == 3 && <small
                                                className="border-primary text-primary border rounded-sm px-2 py-1 d-inline font-10">
                                                {strings.UpcomingBooking} </small>
                                            }
                                            {value.status == 4 &&
                                            <small
                                                className="border-primary text-primary border rounded-sm px-2 py-1 d-inline font-10">
                                                {strings.BookingCompleted}</small>
                                            }
                                            {value.status == 4 && <a>
                                                <button onClick={() => {
                                                    setBookingUserId(value.booking_user_id);
                                                    setBookingRequestId(value.id)
                                                    setMyReview(value?.owner_reviews)
                                                    setShowOwnerRatingModal(true);
                                                }
                                                } className="btn btn-primary confirm-btn">
                                                    {value?.owner_reviews?.length ? strings.EditReview : strings.GiveReview}
                                                </button>
                                            </a>}
                                            {value.status == 1 && value.payment_status == 0 && <small
                                                className="border-primary text-primary border rounded-sm px-2 py-1 d-inline font-10">
                                                {strings.Unpaid}</small>}
                                            {value.status == 1 && value.payment_status == 1 && <small
                                                className="border-primary text-primary border rounded-sm px-2 py-1 d-inline font-10">
                                                {strings.Paid}</small>}

                                            {value.status == 2 &&
                                            <small
                                                className="border-danger text-danger border rounded-sm px-2 py-1 d-inline font-10">
                                                {strings.RequestRejected}</small>}
                                        </>
                                        }
                                        {(value.status == 0 && !has12HoursPassed(value.updated_at)) &&
                                        <div className="p-0 col-12 d-flex align-items-center justify-content-between">
                                            {value?.need_sitter_pickup === 1 ? <a>
                                                <button onClick={() => {
                                                    if (bankAdded) {
                                                        setBookingId(value.id)
                                                        setBookingService(value.service.name)
                                                        setBookingServiceId(value.service.id)
                                                        setShowModal(true)
                                                    } else {
                                                        openError(strings.PleaseAddYourBankAccountDetailsBeforeAcceptingTheBooking)
                                                    }
                                                }} className="btn btn-primary confirm-btn ml-1">
                                                    {strings.AcceptRequest + "hiii"}
                                                </button>
                                            </a> : <a>
                                                <button onClick={() => {
                                                    if (bankAdded) {
                                                        updateBookingStatus({id: value.id}, 1, '')
                                                    } else {
                                                        openError(strings.PleaseAddYourBankAccountDetailsBeforeAcceptingTheBooking)
                                                    }
                                                }} className="btn btn-primary confirm-btn ml-1">
                                                    {strings.AcceptRequest}
                                                </button>
                                            </a>}
                                            <a>
                                                <button onClick={() => rejectRequest(value)}
                                                        className="btn btn-outline-primary border-danger text-danger confirm-btn ml-1">
                                                    {strings.RejectRequest}
                                                </button>
                                            </a>
                                        </div>
                                        }
                                        {has12HoursPassed(value.updated_at) &&
                                        <div className="p-0 col-12 d-flex align-items-center justify-content-start">
                                                <p className="btn btn-outline-primary border-danger text-danger confirm-btn mr-1">
                                                    {strings.TimeOut}
                                                </p>
                                        </div>
                                        }
                                    </>
                                    }
                                    <hr/>
                                </div>
                            </div>
                        <hr className="mt-4 mb-3"/>
                        </div>
                    )
                })}
            </div>
            {request &&
            <RequestCancelModal showModal={cancelModal} hideModal={() => setCancelModal(false)} request={request}
                                paid={false} withdrawRequest={(id, data) => withdrawRequest(id, data)}
                                loading={actionLoading}/>}
            <RateSitter bookingId={bookingRequestId} sitterId={sitterId} showModal={showRatingModal}
                        hideModal={() => setShowRatingModal(false)} getThreadBooking={() => getThreadBooking()}
                        myReview={myReview}/>
            <OwnerRating bookingId={bookingRequestId} userId={bookingUserId} showModal={ownerRatingModal}
                         onHide={() => setShowOwnerRatingModal(false)} getThreadBooking={() => getThreadBooking()}
                         myReview={myReview}/>
            <TransportFeeModal bookingServiceId={bookingServiceId} bookingService={bookingService} id={bookingId}
                               showModal={showModal} hideModal={() => setShowModal(false)}
                               handleAddCharge={(amount) => handleAddCharge(amount)} loading={actionLoading}/>
            <RequestDisputeModal
                showModal={showDisputeModal}
                hideModal={() => setShowDisputeModal(false)}
                handleSubmit={(data) => {
                    disputeBooking(data);
                }}
                request={request}
                loading={loading}
            />
        </div>
    );
};

export default ChatBooking;
