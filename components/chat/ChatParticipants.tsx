import { collection, getDocs } from "firebase/firestore/lite";
import React from "react";
import { useEffect } from "react";
import db from "../../service/Firebase";
import { ref, onValue } from "firebase/database";
import Cookies from "universal-cookie";
import { useState } from "react";
import { I_CHAT_PARTICIPANTS } from "../../models/chat.interface";
import { formatDistance } from "date-fns";
import API from "../../api/Api";
import { useRouter } from "next/router";
import {strings} from "../../public/lang/Strings";
import {threadId} from "worker_threads";

interface I_Props {
  updateParticipant: (participant: I_CHAT_PARTICIPANTS) => void;
  active: I_CHAT_PARTICIPANTS;
  setActive: (active: I_CHAT_PARTICIPANTS) => void;
  updateMsg: boolean,
}
//fgg
const cookies = new Cookies();
const api = new API();


const ChatParticipants: React.FC<I_Props> = ({
                                               updateParticipant,
                                               active,
                                               setActive,
                                               updateMsg
                                             }: I_Props) => {
  const [participants, setParticipants] = useState<I_CHAT_PARTICIPANTS[]>([]);
  const [searchData, setSearchData] = useState<I_CHAT_PARTICIPANTS[]>([]);
  const [searchedText, setSearchedText] = useState<string>();
  const router = useRouter();

  useEffect(() => {
    getParticipants().then(r => {});
  }, [router?.query?.id]);

    useEffect(() => {
      if(participants.length && router.query?.threadId) {
        const findChat = participants.find(val => Number(val.id) === Number(router.query?.threadId));
        if(findChat) {
          setActive(findChat);
          updateParticipant(findChat);
        }
      }
    }, [router?.query, participants]);

  useEffect(() => {
    if(active) {
      const user = cookies.get("id");
      const messagesRef = ref(db, `chat_thread/${user}`);

      onValue(messagesRef, (snapshot) => {
        var data = [];
        snapshot.forEach((element) => {
          data.push(element.val());
        });
        if (data.length) {
          data = data.slice().sort((a, b) => b.last_timestamp - a.last_timestamp)
          api.updateReadStatus({thread_id: active?.id}).then((res) => {
          });
          setParticipants(data);
        }
      });
    }
  }, [updateMsg]);

  useEffect(() => {
    if(active && active.id) {
      api.updateReadStatus({thread_id: active?.id}).then((res) => {
      })
    }
  }, [active]);

  const getParticipants = async () => {
    const user = cookies.get("id");
    const messagesRef = ref(db, `chat_thread/${user}`);

    await onValue(messagesRef, async (snapshot) => {
      var data = [];
      snapshot.forEach((element) => {
        let e = element.val()
        data.push(e);
        console.log("last_timestamp",e.last_timestamp)
      });
      if(data.length) {
        data = data.slice().sort((a, b) => Number(b?.last_timestamp ?? '9665480069') - Number(a.last_timestamp ?? '9665480069') )

        await setParticipants(data);
        if(router.query.id) {
          data.map((d, index) => {
            if (d.user.id == router.query.id) {
              setActive(data[index]);
              updateParticipant(data[index]);
              return;
            }
          })
        }else {
          setActive(data[0]);
          updateParticipant(data[0]);
        }
      }
    });
  };

  const handleSearch = (e) => {
    let data = [];
    if(e.target.value) {
      setSearchedText(e.target.value)
      data = participants.filter((val) =>{
        let name = val.user.firstname + " " + val.user.lastname
        let filterVal = e.target.value
        return (name).toLowerCase().indexOf(filterVal.toLowerCase()) > -1
      })
      setSearchData(data)
    }else {
      setSearchedText('')
      setSearchData([])
    }
  };

  return (
      <div className="col-md-6 col-lg-3 col-xl-3 col-sm-3 pd-right participant px-md-3 px-0" id="search">
        <div className="bg-white main-background chat-content">
          <div className="search-sitters mb-4 mt-3 mt-md-0">
            <input
                className="form-control"
                placeholder="Search sitter"
                type="text"
                value={searchedText}
                onChange={handleSearch}
            />
          </div>
          <div className="scrollbar">
            {(searchData.length ? searchData : participants).map((value, index) => (
                <div key={index} className="">
                  <a
                      onClick={() => {
                        updateParticipant(value);
                        setActive(value);
                      }}
                      className="sitters-name"
                  >
                    <div className="row">
                      <div className="col">
                        <div className="d-flex">
                          <div className="invited-img">
                            <img
                                src={value.user.profile_picture}
                                className="img-fluid"
                            />
                          </div>
                          <div className="my-auto ml-3">
                            <h6 className={"font-14 mb-0 chat-titles ml-0 " + (active?.user?.id == value.user.id ? 'green':'')}>{value.user.firstname + " " + value.user.lastname}&nbsp;&nbsp;
                              {/* {searchData.length ? searchData.length-2 <= index && console.log(value) : participants.length-2 <= index && console.log(value)} */}
                              {value.last_timestamp ? (
                                  <span className="font-10 text-muted">
                                    {formatDistance(
                                        new Date(Number(value.last_timestamp) * 1000),
                                        new Date(),
                                        {
                                          addSuffix: true,
                                        }
                                    )}
                                  </span>
                              ) : null}
                            </h6>
                            <p className="font-12 mb-0">{value.last_message ? value.last_message.includes('http') ? strings.sentAnAttachment : value.last_message : ''}</p>
                          </div>
                        </div>
                      </div>
                      {value.unread_count > 0 && active && active.id != value.id ? (
                          <div className="col-auto">
                            <div className="chat-count my-auto">
                              <h6>{value.unread_count}</h6>
                            </div>
                          </div>
                      ) : null}
                    </div>
                  </a><hr/>
                </div>
            ))}
          </div>
        </div>
      </div>
  );
};

export default ChatParticipants;
