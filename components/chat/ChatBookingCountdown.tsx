import React, { useState, useEffect, useMemo } from 'react';
import moment from 'moment';

const ChatBookingCountdown = ({ time }) => {
    const [timeRemaining, setTimeRemaining] = useState<any>('12:00:00');

    const targetTime = time ? moment(time).add(12, 'hours') : '';

    useEffect(() => {
        if (targetTime) {
            const countdown = setInterval(() => {
                const now = moment();
                const timeRemaining = moment.duration(targetTime.diff(now));

                if (timeRemaining.asSeconds() <= 0) {
                    clearInterval(countdown);
                    setTimeRemaining('00:00:00');
                } else {
                    const hours = timeRemaining.hours();
                    const minutes = timeRemaining.minutes();
                    const seconds = timeRemaining.seconds();
                    setTimeRemaining(`${hours}:${minutes}:${seconds}`);
                }
            }, 1000);

            return () => {
                clearInterval(countdown);
            };
        }
    }, [targetTime]);

    return (
        <React.Fragment>Time remaining : <strong> {timeRemaining}</strong></React.Fragment>
    );
};

export default ChatBookingCountdown;