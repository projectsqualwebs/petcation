import React, {useEffect, useMemo, useState} from "react";
import Select from "react-select";
import {strings} from "../../public/lang/Strings";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import {addDays, addMonths} from "date-fns";
import API from "../../api/Api";
import {useRouter} from "next/router";
import {I_SINGLE_SITTER} from "../../models/sitter.interface";
import {pets, petSize} from "../../public/appData/AppData";
import {D_FLEXIBILITY, D_FREQUENT_BREAKS,} from "../../public/appData/StaticData";
import {getCurrencySign} from "../../api/Constants";
import moment from "moment";
import OverlayLoader from "../common/OverlayLoader";

interface I_PROPS {
  services: {
    service: {
      id: number;
      name: string;
    };
  }[];
  state: I_SINGLE_SITTER;
  sitterName: string;
  sitterId: any;
}

interface I_SERVICE_PETS {
  custom_services: {
    id: number;
    name: string;
    description: string;
    price: number;
  }[];
  fees: {
    capacity: number;
    earning_amount: number;
    id: number;
    pet_size_id?: number;
    medical_service_id?: number;
    grooming_service_id?: number;
    service_charge: number;
  }[];
  id: number;
  pet_type: number;
}

interface I_SERVICE_PREFERENCE {
  flexibility_id?: number;
  frequently_break_id?: number;
  has_transportation_fee?: number;
  pickup_from_client_home?: number;
  remarks?: string;
  visit_per_day?: number;
  early_visit_provided?: number;
  dogs_walk_together?: number;
  extended_charges_applied?: number;
  extend_walk?: number;
  extended_price_per_mint?: string;
}

const api = new API();
export default function SitterAvailablity({ services, state, sitterName, sitterId }: I_PROPS) {
  const router = useRouter();
  const [type, setType] = useState<0 | 1>(1);
  const [disabledDates, setDisabledDates] = useState<any>([]);
  const [selectedService, setSelectedService] = useState<any>(services[0]);
  const [cancellationPolicy, setCancellationPolicy] = useState<number>();
  const [servicePets, setServicePets] = useState<I_SERVICE_PETS[]>();
  const [servicePreference, setServicePreference] =
    useState<I_SERVICE_PREFERENCE>();
  const [serviceId, setServiceId] = useState<number>(1);
  const [houseCallServices, setHouseCallServices] = useState([]);
  const [dogService, setDogService] = useState<any>([]);
  const [catService, setCatService] = useState<any>([]);
  const [birdService, setBirdService] = useState<any>([]);
  const [reptileService, setReptileService] = useState<any>([]);
  const [smallAnimalService, setSmallAnimalService] = useState<any>([]);
  const [serviceLoading, setServiceLoading] = useState(true);

  useEffect(() => {
    getServiceAvailability();
  }, [selectedService, type]);

  const getServiceByPet = (id: number) => {
    api
      .getHouseCallService({
        pet_type: id,
      })
      .then(async (res) => {
        switch (id) {
          case 1:
            setDogService(res.data.response);
            break;
          case 2:
            setCatService(res.data.response);
            break;
          case 3:
            setBirdService(res.data.response);
            break;
          case 4:
            setReptileService(res.data.response);
            break;
          case 5:
            setSmallAnimalService(res.data.response);
            break;
        }
      })
      .catch((error) => console.log(error));
  };
  const getGroomingServiceByPet = (id: number) => {
    api
      .getGroomingService({
        pet_type: id,
      })
      .then(async (res) => {
        switch (id) {
          case 1:
            setDogService(res.data.response);
            break;
          case 2:
            setCatService(res.data.response);
            break;
          case 3:
            setBirdService(res.data.response);
            break;
          case 4:
            setReptileService(res.data.response);
            break;
          case 5:
            setSmallAnimalService(res.data.response);
            break;
        }
        console.log("services", services);
      })
      .catch((error) => console.log(error));
  };
  console.log("custom sevices", type);
  function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
      dateArray.push(new Date(currentDate));
      currentDate = addDays(currentDate, 1);
    }
    return dateArray;
  }

  const getServiceAvailability = () => {
    setServiceLoading(true);
    let data = {
      sitter_id: router.query.id,
      service_id: selectedService.service.id,
      is_available: type,
    };
    api
      .getSitterAvailability(data)
      .then(async (res) => {
        let dates = res.data.response.services.map(
          (value) => new Date(value.ScheduleDate)
        );
        let preference = res.data.response.preferences;
        if (selectedService.service.name == strings.Boarding) {
          await setServiceId(1);
          await setCancellationPolicy(
            preference.boarding_service_fee.cancellation_policy
          );
          await setServicePets(preference.boarding_service_fee.service_pets);
          await setServicePreference(preference.boarding_preference);
          setHouseCallServices([]);
        }
        if (selectedService.service.name == strings.HouseSitting) {
          await setServiceId(2);
          setCancellationPolicy(
            preference.house_sitting_service_fee.cancellation_policy
          );
          setServicePets(preference.house_sitting_service_fee.service_pets);
          setServicePreference(preference.house_sitting_service_preference);
        }
        if (selectedService.service.name == strings.dropInVisits) {
          await setServiceId(3);
          setCancellationPolicy(
            preference.drop_in_visit_service_fee.cancellation_policy
          );
          setServicePets(preference.drop_in_visit_service_fee.service_pets);
          setServicePreference(preference.drop_in_visit_service_preference);
        }
        if (selectedService.service.name == strings.petDayCare) {
          await setServiceId(4);
          setCancellationPolicy(
            preference.day_care_service_fee.cancellation_policy
          );
          setServicePets(preference.day_care_service_fee.service_pets);
          setServicePreference(preference.day_care_service_preference);
        }
        if (selectedService.service.name == strings.dogWalking) {
          await setServiceId(5);
          setCancellationPolicy(
            preference.pet_walking_service_fee.cancellation_policy
          );
          preference.pet_walking_service_fee
            ? setServicePets(preference.pet_walking_service_fee.service_pets)
            : setServicePets([]);
          setServicePreference(preference.pet_walking_service_preference);
        }
        if (selectedService.service.name == strings.grooming) {
          await setServiceId(6);
          await preference.grooming_service_fee.service_pets?.map(
            async (val) => await getGroomingServiceByPet(val?.pet_type)
          );
          await setCancellationPolicy(
            preference.grooming_service_fee.cancellation_policy
          );
          await setServicePets(preference.grooming_service_fee.service_pets);
          await setServicePreference(preference.grooming_service_preference);
        }
        if (selectedService.service.name == strings.houseCall) {
          await setServiceId(7);
          await preference.house_call_service_fee.service_pets?.map(
            async (val) => await getServiceByPet(val?.pet_type)
          );
          await setCancellationPolicy(
            preference.house_call_service_fee.cancellation_policy
          );
          await setServicePets(preference.house_call_service_fee.service_pets);
          await setServicePreference(preference.house_call_service_preference);
        }
        let data = getDates(new Date(), addMonths(new Date(), 2));
        let newData = data.filter((date) => {
          if (
            !dates.find(
              (d) =>
                moment(d).format("D/M/YYYY") === moment(date).format("D/M/YYYY")
            )
          ) {
            return true;
          }
        });
        setDisabledDates(newData);
        setServiceLoading(false);
      })
      .catch((error) => {
        setServiceLoading(false);
      });
  };
  const serviceView = useMemo(() => {
    return (
      <div>
        {servicePets && servicePets.length
          ? servicePets.map((service, index) => (
              <>
                <div className="px-0 col-12 d-flex mb-0">
                  <div className="row">
                    <div className="col-12 px-3 pt-3 pb-0 host-details d-flex ">
                      <div className="img">
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="cat"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                          className="m-0">
                          <path
                            fill="currentColor"
                            d="M290.59 192c-20.18 0-106.82 1.98-162.59 85.95V192c0-52.94-43.06-96-96-96-17.67 0-32 14.33-32 32s14.33 32 32 32c17.64 0 32 14.36 32 32v256c0 35.3 28.7 64 64 64h176c8.84 0 16-7.16 16-16v-16c0-17.67-14.33-32-32-32h-32l128-96v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V289.86c-10.29 2.67-20.89 4.54-32 4.54-61.81 0-113.52-44.05-125.41-102.4zM448 96h-64l-64-64v134.4c0 53.02 42.98 96 96 96s96-42.98 96-96V32l-64 64zm-72 80c-8.84 0-16-7.16-16-16s7.16-16 16-16 16 7.16 16 16-7.16 16-16 16zm80 0c-8.84 0-16-7.16-16-16s7.16-16 16-16 16 7.16 16 16-7.16 16-16 16z"></path>
                        </svg>
                      </div>
                      <div className="col pl-0">
                        <h6 className="mb-1 mt-1 font-semibold">
                          {
                            pets.find((pet) => service.pet_type == pet.value)
                              .label
                          }
                        </h6>
                        <div className="col-12 pt-1 pb-3 weight-pets px-2">
                          {serviceId === 1 ||
                          serviceId == 2 ||
                          serviceId === 3 ||
                          serviceId == 4 ||
                          serviceId == 5 ? (
                            <div className="row">
                              {/*<div className="col-auto pr-0">*/}
                              {/*    <p className="mb-0 font-normal">{strings.Weight}:</p>*/}
                              {/*</div>*/}
                              {service.fees.length &&
                                service.fees.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {
                                        petSize.find(
                                          (size) =>
                                            fee.pet_size_id == size.value
                                        )?.label
                                      }
                                    </p>
                                  </div>
                                ))}
                            </div>
                          ) : null}
                          {serviceId === 7 ? (
                            <div className="row">
                              <div className="col-auto pr-0">
                                <p className="mb-0 font-normal">
                                  {strings.Service}:
                                </p>
                              </div>
                              {service.pet_type == 1 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {dogService?.length
                                        ? dogService?.find(
                                            (val) =>
                                              fee?.medical_service_id == val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 2 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {catService?.length
                                        ? catService?.find(
                                            (val) =>
                                              fee?.medical_service_id == val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 3 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {birdService?.length
                                        ? birdService?.find(
                                            (val) =>
                                              fee?.medical_service_id == val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 4 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {reptileService?.length
                                        ? reptileService?.find(
                                            (val) =>
                                              fee?.medical_service_id == val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 5 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {smallAnimalService?.length
                                        ? smallAnimalService?.find(
                                            (val) =>
                                              fee?.medical_service_id == val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                            </div>
                          ) : null}
                          {serviceId === 6 ? (
                            <div className="row">
                              <div className="col-auto pr-0">
                                <p className="mb-0 font-normal">
                                  {strings.Service}:
                                </p>
                              </div>
                              {service.pet_type == 1 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {dogService?.length
                                        ? dogService?.find(
                                            (val) =>
                                              fee?.grooming_service_id ==
                                              val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 2 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {catService?.length
                                        ? catService?.find(
                                            (val) =>
                                              fee?.grooming_service_id ==
                                              val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 3 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {birdService?.length
                                        ? birdService?.find(
                                            (val) =>
                                              fee?.grooming_service_id ==
                                              val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 4 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {reptileService?.length
                                        ? reptileService?.find(
                                            (val) =>
                                              fee?.grooming_service_id ==
                                              val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                              {service.pet_type == 5 &&
                                service?.fees?.length &&
                                service?.fees?.map((fee, index) => (
                                  <div className="col-auto text-center border-right host-details mb-1">
                                    <p className="mb-0 font-medium">
                                      {smallAnimalService?.length
                                        ? smallAnimalService?.find(
                                            (val) =>
                                              fee?.grooming_service_id ==
                                              val?.id
                                          )?.name
                                        : null}
                                    </p>
                                  </div>
                                ))}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {servicePets.length != index + 1 && <hr className="m-0" />}
              </>
            ))
          : null}
      </div>
    );
  }, [
    serviceId,
    servicePets,
    dogService,
    catService,
    birdService,
    reptileService,
    smallAnimalService,
  ]);

  const preferenceView = useMemo(() => {
    return servicePreference ? (
      <div className="prefer-content">
        <h6>{strings.PreferencesDetails}</h6>
        {serviceId === 1 || serviceId === 2 || serviceId === 4 ? (
          <div className="prefer-inner-details">
            <p>
              {strings.YourAvailabilityWhilePetIsAt +
                selectedService?.service?.name}
            </p>
            <h6>
              {
                D_FLEXIBILITY.find(
                  (val) => val.value == servicePreference?.flexibility_id
                )?.label
              }
            </h6>
          </div>
        ) : null}

        {serviceId === 1 || serviceId === 2 || serviceId === 4 ? (
          <div className="prefer-inner-details">
            <p>
              {
                strings.whenhostingpetsinyourhomehowfrequentlycanyouprovidebreaks
              }
            </p>
            <h6>
              {
                D_FREQUENT_BREAKS.find(
                  (val) => val.value == servicePreference?.frequently_break_id
                )?.label
              }
            </h6>
          </div>
        ) : null}
        {serviceId === 7 || serviceId === 6 ? (
          <div className="prefer-inner-details">
            <p>{strings.Remarks}</p>
            <h6>{servicePreference?.remarks}</h6>
          </div>
        ) : null}
        {serviceId === 3 ? (
          <div className="prefer-inner-details">
            <p>{strings.HowManyVisitsCanYouProvidePerDay_Q}</p>
            <h6>
              {servicePreference?.visit_per_day === 1
                ? strings.Time1
                : servicePreference?.visit_per_day === 2
                ? strings.Times2
                : strings.Times3}
            </h6>
          </div>
        ) : null}
        {serviceId === 3 ? (
          <div className="prefer-inner-details">
            <p>{strings.DoYouProvideEarlyOrLateVisits}</p>
            <h6>
              {servicePreference?.early_visit_provided == 1
                ? strings.Before8am
                : strings.after6pm}
            </h6>
          </div>
        ) : null}
        {serviceId === 5 ? (
          <div className="prefer-inner-details">
            <p>{strings.HowMayDogsYouCanTakeForWalkTogether}</p>
            <h6>{servicePreference?.dogs_walk_together}</h6>
          </div>
        ) : null}
        {serviceId === 5 ? (
          <div className="prefer-inner-details">
            <p>
              {
                strings.WouldYouAllowToExtendWalkTimingsIfAskedAfterReachingClientsPlace
              }
            </p>
            <h6>{servicePreference?.extend_walk ? strings.Yes : strings.No}</h6>
          </div>
        ) : null}
        {serviceId === 5 && servicePreference?.extend_walk === 1 ? (
          <div className="prefer-inner-details">
            <p>{strings.AdditionChargesForExtendingTime}</p>
            <h6>
              {servicePreference?.extended_charges_applied
                ? servicePreference.extended_price_per_mint
                : null}
              {" " + getCurrencySign()}
            </h6>
          </div>
        ) : null}

        <hr />
      </div>
    ) : null;
  }, [servicePreference, serviceId]);

  //const handleDayClick = (day) => {
  // router.push({
  //   pathname: "/booking",
  //   query: {
  //    sitterId: router.query.id,
  //    service: router.query.serviceId,
  //     petType: type,
  //  },
  // });
  //};

  const handleDayClick:any = (day, modifiers) => {
    // Check if the clicked date is not disabled
    if (!modifiers?.disabled) {
      // Navigate to the booking page with the sitterId, name, and service query params
      let query = {};
      if (serviceId == 5 || serviceId == 3) {
        query = {
          sitterId: router.query.id,
          service: router.query.serviceId,
          name: sitterName,
          duration: `30`
        }
      } else {
        query = {
          sitterId: router.query.id,
          service: router.query.serviceId,
          name: sitterName
        }
      }

      router.push({
        pathname: "/booking",
        query: query,
      });
    }
  };

  return (
    <div>
      {serviceLoading ? <OverlayLoader /> : null}
      <div className="bg-white main-background pb-0">
        <h5 className="font-semibold mb-3">{strings.Availability}</h5>
        <div className="form-group">
          <Select
            isSearchable={false}
            options={services}
            value={selectedService}
            onChange={setSelectedService}
            getOptionLabel={(option) => option.service.name}
            getOptionValue={(option) => option.service.id.toString()}
          />
        </div>
        {preferenceView}
        <div className="calendar mb-3 d-inline-block w-100 border-0">
          {disabledDates && (
            <DayPicker
              className="Selectable w-100 px-0 mx-0"
              numberOfMonths={1}
              fromMonth={new Date()}
              toMonth={addMonths(new Date(), 2)}
              selectedDays={null}
              disabledDays={[
                ...disabledDates,
                {
                  before: new Date(),
                },
              ]}
              onDayClick={handleDayClick}
            />
          )}
        </div>
      </div>
      <div className="bg-white main-background policy">
        <h5 className="font-semibold mb-0">
          {`${selectedService.service.name} ${strings.CancellationPolicy}`}
        </h5>
        <hr />
        {cancellationPolicy == 1 && (
          <>
            <h6>{strings.Flexible}</h6>
            <li>
              {
                strings.YouWillGetAFullRefundIfYouCancelBeforeTheDaysServiceIsDelivered
              }
            </li>
            <li>
              {
                strings.IfYouCancelAftertheStayOrWalkBeginsYouWillGetA50RefundFor
              }
            </li>
          </>
        )}
        {cancellationPolicy == 2 && (
          <>
            <h6>{strings.Moderate}</h6>
            <li>{strings.YouCancelWithin48HoursOfBooking}</li>
            <li>{strings.TheReservationYouAreCancellingDoesnotOverlapWith}</li>
            <li>{strings.IfYouCancelAfter1200Noon3DaysBeforeTheStayBegins}</li>
          </>
        )}
        {cancellationPolicy == 3 && (
          <>
            <h6>{strings.Strict}</h6>
            <li>
              {strings.IfYouCancelAfter1200NoonOneWeekBeforeTheStayBegins}
            </li>
          </>
        )}
      </div>
      <div className="bg-white main-background pb-0">
        <h5 className="font-semibold mb-0">
          {`${state.firstname} ${strings.canHost}`}
        </h5>
        <hr className="mb-0" />
        {serviceView}
      </div>
    </div>
  );
}
