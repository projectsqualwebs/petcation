import React , {useState} from "react";
import { LightgalleryProvider, LightgalleryItem, withLightgallery, useLightgallery } from "react-lightgallery";
import "lightgallery.js/dist/css/lightgallery.css";

interface Iprops {
  images: any;
}

export default function GalleryView(props: Iprops) {
const [imgArr, setImgArr] = useState(props.images.map((val) => val.path));
  const getImages = () => {

    let array = [];
    for (let i = 0; i < imgArr.length; i++) {
      if (i === 4) {
        array.push(
            <div
                key={`image_${i}`}
                style={{ display: "block" }}
                className="col-md-6 col-sm-3 holder-sm col-6 image-card"
            >
              <LightgalleryItem group={imgArr} src={imgArr[i]} thumb={imgArr[i]}>
                {imgArr.length > 5 ? <div className='show-image-plus-option m-0 p-0'>
                  <img className="img-fluid" style={{opacity: 0.2}} src={imgArr[i]} alt="img" />
                  <span className='fw-bold'>+ {imgArr.length - 5}</span>
                </div> : <img className="img-fluid" src={imgArr[i]} alt="img" />}
              </LightgalleryItem>
            </div>
        );
      } else {
        array.push(
            <div
                key={`image${i}`}
                style={{ display: i == 0 || i > 4 ? "none" : "block" }}
                className="col-md-6 col-sm-3 holder-sm col-6 image-card"
            >
              <LightgalleryItem group={imgArr} src={imgArr[i]} thumb={imgArr[i]}>
                <img className="img-fluid" src={imgArr[i]} alt="img" />
              </LightgalleryItem>
            </div>
        );
      }
    }
    return array;
  };
  return (
    <div className="bg-white main-background gallery-grid">
    {console.log("props",props.images)}
      <LightgalleryProvider>
        <div className="row">
          <div className="col-md-6 col-sm-12 card-holder">
            <div className="row">
              <div className="col-12  image-card pr-1">
                <LightgalleryItem
                    group={imgArr}
                    src={props.images[0].path}
                    thumb={props.images[0].path}
                >
                  <img className="img-fluid cursor-pointer" src={props.images[0].path}
                       alt="img" />
                </LightgalleryItem>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 d-sm-block d-none card-holder">
            <div className="row cursor-pointer">{getImages()}</div>
          </div>
        </div>
      </LightgalleryProvider>
    </div>
  );
}
