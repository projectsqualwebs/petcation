import React from "react";
import { strings } from "../../public/lang/Strings";

interface IProps {
  pets: any;
}

const MyPets: React.FC<IProps> = (props: IProps) => {
  return (
    <div className="bg-white main-background">
      <h5 className="font-semibold mb-3">{strings.Mypets}</h5>
      <div className="row">
        {props.pets ? props.pets.map((value, index) => {
          return (
            <div key={index} className="col-12 col-md-6 col-lg-6 col-xl-6">
              <div className="mypet-details mb-4">
                <div className="mb-2 position-relative img-pet">
                  <img src={value.pet_image} className="img-fluid rounded-lg w-100" />
                </div>
                  <div className="col-sm-6 p-left my-auto d-none d-md-block d-lg-block d-xl-block">
                      <div className="mypet-details">
                          <h6 className="mb-0 font-20">{value.pet_name}</h6>
                          <p>{value.breed.breed}</p>
                          <p className="mb-0">{value.age_year + " " + strings.yearsOld}</p>
                          <p className="mb-0">{value.weight.name}</p>
                      </div>
                  </div>
              </div>
            </div>
          );
        }):<div className="text-center padding">
            <p className="font-13 mb-0 font-italic">{strings.noPets}</p>
        </div>}
      </div>
    </div>
  );
};

export default MyPets;
