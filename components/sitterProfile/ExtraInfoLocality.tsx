import { strings } from "../../public/lang/Strings";
import React from "react";

export default function ExtraInfoLocality(props: any) {
  return (
    <div className="bg-white main-background about-place">
      <h5 className="font-semibold mb-0">
        {strings.Extrainfoaboutlocality}
      </h5>
        <hr/>
      <div className="d-flex flex-column loc-height scr-custom">
          <p className="locality-describe">{props.address.description}</p>
        {props.address.live_in_house == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.LivesInAHouse}</p> : null}
        {props.address.non_smoking_household == 1 ? (
            <p className="d-inline-flex align-items-baseline mb-2" >{strings.NonSmokingHousehold}</p>
        ) : null}
        {props.address.no_children_present == 1 ? (
            <p className="d-inline-flex align-items-baseline mb-2" >{strings.NoChildrenPresent}</p>
        ) : null}
        {props.address.fenced_yard == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.HasAFencedYard}</p> : null}
        {props.address.dog_other_pets == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.HasPet}</p> : null}
        {/*new points*/}
          {props.address.accept_only_on_client_at_time == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.AcceptOnlyOneClientAtATime}</p> : null}
          {props.address.dogs_on_bed == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.DogsAllowedOnBed}</p> : null}
          {props.address.dogs_on_furniture == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.DogsAllowedOnFurniture}</p> : null}
          {props.address.does_not_own_a_cat == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.DoesNotOwnACat}</p> : null}
          {props.address.does_not_own_a_dog == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.DoesNotOwnADog}</p> : null}
          {props.address.does_not_own_caged_pets == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.DoesNotOwnCagedPets}</p> : null}
          {props.address.dogs_allowed_on_bed == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.DogsAllowedOnBed}</p> : null}
          {props.address.dogs_allowed_on_furniture == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.DogsAllowedOnFurniture}</p> : null}
          {props.address.no_children_5_year_old == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.NoChildren05YearsOld}</p> : null}
          {props.address.no_children_12_year_old == 1 ? <p className="d-inline-flex align-items-baseline mb-2" >{strings.NoChildren612YearsOld}</p> : null}
      </div>
    </div>
  );
}
