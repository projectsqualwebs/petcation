import { strings } from "../../public/lang/Strings";
import ReviewObject from "./ReviewObject";
import React, { useState } from "react";
import {
  reviewObjectData,
  reviewStateData,
} from "../../public/appData/AppData";
import RateSitter from "../common/RateSitter";
import Link from "next/link";
import {AVG_RATING, MY_REVIEWS, OWNER_REVIEWS} from "../../models/sitter.interface";
import {json} from "stream/consumers";
import moment from "moment";
import RatingStars from "../common/RatingStars";
import Cookies from "universal-cookie";

interface I_PROPS {
  reviews: MY_REVIEWS[];
  average: AVG_RATING;
  sitterDetails: {
    id: number;
    profile_picture: string;
    firstName: string;
    lastName: string;
  }
  overallRate: number;
  updateSitterProfile: () => void;
  ownerReview: OWNER_REVIEWS[];
}

const Reviews = ({reviews, average, sitterDetails, updateSitterProfile, ownerReview, overallRate}: I_PROPS) => {
  const [press, setPress] = useState<boolean>(true)
  const cookie = new Cookies();

  const OverallRating = (data) => {
    let total = Number(data.cleanliness) + Number(data.accuracy) + Number(data.communication) + Number(data.location) + Number(data.check_in) + Number(data.value);
    return (total/6).toFixed(1)
  };

  return (
    <div className="bg-white main-background">
                  <div className="pay-tabs ">
                    <ul className="nav nav-tabs mb-0" id="myTab" role="tablist">
                      <li className="nav-item cursor-pointer" role="presentation">
                        <a
                            className={press === true ? "nav-link active" : "nav-link"}
                            style={{ position: "relative" }}
                            id="cards-tab"
                            data-toggle="tab"
                            onClick={() => { setPress(true) }}
                            role="tab"
                        >
                          {strings.SitterReview}
                        </a>
                      </li>
                      <li className="nav-item cursor-pointer" role="presentation">
                        <a
                            className={press === false ? "nav-link active" : "nav-link"}
                            id="bank-tab"
                            data-toggle="tab"
                            onClick={() => { setPress(false) }}
                            role="tab"
                        >
                          {strings.OwnerReview}
                        </a>
                      </li>
                    </ul>
                  </div>
                  {/*------------review details--------------------*/}
                  <div className="release-content pt-3">
                    {press == true ? <>
                          {average?.value ? <div className="col-12 mb-3">
                            <div className="basic-info">
                              <div className="row align-items-center">
                                <div className="col-12 col-md-3 col-lg-3 col-xl-3">
                                  <div className="overall-ratings">
                                    <div>
                                      <h4 className="font-semibold">{OverallRating(average)}</h4>
                                      <p className="mb-0 font-14 font-semibold text-white">
                                        {strings.Overallrating}
                                      </p>
                                    </div>
                                  </div>
                                </div>

                                <div className="col-12 col-md-9 col-lg-9 col-xl-9">
                                  <div className="ratings-score">
                                    <div className="row">
                                      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="score score1">
                                          <div className="score-details">
                                            <h6 className="mb-0">
                                              {strings.Cleanliness}
                                              <span>{parseInt(average.cleanliness) + "/ 5"}</span>
                                            </h6>
                                          </div>
                                          <div className="score-bar">
                                            <div className="rSlider">
                                              <span
                                                  className="slide"
                                                  style={{
                                                    width: `${parseInt(average.cleanliness) * 20}%`,
                                                  }}
                                              ></span>
                                              <input id="range" type="range" min="0" max="5" />
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="score score1">
                                          <div className="score-details">
                                            <h6 className="mb-0">
                                              {strings.Accuracy}
                                              <span>{parseInt(average.accuracy) + " / 5"}</span>
                                            </h6>
                                          </div>
                                          <div className="score-bar">
                                            <div className="rSlider">
                                              <span
                                                  className="slide"
                                                  style={{ width: `${parseInt(average.accuracy) * 20}%` }}
                                              ></span>
                                              <input id="range" type="range" min="0" max="5" />
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="score score1">
                                          <div className="score-details">
                                            <h6 className="mb-0">
                                              {strings.Communication}
                                              <span>{parseInt(average.communication) + "/ 5"}</span>
                                            </h6>
                                          </div>
                                          <div className="score-bar">
                                            <div className="rSlider">
                                              <span
                                                  className="slide"
                                                  style={{
                                                    width: `${parseInt(average.communication) * 20}%`,
                                                  }}
                                              ></span>
                                              <input id="range" type="range" min="0" max="5" />
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="score score1">
                                          <div className="score-details">
                                            <h6 className="mb-0">
                                              {strings.Location}{" "}
                                              <span>{parseInt(average.location) + "/ 5"}</span>
                                            </h6>
                                          </div>
                                          <div className="score-bar">
                                            <div className="rSlider">
                                              <span
                                                  className="slide"
                                                  style={{ width: `${parseInt(average.location) * 20}%` }}
                                              ></span>
                                              <input id="range" type="range" min="0" max="5" />
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="score score1 mb-md-0">
                                          <div className="score-details">
                                            <h6 className="mb-0">
                                              {strings.Check_in}{" "}
                                              <span>{parseInt(average.check_in) + "/ 5"}</span>
                                            </h6>
                                          </div>
                                          <div className="score-bar">
                                            <div className="rSlider">
                                              <span
                                                  className="slide"
                                                  style={{ width: `${parseInt(average.check_in) * 20}%` }}
                                              ></span>
                                              <input id="range" type="range" min="0" max="5" />
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="score score1 mb-0">
                                          <div className="score-details">
                                            <h6 className="mb-0">
                                              {strings.Value}{" "}
                                              <span>{parseInt(average.value) + " / 5"}</span>
                                            </h6>
                                          </div>
                                          <div className="score-bar">
                                            <div className="rSlider">
                                              <span
                                                  className="slide"
                                                  style={{ width: `${parseInt(average.value) * 20}%` }}
                                              ></span>
                                              <input id="range" type="range" min="0" max="5" />
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div> : null}
                          {average?.value ? <hr /> : null}
                          <div className="sitter-review-section">
                            {reviews?.length ? reviews.slice(0,3).map((val)=> <ReviewObject value={val} sitterDetails={sitterDetails} isMe={val.sitter_id == cookie.get('id') ? true : false} updateSitterProfile={() => updateSitterProfile()} />) : <h5>{strings.ReviewsAreNotGivenYet}</h5>}

                            {reviews.length > 3 ? <div className="col-12 px-0 d-flex justify-content-between align-items-center">
                              <div className="read-more">
                                <p className="mb-0"><Link href={{pathname: '/sitter-profile/MyReviews', query: {id: sitterDetails.id}}}>{strings.ReadMoreReviews}</Link></p>
                              </div>
                            </div> : null}
                          </div>
                        </>
                     : <>
                        <div className="sitter-review-section">
                          {ownerReview?.length ? ownerReview.map((value)=> <div className="user-review-details">
                            <div className="row mb-2">
                              <div className="col-12 col-md">
                                <div className="d-flex">
                                  <div className="sitter-profile-img">
                                    <img src={value?.sitter_info?.profile_picture} className="img-fluid" />
                                  </div>
                                  <div className="sitter-review-details ml-2 my-auto">
                                    <h5 className="mb-0 font-medium">{value?.sitter_info?.firstname + ' ' + value?.sitter_info?.lastname}</h5>
                                    <p className="font-12 mb-0">
                                      {moment(value.created_at).format('DD-MM-YYYY') + " | " + moment(value.created_at).format('HH:MM a')}
                                    </p>
                                    <div className="d-flex rating-star">
                                      <RatingStars rating={value.rating} />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <p className="font-14 ">{value.review}</p>
                            <hr />
                          </div>) : <h5>{strings.ReviewsAreNotGivenYet}</h5>}

                          {ownerReview.length > 3 ? <div className="col-12 px-0 d-flex justify-content-between align-items-center">
                            <div className="read-more">
                              <p className="mb-0"><Link href={{pathname: '/sitter-profile/MyReviews', query: {id: sitterDetails.id}}}>{strings.ReadMoreReviews}</Link></p>
                            </div>
                          </div> : null}
                        </div>
                    </>}
                  </div>
                  {/*------------/press release details--------------------*/}
    </div>
  );
};

export default Reviews;
