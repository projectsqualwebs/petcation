import React, { useEffect, useState } from "react";
import { strings } from "../../public/lang/Strings";
import Link from "next/link";
import { Mailto } from "../../utils/Helper";
import { useRouter } from "next/router";
import { useSnackbar } from "react-simple-snackbar";
import { errorOptions, successOptions } from "../../public/appData/AppData";
import { getCurrencySign, U_FACEBOOK_ID } from "../../api/Constants";
import Cookies from "universal-cookie";
import { Dropdown, DropdownButton } from "react-bootstrap";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu/Menu";
import MenuItem from "@mui/material/MenuItem";
import CreateWishlist from "../common/CreateWishlist";
import API from "../../api/Api";
import boolean from "async-validator/dist-types/validator/boolean";
import ReportSitterModal from "../common/ReportSitterModal";

type IProps = {
  name: string;
  location: string;
  distance: string;
  responseRate: number;
  online: boolean;
  petTakenCare: number;
  happyCustomers: number;
  rehireRate: number;
  experience_in_month: number;
  experience_in_year: number;
  rating: number;
  review: number;
  profile_pic: string;
  responseWithin: string;
  jobCompletion: number;
  isFavorite: any;
  isVerified: number;
  hide_address: number;
  markUnmark?: any;
  id: number;
  serviceId: string;
  onAvailableClick: any;
  isSitter: boolean;
};
/**
 *
 * @param props contains all details of sitter which we want to render in our header component
 * @function will render header component of sitter profile contaning dynamic data.
 */
const SitterProfileHeader: React.FC<IProps> = (props: IProps) => {
  const api = new API();
  const cookie = new Cookies();
  const [copied, setCopied] = useState(false);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const router = useRouter();
  const [wishList, setWishlist] = useState<any>([]);
  const [wishlistShowModal, setWishlistShowModal] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<any>(false);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [reported, setReported] = useState<boolean>(false);
  const [showReportModal, setShowReportModal] = useState<boolean>(false);
  //Auth
  var token = cookie.get("token");
  useEffect(() => {
    getAllWishlist();

    if (props.id) {
      api
        .getReportedSitter(props.id)
        .then((res) => {
          setReported(res.data.response);
        })
        .catch((error) => console.log(error.response));
    }
  }, []);
  
  /**
   * @params e contains event which help in prevent default
   * @function will copy url of sitter profile.
   */
  const copyUrl = (e) => {
    e.preventDefault();
    const el = document.createElement("input");
    el.value = window.location.href;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopied(true);
    openSuccess("Copied to clipboard!");
  };

  /**
   * @params
   *        year: contains value of experience in year
   *        month: contains value of experience in month
   * @function will return different text as per our preference.
   */
  const getExperience = (year: any, month: any) => {
    if (year == 1) {
      return `Novice`;
    } else if (year == 3) {
      return `Intermediate`;
    } else if (year == 5) {
      return `Proficient`; // Advanced
    } else if (year == 7) {
      return `Expert`;
    } else if (year == 8) {
      return `${
        month + " " + strings.year + " " + strings.of + " " + strings.experience
      }`;
    } else {
      return "";
    }
  };

  const getAllWishlist = () => {
    api
      .getSitterLists()
      .then(async (res) => {
        await setWishlist(res.data.response);
      })
      .catch((err) => console.log(err.response.data.message));
  };

  const handleClick = (event) => {
    if (token) {
      setAnchorEl(event.currentTarget);
    } else {
      router.push("/signin").then((r) => {});
    }
  };
  const handleClose = () => {
    setAnchorEl(false);
  };

  const markAsFavourite = (listId, spotId) => {
    setAnchorEl(false);
    let data = {
      list_id: listId,
      sitter_id: spotId,
    };
    api
      .markSitterAsFavourite(data)
      .then((res) => {
        props.markUnmark();
        openSuccess(res.data.message);
      })
      .catch((err) => openError(err.response.data.message));
  };

  return (
    <div className="bg-white single-spot main-background">
      <div className="row">
        <div className="col-12 col-md-12 col-lg-8 col-xl-9">
          <div className="row">
            <div className="col-12 col-md-3 col-lg-3 col-xl-2">
              <div className="user-profile-img mx-auto">
                <img src={props.profile_pic} />
              </div>
            </div>
            <div className="col-12 col-md-9 col-lg-9 col-xl-10 my-2 my-md-0 pl-md-4">
              <div className="user-profile-details mx-0">
                <div className="col-12 mb-2 mb-md-0">
                  <div className="row align-items-center">
                    <div className="col-12 col-md-auto px-0 text-center text-md-left">
                      <h3 className="font-semibold mb-1 text-capitalize">
                        {props.name}
                      </h3>
                    </div>
                    {props.isSitter ? (
                      <div className="col-12 col-md-auto my-auto text-center text-md-left">
                        <img src="/images/icons2.png" />{" "}
                        <span>
                          {props.isVerified === 1
                            ? strings.PetcationVerified
                            : strings.PetcationNotVerified}
                        </span>
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="main-padding pb-2 text-md-left text-center">
                  {props.hide_address === 1 ? null : (
                    <>
                      {props.location ? (
                        <p className="text-muted font-14 mb-0">
                          <span>{props.location}</span>{" "}
                          <span>
                            {props.distance +
                              "  " +
                              strings.Kmsfromyourcurrentlocation}
                          </span>
                        </p>
                      ) : null}
                    </>
                  )}
                </div>
                <div className="col-12 featured-details my-auto d-block d-md-none">
                  <div className="d-flex justify-content-center hotel-rating sitter-rating">
                    <div className="rating-star d-flex">
                      {[1, 2, 3, 4, 5].map((val, index) => (
                        <div
                          key={index}
                          className={val <= props.rating ? "active" : ""}>
                          <svg
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fas"
                            data-icon="star"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 576 512"
                            className="svg-inline--fa fa-star fa-w-18 fa-2x">
                            <path
                              fill="currentColor"
                              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                              className=""></path>
                          </svg>
                        </div>
                      ))}
                    </div>
                    <div>
                      <h3 className="font-semibold mb-0">
                        {props.rating}{" "}
                        <span className="font-14 font-normal cursor-pointer">
                          <Link
                            href={{
                              pathname: "/sitter-profile/MyReviews",
                              query: { id: props.id },
                            }}>
                            {"(" + props.review + " reviews)"}
                          </Link>
                        </span>
                      </h3>
                    </div>
                  </div>
                </div>
                <div className="col-12 mb-2">
                  <div className="row justify-content-center justify-content-md-start">
                    {props.isSitter ? (
                      <div className="pl-0 col-md-auto col-auto rate-details">
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fal"
                          data-icon="check-double"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 448 512"
                          className="svg-inline--fa fa-check-double fa-w-14 fa-2x">
                          <path
                            fill="currentColor"
                            d="M444.96 159l-12.16-11c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L131.77 428 31.42 329c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L3.04 340C1.01 342.67 0 345.67 0 349s1.01 6 3.04 8l120.62 119c2.69 2.67 5.57 4 8.62 4s5.92-1.33 8.62-4l304.07-300c2.03-2 3.04-4.67 3.04-8s-1.02-6.33-3.05-9zM127.17 284.03c2.65 2.65 5.48 3.97 8.47 3.97s5.82-1.32 8.47-3.97L365.01 63.8c1.99-2 2.99-4.65 2.99-7.96s-1-6.29-2.99-8.94l-11.96-10.93c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97L135.14 236.34l-72.25-72.03c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97l-11.96 10.93C33 177.89 32 180.87 32 184.18s1 5.96 2.99 7.95l92.18 91.9z"
                            className=""></path>
                        </svg>
                        <p className="d-inline font-14">
                          &nbsp;
                          {getExperience(
                            props.experience_in_year,
                            props.experience_in_month
                          )}
                        </p>
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-md-4 col-lg-4 col-xl-3 my-auto">
          <div className="col-12">
            <div className="row align-items-center justify-content-between">
              <div className="col-auto px-0">
                <div className="px-0 col-12 d-flex flex-md-column flex-row">
                  <div className="contact-now instant-button">
                    {props.isSitter && props.serviceId && (
                      <Link
                        href={{
                          pathname: "/booking",
                          query: {
                            sitterId: props.id,
                            name: props.name,
                            service: props.serviceId,
                          },
                        }}>
                        <button className="btn btn-primary">
                          {strings.Contactnow}
                        </button>
                      </Link>
                    )}
                    {/*<button className="btn btn-primary">Instant Book</button>*/}
                  </div>
                 
                  {props.isSitter ? (
                    <div className="text-center cursor-pointer availability">
                      <a onClick={props.onAvailableClick} className="font-14">
                        {strings.Seeavailability}
                      </a>
                    </div>
                  ) : null}

                  <div className="col-auto pr-2 pl-0 my-2"></div>
                </div>
              </div>
              <div className="col-auto px-0 user-profile-icon">
                <div className="d-flex profile-share-icon">
                  {props.serviceId && token && (
                    <div>
                      <Button
                        id="basic-button1"
                        aria-controls={open ? "basic-menu1" : undefined}
                        aria-haspopup="true"
                        className={"p-0s"}
                        aria-expanded={open ? "true" : undefined}
                        onClick={handleClick}>
                        <div className="single-share-details cursor-pointer">
                          {props ? (
                            <a className="icon-share-single">
                              {props?.isFavorite && (
                                <svg viewBox="0 0 512 512">
                                  <path
                                    fill={"#20847e"}
                                    d="M0 190.9V185.1C0 115.2 50.52 55.58 119.4 44.1C164.1 36.51 211.4 51.37 244 84.02L256 96L267.1 84.02C300.6 51.37 347 36.51 392.6 44.1C461.5 55.58 512 115.2 512 185.1V190.9C512 232.4 494.8 272.1 464.4 300.4L283.7 469.1C276.2 476.1 266.3 480 256 480C245.7 480 235.8 476.1 228.3 469.1L47.59 300.4C17.23 272.1 .0003 232.4 .0003 190.9L0 190.9z"
                                  />
                                </svg>
                              )}
                              {!props?.isFavorite && (
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 512 512">
                                  <path d="M244 84L255.1 96L267.1 84.02C300.6 51.37 347 36.51 392.6 44.1C461.5 55.58 512 115.2 512 185.1V190.9C512 232.4 494.8 272.1 464.4 300.4L283.7 469.1C276.2 476.1 266.3 480 256 480C245.7 480 235.8 476.1 228.3 469.1L47.59 300.4C17.23 272.1 0 232.4 0 190.9V185.1C0 115.2 50.52 55.58 119.4 44.1C164.1 36.51 211.4 51.37 244 84C243.1 84 244 84.01 244 84L244 84zM255.1 163.9L210.1 117.1C188.4 96.28 157.6 86.4 127.3 91.44C81.55 99.07 48 138.7 48 185.1V190.9C48 219.1 59.71 246.1 80.34 265.3L256 429.3L431.7 265.3C452.3 246.1 464 219.1 464 190.9V185.1C464 138.7 430.4 99.07 384.7 91.44C354.4 86.4 323.6 96.28 301.9 117.1L255.1 163.9z" />
                                </svg>
                              )}
                            </a>
                          ) : null}
                          &nbsp;
                        </div>
                      </Button>
                      <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={anchorEl}
                        onClose={handleClose}
                        MenuListProps={{
                          "aria-labelledby": "basic-button",
                        }}>
                        {wishList.map((val) => (
                          <MenuItem
                            onClick={() => markAsFavourite(val.id, props.id)}>
                            {val.name}
                          </MenuItem>
                        ))}
                        <MenuItem
                          onClick={() => {
                            setWishlistShowModal(true);
                            handleClose();
                          }}>
                          {strings.CreateNewList}
                        </MenuItem>
                      </Menu>
                    </div>
                  )}
                  <div className="profile-share-drop">
                    <DropdownButton
                      className="bg-transparent"
                      align="end"
                      title={
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="share"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                          className="svg-inline--fa fa-share fa-w-16 fa-2x">
                          <path
                            fill="currentColor"
                            d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                            className=""></path>
                        </svg>
                      }
                      id="dropdown-menu-align-end">
                      <Dropdown.Item
                        href={`mailto:?subject=${
                          props.name + " " + strings.profile
                        }&body=${
                          encodeURIComponent(window.location.href) || ""
                        }`}
                        target={"_blank"}>
                        <a className="">
                          <img src="/images/social-img4.png" />
                          {strings.viaEmail}
                        </a>
                      </Dropdown.Item>
                      <Dropdown.Item
                        href={`https://api.whatsapp.com/send?text=${
                          props.name +
                          ` ${strings.profile}:\n` +
                          window.location.href
                        }`}
                        target={"_blank"}
                        data-action="share/whatsapp/share">
                        <a className="">
                          <img src="/images/social-img3.png" />
                          {strings.viaWhatsapp}
                        </a>
                      </Dropdown.Item>
                      {/*<Dropdown.Item*/}
                      {/*  href={`fb-messenger://share/?link=${window.location.href}&app_id=${U_FACEBOOK_ID}`}*/}
                      {/*  target={"_blank"}>*/}
                      {/*  <a className="">*/}
                      {/*    {" "}*/}
                      {/*    <img src="/images/social-img1.png" />*/}
                      {/*    {strings.viaMessenger}*/}
                      {/*  </a>*/}
                      {/*</Dropdown.Item>*/}
                      <Dropdown.Item onClick={copyUrl}>
                        <a className="">
                          {" "}
                          <img src="/images/copy.png" />
                          {!copied ? strings.CopyLink : strings.Copied}
                        </a>
                      </Dropdown.Item>
                    </DropdownButton>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div className="row">
        <div className="col-xl-6">
          <div className="d-flex justify-content-between">
            {props.isSitter ? (
              <div className="sitter-availability">
                <h3 className="mb-0 font-semibold">
                  &nbsp;{props.jobCompletion}
                </h3>
                <p className="font-12 mb-0">{strings.JobsCompleted}</p>
              </div>
            ) : null}
            {props.isSitter && (
              <div className="sitter-availability">
                <h3 className="mb-0 font-semibold">{props.petTakenCare}</h3>
                <p className="font-12 mb-0">{strings.Petstakencare}</p>
              </div>
            )}
            {props.isSitter && (
              <div className="sitter-availability">
                <h3 className="mb-0 font-semibold">
                  {props.happyCustomers + "%"}
                </h3>
                <p className="font-12 mb-0">{strings.HappyCustomers}</p>
              </div>
            )}
            {props.isSitter && (
              <div className="sitter-availability">
                <h3 className="mb-0 font-semibold">{props.rehireRate + "%"}</h3>
                <p className="font-12 mb-0">{strings.Rehirerate}</p>
              </div>
            )}
            {/*<div className="sitter-availability">*/}
            {/*  <h3 className="mb-0 font-semibold">{props.experience + "yrs"}</h3>*/}
            {/*  <p className="font-12 mb-0">{strings.Experience}</p>*/}
            {/*</div>*/}
          </div>
        </div>
        <div className="col-xl-4 offset-xl-2 featured-details my-auto d-none d-md-block">
          <div className="d-flex justify-content-end hotel-rating sitter-rating">
            <div className="rating-star d-flex">
              {[1, 2, 3, 4, 5].map((val, index) => (
                <div
                  key={index}
                  className={val <= props.rating ? "active" : ""}>
                  <svg
                    aria-hidden="true"
                    focusable="false"
                    data-prefix="fas"
                    data-icon="star"
                    role="img"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 576 512"
                    className="svg-inline--fa fa-star fa-w-18 fa-2x">
                    <path
                      fill="currentColor"
                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                      className=""></path>
                  </svg>
                </div>
              ))}
            </div>
            <div>
              <h3 className="font-semibold mb-0">
                {props.rating}{" "}
                <span className="font-14 font-normal">
                  <Link
                    href={{
                      pathname: "/sitter-profile/MyReviews",
                      query: { id: props.id },
                    }}>
                    {"(" + props.review + " " + strings.reviews + ")"}
                  </Link>
                </span>
              </h3>
            </div>
          </div>
        </div>
      </div>
      <CreateWishlist
        showModal={wishlistShowModal}
        hideModal={() => setWishlistShowModal(false)}
        reRender={(value) => getAllWishlist()}
        data={null}
        isSitterWishlist={true}
      />
      {props.isSitter && props.serviceId && (
        <div className="d-block d-md-none book-fixed">
          <div className="col-12">
            <Link
              href={{
                pathname: "/booking",
                query: {
                  sitterId: props.id,
                  name: props.name,
                  service: props.serviceId,
                },
              }}>
              <button className="btn btn-primary px-3">
                {strings.Contactnow}
              </button>
            </Link>
          </div>
        </div>
      )}
      <ReportSitterModal
        showModal={showReportModal}
        hideModal={() => setShowReportModal(false)}
        participantId={props.id}
        setReported={() => setReported(reported == true ? false : true)}
      />
    </div>
  );
};

export default SitterProfileHeader;
