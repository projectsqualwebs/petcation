import { strings } from "../../public/lang/Strings";
import AdditionalServicesModal from '../../components/common/AdditionalServicesModal';
import React, {useState} from 'react';

export default function SitterServices(props: any) {
  const [showAdditionalServices, setShowAdditionalServices] = useState(false);
  return (
    <div className="bg-white main-background pb-2">
        <h5 className="font-semibold mb-0">{props.name+strings.A_s + ' ' + strings.Services}</h5>
        <hr/>
      <div className="sitter-services-details p-0">
        {props.service.map((v,index) => {
          return (
            <div key={index} className="col-12 mb-3 p-0">
                <div className="row align-items-center">
                  <div className="col">
                    <h6 className="font-semibold mb-1">{v.service.name}</h6>
                    <p className="mb-0 font-14">Dog | Cat | Bird</p>
                  </div>
                  <div className="col-auto alignment">
                      <h6 className="font-semibold mb-1">¥800.00</h6>
                      <p className="font-14 mb-0">Starting from</p>
                  </div>
                </div>
            </div>
          );
        })}
      </div>
      <div className="text-center">
        <div className="read-more px-3">
          <h6 className="mb-1">
              <a type='button' onClick={()=>setShowAdditionalServices(true)}>{strings.SeeAdditionalServicesRates}</a>
          </h6>
          <p className="font-12 mb-2">{strings.PickUpAnddropOffBathingGrooming} </p>
        </div>
      </div>
      <AdditionalServicesModal
        showModal={showAdditionalServices}
        hideModal={()=>setShowAdditionalServices(false)}
        services={props.service}
        name={props.name}
      />
    </div>
  );
}
