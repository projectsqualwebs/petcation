import React, {useEffect, useState} from "react";
import RatingStars from "../common/RatingStars";
import {MY_REVIEWS} from "../../models/sitter.interface";
import moment from "moment";
import API from "../../api/Api";
import { useSnackbar } from "react-simple-snackbar";
import {errorOptions, successOptions} from "../../public/appData/AppData";
import Cookies from "universal-cookie";
import {strings} from "../../public/lang/Strings";

interface IProps {
  value: MY_REVIEWS;
  sitterDetails: {
    id: number;
    profile_picture: string;
    firstName: string;
    lastName: string;
  }
  isMe: boolean;
  updateSitterProfile: () => void;
}

const ReviewObject: React.FC<IProps> = ({value, sitterDetails, isMe, updateSitterProfile}: IProps) => {
  const api = new API();
  const [reply, setReply] = useState<string>('')
  const [openError, closeError] = useSnackbar(errorOptions);
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);

  const giveReply = (id) => {
    if(!reply) {
      openError(strings.PleaseEnterCommentFirst)
      return false
    }
    let data = {
      review_id: id,
      comment: reply
    }
      api.sitterReviewComment(data)
          .then((response) => {
            console.log(response.data)
            updateSitterProfile()
            openSuccess(strings.CommentedSuccessfully)
          })
          .catch((error) => openError(error.response.data.message));
    }

  return (
    <div className="review-main-tile">
      <div className="user-review-details">
        <div className="row mb-2">
          <div className="col-12 col-md">
            <div className="d-flex">
              <div className="sitter-profile-img">
                <img src={value.user_info.profile_picture} className="img-fluid" />
              </div>
              <div className="sitter-review-details ml-2 my-auto">
                <h5 className="mb-0 font-medium">{value.user_info.firstname + ' ' + value.user_info.lastname}</h5>
                <p className="font-12 mb-0">
                  {moment(value.created_at).format('DD-MM-YYYY') + " | " + moment(value.created_at).format('HH:MM a')}
                </p>
                <div className="d-flex rating-star">
                  <RatingStars rating={value?.average_rating} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <p className="font-14 ">{value.review}</p>
        {value.comment ? (
          <div className="row">
            <div className="col-12 col-md-11 col-lg-11 col-xl-11 offset-xl-1">
              <div className="d-flex align-items-center">
                <div className="sitter-profile-img review-replay">
                  <img src={sitterDetails.profile_picture} className="img-fluid" />
                </div>
                <div className="sitter-review-details ml-2 my-auto">
                  <p className="mb-0">{value.comment}</p>
                  <p className="mb-0 font-medium font-14">
                    {sitterDetails.firstName+ ' '+ sitterDetails.lastName}
                  </p>
                </div>
              </div>
            </div>
          </div>
        ) : isMe ? <div className='d-flex justify-content-between'>
          <div className="input-group mb-3">
            <input
                type="text"
                className="form-control"
                name="review_reply"
                placeholder="Write a comment"
                aria-label="Write a comment"
                aria-describedby="basic-addon2"
                value={reply}
                onChange={(e)=> setReply(e.target.value)}
            />
              <div className="input-group-append">
                <button onClick={() => giveReply(value.id)} className="btn btn-primary confirm-btn mt-0">
                  {strings.Reply}
                </button>
              </div>
          </div>
        </div> : null}
      </div>
      <hr />
    </div>
  );
};

export default ReviewObject;
