import Link from "next/link";
import { I_SEARCH_SITTER } from "../../models/searchSitter.interface";
import { strings } from "../../public/lang/Strings";
import API from "../../api/Api";
import React, { useEffect, useMemo, useState } from "react";
import {errorOptions, petSize, successOptions} from "../../public/appData/AppData";
import { useSnackbar } from "react-simple-snackbar";
import { useRouter } from "next/router";
import Cookies from "universal-cookie";
import { getCurrencySign, U_BASE_URL } from "../../api/Constants";
import { Dropdown, DropdownButton } from "react-bootstrap";
import boolean from "async-validator/dist-types/validator/boolean";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu/Menu";
import MenuItem from "@mui/material/MenuItem";
import CreateWishlist from "../common/CreateWishlist";

interface I_PROPS {
  props: I_SEARCH_SITTER;
  serviceId: any;
  getSitter: any;
  petType: any;
  transportAvailable: any;
  label: any;
  onMouseOver: any;
  onMouseOut: any;
  getWishlist?: () => void;
  wishlist?: any;
  groomingService?: any;
  medicalServices?: any;
}

let api = new API();

/**
 *
 * @param props contains data
 * @param serviceId contains service id of sitter booking
 * @param getSitter is function to update available sitters.
 * @param petType contains type of pet for which user want to make request.
 * @param transportAvailable contains boolean value to check transportation is available or not.
 * @param label contains service label
 * @funtion will render the sitter object with dynamic data.
 */
const SitterObject: React.FC<I_PROPS> = ({
  props,
  serviceId,
  getSitter,
  petType,
  transportAvailable,
  label,
  onMouseOver,
  onMouseOut,
  getWishlist,
  wishlist,
                                           medicalServices,groomingService
}: I_PROPS) => {
  const [openSuccess, closeSuccess] = useSnackbar(successOptions);
  const [openError, closeError] = useSnackbar(errorOptions);
  const [copied, setCopied] = useState(false);
  const cookie = new Cookies();
  const router = useRouter();
  const [showModal, setShowModal] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<any>(false);
  let token = cookie.get("token");

  const wishlistModal = useMemo(
    () => (
      <CreateWishlist
        showModal={showModal}
        hideModal={() => setShowModal(false)}
        reRender={(value) => getWishlist()}
        data={null}
        isSitterWishlist={true}
      />
    ),
    [showModal]
  );

  /**
   * @params contains address of sitter.
   * @function will trim the address of sitter to make it small if it is too large.
   *
   */
  const getTrimmedAddress = (address: string) => {
    if (address.length > 12) {
      let newString = address.substring(0, 12) + "...";
      return newString;
    } else {
      return address;
    }
  };

  /**
   * @params
   *        e: contains event which helps in prevent default.
   *        id: contains sitter id.
   * @function will copy url to clipboard to share or paste anywhere.
   * @success show openSuccess snackbar to user to update that url is copied successfully.
   */
  const copyUrl = (e, id) => {
    e.preventDefault();
    const el = document.createElement("input");
    el.value = api.getSitterProfileUrl(id);
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setCopied(true);
    openSuccess("Copied to clipboard!");
  };

  /**
   * @params
   *        year: contains value of experience in year
   *        month: contains value of experience in month
   * @function will return different text as per our preference.
   */
  const getExperience = (year: any, month: any) => {
    if (year == 1) {
      return `Novice`;
    } else if (year == 3) {
      return `Intermediate`;
    } else if (year == 5) {
      return `Proficient`; // Advanced
    } else if (year == 7) {
      return `Expert`;
    } else if (year == 8) {
      return `${
        month + " " + strings.year + " " + strings.of + " " + strings.experience
      }`;
    } else {
      return "";
    }
  };

  const handleClick = (event: any) => {
    if (token) {
      setAnchorEl(event.currentTarget);
    } else {
      router.push("/signin");
    }
  };
  const handleClose = () => {
    setAnchorEl(false);
  };

  /**
   * @fuction will mark sitter as favourite sitter via api call.
   */
  const markAsFavourite = (listId: any, spotId: any) => {
    setAnchorEl(false);
    let data = {
      list_id: listId,
      sitter_id: spotId,
    };
    api
      .markSitterAsFavourite(data)
      .then((res) => {
        getSitter();
        openSuccess(res.data.message);
      })
      .catch((err) => openError(err.response.data.message));
  };


  return (

        <div className="col-md-6 px-2"
             key={props.id}
             onMouseOver={onMouseOver}
             onMouseOut={onMouseOut}
        >
          <div className="bg-white search-background">
            <div className="col-12 search-sitter-content pr-0">
              <div className="row mb-2">
                <Link href={{ pathname: "/sitter-profile/" + props.id, query: {serviceId: serviceId} }}>
                  <div className="col cursor-pointer pr-0">
                    <div className="row">
                      <div className="col-auto p-0 search-sitter-img">
                        <img
                            src={props.profile_picture}
                            alt=""
                            className="img-fluid"
                        />
                      </div>
                      <div className="col search-sitter-details my-auto">
                        <h6 className="mb-1">
                          {props.firstname + " " + props.lastname} <img src="/images/icons2.png" />{" "}
                        </h6>
                        <p className='mb-0 mt-1 font-medium'>
                        <svg className="mr-1" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="check-double" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                          <path fill="currentColor" d="M444.96 159l-12.16-11c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L131.77 428 31.42 329c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L3.04 340C1.01 342.67 0 345.67 0 349s1.01 6 3.04 8l120.62 119c2.69 2.67 5.57 4 8.62 4s5.92-1.33 8.62-4l304.07-300c2.03-2 3.04-4.67 3.04-8s-1.02-6.33-3.05-9zM127.17 284.03c2.65 2.65 5.48 3.97 8.47 3.97s5.82-1.32 8.47-3.97L365.01 63.8c1.99-2 2.99-4.65 2.99-7.96s-1-6.29-2.99-8.94l-11.96-10.93c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97L135.14 236.34l-72.25-72.03c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97l-11.96 10.93C33 177.89 32 180.87 32 184.18s1 5.96 2.99 7.95l92.18 91.9z" className=""></path>
                        </svg>
                        {getExperience(props.experience_in_year, props.experience_in_month)}
                      </p>
                        {props?.address?.description && (
                          <p className="font-12 mb-0">
                            {props.address.description.slice(0, 20)}
                            {props.address.description.length > 20 && <>... <Link href={{ pathname: "/sitter-profile/" + props.id, query: {serviceId: serviceId} }}>
                                                                                  <a style={{ color: 'black' }}>Read More</a>
                                                                                </Link> </>}
                          </p>
                        )}

                        {/* <p className="font-12 mb-0">
                          {props.address ? props.address.hide_address == 0 ? props.address.address :(props.address?.city  + (props.address.prefecture ? " , "+props?.address?.prefecture : "" )) :""}
                        </p> */}

                      </div>
                    </div>
                  </div>
                </Link>
                <div className="col-auto my-auto">
                  <div className="d-flex profile-share-icon">
                    <div className="profile-share-drop">
                      <DropdownButton
                          className='bg-transparent'
                          align="end"
                          title={<a className="dropdown-toggle">
                            <svg
                                className="ml-0"
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="fas"
                                data-icon="share"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512"
                            >
                              <path
                                  fill="currentColor"
                                  d="M503.691 189.836L327.687 37.851C312.281 24.546 288 35.347 288 56.015v80.053C127.371 137.907 0 170.1 0 322.326c0 61.441 39.581 122.309 83.333 154.132 13.653 9.931 33.111-2.533 28.077-18.631C66.066 312.814 132.917 274.316 288 272.085V360c0 20.7 24.3 31.453 39.687 18.164l176.004-152c11.071-9.562 11.086-26.753 0-36.328z"
                                  className=""
                              ></path>
                            </svg>
                          </a>}
                          id="dropdown-menu-align-end"
                      >
                        <p className="mb-0 font-semibold font-12">{strings.ShareThisProfile}</p>
                        <Dropdown.Item href={`mailto:?subject=${props.firstname + " " + props.lastname + ` ${strings.profile}`}&body=${encodeURIComponent(`${U_BASE_URL}sitter-profile/${props.id}?serviceId=1`) || ''}`}>
                          <a className="">
                            <img src="/images/social-img4.png"/> {strings.viaEmail}
                          </a>
                        </Dropdown.Item>
                        <Dropdown.Item href={`https://api.whatsapp.com/send?text=${props.firstname + " " + props.lastname + ' profile:\n' + (`${U_BASE_URL}sitter-profile/${props.id}?serviceId=1`)}`} data-action="share/whatsapp/share">
                          <a className="">
                            <img src="/images/social-img3.png"/> {strings.viaWhatsapp}
                          </a>
                        </Dropdown.Item>
                        {/*<Dropdown.Item eventKey="2">*/}
                        {/*  <a className=""*/}
                        {/*     href={`fb-messenger://share/?link=${`${U_BASE_URL}sitter-profile/${props.id}?serviceId=1`}&app_id=123456789`}>*/}
                        {/*    {" "}*/}
                        {/*    <img src="/images/social-img1.png"/> {strings.viaMessenger}*/}
                        {/*  </a>*/}
                        {/*</Dropdown.Item>*/}
                        <Dropdown.Item eventKey="3">
                          <a className="" onClick={(e) => copyUrl(e, props.id)}>
                            {" "}
                            <img src="/images/social-img5.png"/>
                            {!copied ? strings.CopyLink : strings.Copied}
                          </a>
                        </Dropdown.Item>
                      </DropdownButton>
                        <div>
                            <Button
                                id={`basic-button${props?.id}`}
                                aria-controls={open ? `basic-menu${props?.id}` : undefined}
                                aria-haspopup="true"
                                className={"p-0s"}
                                aria-expanded={open ? 'true' : undefined}
                                onClick={handleClick}
                            >
                                <div className="single-share-details cursor-pointer">
                                    {props ? <a>
                                        {props?.is_favorite && <svg viewBox="0 0 512 512">
                                            <path
                                                fill={"#20847e"}
                                                d="M0 190.9V185.1C0 115.2 50.52 55.58 119.4 44.1C164.1 36.51 211.4 51.37 244 84.02L256 96L267.1 84.02C300.6 51.37 347 36.51 392.6 44.1C461.5 55.58 512 115.2 512 185.1V190.9C512 232.4 494.8 272.1 464.4 300.4L283.7 469.1C276.2 476.1 266.3 480 256 480C245.7 480 235.8 476.1 228.3 469.1L47.59 300.4C17.23 272.1 .0003 232.4 .0003 190.9L0 190.9z"
                                            />
                                        </svg>}
                                        {!props?.is_favorite && <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path d="M244 84L255.1 96L267.1 84.02C300.6 51.37 347 36.51 392.6 44.1C461.5 55.58 512 115.2 512 185.1V190.9C512 232.4 494.8 272.1 464.4 300.4L283.7 469.1C276.2 476.1 266.3 480 256 480C245.7 480 235.8 476.1 228.3 469.1L47.59 300.4C17.23 272.1 0 232.4 0 190.9V185.1C0 115.2 50.52 55.58 119.4 44.1C164.1 36.51 211.4 51.37 244 84C243.1 84 244 84.01 244 84L244 84zM255.1 163.9L210.1 117.1C188.4 96.28 157.6 86.4 127.3 91.44C81.55 99.07 48 138.7 48 185.1V190.9C48 219.1 59.71 246.1 80.34 265.3L256 429.3L431.7 265.3C452.3 246.1 464 219.1 464 190.9V185.1C464 138.7 430.4 99.07 384.7 91.44C354.4 86.4 323.6 96.28 301.9 117.1L255.1 163.9z"/></svg>}
                                    </a> : null}
                                </div>
                            </Button>
                            <Menu
                                id={`basic-menu${props?.id}`}
                                anchorEl={anchorEl}
                                open={anchorEl}
                                onClose={handleClose}
                                MenuListProps={{
                                    'aria-labelledby': 'basic-button',
                                }}
                            >
                                {wishlist?.map((val) => <MenuItem onClick={()=> markAsFavourite(val.id, props.id)}>{val.name}</MenuItem>)}
                                <MenuItem onClick={() => {
                                    setShowModal(true)
                                    handleClose()
                                }}>{strings.CreateNewList}</MenuItem>
                            </Menu>
                        </div>
                    </div>
                    <div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
              <p className="font-12 mb-0">
                         <label className="text-secondary">Address:</label> {props.address ? props.address.hide_address == 0 ? props.address.address :(props.address?.city  + (props.address.prefecture ? " , "+props?.address?.prefecture : "" )) :""}
              </p>
              </div>
              <div className="row mb-2 cursor-pointer">
              <Link href={{ pathname: "/sitter-profile/" + props.id , query: {serviceId: serviceId} }}>
                    <div className="col pl-0">
                      {/* <p className='mb-0 mt-1 font-medium'>
                        <svg className="mr-1" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="check-double" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                          <path fill="currentColor" d="M444.96 159l-12.16-11c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L131.77 428 31.42 329c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L3.04 340C1.01 342.67 0 345.67 0 349s1.01 6 3.04 8l120.62 119c2.69 2.67 5.57 4 8.62 4s5.92-1.33 8.62-4l304.07-300c2.03-2 3.04-4.67 3.04-8s-1.02-6.33-3.05-9zM127.17 284.03c2.65 2.65 5.48 3.97 8.47 3.97s5.82-1.32 8.47-3.97L365.01 63.8c1.99-2 2.99-4.65 2.99-7.96s-1-6.29-2.99-8.94l-11.96-10.93c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97L135.14 236.34l-72.25-72.03c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97l-11.96 10.93C33 177.89 32 180.87 32 184.18s1 5.96 2.99 7.95l92.18 91.9z" className=""></path>
                        </svg>
                        {getExperience(props.experience_in_year, props.experience_in_month)}
                      </p> */}
                      {(serviceId == 3 || serviceId == 5) &&
                          <>
                          <span className="text-muted font-10 mt-1">Duration : {props.duration + "min"}</span>
                          <br/>
                        </>
                      }
                      <div className="badge bg-secondary w-50 h-30"> <span className="text-white font-normal text-left"> {props.repeat_client ? props.repeat_client : "0" } {strings.repeat_client +"     "} </span></div>
                      <div>
                      {!(serviceId == 6 || serviceId == 7) && <span className="text-muted font-10">Pet size :  {petSize.find(item => item.value == props.size)?.label}</span>}
                      {serviceId === 6 && <span className="text-muted font-10">Service :  {groomingService.find(item => item.value == props.grooming)?.label}</span>}
                      {serviceId === 7 && <span className="text-muted font-10">Service :  {medicalServices.find(item => item.value == props.medical)?.label}</span>}
                      </div>
                    <div className="text-muted font-10">{strings.ApproximateLocation} :  {props?.distance}</div>
                    </div>
              </Link>
                    <div className="col-auto p-left">
                      <div className="contact-now">
                        {(serviceId == 3 || serviceId == 5) ? <Link
                            href={{
                              pathname: "/booking",
                              query: {
                                sitterId: props.id,
                                service: serviceId,
                                name: props.firstname + " " + props.lastname,
                                petType: petType?.key,
                                duration: props.duration
                              },
                            }}
                        >
                          <button className="btn btn-primary px-2">{strings.Contactnow}</button>
                        </Link> : serviceId == 6 ? <Link
                            href={{
                              pathname: "/booking",
                              query: {
                                sitterId: props.id,
                                service: serviceId,
                                name: props.firstname + " " + props.lastname,
                                petType: petType?.key,
                                grooming: props.grooming,
                              }
                            }}
                        >
                          <button className="btn btn-primary px-2">{strings.Contactnow}</button>
                        </Link> : serviceId == 7 ? <Link
                            href={{
                              pathname: "/booking",
                              query: {
                                sitterId: props.id,
                                service: serviceId,
                                name: props.firstname + " " + props.lastname,
                                petType: petType?.key,
                                medical: props.medical,
                              }
                            }}
                        >
                          <button className="btn btn-primary px-2">{strings.Contactnow}</button>
                        </Link> : <Link
                            href={{
                              pathname: "/booking",
                              query: {
                                sitterId: props.id,
                                service: serviceId,
                                name: props.firstname + " " + props.lastname,
                                petType: petType?.key}
                            }}
                        >
                          <button className="btn btn-primary px-2">{strings.Contactnow}</button>
                        </Link>}
                      </div>
                      <div className="view-details">
                        <Link href={{ pathname: "/sitter-profile/" + props.id, query: {serviceId: serviceId, duration: props.duration} }}>
                          {strings.Viewdetails}
                        </Link>
                      </div>
                    </div>
                  </div>
                  <Link href={{ pathname: "/sitter-profile/" + props.id, query: {serviceId: serviceId} }}>
                  <div className="row align-items-center cursor-pointer">
                    <div className="col pl-0">
                      <div className="d-flex featured-details">
                        <div className="d-flex hotel-rating">
                          <div className="rating-star d-flex">
                            {[1,2,3,4,5].map((value)=>
                                <div key={`rating_${value}`} className={value <= props.overall_rate ? "active" : ''}>
                                  <svg
                                      aria-hidden="true"
                                      focusable="false"
                                      data-prefix="fas"
                                      data-icon="star"
                                      role="img"
                                      xmlns="http://www.w3.org/2000/svg"
                                      viewBox="0 0 576 512"
                                      className="svg-inline--fa fa-star fa-w-18 fa-2x"
                                  >
                                    <path
                                        fill="currentColor"
                                        d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                                        className=""
                                    ></path>
                                  </svg>
                                </div>
                            )}
                          </div>
                          <div className="my-auto">
                            <h6 className="mb-0  font-semibold">
                              {props.overall_rate}
                              <span className="text-muted font-normal font-12">
                            {`(${props.total_review} ${strings.reviews})`}
                          </span>
                            </h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-auto p-left">
                      {serviceId != 6 ? <div>
                        <h6 className="mb-0  font-semibold">
                          {`${getCurrencySign() + props.price}`}
                          <span className="text-muted font-normal font-12">
                            / {label == strings.DropInVisits ? strings.request : strings.night}
                          </span>
                        </h6>
                      </div> : null}
                    </div>
                  </div>
              </Link>
            </div>

                  <div className="user-review-details">
                    <div className="row mb-0">
                        <div className="sitter-profile-img col-2">
                            <img src={props.single_review?.user_info?.profile_picture} className="ml-3"/>
                        </div>
                        <div className="sitter-review-details mt-1 col-10">
                        {props.single_review ? <>
                            <p className="mb-0 font-medium font-12">
                              {props.single_review?.user_info?.firstname+ ' '+ props.single_review?.user_info?.lastname}
                            </p>
                            <p className="font-10">
                               {props.single_review.review.split(' ').slice(0, 15).join(' ')}
                                {props.single_review.review.split(' ').slice(0, 15).length  >12 && '...' }
                            </p>  </>:   <div className="d-flex col-12 p-3 align-items-start justify-content-start">
                                   <p className="p-2 mb-0">No review found.</p>
                             </div>}
                        </div>
                    </div>
                 </div>
          </div>
         {wishlistModal}
        </div>
  );
};

export default SitterObject;
